
_ls:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
  close(fd);
}

int
main(int argc, char *argv[])
{
   0:	f3 0f 1e fb          	endbr32 
   4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   8:	83 e4 f0             	and    $0xfffffff0,%esp
   b:	ff 71 fc             	push   -0x4(%ecx)
   e:	55                   	push   %ebp
   f:	89 e5                	mov    %esp,%ebp
  11:	57                   	push   %edi
  12:	56                   	push   %esi
  13:	53                   	push   %ebx
  14:	bb 01 00 00 00       	mov    $0x1,%ebx
  19:	51                   	push   %ecx
  1a:	83 ec 08             	sub    $0x8,%esp
  1d:	8b 31                	mov    (%ecx),%esi
  1f:	8b 79 04             	mov    0x4(%ecx),%edi
  int i;

  if(argc < 2){
  22:	83 fe 01             	cmp    $0x1,%esi
  25:	7e 23                	jle    4a <main+0x4a>
  27:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  2e:	66 90                	xchg   %ax,%ax
    ls(".");
    exit();
  }
  for(i=1; i<argc; i++)
    ls(argv[i]);
  30:	83 ec 0c             	sub    $0xc,%esp
  33:	ff 34 9f             	push   (%edi,%ebx,4)
  for(i=1; i<argc; i++)
  36:	83 c3 01             	add    $0x1,%ebx
    ls(argv[i]);
  39:	e8 c2 00 00 00       	call   100 <ls>
  for(i=1; i<argc; i++)
  3e:	83 c4 10             	add    $0x10,%esp
  41:	39 de                	cmp    %ebx,%esi
  43:	75 eb                	jne    30 <main+0x30>
  exit();
  45:	e8 59 05 00 00       	call   5a3 <exit>
    ls(".");
  4a:	83 ec 0c             	sub    $0xc,%esp
  4d:	68 b0 0b 00 00       	push   $0xbb0
  52:	e8 a9 00 00 00       	call   100 <ls>
    exit();
  57:	e8 47 05 00 00       	call   5a3 <exit>
  5c:	66 90                	xchg   %ax,%ax
  5e:	66 90                	xchg   %ax,%ax

00000060 <fmtname>:
{
  60:	f3 0f 1e fb          	endbr32 
  64:	55                   	push   %ebp
  65:	89 e5                	mov    %esp,%ebp
  67:	56                   	push   %esi
  68:	53                   	push   %ebx
  69:	8b 75 08             	mov    0x8(%ebp),%esi
  for(p=path+strlen(path); p >= path && *p != '/'; p--)
  6c:	83 ec 0c             	sub    $0xc,%esp
  6f:	56                   	push   %esi
  70:	e8 4b 03 00 00       	call   3c0 <strlen>
  75:	83 c4 10             	add    $0x10,%esp
  78:	01 f0                	add    %esi,%eax
  7a:	89 c3                	mov    %eax,%ebx
  7c:	73 0b                	jae    89 <fmtname+0x29>
  7e:	eb 0e                	jmp    8e <fmtname+0x2e>
  80:	8d 43 ff             	lea    -0x1(%ebx),%eax
  83:	39 c6                	cmp    %eax,%esi
  85:	77 0a                	ja     91 <fmtname+0x31>
  87:	89 c3                	mov    %eax,%ebx
  89:	80 3b 2f             	cmpb   $0x2f,(%ebx)
  8c:	75 f2                	jne    80 <fmtname+0x20>
  p++;
  8e:	83 c3 01             	add    $0x1,%ebx
  if(strlen(p) >= DIRSIZ)
  91:	83 ec 0c             	sub    $0xc,%esp
  94:	53                   	push   %ebx
  95:	e8 26 03 00 00       	call   3c0 <strlen>
  9a:	83 c4 10             	add    $0x10,%esp
  9d:	83 f8 0d             	cmp    $0xd,%eax
  a0:	77 4a                	ja     ec <fmtname+0x8c>
  memmove(buf, p, strlen(p));
  a2:	83 ec 0c             	sub    $0xc,%esp
  a5:	53                   	push   %ebx
  a6:	e8 15 03 00 00       	call   3c0 <strlen>
  ab:	83 c4 0c             	add    $0xc,%esp
  ae:	50                   	push   %eax
  af:	53                   	push   %ebx
  b0:	68 1c 0f 00 00       	push   $0xf1c
  b5:	e8 b6 04 00 00       	call   570 <memmove>
  memset(buf+strlen(p), ' ', DIRSIZ-strlen(p));
  ba:	89 1c 24             	mov    %ebx,(%esp)
  bd:	e8 fe 02 00 00       	call   3c0 <strlen>
  c2:	89 1c 24             	mov    %ebx,(%esp)
  return buf;
  c5:	bb 1c 0f 00 00       	mov    $0xf1c,%ebx
  memset(buf+strlen(p), ' ', DIRSIZ-strlen(p));
  ca:	89 c6                	mov    %eax,%esi
  cc:	e8 ef 02 00 00       	call   3c0 <strlen>
  d1:	ba 0e 00 00 00       	mov    $0xe,%edx
  d6:	83 c4 0c             	add    $0xc,%esp
  d9:	29 f2                	sub    %esi,%edx
  db:	05 1c 0f 00 00       	add    $0xf1c,%eax
  e0:	52                   	push   %edx
  e1:	6a 20                	push   $0x20
  e3:	50                   	push   %eax
  e4:	e8 17 03 00 00       	call   400 <memset>
  return buf;
  e9:	83 c4 10             	add    $0x10,%esp
}
  ec:	8d 65 f8             	lea    -0x8(%ebp),%esp
  ef:	89 d8                	mov    %ebx,%eax
  f1:	5b                   	pop    %ebx
  f2:	5e                   	pop    %esi
  f3:	5d                   	pop    %ebp
  f4:	c3                   	ret    
  f5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000100 <ls>:
{
 100:	f3 0f 1e fb          	endbr32 
 104:	55                   	push   %ebp
 105:	89 e5                	mov    %esp,%ebp
 107:	57                   	push   %edi
 108:	56                   	push   %esi
 109:	53                   	push   %ebx
 10a:	81 ec 64 02 00 00    	sub    $0x264,%esp
 110:	8b 7d 08             	mov    0x8(%ebp),%edi
  if((fd = open(path, 0)) < 0){
 113:	6a 00                	push   $0x0
 115:	57                   	push   %edi
 116:	e8 c8 04 00 00       	call   5e3 <open>
 11b:	83 c4 10             	add    $0x10,%esp
 11e:	85 c0                	test   %eax,%eax
 120:	0f 88 9a 01 00 00    	js     2c0 <ls+0x1c0>
  if(fstat(fd, &st) < 0){
 126:	83 ec 08             	sub    $0x8,%esp
 129:	8d b5 d4 fd ff ff    	lea    -0x22c(%ebp),%esi
 12f:	89 c3                	mov    %eax,%ebx
 131:	56                   	push   %esi
 132:	50                   	push   %eax
 133:	e8 c3 04 00 00       	call   5fb <fstat>
 138:	83 c4 10             	add    $0x10,%esp
 13b:	85 c0                	test   %eax,%eax
 13d:	0f 88 bd 01 00 00    	js     300 <ls+0x200>
  switch(st.type){
 143:	0f b7 85 d4 fd ff ff 	movzwl -0x22c(%ebp),%eax
 14a:	66 83 f8 01          	cmp    $0x1,%ax
 14e:	74 60                	je     1b0 <ls+0xb0>
 150:	66 83 f8 02          	cmp    $0x2,%ax
 154:	74 1a                	je     170 <ls+0x70>
  close(fd);
 156:	83 ec 0c             	sub    $0xc,%esp
 159:	53                   	push   %ebx
 15a:	e8 6c 04 00 00       	call   5cb <close>
 15f:	83 c4 10             	add    $0x10,%esp
}
 162:	8d 65 f4             	lea    -0xc(%ebp),%esp
 165:	5b                   	pop    %ebx
 166:	5e                   	pop    %esi
 167:	5f                   	pop    %edi
 168:	5d                   	pop    %ebp
 169:	c3                   	ret    
 16a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    printf(1, "%s %d %d %d\n", fmtname(path), st.type, st.ino, st.size);
 170:	83 ec 0c             	sub    $0xc,%esp
 173:	8b 95 e4 fd ff ff    	mov    -0x21c(%ebp),%edx
 179:	8b b5 dc fd ff ff    	mov    -0x224(%ebp),%esi
 17f:	57                   	push   %edi
 180:	89 95 b4 fd ff ff    	mov    %edx,-0x24c(%ebp)
 186:	e8 d5 fe ff ff       	call   60 <fmtname>
 18b:	8b 95 b4 fd ff ff    	mov    -0x24c(%ebp),%edx
 191:	59                   	pop    %ecx
 192:	5f                   	pop    %edi
 193:	52                   	push   %edx
 194:	56                   	push   %esi
 195:	6a 02                	push   $0x2
 197:	50                   	push   %eax
 198:	68 90 0b 00 00       	push   $0xb90
 19d:	6a 01                	push   $0x1
 19f:	e8 2c 06 00 00       	call   7d0 <printf>
    break;
 1a4:	83 c4 20             	add    $0x20,%esp
 1a7:	eb ad                	jmp    156 <ls+0x56>
 1a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(strlen(path) + 1 + DIRSIZ + 1 > sizeof buf){
 1b0:	83 ec 0c             	sub    $0xc,%esp
 1b3:	57                   	push   %edi
 1b4:	e8 07 02 00 00       	call   3c0 <strlen>
 1b9:	83 c4 10             	add    $0x10,%esp
 1bc:	83 c0 10             	add    $0x10,%eax
 1bf:	3d 00 02 00 00       	cmp    $0x200,%eax
 1c4:	0f 87 16 01 00 00    	ja     2e0 <ls+0x1e0>
    strcpy(buf, path);
 1ca:	83 ec 08             	sub    $0x8,%esp
 1cd:	57                   	push   %edi
 1ce:	8d bd e8 fd ff ff    	lea    -0x218(%ebp),%edi
 1d4:	57                   	push   %edi
 1d5:	e8 66 01 00 00       	call   340 <strcpy>
    p = buf+strlen(buf);
 1da:	89 3c 24             	mov    %edi,(%esp)
 1dd:	e8 de 01 00 00       	call   3c0 <strlen>
    while(read(fd, &de, sizeof(de)) == sizeof(de)){
 1e2:	83 c4 10             	add    $0x10,%esp
    p = buf+strlen(buf);
 1e5:	01 f8                	add    %edi,%eax
    *p++ = '/';
 1e7:	8d 48 01             	lea    0x1(%eax),%ecx
    p = buf+strlen(buf);
 1ea:	89 85 a8 fd ff ff    	mov    %eax,-0x258(%ebp)
    *p++ = '/';
 1f0:	89 8d a4 fd ff ff    	mov    %ecx,-0x25c(%ebp)
 1f6:	c6 00 2f             	movb   $0x2f,(%eax)
    while(read(fd, &de, sizeof(de)) == sizeof(de)){
 1f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 200:	83 ec 04             	sub    $0x4,%esp
 203:	8d 85 c4 fd ff ff    	lea    -0x23c(%ebp),%eax
 209:	6a 10                	push   $0x10
 20b:	50                   	push   %eax
 20c:	53                   	push   %ebx
 20d:	e8 a9 03 00 00       	call   5bb <read>
 212:	83 c4 10             	add    $0x10,%esp
 215:	83 f8 10             	cmp    $0x10,%eax
 218:	0f 85 38 ff ff ff    	jne    156 <ls+0x56>
      if(de.inum == 0)
 21e:	66 83 bd c4 fd ff ff 	cmpw   $0x0,-0x23c(%ebp)
 225:	00 
 226:	74 d8                	je     200 <ls+0x100>
      memmove(p, de.name, DIRSIZ);
 228:	83 ec 04             	sub    $0x4,%esp
 22b:	8d 85 c6 fd ff ff    	lea    -0x23a(%ebp),%eax
 231:	6a 0e                	push   $0xe
 233:	50                   	push   %eax
 234:	ff b5 a4 fd ff ff    	push   -0x25c(%ebp)
 23a:	e8 31 03 00 00       	call   570 <memmove>
      p[DIRSIZ] = 0;
 23f:	8b 85 a8 fd ff ff    	mov    -0x258(%ebp),%eax
 245:	c6 40 0f 00          	movb   $0x0,0xf(%eax)
      if(stat(buf, &st) < 0){
 249:	58                   	pop    %eax
 24a:	5a                   	pop    %edx
 24b:	56                   	push   %esi
 24c:	57                   	push   %edi
 24d:	e8 8e 02 00 00       	call   4e0 <stat>
 252:	83 c4 10             	add    $0x10,%esp
 255:	85 c0                	test   %eax,%eax
 257:	0f 88 cb 00 00 00    	js     328 <ls+0x228>
      printf(1, "%s %d %d %d\n", fmtname(buf), st.type, st.ino, st.size);
 25d:	83 ec 0c             	sub    $0xc,%esp
 260:	8b 8d e4 fd ff ff    	mov    -0x21c(%ebp),%ecx
 266:	8b 95 dc fd ff ff    	mov    -0x224(%ebp),%edx
 26c:	57                   	push   %edi
 26d:	0f bf 85 d4 fd ff ff 	movswl -0x22c(%ebp),%eax
 274:	89 8d ac fd ff ff    	mov    %ecx,-0x254(%ebp)
 27a:	89 95 b0 fd ff ff    	mov    %edx,-0x250(%ebp)
 280:	89 85 b4 fd ff ff    	mov    %eax,-0x24c(%ebp)
 286:	e8 d5 fd ff ff       	call   60 <fmtname>
 28b:	5a                   	pop    %edx
 28c:	8b 95 b0 fd ff ff    	mov    -0x250(%ebp),%edx
 292:	59                   	pop    %ecx
 293:	8b 8d ac fd ff ff    	mov    -0x254(%ebp),%ecx
 299:	51                   	push   %ecx
 29a:	52                   	push   %edx
 29b:	ff b5 b4 fd ff ff    	push   -0x24c(%ebp)
 2a1:	50                   	push   %eax
 2a2:	68 90 0b 00 00       	push   $0xb90
 2a7:	6a 01                	push   $0x1
 2a9:	e8 22 05 00 00       	call   7d0 <printf>
 2ae:	83 c4 20             	add    $0x20,%esp
 2b1:	e9 4a ff ff ff       	jmp    200 <ls+0x100>
 2b6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 2bd:	8d 76 00             	lea    0x0(%esi),%esi
    printf(2, "ls: cannot open %s\n", path);
 2c0:	83 ec 04             	sub    $0x4,%esp
 2c3:	57                   	push   %edi
 2c4:	68 68 0b 00 00       	push   $0xb68
 2c9:	6a 02                	push   $0x2
 2cb:	e8 00 05 00 00       	call   7d0 <printf>
    return;
 2d0:	83 c4 10             	add    $0x10,%esp
}
 2d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
 2d6:	5b                   	pop    %ebx
 2d7:	5e                   	pop    %esi
 2d8:	5f                   	pop    %edi
 2d9:	5d                   	pop    %ebp
 2da:	c3                   	ret    
 2db:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 2df:	90                   	nop
      printf(1, "ls: path too long\n");
 2e0:	83 ec 08             	sub    $0x8,%esp
 2e3:	68 9d 0b 00 00       	push   $0xb9d
 2e8:	6a 01                	push   $0x1
 2ea:	e8 e1 04 00 00       	call   7d0 <printf>
      break;
 2ef:	83 c4 10             	add    $0x10,%esp
 2f2:	e9 5f fe ff ff       	jmp    156 <ls+0x56>
 2f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 2fe:	66 90                	xchg   %ax,%ax
    printf(2, "ls: cannot stat %s\n", path);
 300:	83 ec 04             	sub    $0x4,%esp
 303:	57                   	push   %edi
 304:	68 7c 0b 00 00       	push   $0xb7c
 309:	6a 02                	push   $0x2
 30b:	e8 c0 04 00 00       	call   7d0 <printf>
    close(fd);
 310:	89 1c 24             	mov    %ebx,(%esp)
 313:	e8 b3 02 00 00       	call   5cb <close>
    return;
 318:	83 c4 10             	add    $0x10,%esp
}
 31b:	8d 65 f4             	lea    -0xc(%ebp),%esp
 31e:	5b                   	pop    %ebx
 31f:	5e                   	pop    %esi
 320:	5f                   	pop    %edi
 321:	5d                   	pop    %ebp
 322:	c3                   	ret    
 323:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 327:	90                   	nop
        printf(1, "ls: cannot stat %s\n", buf);
 328:	83 ec 04             	sub    $0x4,%esp
 32b:	57                   	push   %edi
 32c:	68 7c 0b 00 00       	push   $0xb7c
 331:	6a 01                	push   $0x1
 333:	e8 98 04 00 00       	call   7d0 <printf>
        continue;
 338:	83 c4 10             	add    $0x10,%esp
 33b:	e9 c0 fe ff ff       	jmp    200 <ls+0x100>

00000340 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
 340:	f3 0f 1e fb          	endbr32 
 344:	55                   	push   %ebp
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
 345:	31 c0                	xor    %eax,%eax
{
 347:	89 e5                	mov    %esp,%ebp
 349:	53                   	push   %ebx
 34a:	8b 4d 08             	mov    0x8(%ebp),%ecx
 34d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while((*s++ = *t++) != 0)
 350:	0f b6 14 03          	movzbl (%ebx,%eax,1),%edx
 354:	88 14 01             	mov    %dl,(%ecx,%eax,1)
 357:	83 c0 01             	add    $0x1,%eax
 35a:	84 d2                	test   %dl,%dl
 35c:	75 f2                	jne    350 <strcpy+0x10>
    ;
  return os;
}
 35e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 361:	89 c8                	mov    %ecx,%eax
 363:	c9                   	leave  
 364:	c3                   	ret    
 365:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 36c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000370 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 370:	f3 0f 1e fb          	endbr32 
 374:	55                   	push   %ebp
 375:	89 e5                	mov    %esp,%ebp
 377:	53                   	push   %ebx
 378:	8b 4d 08             	mov    0x8(%ebp),%ecx
 37b:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
 37e:	0f b6 01             	movzbl (%ecx),%eax
 381:	0f b6 1a             	movzbl (%edx),%ebx
 384:	84 c0                	test   %al,%al
 386:	75 18                	jne    3a0 <strcmp+0x30>
 388:	eb 2a                	jmp    3b4 <strcmp+0x44>
 38a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
 390:	0f b6 41 01          	movzbl 0x1(%ecx),%eax
    p++, q++;
 394:	83 c1 01             	add    $0x1,%ecx
 397:	8d 5a 01             	lea    0x1(%edx),%ebx
  while(*p && *p == *q)
 39a:	84 c0                	test   %al,%al
 39c:	74 12                	je     3b0 <strcmp+0x40>
    p++, q++;
 39e:	89 da                	mov    %ebx,%edx
  while(*p && *p == *q)
 3a0:	0f b6 1a             	movzbl (%edx),%ebx
 3a3:	38 c3                	cmp    %al,%bl
 3a5:	74 e9                	je     390 <strcmp+0x20>
  return (uchar)*p - (uchar)*q;
 3a7:	29 d8                	sub    %ebx,%eax
}
 3a9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 3ac:	c9                   	leave  
 3ad:	c3                   	ret    
 3ae:	66 90                	xchg   %ax,%ax
  return (uchar)*p - (uchar)*q;
 3b0:	0f b6 5a 01          	movzbl 0x1(%edx),%ebx
 3b4:	31 c0                	xor    %eax,%eax
 3b6:	29 d8                	sub    %ebx,%eax
}
 3b8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 3bb:	c9                   	leave  
 3bc:	c3                   	ret    
 3bd:	8d 76 00             	lea    0x0(%esi),%esi

000003c0 <strlen>:

uint
strlen(const char *s)
{
 3c0:	f3 0f 1e fb          	endbr32 
 3c4:	55                   	push   %ebp
 3c5:	89 e5                	mov    %esp,%ebp
 3c7:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
 3ca:	80 3a 00             	cmpb   $0x0,(%edx)
 3cd:	74 21                	je     3f0 <strlen+0x30>
 3cf:	31 c0                	xor    %eax,%eax
 3d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 3d8:	83 c0 01             	add    $0x1,%eax
 3db:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
 3df:	89 c1                	mov    %eax,%ecx
 3e1:	75 f5                	jne    3d8 <strlen+0x18>
    ;
  return n;
}
 3e3:	89 c8                	mov    %ecx,%eax
 3e5:	5d                   	pop    %ebp
 3e6:	c3                   	ret    
 3e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 3ee:	66 90                	xchg   %ax,%ax
  for(n = 0; s[n]; n++)
 3f0:	31 c9                	xor    %ecx,%ecx
}
 3f2:	5d                   	pop    %ebp
 3f3:	89 c8                	mov    %ecx,%eax
 3f5:	c3                   	ret    
 3f6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 3fd:	8d 76 00             	lea    0x0(%esi),%esi

00000400 <memset>:

void*
memset(void *dst, int c, uint n)
{
 400:	f3 0f 1e fb          	endbr32 
 404:	55                   	push   %ebp
 405:	89 e5                	mov    %esp,%ebp
 407:	57                   	push   %edi
 408:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 40b:	8b 4d 10             	mov    0x10(%ebp),%ecx
 40e:	8b 45 0c             	mov    0xc(%ebp),%eax
 411:	89 d7                	mov    %edx,%edi
 413:	fc                   	cld    
 414:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 416:	8b 7d fc             	mov    -0x4(%ebp),%edi
 419:	89 d0                	mov    %edx,%eax
 41b:	c9                   	leave  
 41c:	c3                   	ret    
 41d:	8d 76 00             	lea    0x0(%esi),%esi

00000420 <strchr>:

char*
strchr(const char *s, char c)
{
 420:	f3 0f 1e fb          	endbr32 
 424:	55                   	push   %ebp
 425:	89 e5                	mov    %esp,%ebp
 427:	8b 45 08             	mov    0x8(%ebp),%eax
 42a:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 42e:	0f b6 10             	movzbl (%eax),%edx
 431:	84 d2                	test   %dl,%dl
 433:	75 16                	jne    44b <strchr+0x2b>
 435:	eb 21                	jmp    458 <strchr+0x38>
 437:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 43e:	66 90                	xchg   %ax,%ax
 440:	0f b6 50 01          	movzbl 0x1(%eax),%edx
 444:	83 c0 01             	add    $0x1,%eax
 447:	84 d2                	test   %dl,%dl
 449:	74 0d                	je     458 <strchr+0x38>
    if(*s == c)
 44b:	38 d1                	cmp    %dl,%cl
 44d:	75 f1                	jne    440 <strchr+0x20>
      return (char*)s;
  return 0;
}
 44f:	5d                   	pop    %ebp
 450:	c3                   	ret    
 451:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return 0;
 458:	31 c0                	xor    %eax,%eax
}
 45a:	5d                   	pop    %ebp
 45b:	c3                   	ret    
 45c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000460 <gets>:

char*
gets(char *buf, int max)
{
 460:	f3 0f 1e fb          	endbr32 
 464:	55                   	push   %ebp
 465:	89 e5                	mov    %esp,%ebp
 467:	57                   	push   %edi
 468:	56                   	push   %esi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    cc = read(0, &c, 1);
 469:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 46c:	53                   	push   %ebx
  for(i=0; i+1 < max; ){
 46d:	31 db                	xor    %ebx,%ebx
{
 46f:	83 ec 1c             	sub    $0x1c,%esp
  for(i=0; i+1 < max; ){
 472:	eb 2b                	jmp    49f <gets+0x3f>
 474:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cc = read(0, &c, 1);
 478:	83 ec 04             	sub    $0x4,%esp
 47b:	6a 01                	push   $0x1
 47d:	57                   	push   %edi
 47e:	6a 00                	push   $0x0
 480:	e8 36 01 00 00       	call   5bb <read>
    if(cc < 1)
 485:	83 c4 10             	add    $0x10,%esp
 488:	85 c0                	test   %eax,%eax
 48a:	7e 1d                	jle    4a9 <gets+0x49>
      break;
    buf[i++] = c;
 48c:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 490:	8b 55 08             	mov    0x8(%ebp),%edx
 493:	88 44 1a ff          	mov    %al,-0x1(%edx,%ebx,1)
    if(c == '\n' || c == '\r')
 497:	3c 0a                	cmp    $0xa,%al
 499:	74 25                	je     4c0 <gets+0x60>
 49b:	3c 0d                	cmp    $0xd,%al
 49d:	74 21                	je     4c0 <gets+0x60>
  for(i=0; i+1 < max; ){
 49f:	89 de                	mov    %ebx,%esi
 4a1:	83 c3 01             	add    $0x1,%ebx
 4a4:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
 4a7:	7c cf                	jl     478 <gets+0x18>
      break;
  }
  buf[i] = '\0';
 4a9:	8b 45 08             	mov    0x8(%ebp),%eax
 4ac:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
  return buf;
}
 4b0:	8d 65 f4             	lea    -0xc(%ebp),%esp
 4b3:	5b                   	pop    %ebx
 4b4:	5e                   	pop    %esi
 4b5:	5f                   	pop    %edi
 4b6:	5d                   	pop    %ebp
 4b7:	c3                   	ret    
 4b8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4bf:	90                   	nop
  buf[i] = '\0';
 4c0:	8b 45 08             	mov    0x8(%ebp),%eax
 4c3:	89 de                	mov    %ebx,%esi
 4c5:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
}
 4c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
 4cc:	5b                   	pop    %ebx
 4cd:	5e                   	pop    %esi
 4ce:	5f                   	pop    %edi
 4cf:	5d                   	pop    %ebp
 4d0:	c3                   	ret    
 4d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4d8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4df:	90                   	nop

000004e0 <stat>:

int
stat(const char *n, struct stat *st)
{
 4e0:	f3 0f 1e fb          	endbr32 
 4e4:	55                   	push   %ebp
 4e5:	89 e5                	mov    %esp,%ebp
 4e7:	56                   	push   %esi
 4e8:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 4e9:	83 ec 08             	sub    $0x8,%esp
 4ec:	6a 00                	push   $0x0
 4ee:	ff 75 08             	push   0x8(%ebp)
 4f1:	e8 ed 00 00 00       	call   5e3 <open>
  if(fd < 0)
 4f6:	83 c4 10             	add    $0x10,%esp
 4f9:	85 c0                	test   %eax,%eax
 4fb:	78 2b                	js     528 <stat+0x48>
    return -1;
  r = fstat(fd, st);
 4fd:	83 ec 08             	sub    $0x8,%esp
 500:	ff 75 0c             	push   0xc(%ebp)
 503:	89 c3                	mov    %eax,%ebx
 505:	50                   	push   %eax
 506:	e8 f0 00 00 00       	call   5fb <fstat>
  close(fd);
 50b:	89 1c 24             	mov    %ebx,(%esp)
  r = fstat(fd, st);
 50e:	89 c6                	mov    %eax,%esi
  close(fd);
 510:	e8 b6 00 00 00       	call   5cb <close>
  return r;
 515:	83 c4 10             	add    $0x10,%esp
}
 518:	8d 65 f8             	lea    -0x8(%ebp),%esp
 51b:	89 f0                	mov    %esi,%eax
 51d:	5b                   	pop    %ebx
 51e:	5e                   	pop    %esi
 51f:	5d                   	pop    %ebp
 520:	c3                   	ret    
 521:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
 528:	be ff ff ff ff       	mov    $0xffffffff,%esi
 52d:	eb e9                	jmp    518 <stat+0x38>
 52f:	90                   	nop

00000530 <atoi>:

int
atoi(const char *s)
{
 530:	f3 0f 1e fb          	endbr32 
 534:	55                   	push   %ebp
 535:	89 e5                	mov    %esp,%ebp
 537:	53                   	push   %ebx
 538:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 53b:	0f be 02             	movsbl (%edx),%eax
 53e:	8d 48 d0             	lea    -0x30(%eax),%ecx
 541:	80 f9 09             	cmp    $0x9,%cl
  n = 0;
 544:	b9 00 00 00 00       	mov    $0x0,%ecx
  while('0' <= *s && *s <= '9')
 549:	77 1a                	ja     565 <atoi+0x35>
 54b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 54f:	90                   	nop
    n = n*10 + *s++ - '0';
 550:	83 c2 01             	add    $0x1,%edx
 553:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
 556:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
  while('0' <= *s && *s <= '9')
 55a:	0f be 02             	movsbl (%edx),%eax
 55d:	8d 58 d0             	lea    -0x30(%eax),%ebx
 560:	80 fb 09             	cmp    $0x9,%bl
 563:	76 eb                	jbe    550 <atoi+0x20>
  return n;
}
 565:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 568:	89 c8                	mov    %ecx,%eax
 56a:	c9                   	leave  
 56b:	c3                   	ret    
 56c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000570 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 570:	f3 0f 1e fb          	endbr32 
 574:	55                   	push   %ebp
 575:	89 e5                	mov    %esp,%ebp
 577:	57                   	push   %edi
 578:	8b 45 10             	mov    0x10(%ebp),%eax
 57b:	8b 55 08             	mov    0x8(%ebp),%edx
 57e:	56                   	push   %esi
 57f:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst;
  const char *src;

  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 582:	85 c0                	test   %eax,%eax
 584:	7e 0f                	jle    595 <memmove+0x25>
 586:	01 d0                	add    %edx,%eax
  dst = vdst;
 588:	89 d7                	mov    %edx,%edi
 58a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *dst++ = *src++;
 590:	a4                   	movsb  %ds:(%esi),%es:(%edi)
  while(n-- > 0)
 591:	39 f8                	cmp    %edi,%eax
 593:	75 fb                	jne    590 <memmove+0x20>
  return vdst;
}
 595:	5e                   	pop    %esi
 596:	89 d0                	mov    %edx,%eax
 598:	5f                   	pop    %edi
 599:	5d                   	pop    %ebp
 59a:	c3                   	ret    

0000059b <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 59b:	b8 01 00 00 00       	mov    $0x1,%eax
 5a0:	cd 40                	int    $0x40
 5a2:	c3                   	ret    

000005a3 <exit>:
SYSCALL(exit)
 5a3:	b8 02 00 00 00       	mov    $0x2,%eax
 5a8:	cd 40                	int    $0x40
 5aa:	c3                   	ret    

000005ab <wait>:
SYSCALL(wait)
 5ab:	b8 03 00 00 00       	mov    $0x3,%eax
 5b0:	cd 40                	int    $0x40
 5b2:	c3                   	ret    

000005b3 <pipe>:
SYSCALL(pipe)
 5b3:	b8 04 00 00 00       	mov    $0x4,%eax
 5b8:	cd 40                	int    $0x40
 5ba:	c3                   	ret    

000005bb <read>:
SYSCALL(read)
 5bb:	b8 05 00 00 00       	mov    $0x5,%eax
 5c0:	cd 40                	int    $0x40
 5c2:	c3                   	ret    

000005c3 <write>:
SYSCALL(write)
 5c3:	b8 10 00 00 00       	mov    $0x10,%eax
 5c8:	cd 40                	int    $0x40
 5ca:	c3                   	ret    

000005cb <close>:
SYSCALL(close)
 5cb:	b8 15 00 00 00       	mov    $0x15,%eax
 5d0:	cd 40                	int    $0x40
 5d2:	c3                   	ret    

000005d3 <kill>:
SYSCALL(kill)
 5d3:	b8 06 00 00 00       	mov    $0x6,%eax
 5d8:	cd 40                	int    $0x40
 5da:	c3                   	ret    

000005db <exec>:
SYSCALL(exec)
 5db:	b8 07 00 00 00       	mov    $0x7,%eax
 5e0:	cd 40                	int    $0x40
 5e2:	c3                   	ret    

000005e3 <open>:
SYSCALL(open)
 5e3:	b8 0f 00 00 00       	mov    $0xf,%eax
 5e8:	cd 40                	int    $0x40
 5ea:	c3                   	ret    

000005eb <mknod>:
SYSCALL(mknod)
 5eb:	b8 11 00 00 00       	mov    $0x11,%eax
 5f0:	cd 40                	int    $0x40
 5f2:	c3                   	ret    

000005f3 <unlink>:
SYSCALL(unlink)
 5f3:	b8 12 00 00 00       	mov    $0x12,%eax
 5f8:	cd 40                	int    $0x40
 5fa:	c3                   	ret    

000005fb <fstat>:
SYSCALL(fstat)
 5fb:	b8 08 00 00 00       	mov    $0x8,%eax
 600:	cd 40                	int    $0x40
 602:	c3                   	ret    

00000603 <link>:
SYSCALL(link)
 603:	b8 13 00 00 00       	mov    $0x13,%eax
 608:	cd 40                	int    $0x40
 60a:	c3                   	ret    

0000060b <mkdir>:
SYSCALL(mkdir)
 60b:	b8 14 00 00 00       	mov    $0x14,%eax
 610:	cd 40                	int    $0x40
 612:	c3                   	ret    

00000613 <chdir>:
SYSCALL(chdir)
 613:	b8 09 00 00 00       	mov    $0x9,%eax
 618:	cd 40                	int    $0x40
 61a:	c3                   	ret    

0000061b <dup>:
SYSCALL(dup)
 61b:	b8 0a 00 00 00       	mov    $0xa,%eax
 620:	cd 40                	int    $0x40
 622:	c3                   	ret    

00000623 <getpid>:
SYSCALL(getpid)
 623:	b8 0b 00 00 00       	mov    $0xb,%eax
 628:	cd 40                	int    $0x40
 62a:	c3                   	ret    

0000062b <sbrk>:
SYSCALL(sbrk)
 62b:	b8 0c 00 00 00       	mov    $0xc,%eax
 630:	cd 40                	int    $0x40
 632:	c3                   	ret    

00000633 <sleep>:
SYSCALL(sleep)
 633:	b8 0d 00 00 00       	mov    $0xd,%eax
 638:	cd 40                	int    $0x40
 63a:	c3                   	ret    

0000063b <uptime>:
SYSCALL(uptime)
 63b:	b8 0e 00 00 00       	mov    $0xe,%eax
 640:	cd 40                	int    $0x40
 642:	c3                   	ret    

00000643 <poweroff>:
SYSCALL(poweroff)
 643:	b8 16 00 00 00       	mov    $0x16,%eax
 648:	cd 40                	int    $0x40
 64a:	c3                   	ret    

0000064b <calculate_sum_of_digits>:
SYSCALL(calculate_sum_of_digits)
 64b:	b8 17 00 00 00       	mov    $0x17,%eax
 650:	cd 40                	int    $0x40
 652:	c3                   	ret    

00000653 <get_parent_pid>:
SYSCALL(get_parent_pid)
 653:	b8 18 00 00 00       	mov    $0x18,%eax
 658:	cd 40                	int    $0x40
 65a:	c3                   	ret    

0000065b <set_process_parent>:
SYSCALL(set_process_parent)
 65b:	b8 19 00 00 00       	mov    $0x19,%eax
 660:	cd 40                	int    $0x40
 662:	c3                   	ret    
 663:	66 90                	xchg   %ax,%ax
 665:	66 90                	xchg   %ax,%ax
 667:	66 90                	xchg   %ax,%ax
 669:	66 90                	xchg   %ax,%ax
 66b:	66 90                	xchg   %ax,%ax
 66d:	66 90                	xchg   %ax,%ax
 66f:	90                   	nop

00000670 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
 670:	55                   	push   %ebp
 671:	89 e5                	mov    %esp,%ebp
 673:	57                   	push   %edi
 674:	56                   	push   %esi
 675:	53                   	push   %ebx
 676:	83 ec 3c             	sub    $0x3c,%esp
 679:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
 67c:	89 d1                	mov    %edx,%ecx
{
 67e:	89 45 b8             	mov    %eax,-0x48(%ebp)
  if(sgn && xx < 0){
 681:	85 d2                	test   %edx,%edx
 683:	0f 89 7f 00 00 00    	jns    708 <printint+0x98>
 689:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
 68d:	74 79                	je     708 <printint+0x98>
    neg = 1;
 68f:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
    x = -xx;
 696:	f7 d9                	neg    %ecx
  } else {
    x = xx;
  }

  i = 0;
 698:	31 db                	xor    %ebx,%ebx
 69a:	8d 75 d7             	lea    -0x29(%ebp),%esi
 69d:	8d 76 00             	lea    0x0(%esi),%esi
  do{
    buf[i++] = digits[x % base];
 6a0:	89 c8                	mov    %ecx,%eax
 6a2:	31 d2                	xor    %edx,%edx
 6a4:	89 cf                	mov    %ecx,%edi
 6a6:	f7 75 c4             	divl   -0x3c(%ebp)
 6a9:	0f b6 92 bc 0b 00 00 	movzbl 0xbbc(%edx),%edx
 6b0:	89 45 c0             	mov    %eax,-0x40(%ebp)
 6b3:	89 d8                	mov    %ebx,%eax
 6b5:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
 6b8:	8b 4d c0             	mov    -0x40(%ebp),%ecx
    buf[i++] = digits[x % base];
 6bb:	88 14 1e             	mov    %dl,(%esi,%ebx,1)
  }while((x /= base) != 0);
 6be:	39 7d c4             	cmp    %edi,-0x3c(%ebp)
 6c1:	76 dd                	jbe    6a0 <printint+0x30>
  if(neg)
 6c3:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 6c6:	85 c9                	test   %ecx,%ecx
 6c8:	74 0c                	je     6d6 <printint+0x66>
    buf[i++] = '-';
 6ca:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
 6cf:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
 6d1:	ba 2d 00 00 00       	mov    $0x2d,%edx

  while(--i >= 0)
 6d6:	8b 7d b8             	mov    -0x48(%ebp),%edi
 6d9:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
 6dd:	eb 07                	jmp    6e6 <printint+0x76>
 6df:	90                   	nop
    putc(fd, buf[i]);
 6e0:	0f b6 13             	movzbl (%ebx),%edx
 6e3:	83 eb 01             	sub    $0x1,%ebx
  write(fd, &c, 1);
 6e6:	83 ec 04             	sub    $0x4,%esp
 6e9:	88 55 d7             	mov    %dl,-0x29(%ebp)
 6ec:	6a 01                	push   $0x1
 6ee:	56                   	push   %esi
 6ef:	57                   	push   %edi
 6f0:	e8 ce fe ff ff       	call   5c3 <write>
  while(--i >= 0)
 6f5:	83 c4 10             	add    $0x10,%esp
 6f8:	39 de                	cmp    %ebx,%esi
 6fa:	75 e4                	jne    6e0 <printint+0x70>
}
 6fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
 6ff:	5b                   	pop    %ebx
 700:	5e                   	pop    %esi
 701:	5f                   	pop    %edi
 702:	5d                   	pop    %ebp
 703:	c3                   	ret    
 704:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  neg = 0;
 708:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%ebp)
 70f:	eb 87                	jmp    698 <printint+0x28>
 711:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 718:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 71f:	90                   	nop

00000720 <printfloat>:

// MOD-2 : Added printf for floats
void
printfloat(int fd, float xx)
{
 720:	f3 0f 1e fb          	endbr32 
 724:	55                   	push   %ebp

  int beg=(int)(xx);
	int fin=(int)(xx*100)-beg*100;
  printint(fd, beg, 10, 1);
 725:	b9 0a 00 00 00       	mov    $0xa,%ecx
{
 72a:	89 e5                	mov    %esp,%ebp
 72c:	57                   	push   %edi
 72d:	56                   	push   %esi
  write(fd, &c, 1);
 72e:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 731:	53                   	push   %ebx
 732:	83 ec 38             	sub    $0x38,%esp
 735:	d9 45 0c             	flds   0xc(%ebp)
 738:	8b 75 08             	mov    0x8(%ebp),%esi
  int beg=(int)(xx);
 73b:	d9 7d d6             	fnstcw -0x2a(%ebp)
  printint(fd, beg, 10, 1);
 73e:	6a 01                	push   $0x1
  int beg=(int)(xx);
 740:	0f b7 45 d6          	movzwl -0x2a(%ebp),%eax
 744:	80 cc 0c             	or     $0xc,%ah
 747:	66 89 45 d4          	mov    %ax,-0x2c(%ebp)
  printint(fd, beg, 10, 1);
 74b:	89 f0                	mov    %esi,%eax
  int beg=(int)(xx);
 74d:	d9 6d d4             	fldcw  -0x2c(%ebp)
 750:	db 55 d0             	fistl  -0x30(%ebp)
 753:	d9 6d d6             	fldcw  -0x2a(%ebp)
 756:	8b 55 d0             	mov    -0x30(%ebp),%edx
	int fin=(int)(xx*100)-beg*100;
 759:	d8 0d d0 0b 00 00    	fmuls  0xbd0
 75f:	d9 6d d4             	fldcw  -0x2c(%ebp)
 762:	db 5d d0             	fistpl -0x30(%ebp)
 765:	d9 6d d6             	fldcw  -0x2a(%ebp)
 768:	6b da 9c             	imul   $0xffffff9c,%edx,%ebx
 76b:	03 5d d0             	add    -0x30(%ebp),%ebx
  printint(fd, beg, 10, 1);
 76e:	e8 fd fe ff ff       	call   670 <printint>
  write(fd, &c, 1);
 773:	83 c4 0c             	add    $0xc,%esp
 776:	c6 45 e7 2e          	movb   $0x2e,-0x19(%ebp)
 77a:	6a 01                	push   $0x1
 77c:	57                   	push   %edi
 77d:	56                   	push   %esi
 77e:	e8 40 fe ff ff       	call   5c3 <write>
  putc(fd, '.');
	if(fin<10)
 783:	83 c4 10             	add    $0x10,%esp
 786:	83 fb 09             	cmp    $0x9,%ebx
 789:	7e 25                	jle    7b0 <printfloat+0x90>
    putc(fd, '0');
	printint(fd, fin, 10, 1);
 78b:	c7 45 08 01 00 00 00 	movl   $0x1,0x8(%ebp)
}
 792:	8d 65 f4             	lea    -0xc(%ebp),%esp
	printint(fd, fin, 10, 1);
 795:	89 da                	mov    %ebx,%edx
 797:	89 f0                	mov    %esi,%eax
}
 799:	5b                   	pop    %ebx
	printint(fd, fin, 10, 1);
 79a:	b9 0a 00 00 00       	mov    $0xa,%ecx
}
 79f:	5e                   	pop    %esi
 7a0:	5f                   	pop    %edi
 7a1:	5d                   	pop    %ebp
	printint(fd, fin, 10, 1);
 7a2:	e9 c9 fe ff ff       	jmp    670 <printint>
 7a7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 7ae:	66 90                	xchg   %ax,%ax
  write(fd, &c, 1);
 7b0:	83 ec 04             	sub    $0x4,%esp
 7b3:	c6 45 e7 30          	movb   $0x30,-0x19(%ebp)
 7b7:	6a 01                	push   $0x1
 7b9:	57                   	push   %edi
 7ba:	56                   	push   %esi
 7bb:	e8 03 fe ff ff       	call   5c3 <write>
    putc(fd, '0');
 7c0:	83 c4 10             	add    $0x10,%esp
 7c3:	eb c6                	jmp    78b <printfloat+0x6b>
 7c5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 7cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000007d0 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s, %f.
void
printf(int fd, const char *fmt, ...)
{
 7d0:	f3 0f 1e fb          	endbr32 
 7d4:	55                   	push   %ebp
 7d5:	89 e5                	mov    %esp,%ebp
 7d7:	57                   	push   %edi
 7d8:	56                   	push   %esi
 7d9:	53                   	push   %ebx
 7da:	83 ec 2c             	sub    $0x2c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 7dd:	8b 75 0c             	mov    0xc(%ebp),%esi
 7e0:	0f b6 1e             	movzbl (%esi),%ebx
 7e3:	84 db                	test   %bl,%bl
 7e5:	0f 84 bd 00 00 00    	je     8a8 <printf+0xd8>
  ap = (uint*)(void*)&fmt + 1;
 7eb:	8d 45 10             	lea    0x10(%ebp),%eax
 7ee:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 7f1:	8d 7d e7             	lea    -0x19(%ebp),%edi
  state = 0;
 7f4:	31 d2                	xor    %edx,%edx
  ap = (uint*)(void*)&fmt + 1;
 7f6:	89 45 cc             	mov    %eax,-0x34(%ebp)
 7f9:	eb 33                	jmp    82e <printf+0x5e>
 7fb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 7ff:	90                   	nop
 800:	89 55 d0             	mov    %edx,-0x30(%ebp)
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
 803:	ba 25 00 00 00       	mov    $0x25,%edx
      if(c == '%'){
 808:	83 f8 25             	cmp    $0x25,%eax
 80b:	74 17                	je     824 <printf+0x54>
  write(fd, &c, 1);
 80d:	83 ec 04             	sub    $0x4,%esp
 810:	88 5d e7             	mov    %bl,-0x19(%ebp)
 813:	6a 01                	push   $0x1
 815:	57                   	push   %edi
 816:	ff 75 08             	push   0x8(%ebp)
 819:	e8 a5 fd ff ff       	call   5c3 <write>
 81e:	8b 55 d0             	mov    -0x30(%ebp),%edx
      } else {
        putc(fd, c);
 821:	83 c4 10             	add    $0x10,%esp
  for(i = 0; fmt[i]; i++){
 824:	0f b6 1e             	movzbl (%esi),%ebx
 827:	83 c6 01             	add    $0x1,%esi
 82a:	84 db                	test   %bl,%bl
 82c:	74 7a                	je     8a8 <printf+0xd8>
    c = fmt[i] & 0xff;
 82e:	0f be cb             	movsbl %bl,%ecx
 831:	0f b6 c3             	movzbl %bl,%eax
    if(state == 0){
 834:	85 d2                	test   %edx,%edx
 836:	74 c8                	je     800 <printf+0x30>
      }
    } else if(state == '%'){
 838:	83 fa 25             	cmp    $0x25,%edx
 83b:	75 e7                	jne    824 <printf+0x54>
      if(c == 'd'){
 83d:	83 f8 64             	cmp    $0x64,%eax
 840:	0f 84 9a 00 00 00    	je     8e0 <printf+0x110>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 846:	81 e1 f7 00 00 00    	and    $0xf7,%ecx
 84c:	83 f9 70             	cmp    $0x70,%ecx
 84f:	74 5f                	je     8b0 <printf+0xe0>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 851:	83 f8 73             	cmp    $0x73,%eax
 854:	0f 84 d6 00 00 00    	je     930 <printf+0x160>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 85a:	83 f8 63             	cmp    $0x63,%eax
 85d:	0f 84 8d 00 00 00    	je     8f0 <printf+0x120>
        putc(fd, *ap);
        ap++;
      } else if(c == 'f'){ // MOD-2
 863:	83 f8 66             	cmp    $0x66,%eax
 866:	0f 84 f4 00 00 00    	je     960 <printf+0x190>
        printfloat(fd, (float)*ap);
        ap++;
      } else if(c == '%'){
 86c:	83 f8 25             	cmp    $0x25,%eax
 86f:	0f 84 ab 00 00 00    	je     920 <printf+0x150>
  write(fd, &c, 1);
 875:	83 ec 04             	sub    $0x4,%esp
 878:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
 87c:	6a 01                	push   $0x1
 87e:	57                   	push   %edi
 87f:	ff 75 08             	push   0x8(%ebp)
 882:	e8 3c fd ff ff       	call   5c3 <write>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
        putc(fd, c);
 887:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 88a:	83 c4 0c             	add    $0xc,%esp
 88d:	6a 01                	push   $0x1
  for(i = 0; fmt[i]; i++){
 88f:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 892:	57                   	push   %edi
 893:	ff 75 08             	push   0x8(%ebp)
 896:	e8 28 fd ff ff       	call   5c3 <write>
  for(i = 0; fmt[i]; i++){
 89b:	0f b6 5e ff          	movzbl -0x1(%esi),%ebx
        putc(fd, c);
 89f:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 8a2:	31 d2                	xor    %edx,%edx
  for(i = 0; fmt[i]; i++){
 8a4:	84 db                	test   %bl,%bl
 8a6:	75 86                	jne    82e <printf+0x5e>
    }
  }
 8a8:	8d 65 f4             	lea    -0xc(%ebp),%esp
 8ab:	5b                   	pop    %ebx
 8ac:	5e                   	pop    %esi
 8ad:	5f                   	pop    %edi
 8ae:	5d                   	pop    %ebp
 8af:	c3                   	ret    
        printint(fd, *ap, 16, 0);
 8b0:	83 ec 0c             	sub    $0xc,%esp
 8b3:	b9 10 00 00 00       	mov    $0x10,%ecx
 8b8:	6a 00                	push   $0x0
 8ba:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 8bd:	8b 45 08             	mov    0x8(%ebp),%eax
 8c0:	8b 13                	mov    (%ebx),%edx
 8c2:	e8 a9 fd ff ff       	call   670 <printint>
        ap++;
 8c7:	89 d8                	mov    %ebx,%eax
 8c9:	83 c4 10             	add    $0x10,%esp
      state = 0;
 8cc:	31 d2                	xor    %edx,%edx
        ap++;
 8ce:	83 c0 04             	add    $0x4,%eax
 8d1:	89 45 cc             	mov    %eax,-0x34(%ebp)
 8d4:	e9 4b ff ff ff       	jmp    824 <printf+0x54>
 8d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        printint(fd, *ap, 10, 1);
 8e0:	83 ec 0c             	sub    $0xc,%esp
 8e3:	b9 0a 00 00 00       	mov    $0xa,%ecx
 8e8:	6a 01                	push   $0x1
 8ea:	eb ce                	jmp    8ba <printf+0xea>
 8ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        putc(fd, *ap);
 8f0:	8b 5d cc             	mov    -0x34(%ebp),%ebx
  write(fd, &c, 1);
 8f3:	83 ec 04             	sub    $0x4,%esp
        putc(fd, *ap);
 8f6:	8b 03                	mov    (%ebx),%eax
  write(fd, &c, 1);
 8f8:	6a 01                	push   $0x1
        ap++;
 8fa:	83 c3 04             	add    $0x4,%ebx
  write(fd, &c, 1);
 8fd:	57                   	push   %edi
 8fe:	ff 75 08             	push   0x8(%ebp)
        putc(fd, *ap);
 901:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 904:	e8 ba fc ff ff       	call   5c3 <write>
        ap++;
 909:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 90c:	83 c4 10             	add    $0x10,%esp
      state = 0;
 90f:	31 d2                	xor    %edx,%edx
 911:	e9 0e ff ff ff       	jmp    824 <printf+0x54>
 916:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 91d:	8d 76 00             	lea    0x0(%esi),%esi
        putc(fd, c);
 920:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 923:	83 ec 04             	sub    $0x4,%esp
 926:	e9 62 ff ff ff       	jmp    88d <printf+0xbd>
 92b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 92f:	90                   	nop
        s = (char*)*ap;
 930:	8b 45 cc             	mov    -0x34(%ebp),%eax
 933:	8b 18                	mov    (%eax),%ebx
        ap++;
 935:	83 c0 04             	add    $0x4,%eax
 938:	89 45 cc             	mov    %eax,-0x34(%ebp)
        if(s == 0)
 93b:	85 db                	test   %ebx,%ebx
 93d:	74 4f                	je     98e <printf+0x1be>
        while(*s != 0){
 93f:	0f b6 03             	movzbl (%ebx),%eax
      state = 0;
 942:	31 d2                	xor    %edx,%edx
        while(*s != 0){
 944:	84 c0                	test   %al,%al
 946:	0f 84 d8 fe ff ff    	je     824 <printf+0x54>
 94c:	89 75 d0             	mov    %esi,-0x30(%ebp)
 94f:	89 de                	mov    %ebx,%esi
 951:	8b 5d 08             	mov    0x8(%ebp),%ebx
 954:	eb 4a                	jmp    9a0 <printf+0x1d0>
 956:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 95d:	8d 76 00             	lea    0x0(%esi),%esi
        printfloat(fd, (float)*ap);
 960:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 963:	31 d2                	xor    %edx,%edx
 965:	83 ec 0c             	sub    $0xc,%esp
 968:	89 55 d4             	mov    %edx,-0x2c(%ebp)
 96b:	8b 03                	mov    (%ebx),%eax
        ap++;
 96d:	83 c3 04             	add    $0x4,%ebx
        printfloat(fd, (float)*ap);
 970:	89 45 d0             	mov    %eax,-0x30(%ebp)
 973:	df 6d d0             	fildll -0x30(%ebp)
 976:	d9 1c 24             	fstps  (%esp)
 979:	ff 75 08             	push   0x8(%ebp)
 97c:	e8 9f fd ff ff       	call   720 <printfloat>
        ap++;
 981:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 984:	83 c4 10             	add    $0x10,%esp
      state = 0;
 987:	31 d2                	xor    %edx,%edx
 989:	e9 96 fe ff ff       	jmp    824 <printf+0x54>
          s = "(null)";
 98e:	bb b2 0b 00 00       	mov    $0xbb2,%ebx
        while(*s != 0){
 993:	89 75 d0             	mov    %esi,-0x30(%ebp)
 996:	b8 28 00 00 00       	mov    $0x28,%eax
 99b:	89 de                	mov    %ebx,%esi
 99d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  write(fd, &c, 1);
 9a0:	83 ec 04             	sub    $0x4,%esp
          s++;
 9a3:	83 c6 01             	add    $0x1,%esi
 9a6:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 9a9:	6a 01                	push   $0x1
 9ab:	57                   	push   %edi
 9ac:	53                   	push   %ebx
 9ad:	e8 11 fc ff ff       	call   5c3 <write>
        while(*s != 0){
 9b2:	0f b6 06             	movzbl (%esi),%eax
 9b5:	83 c4 10             	add    $0x10,%esp
 9b8:	84 c0                	test   %al,%al
 9ba:	75 e4                	jne    9a0 <printf+0x1d0>
      state = 0;
 9bc:	8b 75 d0             	mov    -0x30(%ebp),%esi
 9bf:	31 d2                	xor    %edx,%edx
 9c1:	e9 5e fe ff ff       	jmp    824 <printf+0x54>
 9c6:	66 90                	xchg   %ax,%ax
 9c8:	66 90                	xchg   %ax,%ax
 9ca:	66 90                	xchg   %ax,%ax
 9cc:	66 90                	xchg   %ax,%ax
 9ce:	66 90                	xchg   %ax,%ax

000009d0 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 9d0:	f3 0f 1e fb          	endbr32 
 9d4:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 9d5:	a1 2c 0f 00 00       	mov    0xf2c,%eax
{
 9da:	89 e5                	mov    %esp,%ebp
 9dc:	57                   	push   %edi
 9dd:	56                   	push   %esi
 9de:	53                   	push   %ebx
 9df:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = (Header*)ap - 1;
 9e2:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 9e5:	8d 76 00             	lea    0x0(%esi),%esi
 9e8:	89 c2                	mov    %eax,%edx
 9ea:	8b 00                	mov    (%eax),%eax
 9ec:	39 ca                	cmp    %ecx,%edx
 9ee:	73 30                	jae    a20 <free+0x50>
 9f0:	39 c1                	cmp    %eax,%ecx
 9f2:	72 04                	jb     9f8 <free+0x28>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 9f4:	39 c2                	cmp    %eax,%edx
 9f6:	72 f0                	jb     9e8 <free+0x18>
      break;
  if(bp + bp->s.size == p->s.ptr){
 9f8:	8b 73 fc             	mov    -0x4(%ebx),%esi
 9fb:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 9fe:	39 f8                	cmp    %edi,%eax
 a00:	74 30                	je     a32 <free+0x62>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 a02:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 a05:	8b 42 04             	mov    0x4(%edx),%eax
 a08:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 a0b:	39 f1                	cmp    %esi,%ecx
 a0d:	74 3a                	je     a49 <free+0x79>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 a0f:	89 0a                	mov    %ecx,(%edx)
  freep = p;
}
 a11:	5b                   	pop    %ebx
  freep = p;
 a12:	89 15 2c 0f 00 00    	mov    %edx,0xf2c
}
 a18:	5e                   	pop    %esi
 a19:	5f                   	pop    %edi
 a1a:	5d                   	pop    %ebp
 a1b:	c3                   	ret    
 a1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 a20:	39 c2                	cmp    %eax,%edx
 a22:	72 c4                	jb     9e8 <free+0x18>
 a24:	39 c1                	cmp    %eax,%ecx
 a26:	73 c0                	jae    9e8 <free+0x18>
  if(bp + bp->s.size == p->s.ptr){
 a28:	8b 73 fc             	mov    -0x4(%ebx),%esi
 a2b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 a2e:	39 f8                	cmp    %edi,%eax
 a30:	75 d0                	jne    a02 <free+0x32>
    bp->s.size += p->s.ptr->s.size;
 a32:	03 70 04             	add    0x4(%eax),%esi
 a35:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 a38:	8b 02                	mov    (%edx),%eax
 a3a:	8b 00                	mov    (%eax),%eax
 a3c:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 a3f:	8b 42 04             	mov    0x4(%edx),%eax
 a42:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 a45:	39 f1                	cmp    %esi,%ecx
 a47:	75 c6                	jne    a0f <free+0x3f>
    p->s.size += bp->s.size;
 a49:	03 43 fc             	add    -0x4(%ebx),%eax
  freep = p;
 a4c:	89 15 2c 0f 00 00    	mov    %edx,0xf2c
    p->s.size += bp->s.size;
 a52:	89 42 04             	mov    %eax,0x4(%edx)
    p->s.ptr = bp->s.ptr;
 a55:	8b 43 f8             	mov    -0x8(%ebx),%eax
 a58:	89 02                	mov    %eax,(%edx)
}
 a5a:	5b                   	pop    %ebx
 a5b:	5e                   	pop    %esi
 a5c:	5f                   	pop    %edi
 a5d:	5d                   	pop    %ebp
 a5e:	c3                   	ret    
 a5f:	90                   	nop

00000a60 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
 a60:	f3 0f 1e fb          	endbr32 
 a64:	55                   	push   %ebp
 a65:	89 e5                	mov    %esp,%ebp
 a67:	57                   	push   %edi
 a68:	56                   	push   %esi
 a69:	53                   	push   %ebx
 a6a:	83 ec 1c             	sub    $0x1c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 a6d:	8b 45 08             	mov    0x8(%ebp),%eax
  if((prevp = freep) == 0){
 a70:	8b 3d 2c 0f 00 00    	mov    0xf2c,%edi
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 a76:	8d 70 07             	lea    0x7(%eax),%esi
 a79:	c1 ee 03             	shr    $0x3,%esi
 a7c:	83 c6 01             	add    $0x1,%esi
  if((prevp = freep) == 0){
 a7f:	85 ff                	test   %edi,%edi
 a81:	0f 84 a9 00 00 00    	je     b30 <malloc+0xd0>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 a87:	8b 07                	mov    (%edi),%eax
    if(p->s.size >= nunits){
 a89:	8b 48 04             	mov    0x4(%eax),%ecx
 a8c:	39 f1                	cmp    %esi,%ecx
 a8e:	73 6d                	jae    afd <malloc+0x9d>
 a90:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
 a96:	bb 00 10 00 00       	mov    $0x1000,%ebx
 a9b:	0f 43 de             	cmovae %esi,%ebx
  p = sbrk(nu * sizeof(Header));
 a9e:	8d 0c dd 00 00 00 00 	lea    0x0(,%ebx,8),%ecx
 aa5:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
 aa8:	eb 17                	jmp    ac1 <malloc+0x61>
 aaa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 ab0:	8b 10                	mov    (%eax),%edx
    if(p->s.size >= nunits){
 ab2:	8b 4a 04             	mov    0x4(%edx),%ecx
 ab5:	39 f1                	cmp    %esi,%ecx
 ab7:	73 4f                	jae    b08 <malloc+0xa8>
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
 ab9:	8b 3d 2c 0f 00 00    	mov    0xf2c,%edi
 abf:	89 d0                	mov    %edx,%eax
 ac1:	39 c7                	cmp    %eax,%edi
 ac3:	75 eb                	jne    ab0 <malloc+0x50>
  p = sbrk(nu * sizeof(Header));
 ac5:	83 ec 0c             	sub    $0xc,%esp
 ac8:	ff 75 e4             	push   -0x1c(%ebp)
 acb:	e8 5b fb ff ff       	call   62b <sbrk>
  if(p == (char*)-1)
 ad0:	83 c4 10             	add    $0x10,%esp
 ad3:	83 f8 ff             	cmp    $0xffffffff,%eax
 ad6:	74 1b                	je     af3 <malloc+0x93>
  hp->s.size = nu;
 ad8:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 adb:	83 ec 0c             	sub    $0xc,%esp
 ade:	83 c0 08             	add    $0x8,%eax
 ae1:	50                   	push   %eax
 ae2:	e8 e9 fe ff ff       	call   9d0 <free>
  return freep;
 ae7:	a1 2c 0f 00 00       	mov    0xf2c,%eax
      if((p = morecore(nunits)) == 0)
 aec:	83 c4 10             	add    $0x10,%esp
 aef:	85 c0                	test   %eax,%eax
 af1:	75 bd                	jne    ab0 <malloc+0x50>
        return 0;
  }
}
 af3:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return 0;
 af6:	31 c0                	xor    %eax,%eax
}
 af8:	5b                   	pop    %ebx
 af9:	5e                   	pop    %esi
 afa:	5f                   	pop    %edi
 afb:	5d                   	pop    %ebp
 afc:	c3                   	ret    
    if(p->s.size >= nunits){
 afd:	89 c2                	mov    %eax,%edx
 aff:	89 f8                	mov    %edi,%eax
 b01:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->s.size == nunits)
 b08:	39 ce                	cmp    %ecx,%esi
 b0a:	74 54                	je     b60 <malloc+0x100>
        p->s.size -= nunits;
 b0c:	29 f1                	sub    %esi,%ecx
 b0e:	89 4a 04             	mov    %ecx,0x4(%edx)
        p += p->s.size;
 b11:	8d 14 ca             	lea    (%edx,%ecx,8),%edx
        p->s.size = nunits;
 b14:	89 72 04             	mov    %esi,0x4(%edx)
      freep = prevp;
 b17:	a3 2c 0f 00 00       	mov    %eax,0xf2c
}
 b1c:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return (void*)(p + 1);
 b1f:	8d 42 08             	lea    0x8(%edx),%eax
}
 b22:	5b                   	pop    %ebx
 b23:	5e                   	pop    %esi
 b24:	5f                   	pop    %edi
 b25:	5d                   	pop    %ebp
 b26:	c3                   	ret    
 b27:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 b2e:	66 90                	xchg   %ax,%ax
    base.s.ptr = freep = prevp = &base;
 b30:	c7 05 2c 0f 00 00 30 	movl   $0xf30,0xf2c
 b37:	0f 00 00 
    base.s.size = 0;
 b3a:	bf 30 0f 00 00       	mov    $0xf30,%edi
    base.s.ptr = freep = prevp = &base;
 b3f:	c7 05 30 0f 00 00 30 	movl   $0xf30,0xf30
 b46:	0f 00 00 
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 b49:	89 f8                	mov    %edi,%eax
    base.s.size = 0;
 b4b:	c7 05 34 0f 00 00 00 	movl   $0x0,0xf34
 b52:	00 00 00 
    if(p->s.size >= nunits){
 b55:	e9 36 ff ff ff       	jmp    a90 <malloc+0x30>
 b5a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        prevp->s.ptr = p->s.ptr;
 b60:	8b 0a                	mov    (%edx),%ecx
 b62:	89 08                	mov    %ecx,(%eax)
 b64:	eb b1                	jmp    b17 <malloc+0xb7>
