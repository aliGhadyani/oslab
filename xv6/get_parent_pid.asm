
_get_parent_pid:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[])
{
   0:	f3 0f 1e fb          	endbr32 
   4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   8:	83 e4 f0             	and    $0xfffffff0,%esp
   b:	ff 71 fc             	push   -0x4(%ecx)
   e:	55                   	push   %ebp
   f:	89 e5                	mov    %esp,%ebp
  11:	56                   	push   %esi
  12:	53                   	push   %ebx
  13:	51                   	push   %ecx
  14:	83 ec 0c             	sub    $0xc,%esp
    int saved_ebx;
    int pid0, pid1;
    pid0 = fork();
  17:	e8 cf 02 00 00       	call   2eb <fork>
    if (pid0)
  1c:	85 c0                	test   %eax,%eax
  1e:	75 45                	jne    65 <main+0x65>
        printf(1, "pid: %d, parent: %d\n", pid0, getpid());
        
    pid1 = fork();
  20:	e8 c6 02 00 00       	call   2eb <fork>
  25:	89 c3                	mov    %eax,%ebx
    if (pid1 && pid0 == 0){
  27:	85 c0                	test   %eax,%eax
  29:	75 14                	jne    3f <main+0x3f>
  2b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  2f:	90                   	nop
        : "r"(pid1)
        );
        get_parent_pid(pid1);
        asm("movl %0, %%ebx" : : "r"(saved_ebx)); // ebx = saved_ebx -> restore 
    }
    while(wait() != -1); // for preventing to be in zombie state
  30:	e8 c6 02 00 00       	call   2fb <wait>
  35:	83 f8 ff             	cmp    $0xffffffff,%eax
  38:	75 f6                	jne    30 <main+0x30>
    exit();
  3a:	e8 b4 02 00 00       	call   2f3 <exit>
        printf(1, "pid: %d, parent: %d\n", pid1, getpid());
  3f:	e8 2f 03 00 00       	call   373 <getpid>
  44:	50                   	push   %eax
  45:	53                   	push   %ebx
  46:	68 b8 08 00 00       	push   $0x8b8
  4b:	6a 01                	push   $0x1
  4d:	e8 ce 04 00 00       	call   520 <printf>
        asm volatile(
  52:	89 de                	mov    %ebx,%esi
  54:	89 db                	mov    %ebx,%ebx
        get_parent_pid(pid1);
  56:	89 1c 24             	mov    %ebx,(%esp)
  59:	e8 45 03 00 00       	call   3a3 <get_parent_pid>
        asm("movl %0, %%ebx" : : "r"(saved_ebx)); // ebx = saved_ebx -> restore 
  5e:	89 f3                	mov    %esi,%ebx
  60:	83 c4 10             	add    $0x10,%esp
  63:	eb cb                	jmp    30 <main+0x30>
  65:	89 c3                	mov    %eax,%ebx
        printf(1, "pid: %d, parent: %d\n", pid0, getpid());
  67:	e8 07 03 00 00       	call   373 <getpid>
  6c:	50                   	push   %eax
  6d:	53                   	push   %ebx
  6e:	68 b8 08 00 00       	push   $0x8b8
  73:	6a 01                	push   $0x1
  75:	e8 a6 04 00 00       	call   520 <printf>
    pid1 = fork();
  7a:	e8 6c 02 00 00       	call   2eb <fork>
  7f:	83 c4 10             	add    $0x10,%esp
  82:	eb ac                	jmp    30 <main+0x30>
  84:	66 90                	xchg   %ax,%ax
  86:	66 90                	xchg   %ax,%ax
  88:	66 90                	xchg   %ax,%ax
  8a:	66 90                	xchg   %ax,%ax
  8c:	66 90                	xchg   %ax,%ax
  8e:	66 90                	xchg   %ax,%ax

00000090 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
  90:	f3 0f 1e fb          	endbr32 
  94:	55                   	push   %ebp
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
  95:	31 c0                	xor    %eax,%eax
{
  97:	89 e5                	mov    %esp,%ebp
  99:	53                   	push   %ebx
  9a:	8b 4d 08             	mov    0x8(%ebp),%ecx
  9d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while((*s++ = *t++) != 0)
  a0:	0f b6 14 03          	movzbl (%ebx,%eax,1),%edx
  a4:	88 14 01             	mov    %dl,(%ecx,%eax,1)
  a7:	83 c0 01             	add    $0x1,%eax
  aa:	84 d2                	test   %dl,%dl
  ac:	75 f2                	jne    a0 <strcpy+0x10>
    ;
  return os;
}
  ae:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  b1:	89 c8                	mov    %ecx,%eax
  b3:	c9                   	leave  
  b4:	c3                   	ret    
  b5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000000c0 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  c0:	f3 0f 1e fb          	endbr32 
  c4:	55                   	push   %ebp
  c5:	89 e5                	mov    %esp,%ebp
  c7:	53                   	push   %ebx
  c8:	8b 4d 08             	mov    0x8(%ebp),%ecx
  cb:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
  ce:	0f b6 01             	movzbl (%ecx),%eax
  d1:	0f b6 1a             	movzbl (%edx),%ebx
  d4:	84 c0                	test   %al,%al
  d6:	75 18                	jne    f0 <strcmp+0x30>
  d8:	eb 2a                	jmp    104 <strcmp+0x44>
  da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  e0:	0f b6 41 01          	movzbl 0x1(%ecx),%eax
    p++, q++;
  e4:	83 c1 01             	add    $0x1,%ecx
  e7:	8d 5a 01             	lea    0x1(%edx),%ebx
  while(*p && *p == *q)
  ea:	84 c0                	test   %al,%al
  ec:	74 12                	je     100 <strcmp+0x40>
    p++, q++;
  ee:	89 da                	mov    %ebx,%edx
  while(*p && *p == *q)
  f0:	0f b6 1a             	movzbl (%edx),%ebx
  f3:	38 c3                	cmp    %al,%bl
  f5:	74 e9                	je     e0 <strcmp+0x20>
  return (uchar)*p - (uchar)*q;
  f7:	29 d8                	sub    %ebx,%eax
}
  f9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  fc:	c9                   	leave  
  fd:	c3                   	ret    
  fe:	66 90                	xchg   %ax,%ax
  return (uchar)*p - (uchar)*q;
 100:	0f b6 5a 01          	movzbl 0x1(%edx),%ebx
 104:	31 c0                	xor    %eax,%eax
 106:	29 d8                	sub    %ebx,%eax
}
 108:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 10b:	c9                   	leave  
 10c:	c3                   	ret    
 10d:	8d 76 00             	lea    0x0(%esi),%esi

00000110 <strlen>:

uint
strlen(const char *s)
{
 110:	f3 0f 1e fb          	endbr32 
 114:	55                   	push   %ebp
 115:	89 e5                	mov    %esp,%ebp
 117:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
 11a:	80 3a 00             	cmpb   $0x0,(%edx)
 11d:	74 21                	je     140 <strlen+0x30>
 11f:	31 c0                	xor    %eax,%eax
 121:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 128:	83 c0 01             	add    $0x1,%eax
 12b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
 12f:	89 c1                	mov    %eax,%ecx
 131:	75 f5                	jne    128 <strlen+0x18>
    ;
  return n;
}
 133:	89 c8                	mov    %ecx,%eax
 135:	5d                   	pop    %ebp
 136:	c3                   	ret    
 137:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 13e:	66 90                	xchg   %ax,%ax
  for(n = 0; s[n]; n++)
 140:	31 c9                	xor    %ecx,%ecx
}
 142:	5d                   	pop    %ebp
 143:	89 c8                	mov    %ecx,%eax
 145:	c3                   	ret    
 146:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 14d:	8d 76 00             	lea    0x0(%esi),%esi

00000150 <memset>:

void*
memset(void *dst, int c, uint n)
{
 150:	f3 0f 1e fb          	endbr32 
 154:	55                   	push   %ebp
 155:	89 e5                	mov    %esp,%ebp
 157:	57                   	push   %edi
 158:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 15b:	8b 4d 10             	mov    0x10(%ebp),%ecx
 15e:	8b 45 0c             	mov    0xc(%ebp),%eax
 161:	89 d7                	mov    %edx,%edi
 163:	fc                   	cld    
 164:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 166:	8b 7d fc             	mov    -0x4(%ebp),%edi
 169:	89 d0                	mov    %edx,%eax
 16b:	c9                   	leave  
 16c:	c3                   	ret    
 16d:	8d 76 00             	lea    0x0(%esi),%esi

00000170 <strchr>:

char*
strchr(const char *s, char c)
{
 170:	f3 0f 1e fb          	endbr32 
 174:	55                   	push   %ebp
 175:	89 e5                	mov    %esp,%ebp
 177:	8b 45 08             	mov    0x8(%ebp),%eax
 17a:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 17e:	0f b6 10             	movzbl (%eax),%edx
 181:	84 d2                	test   %dl,%dl
 183:	75 16                	jne    19b <strchr+0x2b>
 185:	eb 21                	jmp    1a8 <strchr+0x38>
 187:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 18e:	66 90                	xchg   %ax,%ax
 190:	0f b6 50 01          	movzbl 0x1(%eax),%edx
 194:	83 c0 01             	add    $0x1,%eax
 197:	84 d2                	test   %dl,%dl
 199:	74 0d                	je     1a8 <strchr+0x38>
    if(*s == c)
 19b:	38 d1                	cmp    %dl,%cl
 19d:	75 f1                	jne    190 <strchr+0x20>
      return (char*)s;
  return 0;
}
 19f:	5d                   	pop    %ebp
 1a0:	c3                   	ret    
 1a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return 0;
 1a8:	31 c0                	xor    %eax,%eax
}
 1aa:	5d                   	pop    %ebp
 1ab:	c3                   	ret    
 1ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000001b0 <gets>:

char*
gets(char *buf, int max)
{
 1b0:	f3 0f 1e fb          	endbr32 
 1b4:	55                   	push   %ebp
 1b5:	89 e5                	mov    %esp,%ebp
 1b7:	57                   	push   %edi
 1b8:	56                   	push   %esi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    cc = read(0, &c, 1);
 1b9:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 1bc:	53                   	push   %ebx
  for(i=0; i+1 < max; ){
 1bd:	31 db                	xor    %ebx,%ebx
{
 1bf:	83 ec 1c             	sub    $0x1c,%esp
  for(i=0; i+1 < max; ){
 1c2:	eb 2b                	jmp    1ef <gets+0x3f>
 1c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cc = read(0, &c, 1);
 1c8:	83 ec 04             	sub    $0x4,%esp
 1cb:	6a 01                	push   $0x1
 1cd:	57                   	push   %edi
 1ce:	6a 00                	push   $0x0
 1d0:	e8 36 01 00 00       	call   30b <read>
    if(cc < 1)
 1d5:	83 c4 10             	add    $0x10,%esp
 1d8:	85 c0                	test   %eax,%eax
 1da:	7e 1d                	jle    1f9 <gets+0x49>
      break;
    buf[i++] = c;
 1dc:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 1e0:	8b 55 08             	mov    0x8(%ebp),%edx
 1e3:	88 44 1a ff          	mov    %al,-0x1(%edx,%ebx,1)
    if(c == '\n' || c == '\r')
 1e7:	3c 0a                	cmp    $0xa,%al
 1e9:	74 25                	je     210 <gets+0x60>
 1eb:	3c 0d                	cmp    $0xd,%al
 1ed:	74 21                	je     210 <gets+0x60>
  for(i=0; i+1 < max; ){
 1ef:	89 de                	mov    %ebx,%esi
 1f1:	83 c3 01             	add    $0x1,%ebx
 1f4:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
 1f7:	7c cf                	jl     1c8 <gets+0x18>
      break;
  }
  buf[i] = '\0';
 1f9:	8b 45 08             	mov    0x8(%ebp),%eax
 1fc:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
  return buf;
}
 200:	8d 65 f4             	lea    -0xc(%ebp),%esp
 203:	5b                   	pop    %ebx
 204:	5e                   	pop    %esi
 205:	5f                   	pop    %edi
 206:	5d                   	pop    %ebp
 207:	c3                   	ret    
 208:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 20f:	90                   	nop
  buf[i] = '\0';
 210:	8b 45 08             	mov    0x8(%ebp),%eax
 213:	89 de                	mov    %ebx,%esi
 215:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
}
 219:	8d 65 f4             	lea    -0xc(%ebp),%esp
 21c:	5b                   	pop    %ebx
 21d:	5e                   	pop    %esi
 21e:	5f                   	pop    %edi
 21f:	5d                   	pop    %ebp
 220:	c3                   	ret    
 221:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 228:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 22f:	90                   	nop

00000230 <stat>:

int
stat(const char *n, struct stat *st)
{
 230:	f3 0f 1e fb          	endbr32 
 234:	55                   	push   %ebp
 235:	89 e5                	mov    %esp,%ebp
 237:	56                   	push   %esi
 238:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 239:	83 ec 08             	sub    $0x8,%esp
 23c:	6a 00                	push   $0x0
 23e:	ff 75 08             	push   0x8(%ebp)
 241:	e8 ed 00 00 00       	call   333 <open>
  if(fd < 0)
 246:	83 c4 10             	add    $0x10,%esp
 249:	85 c0                	test   %eax,%eax
 24b:	78 2b                	js     278 <stat+0x48>
    return -1;
  r = fstat(fd, st);
 24d:	83 ec 08             	sub    $0x8,%esp
 250:	ff 75 0c             	push   0xc(%ebp)
 253:	89 c3                	mov    %eax,%ebx
 255:	50                   	push   %eax
 256:	e8 f0 00 00 00       	call   34b <fstat>
  close(fd);
 25b:	89 1c 24             	mov    %ebx,(%esp)
  r = fstat(fd, st);
 25e:	89 c6                	mov    %eax,%esi
  close(fd);
 260:	e8 b6 00 00 00       	call   31b <close>
  return r;
 265:	83 c4 10             	add    $0x10,%esp
}
 268:	8d 65 f8             	lea    -0x8(%ebp),%esp
 26b:	89 f0                	mov    %esi,%eax
 26d:	5b                   	pop    %ebx
 26e:	5e                   	pop    %esi
 26f:	5d                   	pop    %ebp
 270:	c3                   	ret    
 271:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
 278:	be ff ff ff ff       	mov    $0xffffffff,%esi
 27d:	eb e9                	jmp    268 <stat+0x38>
 27f:	90                   	nop

00000280 <atoi>:

int
atoi(const char *s)
{
 280:	f3 0f 1e fb          	endbr32 
 284:	55                   	push   %ebp
 285:	89 e5                	mov    %esp,%ebp
 287:	53                   	push   %ebx
 288:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 28b:	0f be 02             	movsbl (%edx),%eax
 28e:	8d 48 d0             	lea    -0x30(%eax),%ecx
 291:	80 f9 09             	cmp    $0x9,%cl
  n = 0;
 294:	b9 00 00 00 00       	mov    $0x0,%ecx
  while('0' <= *s && *s <= '9')
 299:	77 1a                	ja     2b5 <atoi+0x35>
 29b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 29f:	90                   	nop
    n = n*10 + *s++ - '0';
 2a0:	83 c2 01             	add    $0x1,%edx
 2a3:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
 2a6:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
  while('0' <= *s && *s <= '9')
 2aa:	0f be 02             	movsbl (%edx),%eax
 2ad:	8d 58 d0             	lea    -0x30(%eax),%ebx
 2b0:	80 fb 09             	cmp    $0x9,%bl
 2b3:	76 eb                	jbe    2a0 <atoi+0x20>
  return n;
}
 2b5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 2b8:	89 c8                	mov    %ecx,%eax
 2ba:	c9                   	leave  
 2bb:	c3                   	ret    
 2bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000002c0 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 2c0:	f3 0f 1e fb          	endbr32 
 2c4:	55                   	push   %ebp
 2c5:	89 e5                	mov    %esp,%ebp
 2c7:	57                   	push   %edi
 2c8:	8b 45 10             	mov    0x10(%ebp),%eax
 2cb:	8b 55 08             	mov    0x8(%ebp),%edx
 2ce:	56                   	push   %esi
 2cf:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst;
  const char *src;

  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2d2:	85 c0                	test   %eax,%eax
 2d4:	7e 0f                	jle    2e5 <memmove+0x25>
 2d6:	01 d0                	add    %edx,%eax
  dst = vdst;
 2d8:	89 d7                	mov    %edx,%edi
 2da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *dst++ = *src++;
 2e0:	a4                   	movsb  %ds:(%esi),%es:(%edi)
  while(n-- > 0)
 2e1:	39 f8                	cmp    %edi,%eax
 2e3:	75 fb                	jne    2e0 <memmove+0x20>
  return vdst;
}
 2e5:	5e                   	pop    %esi
 2e6:	89 d0                	mov    %edx,%eax
 2e8:	5f                   	pop    %edi
 2e9:	5d                   	pop    %ebp
 2ea:	c3                   	ret    

000002eb <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 2eb:	b8 01 00 00 00       	mov    $0x1,%eax
 2f0:	cd 40                	int    $0x40
 2f2:	c3                   	ret    

000002f3 <exit>:
SYSCALL(exit)
 2f3:	b8 02 00 00 00       	mov    $0x2,%eax
 2f8:	cd 40                	int    $0x40
 2fa:	c3                   	ret    

000002fb <wait>:
SYSCALL(wait)
 2fb:	b8 03 00 00 00       	mov    $0x3,%eax
 300:	cd 40                	int    $0x40
 302:	c3                   	ret    

00000303 <pipe>:
SYSCALL(pipe)
 303:	b8 04 00 00 00       	mov    $0x4,%eax
 308:	cd 40                	int    $0x40
 30a:	c3                   	ret    

0000030b <read>:
SYSCALL(read)
 30b:	b8 05 00 00 00       	mov    $0x5,%eax
 310:	cd 40                	int    $0x40
 312:	c3                   	ret    

00000313 <write>:
SYSCALL(write)
 313:	b8 10 00 00 00       	mov    $0x10,%eax
 318:	cd 40                	int    $0x40
 31a:	c3                   	ret    

0000031b <close>:
SYSCALL(close)
 31b:	b8 15 00 00 00       	mov    $0x15,%eax
 320:	cd 40                	int    $0x40
 322:	c3                   	ret    

00000323 <kill>:
SYSCALL(kill)
 323:	b8 06 00 00 00       	mov    $0x6,%eax
 328:	cd 40                	int    $0x40
 32a:	c3                   	ret    

0000032b <exec>:
SYSCALL(exec)
 32b:	b8 07 00 00 00       	mov    $0x7,%eax
 330:	cd 40                	int    $0x40
 332:	c3                   	ret    

00000333 <open>:
SYSCALL(open)
 333:	b8 0f 00 00 00       	mov    $0xf,%eax
 338:	cd 40                	int    $0x40
 33a:	c3                   	ret    

0000033b <mknod>:
SYSCALL(mknod)
 33b:	b8 11 00 00 00       	mov    $0x11,%eax
 340:	cd 40                	int    $0x40
 342:	c3                   	ret    

00000343 <unlink>:
SYSCALL(unlink)
 343:	b8 12 00 00 00       	mov    $0x12,%eax
 348:	cd 40                	int    $0x40
 34a:	c3                   	ret    

0000034b <fstat>:
SYSCALL(fstat)
 34b:	b8 08 00 00 00       	mov    $0x8,%eax
 350:	cd 40                	int    $0x40
 352:	c3                   	ret    

00000353 <link>:
SYSCALL(link)
 353:	b8 13 00 00 00       	mov    $0x13,%eax
 358:	cd 40                	int    $0x40
 35a:	c3                   	ret    

0000035b <mkdir>:
SYSCALL(mkdir)
 35b:	b8 14 00 00 00       	mov    $0x14,%eax
 360:	cd 40                	int    $0x40
 362:	c3                   	ret    

00000363 <chdir>:
SYSCALL(chdir)
 363:	b8 09 00 00 00       	mov    $0x9,%eax
 368:	cd 40                	int    $0x40
 36a:	c3                   	ret    

0000036b <dup>:
SYSCALL(dup)
 36b:	b8 0a 00 00 00       	mov    $0xa,%eax
 370:	cd 40                	int    $0x40
 372:	c3                   	ret    

00000373 <getpid>:
SYSCALL(getpid)
 373:	b8 0b 00 00 00       	mov    $0xb,%eax
 378:	cd 40                	int    $0x40
 37a:	c3                   	ret    

0000037b <sbrk>:
SYSCALL(sbrk)
 37b:	b8 0c 00 00 00       	mov    $0xc,%eax
 380:	cd 40                	int    $0x40
 382:	c3                   	ret    

00000383 <sleep>:
SYSCALL(sleep)
 383:	b8 0d 00 00 00       	mov    $0xd,%eax
 388:	cd 40                	int    $0x40
 38a:	c3                   	ret    

0000038b <uptime>:
SYSCALL(uptime)
 38b:	b8 0e 00 00 00       	mov    $0xe,%eax
 390:	cd 40                	int    $0x40
 392:	c3                   	ret    

00000393 <poweroff>:
SYSCALL(poweroff)
 393:	b8 16 00 00 00       	mov    $0x16,%eax
 398:	cd 40                	int    $0x40
 39a:	c3                   	ret    

0000039b <calculate_sum_of_digits>:
SYSCALL(calculate_sum_of_digits)
 39b:	b8 17 00 00 00       	mov    $0x17,%eax
 3a0:	cd 40                	int    $0x40
 3a2:	c3                   	ret    

000003a3 <get_parent_pid>:
SYSCALL(get_parent_pid)
 3a3:	b8 18 00 00 00       	mov    $0x18,%eax
 3a8:	cd 40                	int    $0x40
 3aa:	c3                   	ret    

000003ab <set_process_parent>:
SYSCALL(set_process_parent)
 3ab:	b8 19 00 00 00       	mov    $0x19,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    
 3b3:	66 90                	xchg   %ax,%ax
 3b5:	66 90                	xchg   %ax,%ax
 3b7:	66 90                	xchg   %ax,%ax
 3b9:	66 90                	xchg   %ax,%ax
 3bb:	66 90                	xchg   %ax,%ax
 3bd:	66 90                	xchg   %ax,%ax
 3bf:	90                   	nop

000003c0 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
 3c0:	55                   	push   %ebp
 3c1:	89 e5                	mov    %esp,%ebp
 3c3:	57                   	push   %edi
 3c4:	56                   	push   %esi
 3c5:	53                   	push   %ebx
 3c6:	83 ec 3c             	sub    $0x3c,%esp
 3c9:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
 3cc:	89 d1                	mov    %edx,%ecx
{
 3ce:	89 45 b8             	mov    %eax,-0x48(%ebp)
  if(sgn && xx < 0){
 3d1:	85 d2                	test   %edx,%edx
 3d3:	0f 89 7f 00 00 00    	jns    458 <printint+0x98>
 3d9:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
 3dd:	74 79                	je     458 <printint+0x98>
    neg = 1;
 3df:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
    x = -xx;
 3e6:	f7 d9                	neg    %ecx
  } else {
    x = xx;
  }

  i = 0;
 3e8:	31 db                	xor    %ebx,%ebx
 3ea:	8d 75 d7             	lea    -0x29(%ebp),%esi
 3ed:	8d 76 00             	lea    0x0(%esi),%esi
  do{
    buf[i++] = digits[x % base];
 3f0:	89 c8                	mov    %ecx,%eax
 3f2:	31 d2                	xor    %edx,%edx
 3f4:	89 cf                	mov    %ecx,%edi
 3f6:	f7 75 c4             	divl   -0x3c(%ebp)
 3f9:	0f b6 92 d4 08 00 00 	movzbl 0x8d4(%edx),%edx
 400:	89 45 c0             	mov    %eax,-0x40(%ebp)
 403:	89 d8                	mov    %ebx,%eax
 405:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
 408:	8b 4d c0             	mov    -0x40(%ebp),%ecx
    buf[i++] = digits[x % base];
 40b:	88 14 1e             	mov    %dl,(%esi,%ebx,1)
  }while((x /= base) != 0);
 40e:	39 7d c4             	cmp    %edi,-0x3c(%ebp)
 411:	76 dd                	jbe    3f0 <printint+0x30>
  if(neg)
 413:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 416:	85 c9                	test   %ecx,%ecx
 418:	74 0c                	je     426 <printint+0x66>
    buf[i++] = '-';
 41a:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
 41f:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
 421:	ba 2d 00 00 00       	mov    $0x2d,%edx

  while(--i >= 0)
 426:	8b 7d b8             	mov    -0x48(%ebp),%edi
 429:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
 42d:	eb 07                	jmp    436 <printint+0x76>
 42f:	90                   	nop
    putc(fd, buf[i]);
 430:	0f b6 13             	movzbl (%ebx),%edx
 433:	83 eb 01             	sub    $0x1,%ebx
  write(fd, &c, 1);
 436:	83 ec 04             	sub    $0x4,%esp
 439:	88 55 d7             	mov    %dl,-0x29(%ebp)
 43c:	6a 01                	push   $0x1
 43e:	56                   	push   %esi
 43f:	57                   	push   %edi
 440:	e8 ce fe ff ff       	call   313 <write>
  while(--i >= 0)
 445:	83 c4 10             	add    $0x10,%esp
 448:	39 de                	cmp    %ebx,%esi
 44a:	75 e4                	jne    430 <printint+0x70>
}
 44c:	8d 65 f4             	lea    -0xc(%ebp),%esp
 44f:	5b                   	pop    %ebx
 450:	5e                   	pop    %esi
 451:	5f                   	pop    %edi
 452:	5d                   	pop    %ebp
 453:	c3                   	ret    
 454:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  neg = 0;
 458:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%ebp)
 45f:	eb 87                	jmp    3e8 <printint+0x28>
 461:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 468:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 46f:	90                   	nop

00000470 <printfloat>:

// MOD-2 : Added printf for floats
void
printfloat(int fd, float xx)
{
 470:	f3 0f 1e fb          	endbr32 
 474:	55                   	push   %ebp

  int beg=(int)(xx);
	int fin=(int)(xx*100)-beg*100;
  printint(fd, beg, 10, 1);
 475:	b9 0a 00 00 00       	mov    $0xa,%ecx
{
 47a:	89 e5                	mov    %esp,%ebp
 47c:	57                   	push   %edi
 47d:	56                   	push   %esi
  write(fd, &c, 1);
 47e:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 481:	53                   	push   %ebx
 482:	83 ec 38             	sub    $0x38,%esp
 485:	d9 45 0c             	flds   0xc(%ebp)
 488:	8b 75 08             	mov    0x8(%ebp),%esi
  int beg=(int)(xx);
 48b:	d9 7d d6             	fnstcw -0x2a(%ebp)
  printint(fd, beg, 10, 1);
 48e:	6a 01                	push   $0x1
  int beg=(int)(xx);
 490:	0f b7 45 d6          	movzwl -0x2a(%ebp),%eax
 494:	80 cc 0c             	or     $0xc,%ah
 497:	66 89 45 d4          	mov    %ax,-0x2c(%ebp)
  printint(fd, beg, 10, 1);
 49b:	89 f0                	mov    %esi,%eax
  int beg=(int)(xx);
 49d:	d9 6d d4             	fldcw  -0x2c(%ebp)
 4a0:	db 55 d0             	fistl  -0x30(%ebp)
 4a3:	d9 6d d6             	fldcw  -0x2a(%ebp)
 4a6:	8b 55 d0             	mov    -0x30(%ebp),%edx
	int fin=(int)(xx*100)-beg*100;
 4a9:	d8 0d e8 08 00 00    	fmuls  0x8e8
 4af:	d9 6d d4             	fldcw  -0x2c(%ebp)
 4b2:	db 5d d0             	fistpl -0x30(%ebp)
 4b5:	d9 6d d6             	fldcw  -0x2a(%ebp)
 4b8:	6b da 9c             	imul   $0xffffff9c,%edx,%ebx
 4bb:	03 5d d0             	add    -0x30(%ebp),%ebx
  printint(fd, beg, 10, 1);
 4be:	e8 fd fe ff ff       	call   3c0 <printint>
  write(fd, &c, 1);
 4c3:	83 c4 0c             	add    $0xc,%esp
 4c6:	c6 45 e7 2e          	movb   $0x2e,-0x19(%ebp)
 4ca:	6a 01                	push   $0x1
 4cc:	57                   	push   %edi
 4cd:	56                   	push   %esi
 4ce:	e8 40 fe ff ff       	call   313 <write>
  putc(fd, '.');
	if(fin<10)
 4d3:	83 c4 10             	add    $0x10,%esp
 4d6:	83 fb 09             	cmp    $0x9,%ebx
 4d9:	7e 25                	jle    500 <printfloat+0x90>
    putc(fd, '0');
	printint(fd, fin, 10, 1);
 4db:	c7 45 08 01 00 00 00 	movl   $0x1,0x8(%ebp)
}
 4e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
	printint(fd, fin, 10, 1);
 4e5:	89 da                	mov    %ebx,%edx
 4e7:	89 f0                	mov    %esi,%eax
}
 4e9:	5b                   	pop    %ebx
	printint(fd, fin, 10, 1);
 4ea:	b9 0a 00 00 00       	mov    $0xa,%ecx
}
 4ef:	5e                   	pop    %esi
 4f0:	5f                   	pop    %edi
 4f1:	5d                   	pop    %ebp
	printint(fd, fin, 10, 1);
 4f2:	e9 c9 fe ff ff       	jmp    3c0 <printint>
 4f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4fe:	66 90                	xchg   %ax,%ax
  write(fd, &c, 1);
 500:	83 ec 04             	sub    $0x4,%esp
 503:	c6 45 e7 30          	movb   $0x30,-0x19(%ebp)
 507:	6a 01                	push   $0x1
 509:	57                   	push   %edi
 50a:	56                   	push   %esi
 50b:	e8 03 fe ff ff       	call   313 <write>
    putc(fd, '0');
 510:	83 c4 10             	add    $0x10,%esp
 513:	eb c6                	jmp    4db <printfloat+0x6b>
 515:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 51c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000520 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s, %f.
void
printf(int fd, const char *fmt, ...)
{
 520:	f3 0f 1e fb          	endbr32 
 524:	55                   	push   %ebp
 525:	89 e5                	mov    %esp,%ebp
 527:	57                   	push   %edi
 528:	56                   	push   %esi
 529:	53                   	push   %ebx
 52a:	83 ec 2c             	sub    $0x2c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 52d:	8b 75 0c             	mov    0xc(%ebp),%esi
 530:	0f b6 1e             	movzbl (%esi),%ebx
 533:	84 db                	test   %bl,%bl
 535:	0f 84 bd 00 00 00    	je     5f8 <printf+0xd8>
  ap = (uint*)(void*)&fmt + 1;
 53b:	8d 45 10             	lea    0x10(%ebp),%eax
 53e:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 541:	8d 7d e7             	lea    -0x19(%ebp),%edi
  state = 0;
 544:	31 d2                	xor    %edx,%edx
  ap = (uint*)(void*)&fmt + 1;
 546:	89 45 cc             	mov    %eax,-0x34(%ebp)
 549:	eb 33                	jmp    57e <printf+0x5e>
 54b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 54f:	90                   	nop
 550:	89 55 d0             	mov    %edx,-0x30(%ebp)
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
 553:	ba 25 00 00 00       	mov    $0x25,%edx
      if(c == '%'){
 558:	83 f8 25             	cmp    $0x25,%eax
 55b:	74 17                	je     574 <printf+0x54>
  write(fd, &c, 1);
 55d:	83 ec 04             	sub    $0x4,%esp
 560:	88 5d e7             	mov    %bl,-0x19(%ebp)
 563:	6a 01                	push   $0x1
 565:	57                   	push   %edi
 566:	ff 75 08             	push   0x8(%ebp)
 569:	e8 a5 fd ff ff       	call   313 <write>
 56e:	8b 55 d0             	mov    -0x30(%ebp),%edx
      } else {
        putc(fd, c);
 571:	83 c4 10             	add    $0x10,%esp
  for(i = 0; fmt[i]; i++){
 574:	0f b6 1e             	movzbl (%esi),%ebx
 577:	83 c6 01             	add    $0x1,%esi
 57a:	84 db                	test   %bl,%bl
 57c:	74 7a                	je     5f8 <printf+0xd8>
    c = fmt[i] & 0xff;
 57e:	0f be cb             	movsbl %bl,%ecx
 581:	0f b6 c3             	movzbl %bl,%eax
    if(state == 0){
 584:	85 d2                	test   %edx,%edx
 586:	74 c8                	je     550 <printf+0x30>
      }
    } else if(state == '%'){
 588:	83 fa 25             	cmp    $0x25,%edx
 58b:	75 e7                	jne    574 <printf+0x54>
      if(c == 'd'){
 58d:	83 f8 64             	cmp    $0x64,%eax
 590:	0f 84 9a 00 00 00    	je     630 <printf+0x110>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 596:	81 e1 f7 00 00 00    	and    $0xf7,%ecx
 59c:	83 f9 70             	cmp    $0x70,%ecx
 59f:	74 5f                	je     600 <printf+0xe0>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 5a1:	83 f8 73             	cmp    $0x73,%eax
 5a4:	0f 84 d6 00 00 00    	je     680 <printf+0x160>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 5aa:	83 f8 63             	cmp    $0x63,%eax
 5ad:	0f 84 8d 00 00 00    	je     640 <printf+0x120>
        putc(fd, *ap);
        ap++;
      } else if(c == 'f'){ // MOD-2
 5b3:	83 f8 66             	cmp    $0x66,%eax
 5b6:	0f 84 f4 00 00 00    	je     6b0 <printf+0x190>
        printfloat(fd, (float)*ap);
        ap++;
      } else if(c == '%'){
 5bc:	83 f8 25             	cmp    $0x25,%eax
 5bf:	0f 84 ab 00 00 00    	je     670 <printf+0x150>
  write(fd, &c, 1);
 5c5:	83 ec 04             	sub    $0x4,%esp
 5c8:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
 5cc:	6a 01                	push   $0x1
 5ce:	57                   	push   %edi
 5cf:	ff 75 08             	push   0x8(%ebp)
 5d2:	e8 3c fd ff ff       	call   313 <write>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
        putc(fd, c);
 5d7:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 5da:	83 c4 0c             	add    $0xc,%esp
 5dd:	6a 01                	push   $0x1
  for(i = 0; fmt[i]; i++){
 5df:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 5e2:	57                   	push   %edi
 5e3:	ff 75 08             	push   0x8(%ebp)
 5e6:	e8 28 fd ff ff       	call   313 <write>
  for(i = 0; fmt[i]; i++){
 5eb:	0f b6 5e ff          	movzbl -0x1(%esi),%ebx
        putc(fd, c);
 5ef:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 5f2:	31 d2                	xor    %edx,%edx
  for(i = 0; fmt[i]; i++){
 5f4:	84 db                	test   %bl,%bl
 5f6:	75 86                	jne    57e <printf+0x5e>
    }
  }
 5f8:	8d 65 f4             	lea    -0xc(%ebp),%esp
 5fb:	5b                   	pop    %ebx
 5fc:	5e                   	pop    %esi
 5fd:	5f                   	pop    %edi
 5fe:	5d                   	pop    %ebp
 5ff:	c3                   	ret    
        printint(fd, *ap, 16, 0);
 600:	83 ec 0c             	sub    $0xc,%esp
 603:	b9 10 00 00 00       	mov    $0x10,%ecx
 608:	6a 00                	push   $0x0
 60a:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 60d:	8b 45 08             	mov    0x8(%ebp),%eax
 610:	8b 13                	mov    (%ebx),%edx
 612:	e8 a9 fd ff ff       	call   3c0 <printint>
        ap++;
 617:	89 d8                	mov    %ebx,%eax
 619:	83 c4 10             	add    $0x10,%esp
      state = 0;
 61c:	31 d2                	xor    %edx,%edx
        ap++;
 61e:	83 c0 04             	add    $0x4,%eax
 621:	89 45 cc             	mov    %eax,-0x34(%ebp)
 624:	e9 4b ff ff ff       	jmp    574 <printf+0x54>
 629:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        printint(fd, *ap, 10, 1);
 630:	83 ec 0c             	sub    $0xc,%esp
 633:	b9 0a 00 00 00       	mov    $0xa,%ecx
 638:	6a 01                	push   $0x1
 63a:	eb ce                	jmp    60a <printf+0xea>
 63c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        putc(fd, *ap);
 640:	8b 5d cc             	mov    -0x34(%ebp),%ebx
  write(fd, &c, 1);
 643:	83 ec 04             	sub    $0x4,%esp
        putc(fd, *ap);
 646:	8b 03                	mov    (%ebx),%eax
  write(fd, &c, 1);
 648:	6a 01                	push   $0x1
        ap++;
 64a:	83 c3 04             	add    $0x4,%ebx
  write(fd, &c, 1);
 64d:	57                   	push   %edi
 64e:	ff 75 08             	push   0x8(%ebp)
        putc(fd, *ap);
 651:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 654:	e8 ba fc ff ff       	call   313 <write>
        ap++;
 659:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 65c:	83 c4 10             	add    $0x10,%esp
      state = 0;
 65f:	31 d2                	xor    %edx,%edx
 661:	e9 0e ff ff ff       	jmp    574 <printf+0x54>
 666:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 66d:	8d 76 00             	lea    0x0(%esi),%esi
        putc(fd, c);
 670:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 673:	83 ec 04             	sub    $0x4,%esp
 676:	e9 62 ff ff ff       	jmp    5dd <printf+0xbd>
 67b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 67f:	90                   	nop
        s = (char*)*ap;
 680:	8b 45 cc             	mov    -0x34(%ebp),%eax
 683:	8b 18                	mov    (%eax),%ebx
        ap++;
 685:	83 c0 04             	add    $0x4,%eax
 688:	89 45 cc             	mov    %eax,-0x34(%ebp)
        if(s == 0)
 68b:	85 db                	test   %ebx,%ebx
 68d:	74 4f                	je     6de <printf+0x1be>
        while(*s != 0){
 68f:	0f b6 03             	movzbl (%ebx),%eax
      state = 0;
 692:	31 d2                	xor    %edx,%edx
        while(*s != 0){
 694:	84 c0                	test   %al,%al
 696:	0f 84 d8 fe ff ff    	je     574 <printf+0x54>
 69c:	89 75 d0             	mov    %esi,-0x30(%ebp)
 69f:	89 de                	mov    %ebx,%esi
 6a1:	8b 5d 08             	mov    0x8(%ebp),%ebx
 6a4:	eb 4a                	jmp    6f0 <printf+0x1d0>
 6a6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 6ad:	8d 76 00             	lea    0x0(%esi),%esi
        printfloat(fd, (float)*ap);
 6b0:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 6b3:	31 d2                	xor    %edx,%edx
 6b5:	83 ec 0c             	sub    $0xc,%esp
 6b8:	89 55 d4             	mov    %edx,-0x2c(%ebp)
 6bb:	8b 03                	mov    (%ebx),%eax
        ap++;
 6bd:	83 c3 04             	add    $0x4,%ebx
        printfloat(fd, (float)*ap);
 6c0:	89 45 d0             	mov    %eax,-0x30(%ebp)
 6c3:	df 6d d0             	fildll -0x30(%ebp)
 6c6:	d9 1c 24             	fstps  (%esp)
 6c9:	ff 75 08             	push   0x8(%ebp)
 6cc:	e8 9f fd ff ff       	call   470 <printfloat>
        ap++;
 6d1:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 6d4:	83 c4 10             	add    $0x10,%esp
      state = 0;
 6d7:	31 d2                	xor    %edx,%edx
 6d9:	e9 96 fe ff ff       	jmp    574 <printf+0x54>
          s = "(null)";
 6de:	bb cd 08 00 00       	mov    $0x8cd,%ebx
        while(*s != 0){
 6e3:	89 75 d0             	mov    %esi,-0x30(%ebp)
 6e6:	b8 28 00 00 00       	mov    $0x28,%eax
 6eb:	89 de                	mov    %ebx,%esi
 6ed:	8b 5d 08             	mov    0x8(%ebp),%ebx
  write(fd, &c, 1);
 6f0:	83 ec 04             	sub    $0x4,%esp
          s++;
 6f3:	83 c6 01             	add    $0x1,%esi
 6f6:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 6f9:	6a 01                	push   $0x1
 6fb:	57                   	push   %edi
 6fc:	53                   	push   %ebx
 6fd:	e8 11 fc ff ff       	call   313 <write>
        while(*s != 0){
 702:	0f b6 06             	movzbl (%esi),%eax
 705:	83 c4 10             	add    $0x10,%esp
 708:	84 c0                	test   %al,%al
 70a:	75 e4                	jne    6f0 <printf+0x1d0>
      state = 0;
 70c:	8b 75 d0             	mov    -0x30(%ebp),%esi
 70f:	31 d2                	xor    %edx,%edx
 711:	e9 5e fe ff ff       	jmp    574 <printf+0x54>
 716:	66 90                	xchg   %ax,%ax
 718:	66 90                	xchg   %ax,%ax
 71a:	66 90                	xchg   %ax,%ax
 71c:	66 90                	xchg   %ax,%ax
 71e:	66 90                	xchg   %ax,%ax

00000720 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 720:	f3 0f 1e fb          	endbr32 
 724:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 725:	a1 b4 0b 00 00       	mov    0xbb4,%eax
{
 72a:	89 e5                	mov    %esp,%ebp
 72c:	57                   	push   %edi
 72d:	56                   	push   %esi
 72e:	53                   	push   %ebx
 72f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = (Header*)ap - 1;
 732:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 735:	8d 76 00             	lea    0x0(%esi),%esi
 738:	89 c2                	mov    %eax,%edx
 73a:	8b 00                	mov    (%eax),%eax
 73c:	39 ca                	cmp    %ecx,%edx
 73e:	73 30                	jae    770 <free+0x50>
 740:	39 c1                	cmp    %eax,%ecx
 742:	72 04                	jb     748 <free+0x28>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 744:	39 c2                	cmp    %eax,%edx
 746:	72 f0                	jb     738 <free+0x18>
      break;
  if(bp + bp->s.size == p->s.ptr){
 748:	8b 73 fc             	mov    -0x4(%ebx),%esi
 74b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 74e:	39 f8                	cmp    %edi,%eax
 750:	74 30                	je     782 <free+0x62>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 752:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 755:	8b 42 04             	mov    0x4(%edx),%eax
 758:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 75b:	39 f1                	cmp    %esi,%ecx
 75d:	74 3a                	je     799 <free+0x79>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 75f:	89 0a                	mov    %ecx,(%edx)
  freep = p;
}
 761:	5b                   	pop    %ebx
  freep = p;
 762:	89 15 b4 0b 00 00    	mov    %edx,0xbb4
}
 768:	5e                   	pop    %esi
 769:	5f                   	pop    %edi
 76a:	5d                   	pop    %ebp
 76b:	c3                   	ret    
 76c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 770:	39 c2                	cmp    %eax,%edx
 772:	72 c4                	jb     738 <free+0x18>
 774:	39 c1                	cmp    %eax,%ecx
 776:	73 c0                	jae    738 <free+0x18>
  if(bp + bp->s.size == p->s.ptr){
 778:	8b 73 fc             	mov    -0x4(%ebx),%esi
 77b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 77e:	39 f8                	cmp    %edi,%eax
 780:	75 d0                	jne    752 <free+0x32>
    bp->s.size += p->s.ptr->s.size;
 782:	03 70 04             	add    0x4(%eax),%esi
 785:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 788:	8b 02                	mov    (%edx),%eax
 78a:	8b 00                	mov    (%eax),%eax
 78c:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 78f:	8b 42 04             	mov    0x4(%edx),%eax
 792:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 795:	39 f1                	cmp    %esi,%ecx
 797:	75 c6                	jne    75f <free+0x3f>
    p->s.size += bp->s.size;
 799:	03 43 fc             	add    -0x4(%ebx),%eax
  freep = p;
 79c:	89 15 b4 0b 00 00    	mov    %edx,0xbb4
    p->s.size += bp->s.size;
 7a2:	89 42 04             	mov    %eax,0x4(%edx)
    p->s.ptr = bp->s.ptr;
 7a5:	8b 43 f8             	mov    -0x8(%ebx),%eax
 7a8:	89 02                	mov    %eax,(%edx)
}
 7aa:	5b                   	pop    %ebx
 7ab:	5e                   	pop    %esi
 7ac:	5f                   	pop    %edi
 7ad:	5d                   	pop    %ebp
 7ae:	c3                   	ret    
 7af:	90                   	nop

000007b0 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
 7b0:	f3 0f 1e fb          	endbr32 
 7b4:	55                   	push   %ebp
 7b5:	89 e5                	mov    %esp,%ebp
 7b7:	57                   	push   %edi
 7b8:	56                   	push   %esi
 7b9:	53                   	push   %ebx
 7ba:	83 ec 1c             	sub    $0x1c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7bd:	8b 45 08             	mov    0x8(%ebp),%eax
  if((prevp = freep) == 0){
 7c0:	8b 3d b4 0b 00 00    	mov    0xbb4,%edi
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7c6:	8d 70 07             	lea    0x7(%eax),%esi
 7c9:	c1 ee 03             	shr    $0x3,%esi
 7cc:	83 c6 01             	add    $0x1,%esi
  if((prevp = freep) == 0){
 7cf:	85 ff                	test   %edi,%edi
 7d1:	0f 84 a9 00 00 00    	je     880 <malloc+0xd0>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7d7:	8b 07                	mov    (%edi),%eax
    if(p->s.size >= nunits){
 7d9:	8b 48 04             	mov    0x4(%eax),%ecx
 7dc:	39 f1                	cmp    %esi,%ecx
 7de:	73 6d                	jae    84d <malloc+0x9d>
 7e0:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
 7e6:	bb 00 10 00 00       	mov    $0x1000,%ebx
 7eb:	0f 43 de             	cmovae %esi,%ebx
  p = sbrk(nu * sizeof(Header));
 7ee:	8d 0c dd 00 00 00 00 	lea    0x0(,%ebx,8),%ecx
 7f5:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
 7f8:	eb 17                	jmp    811 <malloc+0x61>
 7fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 800:	8b 10                	mov    (%eax),%edx
    if(p->s.size >= nunits){
 802:	8b 4a 04             	mov    0x4(%edx),%ecx
 805:	39 f1                	cmp    %esi,%ecx
 807:	73 4f                	jae    858 <malloc+0xa8>
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
 809:	8b 3d b4 0b 00 00    	mov    0xbb4,%edi
 80f:	89 d0                	mov    %edx,%eax
 811:	39 c7                	cmp    %eax,%edi
 813:	75 eb                	jne    800 <malloc+0x50>
  p = sbrk(nu * sizeof(Header));
 815:	83 ec 0c             	sub    $0xc,%esp
 818:	ff 75 e4             	push   -0x1c(%ebp)
 81b:	e8 5b fb ff ff       	call   37b <sbrk>
  if(p == (char*)-1)
 820:	83 c4 10             	add    $0x10,%esp
 823:	83 f8 ff             	cmp    $0xffffffff,%eax
 826:	74 1b                	je     843 <malloc+0x93>
  hp->s.size = nu;
 828:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 82b:	83 ec 0c             	sub    $0xc,%esp
 82e:	83 c0 08             	add    $0x8,%eax
 831:	50                   	push   %eax
 832:	e8 e9 fe ff ff       	call   720 <free>
  return freep;
 837:	a1 b4 0b 00 00       	mov    0xbb4,%eax
      if((p = morecore(nunits)) == 0)
 83c:	83 c4 10             	add    $0x10,%esp
 83f:	85 c0                	test   %eax,%eax
 841:	75 bd                	jne    800 <malloc+0x50>
        return 0;
  }
}
 843:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return 0;
 846:	31 c0                	xor    %eax,%eax
}
 848:	5b                   	pop    %ebx
 849:	5e                   	pop    %esi
 84a:	5f                   	pop    %edi
 84b:	5d                   	pop    %ebp
 84c:	c3                   	ret    
    if(p->s.size >= nunits){
 84d:	89 c2                	mov    %eax,%edx
 84f:	89 f8                	mov    %edi,%eax
 851:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->s.size == nunits)
 858:	39 ce                	cmp    %ecx,%esi
 85a:	74 54                	je     8b0 <malloc+0x100>
        p->s.size -= nunits;
 85c:	29 f1                	sub    %esi,%ecx
 85e:	89 4a 04             	mov    %ecx,0x4(%edx)
        p += p->s.size;
 861:	8d 14 ca             	lea    (%edx,%ecx,8),%edx
        p->s.size = nunits;
 864:	89 72 04             	mov    %esi,0x4(%edx)
      freep = prevp;
 867:	a3 b4 0b 00 00       	mov    %eax,0xbb4
}
 86c:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return (void*)(p + 1);
 86f:	8d 42 08             	lea    0x8(%edx),%eax
}
 872:	5b                   	pop    %ebx
 873:	5e                   	pop    %esi
 874:	5f                   	pop    %edi
 875:	5d                   	pop    %ebp
 876:	c3                   	ret    
 877:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 87e:	66 90                	xchg   %ax,%ax
    base.s.ptr = freep = prevp = &base;
 880:	c7 05 b4 0b 00 00 b8 	movl   $0xbb8,0xbb4
 887:	0b 00 00 
    base.s.size = 0;
 88a:	bf b8 0b 00 00       	mov    $0xbb8,%edi
    base.s.ptr = freep = prevp = &base;
 88f:	c7 05 b8 0b 00 00 b8 	movl   $0xbb8,0xbb8
 896:	0b 00 00 
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 899:	89 f8                	mov    %edi,%eax
    base.s.size = 0;
 89b:	c7 05 bc 0b 00 00 00 	movl   $0x0,0xbbc
 8a2:	00 00 00 
    if(p->s.size >= nunits){
 8a5:	e9 36 ff ff ff       	jmp    7e0 <malloc+0x30>
 8aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        prevp->s.ptr = p->s.ptr;
 8b0:	8b 0a                	mov    (%edx),%ecx
 8b2:	89 08                	mov    %ecx,(%eax)
 8b4:	eb b1                	jmp    867 <malloc+0xb7>
