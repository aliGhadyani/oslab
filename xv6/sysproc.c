#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

// poweroff syscall
int
sys_poweroff(void) {
  cprintf("sutting down xv6 ...\n");
  outw(0x604, 0x2000);
  return 0;
}

// sum of digits
int
sys_calculate_sum_of_digits(void)
{
  int a = myproc()->tf->ebx; //register after eax
  cprintf("in kernel systemcall sys_calculate_sum_of_digits() called for number %d\n", a);
  return calculate_sum_of_digits(a);
}

// get parent proc id
void
sys_get_parent_pid(void)
{
  int pid = myproc()->tf->ebx; 
  cprintf("in kernel systemcall sys_get_parent_id() called for pid %d which is curent procces\n", pid);
  get_parent_pid(pid);
}

// set parent process
int
sys_set_process_parent(void)
{
  int pid;
  if(argint(0, &pid) < 0){
    return -1;
  }
  return set_process_parent(pid);
}

// get list of file sectors
/* int 
sys_get_file_sectors(void)
{
  int fd;
  int* seq;
  int seq_sz;
  if(argint(0, &fd) < 0) {
    cprintf("file description fail!\n");
    return -1;
  } 
  if(argptr(1, &seq, sizeof(int)) < 0) {
    cprintf("array fail!\n");
    return -1;
  }
  if(argint(0, &seq_sz) < 0) {
    cprintf("seq size fail!\n");
    return -1;
  }

  return get_file_sectors(fd, seq);
} */