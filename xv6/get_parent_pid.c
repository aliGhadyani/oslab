#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[])
{
    int saved_ebx;
    int pid0, pid1;
    pid0 = fork();
    if (pid0)
        printf(1, "pid: %d, parent: %d\n", pid0, getpid());
        
    pid1 = fork();
    if (pid1 && pid0 == 0){
        printf(1, "pid: %d, parent: %d\n", pid1, getpid());
        asm volatile(
        "movl %%ebx, %0;" // saved_ebx = ebx
        "movl %1, %%ebx;" // ebx = number
        : "=r" (saved_ebx)
        : "r"(pid1)
        );
        get_parent_pid(pid1);
        asm("movl %0, %%ebx" : : "r"(saved_ebx)); // ebx = saved_ebx -> restore 
    }
    while(wait() != -1); // for preventing to be in zombie state
    exit();
}