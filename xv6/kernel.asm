
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:
8010000c:	0f 20 e0             	mov    %cr4,%eax
8010000f:	83 c8 10             	or     $0x10,%eax
80100012:	0f 22 e0             	mov    %eax,%cr4
80100015:	b8 00 a0 10 00       	mov    $0x10a000,%eax
8010001a:	0f 22 d8             	mov    %eax,%cr3
8010001d:	0f 20 c0             	mov    %cr0,%eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
80100025:	0f 22 c0             	mov    %eax,%cr0
80100028:	bc d0 6a 11 80       	mov    $0x80116ad0,%esp
8010002d:	b8 b0 37 10 80       	mov    $0x801037b0,%eax
80100032:	ff e0                	jmp    *%eax
80100034:	66 90                	xchg   %ax,%ax
80100036:	66 90                	xchg   %ax,%ax
80100038:	66 90                	xchg   %ax,%ax
8010003a:	66 90                	xchg   %ax,%ax
8010003c:	66 90                	xchg   %ax,%ax
8010003e:	66 90                	xchg   %ax,%ax

80100040 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100040:	f3 0f 1e fb          	endbr32 
80100044:	55                   	push   %ebp
80100045:	89 e5                	mov    %esp,%ebp
80100047:	53                   	push   %ebx

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100048:	bb 54 b5 10 80       	mov    $0x8010b554,%ebx
{
8010004d:	83 ec 0c             	sub    $0xc,%esp
  initlock(&bcache.lock, "bcache");
80100050:	68 40 7f 10 80       	push   $0x80107f40
80100055:	68 20 b5 10 80       	push   $0x8010b520
8010005a:	e8 01 4f 00 00       	call   80104f60 <initlock>
  bcache.head.next = &bcache.head;
8010005f:	83 c4 10             	add    $0x10,%esp
80100062:	b8 1c fc 10 80       	mov    $0x8010fc1c,%eax
  bcache.head.prev = &bcache.head;
80100067:	c7 05 6c fc 10 80 1c 	movl   $0x8010fc1c,0x8010fc6c
8010006e:	fc 10 80 
  bcache.head.next = &bcache.head;
80100071:	c7 05 70 fc 10 80 1c 	movl   $0x8010fc1c,0x8010fc70
80100078:	fc 10 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010007b:	eb 05                	jmp    80100082 <binit+0x42>
8010007d:	8d 76 00             	lea    0x0(%esi),%esi
80100080:	89 d3                	mov    %edx,%ebx
    b->next = bcache.head.next;
80100082:	89 43 54             	mov    %eax,0x54(%ebx)
    b->prev = &bcache.head;
    initsleeplock(&b->lock, "buffer");
80100085:	83 ec 08             	sub    $0x8,%esp
80100088:	8d 43 0c             	lea    0xc(%ebx),%eax
    b->prev = &bcache.head;
8010008b:	c7 43 50 1c fc 10 80 	movl   $0x8010fc1c,0x50(%ebx)
    initsleeplock(&b->lock, "buffer");
80100092:	68 47 7f 10 80       	push   $0x80107f47
80100097:	50                   	push   %eax
80100098:	e8 83 4d 00 00       	call   80104e20 <initsleeplock>
    bcache.head.next->prev = b;
8010009d:	a1 70 fc 10 80       	mov    0x8010fc70,%eax
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000a2:	8d 93 5c 02 00 00    	lea    0x25c(%ebx),%edx
801000a8:	83 c4 10             	add    $0x10,%esp
    bcache.head.next->prev = b;
801000ab:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
801000ae:	89 d8                	mov    %ebx,%eax
801000b0:	89 1d 70 fc 10 80    	mov    %ebx,0x8010fc70
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000b6:	81 fb c0 f9 10 80    	cmp    $0x8010f9c0,%ebx
801000bc:	75 c2                	jne    80100080 <binit+0x40>
  }
}
801000be:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801000c1:	c9                   	leave  
801000c2:	c3                   	ret    
801000c3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801000ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801000d0 <bread>:
}

// Return a locked buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801000d0:	f3 0f 1e fb          	endbr32 
801000d4:	55                   	push   %ebp
801000d5:	89 e5                	mov    %esp,%ebp
801000d7:	57                   	push   %edi
801000d8:	56                   	push   %esi
801000d9:	53                   	push   %ebx
801000da:	83 ec 18             	sub    $0x18,%esp
801000dd:	8b 75 08             	mov    0x8(%ebp),%esi
801000e0:	8b 7d 0c             	mov    0xc(%ebp),%edi
  acquire(&bcache.lock);
801000e3:	68 20 b5 10 80       	push   $0x8010b520
801000e8:	e8 73 50 00 00       	call   80105160 <acquire>
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000ed:	8b 1d 70 fc 10 80    	mov    0x8010fc70,%ebx
801000f3:	83 c4 10             	add    $0x10,%esp
801000f6:	81 fb 1c fc 10 80    	cmp    $0x8010fc1c,%ebx
801000fc:	75 0d                	jne    8010010b <bread+0x3b>
801000fe:	eb 20                	jmp    80100120 <bread+0x50>
80100100:	8b 5b 54             	mov    0x54(%ebx),%ebx
80100103:	81 fb 1c fc 10 80    	cmp    $0x8010fc1c,%ebx
80100109:	74 15                	je     80100120 <bread+0x50>
    if(b->dev == dev && b->blockno == blockno){
8010010b:	3b 73 04             	cmp    0x4(%ebx),%esi
8010010e:	75 f0                	jne    80100100 <bread+0x30>
80100110:	3b 7b 08             	cmp    0x8(%ebx),%edi
80100113:	75 eb                	jne    80100100 <bread+0x30>
      b->refcnt++;
80100115:	83 43 4c 01          	addl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
80100119:	eb 3f                	jmp    8010015a <bread+0x8a>
8010011b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010011f:	90                   	nop
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100120:	8b 1d 6c fc 10 80    	mov    0x8010fc6c,%ebx
80100126:	81 fb 1c fc 10 80    	cmp    $0x8010fc1c,%ebx
8010012c:	75 0d                	jne    8010013b <bread+0x6b>
8010012e:	eb 6e                	jmp    8010019e <bread+0xce>
80100130:	8b 5b 50             	mov    0x50(%ebx),%ebx
80100133:	81 fb 1c fc 10 80    	cmp    $0x8010fc1c,%ebx
80100139:	74 63                	je     8010019e <bread+0xce>
    if(b->refcnt == 0 && (b->flags & B_DIRTY) == 0) {
8010013b:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010013e:	85 c0                	test   %eax,%eax
80100140:	75 ee                	jne    80100130 <bread+0x60>
80100142:	f6 03 04             	testb  $0x4,(%ebx)
80100145:	75 e9                	jne    80100130 <bread+0x60>
      b->dev = dev;
80100147:	89 73 04             	mov    %esi,0x4(%ebx)
      b->blockno = blockno;
8010014a:	89 7b 08             	mov    %edi,0x8(%ebx)
      b->flags = 0;
8010014d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
      b->refcnt = 1;
80100153:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
8010015a:	83 ec 0c             	sub    $0xc,%esp
8010015d:	68 20 b5 10 80       	push   $0x8010b520
80100162:	e8 89 4f 00 00       	call   801050f0 <release>
      acquiresleep(&b->lock);
80100167:	8d 43 0c             	lea    0xc(%ebx),%eax
8010016a:	89 04 24             	mov    %eax,(%esp)
8010016d:	e8 ee 4c 00 00       	call   80104e60 <acquiresleep>
      return b;
80100172:	83 c4 10             	add    $0x10,%esp
  struct buf *b;

  b = bget(dev, blockno);
  if((b->flags & B_VALID) == 0) {
80100175:	f6 03 02             	testb  $0x2,(%ebx)
80100178:	74 0e                	je     80100188 <bread+0xb8>
    iderw(b);
  }
  return b;
}
8010017a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010017d:	89 d8                	mov    %ebx,%eax
8010017f:	5b                   	pop    %ebx
80100180:	5e                   	pop    %esi
80100181:	5f                   	pop    %edi
80100182:	5d                   	pop    %ebp
80100183:	c3                   	ret    
80100184:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    iderw(b);
80100188:	83 ec 0c             	sub    $0xc,%esp
8010018b:	53                   	push   %ebx
8010018c:	e8 5f 28 00 00       	call   801029f0 <iderw>
80100191:	83 c4 10             	add    $0x10,%esp
}
80100194:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100197:	89 d8                	mov    %ebx,%eax
80100199:	5b                   	pop    %ebx
8010019a:	5e                   	pop    %esi
8010019b:	5f                   	pop    %edi
8010019c:	5d                   	pop    %ebp
8010019d:	c3                   	ret    
  panic("bget: no buffers");
8010019e:	83 ec 0c             	sub    $0xc,%esp
801001a1:	68 4e 7f 10 80       	push   $0x80107f4e
801001a6:	e8 e5 01 00 00       	call   80100390 <panic>
801001ab:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801001af:	90                   	nop

801001b0 <bwrite>:

// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
801001b0:	f3 0f 1e fb          	endbr32 
801001b4:	55                   	push   %ebp
801001b5:	89 e5                	mov    %esp,%ebp
801001b7:	53                   	push   %ebx
801001b8:	83 ec 10             	sub    $0x10,%esp
801001bb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001be:	8d 43 0c             	lea    0xc(%ebx),%eax
801001c1:	50                   	push   %eax
801001c2:	e8 39 4d 00 00       	call   80104f00 <holdingsleep>
801001c7:	83 c4 10             	add    $0x10,%esp
801001ca:	85 c0                	test   %eax,%eax
801001cc:	74 0f                	je     801001dd <bwrite+0x2d>
    panic("bwrite");
  b->flags |= B_DIRTY;
801001ce:	83 0b 04             	orl    $0x4,(%ebx)
  iderw(b);
801001d1:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
801001d4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801001d7:	c9                   	leave  
  iderw(b);
801001d8:	e9 13 28 00 00       	jmp    801029f0 <iderw>
    panic("bwrite");
801001dd:	83 ec 0c             	sub    $0xc,%esp
801001e0:	68 5f 7f 10 80       	push   $0x80107f5f
801001e5:	e8 a6 01 00 00       	call   80100390 <panic>
801001ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801001f0 <brelse>:

// Release a locked buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
801001f0:	f3 0f 1e fb          	endbr32 
801001f4:	55                   	push   %ebp
801001f5:	89 e5                	mov    %esp,%ebp
801001f7:	56                   	push   %esi
801001f8:	53                   	push   %ebx
801001f9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001fc:	8d 73 0c             	lea    0xc(%ebx),%esi
801001ff:	83 ec 0c             	sub    $0xc,%esp
80100202:	56                   	push   %esi
80100203:	e8 f8 4c 00 00       	call   80104f00 <holdingsleep>
80100208:	83 c4 10             	add    $0x10,%esp
8010020b:	85 c0                	test   %eax,%eax
8010020d:	74 66                	je     80100275 <brelse+0x85>
    panic("brelse");

  releasesleep(&b->lock);
8010020f:	83 ec 0c             	sub    $0xc,%esp
80100212:	56                   	push   %esi
80100213:	e8 a8 4c 00 00       	call   80104ec0 <releasesleep>

  acquire(&bcache.lock);
80100218:	c7 04 24 20 b5 10 80 	movl   $0x8010b520,(%esp)
8010021f:	e8 3c 4f 00 00       	call   80105160 <acquire>
  b->refcnt--;
80100224:	8b 43 4c             	mov    0x4c(%ebx),%eax
  if (b->refcnt == 0) {
80100227:	83 c4 10             	add    $0x10,%esp
  b->refcnt--;
8010022a:	83 e8 01             	sub    $0x1,%eax
8010022d:	89 43 4c             	mov    %eax,0x4c(%ebx)
  if (b->refcnt == 0) {
80100230:	85 c0                	test   %eax,%eax
80100232:	75 2f                	jne    80100263 <brelse+0x73>
    // no one is waiting for it.
    b->next->prev = b->prev;
80100234:	8b 43 54             	mov    0x54(%ebx),%eax
80100237:	8b 53 50             	mov    0x50(%ebx),%edx
8010023a:	89 50 50             	mov    %edx,0x50(%eax)
    b->prev->next = b->next;
8010023d:	8b 43 50             	mov    0x50(%ebx),%eax
80100240:	8b 53 54             	mov    0x54(%ebx),%edx
80100243:	89 50 54             	mov    %edx,0x54(%eax)
    b->next = bcache.head.next;
80100246:	a1 70 fc 10 80       	mov    0x8010fc70,%eax
    b->prev = &bcache.head;
8010024b:	c7 43 50 1c fc 10 80 	movl   $0x8010fc1c,0x50(%ebx)
    b->next = bcache.head.next;
80100252:	89 43 54             	mov    %eax,0x54(%ebx)
    bcache.head.next->prev = b;
80100255:	a1 70 fc 10 80       	mov    0x8010fc70,%eax
8010025a:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
8010025d:	89 1d 70 fc 10 80    	mov    %ebx,0x8010fc70
  }
  
  release(&bcache.lock);
80100263:	c7 45 08 20 b5 10 80 	movl   $0x8010b520,0x8(%ebp)
}
8010026a:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010026d:	5b                   	pop    %ebx
8010026e:	5e                   	pop    %esi
8010026f:	5d                   	pop    %ebp
  release(&bcache.lock);
80100270:	e9 7b 4e 00 00       	jmp    801050f0 <release>
    panic("brelse");
80100275:	83 ec 0c             	sub    $0xc,%esp
80100278:	68 66 7f 10 80       	push   $0x80107f66
8010027d:	e8 0e 01 00 00       	call   80100390 <panic>
80100282:	66 90                	xchg   %ax,%ax
80100284:	66 90                	xchg   %ax,%ax
80100286:	66 90                	xchg   %ax,%ax
80100288:	66 90                	xchg   %ax,%ax
8010028a:	66 90                	xchg   %ax,%ax
8010028c:	66 90                	xchg   %ax,%ax
8010028e:	66 90                	xchg   %ax,%ax

80100290 <consoleread>:
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
80100290:	f3 0f 1e fb          	endbr32 
80100294:	55                   	push   %ebp
80100295:	89 e5                	mov    %esp,%ebp
80100297:	57                   	push   %edi
80100298:	56                   	push   %esi
80100299:	53                   	push   %ebx
8010029a:	83 ec 18             	sub    $0x18,%esp
8010029d:	8b 5d 10             	mov    0x10(%ebp),%ebx
801002a0:	8b 75 0c             	mov    0xc(%ebp),%esi
  uint target;
  int c;

  iunlock(ip);
801002a3:	ff 75 08             	push   0x8(%ebp)
  target = n;
801002a6:	89 df                	mov    %ebx,%edi
  iunlock(ip);
801002a8:	e8 93 1c 00 00       	call   80101f40 <iunlock>
  acquire(&cons.lock);
801002ad:	c7 04 24 20 ff 10 80 	movl   $0x8010ff20,(%esp)
801002b4:	e8 a7 4e 00 00       	call   80105160 <acquire>
  while(n > 0){
801002b9:	83 c4 10             	add    $0x10,%esp
801002bc:	85 db                	test   %ebx,%ebx
801002be:	0f 8e 98 00 00 00    	jle    8010035c <consoleread+0xcc>
    while(input.r == input.w){
801002c4:	a1 00 ff 10 80       	mov    0x8010ff00,%eax
801002c9:	3b 05 04 ff 10 80    	cmp    0x8010ff04,%eax
801002cf:	74 29                	je     801002fa <consoleread+0x6a>
801002d1:	eb 5d                	jmp    80100330 <consoleread+0xa0>
801002d3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801002d7:	90                   	nop
      if(myproc()->killed){
        release(&cons.lock);
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
801002d8:	83 ec 08             	sub    $0x8,%esp
801002db:	68 20 ff 10 80       	push   $0x8010ff20
801002e0:	68 00 ff 10 80       	push   $0x8010ff00
801002e5:	e8 76 47 00 00       	call   80104a60 <sleep>
    while(input.r == input.w){
801002ea:	a1 00 ff 10 80       	mov    0x8010ff00,%eax
801002ef:	83 c4 10             	add    $0x10,%esp
801002f2:	3b 05 04 ff 10 80    	cmp    0x8010ff04,%eax
801002f8:	75 36                	jne    80100330 <consoleread+0xa0>
      if(myproc()->killed){
801002fa:	e8 11 3e 00 00       	call   80104110 <myproc>
801002ff:	8b 48 28             	mov    0x28(%eax),%ecx
80100302:	85 c9                	test   %ecx,%ecx
80100304:	74 d2                	je     801002d8 <consoleread+0x48>
        release(&cons.lock);
80100306:	83 ec 0c             	sub    $0xc,%esp
80100309:	68 20 ff 10 80       	push   $0x8010ff20
8010030e:	e8 dd 4d 00 00       	call   801050f0 <release>
        ilock(ip);
80100313:	5a                   	pop    %edx
80100314:	ff 75 08             	push   0x8(%ebp)
80100317:	e8 44 1b 00 00       	call   80101e60 <ilock>
        return -1;
8010031c:	83 c4 10             	add    $0x10,%esp
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}
8010031f:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return -1;
80100322:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100327:	5b                   	pop    %ebx
80100328:	5e                   	pop    %esi
80100329:	5f                   	pop    %edi
8010032a:	5d                   	pop    %ebp
8010032b:	c3                   	ret    
8010032c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    c = input.buf[input.r++ % INPUT_BUF];
80100330:	8d 50 01             	lea    0x1(%eax),%edx
80100333:	89 15 00 ff 10 80    	mov    %edx,0x8010ff00
80100339:	89 c2                	mov    %eax,%edx
8010033b:	83 e2 7f             	and    $0x7f,%edx
8010033e:	0f be 8a 80 fe 10 80 	movsbl -0x7fef0180(%edx),%ecx
    if(c == C('D')){  // EOF
80100345:	80 f9 04             	cmp    $0x4,%cl
80100348:	74 37                	je     80100381 <consoleread+0xf1>
    *dst++ = c;
8010034a:	83 c6 01             	add    $0x1,%esi
    --n;
8010034d:	83 eb 01             	sub    $0x1,%ebx
    *dst++ = c;
80100350:	88 4e ff             	mov    %cl,-0x1(%esi)
    if(c == '\n')
80100353:	83 f9 0a             	cmp    $0xa,%ecx
80100356:	0f 85 60 ff ff ff    	jne    801002bc <consoleread+0x2c>
  release(&cons.lock);
8010035c:	83 ec 0c             	sub    $0xc,%esp
8010035f:	68 20 ff 10 80       	push   $0x8010ff20
80100364:	e8 87 4d 00 00       	call   801050f0 <release>
  ilock(ip);
80100369:	58                   	pop    %eax
8010036a:	ff 75 08             	push   0x8(%ebp)
8010036d:	e8 ee 1a 00 00       	call   80101e60 <ilock>
  return target - n;
80100372:	89 f8                	mov    %edi,%eax
80100374:	83 c4 10             	add    $0x10,%esp
}
80100377:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return target - n;
8010037a:	29 d8                	sub    %ebx,%eax
}
8010037c:	5b                   	pop    %ebx
8010037d:	5e                   	pop    %esi
8010037e:	5f                   	pop    %edi
8010037f:	5d                   	pop    %ebp
80100380:	c3                   	ret    
      if(n < target){
80100381:	39 fb                	cmp    %edi,%ebx
80100383:	73 d7                	jae    8010035c <consoleread+0xcc>
        input.r--;
80100385:	a3 00 ff 10 80       	mov    %eax,0x8010ff00
8010038a:	eb d0                	jmp    8010035c <consoleread+0xcc>
8010038c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80100390 <panic>:
{
80100390:	f3 0f 1e fb          	endbr32 
80100394:	55                   	push   %ebp
80100395:	89 e5                	mov    %esp,%ebp
80100397:	56                   	push   %esi
80100398:	53                   	push   %ebx
80100399:	83 ec 30             	sub    $0x30,%esp
}

static inline void
cli(void)
{
  asm volatile("cli");
8010039c:	fa                   	cli    
  cons.locking = 0;
8010039d:	c7 05 54 ff 10 80 00 	movl   $0x0,0x8010ff54
801003a4:	00 00 00 
  getcallerpcs(&s, pcs);
801003a7:	8d 5d d0             	lea    -0x30(%ebp),%ebx
801003aa:	8d 75 f8             	lea    -0x8(%ebp),%esi
  cprintf("lapicid %d: panic: ", lapicid()); 
801003ad:	e8 5e 2c 00 00       	call   80103010 <lapicid>
801003b2:	83 ec 08             	sub    $0x8,%esp
801003b5:	50                   	push   %eax
801003b6:	68 6d 7f 10 80       	push   $0x80107f6d
801003bb:	e8 70 03 00 00       	call   80100730 <cprintf>
  cprintf(s);
801003c0:	58                   	pop    %eax
801003c1:	ff 75 08             	push   0x8(%ebp)
801003c4:	e8 67 03 00 00       	call   80100730 <cprintf>
  cprintf("\n");
801003c9:	c7 04 24 ff 89 10 80 	movl   $0x801089ff,(%esp)
801003d0:	e8 5b 03 00 00       	call   80100730 <cprintf>
  getcallerpcs(&s, pcs);
801003d5:	8d 45 08             	lea    0x8(%ebp),%eax
801003d8:	5a                   	pop    %edx
801003d9:	59                   	pop    %ecx
801003da:	53                   	push   %ebx
801003db:	50                   	push   %eax
801003dc:	e8 9f 4b 00 00       	call   80104f80 <getcallerpcs>
  for(i=0; i<10; i++)
801003e1:	83 c4 10             	add    $0x10,%esp
801003e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cprintf(" %p", pcs[i]);
801003e8:	83 ec 08             	sub    $0x8,%esp
801003eb:	ff 33                	push   (%ebx)
  for(i=0; i<10; i++)
801003ed:	83 c3 04             	add    $0x4,%ebx
    cprintf(" %p", pcs[i]);
801003f0:	68 81 7f 10 80       	push   $0x80107f81
801003f5:	e8 36 03 00 00       	call   80100730 <cprintf>
  for(i=0; i<10; i++)
801003fa:	83 c4 10             	add    $0x10,%esp
801003fd:	39 f3                	cmp    %esi,%ebx
801003ff:	75 e7                	jne    801003e8 <panic+0x58>
  panicked = 1; // freeze other CPU
80100401:	c7 05 58 ff 10 80 01 	movl   $0x1,0x8010ff58
80100408:	00 00 00 
  for(;;)
8010040b:	eb fe                	jmp    8010040b <panic+0x7b>
8010040d:	8d 76 00             	lea    0x0(%esi),%esi

80100410 <cgaputc>:
{
80100410:	55                   	push   %ebp
80100411:	89 e5                	mov    %esp,%ebp
80100413:	57                   	push   %edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100414:	bf d4 03 00 00       	mov    $0x3d4,%edi
80100419:	56                   	push   %esi
8010041a:	89 fa                	mov    %edi,%edx
8010041c:	89 c6                	mov    %eax,%esi
8010041e:	b8 0e 00 00 00       	mov    $0xe,%eax
80100423:	53                   	push   %ebx
80100424:	83 ec 1c             	sub    $0x1c,%esp
80100427:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100428:	bb d5 03 00 00       	mov    $0x3d5,%ebx
8010042d:	89 da                	mov    %ebx,%edx
8010042f:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
80100430:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100433:	89 fa                	mov    %edi,%edx
80100435:	c1 e0 08             	shl    $0x8,%eax
80100438:	89 c1                	mov    %eax,%ecx
8010043a:	b8 0f 00 00 00       	mov    $0xf,%eax
8010043f:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100440:	89 da                	mov    %ebx,%edx
80100442:	ec                   	in     (%dx),%al
  if (pos < end && c == BACKSPACE) {
80100443:	8b 1d 00 90 10 80    	mov    0x80109000,%ebx
  pos |= inb(CRTPORT+1);
80100449:	0f b6 c0             	movzbl %al,%eax
8010044c:	09 c8                	or     %ecx,%eax
  if (pos < end && c == BACKSPACE) {
8010044e:	39 c3                	cmp    %eax,%ebx
80100450:	7e 5e                	jle    801004b0 <cgaputc+0xa0>
80100452:	81 fe 00 01 00 00    	cmp    $0x100,%esi
80100458:	0f 84 22 01 00 00    	je     80100580 <cgaputc+0x170>
8010045e:	8d 8c 1b 00 80 0b 80 	lea    -0x7ff48000(%ebx,%ebx,1),%ecx
80100465:	8d bc 00 fe 7f 0b 80 	lea    -0x7ff48002(%eax,%eax,1),%edi
8010046c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      crt[i+1] = crt[i];
80100470:	0f b7 11             	movzwl (%ecx),%edx
    for (int i = end ; i >= pos ; i--) {
80100473:	83 e9 02             	sub    $0x2,%ecx
      crt[i+1] = crt[i];
80100476:	66 89 51 04          	mov    %dx,0x4(%ecx)
    for (int i = end ; i >= pos ; i--) {
8010047a:	39 cf                	cmp    %ecx,%edi
8010047c:	75 f2                	jne    80100470 <cgaputc+0x60>
    end += 1;
8010047e:	83 c3 01             	add    $0x1,%ebx
    edited_index += 1;
80100481:	83 05 0c ff 10 80 01 	addl   $0x1,0x8010ff0c
    end += 1;
80100488:	89 1d 00 90 10 80    	mov    %ebx,0x80109000
  if(c == '\n')
8010048e:	83 fe 0a             	cmp    $0xa,%esi
80100491:	75 2e                	jne    801004c1 <cgaputc+0xb1>
    pos += 80 - pos%80;
80100493:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
80100498:	f7 e2                	mul    %edx
8010049a:	c1 ea 06             	shr    $0x6,%edx
8010049d:	8d 04 92             	lea    (%edx,%edx,4),%eax
801004a0:	c1 e0 04             	shl    $0x4,%eax
801004a3:	8d 58 50             	lea    0x50(%eax),%ebx
801004a6:	eb 2e                	jmp    801004d6 <cgaputc+0xc6>
801004a8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801004af:	90                   	nop
  if(c == '\n')
801004b0:	83 fe 0a             	cmp    $0xa,%esi
801004b3:	74 de                	je     80100493 <cgaputc+0x83>
  else if(c == BACKSPACE){
801004b5:	81 fe 00 01 00 00    	cmp    $0x100,%esi
801004bb:	0f 84 f6 00 00 00    	je     801005b7 <cgaputc+0x1a7>
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
801004c1:	89 f1                	mov    %esi,%ecx
801004c3:	8d 58 01             	lea    0x1(%eax),%ebx
801004c6:	0f b6 f1             	movzbl %cl,%esi
801004c9:	66 81 ce 00 07       	or     $0x700,%si
801004ce:	66 89 b4 00 00 80 0b 	mov    %si,-0x7ff48000(%eax,%eax,1)
801004d5:	80 
  if(pos < 0 || pos > 25*80)
801004d6:	81 fb d0 07 00 00    	cmp    $0x7d0,%ebx
801004dc:	0f 8f f3 00 00 00    	jg     801005d5 <cgaputc+0x1c5>
  if((pos/80) >= 24){  // Scroll up.
801004e2:	81 fb 7f 07 00 00    	cmp    $0x77f,%ebx
801004e8:	7f 4e                	jg     80100538 <cgaputc+0x128>
  outb(CRTPORT+1, pos);
801004ea:	88 5d e7             	mov    %bl,-0x19(%ebp)
  outb(CRTPORT+1, pos>>8);
801004ed:	0f b6 ff             	movzbl %bh,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801004f0:	be d4 03 00 00       	mov    $0x3d4,%esi
801004f5:	b8 0e 00 00 00       	mov    $0xe,%eax
801004fa:	89 f2                	mov    %esi,%edx
801004fc:	ee                   	out    %al,(%dx)
801004fd:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
80100502:	89 f8                	mov    %edi,%eax
80100504:	89 ca                	mov    %ecx,%edx
80100506:	ee                   	out    %al,(%dx)
80100507:	b8 0f 00 00 00       	mov    $0xf,%eax
8010050c:	89 f2                	mov    %esi,%edx
8010050e:	ee                   	out    %al,(%dx)
8010050f:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
80100513:	89 ca                	mov    %ecx,%edx
80100515:	ee                   	out    %al,(%dx)
  if(pos >= end) crt[pos] = ' ' | 0x0700;
80100516:	39 1d 00 90 10 80    	cmp    %ebx,0x80109000
8010051c:	7f 0d                	jg     8010052b <cgaputc+0x11b>
8010051e:	b8 20 07 00 00       	mov    $0x720,%eax
80100523:	66 89 84 1b 00 80 0b 	mov    %ax,-0x7ff48000(%ebx,%ebx,1)
8010052a:	80 
}
8010052b:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010052e:	5b                   	pop    %ebx
8010052f:	5e                   	pop    %esi
80100530:	5f                   	pop    %edi
80100531:	5d                   	pop    %ebp
80100532:	c3                   	ret    
80100533:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100537:	90                   	nop
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100538:	83 ec 04             	sub    $0x4,%esp
    pos -= 80;
8010053b:	83 eb 50             	sub    $0x50,%ebx
  outb(CRTPORT+1, pos);
8010053e:	bf 07 00 00 00       	mov    $0x7,%edi
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100543:	68 60 0e 00 00       	push   $0xe60
80100548:	68 a0 80 0b 80       	push   $0x800b80a0
8010054d:	68 00 80 0b 80       	push   $0x800b8000
80100552:	e8 79 4d 00 00       	call   801052d0 <memmove>
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100557:	b8 80 07 00 00       	mov    $0x780,%eax
8010055c:	83 c4 0c             	add    $0xc,%esp
8010055f:	29 d8                	sub    %ebx,%eax
80100561:	01 c0                	add    %eax,%eax
80100563:	50                   	push   %eax
80100564:	8d 84 1b 00 80 0b 80 	lea    -0x7ff48000(%ebx,%ebx,1),%eax
8010056b:	6a 00                	push   $0x0
8010056d:	50                   	push   %eax
8010056e:	e8 bd 4c 00 00       	call   80105230 <memset>
  outb(CRTPORT+1, pos);
80100573:	88 5d e7             	mov    %bl,-0x19(%ebp)
80100576:	83 c4 10             	add    $0x10,%esp
80100579:	e9 72 ff ff ff       	jmp    801004f0 <cgaputc+0xe0>
8010057e:	66 90                	xchg   %ax,%ax
80100580:	8d 94 00 00 80 0b 80 	lea    -0x7ff48000(%eax,%eax,1),%edx
80100587:	8d b4 1b 00 80 0b 80 	lea    -0x7ff48000(%ebx,%ebx,1),%esi
8010058e:	66 90                	xchg   %ax,%ax
      crt[i-1] = crt[i];
80100590:	0f b7 0a             	movzwl (%edx),%ecx
    for (int i = pos ; i < end ; i++) {
80100593:	83 c2 02             	add    $0x2,%edx
      crt[i-1] = crt[i];
80100596:	66 89 4a fc          	mov    %cx,-0x4(%edx)
    for (int i = pos ; i < end ; i++) {
8010059a:	39 d6                	cmp    %edx,%esi
8010059c:	75 f2                	jne    80100590 <cgaputc+0x180>
    end -= 1;
8010059e:	83 eb 01             	sub    $0x1,%ebx
    crt[end] = ' ' | 0x0700;
801005a1:	ba 20 07 00 00       	mov    $0x720,%edx
    edited_index -= 1;
801005a6:	83 2d 0c ff 10 80 01 	subl   $0x1,0x8010ff0c
    end -= 1;
801005ad:	89 1d 00 90 10 80    	mov    %ebx,0x80109000
    crt[end] = ' ' | 0x0700;
801005b3:	66 89 56 fe          	mov    %dx,-0x2(%esi)
    if(pos > 0) --pos;
801005b7:	85 c0                	test   %eax,%eax
801005b9:	74 0d                	je     801005c8 <cgaputc+0x1b8>
801005bb:	8d 58 ff             	lea    -0x1(%eax),%ebx
801005be:	e9 13 ff ff ff       	jmp    801004d6 <cgaputc+0xc6>
801005c3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801005c7:	90                   	nop
801005c8:	c6 45 e7 00          	movb   $0x0,-0x19(%ebp)
801005cc:	31 db                	xor    %ebx,%ebx
801005ce:	31 ff                	xor    %edi,%edi
801005d0:	e9 1b ff ff ff       	jmp    801004f0 <cgaputc+0xe0>
    panic("pos under/overflow");
801005d5:	83 ec 0c             	sub    $0xc,%esp
801005d8:	68 85 7f 10 80       	push   $0x80107f85
801005dd:	e8 ae fd ff ff       	call   80100390 <panic>
801005e2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801005e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801005f0 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
801005f0:	f3 0f 1e fb          	endbr32 
801005f4:	55                   	push   %ebp
801005f5:	89 e5                	mov    %esp,%ebp
801005f7:	57                   	push   %edi
801005f8:	56                   	push   %esi
801005f9:	53                   	push   %ebx
801005fa:	83 ec 28             	sub    $0x28,%esp
  int i;

  iunlock(ip);
801005fd:	ff 75 08             	push   0x8(%ebp)
{
80100600:	8b 75 10             	mov    0x10(%ebp),%esi
  iunlock(ip);
80100603:	e8 38 19 00 00       	call   80101f40 <iunlock>
  acquire(&cons.lock);
80100608:	c7 04 24 20 ff 10 80 	movl   $0x8010ff20,(%esp)
8010060f:	e8 4c 4b 00 00       	call   80105160 <acquire>
  for(i = 0; i < n; i++)
80100614:	83 c4 10             	add    $0x10,%esp
80100617:	85 f6                	test   %esi,%esi
80100619:	7e 36                	jle    80100651 <consolewrite+0x61>
8010061b:	8b 5d 0c             	mov    0xc(%ebp),%ebx
8010061e:	8d 3c 33             	lea    (%ebx,%esi,1),%edi
  if(panicked){
80100621:	8b 15 58 ff 10 80    	mov    0x8010ff58,%edx
80100627:	85 d2                	test   %edx,%edx
80100629:	74 05                	je     80100630 <consolewrite+0x40>
  asm volatile("cli");
8010062b:	fa                   	cli    
    for(;;)
8010062c:	eb fe                	jmp    8010062c <consolewrite+0x3c>
8010062e:	66 90                	xchg   %ax,%ax
    consputc(buf[i] & 0xff);
80100630:	0f b6 03             	movzbl (%ebx),%eax
    uartputc(c);
80100633:	83 ec 0c             	sub    $0xc,%esp
  for(i = 0; i < n; i++)
80100636:	83 c3 01             	add    $0x1,%ebx
    uartputc(c);
80100639:	50                   	push   %eax
8010063a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010063d:	e8 fe 63 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100642:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100645:	e8 c6 fd ff ff       	call   80100410 <cgaputc>
  for(i = 0; i < n; i++)
8010064a:	83 c4 10             	add    $0x10,%esp
8010064d:	39 df                	cmp    %ebx,%edi
8010064f:	75 d0                	jne    80100621 <consolewrite+0x31>
  release(&cons.lock);
80100651:	83 ec 0c             	sub    $0xc,%esp
80100654:	68 20 ff 10 80       	push   $0x8010ff20
80100659:	e8 92 4a 00 00       	call   801050f0 <release>
  ilock(ip);
8010065e:	58                   	pop    %eax
8010065f:	ff 75 08             	push   0x8(%ebp)
80100662:	e8 f9 17 00 00       	call   80101e60 <ilock>

  return n;
}
80100667:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010066a:	89 f0                	mov    %esi,%eax
8010066c:	5b                   	pop    %ebx
8010066d:	5e                   	pop    %esi
8010066e:	5f                   	pop    %edi
8010066f:	5d                   	pop    %ebp
80100670:	c3                   	ret    
80100671:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100678:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010067f:	90                   	nop

80100680 <printint>:
{
80100680:	55                   	push   %ebp
80100681:	89 e5                	mov    %esp,%ebp
80100683:	57                   	push   %edi
80100684:	56                   	push   %esi
80100685:	53                   	push   %ebx
80100686:	83 ec 2c             	sub    $0x2c,%esp
80100689:	89 55 d4             	mov    %edx,-0x2c(%ebp)
8010068c:	89 4d d0             	mov    %ecx,-0x30(%ebp)
  if(sign && (sign = xx < 0))
8010068f:	85 c9                	test   %ecx,%ecx
80100691:	74 04                	je     80100697 <printint+0x17>
80100693:	85 c0                	test   %eax,%eax
80100695:	78 7e                	js     80100715 <printint+0x95>
    x = xx;
80100697:	c7 45 d0 00 00 00 00 	movl   $0x0,-0x30(%ebp)
8010069e:	89 c1                	mov    %eax,%ecx
  i = 0;
801006a0:	31 db                	xor    %ebx,%ebx
801006a2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    buf[i++] = digits[x % base];
801006a8:	89 c8                	mov    %ecx,%eax
801006aa:	31 d2                	xor    %edx,%edx
801006ac:	89 de                	mov    %ebx,%esi
801006ae:	89 cf                	mov    %ecx,%edi
801006b0:	f7 75 d4             	divl   -0x2c(%ebp)
801006b3:	8d 5b 01             	lea    0x1(%ebx),%ebx
801006b6:	0f b6 92 08 80 10 80 	movzbl -0x7fef7ff8(%edx),%edx
  }while((x /= base) != 0);
801006bd:	89 c1                	mov    %eax,%ecx
    buf[i++] = digits[x % base];
801006bf:	88 54 1d d7          	mov    %dl,-0x29(%ebp,%ebx,1)
  }while((x /= base) != 0);
801006c3:	3b 7d d4             	cmp    -0x2c(%ebp),%edi
801006c6:	73 e0                	jae    801006a8 <printint+0x28>
  if(sign)
801006c8:	8b 4d d0             	mov    -0x30(%ebp),%ecx
801006cb:	85 c9                	test   %ecx,%ecx
801006cd:	74 0c                	je     801006db <printint+0x5b>
    buf[i++] = '-';
801006cf:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
801006d4:	89 de                	mov    %ebx,%esi
    buf[i++] = '-';
801006d6:	ba 2d 00 00 00       	mov    $0x2d,%edx
  while(--i >= 0)
801006db:	8d 5c 35 d7          	lea    -0x29(%ebp,%esi,1),%ebx
  if(panicked){
801006df:	a1 58 ff 10 80       	mov    0x8010ff58,%eax
801006e4:	85 c0                	test   %eax,%eax
801006e6:	74 08                	je     801006f0 <printint+0x70>
801006e8:	fa                   	cli    
    for(;;)
801006e9:	eb fe                	jmp    801006e9 <printint+0x69>
801006eb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801006ef:	90                   	nop
    consputc(buf[i]);
801006f0:	0f be f2             	movsbl %dl,%esi
    uartputc(c);
801006f3:	83 ec 0c             	sub    $0xc,%esp
801006f6:	56                   	push   %esi
801006f7:	e8 44 63 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
801006fc:	89 f0                	mov    %esi,%eax
801006fe:	e8 0d fd ff ff       	call   80100410 <cgaputc>
  while(--i >= 0)
80100703:	8d 45 d7             	lea    -0x29(%ebp),%eax
80100706:	83 c4 10             	add    $0x10,%esp
80100709:	39 c3                	cmp    %eax,%ebx
8010070b:	74 0e                	je     8010071b <printint+0x9b>
    consputc(buf[i]);
8010070d:	0f b6 13             	movzbl (%ebx),%edx
80100710:	83 eb 01             	sub    $0x1,%ebx
80100713:	eb ca                	jmp    801006df <printint+0x5f>
    x = -xx;
80100715:	f7 d8                	neg    %eax
80100717:	89 c1                	mov    %eax,%ecx
80100719:	eb 85                	jmp    801006a0 <printint+0x20>
}
8010071b:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010071e:	5b                   	pop    %ebx
8010071f:	5e                   	pop    %esi
80100720:	5f                   	pop    %edi
80100721:	5d                   	pop    %ebp
80100722:	c3                   	ret    
80100723:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010072a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80100730 <cprintf>:
{
80100730:	f3 0f 1e fb          	endbr32 
80100734:	55                   	push   %ebp
80100735:	89 e5                	mov    %esp,%ebp
80100737:	57                   	push   %edi
80100738:	56                   	push   %esi
80100739:	53                   	push   %ebx
8010073a:	83 ec 1c             	sub    $0x1c,%esp
  locking = cons.locking;
8010073d:	a1 54 ff 10 80       	mov    0x8010ff54,%eax
80100742:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(locking)
80100745:	85 c0                	test   %eax,%eax
80100747:	0f 85 33 01 00 00    	jne    80100880 <cprintf+0x150>
  if (fmt == 0)
8010074d:	8b 75 08             	mov    0x8(%ebp),%esi
80100750:	85 f6                	test   %esi,%esi
80100752:	0f 84 3b 02 00 00    	je     80100993 <cprintf+0x263>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100758:	0f b6 06             	movzbl (%esi),%eax
  argp = (uint*)(void*)(&fmt + 1);
8010075b:	8d 7d 0c             	lea    0xc(%ebp),%edi
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010075e:	31 db                	xor    %ebx,%ebx
80100760:	85 c0                	test   %eax,%eax
80100762:	74 56                	je     801007ba <cprintf+0x8a>
    if(c != '%'){
80100764:	83 f8 25             	cmp    $0x25,%eax
80100767:	0f 85 d3 00 00 00    	jne    80100840 <cprintf+0x110>
    c = fmt[++i] & 0xff;
8010076d:	83 c3 01             	add    $0x1,%ebx
80100770:	0f b6 14 1e          	movzbl (%esi,%ebx,1),%edx
    if(c == 0)
80100774:	85 d2                	test   %edx,%edx
80100776:	74 42                	je     801007ba <cprintf+0x8a>
    switch(c){
80100778:	83 fa 70             	cmp    $0x70,%edx
8010077b:	0f 84 90 00 00 00    	je     80100811 <cprintf+0xe1>
80100781:	7f 4d                	jg     801007d0 <cprintf+0xa0>
80100783:	83 fa 25             	cmp    $0x25,%edx
80100786:	0f 84 44 01 00 00    	je     801008d0 <cprintf+0x1a0>
8010078c:	83 fa 64             	cmp    $0x64,%edx
8010078f:	0f 85 00 01 00 00    	jne    80100895 <cprintf+0x165>
      printint(*argp++, 10, 1);
80100795:	8d 47 04             	lea    0x4(%edi),%eax
80100798:	b9 01 00 00 00       	mov    $0x1,%ecx
8010079d:	ba 0a 00 00 00       	mov    $0xa,%edx
801007a2:	89 45 e0             	mov    %eax,-0x20(%ebp)
801007a5:	8b 07                	mov    (%edi),%eax
801007a7:	e8 d4 fe ff ff       	call   80100680 <printint>
801007ac:	8b 7d e0             	mov    -0x20(%ebp),%edi
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801007af:	83 c3 01             	add    $0x1,%ebx
801007b2:	0f b6 04 1e          	movzbl (%esi,%ebx,1),%eax
801007b6:	85 c0                	test   %eax,%eax
801007b8:	75 aa                	jne    80100764 <cprintf+0x34>
  if(locking)
801007ba:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801007bd:	85 c0                	test   %eax,%eax
801007bf:	0f 85 b1 01 00 00    	jne    80100976 <cprintf+0x246>
}
801007c5:	8d 65 f4             	lea    -0xc(%ebp),%esp
801007c8:	5b                   	pop    %ebx
801007c9:	5e                   	pop    %esi
801007ca:	5f                   	pop    %edi
801007cb:	5d                   	pop    %ebp
801007cc:	c3                   	ret    
801007cd:	8d 76 00             	lea    0x0(%esi),%esi
    switch(c){
801007d0:	83 fa 73             	cmp    $0x73,%edx
801007d3:	75 33                	jne    80100808 <cprintf+0xd8>
      if((s = (char*)*argp++) == 0)
801007d5:	8d 47 04             	lea    0x4(%edi),%eax
801007d8:	8b 3f                	mov    (%edi),%edi
801007da:	89 45 e0             	mov    %eax,-0x20(%ebp)
801007dd:	85 ff                	test   %edi,%edi
801007df:	0f 85 33 01 00 00    	jne    80100918 <cprintf+0x1e8>
        s = "(null)";
801007e5:	bf 98 7f 10 80       	mov    $0x80107f98,%edi
      for(; *s; s++)
801007ea:	89 5d dc             	mov    %ebx,-0x24(%ebp)
801007ed:	b8 28 00 00 00       	mov    $0x28,%eax
801007f2:	89 fb                	mov    %edi,%ebx
  if(panicked){
801007f4:	8b 15 58 ff 10 80    	mov    0x8010ff58,%edx
801007fa:	85 d2                	test   %edx,%edx
801007fc:	0f 84 27 01 00 00    	je     80100929 <cprintf+0x1f9>
80100802:	fa                   	cli    
    for(;;)
80100803:	eb fe                	jmp    80100803 <cprintf+0xd3>
80100805:	8d 76 00             	lea    0x0(%esi),%esi
    switch(c){
80100808:	83 fa 78             	cmp    $0x78,%edx
8010080b:	0f 85 84 00 00 00    	jne    80100895 <cprintf+0x165>
      printint(*argp++, 16, 0);
80100811:	8d 47 04             	lea    0x4(%edi),%eax
80100814:	31 c9                	xor    %ecx,%ecx
80100816:	ba 10 00 00 00       	mov    $0x10,%edx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010081b:	83 c3 01             	add    $0x1,%ebx
      printint(*argp++, 16, 0);
8010081e:	89 45 e0             	mov    %eax,-0x20(%ebp)
80100821:	8b 07                	mov    (%edi),%eax
80100823:	e8 58 fe ff ff       	call   80100680 <printint>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100828:	0f b6 04 1e          	movzbl (%esi,%ebx,1),%eax
      printint(*argp++, 16, 0);
8010082c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010082f:	85 c0                	test   %eax,%eax
80100831:	0f 85 2d ff ff ff    	jne    80100764 <cprintf+0x34>
80100837:	eb 81                	jmp    801007ba <cprintf+0x8a>
80100839:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  if(panicked){
80100840:	8b 0d 58 ff 10 80    	mov    0x8010ff58,%ecx
80100846:	85 c9                	test   %ecx,%ecx
80100848:	74 06                	je     80100850 <cprintf+0x120>
8010084a:	fa                   	cli    
    for(;;)
8010084b:	eb fe                	jmp    8010084b <cprintf+0x11b>
8010084d:	8d 76 00             	lea    0x0(%esi),%esi
    uartputc(c);
80100850:	83 ec 0c             	sub    $0xc,%esp
80100853:	89 45 e0             	mov    %eax,-0x20(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100856:	83 c3 01             	add    $0x1,%ebx
    uartputc(c);
80100859:	50                   	push   %eax
8010085a:	e8 e1 61 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
8010085f:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100862:	e8 a9 fb ff ff       	call   80100410 <cgaputc>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100867:	0f b6 04 1e          	movzbl (%esi,%ebx,1),%eax
      continue;
8010086b:	83 c4 10             	add    $0x10,%esp
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010086e:	85 c0                	test   %eax,%eax
80100870:	0f 85 ee fe ff ff    	jne    80100764 <cprintf+0x34>
80100876:	e9 3f ff ff ff       	jmp    801007ba <cprintf+0x8a>
8010087b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010087f:	90                   	nop
    acquire(&cons.lock);
80100880:	83 ec 0c             	sub    $0xc,%esp
80100883:	68 20 ff 10 80       	push   $0x8010ff20
80100888:	e8 d3 48 00 00       	call   80105160 <acquire>
8010088d:	83 c4 10             	add    $0x10,%esp
80100890:	e9 b8 fe ff ff       	jmp    8010074d <cprintf+0x1d>
  if(panicked){
80100895:	8b 0d 58 ff 10 80    	mov    0x8010ff58,%ecx
8010089b:	85 c9                	test   %ecx,%ecx
8010089d:	75 71                	jne    80100910 <cprintf+0x1e0>
    uartputc(c);
8010089f:	83 ec 0c             	sub    $0xc,%esp
801008a2:	89 55 e0             	mov    %edx,-0x20(%ebp)
801008a5:	6a 25                	push   $0x25
801008a7:	e8 94 61 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
801008ac:	b8 25 00 00 00       	mov    $0x25,%eax
801008b1:	e8 5a fb ff ff       	call   80100410 <cgaputc>
  if(panicked){
801008b6:	8b 15 58 ff 10 80    	mov    0x8010ff58,%edx
801008bc:	83 c4 10             	add    $0x10,%esp
801008bf:	85 d2                	test   %edx,%edx
801008c1:	8b 55 e0             	mov    -0x20(%ebp),%edx
801008c4:	0f 84 8e 00 00 00    	je     80100958 <cprintf+0x228>
801008ca:	fa                   	cli    
    for(;;)
801008cb:	eb fe                	jmp    801008cb <cprintf+0x19b>
801008cd:	8d 76 00             	lea    0x0(%esi),%esi
  if(panicked){
801008d0:	a1 58 ff 10 80       	mov    0x8010ff58,%eax
801008d5:	85 c0                	test   %eax,%eax
801008d7:	74 07                	je     801008e0 <cprintf+0x1b0>
801008d9:	fa                   	cli    
    for(;;)
801008da:	eb fe                	jmp    801008da <cprintf+0x1aa>
801008dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    uartputc(c);
801008e0:	83 ec 0c             	sub    $0xc,%esp
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801008e3:	83 c3 01             	add    $0x1,%ebx
    uartputc(c);
801008e6:	6a 25                	push   $0x25
801008e8:	e8 53 61 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
801008ed:	b8 25 00 00 00       	mov    $0x25,%eax
801008f2:	e8 19 fb ff ff       	call   80100410 <cgaputc>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801008f7:	0f b6 04 1e          	movzbl (%esi,%ebx,1),%eax
}
801008fb:	83 c4 10             	add    $0x10,%esp
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801008fe:	85 c0                	test   %eax,%eax
80100900:	0f 85 5e fe ff ff    	jne    80100764 <cprintf+0x34>
80100906:	e9 af fe ff ff       	jmp    801007ba <cprintf+0x8a>
8010090b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010090f:	90                   	nop
80100910:	fa                   	cli    
    for(;;)
80100911:	eb fe                	jmp    80100911 <cprintf+0x1e1>
80100913:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100917:	90                   	nop
      for(; *s; s++)
80100918:	0f b6 07             	movzbl (%edi),%eax
8010091b:	84 c0                	test   %al,%al
8010091d:	74 6c                	je     8010098b <cprintf+0x25b>
8010091f:	89 5d dc             	mov    %ebx,-0x24(%ebp)
80100922:	89 fb                	mov    %edi,%ebx
80100924:	e9 cb fe ff ff       	jmp    801007f4 <cprintf+0xc4>
    uartputc(c);
80100929:	83 ec 0c             	sub    $0xc,%esp
        consputc(*s);
8010092c:	0f be f8             	movsbl %al,%edi
      for(; *s; s++)
8010092f:	83 c3 01             	add    $0x1,%ebx
    uartputc(c);
80100932:	57                   	push   %edi
80100933:	e8 08 61 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100938:	89 f8                	mov    %edi,%eax
8010093a:	e8 d1 fa ff ff       	call   80100410 <cgaputc>
      for(; *s; s++)
8010093f:	0f b6 03             	movzbl (%ebx),%eax
80100942:	83 c4 10             	add    $0x10,%esp
80100945:	84 c0                	test   %al,%al
80100947:	0f 85 a7 fe ff ff    	jne    801007f4 <cprintf+0xc4>
      if((s = (char*)*argp++) == 0)
8010094d:	8b 5d dc             	mov    -0x24(%ebp),%ebx
80100950:	8b 7d e0             	mov    -0x20(%ebp),%edi
80100953:	e9 57 fe ff ff       	jmp    801007af <cprintf+0x7f>
    uartputc(c);
80100958:	83 ec 0c             	sub    $0xc,%esp
8010095b:	89 55 e0             	mov    %edx,-0x20(%ebp)
8010095e:	52                   	push   %edx
8010095f:	e8 dc 60 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100964:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100967:	89 d0                	mov    %edx,%eax
80100969:	e8 a2 fa ff ff       	call   80100410 <cgaputc>
}
8010096e:	83 c4 10             	add    $0x10,%esp
80100971:	e9 39 fe ff ff       	jmp    801007af <cprintf+0x7f>
    release(&cons.lock);
80100976:	83 ec 0c             	sub    $0xc,%esp
80100979:	68 20 ff 10 80       	push   $0x8010ff20
8010097e:	e8 6d 47 00 00       	call   801050f0 <release>
80100983:	83 c4 10             	add    $0x10,%esp
}
80100986:	e9 3a fe ff ff       	jmp    801007c5 <cprintf+0x95>
      if((s = (char*)*argp++) == 0)
8010098b:	8b 7d e0             	mov    -0x20(%ebp),%edi
8010098e:	e9 1c fe ff ff       	jmp    801007af <cprintf+0x7f>
    panic("null fmt");
80100993:	83 ec 0c             	sub    $0xc,%esp
80100996:	68 9f 7f 10 80       	push   $0x80107f9f
8010099b:	e8 f0 f9 ff ff       	call   80100390 <panic>

801009a0 <swap>:
void swap(char *a , char *b){
801009a0:	f3 0f 1e fb          	endbr32 
801009a4:	55                   	push   %ebp
801009a5:	89 e5                	mov    %esp,%ebp
801009a7:	53                   	push   %ebx
801009a8:	8b 55 08             	mov    0x8(%ebp),%edx
801009ab:	8b 45 0c             	mov    0xc(%ebp),%eax
  temp = *a;
801009ae:	0f b6 0a             	movzbl (%edx),%ecx
  *a = *b;
801009b1:	0f b6 18             	movzbl (%eax),%ebx
801009b4:	88 1a                	mov    %bl,(%edx)
  *b = temp;
801009b6:	88 08                	mov    %cl,(%eax)
}
801009b8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801009bb:	c9                   	leave  
801009bc:	c3                   	ret    
801009bd:	8d 76 00             	lea    0x0(%esi),%esi

801009c0 <upArrow>:
void upArrow() {
801009c0:	f3 0f 1e fb          	endbr32 
801009c4:	55                   	push   %ebp
  uint a = input.w -1;
801009c5:	8b 15 04 ff 10 80    	mov    0x8010ff04,%edx
void upArrow() {
801009cb:	89 e5                	mov    %esp,%ebp
801009cd:	56                   	push   %esi
801009ce:	53                   	push   %ebx
  uint a = input.w -1;
801009cf:	8d 5a ff             	lea    -0x1(%edx),%ebx
  if(a>input.w){
801009d2:	39 da                	cmp    %ebx,%edx
801009d4:	0f 82 db 00 00 00    	jb     80100ab5 <upArrow+0xf5>
  while(input.e != input.w ){
801009da:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
801009df:	39 c2                	cmp    %eax,%edx
801009e1:	0f 84 d5 00 00 00    	je     80100abc <upArrow+0xfc>
  if(panicked){
801009e7:	8b 15 58 ff 10 80    	mov    0x8010ff58,%edx
    input.e--;
801009ed:	83 e8 01             	sub    $0x1,%eax
801009f0:	a3 08 ff 10 80       	mov    %eax,0x8010ff08
  if(panicked){
801009f5:	85 d2                	test   %edx,%edx
801009f7:	74 07                	je     80100a00 <upArrow+0x40>
801009f9:	fa                   	cli    
    for(;;)
801009fa:	eb fe                	jmp    801009fa <upArrow+0x3a>
801009fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100a00:	83 ec 0c             	sub    $0xc,%esp
80100a03:	6a 08                	push   $0x8
80100a05:	e8 36 60 00 00       	call   80106a40 <uartputc>
80100a0a:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100a11:	e8 2a 60 00 00       	call   80106a40 <uartputc>
80100a16:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100a1d:	e8 1e 60 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100a22:	b8 00 01 00 00       	mov    $0x100,%eax
80100a27:	e8 e4 f9 ff ff       	call   80100410 <cgaputc>
  while(input.e != input.w ){
80100a2c:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
80100a31:	83 c4 10             	add    $0x10,%esp
80100a34:	3b 05 04 ff 10 80    	cmp    0x8010ff04,%eax
80100a3a:	75 ab                	jne    801009e7 <upArrow+0x27>
  while(a>=0 && a<(input.w-1)){
80100a3c:	8d 70 ff             	lea    -0x1(%eax),%esi
80100a3f:	eb 23                	jmp    80100a64 <upArrow+0xa4>
80100a41:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  while(a>0 && input.buf[(a-1)%INPUT_BUF]!='\n' && 
80100a48:	8d 53 ff             	lea    -0x1(%ebx),%edx
80100a4b:	89 d1                	mov    %edx,%ecx
80100a4d:	83 e1 7f             	and    $0x7f,%ecx
80100a50:	80 b9 80 fe 10 80 0a 	cmpb   $0xa,-0x7fef0180(%ecx)
80100a57:	74 0f                	je     80100a68 <upArrow+0xa8>
          (a%INPUT_BUF)!=(input.w%INPUT_BUF)){
80100a59:	89 d9                	mov    %ebx,%ecx
80100a5b:	31 c1                	xor    %eax,%ecx
  while(a>0 && input.buf[(a-1)%INPUT_BUF]!='\n' && 
80100a5d:	83 e1 7f             	and    $0x7f,%ecx
80100a60:	74 06                	je     80100a68 <upArrow+0xa8>
    a--;
80100a62:	89 d3                	mov    %edx,%ebx
  while(a>0 && input.buf[(a-1)%INPUT_BUF]!='\n' && 
80100a64:	85 db                	test   %ebx,%ebx
80100a66:	75 e0                	jne    80100a48 <upArrow+0x88>
  while(a>=0 && a<(input.w-1)){
80100a68:	39 de                	cmp    %ebx,%esi
80100a6a:	76 49                	jbe    80100ab5 <upArrow+0xf5>
  if(panicked){
80100a6c:	a1 58 ff 10 80       	mov    0x8010ff58,%eax
80100a71:	85 c0                	test   %eax,%eax
80100a73:	74 0b                	je     80100a80 <upArrow+0xc0>
80100a75:	fa                   	cli    
    for(;;)
80100a76:	eb fe                	jmp    80100a76 <upArrow+0xb6>
80100a78:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100a7f:	90                   	nop
    consputc(input.buf[a%INPUT_BUF]);
80100a80:	89 d8                	mov    %ebx,%eax
    uartputc(c);
80100a82:	83 ec 0c             	sub    $0xc,%esp
    a++;
80100a85:	83 c3 01             	add    $0x1,%ebx
    consputc(input.buf[a%INPUT_BUF]);
80100a88:	83 e0 7f             	and    $0x7f,%eax
80100a8b:	0f be b0 80 fe 10 80 	movsbl -0x7fef0180(%eax),%esi
    uartputc(c);
80100a92:	56                   	push   %esi
80100a93:	e8 a8 5f 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100a98:	89 f0                	mov    %esi,%eax
80100a9a:	e8 71 f9 ff ff       	call   80100410 <cgaputc>
  while(a>=0 && a<(input.w-1)){
80100a9f:	a1 04 ff 10 80       	mov    0x8010ff04,%eax
    input.e++;
80100aa4:	83 05 08 ff 10 80 01 	addl   $0x1,0x8010ff08
  while(a>=0 && a<(input.w-1)){
80100aab:	83 c4 10             	add    $0x10,%esp
80100aae:	83 e8 01             	sub    $0x1,%eax
80100ab1:	39 d8                	cmp    %ebx,%eax
80100ab3:	77 b7                	ja     80100a6c <upArrow+0xac>
}
80100ab5:	8d 65 f8             	lea    -0x8(%ebp),%esp
80100ab8:	5b                   	pop    %ebx
80100ab9:	5e                   	pop    %esi
80100aba:	5d                   	pop    %ebp
80100abb:	c3                   	ret    
  while(input.e != input.w ){
80100abc:	89 de                	mov    %ebx,%esi
80100abe:	eb a4                	jmp    80100a64 <upArrow+0xa4>

80100ac0 <consoleintr>:
{
80100ac0:	f3 0f 1e fb          	endbr32 
80100ac4:	55                   	push   %ebp
80100ac5:	89 e5                	mov    %esp,%ebp
80100ac7:	57                   	push   %edi
80100ac8:	56                   	push   %esi
80100ac9:	53                   	push   %ebx
80100aca:	83 ec 28             	sub    $0x28,%esp
80100acd:	8b 7d 08             	mov    0x8(%ebp),%edi
  acquire(&cons.lock);
80100ad0:	68 20 ff 10 80       	push   $0x8010ff20
80100ad5:	e8 86 46 00 00       	call   80105160 <acquire>
  int c, doprocdump = 0, pos;
80100ada:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  while((c = getc()) >= 0){
80100ae1:	83 c4 10             	add    $0x10,%esp
80100ae4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100ae8:	ff d7                	call   *%edi
80100aea:	85 c0                	test   %eax,%eax
80100aec:	0f 88 e6 01 00 00    	js     80100cd8 <consoleintr+0x218>
    switch(c){
80100af2:	83 f8 15             	cmp    $0x15,%eax
80100af5:	7f 19                	jg     80100b10 <consoleintr+0x50>
80100af7:	85 c0                	test   %eax,%eax
80100af9:	74 ed                	je     80100ae8 <consoleintr+0x28>
80100afb:	83 f8 15             	cmp    $0x15,%eax
80100afe:	0f 87 bd 04 00 00    	ja     80100fc1 <consoleintr+0x501>
80100b04:	3e ff 24 85 b0 7f 10 	notrack jmp *-0x7fef8050(,%eax,4)
80100b0b:	80 
80100b0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100b10:	3d e2 00 00 00       	cmp    $0xe2,%eax
80100b15:	0f 84 e5 02 00 00    	je     80100e00 <consoleintr+0x340>
80100b1b:	0f 8e c7 00 00 00    	jle    80100be8 <consoleintr+0x128>
80100b21:	8d 90 1d ff ff ff    	lea    -0xe3(%eax),%edx
80100b27:	83 fa 02             	cmp    $0x2,%edx
80100b2a:	76 bc                	jbe    80100ae8 <consoleintr+0x28>
      if(c != 0 && input.e-input.r < INPUT_BUF){
80100b2c:	8b 15 08 ff 10 80    	mov    0x8010ff08,%edx
80100b32:	89 d1                	mov    %edx,%ecx
80100b34:	2b 0d 00 ff 10 80    	sub    0x8010ff00,%ecx
80100b3a:	83 f9 7f             	cmp    $0x7f,%ecx
80100b3d:	77 a9                	ja     80100ae8 <consoleintr+0x28>
        input.buf[input.e++ % INPUT_BUF] = c;
80100b3f:	89 d1                	mov    %edx,%ecx
  if(panicked){
80100b41:	8b 35 58 ff 10 80    	mov    0x8010ff58,%esi
        if(make_upper == 1){
80100b47:	8b 1d 10 ff 10 80    	mov    0x8010ff10,%ebx
        input.buf[input.e++ % INPUT_BUF] = c;
80100b4d:	83 c2 01             	add    $0x1,%edx
80100b50:	83 e1 7f             	and    $0x7f,%ecx
        c = (c == '\r') ? '\n' : c;
80100b53:	83 f8 0d             	cmp    $0xd,%eax
80100b56:	0f 84 cc 04 00 00    	je     80101028 <consoleintr+0x568>
        if(make_upper == 1){
80100b5c:	83 fb 01             	cmp    $0x1,%ebx
80100b5f:	0f 84 86 04 00 00    	je     80100feb <consoleintr+0x52b>
        input.buf[input.e++ % INPUT_BUF] = c;
80100b65:	89 c3                	mov    %eax,%ebx
80100b67:	89 15 08 ff 10 80    	mov    %edx,0x8010ff08
80100b6d:	88 99 80 fe 10 80    	mov    %bl,-0x7fef0180(%ecx)
  if(panicked){
80100b73:	85 f6                	test   %esi,%esi
80100b75:	0f 85 a7 04 00 00    	jne    80101022 <consoleintr+0x562>
  if(c == BACKSPACE){
80100b7b:	3d 00 01 00 00       	cmp    $0x100,%eax
80100b80:	0f 85 dd 04 00 00    	jne    80101063 <consoleintr+0x5a3>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100b86:	83 ec 0c             	sub    $0xc,%esp
80100b89:	6a 08                	push   $0x8
80100b8b:	e8 b0 5e 00 00       	call   80106a40 <uartputc>
80100b90:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100b97:	e8 a4 5e 00 00       	call   80106a40 <uartputc>
80100b9c:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100ba3:	e8 98 5e 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100ba8:	b8 00 01 00 00       	mov    $0x100,%eax
80100bad:	e8 5e f8 ff ff       	call   80100410 <cgaputc>
80100bb2:	83 c4 10             	add    $0x10,%esp
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100bb5:	a1 00 ff 10 80       	mov    0x8010ff00,%eax
80100bba:	83 e8 80             	sub    $0xffffff80,%eax
80100bbd:	39 05 08 ff 10 80    	cmp    %eax,0x8010ff08
80100bc3:	0f 85 1f ff ff ff    	jne    80100ae8 <consoleintr+0x28>
          wakeup(&input.r);
80100bc9:	83 ec 0c             	sub    $0xc,%esp
          input.w = input.e;
80100bcc:	a3 04 ff 10 80       	mov    %eax,0x8010ff04
          wakeup(&input.r);
80100bd1:	68 00 ff 10 80       	push   $0x8010ff00
80100bd6:	e8 45 3f 00 00       	call   80104b20 <wakeup>
80100bdb:	83 c4 10             	add    $0x10,%esp
80100bde:	e9 05 ff ff ff       	jmp    80100ae8 <consoleintr+0x28>
80100be3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100be7:	90                   	nop
    switch(c){
80100be8:	83 f8 7f             	cmp    $0x7f,%eax
80100beb:	0f 85 3b ff ff ff    	jne    80100b2c <consoleintr+0x6c>
      if(input.e != input.w){
80100bf1:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
80100bf6:	3b 05 04 ff 10 80    	cmp    0x8010ff04,%eax
80100bfc:	0f 84 e6 fe ff ff    	je     80100ae8 <consoleintr+0x28>
        input.e--;
80100c02:	83 e8 01             	sub    $0x1,%eax
80100c05:	a3 08 ff 10 80       	mov    %eax,0x8010ff08
  if(panicked){
80100c0a:	a1 58 ff 10 80       	mov    0x8010ff58,%eax
80100c0f:	85 c0                	test   %eax,%eax
80100c11:	0f 84 19 03 00 00    	je     80100f30 <consoleintr+0x470>
80100c17:	fa                   	cli    
    for(;;)
80100c18:	eb fe                	jmp    80100c18 <consoleintr+0x158>
80100c1a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100c20:	be d4 03 00 00       	mov    $0x3d4,%esi
80100c25:	b8 0e 00 00 00       	mov    $0xe,%eax
80100c2a:	89 f2                	mov    %esi,%edx
80100c2c:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100c2d:	bb d5 03 00 00       	mov    $0x3d5,%ebx
80100c32:	89 da                	mov    %ebx,%edx
80100c34:	ec                   	in     (%dx),%al
      pos = inb(CRTPORT+1) << 8;
80100c35:	0f b6 c8             	movzbl %al,%ecx
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100c38:	89 f2                	mov    %esi,%edx
80100c3a:	b8 0f 00 00 00       	mov    $0xf,%eax
80100c3f:	c1 e1 08             	shl    $0x8,%ecx
80100c42:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100c43:	89 da                	mov    %ebx,%edx
80100c45:	ec                   	in     (%dx),%al
      pos |= inb(CRTPORT+1); 
80100c46:	0f b6 c0             	movzbl %al,%eax
80100c49:	09 c1                	or     %eax,%ecx
      while(input.e != input.w &&
80100c4b:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
      if (pos > end) {
80100c50:	39 0d 00 90 10 80    	cmp    %ecx,0x80109000
80100c56:	0f 8c f4 01 00 00    	jl     80100e50 <consoleintr+0x390>
      while(input.e != input.w 
80100c5c:	8b 35 04 ff 10 80    	mov    0x8010ff04,%esi
80100c62:	31 db                	xor    %ebx,%ebx
80100c64:	39 c6                	cmp    %eax,%esi
80100c66:	74 38                	je     80100ca0 <consoleintr+0x1e0>
80100c68:	89 7d e0             	mov    %edi,-0x20(%ebp)
80100c6b:	89 f7                	mov    %esi,%edi
80100c6d:	eb 11                	jmp    80100c80 <consoleintr+0x1c0>
80100c6f:	90                   	nop
        pos--;
80100c70:	83 e9 01             	sub    $0x1,%ecx
      while(input.e != input.w 
80100c73:	bb 01 00 00 00       	mov    $0x1,%ebx
80100c78:	39 f8                	cmp    %edi,%eax
80100c7a:	0f 84 82 02 00 00    	je     80100f02 <consoleintr+0x442>
80100c80:	89 c6                	mov    %eax,%esi
            && input.buf[(input.e-1) % INPUT_BUF] != '\n'
80100c82:	83 e8 01             	sub    $0x1,%eax
80100c85:	89 c2                	mov    %eax,%edx
80100c87:	83 e2 7f             	and    $0x7f,%edx
80100c8a:	80 ba 80 fe 10 80 0a 	cmpb   $0xa,-0x7fef0180(%edx)
80100c91:	75 dd                	jne    80100c70 <consoleintr+0x1b0>
80100c93:	8b 7d e0             	mov    -0x20(%ebp),%edi
80100c96:	84 db                	test   %bl,%bl
80100c98:	74 06                	je     80100ca0 <consoleintr+0x1e0>
80100c9a:	89 35 08 ff 10 80    	mov    %esi,0x8010ff08
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100ca0:	be d4 03 00 00       	mov    $0x3d4,%esi
80100ca5:	b8 0e 00 00 00       	mov    $0xe,%eax
80100caa:	89 f2                	mov    %esi,%edx
80100cac:	ee                   	out    %al,(%dx)
80100cad:	bb d5 03 00 00       	mov    $0x3d5,%ebx
      outb(CRTPORT+1, pos>>8);
80100cb2:	89 c8                	mov    %ecx,%eax
80100cb4:	c1 f8 08             	sar    $0x8,%eax
80100cb7:	89 da                	mov    %ebx,%edx
80100cb9:	ee                   	out    %al,(%dx)
80100cba:	b8 0f 00 00 00       	mov    $0xf,%eax
80100cbf:	89 f2                	mov    %esi,%edx
80100cc1:	ee                   	out    %al,(%dx)
80100cc2:	89 c8                	mov    %ecx,%eax
80100cc4:	89 da                	mov    %ebx,%edx
80100cc6:	ee                   	out    %al,(%dx)
  while((c = getc()) >= 0){
80100cc7:	ff d7                	call   *%edi
80100cc9:	85 c0                	test   %eax,%eax
80100ccb:	0f 89 21 fe ff ff    	jns    80100af2 <consoleintr+0x32>
80100cd1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  release(&cons.lock);
80100cd8:	83 ec 0c             	sub    $0xc,%esp
80100cdb:	68 20 ff 10 80       	push   $0x8010ff20
80100ce0:	e8 0b 44 00 00       	call   801050f0 <release>
  if(doprocdump) {
80100ce5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ce8:	83 c4 10             	add    $0x10,%esp
80100ceb:	85 c0                	test   %eax,%eax
80100ced:	0f 85 1c 02 00 00    	jne    80100f0f <consoleintr+0x44f>
}
80100cf3:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100cf6:	5b                   	pop    %ebx
80100cf7:	5e                   	pop    %esi
80100cf8:	5f                   	pop    %edi
80100cf9:	5d                   	pop    %ebp
80100cfa:	c3                   	ret    
      while(input.e != input.w &&
80100cfb:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
80100d00:	39 05 04 ff 10 80    	cmp    %eax,0x8010ff04
80100d06:	0f 84 dc fd ff ff    	je     80100ae8 <consoleintr+0x28>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100d0c:	83 e8 01             	sub    $0x1,%eax
80100d0f:	89 c2                	mov    %eax,%edx
80100d11:	83 e2 7f             	and    $0x7f,%edx
      while(input.e != input.w &&
80100d14:	80 ba 80 fe 10 80 0a 	cmpb   $0xa,-0x7fef0180(%edx)
80100d1b:	0f 84 c7 fd ff ff    	je     80100ae8 <consoleintr+0x28>
        input.e--;
80100d21:	a3 08 ff 10 80       	mov    %eax,0x8010ff08
  if(panicked){
80100d26:	a1 58 ff 10 80       	mov    0x8010ff58,%eax
80100d2b:	85 c0                	test   %eax,%eax
80100d2d:	0f 84 d7 00 00 00    	je     80100e0a <consoleintr+0x34a>
  asm volatile("cli");
80100d33:	fa                   	cli    
    for(;;)
80100d34:	eb fe                	jmp    80100d34 <consoleintr+0x274>
80100d36:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100d3d:	8d 76 00             	lea    0x0(%esi),%esi
      if(input.buf[(input.e-1) % INPUT_BUF] >= 'a' && input.buf[(input.e-1) % INPUT_BUF] <= 'z'){
80100d40:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
80100d45:	83 e8 01             	sub    $0x1,%eax
80100d48:	89 c2                	mov    %eax,%edx
80100d4a:	83 e2 7f             	and    $0x7f,%edx
80100d4d:	0f b6 92 80 fe 10 80 	movzbl -0x7fef0180(%edx),%edx
80100d54:	83 ea 61             	sub    $0x61,%edx
80100d57:	80 fa 19             	cmp    $0x19,%dl
80100d5a:	0f 86 bb 01 00 00    	jbe    80100f1b <consoleintr+0x45b>
      make_upper = 1;
80100d60:	c7 05 10 ff 10 80 01 	movl   $0x1,0x8010ff10
80100d67:	00 00 00 
      break;
80100d6a:	e9 79 fd ff ff       	jmp    80100ae8 <consoleintr+0x28>
      swap(&input.buf[(input.e-1) % INPUT_BUF],&input.buf[(input.e-2) % INPUT_BUF]);
80100d6f:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
80100d74:	8d 50 fe             	lea    -0x2(%eax),%edx
80100d77:	83 e8 01             	sub    $0x1,%eax
80100d7a:	89 c3                	mov    %eax,%ebx
80100d7c:	83 e2 7f             	and    $0x7f,%edx
        input.e--;
80100d7f:	a3 08 ff 10 80       	mov    %eax,0x8010ff08
  if(panicked){
80100d84:	a1 58 ff 10 80       	mov    0x8010ff58,%eax
      swap(&input.buf[(input.e-1) % INPUT_BUF],&input.buf[(input.e-2) % INPUT_BUF]);
80100d89:	83 e3 7f             	and    $0x7f,%ebx
  *a = *b;
80100d8c:	0f b6 8a 80 fe 10 80 	movzbl -0x7fef0180(%edx),%ecx
  temp = *a;
80100d93:	0f b6 b3 80 fe 10 80 	movzbl -0x7fef0180(%ebx),%esi
  *a = *b;
80100d9a:	88 8b 80 fe 10 80    	mov    %cl,-0x7fef0180(%ebx)
  *b = temp;
80100da0:	89 f1                	mov    %esi,%ecx
80100da2:	88 8a 80 fe 10 80    	mov    %cl,-0x7fef0180(%edx)
  if(panicked){
80100da8:	85 c0                	test   %eax,%eax
80100daa:	75 3f                	jne    80100deb <consoleintr+0x32b>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100dac:	83 ec 0c             	sub    $0xc,%esp
80100daf:	6a 08                	push   $0x8
80100db1:	e8 8a 5c 00 00       	call   80106a40 <uartputc>
80100db6:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100dbd:	e8 7e 5c 00 00       	call   80106a40 <uartputc>
80100dc2:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100dc9:	e8 72 5c 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100dce:	b8 00 01 00 00       	mov    $0x100,%eax
80100dd3:	e8 38 f6 ff ff       	call   80100410 <cgaputc>
  if(panicked){
80100dd8:	a1 58 ff 10 80       	mov    0x8010ff58,%eax
        input.e--;
80100ddd:	83 2d 08 ff 10 80 01 	subl   $0x1,0x8010ff08
  if(panicked){
80100de4:	83 c4 10             	add    $0x10,%esp
80100de7:	85 c0                	test   %eax,%eax
80100de9:	74 75                	je     80100e60 <consoleintr+0x3a0>
80100deb:	fa                   	cli    
    for(;;)
80100dec:	eb fe                	jmp    80100dec <consoleintr+0x32c>
80100dee:	66 90                	xchg   %ax,%ax
    switch(c){
80100df0:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
80100df7:	e9 ec fc ff ff       	jmp    80100ae8 <consoleintr+0x28>
80100dfc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      upArrow();
80100e00:	e8 bb fb ff ff       	call   801009c0 <upArrow>
      break;
80100e05:	e9 de fc ff ff       	jmp    80100ae8 <consoleintr+0x28>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100e0a:	83 ec 0c             	sub    $0xc,%esp
80100e0d:	6a 08                	push   $0x8
80100e0f:	e8 2c 5c 00 00       	call   80106a40 <uartputc>
80100e14:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100e1b:	e8 20 5c 00 00       	call   80106a40 <uartputc>
80100e20:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100e27:	e8 14 5c 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100e2c:	b8 00 01 00 00       	mov    $0x100,%eax
80100e31:	e8 da f5 ff ff       	call   80100410 <cgaputc>
      while(input.e != input.w &&
80100e36:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
80100e3b:	83 c4 10             	add    $0x10,%esp
80100e3e:	3b 05 04 ff 10 80    	cmp    0x8010ff04,%eax
80100e44:	0f 85 c2 fe ff ff    	jne    80100d0c <consoleintr+0x24c>
80100e4a:	e9 99 fc ff ff       	jmp    80100ae8 <consoleintr+0x28>
80100e4f:	90                   	nop
        end = pos;
80100e50:	89 0d 00 90 10 80    	mov    %ecx,0x80109000
        edited_index = input.e;
80100e56:	a3 0c ff 10 80       	mov    %eax,0x8010ff0c
80100e5b:	e9 fc fd ff ff       	jmp    80100c5c <consoleintr+0x19c>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100e60:	83 ec 0c             	sub    $0xc,%esp
80100e63:	6a 08                	push   $0x8
80100e65:	e8 d6 5b 00 00       	call   80106a40 <uartputc>
80100e6a:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100e71:	e8 ca 5b 00 00       	call   80106a40 <uartputc>
80100e76:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100e7d:	e8 be 5b 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100e82:	b8 00 01 00 00       	mov    $0x100,%eax
80100e87:	e8 84 f5 ff ff       	call   80100410 <cgaputc>
        consputc(input.buf[(input.e++) % INPUT_BUF]);
80100e8c:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
  if(panicked){
80100e91:	8b 35 58 ff 10 80    	mov    0x8010ff58,%esi
80100e97:	83 c4 10             	add    $0x10,%esp
        consputc(input.buf[(input.e++) % INPUT_BUF]);
80100e9a:	8d 50 01             	lea    0x1(%eax),%edx
80100e9d:	89 15 08 ff 10 80    	mov    %edx,0x8010ff08
  if(panicked){
80100ea3:	85 f6                	test   %esi,%esi
80100ea5:	75 35                	jne    80100edc <consoleintr+0x41c>
        consputc(input.buf[(input.e++) % INPUT_BUF]);
80100ea7:	83 e0 7f             	and    $0x7f,%eax
    uartputc(c);
80100eaa:	83 ec 0c             	sub    $0xc,%esp
        consputc(input.buf[(input.e++) % INPUT_BUF]);
80100ead:	0f be 98 80 fe 10 80 	movsbl -0x7fef0180(%eax),%ebx
    uartputc(c);
80100eb4:	53                   	push   %ebx
80100eb5:	e8 86 5b 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100eba:	89 d8                	mov    %ebx,%eax
80100ebc:	e8 4f f5 ff ff       	call   80100410 <cgaputc>
        consputc(input.buf[(input.e++) % INPUT_BUF]);
80100ec1:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
  if(panicked){
80100ec6:	8b 1d 58 ff 10 80    	mov    0x8010ff58,%ebx
80100ecc:	83 c4 10             	add    $0x10,%esp
        consputc(input.buf[(input.e++) % INPUT_BUF]);
80100ecf:	8d 50 01             	lea    0x1(%eax),%edx
80100ed2:	89 15 08 ff 10 80    	mov    %edx,0x8010ff08
  if(panicked){
80100ed8:	85 db                	test   %ebx,%ebx
80100eda:	74 04                	je     80100ee0 <consoleintr+0x420>
80100edc:	fa                   	cli    
    for(;;)
80100edd:	eb fe                	jmp    80100edd <consoleintr+0x41d>
80100edf:	90                   	nop
        consputc(input.buf[(input.e++) % INPUT_BUF]);
80100ee0:	83 e0 7f             	and    $0x7f,%eax
    uartputc(c);
80100ee3:	83 ec 0c             	sub    $0xc,%esp
        consputc(input.buf[(input.e++) % INPUT_BUF]);
80100ee6:	0f be 98 80 fe 10 80 	movsbl -0x7fef0180(%eax),%ebx
    uartputc(c);
80100eed:	53                   	push   %ebx
80100eee:	e8 4d 5b 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100ef3:	89 d8                	mov    %ebx,%eax
80100ef5:	e8 16 f5 ff ff       	call   80100410 <cgaputc>
80100efa:	83 c4 10             	add    $0x10,%esp
80100efd:	e9 e6 fb ff ff       	jmp    80100ae8 <consoleintr+0x28>
80100f02:	a3 08 ff 10 80       	mov    %eax,0x8010ff08
80100f07:	8b 7d e0             	mov    -0x20(%ebp),%edi
80100f0a:	e9 91 fd ff ff       	jmp    80100ca0 <consoleintr+0x1e0>
}
80100f0f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100f12:	5b                   	pop    %ebx
80100f13:	5e                   	pop    %esi
80100f14:	5f                   	pop    %edi
80100f15:	5d                   	pop    %ebp
    procdump();  // now call procdump() wo. cons.lock held
80100f16:	e9 05 3d 00 00       	jmp    80104c20 <procdump>
  if(panicked){
80100f1b:	8b 0d 58 ff 10 80    	mov    0x8010ff58,%ecx
        input.e--;
80100f21:	a3 08 ff 10 80       	mov    %eax,0x8010ff08
  if(panicked){
80100f26:	85 c9                	test   %ecx,%ecx
80100f28:	74 3a                	je     80100f64 <consoleintr+0x4a4>
80100f2a:	fa                   	cli    
    for(;;)
80100f2b:	eb fe                	jmp    80100f2b <consoleintr+0x46b>
80100f2d:	8d 76 00             	lea    0x0(%esi),%esi
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100f30:	83 ec 0c             	sub    $0xc,%esp
80100f33:	6a 08                	push   $0x8
80100f35:	e8 06 5b 00 00       	call   80106a40 <uartputc>
80100f3a:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100f41:	e8 fa 5a 00 00       	call   80106a40 <uartputc>
80100f46:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100f4d:	e8 ee 5a 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100f52:	b8 00 01 00 00       	mov    $0x100,%eax
80100f57:	e8 b4 f4 ff ff       	call   80100410 <cgaputc>
}
80100f5c:	83 c4 10             	add    $0x10,%esp
80100f5f:	e9 84 fb ff ff       	jmp    80100ae8 <consoleintr+0x28>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100f64:	83 ec 0c             	sub    $0xc,%esp
80100f67:	6a 08                	push   $0x8
80100f69:	e8 d2 5a 00 00       	call   80106a40 <uartputc>
80100f6e:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100f75:	e8 c6 5a 00 00       	call   80106a40 <uartputc>
80100f7a:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100f81:	e8 ba 5a 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100f86:	b8 00 01 00 00       	mov    $0x100,%eax
80100f8b:	e8 80 f4 ff ff       	call   80100410 <cgaputc>
        input.e++;
80100f90:	8b 15 08 ff 10 80    	mov    0x8010ff08,%edx
  if(panicked){
80100f96:	83 c4 10             	add    $0x10,%esp
        input.e++;
80100f99:	8d 42 01             	lea    0x1(%edx),%eax
        input.buf[(input.e-1) % INPUT_BUF] = input.buf[(input.e-1) % INPUT_BUF] - ('a' -'A');
80100f9c:	83 e2 7f             	and    $0x7f,%edx
        input.e++;
80100f9f:	a3 08 ff 10 80       	mov    %eax,0x8010ff08
        input.buf[(input.e-1) % INPUT_BUF] = input.buf[(input.e-1) % INPUT_BUF] - ('a' -'A');
80100fa4:	0f b6 82 80 fe 10 80 	movzbl -0x7fef0180(%edx),%eax
80100fab:	8d 58 e0             	lea    -0x20(%eax),%ebx
80100fae:	88 9a 80 fe 10 80    	mov    %bl,-0x7fef0180(%edx)
  if(panicked){
80100fb4:	8b 15 58 ff 10 80    	mov    0x8010ff58,%edx
80100fba:	85 d2                	test   %edx,%edx
80100fbc:	74 12                	je     80100fd0 <consoleintr+0x510>
80100fbe:	fa                   	cli    
    for(;;)
80100fbf:	eb fe                	jmp    80100fbf <consoleintr+0x4ff>
      if(c != 0 && input.e-input.r < INPUT_BUF){
80100fc1:	85 c0                	test   %eax,%eax
80100fc3:	0f 84 1f fb ff ff    	je     80100ae8 <consoleintr+0x28>
80100fc9:	e9 5e fb ff ff       	jmp    80100b2c <consoleintr+0x6c>
80100fce:	66 90                	xchg   %ax,%ax
    uartputc(c);
80100fd0:	83 ec 0c             	sub    $0xc,%esp
        consputc(input.buf[(input.e-1) % INPUT_BUF]);
80100fd3:	0f be db             	movsbl %bl,%ebx
    uartputc(c);
80100fd6:	53                   	push   %ebx
80100fd7:	e8 64 5a 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80100fdc:	89 d8                	mov    %ebx,%eax
80100fde:	e8 2d f4 ff ff       	call   80100410 <cgaputc>
}
80100fe3:	83 c4 10             	add    $0x10,%esp
80100fe6:	e9 75 fd ff ff       	jmp    80100d60 <consoleintr+0x2a0>
          if(c >= 'a' && c <= 'z'){
80100feb:	8d 58 9f             	lea    -0x61(%eax),%ebx
80100fee:	83 fb 19             	cmp    $0x19,%ebx
80100ff1:	0f 87 9e 00 00 00    	ja     80101095 <consoleintr+0x5d5>
            c = c - ('a' - 'A');
80100ff7:	8d 58 e0             	lea    -0x20(%eax),%ebx
        input.buf[input.e++ % INPUT_BUF] = c;
80100ffa:	89 15 08 ff 10 80    	mov    %edx,0x8010ff08
80101000:	88 99 80 fe 10 80    	mov    %bl,-0x7fef0180(%ecx)
  if(panicked){
80101006:	85 f6                	test   %esi,%esi
80101008:	75 18                	jne    80101022 <consoleintr+0x562>
    uartputc(c);
8010100a:	83 ec 0c             	sub    $0xc,%esp
8010100d:	53                   	push   %ebx
8010100e:	e8 2d 5a 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
80101013:	89 d8                	mov    %ebx,%eax
80101015:	e8 f6 f3 ff ff       	call   80100410 <cgaputc>
8010101a:	83 c4 10             	add    $0x10,%esp
8010101d:	e9 93 fb ff ff       	jmp    80100bb5 <consoleintr+0xf5>
80101022:	fa                   	cli    
    for(;;)
80101023:	eb fe                	jmp    80101023 <consoleintr+0x563>
80101025:	8d 76 00             	lea    0x0(%esi),%esi
        if(make_upper == 1){
80101028:	83 fb 01             	cmp    $0x1,%ebx
8010102b:	0f 84 87 00 00 00    	je     801010b8 <consoleintr+0x5f8>
        input.buf[input.e++ % INPUT_BUF] = c;
80101031:	89 15 08 ff 10 80    	mov    %edx,0x8010ff08
80101037:	c6 81 80 fe 10 80 0a 	movb   $0xa,-0x7fef0180(%ecx)
  if(panicked){
8010103e:	85 f6                	test   %esi,%esi
80101040:	75 e0                	jne    80101022 <consoleintr+0x562>
    uartputc(c);
80101042:	83 ec 0c             	sub    $0xc,%esp
80101045:	6a 0a                	push   $0xa
80101047:	e8 f4 59 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
8010104c:	b8 0a 00 00 00       	mov    $0xa,%eax
80101051:	e8 ba f3 ff ff       	call   80100410 <cgaputc>
          input.w = input.e;
80101056:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
8010105b:	83 c4 10             	add    $0x10,%esp
8010105e:	e9 66 fb ff ff       	jmp    80100bc9 <consoleintr+0x109>
    uartputc(c);
80101063:	83 ec 0c             	sub    $0xc,%esp
80101066:	89 45 e0             	mov    %eax,-0x20(%ebp)
80101069:	50                   	push   %eax
8010106a:	e8 d1 59 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
8010106f:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101072:	e8 99 f3 ff ff       	call   80100410 <cgaputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80101077:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010107a:	83 c4 10             	add    $0x10,%esp
8010107d:	83 f8 0a             	cmp    $0xa,%eax
80101080:	74 09                	je     8010108b <consoleintr+0x5cb>
80101082:	83 f8 04             	cmp    $0x4,%eax
80101085:	0f 85 2a fb ff ff    	jne    80100bb5 <consoleintr+0xf5>
          input.w = input.e;
8010108b:	a1 08 ff 10 80       	mov    0x8010ff08,%eax
80101090:	e9 34 fb ff ff       	jmp    80100bc9 <consoleintr+0x109>
        input.buf[input.e++ % INPUT_BUF] = c;
80101095:	89 c3                	mov    %eax,%ebx
          } else if(c == ' ' || c == '\n') {
80101097:	83 f8 20             	cmp    $0x20,%eax
8010109a:	74 09                	je     801010a5 <consoleintr+0x5e5>
8010109c:	83 f8 0a             	cmp    $0xa,%eax
8010109f:	0f 85 c2 fa ff ff    	jne    80100b67 <consoleintr+0xa7>
            make_upper = 0;
801010a5:	c7 05 10 ff 10 80 00 	movl   $0x0,0x8010ff10
801010ac:	00 00 00 
801010af:	e9 b3 fa ff ff       	jmp    80100b67 <consoleintr+0xa7>
801010b4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801010b8:	c7 05 10 ff 10 80 00 	movl   $0x0,0x8010ff10
801010bf:	00 00 00 
        input.buf[input.e++ % INPUT_BUF] = c;
801010c2:	89 15 08 ff 10 80    	mov    %edx,0x8010ff08
801010c8:	c6 81 80 fe 10 80 0a 	movb   $0xa,-0x7fef0180(%ecx)
  if(panicked){
801010cf:	85 f6                	test   %esi,%esi
801010d1:	0f 85 4b ff ff ff    	jne    80101022 <consoleintr+0x562>
    uartputc(c);
801010d7:	83 ec 0c             	sub    $0xc,%esp
801010da:	6a 0a                	push   $0xa
801010dc:	e8 5f 59 00 00       	call   80106a40 <uartputc>
  cgaputc(c);
801010e1:	b8 0a 00 00 00       	mov    $0xa,%eax
801010e6:	e8 25 f3 ff ff       	call   80100410 <cgaputc>
801010eb:	83 c4 10             	add    $0x10,%esp
801010ee:	eb 9b                	jmp    8010108b <consoleintr+0x5cb>

801010f0 <consoleinit>:

void
consoleinit(void)
{
801010f0:	f3 0f 1e fb          	endbr32 
801010f4:	55                   	push   %ebp
801010f5:	89 e5                	mov    %esp,%ebp
801010f7:	83 ec 10             	sub    $0x10,%esp
  initlock(&cons.lock, "console");
801010fa:	68 a8 7f 10 80       	push   $0x80107fa8
801010ff:	68 20 ff 10 80       	push   $0x8010ff20
80101104:	e8 57 3e 00 00       	call   80104f60 <initlock>

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  cons.locking = 1;

  ioapicenable(IRQ_KBD, 0);
80101109:	58                   	pop    %eax
8010110a:	5a                   	pop    %edx
8010110b:	6a 00                	push   $0x0
8010110d:	6a 01                	push   $0x1
  devsw[CONSOLE].write = consolewrite;
8010110f:	c7 05 0c 09 11 80 f0 	movl   $0x801005f0,0x8011090c
80101116:	05 10 80 
  devsw[CONSOLE].read = consoleread;
80101119:	c7 05 08 09 11 80 90 	movl   $0x80100290,0x80110908
80101120:	02 10 80 
  cons.locking = 1;
80101123:	c7 05 54 ff 10 80 01 	movl   $0x1,0x8010ff54
8010112a:	00 00 00 
  ioapicenable(IRQ_KBD, 0);
8010112d:	e8 6e 1a 00 00       	call   80102ba0 <ioapicenable>
}
80101132:	83 c4 10             	add    $0x10,%esp
80101135:	c9                   	leave  
80101136:	c3                   	ret    
80101137:	66 90                	xchg   %ax,%ax
80101139:	66 90                	xchg   %ax,%ax
8010113b:	66 90                	xchg   %ax,%ax
8010113d:	66 90                	xchg   %ax,%ax
8010113f:	90                   	nop

80101140 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80101140:	f3 0f 1e fb          	endbr32 
80101144:	55                   	push   %ebp
80101145:	89 e5                	mov    %esp,%ebp
80101147:	57                   	push   %edi
80101148:	56                   	push   %esi
80101149:	53                   	push   %ebx
8010114a:	81 ec 0c 01 00 00    	sub    $0x10c,%esp
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;
  struct proc *curproc = myproc();
80101150:	e8 bb 2f 00 00       	call   80104110 <myproc>
80101155:	89 85 ec fe ff ff    	mov    %eax,-0x114(%ebp)

  begin_op();
8010115b:	e8 40 23 00 00       	call   801034a0 <begin_op>

  if((ip = namei(path)) == 0){
80101160:	83 ec 0c             	sub    $0xc,%esp
80101163:	ff 75 08             	push   0x8(%ebp)
80101166:	e8 35 16 00 00       	call   801027a0 <namei>
8010116b:	83 c4 10             	add    $0x10,%esp
8010116e:	85 c0                	test   %eax,%eax
80101170:	0f 84 fe 02 00 00    	je     80101474 <exec+0x334>
    end_op();
    cprintf("exec: fail\n");
    return -1;
  }
  ilock(ip);
80101176:	83 ec 0c             	sub    $0xc,%esp
80101179:	89 c3                	mov    %eax,%ebx
8010117b:	50                   	push   %eax
8010117c:	e8 df 0c 00 00       	call   80101e60 <ilock>
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) != sizeof(elf))
80101181:	8d 85 24 ff ff ff    	lea    -0xdc(%ebp),%eax
80101187:	6a 34                	push   $0x34
80101189:	6a 00                	push   $0x0
8010118b:	50                   	push   %eax
8010118c:	53                   	push   %ebx
8010118d:	e8 ee 0f 00 00       	call   80102180 <readi>
80101192:	83 c4 20             	add    $0x20,%esp
80101195:	83 f8 34             	cmp    $0x34,%eax
80101198:	74 26                	je     801011c0 <exec+0x80>

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
    iunlockput(ip);
8010119a:	83 ec 0c             	sub    $0xc,%esp
8010119d:	53                   	push   %ebx
8010119e:	e8 4d 0f 00 00       	call   801020f0 <iunlockput>
    end_op();
801011a3:	e8 68 23 00 00       	call   80103510 <end_op>
801011a8:	83 c4 10             	add    $0x10,%esp
  }
  return -1;
801011ab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801011b0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801011b3:	5b                   	pop    %ebx
801011b4:	5e                   	pop    %esi
801011b5:	5f                   	pop    %edi
801011b6:	5d                   	pop    %ebp
801011b7:	c3                   	ret    
801011b8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801011bf:	90                   	nop
  if(elf.magic != ELF_MAGIC)
801011c0:	81 bd 24 ff ff ff 7f 	cmpl   $0x464c457f,-0xdc(%ebp)
801011c7:	45 4c 46 
801011ca:	75 ce                	jne    8010119a <exec+0x5a>
  if((pgdir = setupkvm()) == 0)
801011cc:	e8 0f 6a 00 00       	call   80107be0 <setupkvm>
801011d1:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)
801011d7:	85 c0                	test   %eax,%eax
801011d9:	74 bf                	je     8010119a <exec+0x5a>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
801011db:	66 83 bd 50 ff ff ff 	cmpw   $0x0,-0xb0(%ebp)
801011e2:	00 
801011e3:	8b b5 40 ff ff ff    	mov    -0xc0(%ebp),%esi
801011e9:	0f 84 a4 02 00 00    	je     80101493 <exec+0x353>
  sz = 0;
801011ef:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
801011f6:	00 00 00 
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
801011f9:	31 ff                	xor    %edi,%edi
801011fb:	e9 86 00 00 00       	jmp    80101286 <exec+0x146>
    if(ph.type != ELF_PROG_LOAD)
80101200:	83 bd 04 ff ff ff 01 	cmpl   $0x1,-0xfc(%ebp)
80101207:	75 6c                	jne    80101275 <exec+0x135>
    if(ph.memsz < ph.filesz)
80101209:	8b 85 18 ff ff ff    	mov    -0xe8(%ebp),%eax
8010120f:	3b 85 14 ff ff ff    	cmp    -0xec(%ebp),%eax
80101215:	0f 82 87 00 00 00    	jb     801012a2 <exec+0x162>
    if(ph.vaddr + ph.memsz < ph.vaddr)
8010121b:	03 85 0c ff ff ff    	add    -0xf4(%ebp),%eax
80101221:	72 7f                	jb     801012a2 <exec+0x162>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80101223:	83 ec 04             	sub    $0x4,%esp
80101226:	50                   	push   %eax
80101227:	ff b5 f0 fe ff ff    	push   -0x110(%ebp)
8010122d:	ff b5 f4 fe ff ff    	push   -0x10c(%ebp)
80101233:	e8 c8 67 00 00       	call   80107a00 <allocuvm>
80101238:	83 c4 10             	add    $0x10,%esp
8010123b:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
80101241:	85 c0                	test   %eax,%eax
80101243:	74 5d                	je     801012a2 <exec+0x162>
    if(ph.vaddr % PGSIZE != 0)
80101245:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
8010124b:	a9 ff 0f 00 00       	test   $0xfff,%eax
80101250:	75 50                	jne    801012a2 <exec+0x162>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80101252:	83 ec 0c             	sub    $0xc,%esp
80101255:	ff b5 14 ff ff ff    	push   -0xec(%ebp)
8010125b:	ff b5 08 ff ff ff    	push   -0xf8(%ebp)
80101261:	53                   	push   %ebx
80101262:	50                   	push   %eax
80101263:	ff b5 f4 fe ff ff    	push   -0x10c(%ebp)
80101269:	e8 a2 66 00 00       	call   80107910 <loaduvm>
8010126e:	83 c4 20             	add    $0x20,%esp
80101271:	85 c0                	test   %eax,%eax
80101273:	78 2d                	js     801012a2 <exec+0x162>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80101275:	0f b7 85 50 ff ff ff 	movzwl -0xb0(%ebp),%eax
8010127c:	83 c7 01             	add    $0x1,%edi
8010127f:	83 c6 20             	add    $0x20,%esi
80101282:	39 f8                	cmp    %edi,%eax
80101284:	7e 3a                	jle    801012c0 <exec+0x180>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80101286:	8d 85 04 ff ff ff    	lea    -0xfc(%ebp),%eax
8010128c:	6a 20                	push   $0x20
8010128e:	56                   	push   %esi
8010128f:	50                   	push   %eax
80101290:	53                   	push   %ebx
80101291:	e8 ea 0e 00 00       	call   80102180 <readi>
80101296:	83 c4 10             	add    $0x10,%esp
80101299:	83 f8 20             	cmp    $0x20,%eax
8010129c:	0f 84 5e ff ff ff    	je     80101200 <exec+0xc0>
    freevm(pgdir);
801012a2:	83 ec 0c             	sub    $0xc,%esp
801012a5:	ff b5 f4 fe ff ff    	push   -0x10c(%ebp)
801012ab:	e8 b0 68 00 00       	call   80107b60 <freevm>
  if(ip){
801012b0:	83 c4 10             	add    $0x10,%esp
801012b3:	e9 e2 fe ff ff       	jmp    8010119a <exec+0x5a>
801012b8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801012bf:	90                   	nop
  sz = PGROUNDUP(sz);
801012c0:	8b bd f0 fe ff ff    	mov    -0x110(%ebp),%edi
801012c6:	81 c7 ff 0f 00 00    	add    $0xfff,%edi
801012cc:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
801012d2:	8d b7 00 20 00 00    	lea    0x2000(%edi),%esi
  iunlockput(ip);
801012d8:	83 ec 0c             	sub    $0xc,%esp
801012db:	53                   	push   %ebx
801012dc:	e8 0f 0e 00 00       	call   801020f0 <iunlockput>
  end_op();
801012e1:	e8 2a 22 00 00       	call   80103510 <end_op>
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
801012e6:	83 c4 0c             	add    $0xc,%esp
801012e9:	56                   	push   %esi
801012ea:	57                   	push   %edi
801012eb:	8b bd f4 fe ff ff    	mov    -0x10c(%ebp),%edi
801012f1:	57                   	push   %edi
801012f2:	e8 09 67 00 00       	call   80107a00 <allocuvm>
801012f7:	83 c4 10             	add    $0x10,%esp
801012fa:	89 c6                	mov    %eax,%esi
801012fc:	85 c0                	test   %eax,%eax
801012fe:	0f 84 94 00 00 00    	je     80101398 <exec+0x258>
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80101304:	83 ec 08             	sub    $0x8,%esp
80101307:	8d 80 00 e0 ff ff    	lea    -0x2000(%eax),%eax
  for(argc = 0; argv[argc]; argc++) {
8010130d:	89 f3                	mov    %esi,%ebx
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
8010130f:	50                   	push   %eax
80101310:	57                   	push   %edi
  for(argc = 0; argv[argc]; argc++) {
80101311:	31 ff                	xor    %edi,%edi
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80101313:	e8 68 69 00 00       	call   80107c80 <clearpteu>
  for(argc = 0; argv[argc]; argc++) {
80101318:	8b 45 0c             	mov    0xc(%ebp),%eax
8010131b:	83 c4 10             	add    $0x10,%esp
8010131e:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
80101324:	8b 00                	mov    (%eax),%eax
80101326:	85 c0                	test   %eax,%eax
80101328:	0f 84 8b 00 00 00    	je     801013b9 <exec+0x279>
8010132e:	89 b5 f0 fe ff ff    	mov    %esi,-0x110(%ebp)
80101334:	8b b5 f4 fe ff ff    	mov    -0x10c(%ebp),%esi
8010133a:	eb 23                	jmp    8010135f <exec+0x21f>
8010133c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101340:	8b 45 0c             	mov    0xc(%ebp),%eax
    ustack[3+argc] = sp;
80101343:	89 9c bd 64 ff ff ff 	mov    %ebx,-0x9c(%ebp,%edi,4)
  for(argc = 0; argv[argc]; argc++) {
8010134a:	83 c7 01             	add    $0x1,%edi
    ustack[3+argc] = sp;
8010134d:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
  for(argc = 0; argv[argc]; argc++) {
80101353:	8b 04 b8             	mov    (%eax,%edi,4),%eax
80101356:	85 c0                	test   %eax,%eax
80101358:	74 59                	je     801013b3 <exec+0x273>
    if(argc >= MAXARG)
8010135a:	83 ff 20             	cmp    $0x20,%edi
8010135d:	74 39                	je     80101398 <exec+0x258>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
8010135f:	83 ec 0c             	sub    $0xc,%esp
80101362:	50                   	push   %eax
80101363:	e8 c8 40 00 00       	call   80105430 <strlen>
80101368:	f7 d0                	not    %eax
8010136a:	01 c3                	add    %eax,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
8010136c:	58                   	pop    %eax
8010136d:	8b 45 0c             	mov    0xc(%ebp),%eax
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80101370:	83 e3 fc             	and    $0xfffffffc,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80101373:	ff 34 b8             	push   (%eax,%edi,4)
80101376:	e8 b5 40 00 00       	call   80105430 <strlen>
8010137b:	83 c0 01             	add    $0x1,%eax
8010137e:	50                   	push   %eax
8010137f:	8b 45 0c             	mov    0xc(%ebp),%eax
80101382:	ff 34 b8             	push   (%eax,%edi,4)
80101385:	53                   	push   %ebx
80101386:	56                   	push   %esi
80101387:	e8 c4 6a 00 00       	call   80107e50 <copyout>
8010138c:	83 c4 20             	add    $0x20,%esp
8010138f:	85 c0                	test   %eax,%eax
80101391:	79 ad                	jns    80101340 <exec+0x200>
80101393:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101397:	90                   	nop
    freevm(pgdir);
80101398:	83 ec 0c             	sub    $0xc,%esp
8010139b:	ff b5 f4 fe ff ff    	push   -0x10c(%ebp)
801013a1:	e8 ba 67 00 00       	call   80107b60 <freevm>
801013a6:	83 c4 10             	add    $0x10,%esp
  return -1;
801013a9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801013ae:	e9 fd fd ff ff       	jmp    801011b0 <exec+0x70>
801013b3:	8b b5 f0 fe ff ff    	mov    -0x110(%ebp),%esi
  ustack[2] = sp - (argc+1)*4;  // argv pointer
801013b9:	8d 04 bd 04 00 00 00 	lea    0x4(,%edi,4),%eax
801013c0:	89 d9                	mov    %ebx,%ecx
  ustack[3+argc] = 0;
801013c2:	c7 84 bd 64 ff ff ff 	movl   $0x0,-0x9c(%ebp,%edi,4)
801013c9:	00 00 00 00 
  ustack[2] = sp - (argc+1)*4;  // argv pointer
801013cd:	29 c1                	sub    %eax,%ecx
  sp -= (3+argc+1) * 4;
801013cf:	83 c0 0c             	add    $0xc,%eax
  ustack[1] = argc;
801013d2:	89 bd 5c ff ff ff    	mov    %edi,-0xa4(%ebp)
  sp -= (3+argc+1) * 4;
801013d8:	29 c3                	sub    %eax,%ebx
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
801013da:	50                   	push   %eax
801013db:	52                   	push   %edx
801013dc:	53                   	push   %ebx
801013dd:	ff b5 f4 fe ff ff    	push   -0x10c(%ebp)
  ustack[0] = 0xffffffff;  // fake return PC
801013e3:	c7 85 58 ff ff ff ff 	movl   $0xffffffff,-0xa8(%ebp)
801013ea:	ff ff ff 
  ustack[2] = sp - (argc+1)*4;  // argv pointer
801013ed:	89 8d 60 ff ff ff    	mov    %ecx,-0xa0(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
801013f3:	e8 58 6a 00 00       	call   80107e50 <copyout>
801013f8:	83 c4 10             	add    $0x10,%esp
801013fb:	85 c0                	test   %eax,%eax
801013fd:	78 99                	js     80101398 <exec+0x258>
  for(last=s=path; *s; s++)
801013ff:	8b 45 08             	mov    0x8(%ebp),%eax
80101402:	8b 55 08             	mov    0x8(%ebp),%edx
80101405:	0f b6 00             	movzbl (%eax),%eax
80101408:	84 c0                	test   %al,%al
8010140a:	74 13                	je     8010141f <exec+0x2df>
8010140c:	89 d1                	mov    %edx,%ecx
8010140e:	66 90                	xchg   %ax,%ax
      last = s+1;
80101410:	83 c1 01             	add    $0x1,%ecx
80101413:	3c 2f                	cmp    $0x2f,%al
  for(last=s=path; *s; s++)
80101415:	0f b6 01             	movzbl (%ecx),%eax
      last = s+1;
80101418:	0f 44 d1             	cmove  %ecx,%edx
  for(last=s=path; *s; s++)
8010141b:	84 c0                	test   %al,%al
8010141d:	75 f1                	jne    80101410 <exec+0x2d0>
  safestrcpy(curproc->name, last, sizeof(curproc->name));
8010141f:	8b bd ec fe ff ff    	mov    -0x114(%ebp),%edi
80101425:	83 ec 04             	sub    $0x4,%esp
80101428:	6a 10                	push   $0x10
8010142a:	89 f8                	mov    %edi,%eax
8010142c:	52                   	push   %edx
8010142d:	83 c0 70             	add    $0x70,%eax
80101430:	50                   	push   %eax
80101431:	e8 ba 3f 00 00       	call   801053f0 <safestrcpy>
  curproc->pgdir = pgdir;
80101436:	8b 8d f4 fe ff ff    	mov    -0x10c(%ebp),%ecx
  oldpgdir = curproc->pgdir;
8010143c:	89 f8                	mov    %edi,%eax
8010143e:	8b 7f 04             	mov    0x4(%edi),%edi
  curproc->sz = sz;
80101441:	89 30                	mov    %esi,(%eax)
  curproc->pgdir = pgdir;
80101443:	89 48 04             	mov    %ecx,0x4(%eax)
  curproc->tf->eip = elf.entry;  // main
80101446:	89 c1                	mov    %eax,%ecx
80101448:	8b 95 3c ff ff ff    	mov    -0xc4(%ebp),%edx
8010144e:	8b 40 1c             	mov    0x1c(%eax),%eax
80101451:	89 50 38             	mov    %edx,0x38(%eax)
  curproc->tf->esp = sp;
80101454:	8b 41 1c             	mov    0x1c(%ecx),%eax
80101457:	89 58 44             	mov    %ebx,0x44(%eax)
  switchuvm(curproc);
8010145a:	89 0c 24             	mov    %ecx,(%esp)
8010145d:	e8 1e 63 00 00       	call   80107780 <switchuvm>
  freevm(oldpgdir);
80101462:	89 3c 24             	mov    %edi,(%esp)
80101465:	e8 f6 66 00 00       	call   80107b60 <freevm>
  return 0;
8010146a:	83 c4 10             	add    $0x10,%esp
8010146d:	31 c0                	xor    %eax,%eax
8010146f:	e9 3c fd ff ff       	jmp    801011b0 <exec+0x70>
    end_op();
80101474:	e8 97 20 00 00       	call   80103510 <end_op>
    cprintf("exec: fail\n");
80101479:	83 ec 0c             	sub    $0xc,%esp
8010147c:	68 19 80 10 80       	push   $0x80108019
80101481:	e8 aa f2 ff ff       	call   80100730 <cprintf>
    return -1;
80101486:	83 c4 10             	add    $0x10,%esp
80101489:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010148e:	e9 1d fd ff ff       	jmp    801011b0 <exec+0x70>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80101493:	31 ff                	xor    %edi,%edi
80101495:	be 00 20 00 00       	mov    $0x2000,%esi
8010149a:	e9 39 fe ff ff       	jmp    801012d8 <exec+0x198>
8010149f:	90                   	nop

801014a0 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
801014a0:	f3 0f 1e fb          	endbr32 
801014a4:	55                   	push   %ebp
801014a5:	89 e5                	mov    %esp,%ebp
801014a7:	83 ec 10             	sub    $0x10,%esp
  initlock(&ftable.lock, "ftable");
801014aa:	68 25 80 10 80       	push   $0x80108025
801014af:	68 60 ff 10 80       	push   $0x8010ff60
801014b4:	e8 a7 3a 00 00       	call   80104f60 <initlock>
}
801014b9:	83 c4 10             	add    $0x10,%esp
801014bc:	c9                   	leave  
801014bd:	c3                   	ret    
801014be:	66 90                	xchg   %ax,%ax

801014c0 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
801014c0:	f3 0f 1e fb          	endbr32 
801014c4:	55                   	push   %ebp
801014c5:	89 e5                	mov    %esp,%ebp
801014c7:	53                   	push   %ebx
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
801014c8:	bb 94 ff 10 80       	mov    $0x8010ff94,%ebx
{
801014cd:	83 ec 10             	sub    $0x10,%esp
  acquire(&ftable.lock);
801014d0:	68 60 ff 10 80       	push   $0x8010ff60
801014d5:	e8 86 3c 00 00       	call   80105160 <acquire>
801014da:	83 c4 10             	add    $0x10,%esp
801014dd:	eb 0c                	jmp    801014eb <filealloc+0x2b>
801014df:	90                   	nop
  for(f = ftable.file; f < ftable.file + NFILE; f++){
801014e0:	83 c3 18             	add    $0x18,%ebx
801014e3:	81 fb f4 08 11 80    	cmp    $0x801108f4,%ebx
801014e9:	74 25                	je     80101510 <filealloc+0x50>
    if(f->ref == 0){
801014eb:	8b 43 04             	mov    0x4(%ebx),%eax
801014ee:	85 c0                	test   %eax,%eax
801014f0:	75 ee                	jne    801014e0 <filealloc+0x20>
      f->ref = 1;
      release(&ftable.lock);
801014f2:	83 ec 0c             	sub    $0xc,%esp
      f->ref = 1;
801014f5:	c7 43 04 01 00 00 00 	movl   $0x1,0x4(%ebx)
      release(&ftable.lock);
801014fc:	68 60 ff 10 80       	push   $0x8010ff60
80101501:	e8 ea 3b 00 00       	call   801050f0 <release>
      return f;
    }
  }
  release(&ftable.lock);
  return 0;
}
80101506:	89 d8                	mov    %ebx,%eax
      return f;
80101508:	83 c4 10             	add    $0x10,%esp
}
8010150b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010150e:	c9                   	leave  
8010150f:	c3                   	ret    
  release(&ftable.lock);
80101510:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80101513:	31 db                	xor    %ebx,%ebx
  release(&ftable.lock);
80101515:	68 60 ff 10 80       	push   $0x8010ff60
8010151a:	e8 d1 3b 00 00       	call   801050f0 <release>
}
8010151f:	89 d8                	mov    %ebx,%eax
  return 0;
80101521:	83 c4 10             	add    $0x10,%esp
}
80101524:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101527:	c9                   	leave  
80101528:	c3                   	ret    
80101529:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101530 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80101530:	f3 0f 1e fb          	endbr32 
80101534:	55                   	push   %ebp
80101535:	89 e5                	mov    %esp,%ebp
80101537:	53                   	push   %ebx
80101538:	83 ec 10             	sub    $0x10,%esp
8010153b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ftable.lock);
8010153e:	68 60 ff 10 80       	push   $0x8010ff60
80101543:	e8 18 3c 00 00       	call   80105160 <acquire>
  if(f->ref < 1)
80101548:	8b 43 04             	mov    0x4(%ebx),%eax
8010154b:	83 c4 10             	add    $0x10,%esp
8010154e:	85 c0                	test   %eax,%eax
80101550:	7e 1a                	jle    8010156c <filedup+0x3c>
    panic("filedup");
  f->ref++;
80101552:	83 c0 01             	add    $0x1,%eax
  release(&ftable.lock);
80101555:	83 ec 0c             	sub    $0xc,%esp
  f->ref++;
80101558:	89 43 04             	mov    %eax,0x4(%ebx)
  release(&ftable.lock);
8010155b:	68 60 ff 10 80       	push   $0x8010ff60
80101560:	e8 8b 3b 00 00       	call   801050f0 <release>
  return f;
}
80101565:	89 d8                	mov    %ebx,%eax
80101567:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010156a:	c9                   	leave  
8010156b:	c3                   	ret    
    panic("filedup");
8010156c:	83 ec 0c             	sub    $0xc,%esp
8010156f:	68 2c 80 10 80       	push   $0x8010802c
80101574:	e8 17 ee ff ff       	call   80100390 <panic>
80101579:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101580 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80101580:	f3 0f 1e fb          	endbr32 
80101584:	55                   	push   %ebp
80101585:	89 e5                	mov    %esp,%ebp
80101587:	57                   	push   %edi
80101588:	56                   	push   %esi
80101589:	53                   	push   %ebx
8010158a:	83 ec 28             	sub    $0x28,%esp
8010158d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct file ff;

  acquire(&ftable.lock);
80101590:	68 60 ff 10 80       	push   $0x8010ff60
80101595:	e8 c6 3b 00 00       	call   80105160 <acquire>
  if(f->ref < 1)
8010159a:	8b 53 04             	mov    0x4(%ebx),%edx
8010159d:	83 c4 10             	add    $0x10,%esp
801015a0:	85 d2                	test   %edx,%edx
801015a2:	0f 8e a1 00 00 00    	jle    80101649 <fileclose+0xc9>
    panic("fileclose");
  if(--f->ref > 0){
801015a8:	83 ea 01             	sub    $0x1,%edx
801015ab:	89 53 04             	mov    %edx,0x4(%ebx)
801015ae:	75 40                	jne    801015f0 <fileclose+0x70>
    release(&ftable.lock);
    return;
  }
  ff = *f;
801015b0:	0f b6 43 09          	movzbl 0x9(%ebx),%eax
  f->ref = 0;
  f->type = FD_NONE;
  release(&ftable.lock);
801015b4:	83 ec 0c             	sub    $0xc,%esp
  ff = *f;
801015b7:	8b 3b                	mov    (%ebx),%edi
  f->type = FD_NONE;
801015b9:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  ff = *f;
801015bf:	8b 73 0c             	mov    0xc(%ebx),%esi
801015c2:	88 45 e7             	mov    %al,-0x19(%ebp)
801015c5:	8b 43 10             	mov    0x10(%ebx),%eax
  release(&ftable.lock);
801015c8:	68 60 ff 10 80       	push   $0x8010ff60
  ff = *f;
801015cd:	89 45 e0             	mov    %eax,-0x20(%ebp)
  release(&ftable.lock);
801015d0:	e8 1b 3b 00 00       	call   801050f0 <release>

  if(ff.type == FD_PIPE)
801015d5:	83 c4 10             	add    $0x10,%esp
801015d8:	83 ff 01             	cmp    $0x1,%edi
801015db:	74 53                	je     80101630 <fileclose+0xb0>
    pipeclose(ff.pipe, ff.writable);
  else if(ff.type == FD_INODE){
801015dd:	83 ff 02             	cmp    $0x2,%edi
801015e0:	74 26                	je     80101608 <fileclose+0x88>
    begin_op();
    iput(ff.ip);
    end_op();
  }
}
801015e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
801015e5:	5b                   	pop    %ebx
801015e6:	5e                   	pop    %esi
801015e7:	5f                   	pop    %edi
801015e8:	5d                   	pop    %ebp
801015e9:	c3                   	ret    
801015ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    release(&ftable.lock);
801015f0:	c7 45 08 60 ff 10 80 	movl   $0x8010ff60,0x8(%ebp)
}
801015f7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801015fa:	5b                   	pop    %ebx
801015fb:	5e                   	pop    %esi
801015fc:	5f                   	pop    %edi
801015fd:	5d                   	pop    %ebp
    release(&ftable.lock);
801015fe:	e9 ed 3a 00 00       	jmp    801050f0 <release>
80101603:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101607:	90                   	nop
    begin_op();
80101608:	e8 93 1e 00 00       	call   801034a0 <begin_op>
    iput(ff.ip);
8010160d:	83 ec 0c             	sub    $0xc,%esp
80101610:	ff 75 e0             	push   -0x20(%ebp)
80101613:	e8 78 09 00 00       	call   80101f90 <iput>
    end_op();
80101618:	83 c4 10             	add    $0x10,%esp
}
8010161b:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010161e:	5b                   	pop    %ebx
8010161f:	5e                   	pop    %esi
80101620:	5f                   	pop    %edi
80101621:	5d                   	pop    %ebp
    end_op();
80101622:	e9 e9 1e 00 00       	jmp    80103510 <end_op>
80101627:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010162e:	66 90                	xchg   %ax,%ax
    pipeclose(ff.pipe, ff.writable);
80101630:	0f be 5d e7          	movsbl -0x19(%ebp),%ebx
80101634:	83 ec 08             	sub    $0x8,%esp
80101637:	53                   	push   %ebx
80101638:	56                   	push   %esi
80101639:	e8 62 26 00 00       	call   80103ca0 <pipeclose>
8010163e:	83 c4 10             	add    $0x10,%esp
}
80101641:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101644:	5b                   	pop    %ebx
80101645:	5e                   	pop    %esi
80101646:	5f                   	pop    %edi
80101647:	5d                   	pop    %ebp
80101648:	c3                   	ret    
    panic("fileclose");
80101649:	83 ec 0c             	sub    $0xc,%esp
8010164c:	68 34 80 10 80       	push   $0x80108034
80101651:	e8 3a ed ff ff       	call   80100390 <panic>
80101656:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010165d:	8d 76 00             	lea    0x0(%esi),%esi

80101660 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
80101660:	f3 0f 1e fb          	endbr32 
80101664:	55                   	push   %ebp
80101665:	89 e5                	mov    %esp,%ebp
80101667:	53                   	push   %ebx
80101668:	83 ec 04             	sub    $0x4,%esp
8010166b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(f->type == FD_INODE){
8010166e:	83 3b 02             	cmpl   $0x2,(%ebx)
80101671:	75 2d                	jne    801016a0 <filestat+0x40>
    ilock(f->ip);
80101673:	83 ec 0c             	sub    $0xc,%esp
80101676:	ff 73 10             	push   0x10(%ebx)
80101679:	e8 e2 07 00 00       	call   80101e60 <ilock>
    stati(f->ip, st);
8010167e:	58                   	pop    %eax
8010167f:	5a                   	pop    %edx
80101680:	ff 75 0c             	push   0xc(%ebp)
80101683:	ff 73 10             	push   0x10(%ebx)
80101686:	e8 c5 0a 00 00       	call   80102150 <stati>
    iunlock(f->ip);
8010168b:	59                   	pop    %ecx
8010168c:	ff 73 10             	push   0x10(%ebx)
8010168f:	e8 ac 08 00 00       	call   80101f40 <iunlock>
    return 0;
  }
  return -1;
}
80101694:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    return 0;
80101697:	83 c4 10             	add    $0x10,%esp
8010169a:	31 c0                	xor    %eax,%eax
}
8010169c:	c9                   	leave  
8010169d:	c3                   	ret    
8010169e:	66 90                	xchg   %ax,%ax
801016a0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  return -1;
801016a3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801016a8:	c9                   	leave  
801016a9:	c3                   	ret    
801016aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801016b0 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
801016b0:	f3 0f 1e fb          	endbr32 
801016b4:	55                   	push   %ebp
801016b5:	89 e5                	mov    %esp,%ebp
801016b7:	57                   	push   %edi
801016b8:	56                   	push   %esi
801016b9:	53                   	push   %ebx
801016ba:	83 ec 0c             	sub    $0xc,%esp
801016bd:	8b 5d 08             	mov    0x8(%ebp),%ebx
801016c0:	8b 75 0c             	mov    0xc(%ebp),%esi
801016c3:	8b 7d 10             	mov    0x10(%ebp),%edi
  int r;

  if(f->readable == 0)
801016c6:	80 7b 08 00          	cmpb   $0x0,0x8(%ebx)
801016ca:	74 64                	je     80101730 <fileread+0x80>
    return -1;
  if(f->type == FD_PIPE)
801016cc:	8b 03                	mov    (%ebx),%eax
801016ce:	83 f8 01             	cmp    $0x1,%eax
801016d1:	74 45                	je     80101718 <fileread+0x68>
    return piperead(f->pipe, addr, n);
  if(f->type == FD_INODE){
801016d3:	83 f8 02             	cmp    $0x2,%eax
801016d6:	75 5f                	jne    80101737 <fileread+0x87>
    ilock(f->ip);
801016d8:	83 ec 0c             	sub    $0xc,%esp
801016db:	ff 73 10             	push   0x10(%ebx)
801016de:	e8 7d 07 00 00       	call   80101e60 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
801016e3:	57                   	push   %edi
801016e4:	ff 73 14             	push   0x14(%ebx)
801016e7:	56                   	push   %esi
801016e8:	ff 73 10             	push   0x10(%ebx)
801016eb:	e8 90 0a 00 00       	call   80102180 <readi>
801016f0:	83 c4 20             	add    $0x20,%esp
801016f3:	89 c6                	mov    %eax,%esi
801016f5:	85 c0                	test   %eax,%eax
801016f7:	7e 03                	jle    801016fc <fileread+0x4c>
      f->off += r;
801016f9:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
801016fc:	83 ec 0c             	sub    $0xc,%esp
801016ff:	ff 73 10             	push   0x10(%ebx)
80101702:	e8 39 08 00 00       	call   80101f40 <iunlock>
    return r;
80101707:	83 c4 10             	add    $0x10,%esp
  }
  panic("fileread");
}
8010170a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010170d:	89 f0                	mov    %esi,%eax
8010170f:	5b                   	pop    %ebx
80101710:	5e                   	pop    %esi
80101711:	5f                   	pop    %edi
80101712:	5d                   	pop    %ebp
80101713:	c3                   	ret    
80101714:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return piperead(f->pipe, addr, n);
80101718:	8b 43 0c             	mov    0xc(%ebx),%eax
8010171b:	89 45 08             	mov    %eax,0x8(%ebp)
}
8010171e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101721:	5b                   	pop    %ebx
80101722:	5e                   	pop    %esi
80101723:	5f                   	pop    %edi
80101724:	5d                   	pop    %ebp
    return piperead(f->pipe, addr, n);
80101725:	e9 16 27 00 00       	jmp    80103e40 <piperead>
8010172a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return -1;
80101730:	be ff ff ff ff       	mov    $0xffffffff,%esi
80101735:	eb d3                	jmp    8010170a <fileread+0x5a>
  panic("fileread");
80101737:	83 ec 0c             	sub    $0xc,%esp
8010173a:	68 3e 80 10 80       	push   $0x8010803e
8010173f:	e8 4c ec ff ff       	call   80100390 <panic>
80101744:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010174b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010174f:	90                   	nop

80101750 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
80101750:	f3 0f 1e fb          	endbr32 
80101754:	55                   	push   %ebp
80101755:	89 e5                	mov    %esp,%ebp
80101757:	57                   	push   %edi
80101758:	56                   	push   %esi
80101759:	53                   	push   %ebx
8010175a:	83 ec 1c             	sub    $0x1c,%esp
8010175d:	8b 45 0c             	mov    0xc(%ebp),%eax
80101760:	8b 75 08             	mov    0x8(%ebp),%esi
80101763:	89 45 dc             	mov    %eax,-0x24(%ebp)
80101766:	8b 45 10             	mov    0x10(%ebp),%eax
  int r;

  if(f->writable == 0)
80101769:	80 7e 09 00          	cmpb   $0x0,0x9(%esi)
{
8010176d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(f->writable == 0)
80101770:	0f 84 c1 00 00 00    	je     80101837 <filewrite+0xe7>
    return -1;
  if(f->type == FD_PIPE)
80101776:	8b 06                	mov    (%esi),%eax
80101778:	83 f8 01             	cmp    $0x1,%eax
8010177b:	0f 84 c3 00 00 00    	je     80101844 <filewrite+0xf4>
    return pipewrite(f->pipe, addr, n);
  if(f->type == FD_INODE){
80101781:	83 f8 02             	cmp    $0x2,%eax
80101784:	0f 85 cc 00 00 00    	jne    80101856 <filewrite+0x106>
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
8010178a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    int i = 0;
8010178d:	31 ff                	xor    %edi,%edi
    while(i < n){
8010178f:	85 c0                	test   %eax,%eax
80101791:	7f 34                	jg     801017c7 <filewrite+0x77>
80101793:	e9 98 00 00 00       	jmp    80101830 <filewrite+0xe0>
80101798:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010179f:	90                   	nop
        n1 = max;

      begin_op();
      ilock(f->ip);
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
        f->off += r;
801017a0:	01 46 14             	add    %eax,0x14(%esi)
      iunlock(f->ip);
801017a3:	83 ec 0c             	sub    $0xc,%esp
801017a6:	ff 76 10             	push   0x10(%esi)
        f->off += r;
801017a9:	89 45 e0             	mov    %eax,-0x20(%ebp)
      iunlock(f->ip);
801017ac:	e8 8f 07 00 00       	call   80101f40 <iunlock>
      end_op();
801017b1:	e8 5a 1d 00 00       	call   80103510 <end_op>

      if(r < 0)
        break;
      if(r != n1)
801017b6:	8b 45 e0             	mov    -0x20(%ebp),%eax
801017b9:	83 c4 10             	add    $0x10,%esp
801017bc:	39 c3                	cmp    %eax,%ebx
801017be:	75 60                	jne    80101820 <filewrite+0xd0>
        panic("short filewrite");
      i += r;
801017c0:	01 df                	add    %ebx,%edi
    while(i < n){
801017c2:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
801017c5:	7e 69                	jle    80101830 <filewrite+0xe0>
      int n1 = n - i;
801017c7:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801017ca:	b8 00 06 00 00       	mov    $0x600,%eax
801017cf:	29 fb                	sub    %edi,%ebx
      if(n1 > max)
801017d1:	81 fb 00 06 00 00    	cmp    $0x600,%ebx
801017d7:	0f 4f d8             	cmovg  %eax,%ebx
      begin_op();
801017da:	e8 c1 1c 00 00       	call   801034a0 <begin_op>
      ilock(f->ip);
801017df:	83 ec 0c             	sub    $0xc,%esp
801017e2:	ff 76 10             	push   0x10(%esi)
801017e5:	e8 76 06 00 00       	call   80101e60 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
801017ea:	8b 45 dc             	mov    -0x24(%ebp),%eax
801017ed:	53                   	push   %ebx
801017ee:	ff 76 14             	push   0x14(%esi)
801017f1:	01 f8                	add    %edi,%eax
801017f3:	50                   	push   %eax
801017f4:	ff 76 10             	push   0x10(%esi)
801017f7:	e8 84 0a 00 00       	call   80102280 <writei>
801017fc:	83 c4 20             	add    $0x20,%esp
801017ff:	85 c0                	test   %eax,%eax
80101801:	7f 9d                	jg     801017a0 <filewrite+0x50>
      iunlock(f->ip);
80101803:	83 ec 0c             	sub    $0xc,%esp
80101806:	ff 76 10             	push   0x10(%esi)
80101809:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010180c:	e8 2f 07 00 00       	call   80101f40 <iunlock>
      end_op();
80101811:	e8 fa 1c 00 00       	call   80103510 <end_op>
      if(r < 0)
80101816:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101819:	83 c4 10             	add    $0x10,%esp
8010181c:	85 c0                	test   %eax,%eax
8010181e:	75 17                	jne    80101837 <filewrite+0xe7>
        panic("short filewrite");
80101820:	83 ec 0c             	sub    $0xc,%esp
80101823:	68 47 80 10 80       	push   $0x80108047
80101828:	e8 63 eb ff ff       	call   80100390 <panic>
8010182d:	8d 76 00             	lea    0x0(%esi),%esi
    }
    return i == n ? n : -1;
80101830:	89 f8                	mov    %edi,%eax
80101832:	3b 7d e4             	cmp    -0x1c(%ebp),%edi
80101835:	74 05                	je     8010183c <filewrite+0xec>
80101837:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  panic("filewrite");
}
8010183c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010183f:	5b                   	pop    %ebx
80101840:	5e                   	pop    %esi
80101841:	5f                   	pop    %edi
80101842:	5d                   	pop    %ebp
80101843:	c3                   	ret    
    return pipewrite(f->pipe, addr, n);
80101844:	8b 46 0c             	mov    0xc(%esi),%eax
80101847:	89 45 08             	mov    %eax,0x8(%ebp)
}
8010184a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010184d:	5b                   	pop    %ebx
8010184e:	5e                   	pop    %esi
8010184f:	5f                   	pop    %edi
80101850:	5d                   	pop    %ebp
    return pipewrite(f->pipe, addr, n);
80101851:	e9 ea 24 00 00       	jmp    80103d40 <pipewrite>
  panic("filewrite");
80101856:	83 ec 0c             	sub    $0xc,%esp
80101859:	68 4d 80 10 80       	push   $0x8010804d
8010185e:	e8 2d eb ff ff       	call   80100390 <panic>
80101863:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010186a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101870 <fileptr>:

struct inode*
fileptr(int fd) {
80101870:	f3 0f 1e fb          	endbr32 
80101874:	55                   	push   %ebp
80101875:	89 e5                	mov    %esp,%ebp
80101877:	8b 45 08             	mov    0x8(%ebp),%eax
  return ftable.file[fd].ip;
8010187a:	5d                   	pop    %ebp
  return ftable.file[fd].ip;
8010187b:	8d 04 40             	lea    (%eax,%eax,2),%eax
8010187e:	8b 04 c5 a4 ff 10 80 	mov    -0x7fef005c(,%eax,8),%eax
80101885:	c3                   	ret    
80101886:	66 90                	xchg   %ax,%ax
80101888:	66 90                	xchg   %ax,%ax
8010188a:	66 90                	xchg   %ax,%ax
8010188c:	66 90                	xchg   %ax,%ax
8010188e:	66 90                	xchg   %ax,%ax

80101890 <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
80101890:	55                   	push   %ebp
80101891:	89 c1                	mov    %eax,%ecx
  struct buf *bp;
  int bi, m;

  bp = bread(dev, BBLOCK(b, sb));
80101893:	89 d0                	mov    %edx,%eax
80101895:	c1 e8 0c             	shr    $0xc,%eax
80101898:	03 05 cc 25 11 80    	add    0x801125cc,%eax
{
8010189e:	89 e5                	mov    %esp,%ebp
801018a0:	56                   	push   %esi
801018a1:	53                   	push   %ebx
801018a2:	89 d3                	mov    %edx,%ebx
  bp = bread(dev, BBLOCK(b, sb));
801018a4:	83 ec 08             	sub    $0x8,%esp
801018a7:	50                   	push   %eax
801018a8:	51                   	push   %ecx
801018a9:	e8 22 e8 ff ff       	call   801000d0 <bread>
  bi = b % BPB;
  m = 1 << (bi % 8);
801018ae:	89 d9                	mov    %ebx,%ecx
  if((bp->data[bi/8] & m) == 0)
801018b0:	c1 fb 03             	sar    $0x3,%ebx
801018b3:	83 c4 10             	add    $0x10,%esp
  bp = bread(dev, BBLOCK(b, sb));
801018b6:	89 c6                	mov    %eax,%esi
  m = 1 << (bi % 8);
801018b8:	83 e1 07             	and    $0x7,%ecx
801018bb:	b8 01 00 00 00       	mov    $0x1,%eax
  if((bp->data[bi/8] & m) == 0)
801018c0:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
  m = 1 << (bi % 8);
801018c6:	d3 e0                	shl    %cl,%eax
  if((bp->data[bi/8] & m) == 0)
801018c8:	0f b6 4c 1e 5c       	movzbl 0x5c(%esi,%ebx,1),%ecx
801018cd:	85 c1                	test   %eax,%ecx
801018cf:	74 23                	je     801018f4 <bfree+0x64>
    panic("freeing free block");
  bp->data[bi/8] &= ~m;
801018d1:	f7 d0                	not    %eax
  log_write(bp);
801018d3:	83 ec 0c             	sub    $0xc,%esp
  bp->data[bi/8] &= ~m;
801018d6:	21 c8                	and    %ecx,%eax
801018d8:	88 44 1e 5c          	mov    %al,0x5c(%esi,%ebx,1)
  log_write(bp);
801018dc:	56                   	push   %esi
801018dd:	e8 9e 1d 00 00       	call   80103680 <log_write>
  brelse(bp);
801018e2:	89 34 24             	mov    %esi,(%esp)
801018e5:	e8 06 e9 ff ff       	call   801001f0 <brelse>
}
801018ea:	83 c4 10             	add    $0x10,%esp
801018ed:	8d 65 f8             	lea    -0x8(%ebp),%esp
801018f0:	5b                   	pop    %ebx
801018f1:	5e                   	pop    %esi
801018f2:	5d                   	pop    %ebp
801018f3:	c3                   	ret    
    panic("freeing free block");
801018f4:	83 ec 0c             	sub    $0xc,%esp
801018f7:	68 57 80 10 80       	push   $0x80108057
801018fc:	e8 8f ea ff ff       	call   80100390 <panic>
80101901:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101908:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010190f:	90                   	nop

80101910 <balloc>:
{
80101910:	55                   	push   %ebp
80101911:	89 e5                	mov    %esp,%ebp
80101913:	57                   	push   %edi
80101914:	56                   	push   %esi
80101915:	53                   	push   %ebx
80101916:	83 ec 1c             	sub    $0x1c,%esp
  for(b = 0; b < sb.size; b += BPB){
80101919:	8b 0d b4 25 11 80    	mov    0x801125b4,%ecx
{
8010191f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101922:	85 c9                	test   %ecx,%ecx
80101924:	0f 84 87 00 00 00    	je     801019b1 <balloc+0xa1>
8010192a:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
    bp = bread(dev, BBLOCK(b, sb));
80101931:	8b 75 dc             	mov    -0x24(%ebp),%esi
80101934:	83 ec 08             	sub    $0x8,%esp
80101937:	89 f0                	mov    %esi,%eax
80101939:	c1 f8 0c             	sar    $0xc,%eax
8010193c:	03 05 cc 25 11 80    	add    0x801125cc,%eax
80101942:	50                   	push   %eax
80101943:	ff 75 d8             	push   -0x28(%ebp)
80101946:	e8 85 e7 ff ff       	call   801000d0 <bread>
8010194b:	83 c4 10             	add    $0x10,%esp
8010194e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101951:	a1 b4 25 11 80       	mov    0x801125b4,%eax
80101956:	89 45 e0             	mov    %eax,-0x20(%ebp)
80101959:	31 c0                	xor    %eax,%eax
8010195b:	eb 2f                	jmp    8010198c <balloc+0x7c>
8010195d:	8d 76 00             	lea    0x0(%esi),%esi
      m = 1 << (bi % 8);
80101960:	89 c1                	mov    %eax,%ecx
80101962:	bb 01 00 00 00       	mov    $0x1,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
80101967:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      m = 1 << (bi % 8);
8010196a:	83 e1 07             	and    $0x7,%ecx
8010196d:	d3 e3                	shl    %cl,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
8010196f:	89 c1                	mov    %eax,%ecx
80101971:	c1 f9 03             	sar    $0x3,%ecx
80101974:	0f b6 7c 0a 5c       	movzbl 0x5c(%edx,%ecx,1),%edi
80101979:	89 fa                	mov    %edi,%edx
8010197b:	85 df                	test   %ebx,%edi
8010197d:	74 41                	je     801019c0 <balloc+0xb0>
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
8010197f:	83 c0 01             	add    $0x1,%eax
80101982:	83 c6 01             	add    $0x1,%esi
80101985:	3d 00 10 00 00       	cmp    $0x1000,%eax
8010198a:	74 05                	je     80101991 <balloc+0x81>
8010198c:	39 75 e0             	cmp    %esi,-0x20(%ebp)
8010198f:	77 cf                	ja     80101960 <balloc+0x50>
    brelse(bp);
80101991:	83 ec 0c             	sub    $0xc,%esp
80101994:	ff 75 e4             	push   -0x1c(%ebp)
80101997:	e8 54 e8 ff ff       	call   801001f0 <brelse>
  for(b = 0; b < sb.size; b += BPB){
8010199c:	81 45 dc 00 10 00 00 	addl   $0x1000,-0x24(%ebp)
801019a3:	83 c4 10             	add    $0x10,%esp
801019a6:	8b 45 dc             	mov    -0x24(%ebp),%eax
801019a9:	39 05 b4 25 11 80    	cmp    %eax,0x801125b4
801019af:	77 80                	ja     80101931 <balloc+0x21>
  panic("balloc: out of blocks");
801019b1:	83 ec 0c             	sub    $0xc,%esp
801019b4:	68 6a 80 10 80       	push   $0x8010806a
801019b9:	e8 d2 e9 ff ff       	call   80100390 <panic>
801019be:	66 90                	xchg   %ax,%ax
        bp->data[bi/8] |= m;  // Mark block in use.
801019c0:	8b 7d e4             	mov    -0x1c(%ebp),%edi
        log_write(bp);
801019c3:	83 ec 0c             	sub    $0xc,%esp
        bp->data[bi/8] |= m;  // Mark block in use.
801019c6:	09 da                	or     %ebx,%edx
801019c8:	88 54 0f 5c          	mov    %dl,0x5c(%edi,%ecx,1)
        log_write(bp);
801019cc:	57                   	push   %edi
801019cd:	e8 ae 1c 00 00       	call   80103680 <log_write>
        brelse(bp);
801019d2:	89 3c 24             	mov    %edi,(%esp)
801019d5:	e8 16 e8 ff ff       	call   801001f0 <brelse>
  bp = bread(dev, bno);
801019da:	58                   	pop    %eax
801019db:	5a                   	pop    %edx
801019dc:	56                   	push   %esi
801019dd:	ff 75 d8             	push   -0x28(%ebp)
801019e0:	e8 eb e6 ff ff       	call   801000d0 <bread>
  memset(bp->data, 0, BSIZE);
801019e5:	83 c4 0c             	add    $0xc,%esp
  bp = bread(dev, bno);
801019e8:	89 c3                	mov    %eax,%ebx
  memset(bp->data, 0, BSIZE);
801019ea:	8d 40 5c             	lea    0x5c(%eax),%eax
801019ed:	68 00 02 00 00       	push   $0x200
801019f2:	6a 00                	push   $0x0
801019f4:	50                   	push   %eax
801019f5:	e8 36 38 00 00       	call   80105230 <memset>
  log_write(bp);
801019fa:	89 1c 24             	mov    %ebx,(%esp)
801019fd:	e8 7e 1c 00 00       	call   80103680 <log_write>
  brelse(bp);
80101a02:	89 1c 24             	mov    %ebx,(%esp)
80101a05:	e8 e6 e7 ff ff       	call   801001f0 <brelse>
}
80101a0a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101a0d:	89 f0                	mov    %esi,%eax
80101a0f:	5b                   	pop    %ebx
80101a10:	5e                   	pop    %esi
80101a11:	5f                   	pop    %edi
80101a12:	5d                   	pop    %ebp
80101a13:	c3                   	ret    
80101a14:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101a1b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101a1f:	90                   	nop

80101a20 <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
80101a20:	55                   	push   %ebp
80101a21:	89 e5                	mov    %esp,%ebp
80101a23:	57                   	push   %edi
80101a24:	89 c7                	mov    %eax,%edi
80101a26:	56                   	push   %esi
  struct inode *ip, *empty;

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
80101a27:	31 f6                	xor    %esi,%esi
{
80101a29:	53                   	push   %ebx
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101a2a:	bb 94 09 11 80       	mov    $0x80110994,%ebx
{
80101a2f:	83 ec 28             	sub    $0x28,%esp
80101a32:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  acquire(&icache.lock);
80101a35:	68 60 09 11 80       	push   $0x80110960
80101a3a:	e8 21 37 00 00       	call   80105160 <acquire>
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101a3f:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  acquire(&icache.lock);
80101a42:	83 c4 10             	add    $0x10,%esp
80101a45:	eb 1b                	jmp    80101a62 <iget+0x42>
80101a47:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101a4e:	66 90                	xchg   %ax,%ax
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101a50:	39 3b                	cmp    %edi,(%ebx)
80101a52:	74 6c                	je     80101ac0 <iget+0xa0>
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101a54:	81 c3 90 00 00 00    	add    $0x90,%ebx
80101a5a:	81 fb b4 25 11 80    	cmp    $0x801125b4,%ebx
80101a60:	73 26                	jae    80101a88 <iget+0x68>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101a62:	8b 43 08             	mov    0x8(%ebx),%eax
80101a65:	85 c0                	test   %eax,%eax
80101a67:	7f e7                	jg     80101a50 <iget+0x30>
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101a69:	85 f6                	test   %esi,%esi
80101a6b:	75 e7                	jne    80101a54 <iget+0x34>
80101a6d:	85 c0                	test   %eax,%eax
80101a6f:	75 76                	jne    80101ae7 <iget+0xc7>
80101a71:	89 de                	mov    %ebx,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101a73:	81 c3 90 00 00 00    	add    $0x90,%ebx
80101a79:	81 fb b4 25 11 80    	cmp    $0x801125b4,%ebx
80101a7f:	72 e1                	jb     80101a62 <iget+0x42>
80101a81:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
80101a88:	85 f6                	test   %esi,%esi
80101a8a:	74 79                	je     80101b05 <iget+0xe5>
  ip = empty;
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->valid = 0;
  release(&icache.lock);
80101a8c:	83 ec 0c             	sub    $0xc,%esp
  ip->dev = dev;
80101a8f:	89 3e                	mov    %edi,(%esi)
  ip->inum = inum;
80101a91:	89 56 04             	mov    %edx,0x4(%esi)
  ip->ref = 1;
80101a94:	c7 46 08 01 00 00 00 	movl   $0x1,0x8(%esi)
  ip->valid = 0;
80101a9b:	c7 46 4c 00 00 00 00 	movl   $0x0,0x4c(%esi)
  release(&icache.lock);
80101aa2:	68 60 09 11 80       	push   $0x80110960
80101aa7:	e8 44 36 00 00       	call   801050f0 <release>

  return ip;
80101aac:	83 c4 10             	add    $0x10,%esp
}
80101aaf:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101ab2:	89 f0                	mov    %esi,%eax
80101ab4:	5b                   	pop    %ebx
80101ab5:	5e                   	pop    %esi
80101ab6:	5f                   	pop    %edi
80101ab7:	5d                   	pop    %ebp
80101ab8:	c3                   	ret    
80101ab9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101ac0:	39 53 04             	cmp    %edx,0x4(%ebx)
80101ac3:	75 8f                	jne    80101a54 <iget+0x34>
      release(&icache.lock);
80101ac5:	83 ec 0c             	sub    $0xc,%esp
      ip->ref++;
80101ac8:	83 c0 01             	add    $0x1,%eax
      return ip;
80101acb:	89 de                	mov    %ebx,%esi
      release(&icache.lock);
80101acd:	68 60 09 11 80       	push   $0x80110960
      ip->ref++;
80101ad2:	89 43 08             	mov    %eax,0x8(%ebx)
      release(&icache.lock);
80101ad5:	e8 16 36 00 00       	call   801050f0 <release>
      return ip;
80101ada:	83 c4 10             	add    $0x10,%esp
}
80101add:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101ae0:	89 f0                	mov    %esi,%eax
80101ae2:	5b                   	pop    %ebx
80101ae3:	5e                   	pop    %esi
80101ae4:	5f                   	pop    %edi
80101ae5:	5d                   	pop    %ebp
80101ae6:	c3                   	ret    
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101ae7:	81 c3 90 00 00 00    	add    $0x90,%ebx
80101aed:	81 fb b4 25 11 80    	cmp    $0x801125b4,%ebx
80101af3:	73 10                	jae    80101b05 <iget+0xe5>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101af5:	8b 43 08             	mov    0x8(%ebx),%eax
80101af8:	85 c0                	test   %eax,%eax
80101afa:	0f 8f 50 ff ff ff    	jg     80101a50 <iget+0x30>
80101b00:	e9 68 ff ff ff       	jmp    80101a6d <iget+0x4d>
    panic("iget: no inodes");
80101b05:	83 ec 0c             	sub    $0xc,%esp
80101b08:	68 80 80 10 80       	push   $0x80108080
80101b0d:	e8 7e e8 ff ff       	call   80100390 <panic>
80101b12:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101b19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101b20 <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
80101b20:	55                   	push   %ebp
80101b21:	89 e5                	mov    %esp,%ebp
80101b23:	57                   	push   %edi
80101b24:	56                   	push   %esi
80101b25:	89 c6                	mov    %eax,%esi
80101b27:	53                   	push   %ebx
80101b28:	83 ec 1c             	sub    $0x1c,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
80101b2b:	83 fa 0b             	cmp    $0xb,%edx
80101b2e:	0f 86 8c 00 00 00    	jbe    80101bc0 <bmap+0xa0>
    if((addr = ip->addrs[bn]) == 0)
      ip->addrs[bn] = addr = balloc(ip->dev);
    return addr;
  }
  bn -= NDIRECT;
80101b34:	8d 5a f4             	lea    -0xc(%edx),%ebx

  if(bn < NINDIRECT){
80101b37:	83 fb 7f             	cmp    $0x7f,%ebx
80101b3a:	0f 87 a2 00 00 00    	ja     80101be2 <bmap+0xc2>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
80101b40:	8b 80 8c 00 00 00    	mov    0x8c(%eax),%eax
80101b46:	85 c0                	test   %eax,%eax
80101b48:	74 5e                	je     80101ba8 <bmap+0x88>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
    bp = bread(ip->dev, addr);
80101b4a:	83 ec 08             	sub    $0x8,%esp
80101b4d:	50                   	push   %eax
80101b4e:	ff 36                	push   (%esi)
80101b50:	e8 7b e5 ff ff       	call   801000d0 <bread>
    a = (uint*)bp->data;
    if((addr = a[bn]) == 0){
80101b55:	83 c4 10             	add    $0x10,%esp
80101b58:	8d 5c 98 5c          	lea    0x5c(%eax,%ebx,4),%ebx
    bp = bread(ip->dev, addr);
80101b5c:	89 c2                	mov    %eax,%edx
    if((addr = a[bn]) == 0){
80101b5e:	8b 3b                	mov    (%ebx),%edi
80101b60:	85 ff                	test   %edi,%edi
80101b62:	74 1c                	je     80101b80 <bmap+0x60>
      a[bn] = addr = balloc(ip->dev);
      log_write(bp);
    }
    brelse(bp);
80101b64:	83 ec 0c             	sub    $0xc,%esp
80101b67:	52                   	push   %edx
80101b68:	e8 83 e6 ff ff       	call   801001f0 <brelse>
80101b6d:	83 c4 10             	add    $0x10,%esp
    return addr;
  }

  panic("bmap: out of range");
}
80101b70:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101b73:	89 f8                	mov    %edi,%eax
80101b75:	5b                   	pop    %ebx
80101b76:	5e                   	pop    %esi
80101b77:	5f                   	pop    %edi
80101b78:	5d                   	pop    %ebp
80101b79:	c3                   	ret    
80101b7a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80101b80:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      a[bn] = addr = balloc(ip->dev);
80101b83:	8b 06                	mov    (%esi),%eax
80101b85:	e8 86 fd ff ff       	call   80101910 <balloc>
      log_write(bp);
80101b8a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101b8d:	83 ec 0c             	sub    $0xc,%esp
      a[bn] = addr = balloc(ip->dev);
80101b90:	89 03                	mov    %eax,(%ebx)
80101b92:	89 c7                	mov    %eax,%edi
      log_write(bp);
80101b94:	52                   	push   %edx
80101b95:	e8 e6 1a 00 00       	call   80103680 <log_write>
80101b9a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101b9d:	83 c4 10             	add    $0x10,%esp
80101ba0:	eb c2                	jmp    80101b64 <bmap+0x44>
80101ba2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80101ba8:	8b 06                	mov    (%esi),%eax
80101baa:	e8 61 fd ff ff       	call   80101910 <balloc>
80101baf:	89 86 8c 00 00 00    	mov    %eax,0x8c(%esi)
80101bb5:	eb 93                	jmp    80101b4a <bmap+0x2a>
80101bb7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101bbe:	66 90                	xchg   %ax,%ax
    if((addr = ip->addrs[bn]) == 0)
80101bc0:	8d 5a 14             	lea    0x14(%edx),%ebx
80101bc3:	8b 7c 98 0c          	mov    0xc(%eax,%ebx,4),%edi
80101bc7:	85 ff                	test   %edi,%edi
80101bc9:	75 a5                	jne    80101b70 <bmap+0x50>
      ip->addrs[bn] = addr = balloc(ip->dev);
80101bcb:	8b 00                	mov    (%eax),%eax
80101bcd:	e8 3e fd ff ff       	call   80101910 <balloc>
80101bd2:	89 44 9e 0c          	mov    %eax,0xc(%esi,%ebx,4)
80101bd6:	89 c7                	mov    %eax,%edi
}
80101bd8:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101bdb:	5b                   	pop    %ebx
80101bdc:	89 f8                	mov    %edi,%eax
80101bde:	5e                   	pop    %esi
80101bdf:	5f                   	pop    %edi
80101be0:	5d                   	pop    %ebp
80101be1:	c3                   	ret    
  panic("bmap: out of range");
80101be2:	83 ec 0c             	sub    $0xc,%esp
80101be5:	68 90 80 10 80       	push   $0x80108090
80101bea:	e8 a1 e7 ff ff       	call   80100390 <panic>
80101bef:	90                   	nop

80101bf0 <readsb>:
{
80101bf0:	f3 0f 1e fb          	endbr32 
80101bf4:	55                   	push   %ebp
80101bf5:	89 e5                	mov    %esp,%ebp
80101bf7:	56                   	push   %esi
80101bf8:	53                   	push   %ebx
80101bf9:	8b 75 0c             	mov    0xc(%ebp),%esi
  bp = bread(dev, 1);
80101bfc:	83 ec 08             	sub    $0x8,%esp
80101bff:	6a 01                	push   $0x1
80101c01:	ff 75 08             	push   0x8(%ebp)
80101c04:	e8 c7 e4 ff ff       	call   801000d0 <bread>
  memmove(sb, bp->data, sizeof(*sb));
80101c09:	83 c4 0c             	add    $0xc,%esp
  bp = bread(dev, 1);
80101c0c:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
80101c0e:	8d 40 5c             	lea    0x5c(%eax),%eax
80101c11:	6a 1c                	push   $0x1c
80101c13:	50                   	push   %eax
80101c14:	56                   	push   %esi
80101c15:	e8 b6 36 00 00       	call   801052d0 <memmove>
  brelse(bp);
80101c1a:	89 5d 08             	mov    %ebx,0x8(%ebp)
80101c1d:	83 c4 10             	add    $0x10,%esp
}
80101c20:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101c23:	5b                   	pop    %ebx
80101c24:	5e                   	pop    %esi
80101c25:	5d                   	pop    %ebp
  brelse(bp);
80101c26:	e9 c5 e5 ff ff       	jmp    801001f0 <brelse>
80101c2b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101c2f:	90                   	nop

80101c30 <iinit>:
{
80101c30:	f3 0f 1e fb          	endbr32 
80101c34:	55                   	push   %ebp
80101c35:	89 e5                	mov    %esp,%ebp
80101c37:	53                   	push   %ebx
80101c38:	bb a0 09 11 80       	mov    $0x801109a0,%ebx
80101c3d:	83 ec 0c             	sub    $0xc,%esp
  initlock(&icache.lock, "icache");
80101c40:	68 a3 80 10 80       	push   $0x801080a3
80101c45:	68 60 09 11 80       	push   $0x80110960
80101c4a:	e8 11 33 00 00       	call   80104f60 <initlock>
  for(i = 0; i < NINODE; i++) {
80101c4f:	83 c4 10             	add    $0x10,%esp
80101c52:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    initsleeplock(&icache.inode[i].lock, "inode");
80101c58:	83 ec 08             	sub    $0x8,%esp
80101c5b:	68 aa 80 10 80       	push   $0x801080aa
80101c60:	53                   	push   %ebx
  for(i = 0; i < NINODE; i++) {
80101c61:	81 c3 90 00 00 00    	add    $0x90,%ebx
    initsleeplock(&icache.inode[i].lock, "inode");
80101c67:	e8 b4 31 00 00       	call   80104e20 <initsleeplock>
  for(i = 0; i < NINODE; i++) {
80101c6c:	83 c4 10             	add    $0x10,%esp
80101c6f:	81 fb c0 25 11 80    	cmp    $0x801125c0,%ebx
80101c75:	75 e1                	jne    80101c58 <iinit+0x28>
  bp = bread(dev, 1);
80101c77:	83 ec 08             	sub    $0x8,%esp
80101c7a:	6a 01                	push   $0x1
80101c7c:	ff 75 08             	push   0x8(%ebp)
80101c7f:	e8 4c e4 ff ff       	call   801000d0 <bread>
  memmove(sb, bp->data, sizeof(*sb));
80101c84:	83 c4 0c             	add    $0xc,%esp
  bp = bread(dev, 1);
80101c87:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
80101c89:	8d 40 5c             	lea    0x5c(%eax),%eax
80101c8c:	6a 1c                	push   $0x1c
80101c8e:	50                   	push   %eax
80101c8f:	68 b4 25 11 80       	push   $0x801125b4
80101c94:	e8 37 36 00 00       	call   801052d0 <memmove>
  brelse(bp);
80101c99:	89 1c 24             	mov    %ebx,(%esp)
80101c9c:	e8 4f e5 ff ff       	call   801001f0 <brelse>
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d\
80101ca1:	ff 35 cc 25 11 80    	push   0x801125cc
80101ca7:	ff 35 c8 25 11 80    	push   0x801125c8
80101cad:	ff 35 c4 25 11 80    	push   0x801125c4
80101cb3:	ff 35 c0 25 11 80    	push   0x801125c0
80101cb9:	ff 35 bc 25 11 80    	push   0x801125bc
80101cbf:	ff 35 b8 25 11 80    	push   0x801125b8
80101cc5:	ff 35 b4 25 11 80    	push   0x801125b4
80101ccb:	68 10 81 10 80       	push   $0x80108110
80101cd0:	e8 5b ea ff ff       	call   80100730 <cprintf>
}
80101cd5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101cd8:	83 c4 30             	add    $0x30,%esp
80101cdb:	c9                   	leave  
80101cdc:	c3                   	ret    
80101cdd:	8d 76 00             	lea    0x0(%esi),%esi

80101ce0 <ialloc>:
{
80101ce0:	f3 0f 1e fb          	endbr32 
80101ce4:	55                   	push   %ebp
80101ce5:	89 e5                	mov    %esp,%ebp
80101ce7:	57                   	push   %edi
80101ce8:	56                   	push   %esi
80101ce9:	53                   	push   %ebx
80101cea:	83 ec 1c             	sub    $0x1c,%esp
80101ced:	8b 45 0c             	mov    0xc(%ebp),%eax
  for(inum = 1; inum < sb.ninodes; inum++){
80101cf0:	83 3d bc 25 11 80 01 	cmpl   $0x1,0x801125bc
{
80101cf7:	8b 75 08             	mov    0x8(%ebp),%esi
80101cfa:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  for(inum = 1; inum < sb.ninodes; inum++){
80101cfd:	0f 86 8d 00 00 00    	jbe    80101d90 <ialloc+0xb0>
80101d03:	bf 01 00 00 00       	mov    $0x1,%edi
80101d08:	eb 1d                	jmp    80101d27 <ialloc+0x47>
80101d0a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    brelse(bp);
80101d10:	83 ec 0c             	sub    $0xc,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
80101d13:	83 c7 01             	add    $0x1,%edi
    brelse(bp);
80101d16:	53                   	push   %ebx
80101d17:	e8 d4 e4 ff ff       	call   801001f0 <brelse>
  for(inum = 1; inum < sb.ninodes; inum++){
80101d1c:	83 c4 10             	add    $0x10,%esp
80101d1f:	3b 3d bc 25 11 80    	cmp    0x801125bc,%edi
80101d25:	73 69                	jae    80101d90 <ialloc+0xb0>
    bp = bread(dev, IBLOCK(inum, sb));
80101d27:	89 f8                	mov    %edi,%eax
80101d29:	83 ec 08             	sub    $0x8,%esp
80101d2c:	c1 e8 03             	shr    $0x3,%eax
80101d2f:	03 05 c8 25 11 80    	add    0x801125c8,%eax
80101d35:	50                   	push   %eax
80101d36:	56                   	push   %esi
80101d37:	e8 94 e3 ff ff       	call   801000d0 <bread>
    if(dip->type == 0){  // a free inode
80101d3c:	83 c4 10             	add    $0x10,%esp
    bp = bread(dev, IBLOCK(inum, sb));
80101d3f:	89 c3                	mov    %eax,%ebx
    dip = (struct dinode*)bp->data + inum%IPB;
80101d41:	89 f8                	mov    %edi,%eax
80101d43:	83 e0 07             	and    $0x7,%eax
80101d46:	c1 e0 06             	shl    $0x6,%eax
80101d49:	8d 4c 03 5c          	lea    0x5c(%ebx,%eax,1),%ecx
    if(dip->type == 0){  // a free inode
80101d4d:	66 83 39 00          	cmpw   $0x0,(%ecx)
80101d51:	75 bd                	jne    80101d10 <ialloc+0x30>
      memset(dip, 0, sizeof(*dip));
80101d53:	83 ec 04             	sub    $0x4,%esp
80101d56:	89 4d e0             	mov    %ecx,-0x20(%ebp)
80101d59:	6a 40                	push   $0x40
80101d5b:	6a 00                	push   $0x0
80101d5d:	51                   	push   %ecx
80101d5e:	e8 cd 34 00 00       	call   80105230 <memset>
      dip->type = type;
80101d63:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
80101d67:	8b 4d e0             	mov    -0x20(%ebp),%ecx
80101d6a:	66 89 01             	mov    %ax,(%ecx)
      log_write(bp);   // mark it allocated on the disk
80101d6d:	89 1c 24             	mov    %ebx,(%esp)
80101d70:	e8 0b 19 00 00       	call   80103680 <log_write>
      brelse(bp);
80101d75:	89 1c 24             	mov    %ebx,(%esp)
80101d78:	e8 73 e4 ff ff       	call   801001f0 <brelse>
      return iget(dev, inum);
80101d7d:	83 c4 10             	add    $0x10,%esp
}
80101d80:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return iget(dev, inum);
80101d83:	89 fa                	mov    %edi,%edx
}
80101d85:	5b                   	pop    %ebx
      return iget(dev, inum);
80101d86:	89 f0                	mov    %esi,%eax
}
80101d88:	5e                   	pop    %esi
80101d89:	5f                   	pop    %edi
80101d8a:	5d                   	pop    %ebp
      return iget(dev, inum);
80101d8b:	e9 90 fc ff ff       	jmp    80101a20 <iget>
  panic("ialloc: no inodes");
80101d90:	83 ec 0c             	sub    $0xc,%esp
80101d93:	68 b0 80 10 80       	push   $0x801080b0
80101d98:	e8 f3 e5 ff ff       	call   80100390 <panic>
80101d9d:	8d 76 00             	lea    0x0(%esi),%esi

80101da0 <iupdate>:
{
80101da0:	f3 0f 1e fb          	endbr32 
80101da4:	55                   	push   %ebp
80101da5:	89 e5                	mov    %esp,%ebp
80101da7:	56                   	push   %esi
80101da8:	53                   	push   %ebx
80101da9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101dac:	8b 43 04             	mov    0x4(%ebx),%eax
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101daf:	83 c3 5c             	add    $0x5c,%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101db2:	83 ec 08             	sub    $0x8,%esp
80101db5:	c1 e8 03             	shr    $0x3,%eax
80101db8:	03 05 c8 25 11 80    	add    0x801125c8,%eax
80101dbe:	50                   	push   %eax
80101dbf:	ff 73 a4             	push   -0x5c(%ebx)
80101dc2:	e8 09 e3 ff ff       	call   801000d0 <bread>
  dip->type = ip->type;
80101dc7:	0f b7 53 f4          	movzwl -0xc(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101dcb:	83 c4 0c             	add    $0xc,%esp
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101dce:	89 c6                	mov    %eax,%esi
  dip = (struct dinode*)bp->data + ip->inum%IPB;
80101dd0:	8b 43 a8             	mov    -0x58(%ebx),%eax
80101dd3:	83 e0 07             	and    $0x7,%eax
80101dd6:	c1 e0 06             	shl    $0x6,%eax
80101dd9:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
  dip->type = ip->type;
80101ddd:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
80101de0:	0f b7 53 f6          	movzwl -0xa(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101de4:	83 c0 0c             	add    $0xc,%eax
  dip->major = ip->major;
80101de7:	66 89 50 f6          	mov    %dx,-0xa(%eax)
  dip->minor = ip->minor;
80101deb:	0f b7 53 f8          	movzwl -0x8(%ebx),%edx
80101def:	66 89 50 f8          	mov    %dx,-0x8(%eax)
  dip->nlink = ip->nlink;
80101df3:	0f b7 53 fa          	movzwl -0x6(%ebx),%edx
80101df7:	66 89 50 fa          	mov    %dx,-0x6(%eax)
  dip->size = ip->size;
80101dfb:	8b 53 fc             	mov    -0x4(%ebx),%edx
80101dfe:	89 50 fc             	mov    %edx,-0x4(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101e01:	6a 34                	push   $0x34
80101e03:	53                   	push   %ebx
80101e04:	50                   	push   %eax
80101e05:	e8 c6 34 00 00       	call   801052d0 <memmove>
  log_write(bp);
80101e0a:	89 34 24             	mov    %esi,(%esp)
80101e0d:	e8 6e 18 00 00       	call   80103680 <log_write>
  brelse(bp);
80101e12:	89 75 08             	mov    %esi,0x8(%ebp)
80101e15:	83 c4 10             	add    $0x10,%esp
}
80101e18:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101e1b:	5b                   	pop    %ebx
80101e1c:	5e                   	pop    %esi
80101e1d:	5d                   	pop    %ebp
  brelse(bp);
80101e1e:	e9 cd e3 ff ff       	jmp    801001f0 <brelse>
80101e23:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101e2a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101e30 <idup>:
{
80101e30:	f3 0f 1e fb          	endbr32 
80101e34:	55                   	push   %ebp
80101e35:	89 e5                	mov    %esp,%ebp
80101e37:	53                   	push   %ebx
80101e38:	83 ec 10             	sub    $0x10,%esp
80101e3b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&icache.lock);
80101e3e:	68 60 09 11 80       	push   $0x80110960
80101e43:	e8 18 33 00 00       	call   80105160 <acquire>
  ip->ref++;
80101e48:	83 43 08 01          	addl   $0x1,0x8(%ebx)
  release(&icache.lock);
80101e4c:	c7 04 24 60 09 11 80 	movl   $0x80110960,(%esp)
80101e53:	e8 98 32 00 00       	call   801050f0 <release>
}
80101e58:	89 d8                	mov    %ebx,%eax
80101e5a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101e5d:	c9                   	leave  
80101e5e:	c3                   	ret    
80101e5f:	90                   	nop

80101e60 <ilock>:
{
80101e60:	f3 0f 1e fb          	endbr32 
80101e64:	55                   	push   %ebp
80101e65:	89 e5                	mov    %esp,%ebp
80101e67:	56                   	push   %esi
80101e68:	53                   	push   %ebx
80101e69:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || ip->ref < 1)
80101e6c:	85 db                	test   %ebx,%ebx
80101e6e:	0f 84 b3 00 00 00    	je     80101f27 <ilock+0xc7>
80101e74:	8b 53 08             	mov    0x8(%ebx),%edx
80101e77:	85 d2                	test   %edx,%edx
80101e79:	0f 8e a8 00 00 00    	jle    80101f27 <ilock+0xc7>
  acquiresleep(&ip->lock);
80101e7f:	83 ec 0c             	sub    $0xc,%esp
80101e82:	8d 43 0c             	lea    0xc(%ebx),%eax
80101e85:	50                   	push   %eax
80101e86:	e8 d5 2f 00 00       	call   80104e60 <acquiresleep>
  if(ip->valid == 0){
80101e8b:	8b 43 4c             	mov    0x4c(%ebx),%eax
80101e8e:	83 c4 10             	add    $0x10,%esp
80101e91:	85 c0                	test   %eax,%eax
80101e93:	74 0b                	je     80101ea0 <ilock+0x40>
}
80101e95:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101e98:	5b                   	pop    %ebx
80101e99:	5e                   	pop    %esi
80101e9a:	5d                   	pop    %ebp
80101e9b:	c3                   	ret    
80101e9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101ea0:	8b 43 04             	mov    0x4(%ebx),%eax
80101ea3:	83 ec 08             	sub    $0x8,%esp
80101ea6:	c1 e8 03             	shr    $0x3,%eax
80101ea9:	03 05 c8 25 11 80    	add    0x801125c8,%eax
80101eaf:	50                   	push   %eax
80101eb0:	ff 33                	push   (%ebx)
80101eb2:	e8 19 e2 ff ff       	call   801000d0 <bread>
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80101eb7:	83 c4 0c             	add    $0xc,%esp
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101eba:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + ip->inum%IPB;
80101ebc:	8b 43 04             	mov    0x4(%ebx),%eax
80101ebf:	83 e0 07             	and    $0x7,%eax
80101ec2:	c1 e0 06             	shl    $0x6,%eax
80101ec5:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
    ip->type = dip->type;
80101ec9:	0f b7 10             	movzwl (%eax),%edx
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80101ecc:	83 c0 0c             	add    $0xc,%eax
    ip->type = dip->type;
80101ecf:	66 89 53 50          	mov    %dx,0x50(%ebx)
    ip->major = dip->major;
80101ed3:	0f b7 50 f6          	movzwl -0xa(%eax),%edx
80101ed7:	66 89 53 52          	mov    %dx,0x52(%ebx)
    ip->minor = dip->minor;
80101edb:	0f b7 50 f8          	movzwl -0x8(%eax),%edx
80101edf:	66 89 53 54          	mov    %dx,0x54(%ebx)
    ip->nlink = dip->nlink;
80101ee3:	0f b7 50 fa          	movzwl -0x6(%eax),%edx
80101ee7:	66 89 53 56          	mov    %dx,0x56(%ebx)
    ip->size = dip->size;
80101eeb:	8b 50 fc             	mov    -0x4(%eax),%edx
80101eee:	89 53 58             	mov    %edx,0x58(%ebx)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80101ef1:	6a 34                	push   $0x34
80101ef3:	50                   	push   %eax
80101ef4:	8d 43 5c             	lea    0x5c(%ebx),%eax
80101ef7:	50                   	push   %eax
80101ef8:	e8 d3 33 00 00       	call   801052d0 <memmove>
    brelse(bp);
80101efd:	89 34 24             	mov    %esi,(%esp)
80101f00:	e8 eb e2 ff ff       	call   801001f0 <brelse>
    if(ip->type == 0)
80101f05:	83 c4 10             	add    $0x10,%esp
80101f08:	66 83 7b 50 00       	cmpw   $0x0,0x50(%ebx)
    ip->valid = 1;
80101f0d:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
    if(ip->type == 0)
80101f14:	0f 85 7b ff ff ff    	jne    80101e95 <ilock+0x35>
      panic("ilock: no type");
80101f1a:	83 ec 0c             	sub    $0xc,%esp
80101f1d:	68 c8 80 10 80       	push   $0x801080c8
80101f22:	e8 69 e4 ff ff       	call   80100390 <panic>
    panic("ilock");
80101f27:	83 ec 0c             	sub    $0xc,%esp
80101f2a:	68 c2 80 10 80       	push   $0x801080c2
80101f2f:	e8 5c e4 ff ff       	call   80100390 <panic>
80101f34:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101f3b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101f3f:	90                   	nop

80101f40 <iunlock>:
{
80101f40:	f3 0f 1e fb          	endbr32 
80101f44:	55                   	push   %ebp
80101f45:	89 e5                	mov    %esp,%ebp
80101f47:	56                   	push   %esi
80101f48:	53                   	push   %ebx
80101f49:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80101f4c:	85 db                	test   %ebx,%ebx
80101f4e:	74 28                	je     80101f78 <iunlock+0x38>
80101f50:	83 ec 0c             	sub    $0xc,%esp
80101f53:	8d 73 0c             	lea    0xc(%ebx),%esi
80101f56:	56                   	push   %esi
80101f57:	e8 a4 2f 00 00       	call   80104f00 <holdingsleep>
80101f5c:	83 c4 10             	add    $0x10,%esp
80101f5f:	85 c0                	test   %eax,%eax
80101f61:	74 15                	je     80101f78 <iunlock+0x38>
80101f63:	8b 43 08             	mov    0x8(%ebx),%eax
80101f66:	85 c0                	test   %eax,%eax
80101f68:	7e 0e                	jle    80101f78 <iunlock+0x38>
  releasesleep(&ip->lock);
80101f6a:	89 75 08             	mov    %esi,0x8(%ebp)
}
80101f6d:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101f70:	5b                   	pop    %ebx
80101f71:	5e                   	pop    %esi
80101f72:	5d                   	pop    %ebp
  releasesleep(&ip->lock);
80101f73:	e9 48 2f 00 00       	jmp    80104ec0 <releasesleep>
    panic("iunlock");
80101f78:	83 ec 0c             	sub    $0xc,%esp
80101f7b:	68 d7 80 10 80       	push   $0x801080d7
80101f80:	e8 0b e4 ff ff       	call   80100390 <panic>
80101f85:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101f8c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101f90 <iput>:
{
80101f90:	f3 0f 1e fb          	endbr32 
80101f94:	55                   	push   %ebp
80101f95:	89 e5                	mov    %esp,%ebp
80101f97:	57                   	push   %edi
80101f98:	56                   	push   %esi
80101f99:	53                   	push   %ebx
80101f9a:	83 ec 28             	sub    $0x28,%esp
80101f9d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquiresleep(&ip->lock);
80101fa0:	8d 7b 0c             	lea    0xc(%ebx),%edi
80101fa3:	57                   	push   %edi
80101fa4:	e8 b7 2e 00 00       	call   80104e60 <acquiresleep>
  if(ip->valid && ip->nlink == 0){
80101fa9:	8b 53 4c             	mov    0x4c(%ebx),%edx
80101fac:	83 c4 10             	add    $0x10,%esp
80101faf:	85 d2                	test   %edx,%edx
80101fb1:	74 07                	je     80101fba <iput+0x2a>
80101fb3:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80101fb8:	74 36                	je     80101ff0 <iput+0x60>
  releasesleep(&ip->lock);
80101fba:	83 ec 0c             	sub    $0xc,%esp
80101fbd:	57                   	push   %edi
80101fbe:	e8 fd 2e 00 00       	call   80104ec0 <releasesleep>
  acquire(&icache.lock);
80101fc3:	c7 04 24 60 09 11 80 	movl   $0x80110960,(%esp)
80101fca:	e8 91 31 00 00       	call   80105160 <acquire>
  ip->ref--;
80101fcf:	83 6b 08 01          	subl   $0x1,0x8(%ebx)
  release(&icache.lock);
80101fd3:	83 c4 10             	add    $0x10,%esp
80101fd6:	c7 45 08 60 09 11 80 	movl   $0x80110960,0x8(%ebp)
}
80101fdd:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101fe0:	5b                   	pop    %ebx
80101fe1:	5e                   	pop    %esi
80101fe2:	5f                   	pop    %edi
80101fe3:	5d                   	pop    %ebp
  release(&icache.lock);
80101fe4:	e9 07 31 00 00       	jmp    801050f0 <release>
80101fe9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    acquire(&icache.lock);
80101ff0:	83 ec 0c             	sub    $0xc,%esp
80101ff3:	68 60 09 11 80       	push   $0x80110960
80101ff8:	e8 63 31 00 00       	call   80105160 <acquire>
    int r = ip->ref;
80101ffd:	8b 73 08             	mov    0x8(%ebx),%esi
    release(&icache.lock);
80102000:	c7 04 24 60 09 11 80 	movl   $0x80110960,(%esp)
80102007:	e8 e4 30 00 00       	call   801050f0 <release>
    if(r == 1){
8010200c:	83 c4 10             	add    $0x10,%esp
8010200f:	83 fe 01             	cmp    $0x1,%esi
80102012:	75 a6                	jne    80101fba <iput+0x2a>
80102014:	8d 8b 8c 00 00 00    	lea    0x8c(%ebx),%ecx
8010201a:	89 7d e4             	mov    %edi,-0x1c(%ebp)
8010201d:	8d 73 5c             	lea    0x5c(%ebx),%esi
80102020:	89 cf                	mov    %ecx,%edi
80102022:	eb 0b                	jmp    8010202f <iput+0x9f>
80102024:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80102028:	83 c6 04             	add    $0x4,%esi
8010202b:	39 fe                	cmp    %edi,%esi
8010202d:	74 19                	je     80102048 <iput+0xb8>
    if(ip->addrs[i]){
8010202f:	8b 16                	mov    (%esi),%edx
80102031:	85 d2                	test   %edx,%edx
80102033:	74 f3                	je     80102028 <iput+0x98>
      bfree(ip->dev, ip->addrs[i]);
80102035:	8b 03                	mov    (%ebx),%eax
80102037:	e8 54 f8 ff ff       	call   80101890 <bfree>
      ip->addrs[i] = 0;
8010203c:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80102042:	eb e4                	jmp    80102028 <iput+0x98>
80102044:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    }
  }

  if(ip->addrs[NDIRECT]){
80102048:	8b 83 8c 00 00 00    	mov    0x8c(%ebx),%eax
8010204e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80102051:	85 c0                	test   %eax,%eax
80102053:	75 2d                	jne    80102082 <iput+0xf2>
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  ip->size = 0;
  iupdate(ip);
80102055:	83 ec 0c             	sub    $0xc,%esp
  ip->size = 0;
80102058:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  iupdate(ip);
8010205f:	53                   	push   %ebx
80102060:	e8 3b fd ff ff       	call   80101da0 <iupdate>
      ip->type = 0;
80102065:	31 c0                	xor    %eax,%eax
80102067:	66 89 43 50          	mov    %ax,0x50(%ebx)
      iupdate(ip);
8010206b:	89 1c 24             	mov    %ebx,(%esp)
8010206e:	e8 2d fd ff ff       	call   80101da0 <iupdate>
      ip->valid = 0;
80102073:	c7 43 4c 00 00 00 00 	movl   $0x0,0x4c(%ebx)
8010207a:	83 c4 10             	add    $0x10,%esp
8010207d:	e9 38 ff ff ff       	jmp    80101fba <iput+0x2a>
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80102082:	83 ec 08             	sub    $0x8,%esp
80102085:	50                   	push   %eax
80102086:	ff 33                	push   (%ebx)
80102088:	e8 43 e0 ff ff       	call   801000d0 <bread>
8010208d:	89 7d e0             	mov    %edi,-0x20(%ebp)
80102090:	83 c4 10             	add    $0x10,%esp
80102093:	8d 88 5c 02 00 00    	lea    0x25c(%eax),%ecx
80102099:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(j = 0; j < NINDIRECT; j++){
8010209c:	8d 70 5c             	lea    0x5c(%eax),%esi
8010209f:	89 cf                	mov    %ecx,%edi
801020a1:	eb 0c                	jmp    801020af <iput+0x11f>
801020a3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801020a7:	90                   	nop
801020a8:	83 c6 04             	add    $0x4,%esi
801020ab:	39 f7                	cmp    %esi,%edi
801020ad:	74 0f                	je     801020be <iput+0x12e>
      if(a[j])
801020af:	8b 16                	mov    (%esi),%edx
801020b1:	85 d2                	test   %edx,%edx
801020b3:	74 f3                	je     801020a8 <iput+0x118>
        bfree(ip->dev, a[j]);
801020b5:	8b 03                	mov    (%ebx),%eax
801020b7:	e8 d4 f7 ff ff       	call   80101890 <bfree>
801020bc:	eb ea                	jmp    801020a8 <iput+0x118>
    brelse(bp);
801020be:	83 ec 0c             	sub    $0xc,%esp
801020c1:	ff 75 e4             	push   -0x1c(%ebp)
801020c4:	8b 7d e0             	mov    -0x20(%ebp),%edi
801020c7:	e8 24 e1 ff ff       	call   801001f0 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
801020cc:	8b 93 8c 00 00 00    	mov    0x8c(%ebx),%edx
801020d2:	8b 03                	mov    (%ebx),%eax
801020d4:	e8 b7 f7 ff ff       	call   80101890 <bfree>
    ip->addrs[NDIRECT] = 0;
801020d9:	83 c4 10             	add    $0x10,%esp
801020dc:	c7 83 8c 00 00 00 00 	movl   $0x0,0x8c(%ebx)
801020e3:	00 00 00 
801020e6:	e9 6a ff ff ff       	jmp    80102055 <iput+0xc5>
801020eb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801020ef:	90                   	nop

801020f0 <iunlockput>:
{
801020f0:	f3 0f 1e fb          	endbr32 
801020f4:	55                   	push   %ebp
801020f5:	89 e5                	mov    %esp,%ebp
801020f7:	56                   	push   %esi
801020f8:	53                   	push   %ebx
801020f9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
801020fc:	85 db                	test   %ebx,%ebx
801020fe:	74 34                	je     80102134 <iunlockput+0x44>
80102100:	83 ec 0c             	sub    $0xc,%esp
80102103:	8d 73 0c             	lea    0xc(%ebx),%esi
80102106:	56                   	push   %esi
80102107:	e8 f4 2d 00 00       	call   80104f00 <holdingsleep>
8010210c:	83 c4 10             	add    $0x10,%esp
8010210f:	85 c0                	test   %eax,%eax
80102111:	74 21                	je     80102134 <iunlockput+0x44>
80102113:	8b 43 08             	mov    0x8(%ebx),%eax
80102116:	85 c0                	test   %eax,%eax
80102118:	7e 1a                	jle    80102134 <iunlockput+0x44>
  releasesleep(&ip->lock);
8010211a:	83 ec 0c             	sub    $0xc,%esp
8010211d:	56                   	push   %esi
8010211e:	e8 9d 2d 00 00       	call   80104ec0 <releasesleep>
  iput(ip);
80102123:	89 5d 08             	mov    %ebx,0x8(%ebp)
80102126:	83 c4 10             	add    $0x10,%esp
}
80102129:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010212c:	5b                   	pop    %ebx
8010212d:	5e                   	pop    %esi
8010212e:	5d                   	pop    %ebp
  iput(ip);
8010212f:	e9 5c fe ff ff       	jmp    80101f90 <iput>
    panic("iunlock");
80102134:	83 ec 0c             	sub    $0xc,%esp
80102137:	68 d7 80 10 80       	push   $0x801080d7
8010213c:	e8 4f e2 ff ff       	call   80100390 <panic>
80102141:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102148:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010214f:	90                   	nop

80102150 <stati>:

// Copy stat information from inode.
// Caller must hold ip->lock.
void
stati(struct inode *ip, struct stat *st)
{
80102150:	f3 0f 1e fb          	endbr32 
80102154:	55                   	push   %ebp
80102155:	89 e5                	mov    %esp,%ebp
80102157:	8b 55 08             	mov    0x8(%ebp),%edx
8010215a:	8b 45 0c             	mov    0xc(%ebp),%eax
  st->dev = ip->dev;
8010215d:	8b 0a                	mov    (%edx),%ecx
8010215f:	89 48 04             	mov    %ecx,0x4(%eax)
  st->ino = ip->inum;
80102162:	8b 4a 04             	mov    0x4(%edx),%ecx
80102165:	89 48 08             	mov    %ecx,0x8(%eax)
  st->type = ip->type;
80102168:	0f b7 4a 50          	movzwl 0x50(%edx),%ecx
8010216c:	66 89 08             	mov    %cx,(%eax)
  st->nlink = ip->nlink;
8010216f:	0f b7 4a 56          	movzwl 0x56(%edx),%ecx
80102173:	66 89 48 0c          	mov    %cx,0xc(%eax)
  st->size = ip->size;
80102177:	8b 52 58             	mov    0x58(%edx),%edx
8010217a:	89 50 10             	mov    %edx,0x10(%eax)
}
8010217d:	5d                   	pop    %ebp
8010217e:	c3                   	ret    
8010217f:	90                   	nop

80102180 <readi>:
//PAGEBREAK!
// Read data from inode.
// Caller must hold ip->lock.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80102180:	f3 0f 1e fb          	endbr32 
80102184:	55                   	push   %ebp
80102185:	89 e5                	mov    %esp,%ebp
80102187:	57                   	push   %edi
80102188:	56                   	push   %esi
80102189:	53                   	push   %ebx
8010218a:	83 ec 1c             	sub    $0x1c,%esp
8010218d:	8b 7d 0c             	mov    0xc(%ebp),%edi
80102190:	8b 45 08             	mov    0x8(%ebp),%eax
80102193:	8b 75 10             	mov    0x10(%ebp),%esi
80102196:	89 7d e0             	mov    %edi,-0x20(%ebp)
80102199:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
8010219c:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
801021a1:	89 45 d8             	mov    %eax,-0x28(%ebp)
801021a4:	89 7d e4             	mov    %edi,-0x1c(%ebp)
  if(ip->type == T_DEV){
801021a7:	0f 84 a3 00 00 00    	je     80102250 <readi+0xd0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  }

  if(off > ip->size || off + n < off)
801021ad:	8b 45 d8             	mov    -0x28(%ebp),%eax
801021b0:	8b 40 58             	mov    0x58(%eax),%eax
801021b3:	39 c6                	cmp    %eax,%esi
801021b5:	0f 87 b6 00 00 00    	ja     80102271 <readi+0xf1>
801021bb:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801021be:	31 c9                	xor    %ecx,%ecx
801021c0:	89 da                	mov    %ebx,%edx
801021c2:	01 f2                	add    %esi,%edx
801021c4:	0f 92 c1             	setb   %cl
801021c7:	89 cf                	mov    %ecx,%edi
801021c9:	0f 82 a2 00 00 00    	jb     80102271 <readi+0xf1>
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;
801021cf:	89 c1                	mov    %eax,%ecx
801021d1:	29 f1                	sub    %esi,%ecx
801021d3:	39 d0                	cmp    %edx,%eax
801021d5:	0f 43 cb             	cmovae %ebx,%ecx
801021d8:	89 4d e4             	mov    %ecx,-0x1c(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
801021db:	85 c9                	test   %ecx,%ecx
801021dd:	74 63                	je     80102242 <readi+0xc2>
801021df:	90                   	nop
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
801021e0:	8b 5d d8             	mov    -0x28(%ebp),%ebx
801021e3:	89 f2                	mov    %esi,%edx
801021e5:	c1 ea 09             	shr    $0x9,%edx
801021e8:	89 d8                	mov    %ebx,%eax
801021ea:	e8 31 f9 ff ff       	call   80101b20 <bmap>
801021ef:	83 ec 08             	sub    $0x8,%esp
801021f2:	50                   	push   %eax
801021f3:	ff 33                	push   (%ebx)
801021f5:	e8 d6 de ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
801021fa:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801021fd:	b9 00 02 00 00       	mov    $0x200,%ecx
80102202:	83 c4 0c             	add    $0xc,%esp
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102205:	89 c2                	mov    %eax,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
80102207:	89 f0                	mov    %esi,%eax
80102209:	25 ff 01 00 00       	and    $0x1ff,%eax
8010220e:	29 fb                	sub    %edi,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
80102210:	89 55 dc             	mov    %edx,-0x24(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
80102213:	29 c1                	sub    %eax,%ecx
    memmove(dst, bp->data + off%BSIZE, m);
80102215:	8d 44 02 5c          	lea    0x5c(%edx,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
80102219:	39 d9                	cmp    %ebx,%ecx
8010221b:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
8010221e:	53                   	push   %ebx
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
8010221f:	01 df                	add    %ebx,%edi
80102221:	01 de                	add    %ebx,%esi
    memmove(dst, bp->data + off%BSIZE, m);
80102223:	50                   	push   %eax
80102224:	ff 75 e0             	push   -0x20(%ebp)
80102227:	e8 a4 30 00 00       	call   801052d0 <memmove>
    brelse(bp);
8010222c:	8b 55 dc             	mov    -0x24(%ebp),%edx
8010222f:	89 14 24             	mov    %edx,(%esp)
80102232:	e8 b9 df ff ff       	call   801001f0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80102237:	01 5d e0             	add    %ebx,-0x20(%ebp)
8010223a:	83 c4 10             	add    $0x10,%esp
8010223d:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80102240:	77 9e                	ja     801021e0 <readi+0x60>
  }
  return n;
80102242:	8b 45 e4             	mov    -0x1c(%ebp),%eax
}
80102245:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102248:	5b                   	pop    %ebx
80102249:	5e                   	pop    %esi
8010224a:	5f                   	pop    %edi
8010224b:	5d                   	pop    %ebp
8010224c:	c3                   	ret    
8010224d:	8d 76 00             	lea    0x0(%esi),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80102250:	0f bf 40 52          	movswl 0x52(%eax),%eax
80102254:	66 83 f8 09          	cmp    $0x9,%ax
80102258:	77 17                	ja     80102271 <readi+0xf1>
8010225a:	8b 04 c5 00 09 11 80 	mov    -0x7feef700(,%eax,8),%eax
80102261:	85 c0                	test   %eax,%eax
80102263:	74 0c                	je     80102271 <readi+0xf1>
    return devsw[ip->major].read(ip, dst, n);
80102265:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80102268:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010226b:	5b                   	pop    %ebx
8010226c:	5e                   	pop    %esi
8010226d:	5f                   	pop    %edi
8010226e:	5d                   	pop    %ebp
    return devsw[ip->major].read(ip, dst, n);
8010226f:	ff e0                	jmp    *%eax
      return -1;
80102271:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102276:	eb cd                	jmp    80102245 <readi+0xc5>
80102278:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010227f:	90                   	nop

80102280 <writei>:
// PAGEBREAK!
// Write data to inode.
// Caller must hold ip->lock.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
80102280:	f3 0f 1e fb          	endbr32 
80102284:	55                   	push   %ebp
80102285:	89 e5                	mov    %esp,%ebp
80102287:	57                   	push   %edi
80102288:	56                   	push   %esi
80102289:	53                   	push   %ebx
8010228a:	83 ec 1c             	sub    $0x1c,%esp
8010228d:	8b 45 08             	mov    0x8(%ebp),%eax
80102290:	8b 75 0c             	mov    0xc(%ebp),%esi
80102293:	8b 55 14             	mov    0x14(%ebp),%edx
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80102296:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
8010229b:	89 75 dc             	mov    %esi,-0x24(%ebp)
8010229e:	89 45 d8             	mov    %eax,-0x28(%ebp)
801022a1:	8b 75 10             	mov    0x10(%ebp),%esi
801022a4:	89 55 e0             	mov    %edx,-0x20(%ebp)
  if(ip->type == T_DEV){
801022a7:	0f 84 bb 00 00 00    	je     80102368 <writei+0xe8>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
      return -1;
    return devsw[ip->major].write(ip, src, n);
  }

  if(off > ip->size || off + n < off)
801022ad:	8b 45 d8             	mov    -0x28(%ebp),%eax
801022b0:	3b 70 58             	cmp    0x58(%eax),%esi
801022b3:	0f 87 eb 00 00 00    	ja     801023a4 <writei+0x124>
801022b9:	8b 7d e0             	mov    -0x20(%ebp),%edi
801022bc:	31 d2                	xor    %edx,%edx
801022be:	89 f8                	mov    %edi,%eax
801022c0:	01 f0                	add    %esi,%eax
801022c2:	0f 92 c2             	setb   %dl
    return -1;
  if(off + n > MAXFILE*BSIZE)
801022c5:	3d 00 18 01 00       	cmp    $0x11800,%eax
801022ca:	0f 87 d4 00 00 00    	ja     801023a4 <writei+0x124>
801022d0:	85 d2                	test   %edx,%edx
801022d2:	0f 85 cc 00 00 00    	jne    801023a4 <writei+0x124>
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
801022d8:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
801022df:	85 ff                	test   %edi,%edi
801022e1:	74 76                	je     80102359 <writei+0xd9>
801022e3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801022e7:	90                   	nop
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
801022e8:	8b 7d d8             	mov    -0x28(%ebp),%edi
801022eb:	89 f2                	mov    %esi,%edx
801022ed:	c1 ea 09             	shr    $0x9,%edx
801022f0:	89 f8                	mov    %edi,%eax
801022f2:	e8 29 f8 ff ff       	call   80101b20 <bmap>
801022f7:	83 ec 08             	sub    $0x8,%esp
801022fa:	50                   	push   %eax
801022fb:	ff 37                	push   (%edi)
801022fd:	e8 ce dd ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80102302:	b9 00 02 00 00       	mov    $0x200,%ecx
80102307:	8b 5d e0             	mov    -0x20(%ebp),%ebx
8010230a:	2b 5d e4             	sub    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
8010230d:	89 c7                	mov    %eax,%edi
    m = min(n - tot, BSIZE - off%BSIZE);
8010230f:	89 f0                	mov    %esi,%eax
80102311:	83 c4 0c             	add    $0xc,%esp
80102314:	25 ff 01 00 00       	and    $0x1ff,%eax
80102319:	29 c1                	sub    %eax,%ecx
    memmove(bp->data + off%BSIZE, src, m);
8010231b:	8d 44 07 5c          	lea    0x5c(%edi,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
8010231f:	39 d9                	cmp    %ebx,%ecx
80102321:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(bp->data + off%BSIZE, src, m);
80102324:	53                   	push   %ebx
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80102325:	01 de                	add    %ebx,%esi
    memmove(bp->data + off%BSIZE, src, m);
80102327:	ff 75 dc             	push   -0x24(%ebp)
8010232a:	50                   	push   %eax
8010232b:	e8 a0 2f 00 00       	call   801052d0 <memmove>
    log_write(bp);
80102330:	89 3c 24             	mov    %edi,(%esp)
80102333:	e8 48 13 00 00       	call   80103680 <log_write>
    brelse(bp);
80102338:	89 3c 24             	mov    %edi,(%esp)
8010233b:	e8 b0 de ff ff       	call   801001f0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80102340:	01 5d e4             	add    %ebx,-0x1c(%ebp)
80102343:	83 c4 10             	add    $0x10,%esp
80102346:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80102349:	01 5d dc             	add    %ebx,-0x24(%ebp)
8010234c:	39 45 e0             	cmp    %eax,-0x20(%ebp)
8010234f:	77 97                	ja     801022e8 <writei+0x68>
  }

  if(n > 0 && off > ip->size){
80102351:	8b 45 d8             	mov    -0x28(%ebp),%eax
80102354:	3b 70 58             	cmp    0x58(%eax),%esi
80102357:	77 37                	ja     80102390 <writei+0x110>
    ip->size = off;
    iupdate(ip);
  }
  return n;
80102359:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
8010235c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010235f:	5b                   	pop    %ebx
80102360:	5e                   	pop    %esi
80102361:	5f                   	pop    %edi
80102362:	5d                   	pop    %ebp
80102363:	c3                   	ret    
80102364:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
80102368:	0f bf 40 52          	movswl 0x52(%eax),%eax
8010236c:	66 83 f8 09          	cmp    $0x9,%ax
80102370:	77 32                	ja     801023a4 <writei+0x124>
80102372:	8b 04 c5 04 09 11 80 	mov    -0x7feef6fc(,%eax,8),%eax
80102379:	85 c0                	test   %eax,%eax
8010237b:	74 27                	je     801023a4 <writei+0x124>
    return devsw[ip->major].write(ip, src, n);
8010237d:	89 55 10             	mov    %edx,0x10(%ebp)
}
80102380:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102383:	5b                   	pop    %ebx
80102384:	5e                   	pop    %esi
80102385:	5f                   	pop    %edi
80102386:	5d                   	pop    %ebp
    return devsw[ip->major].write(ip, src, n);
80102387:	ff e0                	jmp    *%eax
80102389:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    ip->size = off;
80102390:	8b 45 d8             	mov    -0x28(%ebp),%eax
    iupdate(ip);
80102393:	83 ec 0c             	sub    $0xc,%esp
    ip->size = off;
80102396:	89 70 58             	mov    %esi,0x58(%eax)
    iupdate(ip);
80102399:	50                   	push   %eax
8010239a:	e8 01 fa ff ff       	call   80101da0 <iupdate>
8010239f:	83 c4 10             	add    $0x10,%esp
801023a2:	eb b5                	jmp    80102359 <writei+0xd9>
      return -1;
801023a4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801023a9:	eb b1                	jmp    8010235c <writei+0xdc>
801023ab:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801023af:	90                   	nop

801023b0 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
801023b0:	f3 0f 1e fb          	endbr32 
801023b4:	55                   	push   %ebp
801023b5:	89 e5                	mov    %esp,%ebp
801023b7:	83 ec 0c             	sub    $0xc,%esp
  return strncmp(s, t, DIRSIZ);
801023ba:	6a 0e                	push   $0xe
801023bc:	ff 75 0c             	push   0xc(%ebp)
801023bf:	ff 75 08             	push   0x8(%ebp)
801023c2:	e8 79 2f 00 00       	call   80105340 <strncmp>
}
801023c7:	c9                   	leave  
801023c8:	c3                   	ret    
801023c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801023d0 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
801023d0:	f3 0f 1e fb          	endbr32 
801023d4:	55                   	push   %ebp
801023d5:	89 e5                	mov    %esp,%ebp
801023d7:	57                   	push   %edi
801023d8:	56                   	push   %esi
801023d9:	53                   	push   %ebx
801023da:	83 ec 1c             	sub    $0x1c,%esp
801023dd:	8b 5d 08             	mov    0x8(%ebp),%ebx
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
801023e0:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801023e5:	0f 85 89 00 00 00    	jne    80102474 <dirlookup+0xa4>
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
801023eb:	8b 53 58             	mov    0x58(%ebx),%edx
801023ee:	31 ff                	xor    %edi,%edi
801023f0:	8d 75 d8             	lea    -0x28(%ebp),%esi
801023f3:	85 d2                	test   %edx,%edx
801023f5:	74 42                	je     80102439 <dirlookup+0x69>
801023f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801023fe:	66 90                	xchg   %ax,%ax
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102400:	6a 10                	push   $0x10
80102402:	57                   	push   %edi
80102403:	56                   	push   %esi
80102404:	53                   	push   %ebx
80102405:	e8 76 fd ff ff       	call   80102180 <readi>
8010240a:	83 c4 10             	add    $0x10,%esp
8010240d:	83 f8 10             	cmp    $0x10,%eax
80102410:	75 55                	jne    80102467 <dirlookup+0x97>
      panic("dirlookup read");
    if(de.inum == 0)
80102412:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80102417:	74 18                	je     80102431 <dirlookup+0x61>
  return strncmp(s, t, DIRSIZ);
80102419:	83 ec 04             	sub    $0x4,%esp
8010241c:	8d 45 da             	lea    -0x26(%ebp),%eax
8010241f:	6a 0e                	push   $0xe
80102421:	50                   	push   %eax
80102422:	ff 75 0c             	push   0xc(%ebp)
80102425:	e8 16 2f 00 00       	call   80105340 <strncmp>
      continue;
    if(namecmp(name, de.name) == 0){
8010242a:	83 c4 10             	add    $0x10,%esp
8010242d:	85 c0                	test   %eax,%eax
8010242f:	74 17                	je     80102448 <dirlookup+0x78>
  for(off = 0; off < dp->size; off += sizeof(de)){
80102431:	83 c7 10             	add    $0x10,%edi
80102434:	3b 7b 58             	cmp    0x58(%ebx),%edi
80102437:	72 c7                	jb     80102400 <dirlookup+0x30>
      return iget(dp->dev, inum);
    }
  }

  return 0;
}
80102439:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010243c:	31 c0                	xor    %eax,%eax
}
8010243e:	5b                   	pop    %ebx
8010243f:	5e                   	pop    %esi
80102440:	5f                   	pop    %edi
80102441:	5d                   	pop    %ebp
80102442:	c3                   	ret    
80102443:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102447:	90                   	nop
      if(poff)
80102448:	8b 45 10             	mov    0x10(%ebp),%eax
8010244b:	85 c0                	test   %eax,%eax
8010244d:	74 05                	je     80102454 <dirlookup+0x84>
        *poff = off;
8010244f:	8b 45 10             	mov    0x10(%ebp),%eax
80102452:	89 38                	mov    %edi,(%eax)
      inum = de.inum;
80102454:	0f b7 55 d8          	movzwl -0x28(%ebp),%edx
      return iget(dp->dev, inum);
80102458:	8b 03                	mov    (%ebx),%eax
8010245a:	e8 c1 f5 ff ff       	call   80101a20 <iget>
}
8010245f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102462:	5b                   	pop    %ebx
80102463:	5e                   	pop    %esi
80102464:	5f                   	pop    %edi
80102465:	5d                   	pop    %ebp
80102466:	c3                   	ret    
      panic("dirlookup read");
80102467:	83 ec 0c             	sub    $0xc,%esp
8010246a:	68 f1 80 10 80       	push   $0x801080f1
8010246f:	e8 1c df ff ff       	call   80100390 <panic>
    panic("dirlookup not DIR");
80102474:	83 ec 0c             	sub    $0xc,%esp
80102477:	68 df 80 10 80       	push   $0x801080df
8010247c:	e8 0f df ff ff       	call   80100390 <panic>
80102481:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102488:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010248f:	90                   	nop

80102490 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80102490:	55                   	push   %ebp
80102491:	89 e5                	mov    %esp,%ebp
80102493:	57                   	push   %edi
80102494:	56                   	push   %esi
80102495:	53                   	push   %ebx
80102496:	89 c3                	mov    %eax,%ebx
80102498:	83 ec 1c             	sub    $0x1c,%esp
  struct inode *ip, *next;

  if(*path == '/')
8010249b:	80 38 2f             	cmpb   $0x2f,(%eax)
{
8010249e:	89 55 dc             	mov    %edx,-0x24(%ebp)
801024a1:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  if(*path == '/')
801024a4:	0f 84 64 01 00 00    	je     8010260e <namex+0x17e>
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(myproc()->cwd);
801024aa:	e8 61 1c 00 00       	call   80104110 <myproc>
  acquire(&icache.lock);
801024af:	83 ec 0c             	sub    $0xc,%esp
    ip = idup(myproc()->cwd);
801024b2:	8b 70 6c             	mov    0x6c(%eax),%esi
  acquire(&icache.lock);
801024b5:	68 60 09 11 80       	push   $0x80110960
801024ba:	e8 a1 2c 00 00       	call   80105160 <acquire>
  ip->ref++;
801024bf:	83 46 08 01          	addl   $0x1,0x8(%esi)
  release(&icache.lock);
801024c3:	c7 04 24 60 09 11 80 	movl   $0x80110960,(%esp)
801024ca:	e8 21 2c 00 00       	call   801050f0 <release>
801024cf:	83 c4 10             	add    $0x10,%esp
801024d2:	eb 07                	jmp    801024db <namex+0x4b>
801024d4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    path++;
801024d8:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
801024db:	0f b6 03             	movzbl (%ebx),%eax
801024de:	3c 2f                	cmp    $0x2f,%al
801024e0:	74 f6                	je     801024d8 <namex+0x48>
  if(*path == 0)
801024e2:	84 c0                	test   %al,%al
801024e4:	0f 84 06 01 00 00    	je     801025f0 <namex+0x160>
  while(*path != '/' && *path != 0)
801024ea:	0f b6 03             	movzbl (%ebx),%eax
801024ed:	84 c0                	test   %al,%al
801024ef:	0f 84 10 01 00 00    	je     80102605 <namex+0x175>
801024f5:	89 df                	mov    %ebx,%edi
801024f7:	3c 2f                	cmp    $0x2f,%al
801024f9:	0f 84 06 01 00 00    	je     80102605 <namex+0x175>
801024ff:	90                   	nop
80102500:	0f b6 47 01          	movzbl 0x1(%edi),%eax
    path++;
80102504:	83 c7 01             	add    $0x1,%edi
  while(*path != '/' && *path != 0)
80102507:	3c 2f                	cmp    $0x2f,%al
80102509:	74 04                	je     8010250f <namex+0x7f>
8010250b:	84 c0                	test   %al,%al
8010250d:	75 f1                	jne    80102500 <namex+0x70>
  len = path - s;
8010250f:	89 f8                	mov    %edi,%eax
80102511:	29 d8                	sub    %ebx,%eax
  if(len >= DIRSIZ)
80102513:	83 f8 0d             	cmp    $0xd,%eax
80102516:	0f 8e ac 00 00 00    	jle    801025c8 <namex+0x138>
    memmove(name, s, DIRSIZ);
8010251c:	83 ec 04             	sub    $0x4,%esp
8010251f:	6a 0e                	push   $0xe
80102521:	53                   	push   %ebx
    path++;
80102522:	89 fb                	mov    %edi,%ebx
    memmove(name, s, DIRSIZ);
80102524:	ff 75 e4             	push   -0x1c(%ebp)
80102527:	e8 a4 2d 00 00       	call   801052d0 <memmove>
8010252c:	83 c4 10             	add    $0x10,%esp
  while(*path == '/')
8010252f:	80 3f 2f             	cmpb   $0x2f,(%edi)
80102532:	75 0c                	jne    80102540 <namex+0xb0>
80102534:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    path++;
80102538:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
8010253b:	80 3b 2f             	cmpb   $0x2f,(%ebx)
8010253e:	74 f8                	je     80102538 <namex+0xa8>

  while((path = skipelem(path, name)) != 0){
    ilock(ip);
80102540:	83 ec 0c             	sub    $0xc,%esp
80102543:	56                   	push   %esi
80102544:	e8 17 f9 ff ff       	call   80101e60 <ilock>
    if(ip->type != T_DIR){
80102549:	83 c4 10             	add    $0x10,%esp
8010254c:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80102551:	0f 85 cd 00 00 00    	jne    80102624 <namex+0x194>
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
80102557:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010255a:	85 c0                	test   %eax,%eax
8010255c:	74 09                	je     80102567 <namex+0xd7>
8010255e:	80 3b 00             	cmpb   $0x0,(%ebx)
80102561:	0f 84 22 01 00 00    	je     80102689 <namex+0x1f9>
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80102567:	83 ec 04             	sub    $0x4,%esp
8010256a:	6a 00                	push   $0x0
8010256c:	ff 75 e4             	push   -0x1c(%ebp)
8010256f:	56                   	push   %esi
80102570:	e8 5b fe ff ff       	call   801023d0 <dirlookup>
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80102575:	8d 56 0c             	lea    0xc(%esi),%edx
    if((next = dirlookup(ip, name, 0)) == 0){
80102578:	83 c4 10             	add    $0x10,%esp
8010257b:	89 c7                	mov    %eax,%edi
8010257d:	85 c0                	test   %eax,%eax
8010257f:	0f 84 e1 00 00 00    	je     80102666 <namex+0x1d6>
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80102585:	83 ec 0c             	sub    $0xc,%esp
80102588:	89 55 e0             	mov    %edx,-0x20(%ebp)
8010258b:	52                   	push   %edx
8010258c:	e8 6f 29 00 00       	call   80104f00 <holdingsleep>
80102591:	83 c4 10             	add    $0x10,%esp
80102594:	85 c0                	test   %eax,%eax
80102596:	0f 84 30 01 00 00    	je     801026cc <namex+0x23c>
8010259c:	8b 56 08             	mov    0x8(%esi),%edx
8010259f:	85 d2                	test   %edx,%edx
801025a1:	0f 8e 25 01 00 00    	jle    801026cc <namex+0x23c>
  releasesleep(&ip->lock);
801025a7:	8b 55 e0             	mov    -0x20(%ebp),%edx
801025aa:	83 ec 0c             	sub    $0xc,%esp
801025ad:	52                   	push   %edx
801025ae:	e8 0d 29 00 00       	call   80104ec0 <releasesleep>
  iput(ip);
801025b3:	89 34 24             	mov    %esi,(%esp)
801025b6:	89 fe                	mov    %edi,%esi
801025b8:	e8 d3 f9 ff ff       	call   80101f90 <iput>
801025bd:	83 c4 10             	add    $0x10,%esp
801025c0:	e9 16 ff ff ff       	jmp    801024db <namex+0x4b>
801025c5:	8d 76 00             	lea    0x0(%esi),%esi
    name[len] = 0;
801025c8:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
801025cb:	8d 14 01             	lea    (%ecx,%eax,1),%edx
    memmove(name, s, len);
801025ce:	83 ec 04             	sub    $0x4,%esp
801025d1:	89 55 e0             	mov    %edx,-0x20(%ebp)
801025d4:	50                   	push   %eax
801025d5:	53                   	push   %ebx
    name[len] = 0;
801025d6:	89 fb                	mov    %edi,%ebx
    memmove(name, s, len);
801025d8:	ff 75 e4             	push   -0x1c(%ebp)
801025db:	e8 f0 2c 00 00       	call   801052d0 <memmove>
    name[len] = 0;
801025e0:	8b 55 e0             	mov    -0x20(%ebp),%edx
801025e3:	83 c4 10             	add    $0x10,%esp
801025e6:	c6 02 00             	movb   $0x0,(%edx)
801025e9:	e9 41 ff ff ff       	jmp    8010252f <namex+0x9f>
801025ee:	66 90                	xchg   %ax,%ax
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
801025f0:	8b 45 dc             	mov    -0x24(%ebp),%eax
801025f3:	85 c0                	test   %eax,%eax
801025f5:	0f 85 be 00 00 00    	jne    801026b9 <namex+0x229>
    iput(ip);
    return 0;
  }
  return ip;
}
801025fb:	8d 65 f4             	lea    -0xc(%ebp),%esp
801025fe:	89 f0                	mov    %esi,%eax
80102600:	5b                   	pop    %ebx
80102601:	5e                   	pop    %esi
80102602:	5f                   	pop    %edi
80102603:	5d                   	pop    %ebp
80102604:	c3                   	ret    
  while(*path != '/' && *path != 0)
80102605:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80102608:	89 df                	mov    %ebx,%edi
8010260a:	31 c0                	xor    %eax,%eax
8010260c:	eb c0                	jmp    801025ce <namex+0x13e>
    ip = iget(ROOTDEV, ROOTINO);
8010260e:	ba 01 00 00 00       	mov    $0x1,%edx
80102613:	b8 01 00 00 00       	mov    $0x1,%eax
80102618:	e8 03 f4 ff ff       	call   80101a20 <iget>
8010261d:	89 c6                	mov    %eax,%esi
8010261f:	e9 b7 fe ff ff       	jmp    801024db <namex+0x4b>
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80102624:	83 ec 0c             	sub    $0xc,%esp
80102627:	8d 5e 0c             	lea    0xc(%esi),%ebx
8010262a:	53                   	push   %ebx
8010262b:	e8 d0 28 00 00       	call   80104f00 <holdingsleep>
80102630:	83 c4 10             	add    $0x10,%esp
80102633:	85 c0                	test   %eax,%eax
80102635:	0f 84 91 00 00 00    	je     801026cc <namex+0x23c>
8010263b:	8b 46 08             	mov    0x8(%esi),%eax
8010263e:	85 c0                	test   %eax,%eax
80102640:	0f 8e 86 00 00 00    	jle    801026cc <namex+0x23c>
  releasesleep(&ip->lock);
80102646:	83 ec 0c             	sub    $0xc,%esp
80102649:	53                   	push   %ebx
8010264a:	e8 71 28 00 00       	call   80104ec0 <releasesleep>
  iput(ip);
8010264f:	89 34 24             	mov    %esi,(%esp)
      return 0;
80102652:	31 f6                	xor    %esi,%esi
  iput(ip);
80102654:	e8 37 f9 ff ff       	call   80101f90 <iput>
      return 0;
80102659:	83 c4 10             	add    $0x10,%esp
}
8010265c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010265f:	89 f0                	mov    %esi,%eax
80102661:	5b                   	pop    %ebx
80102662:	5e                   	pop    %esi
80102663:	5f                   	pop    %edi
80102664:	5d                   	pop    %ebp
80102665:	c3                   	ret    
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80102666:	83 ec 0c             	sub    $0xc,%esp
80102669:	89 55 e4             	mov    %edx,-0x1c(%ebp)
8010266c:	52                   	push   %edx
8010266d:	e8 8e 28 00 00       	call   80104f00 <holdingsleep>
80102672:	83 c4 10             	add    $0x10,%esp
80102675:	85 c0                	test   %eax,%eax
80102677:	74 53                	je     801026cc <namex+0x23c>
80102679:	8b 4e 08             	mov    0x8(%esi),%ecx
8010267c:	85 c9                	test   %ecx,%ecx
8010267e:	7e 4c                	jle    801026cc <namex+0x23c>
  releasesleep(&ip->lock);
80102680:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80102683:	83 ec 0c             	sub    $0xc,%esp
80102686:	52                   	push   %edx
80102687:	eb c1                	jmp    8010264a <namex+0x1ba>
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80102689:	83 ec 0c             	sub    $0xc,%esp
8010268c:	8d 5e 0c             	lea    0xc(%esi),%ebx
8010268f:	53                   	push   %ebx
80102690:	e8 6b 28 00 00       	call   80104f00 <holdingsleep>
80102695:	83 c4 10             	add    $0x10,%esp
80102698:	85 c0                	test   %eax,%eax
8010269a:	74 30                	je     801026cc <namex+0x23c>
8010269c:	8b 7e 08             	mov    0x8(%esi),%edi
8010269f:	85 ff                	test   %edi,%edi
801026a1:	7e 29                	jle    801026cc <namex+0x23c>
  releasesleep(&ip->lock);
801026a3:	83 ec 0c             	sub    $0xc,%esp
801026a6:	53                   	push   %ebx
801026a7:	e8 14 28 00 00       	call   80104ec0 <releasesleep>
}
801026ac:	83 c4 10             	add    $0x10,%esp
}
801026af:	8d 65 f4             	lea    -0xc(%ebp),%esp
801026b2:	89 f0                	mov    %esi,%eax
801026b4:	5b                   	pop    %ebx
801026b5:	5e                   	pop    %esi
801026b6:	5f                   	pop    %edi
801026b7:	5d                   	pop    %ebp
801026b8:	c3                   	ret    
    iput(ip);
801026b9:	83 ec 0c             	sub    $0xc,%esp
801026bc:	56                   	push   %esi
    return 0;
801026bd:	31 f6                	xor    %esi,%esi
    iput(ip);
801026bf:	e8 cc f8 ff ff       	call   80101f90 <iput>
    return 0;
801026c4:	83 c4 10             	add    $0x10,%esp
801026c7:	e9 2f ff ff ff       	jmp    801025fb <namex+0x16b>
    panic("iunlock");
801026cc:	83 ec 0c             	sub    $0xc,%esp
801026cf:	68 d7 80 10 80       	push   $0x801080d7
801026d4:	e8 b7 dc ff ff       	call   80100390 <panic>
801026d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801026e0 <dirlink>:
{
801026e0:	f3 0f 1e fb          	endbr32 
801026e4:	55                   	push   %ebp
801026e5:	89 e5                	mov    %esp,%ebp
801026e7:	57                   	push   %edi
801026e8:	56                   	push   %esi
801026e9:	53                   	push   %ebx
801026ea:	83 ec 20             	sub    $0x20,%esp
801026ed:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if((ip = dirlookup(dp, name, 0)) != 0){
801026f0:	6a 00                	push   $0x0
801026f2:	ff 75 0c             	push   0xc(%ebp)
801026f5:	53                   	push   %ebx
801026f6:	e8 d5 fc ff ff       	call   801023d0 <dirlookup>
801026fb:	83 c4 10             	add    $0x10,%esp
801026fe:	85 c0                	test   %eax,%eax
80102700:	75 6b                	jne    8010276d <dirlink+0x8d>
  for(off = 0; off < dp->size; off += sizeof(de)){
80102702:	8b 7b 58             	mov    0x58(%ebx),%edi
80102705:	8d 75 d8             	lea    -0x28(%ebp),%esi
80102708:	85 ff                	test   %edi,%edi
8010270a:	74 2d                	je     80102739 <dirlink+0x59>
8010270c:	31 ff                	xor    %edi,%edi
8010270e:	8d 75 d8             	lea    -0x28(%ebp),%esi
80102711:	eb 0d                	jmp    80102720 <dirlink+0x40>
80102713:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102717:	90                   	nop
80102718:	83 c7 10             	add    $0x10,%edi
8010271b:	3b 7b 58             	cmp    0x58(%ebx),%edi
8010271e:	73 19                	jae    80102739 <dirlink+0x59>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102720:	6a 10                	push   $0x10
80102722:	57                   	push   %edi
80102723:	56                   	push   %esi
80102724:	53                   	push   %ebx
80102725:	e8 56 fa ff ff       	call   80102180 <readi>
8010272a:	83 c4 10             	add    $0x10,%esp
8010272d:	83 f8 10             	cmp    $0x10,%eax
80102730:	75 4e                	jne    80102780 <dirlink+0xa0>
    if(de.inum == 0)
80102732:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80102737:	75 df                	jne    80102718 <dirlink+0x38>
  strncpy(de.name, name, DIRSIZ);
80102739:	83 ec 04             	sub    $0x4,%esp
8010273c:	8d 45 da             	lea    -0x26(%ebp),%eax
8010273f:	6a 0e                	push   $0xe
80102741:	ff 75 0c             	push   0xc(%ebp)
80102744:	50                   	push   %eax
80102745:	e8 46 2c 00 00       	call   80105390 <strncpy>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010274a:	6a 10                	push   $0x10
  de.inum = inum;
8010274c:	8b 45 10             	mov    0x10(%ebp),%eax
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010274f:	57                   	push   %edi
80102750:	56                   	push   %esi
80102751:	53                   	push   %ebx
  de.inum = inum;
80102752:	66 89 45 d8          	mov    %ax,-0x28(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102756:	e8 25 fb ff ff       	call   80102280 <writei>
8010275b:	83 c4 20             	add    $0x20,%esp
8010275e:	83 f8 10             	cmp    $0x10,%eax
80102761:	75 2a                	jne    8010278d <dirlink+0xad>
  return 0;
80102763:	31 c0                	xor    %eax,%eax
}
80102765:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102768:	5b                   	pop    %ebx
80102769:	5e                   	pop    %esi
8010276a:	5f                   	pop    %edi
8010276b:	5d                   	pop    %ebp
8010276c:	c3                   	ret    
    iput(ip);
8010276d:	83 ec 0c             	sub    $0xc,%esp
80102770:	50                   	push   %eax
80102771:	e8 1a f8 ff ff       	call   80101f90 <iput>
    return -1;
80102776:	83 c4 10             	add    $0x10,%esp
80102779:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010277e:	eb e5                	jmp    80102765 <dirlink+0x85>
      panic("dirlink read");
80102780:	83 ec 0c             	sub    $0xc,%esp
80102783:	68 00 81 10 80       	push   $0x80108100
80102788:	e8 03 dc ff ff       	call   80100390 <panic>
    panic("dirlink");
8010278d:	83 ec 0c             	sub    $0xc,%esp
80102790:	68 2e 87 10 80       	push   $0x8010872e
80102795:	e8 f6 db ff ff       	call   80100390 <panic>
8010279a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801027a0 <namei>:

struct inode*
namei(char *path)
{
801027a0:	f3 0f 1e fb          	endbr32 
801027a4:	55                   	push   %ebp
  char name[DIRSIZ];
  return namex(path, 0, name);
801027a5:	31 d2                	xor    %edx,%edx
{
801027a7:	89 e5                	mov    %esp,%ebp
801027a9:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 0, name);
801027ac:	8b 45 08             	mov    0x8(%ebp),%eax
801027af:	8d 4d ea             	lea    -0x16(%ebp),%ecx
801027b2:	e8 d9 fc ff ff       	call   80102490 <namex>
}
801027b7:	c9                   	leave  
801027b8:	c3                   	ret    
801027b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801027c0 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
801027c0:	f3 0f 1e fb          	endbr32 
801027c4:	55                   	push   %ebp
  return namex(path, 1, name);
801027c5:	ba 01 00 00 00       	mov    $0x1,%edx
{
801027ca:	89 e5                	mov    %esp,%ebp
  return namex(path, 1, name);
801027cc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801027cf:	8b 45 08             	mov    0x8(%ebp),%eax
}
801027d2:	5d                   	pop    %ebp
  return namex(path, 1, name);
801027d3:	e9 b8 fc ff ff       	jmp    80102490 <namex>
801027d8:	66 90                	xchg   %ax,%ax
801027da:	66 90                	xchg   %ax,%ax
801027dc:	66 90                	xchg   %ax,%ax
801027de:	66 90                	xchg   %ax,%ax

801027e0 <idestart>:
}

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
801027e0:	55                   	push   %ebp
801027e1:	89 e5                	mov    %esp,%ebp
801027e3:	57                   	push   %edi
801027e4:	56                   	push   %esi
801027e5:	53                   	push   %ebx
801027e6:	83 ec 0c             	sub    $0xc,%esp
  if(b == 0)
801027e9:	85 c0                	test   %eax,%eax
801027eb:	0f 84 b4 00 00 00    	je     801028a5 <idestart+0xc5>
    panic("idestart");
  if(b->blockno >= FSSIZE)
801027f1:	8b 70 08             	mov    0x8(%eax),%esi
801027f4:	89 c3                	mov    %eax,%ebx
801027f6:	81 fe e7 03 00 00    	cmp    $0x3e7,%esi
801027fc:	0f 87 96 00 00 00    	ja     80102898 <idestart+0xb8>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102802:	b9 f7 01 00 00       	mov    $0x1f7,%ecx
80102807:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010280e:	66 90                	xchg   %ax,%ax
80102810:	89 ca                	mov    %ecx,%edx
80102812:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102813:	83 e0 c0             	and    $0xffffffc0,%eax
80102816:	3c 40                	cmp    $0x40,%al
80102818:	75 f6                	jne    80102810 <idestart+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010281a:	31 ff                	xor    %edi,%edi
8010281c:	ba f6 03 00 00       	mov    $0x3f6,%edx
80102821:	89 f8                	mov    %edi,%eax
80102823:	ee                   	out    %al,(%dx)
80102824:	b8 01 00 00 00       	mov    $0x1,%eax
80102829:	ba f2 01 00 00       	mov    $0x1f2,%edx
8010282e:	ee                   	out    %al,(%dx)
8010282f:	ba f3 01 00 00       	mov    $0x1f3,%edx
80102834:	89 f0                	mov    %esi,%eax
80102836:	ee                   	out    %al,(%dx)

  idewait(0);
  outb(0x3f6, 0);  // generate interrupt
  outb(0x1f2, sector_per_block);  // number of sectors
  outb(0x1f3, sector & 0xff);
  outb(0x1f4, (sector >> 8) & 0xff);
80102837:	89 f0                	mov    %esi,%eax
80102839:	ba f4 01 00 00       	mov    $0x1f4,%edx
8010283e:	c1 f8 08             	sar    $0x8,%eax
80102841:	ee                   	out    %al,(%dx)
80102842:	ba f5 01 00 00       	mov    $0x1f5,%edx
80102847:	89 f8                	mov    %edi,%eax
80102849:	ee                   	out    %al,(%dx)
  outb(0x1f5, (sector >> 16) & 0xff);
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
8010284a:	0f b6 43 04          	movzbl 0x4(%ebx),%eax
8010284e:	ba f6 01 00 00       	mov    $0x1f6,%edx
80102853:	c1 e0 04             	shl    $0x4,%eax
80102856:	83 e0 10             	and    $0x10,%eax
80102859:	83 c8 e0             	or     $0xffffffe0,%eax
8010285c:	ee                   	out    %al,(%dx)
  if(b->flags & B_DIRTY){
8010285d:	f6 03 04             	testb  $0x4,(%ebx)
80102860:	75 16                	jne    80102878 <idestart+0x98>
80102862:	b8 20 00 00 00       	mov    $0x20,%eax
80102867:	89 ca                	mov    %ecx,%edx
80102869:	ee                   	out    %al,(%dx)
    outb(0x1f7, write_cmd);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, read_cmd);
  }
}
8010286a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010286d:	5b                   	pop    %ebx
8010286e:	5e                   	pop    %esi
8010286f:	5f                   	pop    %edi
80102870:	5d                   	pop    %ebp
80102871:	c3                   	ret    
80102872:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80102878:	b8 30 00 00 00       	mov    $0x30,%eax
8010287d:	89 ca                	mov    %ecx,%edx
8010287f:	ee                   	out    %al,(%dx)
  asm volatile("cld; rep outsl" :
80102880:	b9 80 00 00 00       	mov    $0x80,%ecx
    outsl(0x1f0, b->data, BSIZE/4);
80102885:	8d 73 5c             	lea    0x5c(%ebx),%esi
80102888:	ba f0 01 00 00       	mov    $0x1f0,%edx
8010288d:	fc                   	cld    
8010288e:	f3 6f                	rep outsl %ds:(%esi),(%dx)
}
80102890:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102893:	5b                   	pop    %ebx
80102894:	5e                   	pop    %esi
80102895:	5f                   	pop    %edi
80102896:	5d                   	pop    %ebp
80102897:	c3                   	ret    
    panic("incorrect blockno");
80102898:	83 ec 0c             	sub    $0xc,%esp
8010289b:	68 6c 81 10 80       	push   $0x8010816c
801028a0:	e8 eb da ff ff       	call   80100390 <panic>
    panic("idestart");
801028a5:	83 ec 0c             	sub    $0xc,%esp
801028a8:	68 63 81 10 80       	push   $0x80108163
801028ad:	e8 de da ff ff       	call   80100390 <panic>
801028b2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801028b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801028c0 <ideinit>:
{
801028c0:	f3 0f 1e fb          	endbr32 
801028c4:	55                   	push   %ebp
801028c5:	89 e5                	mov    %esp,%ebp
801028c7:	83 ec 10             	sub    $0x10,%esp
  initlock(&idelock, "ide");
801028ca:	68 7e 81 10 80       	push   $0x8010817e
801028cf:	68 00 26 11 80       	push   $0x80112600
801028d4:	e8 87 26 00 00       	call   80104f60 <initlock>
  ioapicenable(IRQ_IDE, ncpu - 1);
801028d9:	58                   	pop    %eax
801028da:	a1 84 27 11 80       	mov    0x80112784,%eax
801028df:	5a                   	pop    %edx
801028e0:	83 e8 01             	sub    $0x1,%eax
801028e3:	50                   	push   %eax
801028e4:	6a 0e                	push   $0xe
801028e6:	e8 b5 02 00 00       	call   80102ba0 <ioapicenable>
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
801028eb:	83 c4 10             	add    $0x10,%esp
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801028ee:	ba f7 01 00 00       	mov    $0x1f7,%edx
801028f3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801028f7:	90                   	nop
801028f8:	ec                   	in     (%dx),%al
801028f9:	83 e0 c0             	and    $0xffffffc0,%eax
801028fc:	3c 40                	cmp    $0x40,%al
801028fe:	75 f8                	jne    801028f8 <ideinit+0x38>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102900:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
80102905:	ba f6 01 00 00       	mov    $0x1f6,%edx
8010290a:	ee                   	out    %al,(%dx)
8010290b:	b9 e8 03 00 00       	mov    $0x3e8,%ecx
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102910:	ba f7 01 00 00       	mov    $0x1f7,%edx
80102915:	eb 0e                	jmp    80102925 <ideinit+0x65>
80102917:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010291e:	66 90                	xchg   %ax,%ax
  for(i=0; i<1000; i++){
80102920:	83 e9 01             	sub    $0x1,%ecx
80102923:	74 0f                	je     80102934 <ideinit+0x74>
80102925:	ec                   	in     (%dx),%al
    if(inb(0x1f7) != 0){
80102926:	84 c0                	test   %al,%al
80102928:	74 f6                	je     80102920 <ideinit+0x60>
      havedisk1 = 1;
8010292a:	c7 05 e0 25 11 80 01 	movl   $0x1,0x801125e0
80102931:	00 00 00 
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102934:	b8 e0 ff ff ff       	mov    $0xffffffe0,%eax
80102939:	ba f6 01 00 00       	mov    $0x1f6,%edx
8010293e:	ee                   	out    %al,(%dx)
}
8010293f:	c9                   	leave  
80102940:	c3                   	ret    
80102941:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102948:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010294f:	90                   	nop

80102950 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102950:	f3 0f 1e fb          	endbr32 
80102954:	55                   	push   %ebp
80102955:	89 e5                	mov    %esp,%ebp
80102957:	57                   	push   %edi
80102958:	56                   	push   %esi
80102959:	53                   	push   %ebx
8010295a:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
8010295d:	68 00 26 11 80       	push   $0x80112600
80102962:	e8 f9 27 00 00       	call   80105160 <acquire>

  if((b = idequeue) == 0){
80102967:	8b 1d e4 25 11 80    	mov    0x801125e4,%ebx
8010296d:	83 c4 10             	add    $0x10,%esp
80102970:	85 db                	test   %ebx,%ebx
80102972:	74 5f                	je     801029d3 <ideintr+0x83>
    release(&idelock);
    return;
  }
  idequeue = b->qnext;
80102974:	8b 43 58             	mov    0x58(%ebx),%eax
80102977:	a3 e4 25 11 80       	mov    %eax,0x801125e4

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
8010297c:	8b 33                	mov    (%ebx),%esi
8010297e:	f7 c6 04 00 00 00    	test   $0x4,%esi
80102984:	75 2b                	jne    801029b1 <ideintr+0x61>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102986:	ba f7 01 00 00       	mov    $0x1f7,%edx
8010298b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010298f:	90                   	nop
80102990:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102991:	89 c1                	mov    %eax,%ecx
80102993:	83 e1 c0             	and    $0xffffffc0,%ecx
80102996:	80 f9 40             	cmp    $0x40,%cl
80102999:	75 f5                	jne    80102990 <ideintr+0x40>
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
8010299b:	a8 21                	test   $0x21,%al
8010299d:	75 12                	jne    801029b1 <ideintr+0x61>
    insl(0x1f0, b->data, BSIZE/4);
8010299f:	8d 7b 5c             	lea    0x5c(%ebx),%edi
  asm volatile("cld; rep insl" :
801029a2:	b9 80 00 00 00       	mov    $0x80,%ecx
801029a7:	ba f0 01 00 00       	mov    $0x1f0,%edx
801029ac:	fc                   	cld    
801029ad:	f3 6d                	rep insl (%dx),%es:(%edi)

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
801029af:	8b 33                	mov    (%ebx),%esi
  b->flags &= ~B_DIRTY;
801029b1:	83 e6 fb             	and    $0xfffffffb,%esi
  wakeup(b);
801029b4:	83 ec 0c             	sub    $0xc,%esp
  b->flags &= ~B_DIRTY;
801029b7:	83 ce 02             	or     $0x2,%esi
801029ba:	89 33                	mov    %esi,(%ebx)
  wakeup(b);
801029bc:	53                   	push   %ebx
801029bd:	e8 5e 21 00 00       	call   80104b20 <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
801029c2:	a1 e4 25 11 80       	mov    0x801125e4,%eax
801029c7:	83 c4 10             	add    $0x10,%esp
801029ca:	85 c0                	test   %eax,%eax
801029cc:	74 05                	je     801029d3 <ideintr+0x83>
    idestart(idequeue);
801029ce:	e8 0d fe ff ff       	call   801027e0 <idestart>
    release(&idelock);
801029d3:	83 ec 0c             	sub    $0xc,%esp
801029d6:	68 00 26 11 80       	push   $0x80112600
801029db:	e8 10 27 00 00       	call   801050f0 <release>

  release(&idelock);
}
801029e0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801029e3:	5b                   	pop    %ebx
801029e4:	5e                   	pop    %esi
801029e5:	5f                   	pop    %edi
801029e6:	5d                   	pop    %ebp
801029e7:	c3                   	ret    
801029e8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801029ef:	90                   	nop

801029f0 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
801029f0:	f3 0f 1e fb          	endbr32 
801029f4:	55                   	push   %ebp
801029f5:	89 e5                	mov    %esp,%ebp
801029f7:	53                   	push   %ebx
801029f8:	83 ec 10             	sub    $0x10,%esp
801029fb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf **pp;

  if(!holdingsleep(&b->lock))
801029fe:	8d 43 0c             	lea    0xc(%ebx),%eax
80102a01:	50                   	push   %eax
80102a02:	e8 f9 24 00 00       	call   80104f00 <holdingsleep>
80102a07:	83 c4 10             	add    $0x10,%esp
80102a0a:	85 c0                	test   %eax,%eax
80102a0c:	0f 84 cf 00 00 00    	je     80102ae1 <iderw+0xf1>
    panic("iderw: buf not locked");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
80102a12:	8b 03                	mov    (%ebx),%eax
80102a14:	83 e0 06             	and    $0x6,%eax
80102a17:	83 f8 02             	cmp    $0x2,%eax
80102a1a:	0f 84 b4 00 00 00    	je     80102ad4 <iderw+0xe4>
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
80102a20:	8b 53 04             	mov    0x4(%ebx),%edx
80102a23:	85 d2                	test   %edx,%edx
80102a25:	74 0d                	je     80102a34 <iderw+0x44>
80102a27:	a1 e0 25 11 80       	mov    0x801125e0,%eax
80102a2c:	85 c0                	test   %eax,%eax
80102a2e:	0f 84 93 00 00 00    	je     80102ac7 <iderw+0xd7>
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);  //DOC:acquire-lock
80102a34:	83 ec 0c             	sub    $0xc,%esp
80102a37:	68 00 26 11 80       	push   $0x80112600
80102a3c:	e8 1f 27 00 00       	call   80105160 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102a41:	a1 e4 25 11 80       	mov    0x801125e4,%eax
  b->qnext = 0;
80102a46:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102a4d:	83 c4 10             	add    $0x10,%esp
80102a50:	85 c0                	test   %eax,%eax
80102a52:	74 6c                	je     80102ac0 <iderw+0xd0>
80102a54:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102a58:	89 c2                	mov    %eax,%edx
80102a5a:	8b 40 58             	mov    0x58(%eax),%eax
80102a5d:	85 c0                	test   %eax,%eax
80102a5f:	75 f7                	jne    80102a58 <iderw+0x68>
80102a61:	83 c2 58             	add    $0x58,%edx
    ;
  *pp = b;
80102a64:	89 1a                	mov    %ebx,(%edx)

  // Start disk if necessary.
  if(idequeue == b)
80102a66:	39 1d e4 25 11 80    	cmp    %ebx,0x801125e4
80102a6c:	74 42                	je     80102ab0 <iderw+0xc0>
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
80102a6e:	8b 03                	mov    (%ebx),%eax
80102a70:	83 e0 06             	and    $0x6,%eax
80102a73:	83 f8 02             	cmp    $0x2,%eax
80102a76:	74 23                	je     80102a9b <iderw+0xab>
80102a78:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102a7f:	90                   	nop
    sleep(b, &idelock);
80102a80:	83 ec 08             	sub    $0x8,%esp
80102a83:	68 00 26 11 80       	push   $0x80112600
80102a88:	53                   	push   %ebx
80102a89:	e8 d2 1f 00 00       	call   80104a60 <sleep>
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
80102a8e:	8b 03                	mov    (%ebx),%eax
80102a90:	83 c4 10             	add    $0x10,%esp
80102a93:	83 e0 06             	and    $0x6,%eax
80102a96:	83 f8 02             	cmp    $0x2,%eax
80102a99:	75 e5                	jne    80102a80 <iderw+0x90>
  }


  release(&idelock);
80102a9b:	c7 45 08 00 26 11 80 	movl   $0x80112600,0x8(%ebp)
}
80102aa2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102aa5:	c9                   	leave  
  release(&idelock);
80102aa6:	e9 45 26 00 00       	jmp    801050f0 <release>
80102aab:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102aaf:	90                   	nop
    idestart(b);
80102ab0:	89 d8                	mov    %ebx,%eax
80102ab2:	e8 29 fd ff ff       	call   801027e0 <idestart>
80102ab7:	eb b5                	jmp    80102a6e <iderw+0x7e>
80102ab9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102ac0:	ba e4 25 11 80       	mov    $0x801125e4,%edx
80102ac5:	eb 9d                	jmp    80102a64 <iderw+0x74>
    panic("iderw: ide disk 1 not present");
80102ac7:	83 ec 0c             	sub    $0xc,%esp
80102aca:	68 ad 81 10 80       	push   $0x801081ad
80102acf:	e8 bc d8 ff ff       	call   80100390 <panic>
    panic("iderw: nothing to do");
80102ad4:	83 ec 0c             	sub    $0xc,%esp
80102ad7:	68 98 81 10 80       	push   $0x80108198
80102adc:	e8 af d8 ff ff       	call   80100390 <panic>
    panic("iderw: buf not locked");
80102ae1:	83 ec 0c             	sub    $0xc,%esp
80102ae4:	68 82 81 10 80       	push   $0x80108182
80102ae9:	e8 a2 d8 ff ff       	call   80100390 <panic>
80102aee:	66 90                	xchg   %ax,%ax

80102af0 <ioapicinit>:
  ioapic->data = data;
}

void
ioapicinit(void)
{
80102af0:	f3 0f 1e fb          	endbr32 
80102af4:	55                   	push   %ebp
  int i, id, maxintr;

  ioapic = (volatile struct ioapic*)IOAPIC;
80102af5:	c7 05 34 26 11 80 00 	movl   $0xfec00000,0x80112634
80102afc:	00 c0 fe 
{
80102aff:	89 e5                	mov    %esp,%ebp
80102b01:	56                   	push   %esi
80102b02:	53                   	push   %ebx
  ioapic->reg = reg;
80102b03:	c7 05 00 00 c0 fe 01 	movl   $0x1,0xfec00000
80102b0a:	00 00 00 
  return ioapic->data;
80102b0d:	8b 15 34 26 11 80    	mov    0x80112634,%edx
80102b13:	8b 72 10             	mov    0x10(%edx),%esi
  ioapic->reg = reg;
80102b16:	c7 02 00 00 00 00    	movl   $0x0,(%edx)
  return ioapic->data;
80102b1c:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
80102b22:	0f b6 15 80 27 11 80 	movzbl 0x80112780,%edx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
80102b29:	c1 ee 10             	shr    $0x10,%esi
80102b2c:	89 f0                	mov    %esi,%eax
80102b2e:	0f b6 f0             	movzbl %al,%esi
  return ioapic->data;
80102b31:	8b 41 10             	mov    0x10(%ecx),%eax
  id = ioapicread(REG_ID) >> 24;
80102b34:	c1 e8 18             	shr    $0x18,%eax
  if(id != ioapicid)
80102b37:	39 c2                	cmp    %eax,%edx
80102b39:	74 16                	je     80102b51 <ioapicinit+0x61>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80102b3b:	83 ec 0c             	sub    $0xc,%esp
80102b3e:	68 cc 81 10 80       	push   $0x801081cc
80102b43:	e8 e8 db ff ff       	call   80100730 <cprintf>
  ioapic->reg = reg;
80102b48:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
80102b4e:	83 c4 10             	add    $0x10,%esp
80102b51:	83 c6 21             	add    $0x21,%esi
{
80102b54:	ba 10 00 00 00       	mov    $0x10,%edx
80102b59:	b8 20 00 00 00       	mov    $0x20,%eax
80102b5e:	66 90                	xchg   %ax,%ax
  ioapic->reg = reg;
80102b60:	89 11                	mov    %edx,(%ecx)

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80102b62:	89 c3                	mov    %eax,%ebx
  ioapic->data = data;
80102b64:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  for(i = 0; i <= maxintr; i++){
80102b6a:	83 c0 01             	add    $0x1,%eax
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80102b6d:	81 cb 00 00 01 00    	or     $0x10000,%ebx
  ioapic->data = data;
80102b73:	89 59 10             	mov    %ebx,0x10(%ecx)
  ioapic->reg = reg;
80102b76:	8d 5a 01             	lea    0x1(%edx),%ebx
  for(i = 0; i <= maxintr; i++){
80102b79:	83 c2 02             	add    $0x2,%edx
  ioapic->reg = reg;
80102b7c:	89 19                	mov    %ebx,(%ecx)
  ioapic->data = data;
80102b7e:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
80102b84:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
  for(i = 0; i <= maxintr; i++){
80102b8b:	39 f0                	cmp    %esi,%eax
80102b8d:	75 d1                	jne    80102b60 <ioapicinit+0x70>
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
80102b8f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102b92:	5b                   	pop    %ebx
80102b93:	5e                   	pop    %esi
80102b94:	5d                   	pop    %ebp
80102b95:	c3                   	ret    
80102b96:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102b9d:	8d 76 00             	lea    0x0(%esi),%esi

80102ba0 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
80102ba0:	f3 0f 1e fb          	endbr32 
80102ba4:	55                   	push   %ebp
  ioapic->reg = reg;
80102ba5:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
{
80102bab:	89 e5                	mov    %esp,%ebp
80102bad:	8b 45 08             	mov    0x8(%ebp),%eax
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80102bb0:	8d 50 20             	lea    0x20(%eax),%edx
80102bb3:	8d 44 00 10          	lea    0x10(%eax,%eax,1),%eax
  ioapic->reg = reg;
80102bb7:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80102bb9:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102bbf:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
80102bc2:	89 51 10             	mov    %edx,0x10(%ecx)
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102bc5:	8b 55 0c             	mov    0xc(%ebp),%edx
  ioapic->reg = reg;
80102bc8:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80102bca:	a1 34 26 11 80       	mov    0x80112634,%eax
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102bcf:	c1 e2 18             	shl    $0x18,%edx
  ioapic->data = data;
80102bd2:	89 50 10             	mov    %edx,0x10(%eax)
}
80102bd5:	5d                   	pop    %ebp
80102bd6:	c3                   	ret    
80102bd7:	66 90                	xchg   %ax,%ax
80102bd9:	66 90                	xchg   %ax,%ax
80102bdb:	66 90                	xchg   %ax,%ax
80102bdd:	66 90                	xchg   %ax,%ax
80102bdf:	90                   	nop

80102be0 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80102be0:	f3 0f 1e fb          	endbr32 
80102be4:	55                   	push   %ebp
80102be5:	89 e5                	mov    %esp,%ebp
80102be7:	53                   	push   %ebx
80102be8:	83 ec 04             	sub    $0x4,%esp
80102beb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
80102bee:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
80102bf4:	75 7a                	jne    80102c70 <kfree+0x90>
80102bf6:	81 fb d0 6a 11 80    	cmp    $0x80116ad0,%ebx
80102bfc:	72 72                	jb     80102c70 <kfree+0x90>
80102bfe:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80102c04:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80102c09:	77 65                	ja     80102c70 <kfree+0x90>
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
80102c0b:	83 ec 04             	sub    $0x4,%esp
80102c0e:	68 00 10 00 00       	push   $0x1000
80102c13:	6a 01                	push   $0x1
80102c15:	53                   	push   %ebx
80102c16:	e8 15 26 00 00       	call   80105230 <memset>

  if(kmem.use_lock)
80102c1b:	8b 15 74 26 11 80    	mov    0x80112674,%edx
80102c21:	83 c4 10             	add    $0x10,%esp
80102c24:	85 d2                	test   %edx,%edx
80102c26:	75 20                	jne    80102c48 <kfree+0x68>
    acquire(&kmem.lock);
  r = (struct run*)v;
  r->next = kmem.freelist;
80102c28:	a1 78 26 11 80       	mov    0x80112678,%eax
80102c2d:	89 03                	mov    %eax,(%ebx)
  kmem.freelist = r;
  if(kmem.use_lock)
80102c2f:	a1 74 26 11 80       	mov    0x80112674,%eax
  kmem.freelist = r;
80102c34:	89 1d 78 26 11 80    	mov    %ebx,0x80112678
  if(kmem.use_lock)
80102c3a:	85 c0                	test   %eax,%eax
80102c3c:	75 22                	jne    80102c60 <kfree+0x80>
    release(&kmem.lock);
}
80102c3e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102c41:	c9                   	leave  
80102c42:	c3                   	ret    
80102c43:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102c47:	90                   	nop
    acquire(&kmem.lock);
80102c48:	83 ec 0c             	sub    $0xc,%esp
80102c4b:	68 40 26 11 80       	push   $0x80112640
80102c50:	e8 0b 25 00 00       	call   80105160 <acquire>
80102c55:	83 c4 10             	add    $0x10,%esp
80102c58:	eb ce                	jmp    80102c28 <kfree+0x48>
80102c5a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    release(&kmem.lock);
80102c60:	c7 45 08 40 26 11 80 	movl   $0x80112640,0x8(%ebp)
}
80102c67:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102c6a:	c9                   	leave  
    release(&kmem.lock);
80102c6b:	e9 80 24 00 00       	jmp    801050f0 <release>
    panic("kfree");
80102c70:	83 ec 0c             	sub    $0xc,%esp
80102c73:	68 fe 81 10 80       	push   $0x801081fe
80102c78:	e8 13 d7 ff ff       	call   80100390 <panic>
80102c7d:	8d 76 00             	lea    0x0(%esi),%esi

80102c80 <freerange>:
{
80102c80:	f3 0f 1e fb          	endbr32 
80102c84:	55                   	push   %ebp
80102c85:	89 e5                	mov    %esp,%ebp
80102c87:	56                   	push   %esi
  p = (char*)PGROUNDUP((uint)vstart);
80102c88:	8b 45 08             	mov    0x8(%ebp),%eax
{
80102c8b:	8b 75 0c             	mov    0xc(%ebp),%esi
80102c8e:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
80102c8f:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102c95:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102c9b:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80102ca1:	39 de                	cmp    %ebx,%esi
80102ca3:	72 1f                	jb     80102cc4 <freerange+0x44>
80102ca5:	8d 76 00             	lea    0x0(%esi),%esi
    kfree(p);
80102ca8:	83 ec 0c             	sub    $0xc,%esp
80102cab:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102cb1:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102cb7:	50                   	push   %eax
80102cb8:	e8 23 ff ff ff       	call   80102be0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102cbd:	83 c4 10             	add    $0x10,%esp
80102cc0:	39 f3                	cmp    %esi,%ebx
80102cc2:	76 e4                	jbe    80102ca8 <freerange+0x28>
}
80102cc4:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102cc7:	5b                   	pop    %ebx
80102cc8:	5e                   	pop    %esi
80102cc9:	5d                   	pop    %ebp
80102cca:	c3                   	ret    
80102ccb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102ccf:	90                   	nop

80102cd0 <kinit2>:
{
80102cd0:	f3 0f 1e fb          	endbr32 
80102cd4:	55                   	push   %ebp
80102cd5:	89 e5                	mov    %esp,%ebp
80102cd7:	56                   	push   %esi
  p = (char*)PGROUNDUP((uint)vstart);
80102cd8:	8b 45 08             	mov    0x8(%ebp),%eax
{
80102cdb:	8b 75 0c             	mov    0xc(%ebp),%esi
80102cde:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
80102cdf:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102ce5:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102ceb:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80102cf1:	39 de                	cmp    %ebx,%esi
80102cf3:	72 1f                	jb     80102d14 <kinit2+0x44>
80102cf5:	8d 76 00             	lea    0x0(%esi),%esi
    kfree(p);
80102cf8:	83 ec 0c             	sub    $0xc,%esp
80102cfb:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102d01:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102d07:	50                   	push   %eax
80102d08:	e8 d3 fe ff ff       	call   80102be0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102d0d:	83 c4 10             	add    $0x10,%esp
80102d10:	39 de                	cmp    %ebx,%esi
80102d12:	73 e4                	jae    80102cf8 <kinit2+0x28>
  kmem.use_lock = 1;
80102d14:	c7 05 74 26 11 80 01 	movl   $0x1,0x80112674
80102d1b:	00 00 00 
}
80102d1e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102d21:	5b                   	pop    %ebx
80102d22:	5e                   	pop    %esi
80102d23:	5d                   	pop    %ebp
80102d24:	c3                   	ret    
80102d25:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102d2c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102d30 <kinit1>:
{
80102d30:	f3 0f 1e fb          	endbr32 
80102d34:	55                   	push   %ebp
80102d35:	89 e5                	mov    %esp,%ebp
80102d37:	56                   	push   %esi
80102d38:	53                   	push   %ebx
80102d39:	8b 75 0c             	mov    0xc(%ebp),%esi
  initlock(&kmem.lock, "kmem");
80102d3c:	83 ec 08             	sub    $0x8,%esp
80102d3f:	68 04 82 10 80       	push   $0x80108204
80102d44:	68 40 26 11 80       	push   $0x80112640
80102d49:	e8 12 22 00 00       	call   80104f60 <initlock>
  p = (char*)PGROUNDUP((uint)vstart);
80102d4e:	8b 45 08             	mov    0x8(%ebp),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102d51:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 0;
80102d54:	c7 05 74 26 11 80 00 	movl   $0x0,0x80112674
80102d5b:	00 00 00 
  p = (char*)PGROUNDUP((uint)vstart);
80102d5e:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102d64:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102d6a:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80102d70:	39 de                	cmp    %ebx,%esi
80102d72:	72 20                	jb     80102d94 <kinit1+0x64>
80102d74:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
80102d78:	83 ec 0c             	sub    $0xc,%esp
80102d7b:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102d81:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102d87:	50                   	push   %eax
80102d88:	e8 53 fe ff ff       	call   80102be0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102d8d:	83 c4 10             	add    $0x10,%esp
80102d90:	39 de                	cmp    %ebx,%esi
80102d92:	73 e4                	jae    80102d78 <kinit1+0x48>
}
80102d94:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102d97:	5b                   	pop    %ebx
80102d98:	5e                   	pop    %esi
80102d99:	5d                   	pop    %ebp
80102d9a:	c3                   	ret    
80102d9b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102d9f:	90                   	nop

80102da0 <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
80102da0:	f3 0f 1e fb          	endbr32 
  struct run *r;

  if(kmem.use_lock)
80102da4:	a1 74 26 11 80       	mov    0x80112674,%eax
80102da9:	85 c0                	test   %eax,%eax
80102dab:	75 1b                	jne    80102dc8 <kalloc+0x28>
    acquire(&kmem.lock);
  r = kmem.freelist;
80102dad:	a1 78 26 11 80       	mov    0x80112678,%eax
  if(r)
80102db2:	85 c0                	test   %eax,%eax
80102db4:	74 0a                	je     80102dc0 <kalloc+0x20>
    kmem.freelist = r->next;
80102db6:	8b 10                	mov    (%eax),%edx
80102db8:	89 15 78 26 11 80    	mov    %edx,0x80112678
  if(kmem.use_lock)
80102dbe:	c3                   	ret    
80102dbf:	90                   	nop
    release(&kmem.lock);
  return (char*)r;
}
80102dc0:	c3                   	ret    
80102dc1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
{
80102dc8:	55                   	push   %ebp
80102dc9:	89 e5                	mov    %esp,%ebp
80102dcb:	83 ec 24             	sub    $0x24,%esp
    acquire(&kmem.lock);
80102dce:	68 40 26 11 80       	push   $0x80112640
80102dd3:	e8 88 23 00 00       	call   80105160 <acquire>
  r = kmem.freelist;
80102dd8:	a1 78 26 11 80       	mov    0x80112678,%eax
  if(kmem.use_lock)
80102ddd:	8b 15 74 26 11 80    	mov    0x80112674,%edx
  if(r)
80102de3:	83 c4 10             	add    $0x10,%esp
80102de6:	85 c0                	test   %eax,%eax
80102de8:	74 08                	je     80102df2 <kalloc+0x52>
    kmem.freelist = r->next;
80102dea:	8b 08                	mov    (%eax),%ecx
80102dec:	89 0d 78 26 11 80    	mov    %ecx,0x80112678
  if(kmem.use_lock)
80102df2:	85 d2                	test   %edx,%edx
80102df4:	74 16                	je     80102e0c <kalloc+0x6c>
    release(&kmem.lock);
80102df6:	83 ec 0c             	sub    $0xc,%esp
80102df9:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102dfc:	68 40 26 11 80       	push   $0x80112640
80102e01:	e8 ea 22 00 00       	call   801050f0 <release>
  return (char*)r;
80102e06:	8b 45 f4             	mov    -0xc(%ebp),%eax
    release(&kmem.lock);
80102e09:	83 c4 10             	add    $0x10,%esp
}
80102e0c:	c9                   	leave  
80102e0d:	c3                   	ret    
80102e0e:	66 90                	xchg   %ax,%ax

80102e10 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
80102e10:	f3 0f 1e fb          	endbr32 
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102e14:	ba 64 00 00 00       	mov    $0x64,%edx
80102e19:	ec                   	in     (%dx),%al
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
  if((st & KBS_DIB) == 0)
80102e1a:	a8 01                	test   $0x1,%al
80102e1c:	0f 84 c6 00 00 00    	je     80102ee8 <kbdgetc+0xd8>
{
80102e22:	55                   	push   %ebp
80102e23:	ba 60 00 00 00       	mov    $0x60,%edx
80102e28:	89 e5                	mov    %esp,%ebp
80102e2a:	53                   	push   %ebx
80102e2b:	ec                   	in     (%dx),%al
    return -1;
  data = inb(KBDATAP);

  if(data == 0xE0){
    shift |= E0ESC;
80102e2c:	8b 1d 7c 26 11 80    	mov    0x8011267c,%ebx
  data = inb(KBDATAP);
80102e32:	0f b6 c8             	movzbl %al,%ecx
  if(data == 0xE0){
80102e35:	3c e0                	cmp    $0xe0,%al
80102e37:	74 57                	je     80102e90 <kbdgetc+0x80>
    return 0;
  } else if(data & 0x80){
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
80102e39:	89 da                	mov    %ebx,%edx
80102e3b:	83 e2 40             	and    $0x40,%edx
  } else if(data & 0x80){
80102e3e:	84 c0                	test   %al,%al
80102e40:	78 5e                	js     80102ea0 <kbdgetc+0x90>
    shift &= ~(shiftcode[data] | E0ESC);
    return 0;
  } else if(shift & E0ESC){
80102e42:	85 d2                	test   %edx,%edx
80102e44:	74 09                	je     80102e4f <kbdgetc+0x3f>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
80102e46:	83 c8 80             	or     $0xffffff80,%eax
    shift &= ~E0ESC;
80102e49:	83 e3 bf             	and    $0xffffffbf,%ebx
    data |= 0x80;
80102e4c:	0f b6 c8             	movzbl %al,%ecx
  }

  shift |= shiftcode[data];
80102e4f:	0f b6 91 40 83 10 80 	movzbl -0x7fef7cc0(%ecx),%edx
  shift ^= togglecode[data];
80102e56:	0f b6 81 40 82 10 80 	movzbl -0x7fef7dc0(%ecx),%eax
  shift |= shiftcode[data];
80102e5d:	09 da                	or     %ebx,%edx
  shift ^= togglecode[data];
80102e5f:	31 c2                	xor    %eax,%edx
  c = charcode[shift & (CTL | SHIFT)][data];
80102e61:	89 d0                	mov    %edx,%eax
  shift ^= togglecode[data];
80102e63:	89 15 7c 26 11 80    	mov    %edx,0x8011267c
  c = charcode[shift & (CTL | SHIFT)][data];
80102e69:	83 e0 03             	and    $0x3,%eax
  if(shift & CAPSLOCK){
80102e6c:	83 e2 08             	and    $0x8,%edx
  c = charcode[shift & (CTL | SHIFT)][data];
80102e6f:	8b 04 85 20 82 10 80 	mov    -0x7fef7de0(,%eax,4),%eax
80102e76:	0f b6 04 08          	movzbl (%eax,%ecx,1),%eax
  if(shift & CAPSLOCK){
80102e7a:	74 0b                	je     80102e87 <kbdgetc+0x77>
    if('a' <= c && c <= 'z')
80102e7c:	8d 50 9f             	lea    -0x61(%eax),%edx
80102e7f:	83 fa 19             	cmp    $0x19,%edx
80102e82:	77 4c                	ja     80102ed0 <kbdgetc+0xc0>
      c += 'A' - 'a';
80102e84:	83 e8 20             	sub    $0x20,%eax
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
80102e87:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102e8a:	c9                   	leave  
80102e8b:	c3                   	ret    
80102e8c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    shift |= E0ESC;
80102e90:	83 cb 40             	or     $0x40,%ebx
    return 0;
80102e93:	31 c0                	xor    %eax,%eax
    shift |= E0ESC;
80102e95:	89 1d 7c 26 11 80    	mov    %ebx,0x8011267c
}
80102e9b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102e9e:	c9                   	leave  
80102e9f:	c3                   	ret    
    data = (shift & E0ESC ? data : data & 0x7F);
80102ea0:	83 e0 7f             	and    $0x7f,%eax
80102ea3:	85 d2                	test   %edx,%edx
80102ea5:	0f 44 c8             	cmove  %eax,%ecx
    return 0;
80102ea8:	31 c0                	xor    %eax,%eax
    shift &= ~(shiftcode[data] | E0ESC);
80102eaa:	0f b6 91 40 83 10 80 	movzbl -0x7fef7cc0(%ecx),%edx
80102eb1:	83 ca 40             	or     $0x40,%edx
80102eb4:	0f b6 d2             	movzbl %dl,%edx
80102eb7:	f7 d2                	not    %edx
80102eb9:	21 da                	and    %ebx,%edx
}
80102ebb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    shift &= ~(shiftcode[data] | E0ESC);
80102ebe:	89 15 7c 26 11 80    	mov    %edx,0x8011267c
}
80102ec4:	c9                   	leave  
80102ec5:	c3                   	ret    
80102ec6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102ecd:	8d 76 00             	lea    0x0(%esi),%esi
    else if('A' <= c && c <= 'Z')
80102ed0:	8d 48 bf             	lea    -0x41(%eax),%ecx
      c += 'a' - 'A';
80102ed3:	8d 50 20             	lea    0x20(%eax),%edx
}
80102ed6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102ed9:	c9                   	leave  
      c += 'a' - 'A';
80102eda:	83 f9 1a             	cmp    $0x1a,%ecx
80102edd:	0f 42 c2             	cmovb  %edx,%eax
}
80102ee0:	c3                   	ret    
80102ee1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80102ee8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80102eed:	c3                   	ret    
80102eee:	66 90                	xchg   %ax,%ax

80102ef0 <kbdintr>:

void
kbdintr(void)
{
80102ef0:	f3 0f 1e fb          	endbr32 
80102ef4:	55                   	push   %ebp
80102ef5:	89 e5                	mov    %esp,%ebp
80102ef7:	83 ec 14             	sub    $0x14,%esp
  consoleintr(kbdgetc);
80102efa:	68 10 2e 10 80       	push   $0x80102e10
80102eff:	e8 bc db ff ff       	call   80100ac0 <consoleintr>
}
80102f04:	83 c4 10             	add    $0x10,%esp
80102f07:	c9                   	leave  
80102f08:	c3                   	ret    
80102f09:	66 90                	xchg   %ax,%ax
80102f0b:	66 90                	xchg   %ax,%ax
80102f0d:	66 90                	xchg   %ax,%ax
80102f0f:	90                   	nop

80102f10 <lapicinit>:
  lapic[ID];  // wait for write to finish, by reading
}

void
lapicinit(void)
{
80102f10:	f3 0f 1e fb          	endbr32 
  if(!lapic)
80102f14:	a1 80 26 11 80       	mov    0x80112680,%eax
80102f19:	85 c0                	test   %eax,%eax
80102f1b:	0f 84 c7 00 00 00    	je     80102fe8 <lapicinit+0xd8>
  lapic[index] = value;
80102f21:	c7 80 f0 00 00 00 3f 	movl   $0x13f,0xf0(%eax)
80102f28:	01 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102f2b:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102f2e:	c7 80 e0 03 00 00 0b 	movl   $0xb,0x3e0(%eax)
80102f35:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102f38:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102f3b:	c7 80 20 03 00 00 20 	movl   $0x20020,0x320(%eax)
80102f42:	00 02 00 
  lapic[ID];  // wait for write to finish, by reading
80102f45:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102f48:	c7 80 80 03 00 00 80 	movl   $0x989680,0x380(%eax)
80102f4f:	96 98 00 
  lapic[ID];  // wait for write to finish, by reading
80102f52:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102f55:	c7 80 50 03 00 00 00 	movl   $0x10000,0x350(%eax)
80102f5c:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
80102f5f:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102f62:	c7 80 60 03 00 00 00 	movl   $0x10000,0x360(%eax)
80102f69:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
80102f6c:	8b 50 20             	mov    0x20(%eax),%edx
  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80102f6f:	8b 50 30             	mov    0x30(%eax),%edx
80102f72:	c1 ea 10             	shr    $0x10,%edx
80102f75:	81 e2 fc 00 00 00    	and    $0xfc,%edx
80102f7b:	75 73                	jne    80102ff0 <lapicinit+0xe0>
  lapic[index] = value;
80102f7d:	c7 80 70 03 00 00 33 	movl   $0x33,0x370(%eax)
80102f84:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102f87:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102f8a:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
80102f91:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102f94:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102f97:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
80102f9e:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102fa1:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102fa4:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
80102fab:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102fae:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102fb1:	c7 80 10 03 00 00 00 	movl   $0x0,0x310(%eax)
80102fb8:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102fbb:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102fbe:	c7 80 00 03 00 00 00 	movl   $0x88500,0x300(%eax)
80102fc5:	85 08 00 
  lapic[ID];  // wait for write to finish, by reading
80102fc8:	8b 50 20             	mov    0x20(%eax),%edx
80102fcb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102fcf:	90                   	nop
  lapicw(EOI, 0);

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS)
80102fd0:	8b 90 00 03 00 00    	mov    0x300(%eax),%edx
80102fd6:	80 e6 10             	and    $0x10,%dh
80102fd9:	75 f5                	jne    80102fd0 <lapicinit+0xc0>
  lapic[index] = value;
80102fdb:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
80102fe2:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102fe5:	8b 40 20             	mov    0x20(%eax),%eax
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
80102fe8:	c3                   	ret    
80102fe9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  lapic[index] = value;
80102ff0:	c7 80 40 03 00 00 00 	movl   $0x10000,0x340(%eax)
80102ff7:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
80102ffa:	8b 50 20             	mov    0x20(%eax),%edx
}
80102ffd:	e9 7b ff ff ff       	jmp    80102f7d <lapicinit+0x6d>
80103002:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103009:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103010 <lapicid>:

int
lapicid(void)
{
80103010:	f3 0f 1e fb          	endbr32 
  if (!lapic)
80103014:	a1 80 26 11 80       	mov    0x80112680,%eax
80103019:	85 c0                	test   %eax,%eax
8010301b:	74 0b                	je     80103028 <lapicid+0x18>
    return 0;
  return lapic[ID] >> 24;
8010301d:	8b 40 20             	mov    0x20(%eax),%eax
80103020:	c1 e8 18             	shr    $0x18,%eax
80103023:	c3                   	ret    
80103024:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return 0;
80103028:	31 c0                	xor    %eax,%eax
}
8010302a:	c3                   	ret    
8010302b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010302f:	90                   	nop

80103030 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
80103030:	f3 0f 1e fb          	endbr32 
  if(lapic)
80103034:	a1 80 26 11 80       	mov    0x80112680,%eax
80103039:	85 c0                	test   %eax,%eax
8010303b:	74 0d                	je     8010304a <lapiceoi+0x1a>
  lapic[index] = value;
8010303d:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
80103044:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80103047:	8b 40 20             	mov    0x20(%eax),%eax
    lapicw(EOI, 0);
}
8010304a:	c3                   	ret    
8010304b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010304f:	90                   	nop

80103050 <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
80103050:	f3 0f 1e fb          	endbr32 
}
80103054:	c3                   	ret    
80103055:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010305c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80103060 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80103060:	f3 0f 1e fb          	endbr32 
80103064:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103065:	b8 0f 00 00 00       	mov    $0xf,%eax
8010306a:	ba 70 00 00 00       	mov    $0x70,%edx
8010306f:	89 e5                	mov    %esp,%ebp
80103071:	53                   	push   %ebx
80103072:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80103075:	8b 5d 08             	mov    0x8(%ebp),%ebx
80103078:	ee                   	out    %al,(%dx)
80103079:	b8 0a 00 00 00       	mov    $0xa,%eax
8010307e:	ba 71 00 00 00       	mov    $0x71,%edx
80103083:	ee                   	out    %al,(%dx)
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
  outb(CMOS_PORT+1, 0x0A);
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
  wrv[0] = 0;
80103084:	31 c0                	xor    %eax,%eax
  wrv[1] = addr >> 4;

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
80103086:	c1 e3 18             	shl    $0x18,%ebx
  wrv[0] = 0;
80103089:	66 a3 67 04 00 80    	mov    %ax,0x80000467
  wrv[1] = addr >> 4;
8010308f:	89 c8                	mov    %ecx,%eax
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
80103091:	c1 e9 0c             	shr    $0xc,%ecx
  lapicw(ICRHI, apicid<<24);
80103094:	89 da                	mov    %ebx,%edx
  wrv[1] = addr >> 4;
80103096:	c1 e8 04             	shr    $0x4,%eax
    lapicw(ICRLO, STARTUP | (addr>>12));
80103099:	80 cd 06             	or     $0x6,%ch
  wrv[1] = addr >> 4;
8010309c:	66 a3 69 04 00 80    	mov    %ax,0x80000469
  lapic[index] = value;
801030a2:	a1 80 26 11 80       	mov    0x80112680,%eax
801030a7:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
801030ad:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
801030b0:	c7 80 00 03 00 00 00 	movl   $0xc500,0x300(%eax)
801030b7:	c5 00 00 
  lapic[ID];  // wait for write to finish, by reading
801030ba:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
801030bd:	c7 80 00 03 00 00 00 	movl   $0x8500,0x300(%eax)
801030c4:	85 00 00 
  lapic[ID];  // wait for write to finish, by reading
801030c7:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
801030ca:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
801030d0:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
801030d3:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
801030d9:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
801030dc:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
801030e2:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801030e5:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
801030eb:	8b 40 20             	mov    0x20(%eax),%eax
    microdelay(200);
  }
}
801030ee:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801030f1:	c9                   	leave  
801030f2:	c3                   	ret    
801030f3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801030fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80103100 <cmostime>:
}

// qemu seems to use 24-hour GWT and the values are BCD encoded
void
cmostime(struct rtcdate *r)
{
80103100:	f3 0f 1e fb          	endbr32 
80103104:	55                   	push   %ebp
80103105:	b8 0b 00 00 00       	mov    $0xb,%eax
8010310a:	ba 70 00 00 00       	mov    $0x70,%edx
8010310f:	89 e5                	mov    %esp,%ebp
80103111:	57                   	push   %edi
80103112:	56                   	push   %esi
80103113:	53                   	push   %ebx
80103114:	83 ec 4c             	sub    $0x4c,%esp
80103117:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103118:	ba 71 00 00 00       	mov    $0x71,%edx
8010311d:	ec                   	in     (%dx),%al
  struct rtcdate t1, t2;
  int sb, bcd;

  sb = cmos_read(CMOS_STATB);

  bcd = (sb & (1 << 2)) == 0;
8010311e:	83 e0 04             	and    $0x4,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103121:	bb 70 00 00 00       	mov    $0x70,%ebx
80103126:	88 45 b3             	mov    %al,-0x4d(%ebp)
80103129:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103130:	31 c0                	xor    %eax,%eax
80103132:	89 da                	mov    %ebx,%edx
80103134:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103135:	b9 71 00 00 00       	mov    $0x71,%ecx
8010313a:	89 ca                	mov    %ecx,%edx
8010313c:	ec                   	in     (%dx),%al
8010313d:	88 45 b7             	mov    %al,-0x49(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103140:	89 da                	mov    %ebx,%edx
80103142:	b8 02 00 00 00       	mov    $0x2,%eax
80103147:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103148:	89 ca                	mov    %ecx,%edx
8010314a:	ec                   	in     (%dx),%al
8010314b:	88 45 b6             	mov    %al,-0x4a(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010314e:	89 da                	mov    %ebx,%edx
80103150:	b8 04 00 00 00       	mov    $0x4,%eax
80103155:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103156:	89 ca                	mov    %ecx,%edx
80103158:	ec                   	in     (%dx),%al
80103159:	88 45 b5             	mov    %al,-0x4b(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010315c:	89 da                	mov    %ebx,%edx
8010315e:	b8 07 00 00 00       	mov    $0x7,%eax
80103163:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103164:	89 ca                	mov    %ecx,%edx
80103166:	ec                   	in     (%dx),%al
80103167:	88 45 b4             	mov    %al,-0x4c(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010316a:	89 da                	mov    %ebx,%edx
8010316c:	b8 08 00 00 00       	mov    $0x8,%eax
80103171:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103172:	89 ca                	mov    %ecx,%edx
80103174:	ec                   	in     (%dx),%al
80103175:	89 c7                	mov    %eax,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103177:	89 da                	mov    %ebx,%edx
80103179:	b8 09 00 00 00       	mov    $0x9,%eax
8010317e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010317f:	89 ca                	mov    %ecx,%edx
80103181:	ec                   	in     (%dx),%al
80103182:	89 c6                	mov    %eax,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103184:	89 da                	mov    %ebx,%edx
80103186:	b8 0a 00 00 00       	mov    $0xa,%eax
8010318b:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010318c:	89 ca                	mov    %ecx,%edx
8010318e:	ec                   	in     (%dx),%al

  // make sure CMOS doesn't modify time while we read it
  for(;;) {
    fill_rtcdate(&t1);
    if(cmos_read(CMOS_STATA) & CMOS_UIP)
8010318f:	84 c0                	test   %al,%al
80103191:	78 9d                	js     80103130 <cmostime+0x30>
  return inb(CMOS_RETURN);
80103193:	0f b6 45 b7          	movzbl -0x49(%ebp),%eax
80103197:	89 fa                	mov    %edi,%edx
80103199:	0f b6 fa             	movzbl %dl,%edi
8010319c:	89 f2                	mov    %esi,%edx
8010319e:	89 45 b8             	mov    %eax,-0x48(%ebp)
801031a1:	0f b6 45 b6          	movzbl -0x4a(%ebp),%eax
801031a5:	0f b6 f2             	movzbl %dl,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801031a8:	89 da                	mov    %ebx,%edx
801031aa:	89 7d c8             	mov    %edi,-0x38(%ebp)
801031ad:	89 45 bc             	mov    %eax,-0x44(%ebp)
801031b0:	0f b6 45 b5          	movzbl -0x4b(%ebp),%eax
801031b4:	89 75 cc             	mov    %esi,-0x34(%ebp)
801031b7:	89 45 c0             	mov    %eax,-0x40(%ebp)
801031ba:	0f b6 45 b4          	movzbl -0x4c(%ebp),%eax
801031be:	89 45 c4             	mov    %eax,-0x3c(%ebp)
801031c1:	31 c0                	xor    %eax,%eax
801031c3:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801031c4:	89 ca                	mov    %ecx,%edx
801031c6:	ec                   	in     (%dx),%al
801031c7:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801031ca:	89 da                	mov    %ebx,%edx
801031cc:	89 45 d0             	mov    %eax,-0x30(%ebp)
801031cf:	b8 02 00 00 00       	mov    $0x2,%eax
801031d4:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801031d5:	89 ca                	mov    %ecx,%edx
801031d7:	ec                   	in     (%dx),%al
801031d8:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801031db:	89 da                	mov    %ebx,%edx
801031dd:	89 45 d4             	mov    %eax,-0x2c(%ebp)
801031e0:	b8 04 00 00 00       	mov    $0x4,%eax
801031e5:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801031e6:	89 ca                	mov    %ecx,%edx
801031e8:	ec                   	in     (%dx),%al
801031e9:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801031ec:	89 da                	mov    %ebx,%edx
801031ee:	89 45 d8             	mov    %eax,-0x28(%ebp)
801031f1:	b8 07 00 00 00       	mov    $0x7,%eax
801031f6:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801031f7:	89 ca                	mov    %ecx,%edx
801031f9:	ec                   	in     (%dx),%al
801031fa:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801031fd:	89 da                	mov    %ebx,%edx
801031ff:	89 45 dc             	mov    %eax,-0x24(%ebp)
80103202:	b8 08 00 00 00       	mov    $0x8,%eax
80103207:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103208:	89 ca                	mov    %ecx,%edx
8010320a:	ec                   	in     (%dx),%al
8010320b:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010320e:	89 da                	mov    %ebx,%edx
80103210:	89 45 e0             	mov    %eax,-0x20(%ebp)
80103213:	b8 09 00 00 00       	mov    $0x9,%eax
80103218:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103219:	89 ca                	mov    %ecx,%edx
8010321b:	ec                   	in     (%dx),%al
8010321c:	0f b6 c0             	movzbl %al,%eax
        continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
8010321f:	83 ec 04             	sub    $0x4,%esp
  return inb(CMOS_RETURN);
80103222:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80103225:	8d 45 d0             	lea    -0x30(%ebp),%eax
80103228:	6a 18                	push   $0x18
8010322a:	50                   	push   %eax
8010322b:	8d 45 b8             	lea    -0x48(%ebp),%eax
8010322e:	50                   	push   %eax
8010322f:	e8 4c 20 00 00       	call   80105280 <memcmp>
80103234:	83 c4 10             	add    $0x10,%esp
80103237:	85 c0                	test   %eax,%eax
80103239:	0f 85 f1 fe ff ff    	jne    80103130 <cmostime+0x30>
      break;
  }

  // convert
  if(bcd) {
8010323f:	80 7d b3 00          	cmpb   $0x0,-0x4d(%ebp)
80103243:	75 78                	jne    801032bd <cmostime+0x1bd>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
80103245:	8b 45 b8             	mov    -0x48(%ebp),%eax
80103248:	89 c2                	mov    %eax,%edx
8010324a:	83 e0 0f             	and    $0xf,%eax
8010324d:	c1 ea 04             	shr    $0x4,%edx
80103250:	8d 14 92             	lea    (%edx,%edx,4),%edx
80103253:	8d 04 50             	lea    (%eax,%edx,2),%eax
80103256:	89 45 b8             	mov    %eax,-0x48(%ebp)
    CONV(minute);
80103259:	8b 45 bc             	mov    -0x44(%ebp),%eax
8010325c:	89 c2                	mov    %eax,%edx
8010325e:	83 e0 0f             	and    $0xf,%eax
80103261:	c1 ea 04             	shr    $0x4,%edx
80103264:	8d 14 92             	lea    (%edx,%edx,4),%edx
80103267:	8d 04 50             	lea    (%eax,%edx,2),%eax
8010326a:	89 45 bc             	mov    %eax,-0x44(%ebp)
    CONV(hour  );
8010326d:	8b 45 c0             	mov    -0x40(%ebp),%eax
80103270:	89 c2                	mov    %eax,%edx
80103272:	83 e0 0f             	and    $0xf,%eax
80103275:	c1 ea 04             	shr    $0x4,%edx
80103278:	8d 14 92             	lea    (%edx,%edx,4),%edx
8010327b:	8d 04 50             	lea    (%eax,%edx,2),%eax
8010327e:	89 45 c0             	mov    %eax,-0x40(%ebp)
    CONV(day   );
80103281:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80103284:	89 c2                	mov    %eax,%edx
80103286:	83 e0 0f             	and    $0xf,%eax
80103289:	c1 ea 04             	shr    $0x4,%edx
8010328c:	8d 14 92             	lea    (%edx,%edx,4),%edx
8010328f:	8d 04 50             	lea    (%eax,%edx,2),%eax
80103292:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    CONV(month );
80103295:	8b 45 c8             	mov    -0x38(%ebp),%eax
80103298:	89 c2                	mov    %eax,%edx
8010329a:	83 e0 0f             	and    $0xf,%eax
8010329d:	c1 ea 04             	shr    $0x4,%edx
801032a0:	8d 14 92             	lea    (%edx,%edx,4),%edx
801032a3:	8d 04 50             	lea    (%eax,%edx,2),%eax
801032a6:	89 45 c8             	mov    %eax,-0x38(%ebp)
    CONV(year  );
801032a9:	8b 45 cc             	mov    -0x34(%ebp),%eax
801032ac:	89 c2                	mov    %eax,%edx
801032ae:	83 e0 0f             	and    $0xf,%eax
801032b1:	c1 ea 04             	shr    $0x4,%edx
801032b4:	8d 14 92             	lea    (%edx,%edx,4),%edx
801032b7:	8d 04 50             	lea    (%eax,%edx,2),%eax
801032ba:	89 45 cc             	mov    %eax,-0x34(%ebp)
#undef     CONV
  }

  *r = t1;
801032bd:	8b 75 08             	mov    0x8(%ebp),%esi
801032c0:	8b 45 b8             	mov    -0x48(%ebp),%eax
801032c3:	89 06                	mov    %eax,(%esi)
801032c5:	8b 45 bc             	mov    -0x44(%ebp),%eax
801032c8:	89 46 04             	mov    %eax,0x4(%esi)
801032cb:	8b 45 c0             	mov    -0x40(%ebp),%eax
801032ce:	89 46 08             	mov    %eax,0x8(%esi)
801032d1:	8b 45 c4             	mov    -0x3c(%ebp),%eax
801032d4:	89 46 0c             	mov    %eax,0xc(%esi)
801032d7:	8b 45 c8             	mov    -0x38(%ebp),%eax
801032da:	89 46 10             	mov    %eax,0x10(%esi)
801032dd:	8b 45 cc             	mov    -0x34(%ebp),%eax
801032e0:	89 46 14             	mov    %eax,0x14(%esi)
  r->year += 2000;
801032e3:	81 46 14 d0 07 00 00 	addl   $0x7d0,0x14(%esi)
}
801032ea:	8d 65 f4             	lea    -0xc(%ebp),%esp
801032ed:	5b                   	pop    %ebx
801032ee:	5e                   	pop    %esi
801032ef:	5f                   	pop    %edi
801032f0:	5d                   	pop    %ebp
801032f1:	c3                   	ret    
801032f2:	66 90                	xchg   %ax,%ax
801032f4:	66 90                	xchg   %ax,%ax
801032f6:	66 90                	xchg   %ax,%ax
801032f8:	66 90                	xchg   %ax,%ax
801032fa:	66 90                	xchg   %ax,%ax
801032fc:	66 90                	xchg   %ax,%ax
801032fe:	66 90                	xchg   %ax,%ax

80103300 <install_trans>:
static void
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103300:	8b 0d e8 26 11 80    	mov    0x801126e8,%ecx
80103306:	85 c9                	test   %ecx,%ecx
80103308:	0f 8e 8a 00 00 00    	jle    80103398 <install_trans+0x98>
{
8010330e:	55                   	push   %ebp
8010330f:	89 e5                	mov    %esp,%ebp
80103311:	57                   	push   %edi
  for (tail = 0; tail < log.lh.n; tail++) {
80103312:	31 ff                	xor    %edi,%edi
{
80103314:	56                   	push   %esi
80103315:	53                   	push   %ebx
80103316:	83 ec 0c             	sub    $0xc,%esp
80103319:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
80103320:	a1 d4 26 11 80       	mov    0x801126d4,%eax
80103325:	83 ec 08             	sub    $0x8,%esp
80103328:	01 f8                	add    %edi,%eax
8010332a:	83 c0 01             	add    $0x1,%eax
8010332d:	50                   	push   %eax
8010332e:	ff 35 e4 26 11 80    	push   0x801126e4
80103334:	e8 97 cd ff ff       	call   801000d0 <bread>
80103339:	89 c6                	mov    %eax,%esi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
8010333b:	58                   	pop    %eax
8010333c:	5a                   	pop    %edx
8010333d:	ff 34 bd ec 26 11 80 	push   -0x7feed914(,%edi,4)
80103344:	ff 35 e4 26 11 80    	push   0x801126e4
  for (tail = 0; tail < log.lh.n; tail++) {
8010334a:	83 c7 01             	add    $0x1,%edi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
8010334d:	e8 7e cd ff ff       	call   801000d0 <bread>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80103352:	83 c4 0c             	add    $0xc,%esp
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80103355:	89 c3                	mov    %eax,%ebx
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80103357:	8d 46 5c             	lea    0x5c(%esi),%eax
8010335a:	68 00 02 00 00       	push   $0x200
8010335f:	50                   	push   %eax
80103360:	8d 43 5c             	lea    0x5c(%ebx),%eax
80103363:	50                   	push   %eax
80103364:	e8 67 1f 00 00       	call   801052d0 <memmove>
    bwrite(dbuf);  // write dst to disk
80103369:	89 1c 24             	mov    %ebx,(%esp)
8010336c:	e8 3f ce ff ff       	call   801001b0 <bwrite>
    brelse(lbuf);
80103371:	89 34 24             	mov    %esi,(%esp)
80103374:	e8 77 ce ff ff       	call   801001f0 <brelse>
    brelse(dbuf);
80103379:	89 1c 24             	mov    %ebx,(%esp)
8010337c:	e8 6f ce ff ff       	call   801001f0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80103381:	83 c4 10             	add    $0x10,%esp
80103384:	39 3d e8 26 11 80    	cmp    %edi,0x801126e8
8010338a:	7f 94                	jg     80103320 <install_trans+0x20>
  }
}
8010338c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010338f:	5b                   	pop    %ebx
80103390:	5e                   	pop    %esi
80103391:	5f                   	pop    %edi
80103392:	5d                   	pop    %ebp
80103393:	c3                   	ret    
80103394:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103398:	c3                   	ret    
80103399:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801033a0 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
801033a0:	55                   	push   %ebp
801033a1:	89 e5                	mov    %esp,%ebp
801033a3:	53                   	push   %ebx
801033a4:	83 ec 0c             	sub    $0xc,%esp
  struct buf *buf = bread(log.dev, log.start);
801033a7:	ff 35 d4 26 11 80    	push   0x801126d4
801033ad:	ff 35 e4 26 11 80    	push   0x801126e4
801033b3:	e8 18 cd ff ff       	call   801000d0 <bread>
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
801033b8:	8b 0d e8 26 11 80    	mov    0x801126e8,%ecx
  for (i = 0; i < log.lh.n; i++) {
801033be:	83 c4 10             	add    $0x10,%esp
  struct buf *buf = bread(log.dev, log.start);
801033c1:	89 c3                	mov    %eax,%ebx
  hb->n = log.lh.n;
801033c3:	89 48 5c             	mov    %ecx,0x5c(%eax)
  for (i = 0; i < log.lh.n; i++) {
801033c6:	85 c9                	test   %ecx,%ecx
801033c8:	7e 18                	jle    801033e2 <write_head+0x42>
801033ca:	31 c0                	xor    %eax,%eax
801033cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    hb->block[i] = log.lh.block[i];
801033d0:	8b 14 85 ec 26 11 80 	mov    -0x7feed914(,%eax,4),%edx
801033d7:	89 54 83 60          	mov    %edx,0x60(%ebx,%eax,4)
  for (i = 0; i < log.lh.n; i++) {
801033db:	83 c0 01             	add    $0x1,%eax
801033de:	39 c1                	cmp    %eax,%ecx
801033e0:	75 ee                	jne    801033d0 <write_head+0x30>
  }
  bwrite(buf);
801033e2:	83 ec 0c             	sub    $0xc,%esp
801033e5:	53                   	push   %ebx
801033e6:	e8 c5 cd ff ff       	call   801001b0 <bwrite>
  brelse(buf);
801033eb:	89 1c 24             	mov    %ebx,(%esp)
801033ee:	e8 fd cd ff ff       	call   801001f0 <brelse>
}
801033f3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801033f6:	83 c4 10             	add    $0x10,%esp
801033f9:	c9                   	leave  
801033fa:	c3                   	ret    
801033fb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801033ff:	90                   	nop

80103400 <initlog>:
{
80103400:	f3 0f 1e fb          	endbr32 
80103404:	55                   	push   %ebp
80103405:	89 e5                	mov    %esp,%ebp
80103407:	53                   	push   %ebx
80103408:	83 ec 2c             	sub    $0x2c,%esp
8010340b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&log.lock, "log");
8010340e:	68 40 84 10 80       	push   $0x80108440
80103413:	68 a0 26 11 80       	push   $0x801126a0
80103418:	e8 43 1b 00 00       	call   80104f60 <initlock>
  readsb(dev, &sb);
8010341d:	58                   	pop    %eax
8010341e:	8d 45 dc             	lea    -0x24(%ebp),%eax
80103421:	5a                   	pop    %edx
80103422:	50                   	push   %eax
80103423:	53                   	push   %ebx
80103424:	e8 c7 e7 ff ff       	call   80101bf0 <readsb>
  log.start = sb.logstart;
80103429:	8b 45 ec             	mov    -0x14(%ebp),%eax
  struct buf *buf = bread(log.dev, log.start);
8010342c:	59                   	pop    %ecx
  log.dev = dev;
8010342d:	89 1d e4 26 11 80    	mov    %ebx,0x801126e4
  log.size = sb.nlog;
80103433:	8b 55 e8             	mov    -0x18(%ebp),%edx
  log.start = sb.logstart;
80103436:	a3 d4 26 11 80       	mov    %eax,0x801126d4
  log.size = sb.nlog;
8010343b:	89 15 d8 26 11 80    	mov    %edx,0x801126d8
  struct buf *buf = bread(log.dev, log.start);
80103441:	5a                   	pop    %edx
80103442:	50                   	push   %eax
80103443:	53                   	push   %ebx
80103444:	e8 87 cc ff ff       	call   801000d0 <bread>
  for (i = 0; i < log.lh.n; i++) {
80103449:	83 c4 10             	add    $0x10,%esp
  log.lh.n = lh->n;
8010344c:	8b 58 5c             	mov    0x5c(%eax),%ebx
  struct buf *buf = bread(log.dev, log.start);
8010344f:	89 c1                	mov    %eax,%ecx
  log.lh.n = lh->n;
80103451:	89 1d e8 26 11 80    	mov    %ebx,0x801126e8
  for (i = 0; i < log.lh.n; i++) {
80103457:	85 db                	test   %ebx,%ebx
80103459:	7e 17                	jle    80103472 <initlog+0x72>
8010345b:	31 c0                	xor    %eax,%eax
8010345d:	8d 76 00             	lea    0x0(%esi),%esi
    log.lh.block[i] = lh->block[i];
80103460:	8b 54 81 60          	mov    0x60(%ecx,%eax,4),%edx
80103464:	89 14 85 ec 26 11 80 	mov    %edx,-0x7feed914(,%eax,4)
  for (i = 0; i < log.lh.n; i++) {
8010346b:	83 c0 01             	add    $0x1,%eax
8010346e:	39 c3                	cmp    %eax,%ebx
80103470:	75 ee                	jne    80103460 <initlog+0x60>
  brelse(buf);
80103472:	83 ec 0c             	sub    $0xc,%esp
80103475:	51                   	push   %ecx
80103476:	e8 75 cd ff ff       	call   801001f0 <brelse>

static void
recover_from_log(void)
{
  read_head();
  install_trans(); // if committed, copy from log to disk
8010347b:	e8 80 fe ff ff       	call   80103300 <install_trans>
  log.lh.n = 0;
80103480:	c7 05 e8 26 11 80 00 	movl   $0x0,0x801126e8
80103487:	00 00 00 
  write_head(); // clear the log
8010348a:	e8 11 ff ff ff       	call   801033a0 <write_head>
}
8010348f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103492:	83 c4 10             	add    $0x10,%esp
80103495:	c9                   	leave  
80103496:	c3                   	ret    
80103497:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010349e:	66 90                	xchg   %ax,%ax

801034a0 <begin_op>:
}

// called at the start of each FS system call.
void
begin_op(void)
{
801034a0:	f3 0f 1e fb          	endbr32 
801034a4:	55                   	push   %ebp
801034a5:	89 e5                	mov    %esp,%ebp
801034a7:	83 ec 14             	sub    $0x14,%esp
  acquire(&log.lock);
801034aa:	68 a0 26 11 80       	push   $0x801126a0
801034af:	e8 ac 1c 00 00       	call   80105160 <acquire>
801034b4:	83 c4 10             	add    $0x10,%esp
801034b7:	eb 1c                	jmp    801034d5 <begin_op+0x35>
801034b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
801034c0:	83 ec 08             	sub    $0x8,%esp
801034c3:	68 a0 26 11 80       	push   $0x801126a0
801034c8:	68 a0 26 11 80       	push   $0x801126a0
801034cd:	e8 8e 15 00 00       	call   80104a60 <sleep>
801034d2:	83 c4 10             	add    $0x10,%esp
    if(log.committing){
801034d5:	a1 e0 26 11 80       	mov    0x801126e0,%eax
801034da:	85 c0                	test   %eax,%eax
801034dc:	75 e2                	jne    801034c0 <begin_op+0x20>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
801034de:	a1 dc 26 11 80       	mov    0x801126dc,%eax
801034e3:	8b 15 e8 26 11 80    	mov    0x801126e8,%edx
801034e9:	83 c0 01             	add    $0x1,%eax
801034ec:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
801034ef:	8d 14 4a             	lea    (%edx,%ecx,2),%edx
801034f2:	83 fa 1e             	cmp    $0x1e,%edx
801034f5:	7f c9                	jg     801034c0 <begin_op+0x20>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
      release(&log.lock);
801034f7:	83 ec 0c             	sub    $0xc,%esp
      log.outstanding += 1;
801034fa:	a3 dc 26 11 80       	mov    %eax,0x801126dc
      release(&log.lock);
801034ff:	68 a0 26 11 80       	push   $0x801126a0
80103504:	e8 e7 1b 00 00       	call   801050f0 <release>
      break;
    }
  }
}
80103509:	83 c4 10             	add    $0x10,%esp
8010350c:	c9                   	leave  
8010350d:	c3                   	ret    
8010350e:	66 90                	xchg   %ax,%ax

80103510 <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80103510:	f3 0f 1e fb          	endbr32 
80103514:	55                   	push   %ebp
80103515:	89 e5                	mov    %esp,%ebp
80103517:	57                   	push   %edi
80103518:	56                   	push   %esi
80103519:	53                   	push   %ebx
8010351a:	83 ec 18             	sub    $0x18,%esp
  int do_commit = 0;

  acquire(&log.lock);
8010351d:	68 a0 26 11 80       	push   $0x801126a0
80103522:	e8 39 1c 00 00       	call   80105160 <acquire>
  log.outstanding -= 1;
80103527:	a1 dc 26 11 80       	mov    0x801126dc,%eax
  if(log.committing)
8010352c:	8b 35 e0 26 11 80    	mov    0x801126e0,%esi
80103532:	83 c4 10             	add    $0x10,%esp
  log.outstanding -= 1;
80103535:	8d 58 ff             	lea    -0x1(%eax),%ebx
80103538:	89 1d dc 26 11 80    	mov    %ebx,0x801126dc
  if(log.committing)
8010353e:	85 f6                	test   %esi,%esi
80103540:	0f 85 1e 01 00 00    	jne    80103664 <end_op+0x154>
    panic("log.committing");
  if(log.outstanding == 0){
80103546:	85 db                	test   %ebx,%ebx
80103548:	0f 85 f2 00 00 00    	jne    80103640 <end_op+0x130>
    do_commit = 1;
    log.committing = 1;
8010354e:	c7 05 e0 26 11 80 01 	movl   $0x1,0x801126e0
80103555:	00 00 00 
    // begin_op() may be waiting for log space,
    // and decrementing log.outstanding has decreased
    // the amount of reserved space.
    wakeup(&log);
  }
  release(&log.lock);
80103558:	83 ec 0c             	sub    $0xc,%esp
8010355b:	68 a0 26 11 80       	push   $0x801126a0
80103560:	e8 8b 1b 00 00       	call   801050f0 <release>
}

static void
commit()
{
  if (log.lh.n > 0) {
80103565:	8b 0d e8 26 11 80    	mov    0x801126e8,%ecx
8010356b:	83 c4 10             	add    $0x10,%esp
8010356e:	85 c9                	test   %ecx,%ecx
80103570:	7f 3e                	jg     801035b0 <end_op+0xa0>
    acquire(&log.lock);
80103572:	83 ec 0c             	sub    $0xc,%esp
80103575:	68 a0 26 11 80       	push   $0x801126a0
8010357a:	e8 e1 1b 00 00       	call   80105160 <acquire>
    wakeup(&log);
8010357f:	c7 04 24 a0 26 11 80 	movl   $0x801126a0,(%esp)
    log.committing = 0;
80103586:	c7 05 e0 26 11 80 00 	movl   $0x0,0x801126e0
8010358d:	00 00 00 
    wakeup(&log);
80103590:	e8 8b 15 00 00       	call   80104b20 <wakeup>
    release(&log.lock);
80103595:	c7 04 24 a0 26 11 80 	movl   $0x801126a0,(%esp)
8010359c:	e8 4f 1b 00 00       	call   801050f0 <release>
801035a1:	83 c4 10             	add    $0x10,%esp
}
801035a4:	8d 65 f4             	lea    -0xc(%ebp),%esp
801035a7:	5b                   	pop    %ebx
801035a8:	5e                   	pop    %esi
801035a9:	5f                   	pop    %edi
801035aa:	5d                   	pop    %ebp
801035ab:	c3                   	ret    
801035ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
801035b0:	a1 d4 26 11 80       	mov    0x801126d4,%eax
801035b5:	83 ec 08             	sub    $0x8,%esp
801035b8:	01 d8                	add    %ebx,%eax
801035ba:	83 c0 01             	add    $0x1,%eax
801035bd:	50                   	push   %eax
801035be:	ff 35 e4 26 11 80    	push   0x801126e4
801035c4:	e8 07 cb ff ff       	call   801000d0 <bread>
801035c9:	89 c6                	mov    %eax,%esi
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
801035cb:	58                   	pop    %eax
801035cc:	5a                   	pop    %edx
801035cd:	ff 34 9d ec 26 11 80 	push   -0x7feed914(,%ebx,4)
801035d4:	ff 35 e4 26 11 80    	push   0x801126e4
  for (tail = 0; tail < log.lh.n; tail++) {
801035da:	83 c3 01             	add    $0x1,%ebx
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
801035dd:	e8 ee ca ff ff       	call   801000d0 <bread>
    memmove(to->data, from->data, BSIZE);
801035e2:	83 c4 0c             	add    $0xc,%esp
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
801035e5:	89 c7                	mov    %eax,%edi
    memmove(to->data, from->data, BSIZE);
801035e7:	8d 40 5c             	lea    0x5c(%eax),%eax
801035ea:	68 00 02 00 00       	push   $0x200
801035ef:	50                   	push   %eax
801035f0:	8d 46 5c             	lea    0x5c(%esi),%eax
801035f3:	50                   	push   %eax
801035f4:	e8 d7 1c 00 00       	call   801052d0 <memmove>
    bwrite(to);  // write the log
801035f9:	89 34 24             	mov    %esi,(%esp)
801035fc:	e8 af cb ff ff       	call   801001b0 <bwrite>
    brelse(from);
80103601:	89 3c 24             	mov    %edi,(%esp)
80103604:	e8 e7 cb ff ff       	call   801001f0 <brelse>
    brelse(to);
80103609:	89 34 24             	mov    %esi,(%esp)
8010360c:	e8 df cb ff ff       	call   801001f0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80103611:	83 c4 10             	add    $0x10,%esp
80103614:	3b 1d e8 26 11 80    	cmp    0x801126e8,%ebx
8010361a:	7c 94                	jl     801035b0 <end_op+0xa0>
    write_log();     // Write modified blocks from cache to log
    write_head();    // Write header to disk -- the real commit
8010361c:	e8 7f fd ff ff       	call   801033a0 <write_head>
    install_trans(); // Now install writes to home locations
80103621:	e8 da fc ff ff       	call   80103300 <install_trans>
    log.lh.n = 0;
80103626:	c7 05 e8 26 11 80 00 	movl   $0x0,0x801126e8
8010362d:	00 00 00 
    write_head();    // Erase the transaction from the log
80103630:	e8 6b fd ff ff       	call   801033a0 <write_head>
80103635:	e9 38 ff ff ff       	jmp    80103572 <end_op+0x62>
8010363a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    wakeup(&log);
80103640:	83 ec 0c             	sub    $0xc,%esp
80103643:	68 a0 26 11 80       	push   $0x801126a0
80103648:	e8 d3 14 00 00       	call   80104b20 <wakeup>
  release(&log.lock);
8010364d:	c7 04 24 a0 26 11 80 	movl   $0x801126a0,(%esp)
80103654:	e8 97 1a 00 00       	call   801050f0 <release>
80103659:	83 c4 10             	add    $0x10,%esp
}
8010365c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010365f:	5b                   	pop    %ebx
80103660:	5e                   	pop    %esi
80103661:	5f                   	pop    %edi
80103662:	5d                   	pop    %ebp
80103663:	c3                   	ret    
    panic("log.committing");
80103664:	83 ec 0c             	sub    $0xc,%esp
80103667:	68 44 84 10 80       	push   $0x80108444
8010366c:	e8 1f cd ff ff       	call   80100390 <panic>
80103671:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103678:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010367f:	90                   	nop

80103680 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103680:	f3 0f 1e fb          	endbr32 
80103684:	55                   	push   %ebp
80103685:	89 e5                	mov    %esp,%ebp
80103687:	53                   	push   %ebx
80103688:	83 ec 04             	sub    $0x4,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
8010368b:	8b 15 e8 26 11 80    	mov    0x801126e8,%edx
{
80103691:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103694:	83 fa 1d             	cmp    $0x1d,%edx
80103697:	0f 8f 91 00 00 00    	jg     8010372e <log_write+0xae>
8010369d:	a1 d8 26 11 80       	mov    0x801126d8,%eax
801036a2:	83 e8 01             	sub    $0x1,%eax
801036a5:	39 c2                	cmp    %eax,%edx
801036a7:	0f 8d 81 00 00 00    	jge    8010372e <log_write+0xae>
    panic("too big a transaction");
  if (log.outstanding < 1)
801036ad:	a1 dc 26 11 80       	mov    0x801126dc,%eax
801036b2:	85 c0                	test   %eax,%eax
801036b4:	0f 8e 81 00 00 00    	jle    8010373b <log_write+0xbb>
    panic("log_write outside of trans");

  acquire(&log.lock);
801036ba:	83 ec 0c             	sub    $0xc,%esp
801036bd:	68 a0 26 11 80       	push   $0x801126a0
801036c2:	e8 99 1a 00 00       	call   80105160 <acquire>
  for (i = 0; i < log.lh.n; i++) {
801036c7:	8b 15 e8 26 11 80    	mov    0x801126e8,%edx
801036cd:	83 c4 10             	add    $0x10,%esp
801036d0:	85 d2                	test   %edx,%edx
801036d2:	7e 4e                	jle    80103722 <log_write+0xa2>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
801036d4:	8b 4b 08             	mov    0x8(%ebx),%ecx
  for (i = 0; i < log.lh.n; i++) {
801036d7:	31 c0                	xor    %eax,%eax
801036d9:	eb 0c                	jmp    801036e7 <log_write+0x67>
801036db:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801036df:	90                   	nop
801036e0:	83 c0 01             	add    $0x1,%eax
801036e3:	39 c2                	cmp    %eax,%edx
801036e5:	74 29                	je     80103710 <log_write+0x90>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
801036e7:	39 0c 85 ec 26 11 80 	cmp    %ecx,-0x7feed914(,%eax,4)
801036ee:	75 f0                	jne    801036e0 <log_write+0x60>
      break;
  }
  log.lh.block[i] = b->blockno;
801036f0:	89 0c 85 ec 26 11 80 	mov    %ecx,-0x7feed914(,%eax,4)
  if (i == log.lh.n)
    log.lh.n++;
  b->flags |= B_DIRTY; // prevent eviction
801036f7:	83 0b 04             	orl    $0x4,(%ebx)
  release(&log.lock);
}
801036fa:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  release(&log.lock);
801036fd:	c7 45 08 a0 26 11 80 	movl   $0x801126a0,0x8(%ebp)
}
80103704:	c9                   	leave  
  release(&log.lock);
80103705:	e9 e6 19 00 00       	jmp    801050f0 <release>
8010370a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  log.lh.block[i] = b->blockno;
80103710:	89 0c 95 ec 26 11 80 	mov    %ecx,-0x7feed914(,%edx,4)
    log.lh.n++;
80103717:	83 c2 01             	add    $0x1,%edx
8010371a:	89 15 e8 26 11 80    	mov    %edx,0x801126e8
80103720:	eb d5                	jmp    801036f7 <log_write+0x77>
  log.lh.block[i] = b->blockno;
80103722:	8b 43 08             	mov    0x8(%ebx),%eax
80103725:	a3 ec 26 11 80       	mov    %eax,0x801126ec
  if (i == log.lh.n)
8010372a:	75 cb                	jne    801036f7 <log_write+0x77>
8010372c:	eb e9                	jmp    80103717 <log_write+0x97>
    panic("too big a transaction");
8010372e:	83 ec 0c             	sub    $0xc,%esp
80103731:	68 53 84 10 80       	push   $0x80108453
80103736:	e8 55 cc ff ff       	call   80100390 <panic>
    panic("log_write outside of trans");
8010373b:	83 ec 0c             	sub    $0xc,%esp
8010373e:	68 69 84 10 80       	push   $0x80108469
80103743:	e8 48 cc ff ff       	call   80100390 <panic>
80103748:	66 90                	xchg   %ax,%ax
8010374a:	66 90                	xchg   %ax,%ax
8010374c:	66 90                	xchg   %ax,%ax
8010374e:	66 90                	xchg   %ax,%ax

80103750 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80103750:	55                   	push   %ebp
80103751:	89 e5                	mov    %esp,%ebp
80103753:	53                   	push   %ebx
80103754:	83 ec 04             	sub    $0x4,%esp
  cprintf("cpu%d: starting %d\n", cpuid(), cpuid());
80103757:	e8 94 09 00 00       	call   801040f0 <cpuid>
8010375c:	89 c3                	mov    %eax,%ebx
8010375e:	e8 8d 09 00 00       	call   801040f0 <cpuid>
80103763:	83 ec 04             	sub    $0x4,%esp
80103766:	53                   	push   %ebx
80103767:	50                   	push   %eax
80103768:	68 84 84 10 80       	push   $0x80108484
8010376d:	e8 be cf ff ff       	call   80100730 <cprintf>
  idtinit();       // load idt register
80103772:	e8 d9 2e 00 00       	call   80106650 <idtinit>
  xchg(&(mycpu()->started), 1); // tell startothers() we're up
80103777:	e8 04 09 00 00       	call   80104080 <mycpu>
8010377c:	89 c2                	mov    %eax,%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
8010377e:	b8 01 00 00 00       	mov    $0x1,%eax
80103783:	f0 87 82 a0 00 00 00 	lock xchg %eax,0xa0(%edx)
  scheduler();     // start running processes
8010378a:	e8 a1 0e 00 00       	call   80104630 <scheduler>
8010378f:	90                   	nop

80103790 <mpenter>:
{
80103790:	f3 0f 1e fb          	endbr32 
80103794:	55                   	push   %ebp
80103795:	89 e5                	mov    %esp,%ebp
80103797:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
8010379a:	e8 c1 3f 00 00       	call   80107760 <switchkvm>
  seginit();
8010379f:	e8 2c 3f 00 00       	call   801076d0 <seginit>
  lapicinit();
801037a4:	e8 67 f7 ff ff       	call   80102f10 <lapicinit>
  mpmain();
801037a9:	e8 a2 ff ff ff       	call   80103750 <mpmain>
801037ae:	66 90                	xchg   %ax,%ax

801037b0 <main>:
{
801037b0:	f3 0f 1e fb          	endbr32 
801037b4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
801037b8:	83 e4 f0             	and    $0xfffffff0,%esp
801037bb:	ff 71 fc             	push   -0x4(%ecx)
801037be:	55                   	push   %ebp
801037bf:	89 e5                	mov    %esp,%ebp
801037c1:	53                   	push   %ebx
801037c2:	51                   	push   %ecx
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
801037c3:	83 ec 08             	sub    $0x8,%esp
801037c6:	68 00 00 40 80       	push   $0x80400000
801037cb:	68 d0 6a 11 80       	push   $0x80116ad0
801037d0:	e8 5b f5 ff ff       	call   80102d30 <kinit1>
  kvmalloc();      // kernel page table
801037d5:	e8 86 44 00 00       	call   80107c60 <kvmalloc>
  mpinit();        // detect other processors
801037da:	e8 81 01 00 00       	call   80103960 <mpinit>
  lapicinit();     // interrupt controller
801037df:	e8 2c f7 ff ff       	call   80102f10 <lapicinit>
  seginit();       // segment descriptors
801037e4:	e8 e7 3e 00 00       	call   801076d0 <seginit>
  picinit();       // disable pic
801037e9:	e8 82 03 00 00       	call   80103b70 <picinit>
  ioapicinit();    // another interrupt controller
801037ee:	e8 fd f2 ff ff       	call   80102af0 <ioapicinit>
  consoleinit();   // console hardware
801037f3:	e8 f8 d8 ff ff       	call   801010f0 <consoleinit>
  uartinit();      // serial port
801037f8:	e8 43 31 00 00       	call   80106940 <uartinit>
  pinit();         // process table
801037fd:	e8 5e 08 00 00       	call   80104060 <pinit>
  tvinit();        // trap vectors
80103802:	e8 c9 2d 00 00       	call   801065d0 <tvinit>
  binit();         // buffer cache
80103807:	e8 34 c8 ff ff       	call   80100040 <binit>
  fileinit();      // file table
8010380c:	e8 8f dc ff ff       	call   801014a0 <fileinit>
  ideinit();       // disk 
80103811:	e8 aa f0 ff ff       	call   801028c0 <ideinit>

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103816:	83 c4 0c             	add    $0xc,%esp
80103819:	68 8a 00 00 00       	push   $0x8a
8010381e:	68 8c b4 10 80       	push   $0x8010b48c
80103823:	68 00 70 00 80       	push   $0x80007000
80103828:	e8 a3 1a 00 00       	call   801052d0 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
8010382d:	83 c4 10             	add    $0x10,%esp
80103830:	69 05 84 27 11 80 b0 	imul   $0xb0,0x80112784,%eax
80103837:	00 00 00 
8010383a:	05 a0 27 11 80       	add    $0x801127a0,%eax
8010383f:	3d a0 27 11 80       	cmp    $0x801127a0,%eax
80103844:	76 7a                	jbe    801038c0 <main+0x110>
80103846:	bb a0 27 11 80       	mov    $0x801127a0,%ebx
8010384b:	eb 1c                	jmp    80103869 <main+0xb9>
8010384d:	8d 76 00             	lea    0x0(%esi),%esi
80103850:	69 05 84 27 11 80 b0 	imul   $0xb0,0x80112784,%eax
80103857:	00 00 00 
8010385a:	81 c3 b0 00 00 00    	add    $0xb0,%ebx
80103860:	05 a0 27 11 80       	add    $0x801127a0,%eax
80103865:	39 c3                	cmp    %eax,%ebx
80103867:	73 57                	jae    801038c0 <main+0x110>
    if(c == mycpu())  // We've started already.
80103869:	e8 12 08 00 00       	call   80104080 <mycpu>
8010386e:	39 c3                	cmp    %eax,%ebx
80103870:	74 de                	je     80103850 <main+0xa0>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
80103872:	e8 29 f5 ff ff       	call   80102da0 <kalloc>
    *(void**)(code-4) = stack + KSTACKSIZE;
    *(void(**)(void))(code-8) = mpenter;
    *(int**)(code-12) = (void *) V2P(entrypgdir);

    lapicstartap(c->apicid, V2P(code));
80103877:	83 ec 08             	sub    $0x8,%esp
    *(void(**)(void))(code-8) = mpenter;
8010387a:	c7 05 f8 6f 00 80 90 	movl   $0x80103790,0x80006ff8
80103881:	37 10 80 
    *(int**)(code-12) = (void *) V2P(entrypgdir);
80103884:	c7 05 f4 6f 00 80 00 	movl   $0x10a000,0x80006ff4
8010388b:	a0 10 00 
    *(void**)(code-4) = stack + KSTACKSIZE;
8010388e:	05 00 10 00 00       	add    $0x1000,%eax
80103893:	a3 fc 6f 00 80       	mov    %eax,0x80006ffc
    lapicstartap(c->apicid, V2P(code));
80103898:	0f b6 03             	movzbl (%ebx),%eax
8010389b:	68 00 70 00 00       	push   $0x7000
801038a0:	50                   	push   %eax
801038a1:	e8 ba f7 ff ff       	call   80103060 <lapicstartap>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
801038a6:	83 c4 10             	add    $0x10,%esp
801038a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801038b0:	8b 83 a0 00 00 00    	mov    0xa0(%ebx),%eax
801038b6:	85 c0                	test   %eax,%eax
801038b8:	74 f6                	je     801038b0 <main+0x100>
801038ba:	eb 94                	jmp    80103850 <main+0xa0>
801038bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
801038c0:	83 ec 08             	sub    $0x8,%esp
801038c3:	68 00 00 00 8e       	push   $0x8e000000
801038c8:	68 00 00 40 80       	push   $0x80400000
801038cd:	e8 fe f3 ff ff       	call   80102cd0 <kinit2>
  userinit();      // first user process
801038d2:	e8 69 08 00 00       	call   80104140 <userinit>
  mpmain();        // finish this processor's setup
801038d7:	e8 74 fe ff ff       	call   80103750 <mpmain>
801038dc:	66 90                	xchg   %ax,%ax
801038de:	66 90                	xchg   %ax,%ax

801038e0 <mpsearch1>:
}

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
801038e0:	55                   	push   %ebp
801038e1:	89 e5                	mov    %esp,%ebp
801038e3:	57                   	push   %edi
801038e4:	56                   	push   %esi
  uchar *e, *p, *addr;

  addr = P2V(a);
801038e5:	8d b0 00 00 00 80    	lea    -0x80000000(%eax),%esi
{
801038eb:	53                   	push   %ebx
  e = addr+len;
801038ec:	8d 1c 16             	lea    (%esi,%edx,1),%ebx
{
801038ef:	83 ec 0c             	sub    $0xc,%esp
  for(p = addr; p < e; p += sizeof(struct mp))
801038f2:	39 de                	cmp    %ebx,%esi
801038f4:	72 10                	jb     80103906 <mpsearch1+0x26>
801038f6:	eb 50                	jmp    80103948 <mpsearch1+0x68>
801038f8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801038ff:	90                   	nop
80103900:	89 fe                	mov    %edi,%esi
80103902:	39 fb                	cmp    %edi,%ebx
80103904:	76 42                	jbe    80103948 <mpsearch1+0x68>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103906:	83 ec 04             	sub    $0x4,%esp
80103909:	8d 7e 10             	lea    0x10(%esi),%edi
8010390c:	6a 04                	push   $0x4
8010390e:	68 98 84 10 80       	push   $0x80108498
80103913:	56                   	push   %esi
80103914:	e8 67 19 00 00       	call   80105280 <memcmp>
80103919:	83 c4 10             	add    $0x10,%esp
8010391c:	89 c2                	mov    %eax,%edx
8010391e:	85 c0                	test   %eax,%eax
80103920:	75 de                	jne    80103900 <mpsearch1+0x20>
80103922:	89 f0                	mov    %esi,%eax
80103924:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    sum += addr[i];
80103928:	0f b6 08             	movzbl (%eax),%ecx
  for(i=0; i<len; i++)
8010392b:	83 c0 01             	add    $0x1,%eax
    sum += addr[i];
8010392e:	01 ca                	add    %ecx,%edx
  for(i=0; i<len; i++)
80103930:	39 f8                	cmp    %edi,%eax
80103932:	75 f4                	jne    80103928 <mpsearch1+0x48>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103934:	84 d2                	test   %dl,%dl
80103936:	75 c8                	jne    80103900 <mpsearch1+0x20>
      return (struct mp*)p;
  return 0;
}
80103938:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010393b:	89 f0                	mov    %esi,%eax
8010393d:	5b                   	pop    %ebx
8010393e:	5e                   	pop    %esi
8010393f:	5f                   	pop    %edi
80103940:	5d                   	pop    %ebp
80103941:	c3                   	ret    
80103942:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103948:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010394b:	31 f6                	xor    %esi,%esi
}
8010394d:	5b                   	pop    %ebx
8010394e:	89 f0                	mov    %esi,%eax
80103950:	5e                   	pop    %esi
80103951:	5f                   	pop    %edi
80103952:	5d                   	pop    %ebp
80103953:	c3                   	ret    
80103954:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010395b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010395f:	90                   	nop

80103960 <mpinit>:
  return conf;
}

void
mpinit(void)
{
80103960:	f3 0f 1e fb          	endbr32 
80103964:	55                   	push   %ebp
80103965:	89 e5                	mov    %esp,%ebp
80103967:	57                   	push   %edi
80103968:	56                   	push   %esi
80103969:	53                   	push   %ebx
8010396a:	83 ec 1c             	sub    $0x1c,%esp
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
8010396d:	0f b6 05 0f 04 00 80 	movzbl 0x8000040f,%eax
80103974:	0f b6 15 0e 04 00 80 	movzbl 0x8000040e,%edx
8010397b:	c1 e0 08             	shl    $0x8,%eax
8010397e:	09 d0                	or     %edx,%eax
80103980:	c1 e0 04             	shl    $0x4,%eax
80103983:	75 1b                	jne    801039a0 <mpinit+0x40>
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80103985:	0f b6 05 14 04 00 80 	movzbl 0x80000414,%eax
8010398c:	0f b6 15 13 04 00 80 	movzbl 0x80000413,%edx
80103993:	c1 e0 08             	shl    $0x8,%eax
80103996:	09 d0                	or     %edx,%eax
80103998:	c1 e0 0a             	shl    $0xa,%eax
    if((mp = mpsearch1(p-1024, 1024)))
8010399b:	2d 00 04 00 00       	sub    $0x400,%eax
    if((mp = mpsearch1(p, 1024)))
801039a0:	ba 00 04 00 00       	mov    $0x400,%edx
801039a5:	e8 36 ff ff ff       	call   801038e0 <mpsearch1>
801039aa:	89 c3                	mov    %eax,%ebx
801039ac:	85 c0                	test   %eax,%eax
801039ae:	0f 84 4c 01 00 00    	je     80103b00 <mpinit+0x1a0>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801039b4:	8b 73 04             	mov    0x4(%ebx),%esi
801039b7:	85 f6                	test   %esi,%esi
801039b9:	0f 84 31 01 00 00    	je     80103af0 <mpinit+0x190>
  if(memcmp(conf, "PCMP", 4) != 0)
801039bf:	83 ec 04             	sub    $0x4,%esp
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
801039c2:	8d 86 00 00 00 80    	lea    -0x80000000(%esi),%eax
  if(memcmp(conf, "PCMP", 4) != 0)
801039c8:	6a 04                	push   $0x4
801039ca:	68 9d 84 10 80       	push   $0x8010849d
801039cf:	50                   	push   %eax
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
801039d0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
801039d3:	e8 a8 18 00 00       	call   80105280 <memcmp>
801039d8:	83 c4 10             	add    $0x10,%esp
801039db:	85 c0                	test   %eax,%eax
801039dd:	0f 85 0d 01 00 00    	jne    80103af0 <mpinit+0x190>
  if(conf->version != 1 && conf->version != 4)
801039e3:	0f b6 86 06 00 00 80 	movzbl -0x7ffffffa(%esi),%eax
801039ea:	3c 01                	cmp    $0x1,%al
801039ec:	74 08                	je     801039f6 <mpinit+0x96>
801039ee:	3c 04                	cmp    $0x4,%al
801039f0:	0f 85 fa 00 00 00    	jne    80103af0 <mpinit+0x190>
  if(sum((uchar*)conf, conf->length) != 0)
801039f6:	0f b7 96 04 00 00 80 	movzwl -0x7ffffffc(%esi),%edx
  for(i=0; i<len; i++)
801039fd:	66 85 d2             	test   %dx,%dx
80103a00:	74 26                	je     80103a28 <mpinit+0xc8>
80103a02:	8d 3c 32             	lea    (%edx,%esi,1),%edi
80103a05:	89 f0                	mov    %esi,%eax
  sum = 0;
80103a07:	31 d2                	xor    %edx,%edx
80103a09:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    sum += addr[i];
80103a10:	0f b6 88 00 00 00 80 	movzbl -0x80000000(%eax),%ecx
  for(i=0; i<len; i++)
80103a17:	83 c0 01             	add    $0x1,%eax
    sum += addr[i];
80103a1a:	01 ca                	add    %ecx,%edx
  for(i=0; i<len; i++)
80103a1c:	39 f8                	cmp    %edi,%eax
80103a1e:	75 f0                	jne    80103a10 <mpinit+0xb0>
  if(sum((uchar*)conf, conf->length) != 0)
80103a20:	84 d2                	test   %dl,%dl
80103a22:	0f 85 c8 00 00 00    	jne    80103af0 <mpinit+0x190>
  struct mpioapic *ioapic;

  if((conf = mpconfig(&mp)) == 0)
    panic("Expect to run on an SMP");
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
80103a28:	8b 86 24 00 00 80    	mov    -0x7fffffdc(%esi),%eax
80103a2e:	a3 80 26 11 80       	mov    %eax,0x80112680
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103a33:	0f b7 96 04 00 00 80 	movzwl -0x7ffffffc(%esi),%edx
80103a3a:	8d 86 2c 00 00 80    	lea    -0x7fffffd4(%esi),%eax
  ismp = 1;
80103a40:	be 01 00 00 00       	mov    $0x1,%esi
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103a45:	03 55 e4             	add    -0x1c(%ebp),%edx
80103a48:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
80103a4b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103a4f:	90                   	nop
80103a50:	39 d0                	cmp    %edx,%eax
80103a52:	73 15                	jae    80103a69 <mpinit+0x109>
    switch(*p){
80103a54:	0f b6 08             	movzbl (%eax),%ecx
80103a57:	80 f9 02             	cmp    $0x2,%cl
80103a5a:	74 54                	je     80103ab0 <mpinit+0x150>
80103a5c:	77 42                	ja     80103aa0 <mpinit+0x140>
80103a5e:	84 c9                	test   %cl,%cl
80103a60:	74 5e                	je     80103ac0 <mpinit+0x160>
      p += sizeof(struct mpioapic);
      continue;
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80103a62:	83 c0 08             	add    $0x8,%eax
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103a65:	39 d0                	cmp    %edx,%eax
80103a67:	72 eb                	jb     80103a54 <mpinit+0xf4>
    default:
      ismp = 0;
      break;
    }
  }
  if(!ismp)
80103a69:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80103a6c:	85 f6                	test   %esi,%esi
80103a6e:	0f 84 e1 00 00 00    	je     80103b55 <mpinit+0x1f5>
    panic("Didn't find a suitable machine");

  if(mp->imcrp){
80103a74:	80 7b 0c 00          	cmpb   $0x0,0xc(%ebx)
80103a78:	74 15                	je     80103a8f <mpinit+0x12f>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103a7a:	b8 70 00 00 00       	mov    $0x70,%eax
80103a7f:	ba 22 00 00 00       	mov    $0x22,%edx
80103a84:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103a85:	ba 23 00 00 00       	mov    $0x23,%edx
80103a8a:	ec                   	in     (%dx),%al
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80103a8b:	83 c8 01             	or     $0x1,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103a8e:	ee                   	out    %al,(%dx)
  }
}
80103a8f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103a92:	5b                   	pop    %ebx
80103a93:	5e                   	pop    %esi
80103a94:	5f                   	pop    %edi
80103a95:	5d                   	pop    %ebp
80103a96:	c3                   	ret    
80103a97:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103a9e:	66 90                	xchg   %ax,%ax
    switch(*p){
80103aa0:	83 e9 03             	sub    $0x3,%ecx
80103aa3:	80 f9 01             	cmp    $0x1,%cl
80103aa6:	76 ba                	jbe    80103a62 <mpinit+0x102>
80103aa8:	31 f6                	xor    %esi,%esi
80103aaa:	eb a4                	jmp    80103a50 <mpinit+0xf0>
80103aac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      ioapicid = ioapic->apicno;
80103ab0:	0f b6 48 01          	movzbl 0x1(%eax),%ecx
      p += sizeof(struct mpioapic);
80103ab4:	83 c0 08             	add    $0x8,%eax
      ioapicid = ioapic->apicno;
80103ab7:	88 0d 80 27 11 80    	mov    %cl,0x80112780
      continue;
80103abd:	eb 91                	jmp    80103a50 <mpinit+0xf0>
80103abf:	90                   	nop
      if(ncpu < NCPU) {
80103ac0:	8b 0d 84 27 11 80    	mov    0x80112784,%ecx
80103ac6:	83 f9 07             	cmp    $0x7,%ecx
80103ac9:	7f 19                	jg     80103ae4 <mpinit+0x184>
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
80103acb:	69 f9 b0 00 00 00    	imul   $0xb0,%ecx,%edi
80103ad1:	0f b6 58 01          	movzbl 0x1(%eax),%ebx
        ncpu++;
80103ad5:	83 c1 01             	add    $0x1,%ecx
80103ad8:	89 0d 84 27 11 80    	mov    %ecx,0x80112784
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
80103ade:	88 9f a0 27 11 80    	mov    %bl,-0x7feed860(%edi)
      p += sizeof(struct mpproc);
80103ae4:	83 c0 14             	add    $0x14,%eax
      continue;
80103ae7:	e9 64 ff ff ff       	jmp    80103a50 <mpinit+0xf0>
80103aec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    panic("Expect to run on an SMP");
80103af0:	83 ec 0c             	sub    $0xc,%esp
80103af3:	68 a2 84 10 80       	push   $0x801084a2
80103af8:	e8 93 c8 ff ff       	call   80100390 <panic>
80103afd:	8d 76 00             	lea    0x0(%esi),%esi
{
80103b00:	bb 00 00 0f 80       	mov    $0x800f0000,%ebx
80103b05:	eb 13                	jmp    80103b1a <mpinit+0x1ba>
80103b07:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103b0e:	66 90                	xchg   %ax,%ax
  for(p = addr; p < e; p += sizeof(struct mp))
80103b10:	89 f3                	mov    %esi,%ebx
80103b12:	81 fe 00 00 10 80    	cmp    $0x80100000,%esi
80103b18:	74 d6                	je     80103af0 <mpinit+0x190>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103b1a:	83 ec 04             	sub    $0x4,%esp
80103b1d:	8d 73 10             	lea    0x10(%ebx),%esi
80103b20:	6a 04                	push   $0x4
80103b22:	68 98 84 10 80       	push   $0x80108498
80103b27:	53                   	push   %ebx
80103b28:	e8 53 17 00 00       	call   80105280 <memcmp>
80103b2d:	83 c4 10             	add    $0x10,%esp
80103b30:	89 c2                	mov    %eax,%edx
80103b32:	85 c0                	test   %eax,%eax
80103b34:	75 da                	jne    80103b10 <mpinit+0x1b0>
80103b36:	89 d8                	mov    %ebx,%eax
80103b38:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103b3f:	90                   	nop
    sum += addr[i];
80103b40:	0f b6 08             	movzbl (%eax),%ecx
  for(i=0; i<len; i++)
80103b43:	83 c0 01             	add    $0x1,%eax
    sum += addr[i];
80103b46:	01 ca                	add    %ecx,%edx
  for(i=0; i<len; i++)
80103b48:	39 f0                	cmp    %esi,%eax
80103b4a:	75 f4                	jne    80103b40 <mpinit+0x1e0>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103b4c:	84 d2                	test   %dl,%dl
80103b4e:	75 c0                	jne    80103b10 <mpinit+0x1b0>
80103b50:	e9 5f fe ff ff       	jmp    801039b4 <mpinit+0x54>
    panic("Didn't find a suitable machine");
80103b55:	83 ec 0c             	sub    $0xc,%esp
80103b58:	68 bc 84 10 80       	push   $0x801084bc
80103b5d:	e8 2e c8 ff ff       	call   80100390 <panic>
80103b62:	66 90                	xchg   %ax,%ax
80103b64:	66 90                	xchg   %ax,%ax
80103b66:	66 90                	xchg   %ax,%ax
80103b68:	66 90                	xchg   %ax,%ax
80103b6a:	66 90                	xchg   %ax,%ax
80103b6c:	66 90                	xchg   %ax,%ax
80103b6e:	66 90                	xchg   %ax,%ax

80103b70 <picinit>:
80103b70:	f3 0f 1e fb          	endbr32 
80103b74:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103b79:	ba 21 00 00 00       	mov    $0x21,%edx
80103b7e:	ee                   	out    %al,(%dx)
80103b7f:	ba a1 00 00 00       	mov    $0xa1,%edx
80103b84:	ee                   	out    %al,(%dx)
80103b85:	c3                   	ret    
80103b86:	66 90                	xchg   %ax,%ax
80103b88:	66 90                	xchg   %ax,%ax
80103b8a:	66 90                	xchg   %ax,%ax
80103b8c:	66 90                	xchg   %ax,%ax
80103b8e:	66 90                	xchg   %ax,%ax

80103b90 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80103b90:	f3 0f 1e fb          	endbr32 
80103b94:	55                   	push   %ebp
80103b95:	89 e5                	mov    %esp,%ebp
80103b97:	57                   	push   %edi
80103b98:	56                   	push   %esi
80103b99:	53                   	push   %ebx
80103b9a:	83 ec 0c             	sub    $0xc,%esp
80103b9d:	8b 5d 08             	mov    0x8(%ebp),%ebx
80103ba0:	8b 75 0c             	mov    0xc(%ebp),%esi
  struct pipe *p;

  p = 0;
  *f0 = *f1 = 0;
80103ba3:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80103ba9:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
80103baf:	e8 0c d9 ff ff       	call   801014c0 <filealloc>
80103bb4:	89 03                	mov    %eax,(%ebx)
80103bb6:	85 c0                	test   %eax,%eax
80103bb8:	0f 84 ac 00 00 00    	je     80103c6a <pipealloc+0xda>
80103bbe:	e8 fd d8 ff ff       	call   801014c0 <filealloc>
80103bc3:	89 06                	mov    %eax,(%esi)
80103bc5:	85 c0                	test   %eax,%eax
80103bc7:	0f 84 8b 00 00 00    	je     80103c58 <pipealloc+0xc8>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80103bcd:	e8 ce f1 ff ff       	call   80102da0 <kalloc>
80103bd2:	89 c7                	mov    %eax,%edi
80103bd4:	85 c0                	test   %eax,%eax
80103bd6:	0f 84 b4 00 00 00    	je     80103c90 <pipealloc+0x100>
    goto bad;
  p->readopen = 1;
80103bdc:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
80103be3:	00 00 00 
  p->writeopen = 1;
  p->nwrite = 0;
  p->nread = 0;
  initlock(&p->lock, "pipe");
80103be6:	83 ec 08             	sub    $0x8,%esp
  p->writeopen = 1;
80103be9:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
80103bf0:	00 00 00 
  p->nwrite = 0;
80103bf3:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
80103bfa:	00 00 00 
  p->nread = 0;
80103bfd:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
80103c04:	00 00 00 
  initlock(&p->lock, "pipe");
80103c07:	68 db 84 10 80       	push   $0x801084db
80103c0c:	50                   	push   %eax
80103c0d:	e8 4e 13 00 00       	call   80104f60 <initlock>
  (*f0)->type = FD_PIPE;
80103c12:	8b 03                	mov    (%ebx),%eax
  (*f0)->pipe = p;
  (*f1)->type = FD_PIPE;
  (*f1)->readable = 0;
  (*f1)->writable = 1;
  (*f1)->pipe = p;
  return 0;
80103c14:	83 c4 10             	add    $0x10,%esp
  (*f0)->type = FD_PIPE;
80103c17:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
80103c1d:	8b 03                	mov    (%ebx),%eax
80103c1f:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
80103c23:	8b 03                	mov    (%ebx),%eax
80103c25:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
80103c29:	8b 03                	mov    (%ebx),%eax
80103c2b:	89 78 0c             	mov    %edi,0xc(%eax)
  (*f1)->type = FD_PIPE;
80103c2e:	8b 06                	mov    (%esi),%eax
80103c30:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
80103c36:	8b 06                	mov    (%esi),%eax
80103c38:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
80103c3c:	8b 06                	mov    (%esi),%eax
80103c3e:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
80103c42:	8b 06                	mov    (%esi),%eax
80103c44:	89 78 0c             	mov    %edi,0xc(%eax)
  if(*f0)
    fileclose(*f0);
  if(*f1)
    fileclose(*f1);
  return -1;
}
80103c47:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80103c4a:	31 c0                	xor    %eax,%eax
}
80103c4c:	5b                   	pop    %ebx
80103c4d:	5e                   	pop    %esi
80103c4e:	5f                   	pop    %edi
80103c4f:	5d                   	pop    %ebp
80103c50:	c3                   	ret    
80103c51:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  if(*f0)
80103c58:	8b 03                	mov    (%ebx),%eax
80103c5a:	85 c0                	test   %eax,%eax
80103c5c:	74 1e                	je     80103c7c <pipealloc+0xec>
    fileclose(*f0);
80103c5e:	83 ec 0c             	sub    $0xc,%esp
80103c61:	50                   	push   %eax
80103c62:	e8 19 d9 ff ff       	call   80101580 <fileclose>
80103c67:	83 c4 10             	add    $0x10,%esp
  if(*f1)
80103c6a:	8b 06                	mov    (%esi),%eax
80103c6c:	85 c0                	test   %eax,%eax
80103c6e:	74 0c                	je     80103c7c <pipealloc+0xec>
    fileclose(*f1);
80103c70:	83 ec 0c             	sub    $0xc,%esp
80103c73:	50                   	push   %eax
80103c74:	e8 07 d9 ff ff       	call   80101580 <fileclose>
80103c79:	83 c4 10             	add    $0x10,%esp
}
80103c7c:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return -1;
80103c7f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80103c84:	5b                   	pop    %ebx
80103c85:	5e                   	pop    %esi
80103c86:	5f                   	pop    %edi
80103c87:	5d                   	pop    %ebp
80103c88:	c3                   	ret    
80103c89:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  if(*f0)
80103c90:	8b 03                	mov    (%ebx),%eax
80103c92:	85 c0                	test   %eax,%eax
80103c94:	75 c8                	jne    80103c5e <pipealloc+0xce>
80103c96:	eb d2                	jmp    80103c6a <pipealloc+0xda>
80103c98:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103c9f:	90                   	nop

80103ca0 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80103ca0:	f3 0f 1e fb          	endbr32 
80103ca4:	55                   	push   %ebp
80103ca5:	89 e5                	mov    %esp,%ebp
80103ca7:	56                   	push   %esi
80103ca8:	53                   	push   %ebx
80103ca9:	8b 5d 08             	mov    0x8(%ebp),%ebx
80103cac:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&p->lock);
80103caf:	83 ec 0c             	sub    $0xc,%esp
80103cb2:	53                   	push   %ebx
80103cb3:	e8 a8 14 00 00       	call   80105160 <acquire>
  if(writable){
80103cb8:	83 c4 10             	add    $0x10,%esp
80103cbb:	85 f6                	test   %esi,%esi
80103cbd:	74 61                	je     80103d20 <pipeclose+0x80>
    p->writeopen = 0;
    wakeup(&p->nread);
80103cbf:	83 ec 0c             	sub    $0xc,%esp
80103cc2:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
    p->writeopen = 0;
80103cc8:	c7 83 40 02 00 00 00 	movl   $0x0,0x240(%ebx)
80103ccf:	00 00 00 
    wakeup(&p->nread);
80103cd2:	50                   	push   %eax
80103cd3:	e8 48 0e 00 00       	call   80104b20 <wakeup>
80103cd8:	83 c4 10             	add    $0x10,%esp
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
80103cdb:	8b 93 3c 02 00 00    	mov    0x23c(%ebx),%edx
80103ce1:	85 d2                	test   %edx,%edx
80103ce3:	75 0a                	jne    80103cef <pipeclose+0x4f>
80103ce5:	8b 83 40 02 00 00    	mov    0x240(%ebx),%eax
80103ceb:	85 c0                	test   %eax,%eax
80103ced:	74 11                	je     80103d00 <pipeclose+0x60>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
80103cef:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
80103cf2:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103cf5:	5b                   	pop    %ebx
80103cf6:	5e                   	pop    %esi
80103cf7:	5d                   	pop    %ebp
    release(&p->lock);
80103cf8:	e9 f3 13 00 00       	jmp    801050f0 <release>
80103cfd:	8d 76 00             	lea    0x0(%esi),%esi
    release(&p->lock);
80103d00:	83 ec 0c             	sub    $0xc,%esp
80103d03:	53                   	push   %ebx
80103d04:	e8 e7 13 00 00       	call   801050f0 <release>
    kfree((char*)p);
80103d09:	89 5d 08             	mov    %ebx,0x8(%ebp)
80103d0c:	83 c4 10             	add    $0x10,%esp
}
80103d0f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103d12:	5b                   	pop    %ebx
80103d13:	5e                   	pop    %esi
80103d14:	5d                   	pop    %ebp
    kfree((char*)p);
80103d15:	e9 c6 ee ff ff       	jmp    80102be0 <kfree>
80103d1a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    wakeup(&p->nwrite);
80103d20:	83 ec 0c             	sub    $0xc,%esp
80103d23:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
    p->readopen = 0;
80103d29:	c7 83 3c 02 00 00 00 	movl   $0x0,0x23c(%ebx)
80103d30:	00 00 00 
    wakeup(&p->nwrite);
80103d33:	50                   	push   %eax
80103d34:	e8 e7 0d 00 00       	call   80104b20 <wakeup>
80103d39:	83 c4 10             	add    $0x10,%esp
80103d3c:	eb 9d                	jmp    80103cdb <pipeclose+0x3b>
80103d3e:	66 90                	xchg   %ax,%ax

80103d40 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
80103d40:	f3 0f 1e fb          	endbr32 
80103d44:	55                   	push   %ebp
80103d45:	89 e5                	mov    %esp,%ebp
80103d47:	57                   	push   %edi
80103d48:	56                   	push   %esi
80103d49:	53                   	push   %ebx
80103d4a:	83 ec 28             	sub    $0x28,%esp
80103d4d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
80103d50:	53                   	push   %ebx
80103d51:	e8 0a 14 00 00       	call   80105160 <acquire>
  for(i = 0; i < n; i++){
80103d56:	8b 45 10             	mov    0x10(%ebp),%eax
80103d59:	83 c4 10             	add    $0x10,%esp
80103d5c:	85 c0                	test   %eax,%eax
80103d5e:	0f 8e bc 00 00 00    	jle    80103e20 <pipewrite+0xe0>
80103d64:	8b 45 0c             	mov    0xc(%ebp),%eax
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103d67:	8b 8b 38 02 00 00    	mov    0x238(%ebx),%ecx
      if(p->readopen == 0 || myproc()->killed){
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
80103d6d:	8d bb 34 02 00 00    	lea    0x234(%ebx),%edi
80103d73:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80103d76:	03 45 10             	add    0x10(%ebp),%eax
80103d79:	89 45 e0             	mov    %eax,-0x20(%ebp)
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103d7c:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103d82:	8d b3 38 02 00 00    	lea    0x238(%ebx),%esi
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103d88:	89 ca                	mov    %ecx,%edx
80103d8a:	05 00 02 00 00       	add    $0x200,%eax
80103d8f:	39 c1                	cmp    %eax,%ecx
80103d91:	74 3b                	je     80103dce <pipewrite+0x8e>
80103d93:	eb 63                	jmp    80103df8 <pipewrite+0xb8>
80103d95:	8d 76 00             	lea    0x0(%esi),%esi
      if(p->readopen == 0 || myproc()->killed){
80103d98:	e8 73 03 00 00       	call   80104110 <myproc>
80103d9d:	8b 48 28             	mov    0x28(%eax),%ecx
80103da0:	85 c9                	test   %ecx,%ecx
80103da2:	75 34                	jne    80103dd8 <pipewrite+0x98>
      wakeup(&p->nread);
80103da4:	83 ec 0c             	sub    $0xc,%esp
80103da7:	57                   	push   %edi
80103da8:	e8 73 0d 00 00       	call   80104b20 <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103dad:	58                   	pop    %eax
80103dae:	5a                   	pop    %edx
80103daf:	53                   	push   %ebx
80103db0:	56                   	push   %esi
80103db1:	e8 aa 0c 00 00       	call   80104a60 <sleep>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103db6:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
80103dbc:	8b 93 38 02 00 00    	mov    0x238(%ebx),%edx
80103dc2:	83 c4 10             	add    $0x10,%esp
80103dc5:	05 00 02 00 00       	add    $0x200,%eax
80103dca:	39 c2                	cmp    %eax,%edx
80103dcc:	75 2a                	jne    80103df8 <pipewrite+0xb8>
      if(p->readopen == 0 || myproc()->killed){
80103dce:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
80103dd4:	85 c0                	test   %eax,%eax
80103dd6:	75 c0                	jne    80103d98 <pipewrite+0x58>
        release(&p->lock);
80103dd8:	83 ec 0c             	sub    $0xc,%esp
80103ddb:	53                   	push   %ebx
80103ddc:	e8 0f 13 00 00       	call   801050f0 <release>
        return -1;
80103de1:	83 c4 10             	add    $0x10,%esp
80103de4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  release(&p->lock);
  return n;
}
80103de9:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103dec:	5b                   	pop    %ebx
80103ded:	5e                   	pop    %esi
80103dee:	5f                   	pop    %edi
80103def:	5d                   	pop    %ebp
80103df0:	c3                   	ret    
80103df1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80103df8:	8b 75 e4             	mov    -0x1c(%ebp),%esi
80103dfb:	8d 4a 01             	lea    0x1(%edx),%ecx
80103dfe:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
80103e04:	89 8b 38 02 00 00    	mov    %ecx,0x238(%ebx)
80103e0a:	0f b6 06             	movzbl (%esi),%eax
  for(i = 0; i < n; i++){
80103e0d:	83 c6 01             	add    $0x1,%esi
80103e10:	89 75 e4             	mov    %esi,-0x1c(%ebp)
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80103e13:	88 44 13 34          	mov    %al,0x34(%ebx,%edx,1)
  for(i = 0; i < n; i++){
80103e17:	3b 75 e0             	cmp    -0x20(%ebp),%esi
80103e1a:	0f 85 5c ff ff ff    	jne    80103d7c <pipewrite+0x3c>
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
80103e20:	83 ec 0c             	sub    $0xc,%esp
80103e23:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80103e29:	50                   	push   %eax
80103e2a:	e8 f1 0c 00 00       	call   80104b20 <wakeup>
  release(&p->lock);
80103e2f:	89 1c 24             	mov    %ebx,(%esp)
80103e32:	e8 b9 12 00 00       	call   801050f0 <release>
  return n;
80103e37:	8b 45 10             	mov    0x10(%ebp),%eax
80103e3a:	83 c4 10             	add    $0x10,%esp
80103e3d:	eb aa                	jmp    80103de9 <pipewrite+0xa9>
80103e3f:	90                   	nop

80103e40 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
80103e40:	f3 0f 1e fb          	endbr32 
80103e44:	55                   	push   %ebp
80103e45:	89 e5                	mov    %esp,%ebp
80103e47:	57                   	push   %edi
80103e48:	56                   	push   %esi
80103e49:	53                   	push   %ebx
80103e4a:	83 ec 18             	sub    $0x18,%esp
80103e4d:	8b 75 08             	mov    0x8(%ebp),%esi
80103e50:	8b 7d 0c             	mov    0xc(%ebp),%edi
  int i;

  acquire(&p->lock);
80103e53:	56                   	push   %esi
80103e54:	8d 9e 34 02 00 00    	lea    0x234(%esi),%ebx
80103e5a:	e8 01 13 00 00       	call   80105160 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80103e5f:	8b 86 34 02 00 00    	mov    0x234(%esi),%eax
80103e65:	83 c4 10             	add    $0x10,%esp
80103e68:	39 86 38 02 00 00    	cmp    %eax,0x238(%esi)
80103e6e:	74 33                	je     80103ea3 <piperead+0x63>
80103e70:	eb 3b                	jmp    80103ead <piperead+0x6d>
80103e72:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(myproc()->killed){
80103e78:	e8 93 02 00 00       	call   80104110 <myproc>
80103e7d:	8b 48 28             	mov    0x28(%eax),%ecx
80103e80:	85 c9                	test   %ecx,%ecx
80103e82:	0f 85 88 00 00 00    	jne    80103f10 <piperead+0xd0>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80103e88:	83 ec 08             	sub    $0x8,%esp
80103e8b:	56                   	push   %esi
80103e8c:	53                   	push   %ebx
80103e8d:	e8 ce 0b 00 00       	call   80104a60 <sleep>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80103e92:	8b 86 38 02 00 00    	mov    0x238(%esi),%eax
80103e98:	83 c4 10             	add    $0x10,%esp
80103e9b:	39 86 34 02 00 00    	cmp    %eax,0x234(%esi)
80103ea1:	75 0a                	jne    80103ead <piperead+0x6d>
80103ea3:	8b 86 40 02 00 00    	mov    0x240(%esi),%eax
80103ea9:	85 c0                	test   %eax,%eax
80103eab:	75 cb                	jne    80103e78 <piperead+0x38>
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103ead:	8b 55 10             	mov    0x10(%ebp),%edx
80103eb0:	31 db                	xor    %ebx,%ebx
80103eb2:	85 d2                	test   %edx,%edx
80103eb4:	7f 28                	jg     80103ede <piperead+0x9e>
80103eb6:	eb 34                	jmp    80103eec <piperead+0xac>
80103eb8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103ebf:	90                   	nop
    if(p->nread == p->nwrite)
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
80103ec0:	8d 48 01             	lea    0x1(%eax),%ecx
80103ec3:	25 ff 01 00 00       	and    $0x1ff,%eax
80103ec8:	89 8e 34 02 00 00    	mov    %ecx,0x234(%esi)
80103ece:	0f b6 44 06 34       	movzbl 0x34(%esi,%eax,1),%eax
80103ed3:	88 04 1f             	mov    %al,(%edi,%ebx,1)
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103ed6:	83 c3 01             	add    $0x1,%ebx
80103ed9:	39 5d 10             	cmp    %ebx,0x10(%ebp)
80103edc:	74 0e                	je     80103eec <piperead+0xac>
    if(p->nread == p->nwrite)
80103ede:	8b 86 34 02 00 00    	mov    0x234(%esi),%eax
80103ee4:	3b 86 38 02 00 00    	cmp    0x238(%esi),%eax
80103eea:	75 d4                	jne    80103ec0 <piperead+0x80>
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
80103eec:	83 ec 0c             	sub    $0xc,%esp
80103eef:	8d 86 38 02 00 00    	lea    0x238(%esi),%eax
80103ef5:	50                   	push   %eax
80103ef6:	e8 25 0c 00 00       	call   80104b20 <wakeup>
  release(&p->lock);
80103efb:	89 34 24             	mov    %esi,(%esp)
80103efe:	e8 ed 11 00 00       	call   801050f0 <release>
  return i;
80103f03:	83 c4 10             	add    $0x10,%esp
}
80103f06:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103f09:	89 d8                	mov    %ebx,%eax
80103f0b:	5b                   	pop    %ebx
80103f0c:	5e                   	pop    %esi
80103f0d:	5f                   	pop    %edi
80103f0e:	5d                   	pop    %ebp
80103f0f:	c3                   	ret    
      release(&p->lock);
80103f10:	83 ec 0c             	sub    $0xc,%esp
      return -1;
80103f13:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
      release(&p->lock);
80103f18:	56                   	push   %esi
80103f19:	e8 d2 11 00 00       	call   801050f0 <release>
      return -1;
80103f1e:	83 c4 10             	add    $0x10,%esp
}
80103f21:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103f24:	89 d8                	mov    %ebx,%eax
80103f26:	5b                   	pop    %ebx
80103f27:	5e                   	pop    %esi
80103f28:	5f                   	pop    %edi
80103f29:	5d                   	pop    %ebp
80103f2a:	c3                   	ret    
80103f2b:	66 90                	xchg   %ax,%ax
80103f2d:	66 90                	xchg   %ax,%ax
80103f2f:	90                   	nop

80103f30 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
80103f30:	55                   	push   %ebp
80103f31:	89 e5                	mov    %esp,%ebp
80103f33:	53                   	push   %ebx
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103f34:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
{
80103f39:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);
80103f3c:	68 20 2d 11 80       	push   $0x80112d20
80103f41:	e8 1a 12 00 00       	call   80105160 <acquire>
80103f46:	83 c4 10             	add    $0x10,%esp
80103f49:	eb 13                	jmp    80103f5e <allocproc+0x2e>
80103f4b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103f4f:	90                   	nop
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103f50:	81 c3 94 00 00 00    	add    $0x94,%ebx
80103f56:	81 fb 54 52 11 80    	cmp    $0x80115254,%ebx
80103f5c:	74 7a                	je     80103fd8 <allocproc+0xa8>
    if(p->state == UNUSED)
80103f5e:	8b 43 0c             	mov    0xc(%ebx),%eax
80103f61:	85 c0                	test   %eax,%eax
80103f63:	75 eb                	jne    80103f50 <allocproc+0x20>
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
80103f65:	a1 04 b0 10 80       	mov    0x8010b004,%eax

  release(&ptable.lock);
80103f6a:	83 ec 0c             	sub    $0xc,%esp
  p->state = EMBRYO;
80103f6d:	c7 43 0c 01 00 00 00 	movl   $0x1,0xc(%ebx)
  p->pid = nextpid++;
80103f74:	89 43 10             	mov    %eax,0x10(%ebx)
80103f77:	8d 50 01             	lea    0x1(%eax),%edx
  release(&ptable.lock);
80103f7a:	68 20 2d 11 80       	push   $0x80112d20
  p->pid = nextpid++;
80103f7f:	89 15 04 b0 10 80    	mov    %edx,0x8010b004
  release(&ptable.lock);
80103f85:	e8 66 11 00 00       	call   801050f0 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
80103f8a:	e8 11 ee ff ff       	call   80102da0 <kalloc>
80103f8f:	83 c4 10             	add    $0x10,%esp
80103f92:	89 43 08             	mov    %eax,0x8(%ebx)
80103f95:	85 c0                	test   %eax,%eax
80103f97:	74 58                	je     80103ff1 <allocproc+0xc1>
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
80103f99:	8d 90 b4 0f 00 00    	lea    0xfb4(%eax),%edx
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
80103f9f:	83 ec 04             	sub    $0x4,%esp
  sp -= sizeof *p->context;
80103fa2:	05 9c 0f 00 00       	add    $0xf9c,%eax
  sp -= sizeof *p->tf;
80103fa7:	89 53 1c             	mov    %edx,0x1c(%ebx)
  *(uint*)sp = (uint)trapret;
80103faa:	c7 40 14 bf 65 10 80 	movl   $0x801065bf,0x14(%eax)
  p->context = (struct context*)sp;
80103fb1:	89 43 20             	mov    %eax,0x20(%ebx)
  memset(p->context, 0, sizeof *p->context);
80103fb4:	6a 14                	push   $0x14
80103fb6:	6a 00                	push   $0x0
80103fb8:	50                   	push   %eax
80103fb9:	e8 72 12 00 00       	call   80105230 <memset>
  p->context->eip = (uint)forkret;
80103fbe:	8b 43 20             	mov    0x20(%ebx),%eax

  return p;
80103fc1:	83 c4 10             	add    $0x10,%esp
  p->context->eip = (uint)forkret;
80103fc4:	c7 40 10 10 40 10 80 	movl   $0x80104010,0x10(%eax)
}
80103fcb:	89 d8                	mov    %ebx,%eax
80103fcd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103fd0:	c9                   	leave  
80103fd1:	c3                   	ret    
80103fd2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  release(&ptable.lock);
80103fd8:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80103fdb:	31 db                	xor    %ebx,%ebx
  release(&ptable.lock);
80103fdd:	68 20 2d 11 80       	push   $0x80112d20
80103fe2:	e8 09 11 00 00       	call   801050f0 <release>
}
80103fe7:	89 d8                	mov    %ebx,%eax
  return 0;
80103fe9:	83 c4 10             	add    $0x10,%esp
}
80103fec:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103fef:	c9                   	leave  
80103ff0:	c3                   	ret    
    p->state = UNUSED;
80103ff1:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return 0;
80103ff8:	31 db                	xor    %ebx,%ebx
}
80103ffa:	89 d8                	mov    %ebx,%eax
80103ffc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103fff:	c9                   	leave  
80104000:	c3                   	ret    
80104001:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104008:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010400f:	90                   	nop

80104010 <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
80104010:	f3 0f 1e fb          	endbr32 
80104014:	55                   	push   %ebp
80104015:	89 e5                	mov    %esp,%ebp
80104017:	83 ec 14             	sub    $0x14,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
8010401a:	68 20 2d 11 80       	push   $0x80112d20
8010401f:	e8 cc 10 00 00       	call   801050f0 <release>

  if (first) {
80104024:	a1 00 b0 10 80       	mov    0x8010b000,%eax
80104029:	83 c4 10             	add    $0x10,%esp
8010402c:	85 c0                	test   %eax,%eax
8010402e:	75 08                	jne    80104038 <forkret+0x28>
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}
80104030:	c9                   	leave  
80104031:	c3                   	ret    
80104032:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    first = 0;
80104038:	c7 05 00 b0 10 80 00 	movl   $0x0,0x8010b000
8010403f:	00 00 00 
    iinit(ROOTDEV);
80104042:	83 ec 0c             	sub    $0xc,%esp
80104045:	6a 01                	push   $0x1
80104047:	e8 e4 db ff ff       	call   80101c30 <iinit>
    initlog(ROOTDEV);
8010404c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80104053:	e8 a8 f3 ff ff       	call   80103400 <initlog>
}
80104058:	83 c4 10             	add    $0x10,%esp
8010405b:	c9                   	leave  
8010405c:	c3                   	ret    
8010405d:	8d 76 00             	lea    0x0(%esi),%esi

80104060 <pinit>:
{
80104060:	f3 0f 1e fb          	endbr32 
80104064:	55                   	push   %ebp
80104065:	89 e5                	mov    %esp,%ebp
80104067:	83 ec 10             	sub    $0x10,%esp
  initlock(&ptable.lock, "ptable");
8010406a:	68 e0 84 10 80       	push   $0x801084e0
8010406f:	68 20 2d 11 80       	push   $0x80112d20
80104074:	e8 e7 0e 00 00       	call   80104f60 <initlock>
}
80104079:	83 c4 10             	add    $0x10,%esp
8010407c:	c9                   	leave  
8010407d:	c3                   	ret    
8010407e:	66 90                	xchg   %ax,%ax

80104080 <mycpu>:
{
80104080:	f3 0f 1e fb          	endbr32 
80104084:	55                   	push   %ebp
80104085:	89 e5                	mov    %esp,%ebp
80104087:	56                   	push   %esi
80104088:	53                   	push   %ebx
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80104089:	9c                   	pushf  
8010408a:	58                   	pop    %eax
  if(readeflags()&FL_IF)
8010408b:	f6 c4 02             	test   $0x2,%ah
8010408e:	75 4a                	jne    801040da <mycpu+0x5a>
  apicid = lapicid();
80104090:	e8 7b ef ff ff       	call   80103010 <lapicid>
  for (i = 0; i < ncpu; ++i) {
80104095:	8b 35 84 27 11 80    	mov    0x80112784,%esi
  apicid = lapicid();
8010409b:	89 c3                	mov    %eax,%ebx
  for (i = 0; i < ncpu; ++i) {
8010409d:	85 f6                	test   %esi,%esi
8010409f:	7e 2c                	jle    801040cd <mycpu+0x4d>
801040a1:	31 c0                	xor    %eax,%eax
801040a3:	eb 0a                	jmp    801040af <mycpu+0x2f>
801040a5:	8d 76 00             	lea    0x0(%esi),%esi
801040a8:	83 c0 01             	add    $0x1,%eax
801040ab:	39 f0                	cmp    %esi,%eax
801040ad:	74 1e                	je     801040cd <mycpu+0x4d>
    if (cpus[i].apicid == apicid)
801040af:	69 d0 b0 00 00 00    	imul   $0xb0,%eax,%edx
801040b5:	0f b6 8a a0 27 11 80 	movzbl -0x7feed860(%edx),%ecx
801040bc:	39 d9                	cmp    %ebx,%ecx
801040be:	75 e8                	jne    801040a8 <mycpu+0x28>
}
801040c0:	8d 65 f8             	lea    -0x8(%ebp),%esp
      return &cpus[i];
801040c3:	8d 82 a0 27 11 80    	lea    -0x7feed860(%edx),%eax
}
801040c9:	5b                   	pop    %ebx
801040ca:	5e                   	pop    %esi
801040cb:	5d                   	pop    %ebp
801040cc:	c3                   	ret    
  panic("unknown apicid\n");
801040cd:	83 ec 0c             	sub    $0xc,%esp
801040d0:	68 e7 84 10 80       	push   $0x801084e7
801040d5:	e8 b6 c2 ff ff       	call   80100390 <panic>
    panic("mycpu called with interrupts enabled\n");
801040da:	83 ec 0c             	sub    $0xc,%esp
801040dd:	68 c4 85 10 80       	push   $0x801085c4
801040e2:	e8 a9 c2 ff ff       	call   80100390 <panic>
801040e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801040ee:	66 90                	xchg   %ax,%ax

801040f0 <cpuid>:
cpuid() {
801040f0:	f3 0f 1e fb          	endbr32 
801040f4:	55                   	push   %ebp
801040f5:	89 e5                	mov    %esp,%ebp
801040f7:	83 ec 08             	sub    $0x8,%esp
  return mycpu()-cpus;
801040fa:	e8 81 ff ff ff       	call   80104080 <mycpu>
}
801040ff:	c9                   	leave  
  return mycpu()-cpus;
80104100:	2d a0 27 11 80       	sub    $0x801127a0,%eax
80104105:	c1 f8 04             	sar    $0x4,%eax
80104108:	69 c0 a3 8b 2e ba    	imul   $0xba2e8ba3,%eax,%eax
}
8010410e:	c3                   	ret    
8010410f:	90                   	nop

80104110 <myproc>:
myproc(void) {
80104110:	f3 0f 1e fb          	endbr32 
80104114:	55                   	push   %ebp
80104115:	89 e5                	mov    %esp,%ebp
80104117:	53                   	push   %ebx
80104118:	83 ec 04             	sub    $0x4,%esp
  pushcli();
8010411b:	e8 d0 0e 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
80104120:	e8 5b ff ff ff       	call   80104080 <mycpu>
  p = c->proc;
80104125:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
8010412b:	e8 10 0f 00 00       	call   80105040 <popcli>
}
80104130:	89 d8                	mov    %ebx,%eax
80104132:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104135:	c9                   	leave  
80104136:	c3                   	ret    
80104137:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010413e:	66 90                	xchg   %ax,%ax

80104140 <userinit>:
{
80104140:	f3 0f 1e fb          	endbr32 
80104144:	55                   	push   %ebp
80104145:	89 e5                	mov    %esp,%ebp
80104147:	53                   	push   %ebx
80104148:	83 ec 04             	sub    $0x4,%esp
  p = allocproc();
8010414b:	e8 e0 fd ff ff       	call   80103f30 <allocproc>
80104150:	89 c3                	mov    %eax,%ebx
  initproc = p;
80104152:	a3 54 52 11 80       	mov    %eax,0x80115254
  if((p->pgdir = setupkvm()) == 0)
80104157:	e8 84 3a 00 00       	call   80107be0 <setupkvm>
8010415c:	89 43 04             	mov    %eax,0x4(%ebx)
8010415f:	85 c0                	test   %eax,%eax
80104161:	0f 84 bd 00 00 00    	je     80104224 <userinit+0xe4>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80104167:	83 ec 04             	sub    $0x4,%esp
8010416a:	68 2c 00 00 00       	push   $0x2c
8010416f:	68 60 b4 10 80       	push   $0x8010b460
80104174:	50                   	push   %eax
80104175:	e8 16 37 00 00       	call   80107890 <inituvm>
  memset(p->tf, 0, sizeof(*p->tf));
8010417a:	83 c4 0c             	add    $0xc,%esp
  p->sz = PGSIZE;
8010417d:	c7 03 00 10 00 00    	movl   $0x1000,(%ebx)
  memset(p->tf, 0, sizeof(*p->tf));
80104183:	6a 4c                	push   $0x4c
80104185:	6a 00                	push   $0x0
80104187:	ff 73 1c             	push   0x1c(%ebx)
8010418a:	e8 a1 10 00 00       	call   80105230 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
8010418f:	8b 43 1c             	mov    0x1c(%ebx),%eax
80104192:	ba 1b 00 00 00       	mov    $0x1b,%edx
  safestrcpy(p->name, "initcode", sizeof(p->name));
80104197:	83 c4 0c             	add    $0xc,%esp
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
8010419a:	b9 23 00 00 00       	mov    $0x23,%ecx
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
8010419f:	66 89 50 3c          	mov    %dx,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
801041a3:	8b 43 1c             	mov    0x1c(%ebx),%eax
801041a6:	66 89 48 2c          	mov    %cx,0x2c(%eax)
  p->tf->es = p->tf->ds;
801041aa:	8b 43 1c             	mov    0x1c(%ebx),%eax
801041ad:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
801041b1:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
801041b5:	8b 43 1c             	mov    0x1c(%ebx),%eax
801041b8:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
801041bc:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
801041c0:	8b 43 1c             	mov    0x1c(%ebx),%eax
801041c3:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
801041ca:	8b 43 1c             	mov    0x1c(%ebx),%eax
801041cd:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
801041d4:	8b 43 1c             	mov    0x1c(%ebx),%eax
801041d7:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)
  safestrcpy(p->name, "initcode", sizeof(p->name));
801041de:	8d 43 70             	lea    0x70(%ebx),%eax
801041e1:	6a 10                	push   $0x10
801041e3:	68 10 85 10 80       	push   $0x80108510
801041e8:	50                   	push   %eax
801041e9:	e8 02 12 00 00       	call   801053f0 <safestrcpy>
  p->cwd = namei("/");
801041ee:	c7 04 24 19 85 10 80 	movl   $0x80108519,(%esp)
801041f5:	e8 a6 e5 ff ff       	call   801027a0 <namei>
801041fa:	89 43 6c             	mov    %eax,0x6c(%ebx)
  acquire(&ptable.lock);
801041fd:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80104204:	e8 57 0f 00 00       	call   80105160 <acquire>
  p->state = RUNNABLE;
80104209:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  release(&ptable.lock);
80104210:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80104217:	e8 d4 0e 00 00       	call   801050f0 <release>
}
8010421c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010421f:	83 c4 10             	add    $0x10,%esp
80104222:	c9                   	leave  
80104223:	c3                   	ret    
    panic("userinit: out of memory?");
80104224:	83 ec 0c             	sub    $0xc,%esp
80104227:	68 f7 84 10 80       	push   $0x801084f7
8010422c:	e8 5f c1 ff ff       	call   80100390 <panic>
80104231:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104238:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010423f:	90                   	nop

80104240 <growproc>:
{
80104240:	f3 0f 1e fb          	endbr32 
80104244:	55                   	push   %ebp
80104245:	89 e5                	mov    %esp,%ebp
80104247:	56                   	push   %esi
80104248:	53                   	push   %ebx
80104249:	8b 75 08             	mov    0x8(%ebp),%esi
  pushcli();
8010424c:	e8 9f 0d 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
80104251:	e8 2a fe ff ff       	call   80104080 <mycpu>
  p = c->proc;
80104256:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
8010425c:	e8 df 0d 00 00       	call   80105040 <popcli>
  sz = curproc->sz;
80104261:	8b 03                	mov    (%ebx),%eax
  if(n > 0){
80104263:	85 f6                	test   %esi,%esi
80104265:	7f 19                	jg     80104280 <growproc+0x40>
  } else if(n < 0){
80104267:	75 37                	jne    801042a0 <growproc+0x60>
  switchuvm(curproc);
80104269:	83 ec 0c             	sub    $0xc,%esp
  curproc->sz = sz;
8010426c:	89 03                	mov    %eax,(%ebx)
  switchuvm(curproc);
8010426e:	53                   	push   %ebx
8010426f:	e8 0c 35 00 00       	call   80107780 <switchuvm>
  return 0;
80104274:	83 c4 10             	add    $0x10,%esp
80104277:	31 c0                	xor    %eax,%eax
}
80104279:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010427c:	5b                   	pop    %ebx
8010427d:	5e                   	pop    %esi
8010427e:	5d                   	pop    %ebp
8010427f:	c3                   	ret    
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
80104280:	83 ec 04             	sub    $0x4,%esp
80104283:	01 c6                	add    %eax,%esi
80104285:	56                   	push   %esi
80104286:	50                   	push   %eax
80104287:	ff 73 04             	push   0x4(%ebx)
8010428a:	e8 71 37 00 00       	call   80107a00 <allocuvm>
8010428f:	83 c4 10             	add    $0x10,%esp
80104292:	85 c0                	test   %eax,%eax
80104294:	75 d3                	jne    80104269 <growproc+0x29>
      return -1;
80104296:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010429b:	eb dc                	jmp    80104279 <growproc+0x39>
8010429d:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
801042a0:	83 ec 04             	sub    $0x4,%esp
801042a3:	01 c6                	add    %eax,%esi
801042a5:	56                   	push   %esi
801042a6:	50                   	push   %eax
801042a7:	ff 73 04             	push   0x4(%ebx)
801042aa:	e8 81 38 00 00       	call   80107b30 <deallocuvm>
801042af:	83 c4 10             	add    $0x10,%esp
801042b2:	85 c0                	test   %eax,%eax
801042b4:	75 b3                	jne    80104269 <growproc+0x29>
801042b6:	eb de                	jmp    80104296 <growproc+0x56>
801042b8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801042bf:	90                   	nop

801042c0 <fork>:
{
801042c0:	f3 0f 1e fb          	endbr32 
801042c4:	55                   	push   %ebp
801042c5:	89 e5                	mov    %esp,%ebp
801042c7:	57                   	push   %edi
801042c8:	56                   	push   %esi
801042c9:	53                   	push   %ebx
801042ca:	83 ec 1c             	sub    $0x1c,%esp
  pushcli();
801042cd:	e8 1e 0d 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
801042d2:	e8 a9 fd ff ff       	call   80104080 <mycpu>
  p = c->proc;
801042d7:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
801042dd:	e8 5e 0d 00 00       	call   80105040 <popcli>
  if((np = allocproc()) == 0){
801042e2:	e8 49 fc ff ff       	call   80103f30 <allocproc>
801042e7:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801042ea:	85 c0                	test   %eax,%eax
801042ec:	0f 84 c3 00 00 00    	je     801043b5 <fork+0xf5>
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
801042f2:	83 ec 08             	sub    $0x8,%esp
801042f5:	ff 33                	push   (%ebx)
801042f7:	89 c7                	mov    %eax,%edi
801042f9:	ff 73 04             	push   0x4(%ebx)
801042fc:	e8 cf 39 00 00       	call   80107cd0 <copyuvm>
80104301:	83 c4 10             	add    $0x10,%esp
80104304:	89 47 04             	mov    %eax,0x4(%edi)
80104307:	85 c0                	test   %eax,%eax
80104309:	0f 84 ad 00 00 00    	je     801043bc <fork+0xfc>
  np->sz = curproc->sz;
8010430f:	8b 03                	mov    (%ebx),%eax
80104311:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80104314:	89 01                	mov    %eax,(%ecx)
  *np->tf = *curproc->tf;
80104316:	8b 79 1c             	mov    0x1c(%ecx),%edi
  np->parent = curproc;
80104319:	89 c8                	mov    %ecx,%eax
8010431b:	89 59 14             	mov    %ebx,0x14(%ecx)
  np->debuger_parent = curproc;
8010431e:	89 59 18             	mov    %ebx,0x18(%ecx)
  *np->tf = *curproc->tf;
80104321:	b9 13 00 00 00       	mov    $0x13,%ecx
80104326:	8b 73 1c             	mov    0x1c(%ebx),%esi
80104329:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  for(i = 0; i < NOFILE; i++)
8010432b:	31 f6                	xor    %esi,%esi
  np->tf->eax = 0;
8010432d:	8b 40 1c             	mov    0x1c(%eax),%eax
80104330:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
  for(i = 0; i < NOFILE; i++)
80104337:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010433e:	66 90                	xchg   %ax,%ax
    if(curproc->ofile[i])
80104340:	8b 44 b3 2c          	mov    0x2c(%ebx,%esi,4),%eax
80104344:	85 c0                	test   %eax,%eax
80104346:	74 13                	je     8010435b <fork+0x9b>
      np->ofile[i] = filedup(curproc->ofile[i]);
80104348:	83 ec 0c             	sub    $0xc,%esp
8010434b:	50                   	push   %eax
8010434c:	e8 df d1 ff ff       	call   80101530 <filedup>
80104351:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104354:	83 c4 10             	add    $0x10,%esp
80104357:	89 44 b2 2c          	mov    %eax,0x2c(%edx,%esi,4)
  for(i = 0; i < NOFILE; i++)
8010435b:	83 c6 01             	add    $0x1,%esi
8010435e:	83 fe 10             	cmp    $0x10,%esi
80104361:	75 dd                	jne    80104340 <fork+0x80>
  np->cwd = idup(curproc->cwd);
80104363:	83 ec 0c             	sub    $0xc,%esp
80104366:	ff 73 6c             	push   0x6c(%ebx)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80104369:	83 c3 70             	add    $0x70,%ebx
  np->cwd = idup(curproc->cwd);
8010436c:	e8 bf da ff ff       	call   80101e30 <idup>
80104371:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80104374:	83 c4 0c             	add    $0xc,%esp
  np->cwd = idup(curproc->cwd);
80104377:	89 47 6c             	mov    %eax,0x6c(%edi)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
8010437a:	8d 47 70             	lea    0x70(%edi),%eax
8010437d:	6a 10                	push   $0x10
8010437f:	53                   	push   %ebx
80104380:	50                   	push   %eax
80104381:	e8 6a 10 00 00       	call   801053f0 <safestrcpy>
  pid = np->pid;
80104386:	8b 5f 10             	mov    0x10(%edi),%ebx
  acquire(&ptable.lock);
80104389:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80104390:	e8 cb 0d 00 00       	call   80105160 <acquire>
  np->state = RUNNABLE;
80104395:	c7 47 0c 03 00 00 00 	movl   $0x3,0xc(%edi)
  release(&ptable.lock);
8010439c:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
801043a3:	e8 48 0d 00 00       	call   801050f0 <release>
  return pid;
801043a8:	83 c4 10             	add    $0x10,%esp
}
801043ab:	8d 65 f4             	lea    -0xc(%ebp),%esp
801043ae:	89 d8                	mov    %ebx,%eax
801043b0:	5b                   	pop    %ebx
801043b1:	5e                   	pop    %esi
801043b2:	5f                   	pop    %edi
801043b3:	5d                   	pop    %ebp
801043b4:	c3                   	ret    
    return -1;
801043b5:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801043ba:	eb ef                	jmp    801043ab <fork+0xeb>
    kfree(np->kstack);
801043bc:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801043bf:	83 ec 0c             	sub    $0xc,%esp
801043c2:	ff 73 08             	push   0x8(%ebx)
801043c5:	e8 16 e8 ff ff       	call   80102be0 <kfree>
    np->kstack = 0;
801043ca:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
    return -1;
801043d1:	83 c4 10             	add    $0x10,%esp
    np->state = UNUSED;
801043d4:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return -1;
801043db:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801043e0:	eb c9                	jmp    801043ab <fork+0xeb>
801043e2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801043e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801043f0 <proc_with_round_robin>:
{
801043f0:	f3 0f 1e fb          	endbr32 
801043f4:	55                   	push   %ebp
801043f5:	89 e5                	mov    %esp,%ebp
801043f7:	56                   	push   %esi
  struct proc *chosen_proc = 0;
801043f8:	31 f6                	xor    %esi,%esi
{
801043fa:	53                   	push   %ebx
  acquire(&tickslock);
801043fb:	83 ec 0c             	sub    $0xc,%esp
801043fe:	68 80 52 11 80       	push   $0x80115280
80104403:	e8 58 0d 00 00       	call   80105160 <acquire>
  release(&tickslock);
80104408:	c7 04 24 80 52 11 80 	movl   $0x80115280,(%esp)
  current_time=ticks;
8010440f:	8b 1d 60 52 11 80    	mov    0x80115260,%ebx
  release(&tickslock);
80104415:	e8 d6 0c 00 00       	call   801050f0 <release>
8010441a:	83 c4 10             	add    $0x10,%esp
  int max_differ = -10;
8010441d:	b9 f6 ff ff ff       	mov    $0xfffffff6,%ecx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104422:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80104427:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010442e:	66 90                	xchg   %ax,%ax
      if(p->state != RUNNABLE || p->priority != 1)
80104430:	83 78 0c 03          	cmpl   $0x3,0xc(%eax)
80104434:	75 1a                	jne    80104450 <proc_with_round_robin+0x60>
80104436:	83 b8 80 00 00 00 01 	cmpl   $0x1,0x80(%eax)
8010443d:	75 11                	jne    80104450 <proc_with_round_robin+0x60>
      if((current_time - p->last_time_executed) > max_differ){
8010443f:	89 da                	mov    %ebx,%edx
80104441:	2b 90 8c 00 00 00    	sub    0x8c(%eax),%edx
80104447:	39 ca                	cmp    %ecx,%edx
80104449:	7e 05                	jle    80104450 <proc_with_round_robin+0x60>
8010444b:	89 d1                	mov    %edx,%ecx
8010444d:	89 c6                	mov    %eax,%esi
8010444f:	90                   	nop
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104450:	05 94 00 00 00       	add    $0x94,%eax
80104455:	3d 54 52 11 80       	cmp    $0x80115254,%eax
8010445a:	75 d4                	jne    80104430 <proc_with_round_robin+0x40>
}
8010445c:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010445f:	89 f0                	mov    %esi,%eax
80104461:	5b                   	pop    %ebx
80104462:	5e                   	pop    %esi
80104463:	5d                   	pop    %ebp
80104464:	c3                   	ret    
80104465:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010446c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104470 <proc_with_lcfs>:
{
80104470:	f3 0f 1e fb          	endbr32 
80104474:	55                   	push   %ebp
  int min_time = -100000000;
80104475:	b9 00 1f 0a fa       	mov    $0xfa0a1f00,%ecx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010447a:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
{
8010447f:	89 e5                	mov    %esp,%ebp
80104481:	53                   	push   %ebx
  struct proc *last_proc = 0;
80104482:	31 db                	xor    %ebx,%ebx
80104484:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if (p->state != RUNNABLE || p->priority != 2)
80104488:	83 78 0c 03          	cmpl   $0x3,0xc(%eax)
8010448c:	75 1a                	jne    801044a8 <proc_with_lcfs+0x38>
8010448e:	83 b8 80 00 00 00 02 	cmpl   $0x2,0x80(%eax)
80104495:	75 11                	jne    801044a8 <proc_with_lcfs+0x38>
      if (p->create_time > min_time)
80104497:	8b 90 84 00 00 00    	mov    0x84(%eax),%edx
8010449d:	39 ca                	cmp    %ecx,%edx
8010449f:	76 07                	jbe    801044a8 <proc_with_lcfs+0x38>
        min_time = p->create_time;
801044a1:	89 d1                	mov    %edx,%ecx
801044a3:	89 c3                	mov    %eax,%ebx
801044a5:	8d 76 00             	lea    0x0(%esi),%esi
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801044a8:	05 94 00 00 00       	add    $0x94,%eax
801044ad:	3d 54 52 11 80       	cmp    $0x80115254,%eax
801044b2:	75 d4                	jne    80104488 <proc_with_lcfs+0x18>
}
801044b4:	89 d8                	mov    %ebx,%eax
801044b6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801044b9:	c9                   	leave  
801044ba:	c3                   	ret    
801044bb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801044bf:	90                   	nop

801044c0 <get_MHRRN>:
{
801044c0:	f3 0f 1e fb          	endbr32 
801044c4:	55                   	push   %ebp
801044c5:	89 e5                	mov    %esp,%ebp
801044c7:	56                   	push   %esi
801044c8:	53                   	push   %ebx
801044c9:	83 ec 1c             	sub    $0x1c,%esp
801044cc:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&tickslock);
801044cf:	68 80 52 11 80       	push   $0x80115280
801044d4:	e8 87 0c 00 00       	call   80105160 <acquire>
  current_time = ticks;
801044d9:	8b 35 60 52 11 80    	mov    0x80115260,%esi
  release(&tickslock);
801044df:	c7 04 24 80 52 11 80 	movl   $0x80115280,(%esp)
801044e6:	e8 05 0c 00 00       	call   801050f0 <release>
  float waitng_time = current_time - p->create_time;
801044eb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  hrrn = (waitng_time + p->executed_cycle_number) / p->executed_cycle_number;
801044f2:	db 83 88 00 00 00    	fildl  0x88(%ebx)
  float waitng_time = current_time - p->create_time;
801044f8:	89 f0                	mov    %esi,%eax
801044fa:	2b 83 84 00 00 00    	sub    0x84(%ebx),%eax
80104500:	89 45 f0             	mov    %eax,-0x10(%ebp)
80104503:	df 6d f0             	fildll -0x10(%ebp)
  hrrn = (waitng_time + p->executed_cycle_number) / p->executed_cycle_number;
80104506:	d8 c1                	fadd   %st(1),%st
80104508:	de f1                	fdivp  %st,%st(1)
  float mhrrn = ( hrrn + p->Hrnn_priority ) / 2 ;
8010450a:	db 83 90 00 00 00    	fildl  0x90(%ebx)
80104510:	de c1                	faddp  %st,%st(1)
80104512:	d8 0d 34 86 10 80    	fmuls  0x80108634
}
80104518:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010451b:	5b                   	pop    %ebx
8010451c:	5e                   	pop    %esi
8010451d:	5d                   	pop    %ebp
8010451e:	c3                   	ret    
8010451f:	90                   	nop

80104520 <proc_with_MHRRN>:
{
80104520:	f3 0f 1e fb          	endbr32 
80104524:	55                   	push   %ebp
80104525:	89 e5                	mov    %esp,%ebp
80104527:	57                   	push   %edi
80104528:	56                   	push   %esi
  struct proc* max_proc = 0;
80104529:	31 f6                	xor    %esi,%esi
{
8010452b:	53                   	push   %ebx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){ 
8010452c:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
{
80104531:	83 ec 1c             	sub    $0x1c,%esp
  float max_mhrrn = -10000000000;
80104534:	d9 05 38 86 10 80    	flds   0x80108638
8010453a:	d9 5d dc             	fstps  -0x24(%ebp)
8010453d:	eb 13                	jmp    80104552 <proc_with_MHRRN+0x32>
8010453f:	90                   	nop
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){ 
80104540:	81 c3 94 00 00 00    	add    $0x94,%ebx
80104546:	81 fb 54 52 11 80    	cmp    $0x80115254,%ebx
8010454c:	0f 84 cc 00 00 00    	je     8010461e <proc_with_MHRRN+0xfe>
    if (p->state != RUNNABLE || p->priority != 3)
80104552:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
80104556:	75 e8                	jne    80104540 <proc_with_MHRRN+0x20>
80104558:	83 bb 80 00 00 00 03 	cmpl   $0x3,0x80(%ebx)
8010455f:	75 df                	jne    80104540 <proc_with_MHRRN+0x20>
  acquire(&tickslock);
80104561:	83 ec 0c             	sub    $0xc,%esp
80104564:	68 80 52 11 80       	push   $0x80115280
80104569:	e8 f2 0b 00 00       	call   80105160 <acquire>
  current_time = ticks;
8010456e:	8b 3d 60 52 11 80    	mov    0x80115260,%edi
  release(&tickslock);
80104574:	c7 04 24 80 52 11 80 	movl   $0x80115280,(%esp)
8010457b:	e8 70 0b 00 00       	call   801050f0 <release>
  float waitng_time = current_time - p->create_time;
80104580:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  hrrn = (waitng_time + p->executed_cycle_number) / p->executed_cycle_number;
80104587:	db 83 88 00 00 00    	fildl  0x88(%ebx)
    if (get_MHRRN(p) > max_mhrrn){
8010458d:	83 c4 10             	add    $0x10,%esp
  float waitng_time = current_time - p->create_time;
80104590:	89 f8                	mov    %edi,%eax
80104592:	2b 83 84 00 00 00    	sub    0x84(%ebx),%eax
80104598:	89 45 e0             	mov    %eax,-0x20(%ebp)
8010459b:	df 6d e0             	fildll -0x20(%ebp)
  hrrn = (waitng_time + p->executed_cycle_number) / p->executed_cycle_number;
8010459e:	d8 c1                	fadd   %st(1),%st
801045a0:	de f1                	fdivp  %st,%st(1)
  float mhrrn = ( hrrn + p->Hrnn_priority ) / 2 ;
801045a2:	db 83 90 00 00 00    	fildl  0x90(%ebx)
801045a8:	de c1                	faddp  %st,%st(1)
801045aa:	d8 0d 34 86 10 80    	fmuls  0x80108634
    if (get_MHRRN(p) > max_mhrrn){
801045b0:	d9 45 dc             	flds   -0x24(%ebp)
801045b3:	d9 c9                	fxch   %st(1)
801045b5:	df f1                	fcomip %st(1),%st
801045b7:	dd d8                	fstp   %st(0)
801045b9:	76 85                	jbe    80104540 <proc_with_MHRRN+0x20>
  acquire(&tickslock);
801045bb:	83 ec 0c             	sub    $0xc,%esp
801045be:	68 80 52 11 80       	push   $0x80115280
801045c3:	e8 98 0b 00 00       	call   80105160 <acquire>
  current_time = ticks;
801045c8:	8b 35 60 52 11 80    	mov    0x80115260,%esi
  release(&tickslock);
801045ce:	c7 04 24 80 52 11 80 	movl   $0x80115280,(%esp)
801045d5:	e8 16 0b 00 00       	call   801050f0 <release>
  float waitng_time = current_time - p->create_time;
801045da:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  hrrn = (waitng_time + p->executed_cycle_number) / p->executed_cycle_number;
801045e1:	db 83 88 00 00 00    	fildl  0x88(%ebx)
  float mhrrn = ( hrrn + p->Hrnn_priority ) / 2 ;
801045e7:	83 c4 10             	add    $0x10,%esp
  float waitng_time = current_time - p->create_time;
801045ea:	89 f0                	mov    %esi,%eax
801045ec:	2b 83 84 00 00 00    	sub    0x84(%ebx),%eax
  float mhrrn = ( hrrn + p->Hrnn_priority ) / 2 ;
801045f2:	89 de                	mov    %ebx,%esi
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){ 
801045f4:	81 c3 94 00 00 00    	add    $0x94,%ebx
  float waitng_time = current_time - p->create_time;
801045fa:	89 45 e0             	mov    %eax,-0x20(%ebp)
801045fd:	df 6d e0             	fildll -0x20(%ebp)
  hrrn = (waitng_time + p->executed_cycle_number) / p->executed_cycle_number;
80104600:	d8 c1                	fadd   %st(1),%st
80104602:	de f1                	fdivp  %st,%st(1)
  float mhrrn = ( hrrn + p->Hrnn_priority ) / 2 ;
80104604:	db 43 fc             	fildl  -0x4(%ebx)
80104607:	de c1                	faddp  %st,%st(1)
80104609:	d8 0d 34 86 10 80    	fmuls  0x80108634
8010460f:	d9 5d dc             	fstps  -0x24(%ebp)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){ 
80104612:	81 fb 54 52 11 80    	cmp    $0x80115254,%ebx
80104618:	0f 85 34 ff ff ff    	jne    80104552 <proc_with_MHRRN+0x32>
}
8010461e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104621:	89 f0                	mov    %esi,%eax
80104623:	5b                   	pop    %ebx
80104624:	5e                   	pop    %esi
80104625:	5f                   	pop    %edi
80104626:	5d                   	pop    %ebp
80104627:	c3                   	ret    
80104628:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010462f:	90                   	nop

80104630 <scheduler>:
{
80104630:	f3 0f 1e fb          	endbr32 
80104634:	55                   	push   %ebp
80104635:	89 e5                	mov    %esp,%ebp
80104637:	57                   	push   %edi
80104638:	56                   	push   %esi
80104639:	53                   	push   %ebx
8010463a:	83 ec 0c             	sub    $0xc,%esp
  struct cpu *c = mycpu();
8010463d:	e8 3e fa ff ff       	call   80104080 <mycpu>
  c->proc = 0;
80104642:	c7 80 ac 00 00 00 00 	movl   $0x0,0xac(%eax)
80104649:	00 00 00 
  struct cpu *c = mycpu();
8010464c:	89 c6                	mov    %eax,%esi
  c->proc = 0;
8010464e:	8d 78 04             	lea    0x4(%eax),%edi
80104651:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  asm volatile("sti");
80104658:	fb                   	sti    
    acquire(&ptable.lock);
80104659:	83 ec 0c             	sub    $0xc,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010465c:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
    acquire(&ptable.lock);
80104661:	68 20 2d 11 80       	push   $0x80112d20
80104666:	e8 f5 0a 00 00       	call   80105160 <acquire>
8010466b:	83 c4 10             	add    $0x10,%esp
8010466e:	66 90                	xchg   %ax,%ax
      if(p->state != RUNNABLE)
80104670:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
80104674:	75 33                	jne    801046a9 <scheduler+0x79>
      switchuvm(p);
80104676:	83 ec 0c             	sub    $0xc,%esp
      c->proc = p;
80104679:	89 9e ac 00 00 00    	mov    %ebx,0xac(%esi)
      switchuvm(p);
8010467f:	53                   	push   %ebx
80104680:	e8 fb 30 00 00       	call   80107780 <switchuvm>
      swtch(&(c->scheduler), p->context);
80104685:	58                   	pop    %eax
80104686:	5a                   	pop    %edx
80104687:	ff 73 20             	push   0x20(%ebx)
8010468a:	57                   	push   %edi
      p->state = RUNNING;
8010468b:	c7 43 0c 04 00 00 00 	movl   $0x4,0xc(%ebx)
      swtch(&(c->scheduler), p->context);
80104692:	e8 bc 0d 00 00       	call   80105453 <swtch>
      switchkvm();
80104697:	e8 c4 30 00 00       	call   80107760 <switchkvm>
      c->proc = 0;
8010469c:	83 c4 10             	add    $0x10,%esp
8010469f:	c7 86 ac 00 00 00 00 	movl   $0x0,0xac(%esi)
801046a6:	00 00 00 
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801046a9:	81 c3 94 00 00 00    	add    $0x94,%ebx
801046af:	81 fb 54 52 11 80    	cmp    $0x80115254,%ebx
801046b5:	75 b9                	jne    80104670 <scheduler+0x40>
    release(&ptable.lock);
801046b7:	83 ec 0c             	sub    $0xc,%esp
801046ba:	68 20 2d 11 80       	push   $0x80112d20
801046bf:	e8 2c 0a 00 00       	call   801050f0 <release>
    sti();
801046c4:	83 c4 10             	add    $0x10,%esp
801046c7:	eb 8f                	jmp    80104658 <scheduler+0x28>
801046c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801046d0 <sched>:
{
801046d0:	f3 0f 1e fb          	endbr32 
801046d4:	55                   	push   %ebp
801046d5:	89 e5                	mov    %esp,%ebp
801046d7:	56                   	push   %esi
801046d8:	53                   	push   %ebx
  pushcli();
801046d9:	e8 12 09 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
801046de:	e8 9d f9 ff ff       	call   80104080 <mycpu>
  p = c->proc;
801046e3:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
801046e9:	e8 52 09 00 00       	call   80105040 <popcli>
  if(!holding(&ptable.lock))
801046ee:	83 ec 0c             	sub    $0xc,%esp
801046f1:	68 20 2d 11 80       	push   $0x80112d20
801046f6:	e8 a5 09 00 00       	call   801050a0 <holding>
801046fb:	83 c4 10             	add    $0x10,%esp
801046fe:	85 c0                	test   %eax,%eax
80104700:	74 4f                	je     80104751 <sched+0x81>
  if(mycpu()->ncli != 1)
80104702:	e8 79 f9 ff ff       	call   80104080 <mycpu>
80104707:	83 b8 a4 00 00 00 01 	cmpl   $0x1,0xa4(%eax)
8010470e:	75 68                	jne    80104778 <sched+0xa8>
  if(p->state == RUNNING)
80104710:	83 7b 0c 04          	cmpl   $0x4,0xc(%ebx)
80104714:	74 55                	je     8010476b <sched+0x9b>
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80104716:	9c                   	pushf  
80104717:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80104718:	f6 c4 02             	test   $0x2,%ah
8010471b:	75 41                	jne    8010475e <sched+0x8e>
  intena = mycpu()->intena;
8010471d:	e8 5e f9 ff ff       	call   80104080 <mycpu>
  swtch(&p->context, mycpu()->scheduler);
80104722:	83 c3 20             	add    $0x20,%ebx
  intena = mycpu()->intena;
80104725:	8b b0 a8 00 00 00    	mov    0xa8(%eax),%esi
  swtch(&p->context, mycpu()->scheduler);
8010472b:	e8 50 f9 ff ff       	call   80104080 <mycpu>
80104730:	83 ec 08             	sub    $0x8,%esp
80104733:	ff 70 04             	push   0x4(%eax)
80104736:	53                   	push   %ebx
80104737:	e8 17 0d 00 00       	call   80105453 <swtch>
  mycpu()->intena = intena;
8010473c:	e8 3f f9 ff ff       	call   80104080 <mycpu>
}
80104741:	83 c4 10             	add    $0x10,%esp
  mycpu()->intena = intena;
80104744:	89 b0 a8 00 00 00    	mov    %esi,0xa8(%eax)
}
8010474a:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010474d:	5b                   	pop    %ebx
8010474e:	5e                   	pop    %esi
8010474f:	5d                   	pop    %ebp
80104750:	c3                   	ret    
    panic("sched ptable.lock");
80104751:	83 ec 0c             	sub    $0xc,%esp
80104754:	68 1b 85 10 80       	push   $0x8010851b
80104759:	e8 32 bc ff ff       	call   80100390 <panic>
    panic("sched interruptible");
8010475e:	83 ec 0c             	sub    $0xc,%esp
80104761:	68 47 85 10 80       	push   $0x80108547
80104766:	e8 25 bc ff ff       	call   80100390 <panic>
    panic("sched running");
8010476b:	83 ec 0c             	sub    $0xc,%esp
8010476e:	68 39 85 10 80       	push   $0x80108539
80104773:	e8 18 bc ff ff       	call   80100390 <panic>
    panic("sched locks");
80104778:	83 ec 0c             	sub    $0xc,%esp
8010477b:	68 2d 85 10 80       	push   $0x8010852d
80104780:	e8 0b bc ff ff       	call   80100390 <panic>
80104785:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010478c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104790 <exit>:
{
80104790:	f3 0f 1e fb          	endbr32 
80104794:	55                   	push   %ebp
80104795:	89 e5                	mov    %esp,%ebp
80104797:	57                   	push   %edi
80104798:	56                   	push   %esi
80104799:	53                   	push   %ebx
8010479a:	83 ec 0c             	sub    $0xc,%esp
  struct proc *curproc = myproc();
8010479d:	e8 6e f9 ff ff       	call   80104110 <myproc>
  if(curproc == initproc)
801047a2:	39 05 54 52 11 80    	cmp    %eax,0x80115254
801047a8:	0f 84 03 01 00 00    	je     801048b1 <exit+0x121>
801047ae:	89 c3                	mov    %eax,%ebx
801047b0:	8d 70 2c             	lea    0x2c(%eax),%esi
801047b3:	8d 78 6c             	lea    0x6c(%eax),%edi
801047b6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801047bd:	8d 76 00             	lea    0x0(%esi),%esi
    if(curproc->ofile[fd]){
801047c0:	8b 06                	mov    (%esi),%eax
801047c2:	85 c0                	test   %eax,%eax
801047c4:	74 12                	je     801047d8 <exit+0x48>
      fileclose(curproc->ofile[fd]);
801047c6:	83 ec 0c             	sub    $0xc,%esp
801047c9:	50                   	push   %eax
801047ca:	e8 b1 cd ff ff       	call   80101580 <fileclose>
      curproc->ofile[fd] = 0;
801047cf:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
801047d5:	83 c4 10             	add    $0x10,%esp
  for(fd = 0; fd < NOFILE; fd++){
801047d8:	83 c6 04             	add    $0x4,%esi
801047db:	39 f7                	cmp    %esi,%edi
801047dd:	75 e1                	jne    801047c0 <exit+0x30>
  begin_op();
801047df:	e8 bc ec ff ff       	call   801034a0 <begin_op>
  iput(curproc->cwd);
801047e4:	83 ec 0c             	sub    $0xc,%esp
801047e7:	ff 73 6c             	push   0x6c(%ebx)
801047ea:	e8 a1 d7 ff ff       	call   80101f90 <iput>
  end_op();
801047ef:	e8 1c ed ff ff       	call   80103510 <end_op>
  curproc->cwd = 0;
801047f4:	c7 43 6c 00 00 00 00 	movl   $0x0,0x6c(%ebx)
  acquire(&ptable.lock);
801047fb:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80104802:	e8 59 09 00 00       	call   80105160 <acquire>
  wakeup1(curproc->parent);
80104807:	8b 53 14             	mov    0x14(%ebx),%edx
8010480a:	83 c4 10             	add    $0x10,%esp
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
8010480d:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80104812:	eb 10                	jmp    80104824 <exit+0x94>
80104814:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104818:	05 94 00 00 00       	add    $0x94,%eax
8010481d:	3d 54 52 11 80       	cmp    $0x80115254,%eax
80104822:	74 1e                	je     80104842 <exit+0xb2>
    if(p->state == SLEEPING && p->chan == chan)
80104824:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80104828:	75 ee                	jne    80104818 <exit+0x88>
8010482a:	3b 50 24             	cmp    0x24(%eax),%edx
8010482d:	75 e9                	jne    80104818 <exit+0x88>
      p->state = RUNNABLE;
8010482f:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104836:	05 94 00 00 00       	add    $0x94,%eax
8010483b:	3d 54 52 11 80       	cmp    $0x80115254,%eax
80104840:	75 e2                	jne    80104824 <exit+0x94>
      p->parent = initproc;
80104842:	8b 0d 54 52 11 80    	mov    0x80115254,%ecx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104848:	ba 54 2d 11 80       	mov    $0x80112d54,%edx
8010484d:	eb 0f                	jmp    8010485e <exit+0xce>
8010484f:	90                   	nop
80104850:	81 c2 94 00 00 00    	add    $0x94,%edx
80104856:	81 fa 54 52 11 80    	cmp    $0x80115254,%edx
8010485c:	74 3a                	je     80104898 <exit+0x108>
    if(p->parent == curproc){
8010485e:	39 5a 14             	cmp    %ebx,0x14(%edx)
80104861:	75 ed                	jne    80104850 <exit+0xc0>
      if(p->state == ZOMBIE)
80104863:	83 7a 0c 05          	cmpl   $0x5,0xc(%edx)
      p->parent = initproc;
80104867:	89 4a 14             	mov    %ecx,0x14(%edx)
      if(p->state == ZOMBIE)
8010486a:	75 e4                	jne    80104850 <exit+0xc0>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
8010486c:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80104871:	eb 11                	jmp    80104884 <exit+0xf4>
80104873:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104877:	90                   	nop
80104878:	05 94 00 00 00       	add    $0x94,%eax
8010487d:	3d 54 52 11 80       	cmp    $0x80115254,%eax
80104882:	74 cc                	je     80104850 <exit+0xc0>
    if(p->state == SLEEPING && p->chan == chan)
80104884:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80104888:	75 ee                	jne    80104878 <exit+0xe8>
8010488a:	3b 48 24             	cmp    0x24(%eax),%ecx
8010488d:	75 e9                	jne    80104878 <exit+0xe8>
      p->state = RUNNABLE;
8010488f:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
80104896:	eb e0                	jmp    80104878 <exit+0xe8>
  curproc->state = ZOMBIE;
80104898:	c7 43 0c 05 00 00 00 	movl   $0x5,0xc(%ebx)
  sched();
8010489f:	e8 2c fe ff ff       	call   801046d0 <sched>
  panic("zombie exit");
801048a4:	83 ec 0c             	sub    $0xc,%esp
801048a7:	68 68 85 10 80       	push   $0x80108568
801048ac:	e8 df ba ff ff       	call   80100390 <panic>
    panic("init exiting");
801048b1:	83 ec 0c             	sub    $0xc,%esp
801048b4:	68 5b 85 10 80       	push   $0x8010855b
801048b9:	e8 d2 ba ff ff       	call   80100390 <panic>
801048be:	66 90                	xchg   %ax,%ax

801048c0 <wait>:
{
801048c0:	f3 0f 1e fb          	endbr32 
801048c4:	55                   	push   %ebp
801048c5:	89 e5                	mov    %esp,%ebp
801048c7:	56                   	push   %esi
801048c8:	53                   	push   %ebx
  pushcli();
801048c9:	e8 22 07 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
801048ce:	e8 ad f7 ff ff       	call   80104080 <mycpu>
  p = c->proc;
801048d3:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
801048d9:	e8 62 07 00 00       	call   80105040 <popcli>
  acquire(&ptable.lock);
801048de:	83 ec 0c             	sub    $0xc,%esp
801048e1:	68 20 2d 11 80       	push   $0x80112d20
801048e6:	e8 75 08 00 00       	call   80105160 <acquire>
801048eb:	83 c4 10             	add    $0x10,%esp
    havekids = 0;
801048ee:	31 c0                	xor    %eax,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801048f0:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
801048f5:	eb 17                	jmp    8010490e <wait+0x4e>
801048f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801048fe:	66 90                	xchg   %ax,%ax
80104900:	81 c3 94 00 00 00    	add    $0x94,%ebx
80104906:	81 fb 54 52 11 80    	cmp    $0x80115254,%ebx
8010490c:	74 23                	je     80104931 <wait+0x71>
      if(p->parent != curproc && p->debuger_parent != curproc)
8010490e:	39 73 14             	cmp    %esi,0x14(%ebx)
80104911:	74 05                	je     80104918 <wait+0x58>
80104913:	39 73 18             	cmp    %esi,0x18(%ebx)
80104916:	75 e8                	jne    80104900 <wait+0x40>
      if(p->state == ZOMBIE){
80104918:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
8010491c:	74 62                	je     80104980 <wait+0xc0>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010491e:	81 c3 94 00 00 00    	add    $0x94,%ebx
      havekids = 1;
80104924:	b8 01 00 00 00       	mov    $0x1,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104929:	81 fb 54 52 11 80    	cmp    $0x80115254,%ebx
8010492f:	75 dd                	jne    8010490e <wait+0x4e>
    if(!havekids || curproc->killed){
80104931:	85 c0                	test   %eax,%eax
80104933:	0f 84 a4 00 00 00    	je     801049dd <wait+0x11d>
80104939:	8b 46 28             	mov    0x28(%esi),%eax
8010493c:	85 c0                	test   %eax,%eax
8010493e:	0f 85 99 00 00 00    	jne    801049dd <wait+0x11d>
  pushcli();
80104944:	e8 a7 06 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
80104949:	e8 32 f7 ff ff       	call   80104080 <mycpu>
  p = c->proc;
8010494e:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80104954:	e8 e7 06 00 00       	call   80105040 <popcli>
  if(p == 0)
80104959:	85 db                	test   %ebx,%ebx
8010495b:	0f 84 93 00 00 00    	je     801049f4 <wait+0x134>
  p->chan = chan;
80104961:	89 73 24             	mov    %esi,0x24(%ebx)
  p->state = SLEEPING;
80104964:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
8010496b:	e8 60 fd ff ff       	call   801046d0 <sched>
  p->chan = 0;
80104970:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
}
80104977:	e9 72 ff ff ff       	jmp    801048ee <wait+0x2e>
8010497c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        kfree(p->kstack);
80104980:	83 ec 0c             	sub    $0xc,%esp
        pid = p->pid;
80104983:	8b 73 10             	mov    0x10(%ebx),%esi
        kfree(p->kstack);
80104986:	ff 73 08             	push   0x8(%ebx)
80104989:	e8 52 e2 ff ff       	call   80102be0 <kfree>
        p->kstack = 0;
8010498e:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
        freevm(p->pgdir);
80104995:	5a                   	pop    %edx
80104996:	ff 73 04             	push   0x4(%ebx)
80104999:	e8 c2 31 00 00       	call   80107b60 <freevm>
        p->pid = 0;
8010499e:	c7 43 10 00 00 00 00 	movl   $0x0,0x10(%ebx)
        p->parent = 0;
801049a5:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
        p->debuger_parent = 0;
801049ac:	c7 43 18 00 00 00 00 	movl   $0x0,0x18(%ebx)
        p->name[0] = 0;
801049b3:	c6 43 70 00          	movb   $0x0,0x70(%ebx)
        p->killed = 0;
801049b7:	c7 43 28 00 00 00 00 	movl   $0x0,0x28(%ebx)
        p->state = UNUSED;
801049be:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
        release(&ptable.lock);
801049c5:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
801049cc:	e8 1f 07 00 00       	call   801050f0 <release>
        return pid;
801049d1:	83 c4 10             	add    $0x10,%esp
}
801049d4:	8d 65 f8             	lea    -0x8(%ebp),%esp
801049d7:	89 f0                	mov    %esi,%eax
801049d9:	5b                   	pop    %ebx
801049da:	5e                   	pop    %esi
801049db:	5d                   	pop    %ebp
801049dc:	c3                   	ret    
      release(&ptable.lock);
801049dd:	83 ec 0c             	sub    $0xc,%esp
      return -1;
801049e0:	be ff ff ff ff       	mov    $0xffffffff,%esi
      release(&ptable.lock);
801049e5:	68 20 2d 11 80       	push   $0x80112d20
801049ea:	e8 01 07 00 00       	call   801050f0 <release>
      return -1;
801049ef:	83 c4 10             	add    $0x10,%esp
801049f2:	eb e0                	jmp    801049d4 <wait+0x114>
    panic("sleep");
801049f4:	83 ec 0c             	sub    $0xc,%esp
801049f7:	68 74 85 10 80       	push   $0x80108574
801049fc:	e8 8f b9 ff ff       	call   80100390 <panic>
80104a01:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104a08:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104a0f:	90                   	nop

80104a10 <yield>:
{
80104a10:	f3 0f 1e fb          	endbr32 
80104a14:	55                   	push   %ebp
80104a15:	89 e5                	mov    %esp,%ebp
80104a17:	53                   	push   %ebx
80104a18:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
80104a1b:	68 20 2d 11 80       	push   $0x80112d20
80104a20:	e8 3b 07 00 00       	call   80105160 <acquire>
  pushcli();
80104a25:	e8 c6 05 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
80104a2a:	e8 51 f6 ff ff       	call   80104080 <mycpu>
  p = c->proc;
80104a2f:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80104a35:	e8 06 06 00 00       	call   80105040 <popcli>
  myproc()->state = RUNNABLE;
80104a3a:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  sched();
80104a41:	e8 8a fc ff ff       	call   801046d0 <sched>
  release(&ptable.lock);
80104a46:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80104a4d:	e8 9e 06 00 00       	call   801050f0 <release>
}
80104a52:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104a55:	83 c4 10             	add    $0x10,%esp
80104a58:	c9                   	leave  
80104a59:	c3                   	ret    
80104a5a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104a60 <sleep>:
{
80104a60:	f3 0f 1e fb          	endbr32 
80104a64:	55                   	push   %ebp
80104a65:	89 e5                	mov    %esp,%ebp
80104a67:	57                   	push   %edi
80104a68:	56                   	push   %esi
80104a69:	53                   	push   %ebx
80104a6a:	83 ec 0c             	sub    $0xc,%esp
80104a6d:	8b 7d 08             	mov    0x8(%ebp),%edi
80104a70:	8b 75 0c             	mov    0xc(%ebp),%esi
  pushcli();
80104a73:	e8 78 05 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
80104a78:	e8 03 f6 ff ff       	call   80104080 <mycpu>
  p = c->proc;
80104a7d:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80104a83:	e8 b8 05 00 00       	call   80105040 <popcli>
  if(p == 0)
80104a88:	85 db                	test   %ebx,%ebx
80104a8a:	0f 84 83 00 00 00    	je     80104b13 <sleep+0xb3>
  if(lk == 0)
80104a90:	85 f6                	test   %esi,%esi
80104a92:	74 72                	je     80104b06 <sleep+0xa6>
  if(lk != &ptable.lock){  //DOC: sleeplock0
80104a94:	81 fe 20 2d 11 80    	cmp    $0x80112d20,%esi
80104a9a:	74 4c                	je     80104ae8 <sleep+0x88>
    acquire(&ptable.lock);  //DOC: sleeplock1
80104a9c:	83 ec 0c             	sub    $0xc,%esp
80104a9f:	68 20 2d 11 80       	push   $0x80112d20
80104aa4:	e8 b7 06 00 00       	call   80105160 <acquire>
    release(lk);
80104aa9:	89 34 24             	mov    %esi,(%esp)
80104aac:	e8 3f 06 00 00       	call   801050f0 <release>
  p->chan = chan;
80104ab1:	89 7b 24             	mov    %edi,0x24(%ebx)
  p->state = SLEEPING;
80104ab4:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
80104abb:	e8 10 fc ff ff       	call   801046d0 <sched>
  p->chan = 0;
80104ac0:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
    release(&ptable.lock);
80104ac7:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80104ace:	e8 1d 06 00 00       	call   801050f0 <release>
    acquire(lk);
80104ad3:	89 75 08             	mov    %esi,0x8(%ebp)
80104ad6:	83 c4 10             	add    $0x10,%esp
}
80104ad9:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104adc:	5b                   	pop    %ebx
80104add:	5e                   	pop    %esi
80104ade:	5f                   	pop    %edi
80104adf:	5d                   	pop    %ebp
    acquire(lk);
80104ae0:	e9 7b 06 00 00       	jmp    80105160 <acquire>
80104ae5:	8d 76 00             	lea    0x0(%esi),%esi
  p->chan = chan;
80104ae8:	89 7b 24             	mov    %edi,0x24(%ebx)
  p->state = SLEEPING;
80104aeb:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
80104af2:	e8 d9 fb ff ff       	call   801046d0 <sched>
  p->chan = 0;
80104af7:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
}
80104afe:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104b01:	5b                   	pop    %ebx
80104b02:	5e                   	pop    %esi
80104b03:	5f                   	pop    %edi
80104b04:	5d                   	pop    %ebp
80104b05:	c3                   	ret    
    panic("sleep without lk");
80104b06:	83 ec 0c             	sub    $0xc,%esp
80104b09:	68 7a 85 10 80       	push   $0x8010857a
80104b0e:	e8 7d b8 ff ff       	call   80100390 <panic>
    panic("sleep");
80104b13:	83 ec 0c             	sub    $0xc,%esp
80104b16:	68 74 85 10 80       	push   $0x80108574
80104b1b:	e8 70 b8 ff ff       	call   80100390 <panic>

80104b20 <wakeup>:
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
80104b20:	f3 0f 1e fb          	endbr32 
80104b24:	55                   	push   %ebp
80104b25:	89 e5                	mov    %esp,%ebp
80104b27:	53                   	push   %ebx
80104b28:	83 ec 10             	sub    $0x10,%esp
80104b2b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ptable.lock);
80104b2e:	68 20 2d 11 80       	push   $0x80112d20
80104b33:	e8 28 06 00 00       	call   80105160 <acquire>
80104b38:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104b3b:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80104b40:	eb 12                	jmp    80104b54 <wakeup+0x34>
80104b42:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104b48:	05 94 00 00 00       	add    $0x94,%eax
80104b4d:	3d 54 52 11 80       	cmp    $0x80115254,%eax
80104b52:	74 1e                	je     80104b72 <wakeup+0x52>
    if(p->state == SLEEPING && p->chan == chan)
80104b54:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80104b58:	75 ee                	jne    80104b48 <wakeup+0x28>
80104b5a:	3b 58 24             	cmp    0x24(%eax),%ebx
80104b5d:	75 e9                	jne    80104b48 <wakeup+0x28>
      p->state = RUNNABLE;
80104b5f:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104b66:	05 94 00 00 00       	add    $0x94,%eax
80104b6b:	3d 54 52 11 80       	cmp    $0x80115254,%eax
80104b70:	75 e2                	jne    80104b54 <wakeup+0x34>
  wakeup1(chan);
  release(&ptable.lock);
80104b72:	c7 45 08 20 2d 11 80 	movl   $0x80112d20,0x8(%ebp)
}
80104b79:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104b7c:	c9                   	leave  
  release(&ptable.lock);
80104b7d:	e9 6e 05 00 00       	jmp    801050f0 <release>
80104b82:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104b89:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80104b90 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80104b90:	f3 0f 1e fb          	endbr32 
80104b94:	55                   	push   %ebp
80104b95:	89 e5                	mov    %esp,%ebp
80104b97:	53                   	push   %ebx
80104b98:	83 ec 10             	sub    $0x10,%esp
80104b9b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *p;

  acquire(&ptable.lock);
80104b9e:	68 20 2d 11 80       	push   $0x80112d20
80104ba3:	e8 b8 05 00 00       	call   80105160 <acquire>
80104ba8:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104bab:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80104bb0:	eb 12                	jmp    80104bc4 <kill+0x34>
80104bb2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104bb8:	05 94 00 00 00       	add    $0x94,%eax
80104bbd:	3d 54 52 11 80       	cmp    $0x80115254,%eax
80104bc2:	74 34                	je     80104bf8 <kill+0x68>
    if(p->pid == pid){
80104bc4:	39 58 10             	cmp    %ebx,0x10(%eax)
80104bc7:	75 ef                	jne    80104bb8 <kill+0x28>
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
80104bc9:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
      p->killed = 1;
80104bcd:	c7 40 28 01 00 00 00 	movl   $0x1,0x28(%eax)
      if(p->state == SLEEPING)
80104bd4:	75 07                	jne    80104bdd <kill+0x4d>
        p->state = RUNNABLE;
80104bd6:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
80104bdd:	83 ec 0c             	sub    $0xc,%esp
80104be0:	68 20 2d 11 80       	push   $0x80112d20
80104be5:	e8 06 05 00 00       	call   801050f0 <release>
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}
80104bea:	8b 5d fc             	mov    -0x4(%ebp),%ebx
      return 0;
80104bed:	83 c4 10             	add    $0x10,%esp
80104bf0:	31 c0                	xor    %eax,%eax
}
80104bf2:	c9                   	leave  
80104bf3:	c3                   	ret    
80104bf4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  release(&ptable.lock);
80104bf8:	83 ec 0c             	sub    $0xc,%esp
80104bfb:	68 20 2d 11 80       	push   $0x80112d20
80104c00:	e8 eb 04 00 00       	call   801050f0 <release>
}
80104c05:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  return -1;
80104c08:	83 c4 10             	add    $0x10,%esp
80104c0b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104c10:	c9                   	leave  
80104c11:	c3                   	ret    
80104c12:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104c19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80104c20 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
80104c20:	f3 0f 1e fb          	endbr32 
80104c24:	55                   	push   %ebp
80104c25:	89 e5                	mov    %esp,%ebp
80104c27:	57                   	push   %edi
80104c28:	56                   	push   %esi
80104c29:	8d 75 e8             	lea    -0x18(%ebp),%esi
80104c2c:	53                   	push   %ebx
80104c2d:	bb c4 2d 11 80       	mov    $0x80112dc4,%ebx
80104c32:	83 ec 3c             	sub    $0x3c,%esp
80104c35:	eb 2b                	jmp    80104c62 <procdump+0x42>
80104c37:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104c3e:	66 90                	xchg   %ax,%ax
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
80104c40:	83 ec 0c             	sub    $0xc,%esp
80104c43:	68 ff 89 10 80       	push   $0x801089ff
80104c48:	e8 e3 ba ff ff       	call   80100730 <cprintf>
80104c4d:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104c50:	81 c3 94 00 00 00    	add    $0x94,%ebx
80104c56:	81 fb c4 52 11 80    	cmp    $0x801152c4,%ebx
80104c5c:	0f 84 8e 00 00 00    	je     80104cf0 <procdump+0xd0>
    if(p->state == UNUSED)
80104c62:	8b 43 9c             	mov    -0x64(%ebx),%eax
80104c65:	85 c0                	test   %eax,%eax
80104c67:	74 e7                	je     80104c50 <procdump+0x30>
      state = "???";
80104c69:	ba 8b 85 10 80       	mov    $0x8010858b,%edx
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
80104c6e:	83 f8 05             	cmp    $0x5,%eax
80104c71:	77 11                	ja     80104c84 <procdump+0x64>
80104c73:	8b 14 85 1c 86 10 80 	mov    -0x7fef79e4(,%eax,4),%edx
      state = "???";
80104c7a:	b8 8b 85 10 80       	mov    $0x8010858b,%eax
80104c7f:	85 d2                	test   %edx,%edx
80104c81:	0f 44 d0             	cmove  %eax,%edx
    cprintf("%d %s %s", p->pid, state, p->name);
80104c84:	53                   	push   %ebx
80104c85:	52                   	push   %edx
80104c86:	ff 73 a0             	push   -0x60(%ebx)
80104c89:	68 8f 85 10 80       	push   $0x8010858f
80104c8e:	e8 9d ba ff ff       	call   80100730 <cprintf>
    if(p->state == SLEEPING){
80104c93:	83 c4 10             	add    $0x10,%esp
80104c96:	83 7b 9c 02          	cmpl   $0x2,-0x64(%ebx)
80104c9a:	75 a4                	jne    80104c40 <procdump+0x20>
      getcallerpcs((uint*)p->context->ebp+2, pc);
80104c9c:	83 ec 08             	sub    $0x8,%esp
80104c9f:	8d 45 c0             	lea    -0x40(%ebp),%eax
80104ca2:	8d 7d c0             	lea    -0x40(%ebp),%edi
80104ca5:	50                   	push   %eax
80104ca6:	8b 43 b0             	mov    -0x50(%ebx),%eax
80104ca9:	8b 40 0c             	mov    0xc(%eax),%eax
80104cac:	83 c0 08             	add    $0x8,%eax
80104caf:	50                   	push   %eax
80104cb0:	e8 cb 02 00 00       	call   80104f80 <getcallerpcs>
      for(i=0; i<10 && pc[i] != 0; i++)
80104cb5:	83 c4 10             	add    $0x10,%esp
80104cb8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104cbf:	90                   	nop
80104cc0:	8b 17                	mov    (%edi),%edx
80104cc2:	85 d2                	test   %edx,%edx
80104cc4:	0f 84 76 ff ff ff    	je     80104c40 <procdump+0x20>
        cprintf(" %p", pc[i]);
80104cca:	83 ec 08             	sub    $0x8,%esp
      for(i=0; i<10 && pc[i] != 0; i++)
80104ccd:	83 c7 04             	add    $0x4,%edi
        cprintf(" %p", pc[i]);
80104cd0:	52                   	push   %edx
80104cd1:	68 81 7f 10 80       	push   $0x80107f81
80104cd6:	e8 55 ba ff ff       	call   80100730 <cprintf>
      for(i=0; i<10 && pc[i] != 0; i++)
80104cdb:	83 c4 10             	add    $0x10,%esp
80104cde:	39 fe                	cmp    %edi,%esi
80104ce0:	75 de                	jne    80104cc0 <procdump+0xa0>
80104ce2:	e9 59 ff ff ff       	jmp    80104c40 <procdump+0x20>
80104ce7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104cee:	66 90                	xchg   %ax,%ax
  }
}
80104cf0:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104cf3:	5b                   	pop    %ebx
80104cf4:	5e                   	pop    %esi
80104cf5:	5f                   	pop    %edi
80104cf6:	5d                   	pop    %ebp
80104cf7:	c3                   	ret    
80104cf8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104cff:	90                   	nop

80104d00 <calculate_sum_of_digits>:

// calculation sum of digits
int 
calculate_sum_of_digits(int n)    
{    
80104d00:	f3 0f 1e fb          	endbr32 
80104d04:	55                   	push   %ebp
80104d05:	89 e5                	mov    %esp,%ebp
80104d07:	57                   	push   %edi
80104d08:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104d0b:	56                   	push   %esi
80104d0c:	53                   	push   %ebx
  int sum=0,m;     
80104d0d:	31 db                	xor    %ebx,%ebx
  while(n>0)    
80104d0f:	85 c9                	test   %ecx,%ecx
80104d11:	7e 28                	jle    80104d3b <calculate_sum_of_digits+0x3b>
  {    
    m=n%10;    
80104d13:	be cd cc cc cc       	mov    $0xcccccccd,%esi
80104d18:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104d1f:	90                   	nop
80104d20:	89 c8                	mov    %ecx,%eax
80104d22:	89 cf                	mov    %ecx,%edi
80104d24:	f7 e6                	mul    %esi
80104d26:	c1 ea 03             	shr    $0x3,%edx
80104d29:	8d 04 92             	lea    (%edx,%edx,4),%eax
80104d2c:	01 c0                	add    %eax,%eax
80104d2e:	29 c7                	sub    %eax,%edi
80104d30:	89 c8                	mov    %ecx,%eax
    sum=sum+m;    
    n=n/10;    
80104d32:	89 d1                	mov    %edx,%ecx
    sum=sum+m;    
80104d34:	01 fb                	add    %edi,%ebx
  while(n>0)    
80104d36:	83 f8 09             	cmp    $0x9,%eax
80104d39:	7f e5                	jg     80104d20 <calculate_sum_of_digits+0x20>
  }    
  return sum;
}
80104d3b:	89 d8                	mov    %ebx,%eax
80104d3d:	5b                   	pop    %ebx
80104d3e:	5e                   	pop    %esi
80104d3f:	5f                   	pop    %edi
80104d40:	5d                   	pop    %ebp
80104d41:	c3                   	ret    
80104d42:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104d49:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80104d50 <get_parent_pid>:

// get parent pid
void 
get_parent_pid(int pid)
{
80104d50:	f3 0f 1e fb          	endbr32 
80104d54:	55                   	push   %ebp
80104d55:	89 e5                	mov    %esp,%ebp
80104d57:	53                   	push   %ebx
80104d58:	83 ec 10             	sub    $0x10,%esp
80104d5b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *procces;
  acquire(&ptable.lock);
80104d5e:	68 20 2d 11 80       	push   $0x80112d20
80104d63:	e8 f8 03 00 00       	call   80105160 <acquire>
80104d68:	83 c4 10             	add    $0x10,%esp
    for(procces = ptable.proc; procces < &ptable.proc[NPROC]; procces++){ 
80104d6b:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80104d70:	eb 12                	jmp    80104d84 <get_parent_pid+0x34>
80104d72:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104d78:	05 94 00 00 00       	add    $0x94,%eax
80104d7d:	3d 54 52 11 80       	cmp    $0x80115254,%eax
80104d82:	74 05                	je     80104d89 <get_parent_pid+0x39>
      if (procces->pid == pid)
80104d84:	39 58 10             	cmp    %ebx,0x10(%eax)
80104d87:	75 ef                	jne    80104d78 <get_parent_pid+0x28>
        break;
    }
  cprintf("currnet procces id is : %d , parent id is : %d\n" ,pid , procces->parent->pid);
80104d89:	8b 40 14             	mov    0x14(%eax),%eax
80104d8c:	83 ec 04             	sub    $0x4,%esp
80104d8f:	ff 70 10             	push   0x10(%eax)
80104d92:	53                   	push   %ebx
80104d93:	68 ec 85 10 80       	push   $0x801085ec
80104d98:	e8 93 b9 ff ff       	call   80100730 <cprintf>
  release(&ptable.lock);
80104d9d:	c7 45 08 20 2d 11 80 	movl   $0x80112d20,0x8(%ebp)
}
80104da4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  release(&ptable.lock);
80104da7:	83 c4 10             	add    $0x10,%esp
}
80104daa:	c9                   	leave  
  release(&ptable.lock);
80104dab:	e9 40 03 00 00       	jmp    801050f0 <release>

80104db0 <set_process_parent>:

int
set_process_parent(int pid)
{
80104db0:	f3 0f 1e fb          	endbr32 
80104db4:	55                   	push   %ebp
80104db5:	89 e5                	mov    %esp,%ebp
80104db7:	56                   	push   %esi
80104db8:	53                   	push   %ebx
80104db9:	8b 75 08             	mov    0x8(%ebp),%esi
  struct proc *process;
  acquire(&ptable.lock);
  for(process = ptable.proc; process<&ptable.proc[NPROC]; process++){
80104dbc:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
  acquire(&ptable.lock);
80104dc1:	83 ec 0c             	sub    $0xc,%esp
80104dc4:	68 20 2d 11 80       	push   $0x80112d20
80104dc9:	e8 92 03 00 00       	call   80105160 <acquire>
80104dce:	83 c4 10             	add    $0x10,%esp
80104dd1:	eb 13                	jmp    80104de6 <set_process_parent+0x36>
80104dd3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104dd7:	90                   	nop
  for(process = ptable.proc; process<&ptable.proc[NPROC]; process++){
80104dd8:	81 c3 94 00 00 00    	add    $0x94,%ebx
80104dde:	81 fb 54 52 11 80    	cmp    $0x80115254,%ebx
80104de4:	74 05                	je     80104deb <set_process_parent+0x3b>
    if(process->pid == pid)
80104de6:	39 73 10             	cmp    %esi,0x10(%ebx)
80104de9:	75 ed                	jne    80104dd8 <set_process_parent+0x28>
      break;
  }
  release(&ptable.lock);
80104deb:	83 ec 0c             	sub    $0xc,%esp
80104dee:	68 20 2d 11 80       	push   $0x80112d20
80104df3:	e8 f8 02 00 00       	call   801050f0 <release>
  pushcli();
80104df8:	e8 f3 01 00 00       	call   80104ff0 <pushcli>
  c = mycpu();
80104dfd:	e8 7e f2 ff ff       	call   80104080 <mycpu>
  p = c->proc;
80104e02:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
80104e08:	e8 33 02 00 00       	call   80105040 <popcli>
  process->debuger_parent = myproc();
  return 0;
}
80104e0d:	31 c0                	xor    %eax,%eax
  process->debuger_parent = myproc();
80104e0f:	89 73 18             	mov    %esi,0x18(%ebx)
}
80104e12:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104e15:	5b                   	pop    %ebx
80104e16:	5e                   	pop    %esi
80104e17:	5d                   	pop    %ebp
80104e18:	c3                   	ret    
80104e19:	66 90                	xchg   %ax,%ax
80104e1b:	66 90                	xchg   %ax,%ax
80104e1d:	66 90                	xchg   %ax,%ax
80104e1f:	90                   	nop

80104e20 <initsleeplock>:
#include "spinlock.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
80104e20:	f3 0f 1e fb          	endbr32 
80104e24:	55                   	push   %ebp
80104e25:	89 e5                	mov    %esp,%ebp
80104e27:	53                   	push   %ebx
80104e28:	83 ec 0c             	sub    $0xc,%esp
80104e2b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&lk->lk, "sleep lock");
80104e2e:	68 3c 86 10 80       	push   $0x8010863c
80104e33:	8d 43 04             	lea    0x4(%ebx),%eax
80104e36:	50                   	push   %eax
80104e37:	e8 24 01 00 00       	call   80104f60 <initlock>
  lk->name = name;
80104e3c:	8b 45 0c             	mov    0xc(%ebp),%eax
  lk->locked = 0;
80104e3f:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
}
80104e45:	83 c4 10             	add    $0x10,%esp
  lk->pid = 0;
80104e48:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  lk->name = name;
80104e4f:	89 43 38             	mov    %eax,0x38(%ebx)
}
80104e52:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104e55:	c9                   	leave  
80104e56:	c3                   	ret    
80104e57:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104e5e:	66 90                	xchg   %ax,%ax

80104e60 <acquiresleep>:

void
acquiresleep(struct sleeplock *lk)
{
80104e60:	f3 0f 1e fb          	endbr32 
80104e64:	55                   	push   %ebp
80104e65:	89 e5                	mov    %esp,%ebp
80104e67:	56                   	push   %esi
80104e68:	53                   	push   %ebx
80104e69:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
80104e6c:	8d 73 04             	lea    0x4(%ebx),%esi
80104e6f:	83 ec 0c             	sub    $0xc,%esp
80104e72:	56                   	push   %esi
80104e73:	e8 e8 02 00 00       	call   80105160 <acquire>
  while (lk->locked) {
80104e78:	8b 13                	mov    (%ebx),%edx
80104e7a:	83 c4 10             	add    $0x10,%esp
80104e7d:	85 d2                	test   %edx,%edx
80104e7f:	74 1a                	je     80104e9b <acquiresleep+0x3b>
80104e81:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    sleep(lk, &lk->lk);
80104e88:	83 ec 08             	sub    $0x8,%esp
80104e8b:	56                   	push   %esi
80104e8c:	53                   	push   %ebx
80104e8d:	e8 ce fb ff ff       	call   80104a60 <sleep>
  while (lk->locked) {
80104e92:	8b 03                	mov    (%ebx),%eax
80104e94:	83 c4 10             	add    $0x10,%esp
80104e97:	85 c0                	test   %eax,%eax
80104e99:	75 ed                	jne    80104e88 <acquiresleep+0x28>
  }
  lk->locked = 1;
80104e9b:	c7 03 01 00 00 00    	movl   $0x1,(%ebx)
  lk->pid = myproc()->pid;
80104ea1:	e8 6a f2 ff ff       	call   80104110 <myproc>
80104ea6:	8b 40 10             	mov    0x10(%eax),%eax
80104ea9:	89 43 3c             	mov    %eax,0x3c(%ebx)
  release(&lk->lk);
80104eac:	89 75 08             	mov    %esi,0x8(%ebp)
}
80104eaf:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104eb2:	5b                   	pop    %ebx
80104eb3:	5e                   	pop    %esi
80104eb4:	5d                   	pop    %ebp
  release(&lk->lk);
80104eb5:	e9 36 02 00 00       	jmp    801050f0 <release>
80104eba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104ec0 <releasesleep>:

void
releasesleep(struct sleeplock *lk)
{
80104ec0:	f3 0f 1e fb          	endbr32 
80104ec4:	55                   	push   %ebp
80104ec5:	89 e5                	mov    %esp,%ebp
80104ec7:	56                   	push   %esi
80104ec8:	53                   	push   %ebx
80104ec9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
80104ecc:	8d 73 04             	lea    0x4(%ebx),%esi
80104ecf:	83 ec 0c             	sub    $0xc,%esp
80104ed2:	56                   	push   %esi
80104ed3:	e8 88 02 00 00       	call   80105160 <acquire>
  lk->locked = 0;
80104ed8:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
80104ede:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  wakeup(lk);
80104ee5:	89 1c 24             	mov    %ebx,(%esp)
80104ee8:	e8 33 fc ff ff       	call   80104b20 <wakeup>
  release(&lk->lk);
80104eed:	89 75 08             	mov    %esi,0x8(%ebp)
80104ef0:	83 c4 10             	add    $0x10,%esp
}
80104ef3:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104ef6:	5b                   	pop    %ebx
80104ef7:	5e                   	pop    %esi
80104ef8:	5d                   	pop    %ebp
  release(&lk->lk);
80104ef9:	e9 f2 01 00 00       	jmp    801050f0 <release>
80104efe:	66 90                	xchg   %ax,%ax

80104f00 <holdingsleep>:

int
holdingsleep(struct sleeplock *lk)
{
80104f00:	f3 0f 1e fb          	endbr32 
80104f04:	55                   	push   %ebp
80104f05:	89 e5                	mov    %esp,%ebp
80104f07:	57                   	push   %edi
80104f08:	31 ff                	xor    %edi,%edi
80104f0a:	56                   	push   %esi
80104f0b:	53                   	push   %ebx
80104f0c:	83 ec 18             	sub    $0x18,%esp
80104f0f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;
  
  acquire(&lk->lk);
80104f12:	8d 73 04             	lea    0x4(%ebx),%esi
80104f15:	56                   	push   %esi
80104f16:	e8 45 02 00 00       	call   80105160 <acquire>
  r = lk->locked && (lk->pid == myproc()->pid);
80104f1b:	8b 03                	mov    (%ebx),%eax
80104f1d:	83 c4 10             	add    $0x10,%esp
80104f20:	85 c0                	test   %eax,%eax
80104f22:	75 1c                	jne    80104f40 <holdingsleep+0x40>
  release(&lk->lk);
80104f24:	83 ec 0c             	sub    $0xc,%esp
80104f27:	56                   	push   %esi
80104f28:	e8 c3 01 00 00       	call   801050f0 <release>
  return r;
}
80104f2d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104f30:	89 f8                	mov    %edi,%eax
80104f32:	5b                   	pop    %ebx
80104f33:	5e                   	pop    %esi
80104f34:	5f                   	pop    %edi
80104f35:	5d                   	pop    %ebp
80104f36:	c3                   	ret    
80104f37:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104f3e:	66 90                	xchg   %ax,%ax
  r = lk->locked && (lk->pid == myproc()->pid);
80104f40:	8b 5b 3c             	mov    0x3c(%ebx),%ebx
80104f43:	e8 c8 f1 ff ff       	call   80104110 <myproc>
80104f48:	39 58 10             	cmp    %ebx,0x10(%eax)
80104f4b:	0f 94 c0             	sete   %al
80104f4e:	0f b6 c0             	movzbl %al,%eax
80104f51:	89 c7                	mov    %eax,%edi
80104f53:	eb cf                	jmp    80104f24 <holdingsleep+0x24>
80104f55:	66 90                	xchg   %ax,%ax
80104f57:	66 90                	xchg   %ax,%ax
80104f59:	66 90                	xchg   %ax,%ax
80104f5b:	66 90                	xchg   %ax,%ax
80104f5d:	66 90                	xchg   %ax,%ax
80104f5f:	90                   	nop

80104f60 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
80104f60:	f3 0f 1e fb          	endbr32 
80104f64:	55                   	push   %ebp
80104f65:	89 e5                	mov    %esp,%ebp
80104f67:	8b 45 08             	mov    0x8(%ebp),%eax
  lk->name = name;
80104f6a:	8b 55 0c             	mov    0xc(%ebp),%edx
  lk->locked = 0;
80104f6d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->name = name;
80104f73:	89 50 04             	mov    %edx,0x4(%eax)
  lk->cpu = 0;
80104f76:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
80104f7d:	5d                   	pop    %ebp
80104f7e:	c3                   	ret    
80104f7f:	90                   	nop

80104f80 <getcallerpcs>:
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
80104f80:	f3 0f 1e fb          	endbr32 
80104f84:	55                   	push   %ebp
  uint *ebp;
  int i;

  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
80104f85:	31 d2                	xor    %edx,%edx
{
80104f87:	89 e5                	mov    %esp,%ebp
80104f89:	53                   	push   %ebx
  ebp = (uint*)v - 2;
80104f8a:	8b 45 08             	mov    0x8(%ebp),%eax
{
80104f8d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  ebp = (uint*)v - 2;
80104f90:	83 e8 08             	sub    $0x8,%eax
  for(i = 0; i < 10; i++){
80104f93:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104f97:	90                   	nop
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104f98:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
80104f9e:	81 fb fe ff ff 7f    	cmp    $0x7ffffffe,%ebx
80104fa4:	77 1a                	ja     80104fc0 <getcallerpcs+0x40>
      break;
    pcs[i] = ebp[1];     // saved %eip
80104fa6:	8b 58 04             	mov    0x4(%eax),%ebx
80104fa9:	89 1c 91             	mov    %ebx,(%ecx,%edx,4)
  for(i = 0; i < 10; i++){
80104fac:	83 c2 01             	add    $0x1,%edx
    ebp = (uint*)ebp[0]; // saved %ebp
80104faf:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
80104fb1:	83 fa 0a             	cmp    $0xa,%edx
80104fb4:	75 e2                	jne    80104f98 <getcallerpcs+0x18>
  }
  for(; i < 10; i++)
    pcs[i] = 0;
}
80104fb6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104fb9:	c9                   	leave  
80104fba:	c3                   	ret    
80104fbb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104fbf:	90                   	nop
  for(; i < 10; i++)
80104fc0:	8d 04 91             	lea    (%ecx,%edx,4),%eax
80104fc3:	8d 51 28             	lea    0x28(%ecx),%edx
80104fc6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104fcd:	8d 76 00             	lea    0x0(%esi),%esi
    pcs[i] = 0;
80104fd0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  for(; i < 10; i++)
80104fd6:	83 c0 04             	add    $0x4,%eax
80104fd9:	39 d0                	cmp    %edx,%eax
80104fdb:	75 f3                	jne    80104fd0 <getcallerpcs+0x50>
}
80104fdd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104fe0:	c9                   	leave  
80104fe1:	c3                   	ret    
80104fe2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104fe9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80104ff0 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80104ff0:	f3 0f 1e fb          	endbr32 
80104ff4:	55                   	push   %ebp
80104ff5:	89 e5                	mov    %esp,%ebp
80104ff7:	53                   	push   %ebx
80104ff8:	83 ec 04             	sub    $0x4,%esp
80104ffb:	9c                   	pushf  
80104ffc:	5b                   	pop    %ebx
  asm volatile("cli");
80104ffd:	fa                   	cli    
  int eflags;

  eflags = readeflags();
  cli();
  if(mycpu()->ncli == 0)
80104ffe:	e8 7d f0 ff ff       	call   80104080 <mycpu>
80105003:	8b 80 a4 00 00 00    	mov    0xa4(%eax),%eax
80105009:	85 c0                	test   %eax,%eax
8010500b:	74 13                	je     80105020 <pushcli+0x30>
    mycpu()->intena = eflags & FL_IF;
  mycpu()->ncli += 1;
8010500d:	e8 6e f0 ff ff       	call   80104080 <mycpu>
80105012:	83 80 a4 00 00 00 01 	addl   $0x1,0xa4(%eax)
}
80105019:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010501c:	c9                   	leave  
8010501d:	c3                   	ret    
8010501e:	66 90                	xchg   %ax,%ax
    mycpu()->intena = eflags & FL_IF;
80105020:	e8 5b f0 ff ff       	call   80104080 <mycpu>
80105025:	81 e3 00 02 00 00    	and    $0x200,%ebx
8010502b:	89 98 a8 00 00 00    	mov    %ebx,0xa8(%eax)
80105031:	eb da                	jmp    8010500d <pushcli+0x1d>
80105033:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010503a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80105040 <popcli>:

void
popcli(void)
{
80105040:	f3 0f 1e fb          	endbr32 
80105044:	55                   	push   %ebp
80105045:	89 e5                	mov    %esp,%ebp
80105047:	83 ec 08             	sub    $0x8,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
8010504a:	9c                   	pushf  
8010504b:	58                   	pop    %eax
  if(readeflags()&FL_IF)
8010504c:	f6 c4 02             	test   $0x2,%ah
8010504f:	75 31                	jne    80105082 <popcli+0x42>
    panic("popcli - interruptible");
  if(--mycpu()->ncli < 0)
80105051:	e8 2a f0 ff ff       	call   80104080 <mycpu>
80105056:	83 a8 a4 00 00 00 01 	subl   $0x1,0xa4(%eax)
8010505d:	78 30                	js     8010508f <popcli+0x4f>
    panic("popcli");
  if(mycpu()->ncli == 0 && mycpu()->intena)
8010505f:	e8 1c f0 ff ff       	call   80104080 <mycpu>
80105064:	8b 90 a4 00 00 00    	mov    0xa4(%eax),%edx
8010506a:	85 d2                	test   %edx,%edx
8010506c:	74 02                	je     80105070 <popcli+0x30>
    sti();
}
8010506e:	c9                   	leave  
8010506f:	c3                   	ret    
  if(mycpu()->ncli == 0 && mycpu()->intena)
80105070:	e8 0b f0 ff ff       	call   80104080 <mycpu>
80105075:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
8010507b:	85 c0                	test   %eax,%eax
8010507d:	74 ef                	je     8010506e <popcli+0x2e>
  asm volatile("sti");
8010507f:	fb                   	sti    
}
80105080:	c9                   	leave  
80105081:	c3                   	ret    
    panic("popcli - interruptible");
80105082:	83 ec 0c             	sub    $0xc,%esp
80105085:	68 47 86 10 80       	push   $0x80108647
8010508a:	e8 01 b3 ff ff       	call   80100390 <panic>
    panic("popcli");
8010508f:	83 ec 0c             	sub    $0xc,%esp
80105092:	68 5e 86 10 80       	push   $0x8010865e
80105097:	e8 f4 b2 ff ff       	call   80100390 <panic>
8010509c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801050a0 <holding>:
{
801050a0:	f3 0f 1e fb          	endbr32 
801050a4:	55                   	push   %ebp
801050a5:	89 e5                	mov    %esp,%ebp
801050a7:	56                   	push   %esi
801050a8:	53                   	push   %ebx
801050a9:	8b 75 08             	mov    0x8(%ebp),%esi
801050ac:	31 db                	xor    %ebx,%ebx
  pushcli();
801050ae:	e8 3d ff ff ff       	call   80104ff0 <pushcli>
  r = lock->locked && lock->cpu == mycpu();
801050b3:	8b 06                	mov    (%esi),%eax
801050b5:	85 c0                	test   %eax,%eax
801050b7:	75 0f                	jne    801050c8 <holding+0x28>
  popcli();
801050b9:	e8 82 ff ff ff       	call   80105040 <popcli>
}
801050be:	89 d8                	mov    %ebx,%eax
801050c0:	5b                   	pop    %ebx
801050c1:	5e                   	pop    %esi
801050c2:	5d                   	pop    %ebp
801050c3:	c3                   	ret    
801050c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  r = lock->locked && lock->cpu == mycpu();
801050c8:	8b 5e 08             	mov    0x8(%esi),%ebx
801050cb:	e8 b0 ef ff ff       	call   80104080 <mycpu>
801050d0:	39 c3                	cmp    %eax,%ebx
801050d2:	0f 94 c3             	sete   %bl
  popcli();
801050d5:	e8 66 ff ff ff       	call   80105040 <popcli>
  r = lock->locked && lock->cpu == mycpu();
801050da:	0f b6 db             	movzbl %bl,%ebx
}
801050dd:	89 d8                	mov    %ebx,%eax
801050df:	5b                   	pop    %ebx
801050e0:	5e                   	pop    %esi
801050e1:	5d                   	pop    %ebp
801050e2:	c3                   	ret    
801050e3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801050ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801050f0 <release>:
{
801050f0:	f3 0f 1e fb          	endbr32 
801050f4:	55                   	push   %ebp
801050f5:	89 e5                	mov    %esp,%ebp
801050f7:	56                   	push   %esi
801050f8:	53                   	push   %ebx
801050f9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  pushcli();
801050fc:	e8 ef fe ff ff       	call   80104ff0 <pushcli>
  r = lock->locked && lock->cpu == mycpu();
80105101:	8b 03                	mov    (%ebx),%eax
80105103:	85 c0                	test   %eax,%eax
80105105:	75 19                	jne    80105120 <release+0x30>
  popcli();
80105107:	e8 34 ff ff ff       	call   80105040 <popcli>
    panic("release");
8010510c:	83 ec 0c             	sub    $0xc,%esp
8010510f:	68 65 86 10 80       	push   $0x80108665
80105114:	e8 77 b2 ff ff       	call   80100390 <panic>
80105119:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  r = lock->locked && lock->cpu == mycpu();
80105120:	8b 73 08             	mov    0x8(%ebx),%esi
80105123:	e8 58 ef ff ff       	call   80104080 <mycpu>
80105128:	39 c6                	cmp    %eax,%esi
8010512a:	75 db                	jne    80105107 <release+0x17>
  popcli();
8010512c:	e8 0f ff ff ff       	call   80105040 <popcli>
  lk->pcs[0] = 0;
80105131:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
  lk->cpu = 0;
80105138:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
  __sync_synchronize();
8010513f:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );
80105144:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
}
8010514a:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010514d:	5b                   	pop    %ebx
8010514e:	5e                   	pop    %esi
8010514f:	5d                   	pop    %ebp
  popcli();
80105150:	e9 eb fe ff ff       	jmp    80105040 <popcli>
80105155:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010515c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105160 <acquire>:
{
80105160:	f3 0f 1e fb          	endbr32 
80105164:	55                   	push   %ebp
80105165:	89 e5                	mov    %esp,%ebp
80105167:	53                   	push   %ebx
80105168:	83 ec 04             	sub    $0x4,%esp
  pushcli(); // disable interrupts to avoid deadlock.
8010516b:	e8 80 fe ff ff       	call   80104ff0 <pushcli>
  if(holding(lk))
80105170:	8b 5d 08             	mov    0x8(%ebp),%ebx
  pushcli();
80105173:	e8 78 fe ff ff       	call   80104ff0 <pushcli>
  r = lock->locked && lock->cpu == mycpu();
80105178:	8b 03                	mov    (%ebx),%eax
8010517a:	85 c0                	test   %eax,%eax
8010517c:	0f 85 86 00 00 00    	jne    80105208 <acquire+0xa8>
  popcli();
80105182:	e8 b9 fe ff ff       	call   80105040 <popcli>
  asm volatile("lock; xchgl %0, %1" :
80105187:	b9 01 00 00 00       	mov    $0x1,%ecx
8010518c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  while(xchg(&lk->locked, 1) != 0)
80105190:	8b 55 08             	mov    0x8(%ebp),%edx
80105193:	89 c8                	mov    %ecx,%eax
80105195:	f0 87 02             	lock xchg %eax,(%edx)
80105198:	85 c0                	test   %eax,%eax
8010519a:	75 f4                	jne    80105190 <acquire+0x30>
  __sync_synchronize();
8010519c:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  lk->cpu = mycpu();
801051a1:	8b 5d 08             	mov    0x8(%ebp),%ebx
801051a4:	e8 d7 ee ff ff       	call   80104080 <mycpu>
  getcallerpcs(&lk, lk->pcs);
801051a9:	8b 4d 08             	mov    0x8(%ebp),%ecx
  ebp = (uint*)v - 2;
801051ac:	89 ea                	mov    %ebp,%edx
  lk->cpu = mycpu();
801051ae:	89 43 08             	mov    %eax,0x8(%ebx)
  for(i = 0; i < 10; i++){
801051b1:	31 c0                	xor    %eax,%eax
801051b3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801051b7:	90                   	nop
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
801051b8:	8d 9a 00 00 00 80    	lea    -0x80000000(%edx),%ebx
801051be:	81 fb fe ff ff 7f    	cmp    $0x7ffffffe,%ebx
801051c4:	77 1a                	ja     801051e0 <acquire+0x80>
    pcs[i] = ebp[1];     // saved %eip
801051c6:	8b 5a 04             	mov    0x4(%edx),%ebx
801051c9:	89 5c 81 0c          	mov    %ebx,0xc(%ecx,%eax,4)
  for(i = 0; i < 10; i++){
801051cd:	83 c0 01             	add    $0x1,%eax
    ebp = (uint*)ebp[0]; // saved %ebp
801051d0:	8b 12                	mov    (%edx),%edx
  for(i = 0; i < 10; i++){
801051d2:	83 f8 0a             	cmp    $0xa,%eax
801051d5:	75 e1                	jne    801051b8 <acquire+0x58>
}
801051d7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801051da:	c9                   	leave  
801051db:	c3                   	ret    
801051dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(; i < 10; i++)
801051e0:	8d 44 81 0c          	lea    0xc(%ecx,%eax,4),%eax
801051e4:	8d 51 34             	lea    0x34(%ecx),%edx
801051e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801051ee:	66 90                	xchg   %ax,%ax
    pcs[i] = 0;
801051f0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  for(; i < 10; i++)
801051f6:	83 c0 04             	add    $0x4,%eax
801051f9:	39 c2                	cmp    %eax,%edx
801051fb:	75 f3                	jne    801051f0 <acquire+0x90>
}
801051fd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105200:	c9                   	leave  
80105201:	c3                   	ret    
80105202:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  r = lock->locked && lock->cpu == mycpu();
80105208:	8b 5b 08             	mov    0x8(%ebx),%ebx
8010520b:	e8 70 ee ff ff       	call   80104080 <mycpu>
80105210:	39 c3                	cmp    %eax,%ebx
80105212:	0f 85 6a ff ff ff    	jne    80105182 <acquire+0x22>
  popcli();
80105218:	e8 23 fe ff ff       	call   80105040 <popcli>
    panic("acquire");
8010521d:	83 ec 0c             	sub    $0xc,%esp
80105220:	68 6d 86 10 80       	push   $0x8010866d
80105225:	e8 66 b1 ff ff       	call   80100390 <panic>
8010522a:	66 90                	xchg   %ax,%ax
8010522c:	66 90                	xchg   %ax,%ax
8010522e:	66 90                	xchg   %ax,%ax

80105230 <memset>:
80105230:	f3 0f 1e fb          	endbr32 
80105234:	55                   	push   %ebp
80105235:	89 e5                	mov    %esp,%ebp
80105237:	57                   	push   %edi
80105238:	8b 55 08             	mov    0x8(%ebp),%edx
8010523b:	8b 4d 10             	mov    0x10(%ebp),%ecx
8010523e:	53                   	push   %ebx
8010523f:	8b 45 0c             	mov    0xc(%ebp),%eax
80105242:	89 d7                	mov    %edx,%edi
80105244:	09 cf                	or     %ecx,%edi
80105246:	83 e7 03             	and    $0x3,%edi
80105249:	75 25                	jne    80105270 <memset+0x40>
8010524b:	0f b6 f8             	movzbl %al,%edi
8010524e:	c1 e0 18             	shl    $0x18,%eax
80105251:	89 fb                	mov    %edi,%ebx
80105253:	c1 e9 02             	shr    $0x2,%ecx
80105256:	c1 e3 10             	shl    $0x10,%ebx
80105259:	09 d8                	or     %ebx,%eax
8010525b:	09 f8                	or     %edi,%eax
8010525d:	c1 e7 08             	shl    $0x8,%edi
80105260:	09 f8                	or     %edi,%eax
80105262:	89 d7                	mov    %edx,%edi
80105264:	fc                   	cld    
80105265:	f3 ab                	rep stos %eax,%es:(%edi)
80105267:	5b                   	pop    %ebx
80105268:	89 d0                	mov    %edx,%eax
8010526a:	5f                   	pop    %edi
8010526b:	5d                   	pop    %ebp
8010526c:	c3                   	ret    
8010526d:	8d 76 00             	lea    0x0(%esi),%esi
80105270:	89 d7                	mov    %edx,%edi
80105272:	fc                   	cld    
80105273:	f3 aa                	rep stos %al,%es:(%edi)
80105275:	5b                   	pop    %ebx
80105276:	89 d0                	mov    %edx,%eax
80105278:	5f                   	pop    %edi
80105279:	5d                   	pop    %ebp
8010527a:	c3                   	ret    
8010527b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010527f:	90                   	nop

80105280 <memcmp>:
80105280:	f3 0f 1e fb          	endbr32 
80105284:	55                   	push   %ebp
80105285:	89 e5                	mov    %esp,%ebp
80105287:	56                   	push   %esi
80105288:	8b 75 10             	mov    0x10(%ebp),%esi
8010528b:	8b 55 08             	mov    0x8(%ebp),%edx
8010528e:	53                   	push   %ebx
8010528f:	8b 45 0c             	mov    0xc(%ebp),%eax
80105292:	85 f6                	test   %esi,%esi
80105294:	74 2a                	je     801052c0 <memcmp+0x40>
80105296:	01 c6                	add    %eax,%esi
80105298:	eb 10                	jmp    801052aa <memcmp+0x2a>
8010529a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801052a0:	83 c0 01             	add    $0x1,%eax
801052a3:	83 c2 01             	add    $0x1,%edx
801052a6:	39 f0                	cmp    %esi,%eax
801052a8:	74 16                	je     801052c0 <memcmp+0x40>
801052aa:	0f b6 0a             	movzbl (%edx),%ecx
801052ad:	0f b6 18             	movzbl (%eax),%ebx
801052b0:	38 d9                	cmp    %bl,%cl
801052b2:	74 ec                	je     801052a0 <memcmp+0x20>
801052b4:	0f b6 c1             	movzbl %cl,%eax
801052b7:	29 d8                	sub    %ebx,%eax
801052b9:	5b                   	pop    %ebx
801052ba:	5e                   	pop    %esi
801052bb:	5d                   	pop    %ebp
801052bc:	c3                   	ret    
801052bd:	8d 76 00             	lea    0x0(%esi),%esi
801052c0:	5b                   	pop    %ebx
801052c1:	31 c0                	xor    %eax,%eax
801052c3:	5e                   	pop    %esi
801052c4:	5d                   	pop    %ebp
801052c5:	c3                   	ret    
801052c6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801052cd:	8d 76 00             	lea    0x0(%esi),%esi

801052d0 <memmove>:
801052d0:	f3 0f 1e fb          	endbr32 
801052d4:	55                   	push   %ebp
801052d5:	89 e5                	mov    %esp,%ebp
801052d7:	57                   	push   %edi
801052d8:	8b 55 08             	mov    0x8(%ebp),%edx
801052db:	8b 4d 10             	mov    0x10(%ebp),%ecx
801052de:	56                   	push   %esi
801052df:	8b 75 0c             	mov    0xc(%ebp),%esi
801052e2:	39 d6                	cmp    %edx,%esi
801052e4:	73 2a                	jae    80105310 <memmove+0x40>
801052e6:	8d 3c 0e             	lea    (%esi,%ecx,1),%edi
801052e9:	39 fa                	cmp    %edi,%edx
801052eb:	73 23                	jae    80105310 <memmove+0x40>
801052ed:	8d 41 ff             	lea    -0x1(%ecx),%eax
801052f0:	85 c9                	test   %ecx,%ecx
801052f2:	74 10                	je     80105304 <memmove+0x34>
801052f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801052f8:	0f b6 0c 06          	movzbl (%esi,%eax,1),%ecx
801052fc:	88 0c 02             	mov    %cl,(%edx,%eax,1)
801052ff:	83 e8 01             	sub    $0x1,%eax
80105302:	73 f4                	jae    801052f8 <memmove+0x28>
80105304:	5e                   	pop    %esi
80105305:	89 d0                	mov    %edx,%eax
80105307:	5f                   	pop    %edi
80105308:	5d                   	pop    %ebp
80105309:	c3                   	ret    
8010530a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105310:	8d 04 0e             	lea    (%esi,%ecx,1),%eax
80105313:	89 d7                	mov    %edx,%edi
80105315:	85 c9                	test   %ecx,%ecx
80105317:	74 eb                	je     80105304 <memmove+0x34>
80105319:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105320:	a4                   	movsb  %ds:(%esi),%es:(%edi)
80105321:	39 c6                	cmp    %eax,%esi
80105323:	75 fb                	jne    80105320 <memmove+0x50>
80105325:	5e                   	pop    %esi
80105326:	89 d0                	mov    %edx,%eax
80105328:	5f                   	pop    %edi
80105329:	5d                   	pop    %ebp
8010532a:	c3                   	ret    
8010532b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010532f:	90                   	nop

80105330 <memcpy>:
80105330:	f3 0f 1e fb          	endbr32 
80105334:	eb 9a                	jmp    801052d0 <memmove>
80105336:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010533d:	8d 76 00             	lea    0x0(%esi),%esi

80105340 <strncmp>:
80105340:	f3 0f 1e fb          	endbr32 
80105344:	55                   	push   %ebp
80105345:	89 e5                	mov    %esp,%ebp
80105347:	56                   	push   %esi
80105348:	8b 75 10             	mov    0x10(%ebp),%esi
8010534b:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010534e:	53                   	push   %ebx
8010534f:	8b 45 0c             	mov    0xc(%ebp),%eax
80105352:	85 f6                	test   %esi,%esi
80105354:	74 32                	je     80105388 <strncmp+0x48>
80105356:	01 c6                	add    %eax,%esi
80105358:	eb 14                	jmp    8010536e <strncmp+0x2e>
8010535a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105360:	38 da                	cmp    %bl,%dl
80105362:	75 14                	jne    80105378 <strncmp+0x38>
80105364:	83 c0 01             	add    $0x1,%eax
80105367:	83 c1 01             	add    $0x1,%ecx
8010536a:	39 f0                	cmp    %esi,%eax
8010536c:	74 1a                	je     80105388 <strncmp+0x48>
8010536e:	0f b6 11             	movzbl (%ecx),%edx
80105371:	0f b6 18             	movzbl (%eax),%ebx
80105374:	84 d2                	test   %dl,%dl
80105376:	75 e8                	jne    80105360 <strncmp+0x20>
80105378:	0f b6 c2             	movzbl %dl,%eax
8010537b:	29 d8                	sub    %ebx,%eax
8010537d:	5b                   	pop    %ebx
8010537e:	5e                   	pop    %esi
8010537f:	5d                   	pop    %ebp
80105380:	c3                   	ret    
80105381:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105388:	5b                   	pop    %ebx
80105389:	31 c0                	xor    %eax,%eax
8010538b:	5e                   	pop    %esi
8010538c:	5d                   	pop    %ebp
8010538d:	c3                   	ret    
8010538e:	66 90                	xchg   %ax,%ax

80105390 <strncpy>:
80105390:	f3 0f 1e fb          	endbr32 
80105394:	55                   	push   %ebp
80105395:	89 e5                	mov    %esp,%ebp
80105397:	57                   	push   %edi
80105398:	56                   	push   %esi
80105399:	8b 75 08             	mov    0x8(%ebp),%esi
8010539c:	53                   	push   %ebx
8010539d:	8b 45 10             	mov    0x10(%ebp),%eax
801053a0:	89 f2                	mov    %esi,%edx
801053a2:	eb 1b                	jmp    801053bf <strncpy+0x2f>
801053a4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801053a8:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
801053ac:	8b 7d 0c             	mov    0xc(%ebp),%edi
801053af:	83 c2 01             	add    $0x1,%edx
801053b2:	0f b6 7f ff          	movzbl -0x1(%edi),%edi
801053b6:	89 f9                	mov    %edi,%ecx
801053b8:	88 4a ff             	mov    %cl,-0x1(%edx)
801053bb:	84 c9                	test   %cl,%cl
801053bd:	74 09                	je     801053c8 <strncpy+0x38>
801053bf:	89 c3                	mov    %eax,%ebx
801053c1:	83 e8 01             	sub    $0x1,%eax
801053c4:	85 db                	test   %ebx,%ebx
801053c6:	7f e0                	jg     801053a8 <strncpy+0x18>
801053c8:	89 d1                	mov    %edx,%ecx
801053ca:	85 c0                	test   %eax,%eax
801053cc:	7e 15                	jle    801053e3 <strncpy+0x53>
801053ce:	66 90                	xchg   %ax,%ax
801053d0:	83 c1 01             	add    $0x1,%ecx
801053d3:	c6 41 ff 00          	movb   $0x0,-0x1(%ecx)
801053d7:	89 c8                	mov    %ecx,%eax
801053d9:	f7 d0                	not    %eax
801053db:	01 d0                	add    %edx,%eax
801053dd:	01 d8                	add    %ebx,%eax
801053df:	85 c0                	test   %eax,%eax
801053e1:	7f ed                	jg     801053d0 <strncpy+0x40>
801053e3:	5b                   	pop    %ebx
801053e4:	89 f0                	mov    %esi,%eax
801053e6:	5e                   	pop    %esi
801053e7:	5f                   	pop    %edi
801053e8:	5d                   	pop    %ebp
801053e9:	c3                   	ret    
801053ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801053f0 <safestrcpy>:
801053f0:	f3 0f 1e fb          	endbr32 
801053f4:	55                   	push   %ebp
801053f5:	89 e5                	mov    %esp,%ebp
801053f7:	56                   	push   %esi
801053f8:	8b 55 10             	mov    0x10(%ebp),%edx
801053fb:	8b 75 08             	mov    0x8(%ebp),%esi
801053fe:	53                   	push   %ebx
801053ff:	8b 45 0c             	mov    0xc(%ebp),%eax
80105402:	85 d2                	test   %edx,%edx
80105404:	7e 21                	jle    80105427 <safestrcpy+0x37>
80105406:	8d 5c 10 ff          	lea    -0x1(%eax,%edx,1),%ebx
8010540a:	89 f2                	mov    %esi,%edx
8010540c:	eb 12                	jmp    80105420 <safestrcpy+0x30>
8010540e:	66 90                	xchg   %ax,%ax
80105410:	0f b6 08             	movzbl (%eax),%ecx
80105413:	83 c0 01             	add    $0x1,%eax
80105416:	83 c2 01             	add    $0x1,%edx
80105419:	88 4a ff             	mov    %cl,-0x1(%edx)
8010541c:	84 c9                	test   %cl,%cl
8010541e:	74 04                	je     80105424 <safestrcpy+0x34>
80105420:	39 d8                	cmp    %ebx,%eax
80105422:	75 ec                	jne    80105410 <safestrcpy+0x20>
80105424:	c6 02 00             	movb   $0x0,(%edx)
80105427:	89 f0                	mov    %esi,%eax
80105429:	5b                   	pop    %ebx
8010542a:	5e                   	pop    %esi
8010542b:	5d                   	pop    %ebp
8010542c:	c3                   	ret    
8010542d:	8d 76 00             	lea    0x0(%esi),%esi

80105430 <strlen>:
80105430:	f3 0f 1e fb          	endbr32 
80105434:	55                   	push   %ebp
80105435:	31 c0                	xor    %eax,%eax
80105437:	89 e5                	mov    %esp,%ebp
80105439:	8b 55 08             	mov    0x8(%ebp),%edx
8010543c:	80 3a 00             	cmpb   $0x0,(%edx)
8010543f:	74 10                	je     80105451 <strlen+0x21>
80105441:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105448:	83 c0 01             	add    $0x1,%eax
8010544b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
8010544f:	75 f7                	jne    80105448 <strlen+0x18>
80105451:	5d                   	pop    %ebp
80105452:	c3                   	ret    

80105453 <swtch>:
80105453:	8b 44 24 04          	mov    0x4(%esp),%eax
80105457:	8b 54 24 08          	mov    0x8(%esp),%edx
8010545b:	55                   	push   %ebp
8010545c:	53                   	push   %ebx
8010545d:	56                   	push   %esi
8010545e:	57                   	push   %edi
8010545f:	89 20                	mov    %esp,(%eax)
80105461:	89 d4                	mov    %edx,%esp
80105463:	5f                   	pop    %edi
80105464:	5e                   	pop    %esi
80105465:	5b                   	pop    %ebx
80105466:	5d                   	pop    %ebp
80105467:	c3                   	ret    
80105468:	66 90                	xchg   %ax,%ax
8010546a:	66 90                	xchg   %ax,%ax
8010546c:	66 90                	xchg   %ax,%ax
8010546e:	66 90                	xchg   %ax,%ax

80105470 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80105470:	f3 0f 1e fb          	endbr32 
80105474:	55                   	push   %ebp
80105475:	89 e5                	mov    %esp,%ebp
80105477:	53                   	push   %ebx
80105478:	83 ec 04             	sub    $0x4,%esp
8010547b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *curproc = myproc();
8010547e:	e8 8d ec ff ff       	call   80104110 <myproc>

  if(addr >= curproc->sz || addr+4 > curproc->sz)
80105483:	8b 00                	mov    (%eax),%eax
80105485:	39 d8                	cmp    %ebx,%eax
80105487:	76 17                	jbe    801054a0 <fetchint+0x30>
80105489:	8d 53 04             	lea    0x4(%ebx),%edx
8010548c:	39 d0                	cmp    %edx,%eax
8010548e:	72 10                	jb     801054a0 <fetchint+0x30>
    return -1;
  *ip = *(int*)(addr);
80105490:	8b 45 0c             	mov    0xc(%ebp),%eax
80105493:	8b 13                	mov    (%ebx),%edx
80105495:	89 10                	mov    %edx,(%eax)
  return 0;
80105497:	31 c0                	xor    %eax,%eax
}
80105499:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010549c:	c9                   	leave  
8010549d:	c3                   	ret    
8010549e:	66 90                	xchg   %ax,%ax
    return -1;
801054a0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801054a5:	eb f2                	jmp    80105499 <fetchint+0x29>
801054a7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801054ae:	66 90                	xchg   %ax,%ax

801054b0 <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
801054b0:	f3 0f 1e fb          	endbr32 
801054b4:	55                   	push   %ebp
801054b5:	89 e5                	mov    %esp,%ebp
801054b7:	53                   	push   %ebx
801054b8:	83 ec 04             	sub    $0x4,%esp
801054bb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  char *s, *ep;
  struct proc *curproc = myproc();
801054be:	e8 4d ec ff ff       	call   80104110 <myproc>

  if(addr >= curproc->sz)
801054c3:	39 18                	cmp    %ebx,(%eax)
801054c5:	76 31                	jbe    801054f8 <fetchstr+0x48>
    return -1;
  *pp = (char*)addr;
801054c7:	8b 55 0c             	mov    0xc(%ebp),%edx
801054ca:	89 1a                	mov    %ebx,(%edx)
  ep = (char*)curproc->sz;
801054cc:	8b 10                	mov    (%eax),%edx
  for(s = *pp; s < ep; s++){
801054ce:	39 d3                	cmp    %edx,%ebx
801054d0:	73 26                	jae    801054f8 <fetchstr+0x48>
801054d2:	89 d8                	mov    %ebx,%eax
801054d4:	eb 11                	jmp    801054e7 <fetchstr+0x37>
801054d6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801054dd:	8d 76 00             	lea    0x0(%esi),%esi
801054e0:	83 c0 01             	add    $0x1,%eax
801054e3:	39 c2                	cmp    %eax,%edx
801054e5:	76 11                	jbe    801054f8 <fetchstr+0x48>
    if(*s == 0)
801054e7:	80 38 00             	cmpb   $0x0,(%eax)
801054ea:	75 f4                	jne    801054e0 <fetchstr+0x30>
      return s - *pp;
801054ec:	29 d8                	sub    %ebx,%eax
  }
  return -1;
}
801054ee:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801054f1:	c9                   	leave  
801054f2:	c3                   	ret    
801054f3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801054f7:	90                   	nop
801054f8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    return -1;
801054fb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105500:	c9                   	leave  
80105501:	c3                   	ret    
80105502:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105509:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105510 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80105510:	f3 0f 1e fb          	endbr32 
80105514:	55                   	push   %ebp
80105515:	89 e5                	mov    %esp,%ebp
80105517:	56                   	push   %esi
80105518:	53                   	push   %ebx
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80105519:	e8 f2 eb ff ff       	call   80104110 <myproc>
8010551e:	8b 55 08             	mov    0x8(%ebp),%edx
80105521:	8b 40 1c             	mov    0x1c(%eax),%eax
80105524:	8b 40 44             	mov    0x44(%eax),%eax
80105527:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
  struct proc *curproc = myproc();
8010552a:	e8 e1 eb ff ff       	call   80104110 <myproc>
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
8010552f:	8d 73 04             	lea    0x4(%ebx),%esi
  if(addr >= curproc->sz || addr+4 > curproc->sz)
80105532:	8b 00                	mov    (%eax),%eax
80105534:	39 c6                	cmp    %eax,%esi
80105536:	73 18                	jae    80105550 <argint+0x40>
80105538:	8d 53 08             	lea    0x8(%ebx),%edx
8010553b:	39 d0                	cmp    %edx,%eax
8010553d:	72 11                	jb     80105550 <argint+0x40>
  *ip = *(int*)(addr);
8010553f:	8b 45 0c             	mov    0xc(%ebp),%eax
80105542:	8b 53 04             	mov    0x4(%ebx),%edx
80105545:	89 10                	mov    %edx,(%eax)
  return 0;
80105547:	31 c0                	xor    %eax,%eax
}
80105549:	5b                   	pop    %ebx
8010554a:	5e                   	pop    %esi
8010554b:	5d                   	pop    %ebp
8010554c:	c3                   	ret    
8010554d:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80105550:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80105555:	eb f2                	jmp    80105549 <argint+0x39>
80105557:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010555e:	66 90                	xchg   %ax,%ax

80105560 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80105560:	f3 0f 1e fb          	endbr32 
80105564:	55                   	push   %ebp
80105565:	89 e5                	mov    %esp,%ebp
80105567:	57                   	push   %edi
80105568:	56                   	push   %esi
80105569:	53                   	push   %ebx
8010556a:	83 ec 0c             	sub    $0xc,%esp
  int i;
  struct proc *curproc = myproc();
8010556d:	e8 9e eb ff ff       	call   80104110 <myproc>
80105572:	89 c6                	mov    %eax,%esi
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80105574:	e8 97 eb ff ff       	call   80104110 <myproc>
80105579:	8b 55 08             	mov    0x8(%ebp),%edx
8010557c:	8b 40 1c             	mov    0x1c(%eax),%eax
8010557f:	8b 40 44             	mov    0x44(%eax),%eax
80105582:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
  struct proc *curproc = myproc();
80105585:	e8 86 eb ff ff       	call   80104110 <myproc>
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
8010558a:	8d 7b 04             	lea    0x4(%ebx),%edi
  if(addr >= curproc->sz || addr+4 > curproc->sz)
8010558d:	8b 00                	mov    (%eax),%eax
8010558f:	39 c7                	cmp    %eax,%edi
80105591:	73 35                	jae    801055c8 <argptr+0x68>
80105593:	8d 4b 08             	lea    0x8(%ebx),%ecx
80105596:	39 c8                	cmp    %ecx,%eax
80105598:	72 2e                	jb     801055c8 <argptr+0x68>
 
  if(argint(n, &i) < 0)
    return -1;
  if(size < 0 || (uint)i >= curproc->sz || (uint)i+size > curproc->sz)
8010559a:	8b 55 10             	mov    0x10(%ebp),%edx
  *ip = *(int*)(addr);
8010559d:	8b 43 04             	mov    0x4(%ebx),%eax
  if(size < 0 || (uint)i >= curproc->sz || (uint)i+size > curproc->sz)
801055a0:	85 d2                	test   %edx,%edx
801055a2:	78 24                	js     801055c8 <argptr+0x68>
801055a4:	8b 16                	mov    (%esi),%edx
801055a6:	39 c2                	cmp    %eax,%edx
801055a8:	76 1e                	jbe    801055c8 <argptr+0x68>
801055aa:	8b 5d 10             	mov    0x10(%ebp),%ebx
801055ad:	01 c3                	add    %eax,%ebx
801055af:	39 da                	cmp    %ebx,%edx
801055b1:	72 15                	jb     801055c8 <argptr+0x68>
    return -1;
  *pp = (char*)i;
801055b3:	8b 55 0c             	mov    0xc(%ebp),%edx
801055b6:	89 02                	mov    %eax,(%edx)
  return 0;
801055b8:	31 c0                	xor    %eax,%eax
}
801055ba:	83 c4 0c             	add    $0xc,%esp
801055bd:	5b                   	pop    %ebx
801055be:	5e                   	pop    %esi
801055bf:	5f                   	pop    %edi
801055c0:	5d                   	pop    %ebp
801055c1:	c3                   	ret    
801055c2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return -1;
801055c8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801055cd:	eb eb                	jmp    801055ba <argptr+0x5a>
801055cf:	90                   	nop

801055d0 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
801055d0:	f3 0f 1e fb          	endbr32 
801055d4:	55                   	push   %ebp
801055d5:	89 e5                	mov    %esp,%ebp
801055d7:	56                   	push   %esi
801055d8:	53                   	push   %ebx
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
801055d9:	e8 32 eb ff ff       	call   80104110 <myproc>
801055de:	8b 55 08             	mov    0x8(%ebp),%edx
801055e1:	8b 40 1c             	mov    0x1c(%eax),%eax
801055e4:	8b 40 44             	mov    0x44(%eax),%eax
801055e7:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
  struct proc *curproc = myproc();
801055ea:	e8 21 eb ff ff       	call   80104110 <myproc>
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
801055ef:	8d 73 04             	lea    0x4(%ebx),%esi
  if(addr >= curproc->sz || addr+4 > curproc->sz)
801055f2:	8b 00                	mov    (%eax),%eax
801055f4:	39 c6                	cmp    %eax,%esi
801055f6:	73 40                	jae    80105638 <argstr+0x68>
801055f8:	8d 53 08             	lea    0x8(%ebx),%edx
801055fb:	39 d0                	cmp    %edx,%eax
801055fd:	72 39                	jb     80105638 <argstr+0x68>
  *ip = *(int*)(addr);
801055ff:	8b 5b 04             	mov    0x4(%ebx),%ebx
  struct proc *curproc = myproc();
80105602:	e8 09 eb ff ff       	call   80104110 <myproc>
  if(addr >= curproc->sz)
80105607:	3b 18                	cmp    (%eax),%ebx
80105609:	73 2d                	jae    80105638 <argstr+0x68>
  *pp = (char*)addr;
8010560b:	8b 55 0c             	mov    0xc(%ebp),%edx
8010560e:	89 1a                	mov    %ebx,(%edx)
  ep = (char*)curproc->sz;
80105610:	8b 10                	mov    (%eax),%edx
  for(s = *pp; s < ep; s++){
80105612:	39 d3                	cmp    %edx,%ebx
80105614:	73 22                	jae    80105638 <argstr+0x68>
80105616:	89 d8                	mov    %ebx,%eax
80105618:	eb 0d                	jmp    80105627 <argstr+0x57>
8010561a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105620:	83 c0 01             	add    $0x1,%eax
80105623:	39 c2                	cmp    %eax,%edx
80105625:	76 11                	jbe    80105638 <argstr+0x68>
    if(*s == 0)
80105627:	80 38 00             	cmpb   $0x0,(%eax)
8010562a:	75 f4                	jne    80105620 <argstr+0x50>
      return s - *pp;
8010562c:	29 d8                	sub    %ebx,%eax
  int addr;
  if(argint(n, &addr) < 0)
    return -1;
  return fetchstr(addr, pp);
}
8010562e:	5b                   	pop    %ebx
8010562f:	5e                   	pop    %esi
80105630:	5d                   	pop    %ebp
80105631:	c3                   	ret    
80105632:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105638:	5b                   	pop    %ebx
    return -1;
80105639:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010563e:	5e                   	pop    %esi
8010563f:	5d                   	pop    %ebp
80105640:	c3                   	ret    
80105641:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105648:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010564f:	90                   	nop

80105650 <syscall>:
/* [SYS_get_file_sectors] sys_get_file_sectors, */
};

void
syscall(void)
{
80105650:	f3 0f 1e fb          	endbr32 
80105654:	55                   	push   %ebp
80105655:	89 e5                	mov    %esp,%ebp
80105657:	53                   	push   %ebx
80105658:	83 ec 04             	sub    $0x4,%esp
  int num;
  struct proc *curproc = myproc();
8010565b:	e8 b0 ea ff ff       	call   80104110 <myproc>
80105660:	89 c3                	mov    %eax,%ebx

  num = curproc->tf->eax;
80105662:	8b 40 1c             	mov    0x1c(%eax),%eax
80105665:	8b 40 1c             	mov    0x1c(%eax),%eax
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80105668:	8d 50 ff             	lea    -0x1(%eax),%edx
8010566b:	83 fa 18             	cmp    $0x18,%edx
8010566e:	77 20                	ja     80105690 <syscall+0x40>
80105670:	8b 14 85 a0 86 10 80 	mov    -0x7fef7960(,%eax,4),%edx
80105677:	85 d2                	test   %edx,%edx
80105679:	74 15                	je     80105690 <syscall+0x40>
    curproc->tf->eax = syscalls[num]();
8010567b:	ff d2                	call   *%edx
8010567d:	89 c2                	mov    %eax,%edx
8010567f:	8b 43 1c             	mov    0x1c(%ebx),%eax
80105682:	89 50 1c             	mov    %edx,0x1c(%eax)
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            curproc->pid, curproc->name, num);
    curproc->tf->eax = -1;
  }
}
80105685:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105688:	c9                   	leave  
80105689:	c3                   	ret    
8010568a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    cprintf("%d %s: unknown sys call %d\n",
80105690:	50                   	push   %eax
            curproc->pid, curproc->name, num);
80105691:	8d 43 70             	lea    0x70(%ebx),%eax
    cprintf("%d %s: unknown sys call %d\n",
80105694:	50                   	push   %eax
80105695:	ff 73 10             	push   0x10(%ebx)
80105698:	68 75 86 10 80       	push   $0x80108675
8010569d:	e8 8e b0 ff ff       	call   80100730 <cprintf>
    curproc->tf->eax = -1;
801056a2:	8b 43 1c             	mov    0x1c(%ebx),%eax
801056a5:	83 c4 10             	add    $0x10,%esp
801056a8:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
}
801056af:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801056b2:	c9                   	leave  
801056b3:	c3                   	ret    
801056b4:	66 90                	xchg   %ax,%ax
801056b6:	66 90                	xchg   %ax,%ax
801056b8:	66 90                	xchg   %ax,%ax
801056ba:	66 90                	xchg   %ax,%ax
801056bc:	66 90                	xchg   %ax,%ax
801056be:	66 90                	xchg   %ax,%ax

801056c0 <create>:
  return -1;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
801056c0:	55                   	push   %ebp
801056c1:	89 e5                	mov    %esp,%ebp
801056c3:	57                   	push   %edi
801056c4:	56                   	push   %esi
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
801056c5:	8d 7d da             	lea    -0x26(%ebp),%edi
{
801056c8:	53                   	push   %ebx
801056c9:	83 ec 34             	sub    $0x34,%esp
801056cc:	89 4d d0             	mov    %ecx,-0x30(%ebp)
801056cf:	8b 4d 08             	mov    0x8(%ebp),%ecx
  if((dp = nameiparent(path, name)) == 0)
801056d2:	57                   	push   %edi
801056d3:	50                   	push   %eax
{
801056d4:	89 55 d4             	mov    %edx,-0x2c(%ebp)
801056d7:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  if((dp = nameiparent(path, name)) == 0)
801056da:	e8 e1 d0 ff ff       	call   801027c0 <nameiparent>
801056df:	83 c4 10             	add    $0x10,%esp
801056e2:	85 c0                	test   %eax,%eax
801056e4:	0f 84 46 01 00 00    	je     80105830 <create+0x170>
    return 0;
  ilock(dp);
801056ea:	83 ec 0c             	sub    $0xc,%esp
801056ed:	89 c3                	mov    %eax,%ebx
801056ef:	50                   	push   %eax
801056f0:	e8 6b c7 ff ff       	call   80101e60 <ilock>

  if((ip = dirlookup(dp, name, 0)) != 0){
801056f5:	83 c4 0c             	add    $0xc,%esp
801056f8:	6a 00                	push   $0x0
801056fa:	57                   	push   %edi
801056fb:	53                   	push   %ebx
801056fc:	e8 cf cc ff ff       	call   801023d0 <dirlookup>
80105701:	83 c4 10             	add    $0x10,%esp
80105704:	89 c6                	mov    %eax,%esi
80105706:	85 c0                	test   %eax,%eax
80105708:	74 56                	je     80105760 <create+0xa0>
    iunlockput(dp);
8010570a:	83 ec 0c             	sub    $0xc,%esp
8010570d:	53                   	push   %ebx
8010570e:	e8 dd c9 ff ff       	call   801020f0 <iunlockput>
    ilock(ip);
80105713:	89 34 24             	mov    %esi,(%esp)
80105716:	e8 45 c7 ff ff       	call   80101e60 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
8010571b:	83 c4 10             	add    $0x10,%esp
8010571e:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80105723:	75 1b                	jne    80105740 <create+0x80>
80105725:	66 83 7e 50 02       	cmpw   $0x2,0x50(%esi)
8010572a:	75 14                	jne    80105740 <create+0x80>
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}
8010572c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010572f:	89 f0                	mov    %esi,%eax
80105731:	5b                   	pop    %ebx
80105732:	5e                   	pop    %esi
80105733:	5f                   	pop    %edi
80105734:	5d                   	pop    %ebp
80105735:	c3                   	ret    
80105736:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010573d:	8d 76 00             	lea    0x0(%esi),%esi
    iunlockput(ip);
80105740:	83 ec 0c             	sub    $0xc,%esp
80105743:	56                   	push   %esi
    return 0;
80105744:	31 f6                	xor    %esi,%esi
    iunlockput(ip);
80105746:	e8 a5 c9 ff ff       	call   801020f0 <iunlockput>
    return 0;
8010574b:	83 c4 10             	add    $0x10,%esp
}
8010574e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105751:	89 f0                	mov    %esi,%eax
80105753:	5b                   	pop    %ebx
80105754:	5e                   	pop    %esi
80105755:	5f                   	pop    %edi
80105756:	5d                   	pop    %ebp
80105757:	c3                   	ret    
80105758:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010575f:	90                   	nop
  if((ip = ialloc(dp->dev, type)) == 0)
80105760:	0f bf 45 d4          	movswl -0x2c(%ebp),%eax
80105764:	83 ec 08             	sub    $0x8,%esp
80105767:	50                   	push   %eax
80105768:	ff 33                	push   (%ebx)
8010576a:	e8 71 c5 ff ff       	call   80101ce0 <ialloc>
8010576f:	83 c4 10             	add    $0x10,%esp
80105772:	89 c6                	mov    %eax,%esi
80105774:	85 c0                	test   %eax,%eax
80105776:	0f 84 cd 00 00 00    	je     80105849 <create+0x189>
  ilock(ip);
8010577c:	83 ec 0c             	sub    $0xc,%esp
8010577f:	50                   	push   %eax
80105780:	e8 db c6 ff ff       	call   80101e60 <ilock>
  ip->major = major;
80105785:	0f b7 45 d0          	movzwl -0x30(%ebp),%eax
80105789:	66 89 46 52          	mov    %ax,0x52(%esi)
  ip->minor = minor;
8010578d:	0f b7 45 cc          	movzwl -0x34(%ebp),%eax
80105791:	66 89 46 54          	mov    %ax,0x54(%esi)
  ip->nlink = 1;
80105795:	b8 01 00 00 00       	mov    $0x1,%eax
8010579a:	66 89 46 56          	mov    %ax,0x56(%esi)
  iupdate(ip);
8010579e:	89 34 24             	mov    %esi,(%esp)
801057a1:	e8 fa c5 ff ff       	call   80101da0 <iupdate>
  if(type == T_DIR){  // Create . and .. entries.
801057a6:	83 c4 10             	add    $0x10,%esp
801057a9:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
801057ae:	74 30                	je     801057e0 <create+0x120>
  if(dirlink(dp, name, ip->inum) < 0)
801057b0:	83 ec 04             	sub    $0x4,%esp
801057b3:	ff 76 04             	push   0x4(%esi)
801057b6:	57                   	push   %edi
801057b7:	53                   	push   %ebx
801057b8:	e8 23 cf ff ff       	call   801026e0 <dirlink>
801057bd:	83 c4 10             	add    $0x10,%esp
801057c0:	85 c0                	test   %eax,%eax
801057c2:	78 78                	js     8010583c <create+0x17c>
  iunlockput(dp);
801057c4:	83 ec 0c             	sub    $0xc,%esp
801057c7:	53                   	push   %ebx
801057c8:	e8 23 c9 ff ff       	call   801020f0 <iunlockput>
  return ip;
801057cd:	83 c4 10             	add    $0x10,%esp
}
801057d0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801057d3:	89 f0                	mov    %esi,%eax
801057d5:	5b                   	pop    %ebx
801057d6:	5e                   	pop    %esi
801057d7:	5f                   	pop    %edi
801057d8:	5d                   	pop    %ebp
801057d9:	c3                   	ret    
801057da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    iupdate(dp);
801057e0:	83 ec 0c             	sub    $0xc,%esp
    dp->nlink++;  // for ".."
801057e3:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
    iupdate(dp);
801057e8:	53                   	push   %ebx
801057e9:	e8 b2 c5 ff ff       	call   80101da0 <iupdate>
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
801057ee:	83 c4 0c             	add    $0xc,%esp
801057f1:	ff 76 04             	push   0x4(%esi)
801057f4:	68 24 87 10 80       	push   $0x80108724
801057f9:	56                   	push   %esi
801057fa:	e8 e1 ce ff ff       	call   801026e0 <dirlink>
801057ff:	83 c4 10             	add    $0x10,%esp
80105802:	85 c0                	test   %eax,%eax
80105804:	78 18                	js     8010581e <create+0x15e>
80105806:	83 ec 04             	sub    $0x4,%esp
80105809:	ff 73 04             	push   0x4(%ebx)
8010580c:	68 23 87 10 80       	push   $0x80108723
80105811:	56                   	push   %esi
80105812:	e8 c9 ce ff ff       	call   801026e0 <dirlink>
80105817:	83 c4 10             	add    $0x10,%esp
8010581a:	85 c0                	test   %eax,%eax
8010581c:	79 92                	jns    801057b0 <create+0xf0>
      panic("create dots");
8010581e:	83 ec 0c             	sub    $0xc,%esp
80105821:	68 17 87 10 80       	push   $0x80108717
80105826:	e8 65 ab ff ff       	call   80100390 <panic>
8010582b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010582f:	90                   	nop
}
80105830:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return 0;
80105833:	31 f6                	xor    %esi,%esi
}
80105835:	5b                   	pop    %ebx
80105836:	89 f0                	mov    %esi,%eax
80105838:	5e                   	pop    %esi
80105839:	5f                   	pop    %edi
8010583a:	5d                   	pop    %ebp
8010583b:	c3                   	ret    
    panic("create: dirlink");
8010583c:	83 ec 0c             	sub    $0xc,%esp
8010583f:	68 26 87 10 80       	push   $0x80108726
80105844:	e8 47 ab ff ff       	call   80100390 <panic>
    panic("create: ialloc");
80105849:	83 ec 0c             	sub    $0xc,%esp
8010584c:	68 08 87 10 80       	push   $0x80108708
80105851:	e8 3a ab ff ff       	call   80100390 <panic>
80105856:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010585d:	8d 76 00             	lea    0x0(%esi),%esi

80105860 <sys_dup>:
{
80105860:	f3 0f 1e fb          	endbr32 
80105864:	55                   	push   %ebp
80105865:	89 e5                	mov    %esp,%ebp
80105867:	56                   	push   %esi
80105868:	53                   	push   %ebx
  if(argint(n, &fd) < 0)
80105869:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
8010586c:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
8010586f:	50                   	push   %eax
80105870:	6a 00                	push   $0x0
80105872:	e8 99 fc ff ff       	call   80105510 <argint>
80105877:	83 c4 10             	add    $0x10,%esp
8010587a:	85 c0                	test   %eax,%eax
8010587c:	78 32                	js     801058b0 <sys_dup+0x50>
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
8010587e:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80105882:	77 2c                	ja     801058b0 <sys_dup+0x50>
80105884:	e8 87 e8 ff ff       	call   80104110 <myproc>
80105889:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010588c:	8b 74 90 2c          	mov    0x2c(%eax,%edx,4),%esi
80105890:	85 f6                	test   %esi,%esi
80105892:	74 1c                	je     801058b0 <sys_dup+0x50>
  struct proc *curproc = myproc();
80105894:	e8 77 e8 ff ff       	call   80104110 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
80105899:	31 db                	xor    %ebx,%ebx
8010589b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010589f:	90                   	nop
    if(curproc->ofile[fd] == 0){
801058a0:	8b 54 98 2c          	mov    0x2c(%eax,%ebx,4),%edx
801058a4:	85 d2                	test   %edx,%edx
801058a6:	74 18                	je     801058c0 <sys_dup+0x60>
  for(fd = 0; fd < NOFILE; fd++){
801058a8:	83 c3 01             	add    $0x1,%ebx
801058ab:	83 fb 10             	cmp    $0x10,%ebx
801058ae:	75 f0                	jne    801058a0 <sys_dup+0x40>
}
801058b0:	8d 65 f8             	lea    -0x8(%ebp),%esp
    return -1;
801058b3:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
}
801058b8:	89 d8                	mov    %ebx,%eax
801058ba:	5b                   	pop    %ebx
801058bb:	5e                   	pop    %esi
801058bc:	5d                   	pop    %ebp
801058bd:	c3                   	ret    
801058be:	66 90                	xchg   %ax,%ax
  filedup(f);
801058c0:	83 ec 0c             	sub    $0xc,%esp
      curproc->ofile[fd] = f;
801058c3:	89 74 98 2c          	mov    %esi,0x2c(%eax,%ebx,4)
  filedup(f);
801058c7:	56                   	push   %esi
801058c8:	e8 63 bc ff ff       	call   80101530 <filedup>
  return fd;
801058cd:	83 c4 10             	add    $0x10,%esp
}
801058d0:	8d 65 f8             	lea    -0x8(%ebp),%esp
801058d3:	89 d8                	mov    %ebx,%eax
801058d5:	5b                   	pop    %ebx
801058d6:	5e                   	pop    %esi
801058d7:	5d                   	pop    %ebp
801058d8:	c3                   	ret    
801058d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801058e0 <sys_read>:
{
801058e0:	f3 0f 1e fb          	endbr32 
801058e4:	55                   	push   %ebp
801058e5:	89 e5                	mov    %esp,%ebp
801058e7:	56                   	push   %esi
801058e8:	53                   	push   %ebx
  if(argint(n, &fd) < 0)
801058e9:	8d 5d f4             	lea    -0xc(%ebp),%ebx
{
801058ec:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
801058ef:	53                   	push   %ebx
801058f0:	6a 00                	push   $0x0
801058f2:	e8 19 fc ff ff       	call   80105510 <argint>
801058f7:	83 c4 10             	add    $0x10,%esp
801058fa:	85 c0                	test   %eax,%eax
801058fc:	78 62                	js     80105960 <sys_read+0x80>
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
801058fe:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80105902:	77 5c                	ja     80105960 <sys_read+0x80>
80105904:	e8 07 e8 ff ff       	call   80104110 <myproc>
80105909:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010590c:	8b 74 90 2c          	mov    0x2c(%eax,%edx,4),%esi
80105910:	85 f6                	test   %esi,%esi
80105912:	74 4c                	je     80105960 <sys_read+0x80>
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80105914:	83 ec 08             	sub    $0x8,%esp
80105917:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010591a:	50                   	push   %eax
8010591b:	6a 02                	push   $0x2
8010591d:	e8 ee fb ff ff       	call   80105510 <argint>
80105922:	83 c4 10             	add    $0x10,%esp
80105925:	85 c0                	test   %eax,%eax
80105927:	78 37                	js     80105960 <sys_read+0x80>
80105929:	83 ec 04             	sub    $0x4,%esp
8010592c:	ff 75 f0             	push   -0x10(%ebp)
8010592f:	53                   	push   %ebx
80105930:	6a 01                	push   $0x1
80105932:	e8 29 fc ff ff       	call   80105560 <argptr>
80105937:	83 c4 10             	add    $0x10,%esp
8010593a:	85 c0                	test   %eax,%eax
8010593c:	78 22                	js     80105960 <sys_read+0x80>
  return fileread(f, p, n);
8010593e:	83 ec 04             	sub    $0x4,%esp
80105941:	ff 75 f0             	push   -0x10(%ebp)
80105944:	ff 75 f4             	push   -0xc(%ebp)
80105947:	56                   	push   %esi
80105948:	e8 63 bd ff ff       	call   801016b0 <fileread>
8010594d:	83 c4 10             	add    $0x10,%esp
}
80105950:	8d 65 f8             	lea    -0x8(%ebp),%esp
80105953:	5b                   	pop    %ebx
80105954:	5e                   	pop    %esi
80105955:	5d                   	pop    %ebp
80105956:	c3                   	ret    
80105957:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010595e:	66 90                	xchg   %ax,%ax
    return -1;
80105960:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105965:	eb e9                	jmp    80105950 <sys_read+0x70>
80105967:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010596e:	66 90                	xchg   %ax,%ax

80105970 <sys_write>:
{
80105970:	f3 0f 1e fb          	endbr32 
80105974:	55                   	push   %ebp
80105975:	89 e5                	mov    %esp,%ebp
80105977:	56                   	push   %esi
80105978:	53                   	push   %ebx
  if(argint(n, &fd) < 0)
80105979:	8d 5d f4             	lea    -0xc(%ebp),%ebx
{
8010597c:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
8010597f:	53                   	push   %ebx
80105980:	6a 00                	push   $0x0
80105982:	e8 89 fb ff ff       	call   80105510 <argint>
80105987:	83 c4 10             	add    $0x10,%esp
8010598a:	85 c0                	test   %eax,%eax
8010598c:	78 62                	js     801059f0 <sys_write+0x80>
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
8010598e:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80105992:	77 5c                	ja     801059f0 <sys_write+0x80>
80105994:	e8 77 e7 ff ff       	call   80104110 <myproc>
80105999:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010599c:	8b 74 90 2c          	mov    0x2c(%eax,%edx,4),%esi
801059a0:	85 f6                	test   %esi,%esi
801059a2:	74 4c                	je     801059f0 <sys_write+0x80>
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
801059a4:	83 ec 08             	sub    $0x8,%esp
801059a7:	8d 45 f0             	lea    -0x10(%ebp),%eax
801059aa:	50                   	push   %eax
801059ab:	6a 02                	push   $0x2
801059ad:	e8 5e fb ff ff       	call   80105510 <argint>
801059b2:	83 c4 10             	add    $0x10,%esp
801059b5:	85 c0                	test   %eax,%eax
801059b7:	78 37                	js     801059f0 <sys_write+0x80>
801059b9:	83 ec 04             	sub    $0x4,%esp
801059bc:	ff 75 f0             	push   -0x10(%ebp)
801059bf:	53                   	push   %ebx
801059c0:	6a 01                	push   $0x1
801059c2:	e8 99 fb ff ff       	call   80105560 <argptr>
801059c7:	83 c4 10             	add    $0x10,%esp
801059ca:	85 c0                	test   %eax,%eax
801059cc:	78 22                	js     801059f0 <sys_write+0x80>
  return filewrite(f, p, n);
801059ce:	83 ec 04             	sub    $0x4,%esp
801059d1:	ff 75 f0             	push   -0x10(%ebp)
801059d4:	ff 75 f4             	push   -0xc(%ebp)
801059d7:	56                   	push   %esi
801059d8:	e8 73 bd ff ff       	call   80101750 <filewrite>
801059dd:	83 c4 10             	add    $0x10,%esp
}
801059e0:	8d 65 f8             	lea    -0x8(%ebp),%esp
801059e3:	5b                   	pop    %ebx
801059e4:	5e                   	pop    %esi
801059e5:	5d                   	pop    %ebp
801059e6:	c3                   	ret    
801059e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801059ee:	66 90                	xchg   %ax,%ax
    return -1;
801059f0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801059f5:	eb e9                	jmp    801059e0 <sys_write+0x70>
801059f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801059fe:	66 90                	xchg   %ax,%ax

80105a00 <sys_close>:
{
80105a00:	f3 0f 1e fb          	endbr32 
80105a04:	55                   	push   %ebp
80105a05:	89 e5                	mov    %esp,%ebp
80105a07:	56                   	push   %esi
80105a08:	53                   	push   %ebx
  if(argint(n, &fd) < 0)
80105a09:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
80105a0c:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
80105a0f:	50                   	push   %eax
80105a10:	6a 00                	push   $0x0
80105a12:	e8 f9 fa ff ff       	call   80105510 <argint>
80105a17:	83 c4 10             	add    $0x10,%esp
80105a1a:	85 c0                	test   %eax,%eax
80105a1c:	78 42                	js     80105a60 <sys_close+0x60>
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
80105a1e:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80105a22:	77 3c                	ja     80105a60 <sys_close+0x60>
80105a24:	e8 e7 e6 ff ff       	call   80104110 <myproc>
80105a29:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105a2c:	8d 5a 08             	lea    0x8(%edx),%ebx
80105a2f:	8b 74 98 0c          	mov    0xc(%eax,%ebx,4),%esi
80105a33:	85 f6                	test   %esi,%esi
80105a35:	74 29                	je     80105a60 <sys_close+0x60>
  myproc()->ofile[fd] = 0;
80105a37:	e8 d4 e6 ff ff       	call   80104110 <myproc>
  fileclose(f);
80105a3c:	83 ec 0c             	sub    $0xc,%esp
  myproc()->ofile[fd] = 0;
80105a3f:	c7 44 98 0c 00 00 00 	movl   $0x0,0xc(%eax,%ebx,4)
80105a46:	00 
  fileclose(f);
80105a47:	56                   	push   %esi
80105a48:	e8 33 bb ff ff       	call   80101580 <fileclose>
  return 0;
80105a4d:	83 c4 10             	add    $0x10,%esp
80105a50:	31 c0                	xor    %eax,%eax
}
80105a52:	8d 65 f8             	lea    -0x8(%ebp),%esp
80105a55:	5b                   	pop    %ebx
80105a56:	5e                   	pop    %esi
80105a57:	5d                   	pop    %ebp
80105a58:	c3                   	ret    
80105a59:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80105a60:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105a65:	eb eb                	jmp    80105a52 <sys_close+0x52>
80105a67:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105a6e:	66 90                	xchg   %ax,%ax

80105a70 <sys_fstat>:
{
80105a70:	f3 0f 1e fb          	endbr32 
80105a74:	55                   	push   %ebp
80105a75:	89 e5                	mov    %esp,%ebp
80105a77:	56                   	push   %esi
80105a78:	53                   	push   %ebx
  if(argint(n, &fd) < 0)
80105a79:	8d 5d f4             	lea    -0xc(%ebp),%ebx
{
80105a7c:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
80105a7f:	53                   	push   %ebx
80105a80:	6a 00                	push   $0x0
80105a82:	e8 89 fa ff ff       	call   80105510 <argint>
80105a87:	83 c4 10             	add    $0x10,%esp
80105a8a:	85 c0                	test   %eax,%eax
80105a8c:	78 42                	js     80105ad0 <sys_fstat+0x60>
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
80105a8e:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80105a92:	77 3c                	ja     80105ad0 <sys_fstat+0x60>
80105a94:	e8 77 e6 ff ff       	call   80104110 <myproc>
80105a99:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105a9c:	8b 74 90 2c          	mov    0x2c(%eax,%edx,4),%esi
80105aa0:	85 f6                	test   %esi,%esi
80105aa2:	74 2c                	je     80105ad0 <sys_fstat+0x60>
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80105aa4:	83 ec 04             	sub    $0x4,%esp
80105aa7:	6a 14                	push   $0x14
80105aa9:	53                   	push   %ebx
80105aaa:	6a 01                	push   $0x1
80105aac:	e8 af fa ff ff       	call   80105560 <argptr>
80105ab1:	83 c4 10             	add    $0x10,%esp
80105ab4:	85 c0                	test   %eax,%eax
80105ab6:	78 18                	js     80105ad0 <sys_fstat+0x60>
  return filestat(f, st);
80105ab8:	83 ec 08             	sub    $0x8,%esp
80105abb:	ff 75 f4             	push   -0xc(%ebp)
80105abe:	56                   	push   %esi
80105abf:	e8 9c bb ff ff       	call   80101660 <filestat>
80105ac4:	83 c4 10             	add    $0x10,%esp
}
80105ac7:	8d 65 f8             	lea    -0x8(%ebp),%esp
80105aca:	5b                   	pop    %ebx
80105acb:	5e                   	pop    %esi
80105acc:	5d                   	pop    %ebp
80105acd:	c3                   	ret    
80105ace:	66 90                	xchg   %ax,%ax
    return -1;
80105ad0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105ad5:	eb f0                	jmp    80105ac7 <sys_fstat+0x57>
80105ad7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105ade:	66 90                	xchg   %ax,%ax

80105ae0 <sys_link>:
{
80105ae0:	f3 0f 1e fb          	endbr32 
80105ae4:	55                   	push   %ebp
80105ae5:	89 e5                	mov    %esp,%ebp
80105ae7:	57                   	push   %edi
80105ae8:	56                   	push   %esi
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80105ae9:	8d 45 d4             	lea    -0x2c(%ebp),%eax
{
80105aec:	53                   	push   %ebx
80105aed:	83 ec 34             	sub    $0x34,%esp
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80105af0:	50                   	push   %eax
80105af1:	6a 00                	push   $0x0
80105af3:	e8 d8 fa ff ff       	call   801055d0 <argstr>
80105af8:	83 c4 10             	add    $0x10,%esp
80105afb:	85 c0                	test   %eax,%eax
80105afd:	0f 88 ff 00 00 00    	js     80105c02 <sys_link+0x122>
80105b03:	83 ec 08             	sub    $0x8,%esp
80105b06:	8d 45 d0             	lea    -0x30(%ebp),%eax
80105b09:	50                   	push   %eax
80105b0a:	6a 01                	push   $0x1
80105b0c:	e8 bf fa ff ff       	call   801055d0 <argstr>
80105b11:	83 c4 10             	add    $0x10,%esp
80105b14:	85 c0                	test   %eax,%eax
80105b16:	0f 88 e6 00 00 00    	js     80105c02 <sys_link+0x122>
  begin_op();
80105b1c:	e8 7f d9 ff ff       	call   801034a0 <begin_op>
  if((ip = namei(old)) == 0){
80105b21:	83 ec 0c             	sub    $0xc,%esp
80105b24:	ff 75 d4             	push   -0x2c(%ebp)
80105b27:	e8 74 cc ff ff       	call   801027a0 <namei>
80105b2c:	83 c4 10             	add    $0x10,%esp
80105b2f:	89 c3                	mov    %eax,%ebx
80105b31:	85 c0                	test   %eax,%eax
80105b33:	0f 84 e8 00 00 00    	je     80105c21 <sys_link+0x141>
  ilock(ip);
80105b39:	83 ec 0c             	sub    $0xc,%esp
80105b3c:	50                   	push   %eax
80105b3d:	e8 1e c3 ff ff       	call   80101e60 <ilock>
  if(ip->type == T_DIR){
80105b42:	83 c4 10             	add    $0x10,%esp
80105b45:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105b4a:	0f 84 b9 00 00 00    	je     80105c09 <sys_link+0x129>
  iupdate(ip);
80105b50:	83 ec 0c             	sub    $0xc,%esp
  ip->nlink++;
80105b53:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
  if((dp = nameiparent(new, name)) == 0)
80105b58:	8d 7d da             	lea    -0x26(%ebp),%edi
  iupdate(ip);
80105b5b:	53                   	push   %ebx
80105b5c:	e8 3f c2 ff ff       	call   80101da0 <iupdate>
  iunlock(ip);
80105b61:	89 1c 24             	mov    %ebx,(%esp)
80105b64:	e8 d7 c3 ff ff       	call   80101f40 <iunlock>
  if((dp = nameiparent(new, name)) == 0)
80105b69:	58                   	pop    %eax
80105b6a:	5a                   	pop    %edx
80105b6b:	57                   	push   %edi
80105b6c:	ff 75 d0             	push   -0x30(%ebp)
80105b6f:	e8 4c cc ff ff       	call   801027c0 <nameiparent>
80105b74:	83 c4 10             	add    $0x10,%esp
80105b77:	89 c6                	mov    %eax,%esi
80105b79:	85 c0                	test   %eax,%eax
80105b7b:	74 5f                	je     80105bdc <sys_link+0xfc>
  ilock(dp);
80105b7d:	83 ec 0c             	sub    $0xc,%esp
80105b80:	50                   	push   %eax
80105b81:	e8 da c2 ff ff       	call   80101e60 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80105b86:	8b 03                	mov    (%ebx),%eax
80105b88:	83 c4 10             	add    $0x10,%esp
80105b8b:	39 06                	cmp    %eax,(%esi)
80105b8d:	75 41                	jne    80105bd0 <sys_link+0xf0>
80105b8f:	83 ec 04             	sub    $0x4,%esp
80105b92:	ff 73 04             	push   0x4(%ebx)
80105b95:	57                   	push   %edi
80105b96:	56                   	push   %esi
80105b97:	e8 44 cb ff ff       	call   801026e0 <dirlink>
80105b9c:	83 c4 10             	add    $0x10,%esp
80105b9f:	85 c0                	test   %eax,%eax
80105ba1:	78 2d                	js     80105bd0 <sys_link+0xf0>
  iunlockput(dp);
80105ba3:	83 ec 0c             	sub    $0xc,%esp
80105ba6:	56                   	push   %esi
80105ba7:	e8 44 c5 ff ff       	call   801020f0 <iunlockput>
  iput(ip);
80105bac:	89 1c 24             	mov    %ebx,(%esp)
80105baf:	e8 dc c3 ff ff       	call   80101f90 <iput>
  end_op();
80105bb4:	e8 57 d9 ff ff       	call   80103510 <end_op>
  return 0;
80105bb9:	83 c4 10             	add    $0x10,%esp
80105bbc:	31 c0                	xor    %eax,%eax
}
80105bbe:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105bc1:	5b                   	pop    %ebx
80105bc2:	5e                   	pop    %esi
80105bc3:	5f                   	pop    %edi
80105bc4:	5d                   	pop    %ebp
80105bc5:	c3                   	ret    
80105bc6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105bcd:	8d 76 00             	lea    0x0(%esi),%esi
    iunlockput(dp);
80105bd0:	83 ec 0c             	sub    $0xc,%esp
80105bd3:	56                   	push   %esi
80105bd4:	e8 17 c5 ff ff       	call   801020f0 <iunlockput>
    goto bad;
80105bd9:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
80105bdc:	83 ec 0c             	sub    $0xc,%esp
80105bdf:	53                   	push   %ebx
80105be0:	e8 7b c2 ff ff       	call   80101e60 <ilock>
  ip->nlink--;
80105be5:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
80105bea:	89 1c 24             	mov    %ebx,(%esp)
80105bed:	e8 ae c1 ff ff       	call   80101da0 <iupdate>
  iunlockput(ip);
80105bf2:	89 1c 24             	mov    %ebx,(%esp)
80105bf5:	e8 f6 c4 ff ff       	call   801020f0 <iunlockput>
  end_op();
80105bfa:	e8 11 d9 ff ff       	call   80103510 <end_op>
  return -1;
80105bff:	83 c4 10             	add    $0x10,%esp
80105c02:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c07:	eb b5                	jmp    80105bbe <sys_link+0xde>
    iunlockput(ip);
80105c09:	83 ec 0c             	sub    $0xc,%esp
80105c0c:	53                   	push   %ebx
80105c0d:	e8 de c4 ff ff       	call   801020f0 <iunlockput>
    end_op();
80105c12:	e8 f9 d8 ff ff       	call   80103510 <end_op>
    return -1;
80105c17:	83 c4 10             	add    $0x10,%esp
80105c1a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c1f:	eb 9d                	jmp    80105bbe <sys_link+0xde>
    end_op();
80105c21:	e8 ea d8 ff ff       	call   80103510 <end_op>
    return -1;
80105c26:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c2b:	eb 91                	jmp    80105bbe <sys_link+0xde>
80105c2d:	8d 76 00             	lea    0x0(%esi),%esi

80105c30 <sys_unlink>:
{
80105c30:	f3 0f 1e fb          	endbr32 
80105c34:	55                   	push   %ebp
80105c35:	89 e5                	mov    %esp,%ebp
80105c37:	57                   	push   %edi
80105c38:	56                   	push   %esi
  if(argstr(0, &path) < 0)
80105c39:	8d 45 c0             	lea    -0x40(%ebp),%eax
{
80105c3c:	53                   	push   %ebx
80105c3d:	83 ec 54             	sub    $0x54,%esp
  if(argstr(0, &path) < 0)
80105c40:	50                   	push   %eax
80105c41:	6a 00                	push   $0x0
80105c43:	e8 88 f9 ff ff       	call   801055d0 <argstr>
80105c48:	83 c4 10             	add    $0x10,%esp
80105c4b:	85 c0                	test   %eax,%eax
80105c4d:	0f 88 86 01 00 00    	js     80105dd9 <sys_unlink+0x1a9>
  begin_op();
80105c53:	e8 48 d8 ff ff       	call   801034a0 <begin_op>
  if((dp = nameiparent(path, name)) == 0){
80105c58:	8d 5d ca             	lea    -0x36(%ebp),%ebx
80105c5b:	83 ec 08             	sub    $0x8,%esp
80105c5e:	53                   	push   %ebx
80105c5f:	ff 75 c0             	push   -0x40(%ebp)
80105c62:	e8 59 cb ff ff       	call   801027c0 <nameiparent>
80105c67:	83 c4 10             	add    $0x10,%esp
80105c6a:	89 45 b4             	mov    %eax,-0x4c(%ebp)
80105c6d:	85 c0                	test   %eax,%eax
80105c6f:	0f 84 6e 01 00 00    	je     80105de3 <sys_unlink+0x1b3>
  ilock(dp);
80105c75:	8b 7d b4             	mov    -0x4c(%ebp),%edi
80105c78:	83 ec 0c             	sub    $0xc,%esp
80105c7b:	57                   	push   %edi
80105c7c:	e8 df c1 ff ff       	call   80101e60 <ilock>
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
80105c81:	58                   	pop    %eax
80105c82:	5a                   	pop    %edx
80105c83:	68 24 87 10 80       	push   $0x80108724
80105c88:	53                   	push   %ebx
80105c89:	e8 22 c7 ff ff       	call   801023b0 <namecmp>
80105c8e:	83 c4 10             	add    $0x10,%esp
80105c91:	85 c0                	test   %eax,%eax
80105c93:	0f 84 07 01 00 00    	je     80105da0 <sys_unlink+0x170>
80105c99:	83 ec 08             	sub    $0x8,%esp
80105c9c:	68 23 87 10 80       	push   $0x80108723
80105ca1:	53                   	push   %ebx
80105ca2:	e8 09 c7 ff ff       	call   801023b0 <namecmp>
80105ca7:	83 c4 10             	add    $0x10,%esp
80105caa:	85 c0                	test   %eax,%eax
80105cac:	0f 84 ee 00 00 00    	je     80105da0 <sys_unlink+0x170>
  if((ip = dirlookup(dp, name, &off)) == 0)
80105cb2:	83 ec 04             	sub    $0x4,%esp
80105cb5:	8d 45 c4             	lea    -0x3c(%ebp),%eax
80105cb8:	50                   	push   %eax
80105cb9:	53                   	push   %ebx
80105cba:	57                   	push   %edi
80105cbb:	e8 10 c7 ff ff       	call   801023d0 <dirlookup>
80105cc0:	83 c4 10             	add    $0x10,%esp
80105cc3:	89 c3                	mov    %eax,%ebx
80105cc5:	85 c0                	test   %eax,%eax
80105cc7:	0f 84 d3 00 00 00    	je     80105da0 <sys_unlink+0x170>
  ilock(ip);
80105ccd:	83 ec 0c             	sub    $0xc,%esp
80105cd0:	50                   	push   %eax
80105cd1:	e8 8a c1 ff ff       	call   80101e60 <ilock>
  if(ip->nlink < 1)
80105cd6:	83 c4 10             	add    $0x10,%esp
80105cd9:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80105cde:	0f 8e 28 01 00 00    	jle    80105e0c <sys_unlink+0x1dc>
  if(ip->type == T_DIR && !isdirempty(ip)){
80105ce4:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105ce9:	8d 7d d8             	lea    -0x28(%ebp),%edi
80105cec:	74 6a                	je     80105d58 <sys_unlink+0x128>
  memset(&de, 0, sizeof(de));
80105cee:	83 ec 04             	sub    $0x4,%esp
80105cf1:	6a 10                	push   $0x10
80105cf3:	6a 00                	push   $0x0
80105cf5:	57                   	push   %edi
80105cf6:	e8 35 f5 ff ff       	call   80105230 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80105cfb:	6a 10                	push   $0x10
80105cfd:	ff 75 c4             	push   -0x3c(%ebp)
80105d00:	57                   	push   %edi
80105d01:	ff 75 b4             	push   -0x4c(%ebp)
80105d04:	e8 77 c5 ff ff       	call   80102280 <writei>
80105d09:	83 c4 20             	add    $0x20,%esp
80105d0c:	83 f8 10             	cmp    $0x10,%eax
80105d0f:	0f 85 ea 00 00 00    	jne    80105dff <sys_unlink+0x1cf>
  if(ip->type == T_DIR){
80105d15:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105d1a:	0f 84 a0 00 00 00    	je     80105dc0 <sys_unlink+0x190>
  iunlockput(dp);
80105d20:	83 ec 0c             	sub    $0xc,%esp
80105d23:	ff 75 b4             	push   -0x4c(%ebp)
80105d26:	e8 c5 c3 ff ff       	call   801020f0 <iunlockput>
  ip->nlink--;
80105d2b:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
80105d30:	89 1c 24             	mov    %ebx,(%esp)
80105d33:	e8 68 c0 ff ff       	call   80101da0 <iupdate>
  iunlockput(ip);
80105d38:	89 1c 24             	mov    %ebx,(%esp)
80105d3b:	e8 b0 c3 ff ff       	call   801020f0 <iunlockput>
  end_op();
80105d40:	e8 cb d7 ff ff       	call   80103510 <end_op>
  return 0;
80105d45:	83 c4 10             	add    $0x10,%esp
80105d48:	31 c0                	xor    %eax,%eax
}
80105d4a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105d4d:	5b                   	pop    %ebx
80105d4e:	5e                   	pop    %esi
80105d4f:	5f                   	pop    %edi
80105d50:	5d                   	pop    %ebp
80105d51:	c3                   	ret    
80105d52:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
80105d58:	83 7b 58 20          	cmpl   $0x20,0x58(%ebx)
80105d5c:	76 90                	jbe    80105cee <sys_unlink+0xbe>
80105d5e:	be 20 00 00 00       	mov    $0x20,%esi
80105d63:	eb 0f                	jmp    80105d74 <sys_unlink+0x144>
80105d65:	8d 76 00             	lea    0x0(%esi),%esi
80105d68:	83 c6 10             	add    $0x10,%esi
80105d6b:	3b 73 58             	cmp    0x58(%ebx),%esi
80105d6e:	0f 83 7a ff ff ff    	jae    80105cee <sys_unlink+0xbe>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80105d74:	6a 10                	push   $0x10
80105d76:	56                   	push   %esi
80105d77:	57                   	push   %edi
80105d78:	53                   	push   %ebx
80105d79:	e8 02 c4 ff ff       	call   80102180 <readi>
80105d7e:	83 c4 10             	add    $0x10,%esp
80105d81:	83 f8 10             	cmp    $0x10,%eax
80105d84:	75 6c                	jne    80105df2 <sys_unlink+0x1c2>
    if(de.inum != 0)
80105d86:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80105d8b:	74 db                	je     80105d68 <sys_unlink+0x138>
    iunlockput(ip);
80105d8d:	83 ec 0c             	sub    $0xc,%esp
80105d90:	53                   	push   %ebx
80105d91:	e8 5a c3 ff ff       	call   801020f0 <iunlockput>
    goto bad;
80105d96:	83 c4 10             	add    $0x10,%esp
80105d99:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  iunlockput(dp);
80105da0:	83 ec 0c             	sub    $0xc,%esp
80105da3:	ff 75 b4             	push   -0x4c(%ebp)
80105da6:	e8 45 c3 ff ff       	call   801020f0 <iunlockput>
  end_op();
80105dab:	e8 60 d7 ff ff       	call   80103510 <end_op>
  return -1;
80105db0:	83 c4 10             	add    $0x10,%esp
80105db3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105db8:	eb 90                	jmp    80105d4a <sys_unlink+0x11a>
80105dba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    dp->nlink--;
80105dc0:	8b 45 b4             	mov    -0x4c(%ebp),%eax
    iupdate(dp);
80105dc3:	83 ec 0c             	sub    $0xc,%esp
    dp->nlink--;
80105dc6:	66 83 68 56 01       	subw   $0x1,0x56(%eax)
    iupdate(dp);
80105dcb:	50                   	push   %eax
80105dcc:	e8 cf bf ff ff       	call   80101da0 <iupdate>
80105dd1:	83 c4 10             	add    $0x10,%esp
80105dd4:	e9 47 ff ff ff       	jmp    80105d20 <sys_unlink+0xf0>
    return -1;
80105dd9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105dde:	e9 67 ff ff ff       	jmp    80105d4a <sys_unlink+0x11a>
    end_op();
80105de3:	e8 28 d7 ff ff       	call   80103510 <end_op>
    return -1;
80105de8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105ded:	e9 58 ff ff ff       	jmp    80105d4a <sys_unlink+0x11a>
      panic("isdirempty: readi");
80105df2:	83 ec 0c             	sub    $0xc,%esp
80105df5:	68 48 87 10 80       	push   $0x80108748
80105dfa:	e8 91 a5 ff ff       	call   80100390 <panic>
    panic("unlink: writei");
80105dff:	83 ec 0c             	sub    $0xc,%esp
80105e02:	68 5a 87 10 80       	push   $0x8010875a
80105e07:	e8 84 a5 ff ff       	call   80100390 <panic>
    panic("unlink: nlink < 1");
80105e0c:	83 ec 0c             	sub    $0xc,%esp
80105e0f:	68 36 87 10 80       	push   $0x80108736
80105e14:	e8 77 a5 ff ff       	call   80100390 <panic>
80105e19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105e20 <sys_open>:

int
sys_open(void)
{
80105e20:	f3 0f 1e fb          	endbr32 
80105e24:	55                   	push   %ebp
80105e25:	89 e5                	mov    %esp,%ebp
80105e27:	57                   	push   %edi
80105e28:	56                   	push   %esi
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80105e29:	8d 45 e0             	lea    -0x20(%ebp),%eax
{
80105e2c:	53                   	push   %ebx
80105e2d:	83 ec 24             	sub    $0x24,%esp
  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80105e30:	50                   	push   %eax
80105e31:	6a 00                	push   $0x0
80105e33:	e8 98 f7 ff ff       	call   801055d0 <argstr>
80105e38:	83 c4 10             	add    $0x10,%esp
80105e3b:	85 c0                	test   %eax,%eax
80105e3d:	0f 88 8a 00 00 00    	js     80105ecd <sys_open+0xad>
80105e43:	83 ec 08             	sub    $0x8,%esp
80105e46:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80105e49:	50                   	push   %eax
80105e4a:	6a 01                	push   $0x1
80105e4c:	e8 bf f6 ff ff       	call   80105510 <argint>
80105e51:	83 c4 10             	add    $0x10,%esp
80105e54:	85 c0                	test   %eax,%eax
80105e56:	78 75                	js     80105ecd <sys_open+0xad>
    return -1;

  begin_op();
80105e58:	e8 43 d6 ff ff       	call   801034a0 <begin_op>

  if(omode & O_CREATE){
80105e5d:	f6 45 e5 02          	testb  $0x2,-0x1b(%ebp)
80105e61:	75 75                	jne    80105ed8 <sys_open+0xb8>
    if(ip == 0){
      end_op();
      return -1;
    }
  } else {
    if((ip = namei(path)) == 0){
80105e63:	83 ec 0c             	sub    $0xc,%esp
80105e66:	ff 75 e0             	push   -0x20(%ebp)
80105e69:	e8 32 c9 ff ff       	call   801027a0 <namei>
80105e6e:	83 c4 10             	add    $0x10,%esp
80105e71:	89 c6                	mov    %eax,%esi
80105e73:	85 c0                	test   %eax,%eax
80105e75:	74 7e                	je     80105ef5 <sys_open+0xd5>
      end_op();
      return -1;
    }
    ilock(ip);
80105e77:	83 ec 0c             	sub    $0xc,%esp
80105e7a:	50                   	push   %eax
80105e7b:	e8 e0 bf ff ff       	call   80101e60 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
80105e80:	83 c4 10             	add    $0x10,%esp
80105e83:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80105e88:	0f 84 c2 00 00 00    	je     80105f50 <sys_open+0x130>
      end_op();
      return -1;
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
80105e8e:	e8 2d b6 ff ff       	call   801014c0 <filealloc>
80105e93:	89 c7                	mov    %eax,%edi
80105e95:	85 c0                	test   %eax,%eax
80105e97:	74 23                	je     80105ebc <sys_open+0x9c>
  struct proc *curproc = myproc();
80105e99:	e8 72 e2 ff ff       	call   80104110 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
80105e9e:	31 db                	xor    %ebx,%ebx
    if(curproc->ofile[fd] == 0){
80105ea0:	8b 54 98 2c          	mov    0x2c(%eax,%ebx,4),%edx
80105ea4:	85 d2                	test   %edx,%edx
80105ea6:	74 60                	je     80105f08 <sys_open+0xe8>
  for(fd = 0; fd < NOFILE; fd++){
80105ea8:	83 c3 01             	add    $0x1,%ebx
80105eab:	83 fb 10             	cmp    $0x10,%ebx
80105eae:	75 f0                	jne    80105ea0 <sys_open+0x80>
    if(f)
      fileclose(f);
80105eb0:	83 ec 0c             	sub    $0xc,%esp
80105eb3:	57                   	push   %edi
80105eb4:	e8 c7 b6 ff ff       	call   80101580 <fileclose>
80105eb9:	83 c4 10             	add    $0x10,%esp
    iunlockput(ip);
80105ebc:	83 ec 0c             	sub    $0xc,%esp
80105ebf:	56                   	push   %esi
80105ec0:	e8 2b c2 ff ff       	call   801020f0 <iunlockput>
    end_op();
80105ec5:	e8 46 d6 ff ff       	call   80103510 <end_op>
    return -1;
80105eca:	83 c4 10             	add    $0x10,%esp
80105ecd:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105ed2:	eb 6d                	jmp    80105f41 <sys_open+0x121>
80105ed4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    ip = create(path, T_FILE, 0, 0);
80105ed8:	83 ec 0c             	sub    $0xc,%esp
80105edb:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105ede:	31 c9                	xor    %ecx,%ecx
80105ee0:	ba 02 00 00 00       	mov    $0x2,%edx
80105ee5:	6a 00                	push   $0x0
80105ee7:	e8 d4 f7 ff ff       	call   801056c0 <create>
    if(ip == 0){
80105eec:	83 c4 10             	add    $0x10,%esp
    ip = create(path, T_FILE, 0, 0);
80105eef:	89 c6                	mov    %eax,%esi
    if(ip == 0){
80105ef1:	85 c0                	test   %eax,%eax
80105ef3:	75 99                	jne    80105e8e <sys_open+0x6e>
      end_op();
80105ef5:	e8 16 d6 ff ff       	call   80103510 <end_op>
      return -1;
80105efa:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105eff:	eb 40                	jmp    80105f41 <sys_open+0x121>
80105f01:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  }
  iunlock(ip);
80105f08:	83 ec 0c             	sub    $0xc,%esp
      curproc->ofile[fd] = f;
80105f0b:	89 7c 98 2c          	mov    %edi,0x2c(%eax,%ebx,4)
  iunlock(ip);
80105f0f:	56                   	push   %esi
80105f10:	e8 2b c0 ff ff       	call   80101f40 <iunlock>
  end_op();
80105f15:	e8 f6 d5 ff ff       	call   80103510 <end_op>

  f->type = FD_INODE;
80105f1a:	c7 07 02 00 00 00    	movl   $0x2,(%edi)
  f->ip = ip;
  f->off = 0;
  f->readable = !(omode & O_WRONLY);
80105f20:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105f23:	83 c4 10             	add    $0x10,%esp
  f->ip = ip;
80105f26:	89 77 10             	mov    %esi,0x10(%edi)
  f->readable = !(omode & O_WRONLY);
80105f29:	89 d0                	mov    %edx,%eax
  f->off = 0;
80105f2b:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)
  f->readable = !(omode & O_WRONLY);
80105f32:	f7 d0                	not    %eax
80105f34:	83 e0 01             	and    $0x1,%eax
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105f37:	83 e2 03             	and    $0x3,%edx
  f->readable = !(omode & O_WRONLY);
80105f3a:	88 47 08             	mov    %al,0x8(%edi)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105f3d:	0f 95 47 09          	setne  0x9(%edi)
  return fd;
}
80105f41:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105f44:	89 d8                	mov    %ebx,%eax
80105f46:	5b                   	pop    %ebx
80105f47:	5e                   	pop    %esi
80105f48:	5f                   	pop    %edi
80105f49:	5d                   	pop    %ebp
80105f4a:	c3                   	ret    
80105f4b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105f4f:	90                   	nop
    if(ip->type == T_DIR && omode != O_RDONLY){
80105f50:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80105f53:	85 c9                	test   %ecx,%ecx
80105f55:	0f 84 33 ff ff ff    	je     80105e8e <sys_open+0x6e>
80105f5b:	e9 5c ff ff ff       	jmp    80105ebc <sys_open+0x9c>

80105f60 <sys_mkdir>:

int
sys_mkdir(void)
{
80105f60:	f3 0f 1e fb          	endbr32 
80105f64:	55                   	push   %ebp
80105f65:	89 e5                	mov    %esp,%ebp
80105f67:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
80105f6a:	e8 31 d5 ff ff       	call   801034a0 <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
80105f6f:	83 ec 08             	sub    $0x8,%esp
80105f72:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105f75:	50                   	push   %eax
80105f76:	6a 00                	push   $0x0
80105f78:	e8 53 f6 ff ff       	call   801055d0 <argstr>
80105f7d:	83 c4 10             	add    $0x10,%esp
80105f80:	85 c0                	test   %eax,%eax
80105f82:	78 34                	js     80105fb8 <sys_mkdir+0x58>
80105f84:	83 ec 0c             	sub    $0xc,%esp
80105f87:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f8a:	31 c9                	xor    %ecx,%ecx
80105f8c:	ba 01 00 00 00       	mov    $0x1,%edx
80105f91:	6a 00                	push   $0x0
80105f93:	e8 28 f7 ff ff       	call   801056c0 <create>
80105f98:	83 c4 10             	add    $0x10,%esp
80105f9b:	85 c0                	test   %eax,%eax
80105f9d:	74 19                	je     80105fb8 <sys_mkdir+0x58>
    end_op();
    return -1;
  }
  iunlockput(ip);
80105f9f:	83 ec 0c             	sub    $0xc,%esp
80105fa2:	50                   	push   %eax
80105fa3:	e8 48 c1 ff ff       	call   801020f0 <iunlockput>
  end_op();
80105fa8:	e8 63 d5 ff ff       	call   80103510 <end_op>
  return 0;
80105fad:	83 c4 10             	add    $0x10,%esp
80105fb0:	31 c0                	xor    %eax,%eax
}
80105fb2:	c9                   	leave  
80105fb3:	c3                   	ret    
80105fb4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    end_op();
80105fb8:	e8 53 d5 ff ff       	call   80103510 <end_op>
    return -1;
80105fbd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105fc2:	c9                   	leave  
80105fc3:	c3                   	ret    
80105fc4:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105fcb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105fcf:	90                   	nop

80105fd0 <sys_mknod>:

int
sys_mknod(void)
{
80105fd0:	f3 0f 1e fb          	endbr32 
80105fd4:	55                   	push   %ebp
80105fd5:	89 e5                	mov    %esp,%ebp
80105fd7:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
80105fda:	e8 c1 d4 ff ff       	call   801034a0 <begin_op>
  if((argstr(0, &path)) < 0 ||
80105fdf:	83 ec 08             	sub    $0x8,%esp
80105fe2:	8d 45 ec             	lea    -0x14(%ebp),%eax
80105fe5:	50                   	push   %eax
80105fe6:	6a 00                	push   $0x0
80105fe8:	e8 e3 f5 ff ff       	call   801055d0 <argstr>
80105fed:	83 c4 10             	add    $0x10,%esp
80105ff0:	85 c0                	test   %eax,%eax
80105ff2:	78 64                	js     80106058 <sys_mknod+0x88>
     argint(1, &major) < 0 ||
80105ff4:	83 ec 08             	sub    $0x8,%esp
80105ff7:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105ffa:	50                   	push   %eax
80105ffb:	6a 01                	push   $0x1
80105ffd:	e8 0e f5 ff ff       	call   80105510 <argint>
  if((argstr(0, &path)) < 0 ||
80106002:	83 c4 10             	add    $0x10,%esp
80106005:	85 c0                	test   %eax,%eax
80106007:	78 4f                	js     80106058 <sys_mknod+0x88>
     argint(2, &minor) < 0 ||
80106009:	83 ec 08             	sub    $0x8,%esp
8010600c:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010600f:	50                   	push   %eax
80106010:	6a 02                	push   $0x2
80106012:	e8 f9 f4 ff ff       	call   80105510 <argint>
     argint(1, &major) < 0 ||
80106017:	83 c4 10             	add    $0x10,%esp
8010601a:	85 c0                	test   %eax,%eax
8010601c:	78 3a                	js     80106058 <sys_mknod+0x88>
     (ip = create(path, T_DEV, major, minor)) == 0){
8010601e:	0f bf 45 f4          	movswl -0xc(%ebp),%eax
80106022:	83 ec 0c             	sub    $0xc,%esp
80106025:	0f bf 4d f0          	movswl -0x10(%ebp),%ecx
80106029:	ba 03 00 00 00       	mov    $0x3,%edx
8010602e:	50                   	push   %eax
8010602f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106032:	e8 89 f6 ff ff       	call   801056c0 <create>
     argint(2, &minor) < 0 ||
80106037:	83 c4 10             	add    $0x10,%esp
8010603a:	85 c0                	test   %eax,%eax
8010603c:	74 1a                	je     80106058 <sys_mknod+0x88>
    end_op();
    return -1;
  }
  iunlockput(ip);
8010603e:	83 ec 0c             	sub    $0xc,%esp
80106041:	50                   	push   %eax
80106042:	e8 a9 c0 ff ff       	call   801020f0 <iunlockput>
  end_op();
80106047:	e8 c4 d4 ff ff       	call   80103510 <end_op>
  return 0;
8010604c:	83 c4 10             	add    $0x10,%esp
8010604f:	31 c0                	xor    %eax,%eax
}
80106051:	c9                   	leave  
80106052:	c3                   	ret    
80106053:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106057:	90                   	nop
    end_op();
80106058:	e8 b3 d4 ff ff       	call   80103510 <end_op>
    return -1;
8010605d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106062:	c9                   	leave  
80106063:	c3                   	ret    
80106064:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010606b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010606f:	90                   	nop

80106070 <sys_chdir>:

int
sys_chdir(void)
{
80106070:	f3 0f 1e fb          	endbr32 
80106074:	55                   	push   %ebp
80106075:	89 e5                	mov    %esp,%ebp
80106077:	56                   	push   %esi
80106078:	53                   	push   %ebx
80106079:	83 ec 10             	sub    $0x10,%esp
  char *path;
  struct inode *ip;
  struct proc *curproc = myproc();
8010607c:	e8 8f e0 ff ff       	call   80104110 <myproc>
80106081:	89 c6                	mov    %eax,%esi
  
  begin_op();
80106083:	e8 18 d4 ff ff       	call   801034a0 <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
80106088:	83 ec 08             	sub    $0x8,%esp
8010608b:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010608e:	50                   	push   %eax
8010608f:	6a 00                	push   $0x0
80106091:	e8 3a f5 ff ff       	call   801055d0 <argstr>
80106096:	83 c4 10             	add    $0x10,%esp
80106099:	85 c0                	test   %eax,%eax
8010609b:	78 73                	js     80106110 <sys_chdir+0xa0>
8010609d:	83 ec 0c             	sub    $0xc,%esp
801060a0:	ff 75 f4             	push   -0xc(%ebp)
801060a3:	e8 f8 c6 ff ff       	call   801027a0 <namei>
801060a8:	83 c4 10             	add    $0x10,%esp
801060ab:	89 c3                	mov    %eax,%ebx
801060ad:	85 c0                	test   %eax,%eax
801060af:	74 5f                	je     80106110 <sys_chdir+0xa0>
    end_op();
    return -1;
  }
  ilock(ip);
801060b1:	83 ec 0c             	sub    $0xc,%esp
801060b4:	50                   	push   %eax
801060b5:	e8 a6 bd ff ff       	call   80101e60 <ilock>
  if(ip->type != T_DIR){
801060ba:	83 c4 10             	add    $0x10,%esp
801060bd:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801060c2:	75 2c                	jne    801060f0 <sys_chdir+0x80>
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
801060c4:	83 ec 0c             	sub    $0xc,%esp
801060c7:	53                   	push   %ebx
801060c8:	e8 73 be ff ff       	call   80101f40 <iunlock>
  iput(curproc->cwd);
801060cd:	58                   	pop    %eax
801060ce:	ff 76 6c             	push   0x6c(%esi)
801060d1:	e8 ba be ff ff       	call   80101f90 <iput>
  end_op();
801060d6:	e8 35 d4 ff ff       	call   80103510 <end_op>
  curproc->cwd = ip;
801060db:	89 5e 6c             	mov    %ebx,0x6c(%esi)
  return 0;
801060de:	83 c4 10             	add    $0x10,%esp
801060e1:	31 c0                	xor    %eax,%eax
}
801060e3:	8d 65 f8             	lea    -0x8(%ebp),%esp
801060e6:	5b                   	pop    %ebx
801060e7:	5e                   	pop    %esi
801060e8:	5d                   	pop    %ebp
801060e9:	c3                   	ret    
801060ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    iunlockput(ip);
801060f0:	83 ec 0c             	sub    $0xc,%esp
801060f3:	53                   	push   %ebx
801060f4:	e8 f7 bf ff ff       	call   801020f0 <iunlockput>
    end_op();
801060f9:	e8 12 d4 ff ff       	call   80103510 <end_op>
    return -1;
801060fe:	83 c4 10             	add    $0x10,%esp
80106101:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106106:	eb db                	jmp    801060e3 <sys_chdir+0x73>
80106108:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010610f:	90                   	nop
    end_op();
80106110:	e8 fb d3 ff ff       	call   80103510 <end_op>
    return -1;
80106115:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010611a:	eb c7                	jmp    801060e3 <sys_chdir+0x73>
8010611c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80106120 <sys_exec>:

int
sys_exec(void)
{
80106120:	f3 0f 1e fb          	endbr32 
80106124:	55                   	push   %ebp
80106125:	89 e5                	mov    %esp,%ebp
80106127:	57                   	push   %edi
80106128:	56                   	push   %esi
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80106129:	8d 85 5c ff ff ff    	lea    -0xa4(%ebp),%eax
{
8010612f:	53                   	push   %ebx
80106130:	81 ec a4 00 00 00    	sub    $0xa4,%esp
  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80106136:	50                   	push   %eax
80106137:	6a 00                	push   $0x0
80106139:	e8 92 f4 ff ff       	call   801055d0 <argstr>
8010613e:	83 c4 10             	add    $0x10,%esp
80106141:	85 c0                	test   %eax,%eax
80106143:	0f 88 83 00 00 00    	js     801061cc <sys_exec+0xac>
80106149:	83 ec 08             	sub    $0x8,%esp
8010614c:	8d 85 60 ff ff ff    	lea    -0xa0(%ebp),%eax
80106152:	50                   	push   %eax
80106153:	6a 01                	push   $0x1
80106155:	e8 b6 f3 ff ff       	call   80105510 <argint>
8010615a:	83 c4 10             	add    $0x10,%esp
8010615d:	85 c0                	test   %eax,%eax
8010615f:	78 6b                	js     801061cc <sys_exec+0xac>
    return -1;
  }
  memset(argv, 0, sizeof(argv));
80106161:	83 ec 04             	sub    $0x4,%esp
80106164:	8d b5 68 ff ff ff    	lea    -0x98(%ebp),%esi
  for(i=0;; i++){
8010616a:	31 db                	xor    %ebx,%ebx
  memset(argv, 0, sizeof(argv));
8010616c:	68 80 00 00 00       	push   $0x80
80106171:	6a 00                	push   $0x0
80106173:	56                   	push   %esi
80106174:	e8 b7 f0 ff ff       	call   80105230 <memset>
80106179:	83 c4 10             	add    $0x10,%esp
8010617c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(i >= NELEM(argv))
      return -1;
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
80106180:	83 ec 08             	sub    $0x8,%esp
80106183:	8d 85 64 ff ff ff    	lea    -0x9c(%ebp),%eax
80106189:	8d 3c 9d 00 00 00 00 	lea    0x0(,%ebx,4),%edi
80106190:	50                   	push   %eax
80106191:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
80106197:	01 f8                	add    %edi,%eax
80106199:	50                   	push   %eax
8010619a:	e8 d1 f2 ff ff       	call   80105470 <fetchint>
8010619f:	83 c4 10             	add    $0x10,%esp
801061a2:	85 c0                	test   %eax,%eax
801061a4:	78 26                	js     801061cc <sys_exec+0xac>
      return -1;
    if(uarg == 0){
801061a6:	8b 85 64 ff ff ff    	mov    -0x9c(%ebp),%eax
801061ac:	85 c0                	test   %eax,%eax
801061ae:	74 30                	je     801061e0 <sys_exec+0xc0>
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
801061b0:	83 ec 08             	sub    $0x8,%esp
801061b3:	8d 14 3e             	lea    (%esi,%edi,1),%edx
801061b6:	52                   	push   %edx
801061b7:	50                   	push   %eax
801061b8:	e8 f3 f2 ff ff       	call   801054b0 <fetchstr>
801061bd:	83 c4 10             	add    $0x10,%esp
801061c0:	85 c0                	test   %eax,%eax
801061c2:	78 08                	js     801061cc <sys_exec+0xac>
  for(i=0;; i++){
801061c4:	83 c3 01             	add    $0x1,%ebx
    if(i >= NELEM(argv))
801061c7:	83 fb 20             	cmp    $0x20,%ebx
801061ca:	75 b4                	jne    80106180 <sys_exec+0x60>
      return -1;
  }
  return exec(path, argv);
}
801061cc:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return -1;
801061cf:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801061d4:	5b                   	pop    %ebx
801061d5:	5e                   	pop    %esi
801061d6:	5f                   	pop    %edi
801061d7:	5d                   	pop    %ebp
801061d8:	c3                   	ret    
801061d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      argv[i] = 0;
801061e0:	c7 84 9d 68 ff ff ff 	movl   $0x0,-0x98(%ebp,%ebx,4)
801061e7:	00 00 00 00 
  return exec(path, argv);
801061eb:	83 ec 08             	sub    $0x8,%esp
801061ee:	56                   	push   %esi
801061ef:	ff b5 5c ff ff ff    	push   -0xa4(%ebp)
801061f5:	e8 46 af ff ff       	call   80101140 <exec>
801061fa:	83 c4 10             	add    $0x10,%esp
}
801061fd:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106200:	5b                   	pop    %ebx
80106201:	5e                   	pop    %esi
80106202:	5f                   	pop    %edi
80106203:	5d                   	pop    %ebp
80106204:	c3                   	ret    
80106205:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010620c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80106210 <sys_pipe>:

int
sys_pipe(void)
{
80106210:	f3 0f 1e fb          	endbr32 
80106214:	55                   	push   %ebp
80106215:	89 e5                	mov    %esp,%ebp
80106217:	57                   	push   %edi
80106218:	56                   	push   %esi
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80106219:	8d 45 dc             	lea    -0x24(%ebp),%eax
{
8010621c:	53                   	push   %ebx
8010621d:	83 ec 20             	sub    $0x20,%esp
  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80106220:	6a 08                	push   $0x8
80106222:	50                   	push   %eax
80106223:	6a 00                	push   $0x0
80106225:	e8 36 f3 ff ff       	call   80105560 <argptr>
8010622a:	83 c4 10             	add    $0x10,%esp
8010622d:	85 c0                	test   %eax,%eax
8010622f:	78 4e                	js     8010627f <sys_pipe+0x6f>
    return -1;
  if(pipealloc(&rf, &wf) < 0)
80106231:	83 ec 08             	sub    $0x8,%esp
80106234:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106237:	50                   	push   %eax
80106238:	8d 45 e0             	lea    -0x20(%ebp),%eax
8010623b:	50                   	push   %eax
8010623c:	e8 4f d9 ff ff       	call   80103b90 <pipealloc>
80106241:	83 c4 10             	add    $0x10,%esp
80106244:	85 c0                	test   %eax,%eax
80106246:	78 37                	js     8010627f <sys_pipe+0x6f>
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80106248:	8b 7d e0             	mov    -0x20(%ebp),%edi
  for(fd = 0; fd < NOFILE; fd++){
8010624b:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
8010624d:	e8 be de ff ff       	call   80104110 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
80106252:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(curproc->ofile[fd] == 0){
80106258:	8b 74 98 2c          	mov    0x2c(%eax,%ebx,4),%esi
8010625c:	85 f6                	test   %esi,%esi
8010625e:	74 30                	je     80106290 <sys_pipe+0x80>
  for(fd = 0; fd < NOFILE; fd++){
80106260:	83 c3 01             	add    $0x1,%ebx
80106263:	83 fb 10             	cmp    $0x10,%ebx
80106266:	75 f0                	jne    80106258 <sys_pipe+0x48>
    if(fd0 >= 0)
      myproc()->ofile[fd0] = 0;
    fileclose(rf);
80106268:	83 ec 0c             	sub    $0xc,%esp
8010626b:	ff 75 e0             	push   -0x20(%ebp)
8010626e:	e8 0d b3 ff ff       	call   80101580 <fileclose>
    fileclose(wf);
80106273:	58                   	pop    %eax
80106274:	ff 75 e4             	push   -0x1c(%ebp)
80106277:	e8 04 b3 ff ff       	call   80101580 <fileclose>
    return -1;
8010627c:	83 c4 10             	add    $0x10,%esp
8010627f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106284:	eb 5b                	jmp    801062e1 <sys_pipe+0xd1>
80106286:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010628d:	8d 76 00             	lea    0x0(%esi),%esi
      curproc->ofile[fd] = f;
80106290:	8d 73 08             	lea    0x8(%ebx),%esi
80106293:	89 7c b0 0c          	mov    %edi,0xc(%eax,%esi,4)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80106297:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  struct proc *curproc = myproc();
8010629a:	e8 71 de ff ff       	call   80104110 <myproc>
8010629f:	89 c2                	mov    %eax,%edx
  for(fd = 0; fd < NOFILE; fd++){
801062a1:	31 c0                	xor    %eax,%eax
801062a3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801062a7:	90                   	nop
    if(curproc->ofile[fd] == 0){
801062a8:	8b 4c 82 2c          	mov    0x2c(%edx,%eax,4),%ecx
801062ac:	85 c9                	test   %ecx,%ecx
801062ae:	74 20                	je     801062d0 <sys_pipe+0xc0>
  for(fd = 0; fd < NOFILE; fd++){
801062b0:	83 c0 01             	add    $0x1,%eax
801062b3:	83 f8 10             	cmp    $0x10,%eax
801062b6:	75 f0                	jne    801062a8 <sys_pipe+0x98>
      myproc()->ofile[fd0] = 0;
801062b8:	e8 53 de ff ff       	call   80104110 <myproc>
801062bd:	c7 44 b0 0c 00 00 00 	movl   $0x0,0xc(%eax,%esi,4)
801062c4:	00 
801062c5:	eb a1                	jmp    80106268 <sys_pipe+0x58>
801062c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801062ce:	66 90                	xchg   %ax,%ax
      curproc->ofile[fd] = f;
801062d0:	89 7c 82 2c          	mov    %edi,0x2c(%edx,%eax,4)
  }
  fd[0] = fd0;
801062d4:	8b 55 dc             	mov    -0x24(%ebp),%edx
801062d7:	89 1a                	mov    %ebx,(%edx)
  fd[1] = fd1;
801062d9:	8b 55 dc             	mov    -0x24(%ebp),%edx
801062dc:	89 42 04             	mov    %eax,0x4(%edx)
  return 0;
801062df:	31 c0                	xor    %eax,%eax
}
801062e1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801062e4:	5b                   	pop    %ebx
801062e5:	5e                   	pop    %esi
801062e6:	5f                   	pop    %edi
801062e7:	5d                   	pop    %ebp
801062e8:	c3                   	ret    
801062e9:	66 90                	xchg   %ax,%ax
801062eb:	66 90                	xchg   %ax,%ax
801062ed:	66 90                	xchg   %ax,%ax
801062ef:	90                   	nop

801062f0 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
801062f0:	f3 0f 1e fb          	endbr32 
  return fork();
801062f4:	e9 c7 df ff ff       	jmp    801042c0 <fork>
801062f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80106300 <sys_exit>:
}

int
sys_exit(void)
{
80106300:	f3 0f 1e fb          	endbr32 
80106304:	55                   	push   %ebp
80106305:	89 e5                	mov    %esp,%ebp
80106307:	83 ec 08             	sub    $0x8,%esp
  exit();
8010630a:	e8 81 e4 ff ff       	call   80104790 <exit>
  return 0;  // not reached
}
8010630f:	31 c0                	xor    %eax,%eax
80106311:	c9                   	leave  
80106312:	c3                   	ret    
80106313:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010631a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106320 <sys_wait>:

int
sys_wait(void)
{
80106320:	f3 0f 1e fb          	endbr32 
  return wait();
80106324:	e9 97 e5 ff ff       	jmp    801048c0 <wait>
80106329:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80106330 <sys_kill>:
}

int
sys_kill(void)
{
80106330:	f3 0f 1e fb          	endbr32 
80106334:	55                   	push   %ebp
80106335:	89 e5                	mov    %esp,%ebp
80106337:	83 ec 20             	sub    $0x20,%esp
  int pid;

  if(argint(0, &pid) < 0)
8010633a:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010633d:	50                   	push   %eax
8010633e:	6a 00                	push   $0x0
80106340:	e8 cb f1 ff ff       	call   80105510 <argint>
80106345:	83 c4 10             	add    $0x10,%esp
80106348:	85 c0                	test   %eax,%eax
8010634a:	78 14                	js     80106360 <sys_kill+0x30>
    return -1;
  return kill(pid);
8010634c:	83 ec 0c             	sub    $0xc,%esp
8010634f:	ff 75 f4             	push   -0xc(%ebp)
80106352:	e8 39 e8 ff ff       	call   80104b90 <kill>
80106357:	83 c4 10             	add    $0x10,%esp
}
8010635a:	c9                   	leave  
8010635b:	c3                   	ret    
8010635c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106360:	c9                   	leave  
    return -1;
80106361:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106366:	c3                   	ret    
80106367:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010636e:	66 90                	xchg   %ax,%ax

80106370 <sys_getpid>:

int
sys_getpid(void)
{
80106370:	f3 0f 1e fb          	endbr32 
80106374:	55                   	push   %ebp
80106375:	89 e5                	mov    %esp,%ebp
80106377:	83 ec 08             	sub    $0x8,%esp
  return myproc()->pid;
8010637a:	e8 91 dd ff ff       	call   80104110 <myproc>
8010637f:	8b 40 10             	mov    0x10(%eax),%eax
}
80106382:	c9                   	leave  
80106383:	c3                   	ret    
80106384:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010638b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010638f:	90                   	nop

80106390 <sys_sbrk>:

int
sys_sbrk(void)
{
80106390:	f3 0f 1e fb          	endbr32 
80106394:	55                   	push   %ebp
80106395:	89 e5                	mov    %esp,%ebp
80106397:	53                   	push   %ebx
  int addr;
  int n;

  if(argint(0, &n) < 0)
80106398:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
8010639b:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
8010639e:	50                   	push   %eax
8010639f:	6a 00                	push   $0x0
801063a1:	e8 6a f1 ff ff       	call   80105510 <argint>
801063a6:	83 c4 10             	add    $0x10,%esp
801063a9:	85 c0                	test   %eax,%eax
801063ab:	78 23                	js     801063d0 <sys_sbrk+0x40>
    return -1;
  addr = myproc()->sz;
801063ad:	e8 5e dd ff ff       	call   80104110 <myproc>
  if(growproc(n) < 0)
801063b2:	83 ec 0c             	sub    $0xc,%esp
  addr = myproc()->sz;
801063b5:	8b 18                	mov    (%eax),%ebx
  if(growproc(n) < 0)
801063b7:	ff 75 f4             	push   -0xc(%ebp)
801063ba:	e8 81 de ff ff       	call   80104240 <growproc>
801063bf:	83 c4 10             	add    $0x10,%esp
801063c2:	85 c0                	test   %eax,%eax
801063c4:	78 0a                	js     801063d0 <sys_sbrk+0x40>
    return -1;
  return addr;
}
801063c6:	89 d8                	mov    %ebx,%eax
801063c8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801063cb:	c9                   	leave  
801063cc:	c3                   	ret    
801063cd:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
801063d0:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801063d5:	eb ef                	jmp    801063c6 <sys_sbrk+0x36>
801063d7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801063de:	66 90                	xchg   %ax,%ax

801063e0 <sys_sleep>:

int
sys_sleep(void)
{
801063e0:	f3 0f 1e fb          	endbr32 
801063e4:	55                   	push   %ebp
801063e5:	89 e5                	mov    %esp,%ebp
801063e7:	53                   	push   %ebx
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
801063e8:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
801063eb:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
801063ee:	50                   	push   %eax
801063ef:	6a 00                	push   $0x0
801063f1:	e8 1a f1 ff ff       	call   80105510 <argint>
801063f6:	83 c4 10             	add    $0x10,%esp
801063f9:	85 c0                	test   %eax,%eax
801063fb:	0f 88 86 00 00 00    	js     80106487 <sys_sleep+0xa7>
    return -1;
  acquire(&tickslock);
80106401:	83 ec 0c             	sub    $0xc,%esp
80106404:	68 80 52 11 80       	push   $0x80115280
80106409:	e8 52 ed ff ff       	call   80105160 <acquire>
  ticks0 = ticks;
  while(ticks - ticks0 < n){
8010640e:	8b 55 f4             	mov    -0xc(%ebp),%edx
  ticks0 = ticks;
80106411:	8b 1d 60 52 11 80    	mov    0x80115260,%ebx
  while(ticks - ticks0 < n){
80106417:	83 c4 10             	add    $0x10,%esp
8010641a:	85 d2                	test   %edx,%edx
8010641c:	75 23                	jne    80106441 <sys_sleep+0x61>
8010641e:	eb 50                	jmp    80106470 <sys_sleep+0x90>
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
80106420:	83 ec 08             	sub    $0x8,%esp
80106423:	68 80 52 11 80       	push   $0x80115280
80106428:	68 60 52 11 80       	push   $0x80115260
8010642d:	e8 2e e6 ff ff       	call   80104a60 <sleep>
  while(ticks - ticks0 < n){
80106432:	a1 60 52 11 80       	mov    0x80115260,%eax
80106437:	83 c4 10             	add    $0x10,%esp
8010643a:	29 d8                	sub    %ebx,%eax
8010643c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
8010643f:	73 2f                	jae    80106470 <sys_sleep+0x90>
    if(myproc()->killed){
80106441:	e8 ca dc ff ff       	call   80104110 <myproc>
80106446:	8b 40 28             	mov    0x28(%eax),%eax
80106449:	85 c0                	test   %eax,%eax
8010644b:	74 d3                	je     80106420 <sys_sleep+0x40>
      release(&tickslock);
8010644d:	83 ec 0c             	sub    $0xc,%esp
80106450:	68 80 52 11 80       	push   $0x80115280
80106455:	e8 96 ec ff ff       	call   801050f0 <release>
  }
  release(&tickslock);
  return 0;
}
8010645a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
      return -1;
8010645d:	83 c4 10             	add    $0x10,%esp
80106460:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106465:	c9                   	leave  
80106466:	c3                   	ret    
80106467:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010646e:	66 90                	xchg   %ax,%ax
  release(&tickslock);
80106470:	83 ec 0c             	sub    $0xc,%esp
80106473:	68 80 52 11 80       	push   $0x80115280
80106478:	e8 73 ec ff ff       	call   801050f0 <release>
  return 0;
8010647d:	83 c4 10             	add    $0x10,%esp
80106480:	31 c0                	xor    %eax,%eax
}
80106482:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80106485:	c9                   	leave  
80106486:	c3                   	ret    
    return -1;
80106487:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010648c:	eb f4                	jmp    80106482 <sys_sleep+0xa2>
8010648e:	66 90                	xchg   %ax,%ax

80106490 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
80106490:	f3 0f 1e fb          	endbr32 
80106494:	55                   	push   %ebp
80106495:	89 e5                	mov    %esp,%ebp
80106497:	53                   	push   %ebx
80106498:	83 ec 10             	sub    $0x10,%esp
  uint xticks;

  acquire(&tickslock);
8010649b:	68 80 52 11 80       	push   $0x80115280
801064a0:	e8 bb ec ff ff       	call   80105160 <acquire>
  xticks = ticks;
801064a5:	8b 1d 60 52 11 80    	mov    0x80115260,%ebx
  release(&tickslock);
801064ab:	c7 04 24 80 52 11 80 	movl   $0x80115280,(%esp)
801064b2:	e8 39 ec ff ff       	call   801050f0 <release>
  return xticks;
}
801064b7:	89 d8                	mov    %ebx,%eax
801064b9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801064bc:	c9                   	leave  
801064bd:	c3                   	ret    
801064be:	66 90                	xchg   %ax,%ax

801064c0 <sys_poweroff>:

// poweroff syscall
int
sys_poweroff(void) {
801064c0:	f3 0f 1e fb          	endbr32 
801064c4:	55                   	push   %ebp
801064c5:	89 e5                	mov    %esp,%ebp
801064c7:	83 ec 14             	sub    $0x14,%esp
  cprintf("sutting down xv6 ...\n");
801064ca:	68 69 87 10 80       	push   $0x80108769
801064cf:	e8 5c a2 ff ff       	call   80100730 <cprintf>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801064d4:	b8 00 20 00 00       	mov    $0x2000,%eax
801064d9:	ba 04 06 00 00       	mov    $0x604,%edx
801064de:	66 ef                	out    %ax,(%dx)
  outw(0x604, 0x2000);
  return 0;
}
801064e0:	c9                   	leave  
801064e1:	31 c0                	xor    %eax,%eax
801064e3:	c3                   	ret    
801064e4:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801064eb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801064ef:	90                   	nop

801064f0 <sys_calculate_sum_of_digits>:

// sum of digits
int
sys_calculate_sum_of_digits(void)
{
801064f0:	f3 0f 1e fb          	endbr32 
801064f4:	55                   	push   %ebp
801064f5:	89 e5                	mov    %esp,%ebp
801064f7:	53                   	push   %ebx
801064f8:	83 ec 04             	sub    $0x4,%esp
  int a = myproc()->tf->ebx; //register after eax
801064fb:	e8 10 dc ff ff       	call   80104110 <myproc>
  cprintf("in kernel systemcall sys_calculate_sum_of_digits() called for number %d\n", a);
80106500:	83 ec 08             	sub    $0x8,%esp
  int a = myproc()->tf->ebx; //register after eax
80106503:	8b 40 1c             	mov    0x1c(%eax),%eax
80106506:	8b 58 10             	mov    0x10(%eax),%ebx
  cprintf("in kernel systemcall sys_calculate_sum_of_digits() called for number %d\n", a);
80106509:	53                   	push   %ebx
8010650a:	68 80 87 10 80       	push   $0x80108780
8010650f:	e8 1c a2 ff ff       	call   80100730 <cprintf>
  return calculate_sum_of_digits(a);
80106514:	89 1c 24             	mov    %ebx,(%esp)
80106517:	e8 e4 e7 ff ff       	call   80104d00 <calculate_sum_of_digits>
}
8010651c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010651f:	c9                   	leave  
80106520:	c3                   	ret    
80106521:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106528:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010652f:	90                   	nop

80106530 <sys_get_parent_pid>:

// get parent proc id
void
sys_get_parent_pid(void)
{
80106530:	f3 0f 1e fb          	endbr32 
80106534:	55                   	push   %ebp
80106535:	89 e5                	mov    %esp,%ebp
80106537:	53                   	push   %ebx
80106538:	83 ec 04             	sub    $0x4,%esp
  int pid = myproc()->tf->ebx; 
8010653b:	e8 d0 db ff ff       	call   80104110 <myproc>
  cprintf("in kernel systemcall sys_get_parent_id() called for pid %d which is curent procces\n", pid);
80106540:	83 ec 08             	sub    $0x8,%esp
  int pid = myproc()->tf->ebx; 
80106543:	8b 40 1c             	mov    0x1c(%eax),%eax
80106546:	8b 58 10             	mov    0x10(%eax),%ebx
  cprintf("in kernel systemcall sys_get_parent_id() called for pid %d which is curent procces\n", pid);
80106549:	53                   	push   %ebx
8010654a:	68 cc 87 10 80       	push   $0x801087cc
8010654f:	e8 dc a1 ff ff       	call   80100730 <cprintf>
  get_parent_pid(pid);
80106554:	89 1c 24             	mov    %ebx,(%esp)
80106557:	e8 f4 e7 ff ff       	call   80104d50 <get_parent_pid>
}
8010655c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010655f:	83 c4 10             	add    $0x10,%esp
80106562:	c9                   	leave  
80106563:	c3                   	ret    
80106564:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010656b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010656f:	90                   	nop

80106570 <sys_set_process_parent>:

// set parent process
int
sys_set_process_parent(void)
{
80106570:	f3 0f 1e fb          	endbr32 
80106574:	55                   	push   %ebp
80106575:	89 e5                	mov    %esp,%ebp
80106577:	83 ec 20             	sub    $0x20,%esp
  int pid;
  if(argint(0, &pid) < 0){
8010657a:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010657d:	50                   	push   %eax
8010657e:	6a 00                	push   $0x0
80106580:	e8 8b ef ff ff       	call   80105510 <argint>
80106585:	83 c4 10             	add    $0x10,%esp
80106588:	85 c0                	test   %eax,%eax
8010658a:	78 14                	js     801065a0 <sys_set_process_parent+0x30>
    return -1;
  }
  return set_process_parent(pid);
8010658c:	83 ec 0c             	sub    $0xc,%esp
8010658f:	ff 75 f4             	push   -0xc(%ebp)
80106592:	e8 19 e8 ff ff       	call   80104db0 <set_process_parent>
80106597:	83 c4 10             	add    $0x10,%esp
}
8010659a:	c9                   	leave  
8010659b:	c3                   	ret    
8010659c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801065a0:	c9                   	leave  
    return -1;
801065a1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801065a6:	c3                   	ret    

801065a7 <alltraps>:
801065a7:	1e                   	push   %ds
801065a8:	06                   	push   %es
801065a9:	0f a0                	push   %fs
801065ab:	0f a8                	push   %gs
801065ad:	60                   	pusha  
801065ae:	66 b8 10 00          	mov    $0x10,%ax
801065b2:	8e d8                	mov    %eax,%ds
801065b4:	8e c0                	mov    %eax,%es
801065b6:	54                   	push   %esp
801065b7:	e8 c4 00 00 00       	call   80106680 <trap>
801065bc:	83 c4 04             	add    $0x4,%esp

801065bf <trapret>:
801065bf:	61                   	popa   
801065c0:	0f a9                	pop    %gs
801065c2:	0f a1                	pop    %fs
801065c4:	07                   	pop    %es
801065c5:	1f                   	pop    %ds
801065c6:	83 c4 08             	add    $0x8,%esp
801065c9:	cf                   	iret   
801065ca:	66 90                	xchg   %ax,%ax
801065cc:	66 90                	xchg   %ax,%ax
801065ce:	66 90                	xchg   %ax,%ax

801065d0 <tvinit>:
struct spinlock tickslock;
uint ticks;

void
tvinit(void)
{
801065d0:	f3 0f 1e fb          	endbr32 
801065d4:	55                   	push   %ebp
  int i;

  for(i = 0; i < 256; i++)
801065d5:	31 c0                	xor    %eax,%eax
{
801065d7:	89 e5                	mov    %esp,%ebp
801065d9:	83 ec 08             	sub    $0x8,%esp
801065dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
801065e0:	8b 14 85 08 b0 10 80 	mov    -0x7fef4ff8(,%eax,4),%edx
801065e7:	c7 04 c5 c2 52 11 80 	movl   $0x8e000008,-0x7feead3e(,%eax,8)
801065ee:	08 00 00 8e 
801065f2:	66 89 14 c5 c0 52 11 	mov    %dx,-0x7feead40(,%eax,8)
801065f9:	80 
801065fa:	c1 ea 10             	shr    $0x10,%edx
801065fd:	66 89 14 c5 c6 52 11 	mov    %dx,-0x7feead3a(,%eax,8)
80106604:	80 
  for(i = 0; i < 256; i++)
80106605:	83 c0 01             	add    $0x1,%eax
80106608:	3d 00 01 00 00       	cmp    $0x100,%eax
8010660d:	75 d1                	jne    801065e0 <tvinit+0x10>
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);

  initlock(&tickslock, "time");
8010660f:	83 ec 08             	sub    $0x8,%esp
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80106612:	a1 08 b1 10 80       	mov    0x8010b108,%eax
80106617:	c7 05 c2 54 11 80 08 	movl   $0xef000008,0x801154c2
8010661e:	00 00 ef 
  initlock(&tickslock, "time");
80106621:	68 20 88 10 80       	push   $0x80108820
80106626:	68 80 52 11 80       	push   $0x80115280
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
8010662b:	66 a3 c0 54 11 80    	mov    %ax,0x801154c0
80106631:	c1 e8 10             	shr    $0x10,%eax
80106634:	66 a3 c6 54 11 80    	mov    %ax,0x801154c6
  initlock(&tickslock, "time");
8010663a:	e8 21 e9 ff ff       	call   80104f60 <initlock>
}
8010663f:	83 c4 10             	add    $0x10,%esp
80106642:	c9                   	leave  
80106643:	c3                   	ret    
80106644:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010664b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010664f:	90                   	nop

80106650 <idtinit>:

void
idtinit(void)
{
80106650:	f3 0f 1e fb          	endbr32 
80106654:	55                   	push   %ebp
  pd[0] = size-1;
80106655:	b8 ff 07 00 00       	mov    $0x7ff,%eax
8010665a:	89 e5                	mov    %esp,%ebp
8010665c:	83 ec 10             	sub    $0x10,%esp
8010665f:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80106663:	b8 c0 52 11 80       	mov    $0x801152c0,%eax
80106668:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
8010666c:	c1 e8 10             	shr    $0x10,%eax
8010666f:	66 89 45 fe          	mov    %ax,-0x2(%ebp)
  asm volatile("lidt (%0)" : : "r" (pd));
80106673:	8d 45 fa             	lea    -0x6(%ebp),%eax
80106676:	0f 01 18             	lidtl  (%eax)
  lidt(idt, sizeof(idt));
}
80106679:	c9                   	leave  
8010667a:	c3                   	ret    
8010667b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010667f:	90                   	nop

80106680 <trap>:

//PAGEBREAK: 41
void
trap(struct trapframe *tf)
{
80106680:	f3 0f 1e fb          	endbr32 
80106684:	55                   	push   %ebp
80106685:	89 e5                	mov    %esp,%ebp
80106687:	57                   	push   %edi
80106688:	56                   	push   %esi
80106689:	53                   	push   %ebx
8010668a:	83 ec 1c             	sub    $0x1c,%esp
8010668d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(tf->trapno == T_SYSCALL){
80106690:	8b 43 30             	mov    0x30(%ebx),%eax
80106693:	83 f8 40             	cmp    $0x40,%eax
80106696:	0f 84 64 01 00 00    	je     80106800 <trap+0x180>
    if(myproc()->killed)
      exit();
    return;
  }

  switch(tf->trapno){
8010669c:	83 e8 20             	sub    $0x20,%eax
8010669f:	83 f8 1f             	cmp    $0x1f,%eax
801066a2:	0f 87 88 00 00 00    	ja     80106730 <trap+0xb0>
801066a8:	3e ff 24 85 c8 88 10 	notrack jmp *-0x7fef7738(,%eax,4)
801066af:	80 
      release(&tickslock);
    }
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE:
    ideintr();
801066b0:	e8 9b c2 ff ff       	call   80102950 <ideintr>
    lapiceoi();
801066b5:	e8 76 c9 ff ff       	call   80103030 <lapiceoi>
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
801066ba:	e8 51 da ff ff       	call   80104110 <myproc>
801066bf:	85 c0                	test   %eax,%eax
801066c1:	74 1d                	je     801066e0 <trap+0x60>
801066c3:	e8 48 da ff ff       	call   80104110 <myproc>
801066c8:	8b 50 28             	mov    0x28(%eax),%edx
801066cb:	85 d2                	test   %edx,%edx
801066cd:	74 11                	je     801066e0 <trap+0x60>
801066cf:	0f b7 43 3c          	movzwl 0x3c(%ebx),%eax
801066d3:	83 e0 03             	and    $0x3,%eax
801066d6:	66 83 f8 03          	cmp    $0x3,%ax
801066da:	0f 84 e8 01 00 00    	je     801068c8 <trap+0x248>
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(myproc() && myproc()->state == RUNNING &&
801066e0:	e8 2b da ff ff       	call   80104110 <myproc>
801066e5:	85 c0                	test   %eax,%eax
801066e7:	74 0f                	je     801066f8 <trap+0x78>
801066e9:	e8 22 da ff ff       	call   80104110 <myproc>
801066ee:	83 78 0c 04          	cmpl   $0x4,0xc(%eax)
801066f2:	0f 84 b8 00 00 00    	je     801067b0 <trap+0x130>
     tf->trapno == T_IRQ0+IRQ_TIMER)
    yield();

  // Check if the process has been killed since we yielded
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
801066f8:	e8 13 da ff ff       	call   80104110 <myproc>
801066fd:	85 c0                	test   %eax,%eax
801066ff:	74 1d                	je     8010671e <trap+0x9e>
80106701:	e8 0a da ff ff       	call   80104110 <myproc>
80106706:	8b 40 28             	mov    0x28(%eax),%eax
80106709:	85 c0                	test   %eax,%eax
8010670b:	74 11                	je     8010671e <trap+0x9e>
8010670d:	0f b7 43 3c          	movzwl 0x3c(%ebx),%eax
80106711:	83 e0 03             	and    $0x3,%eax
80106714:	66 83 f8 03          	cmp    $0x3,%ax
80106718:	0f 84 0f 01 00 00    	je     8010682d <trap+0x1ad>
    exit();
}
8010671e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106721:	5b                   	pop    %ebx
80106722:	5e                   	pop    %esi
80106723:	5f                   	pop    %edi
80106724:	5d                   	pop    %ebp
80106725:	c3                   	ret    
80106726:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010672d:	8d 76 00             	lea    0x0(%esi),%esi
    if(myproc() == 0 || (tf->cs&3) == 0){
80106730:	e8 db d9 ff ff       	call   80104110 <myproc>
80106735:	8b 7b 38             	mov    0x38(%ebx),%edi
80106738:	85 c0                	test   %eax,%eax
8010673a:	0f 84 a2 01 00 00    	je     801068e2 <trap+0x262>
80106740:	f6 43 3c 03          	testb  $0x3,0x3c(%ebx)
80106744:	0f 84 98 01 00 00    	je     801068e2 <trap+0x262>

static inline uint
rcr2(void)
{
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
8010674a:	0f 20 d1             	mov    %cr2,%ecx
8010674d:	89 4d d8             	mov    %ecx,-0x28(%ebp)
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106750:	e8 9b d9 ff ff       	call   801040f0 <cpuid>
80106755:	8b 73 30             	mov    0x30(%ebx),%esi
80106758:	89 45 dc             	mov    %eax,-0x24(%ebp)
8010675b:	8b 43 34             	mov    0x34(%ebx),%eax
8010675e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            myproc()->pid, myproc()->name, tf->trapno,
80106761:	e8 aa d9 ff ff       	call   80104110 <myproc>
80106766:	89 45 e0             	mov    %eax,-0x20(%ebp)
80106769:	e8 a2 d9 ff ff       	call   80104110 <myproc>
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010676e:	8b 4d d8             	mov    -0x28(%ebp),%ecx
80106771:	8b 55 dc             	mov    -0x24(%ebp),%edx
80106774:	51                   	push   %ecx
80106775:	57                   	push   %edi
80106776:	52                   	push   %edx
80106777:	ff 75 e4             	push   -0x1c(%ebp)
8010677a:	56                   	push   %esi
            myproc()->pid, myproc()->name, tf->trapno,
8010677b:	8b 75 e0             	mov    -0x20(%ebp),%esi
8010677e:	83 c6 70             	add    $0x70,%esi
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106781:	56                   	push   %esi
80106782:	ff 70 10             	push   0x10(%eax)
80106785:	68 84 88 10 80       	push   $0x80108884
8010678a:	e8 a1 9f ff ff       	call   80100730 <cprintf>
    myproc()->killed = 1;
8010678f:	83 c4 20             	add    $0x20,%esp
80106792:	e8 79 d9 ff ff       	call   80104110 <myproc>
80106797:	c7 40 28 01 00 00 00 	movl   $0x1,0x28(%eax)
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
8010679e:	e8 6d d9 ff ff       	call   80104110 <myproc>
801067a3:	85 c0                	test   %eax,%eax
801067a5:	0f 85 18 ff ff ff    	jne    801066c3 <trap+0x43>
801067ab:	e9 30 ff ff ff       	jmp    801066e0 <trap+0x60>
  if(myproc() && myproc()->state == RUNNING &&
801067b0:	83 7b 30 20          	cmpl   $0x20,0x30(%ebx)
801067b4:	0f 85 3e ff ff ff    	jne    801066f8 <trap+0x78>
    yield();
801067ba:	e8 51 e2 ff ff       	call   80104a10 <yield>
801067bf:	e9 34 ff ff ff       	jmp    801066f8 <trap+0x78>
801067c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801067c8:	8b 7b 38             	mov    0x38(%ebx),%edi
801067cb:	0f b7 73 3c          	movzwl 0x3c(%ebx),%esi
801067cf:	e8 1c d9 ff ff       	call   801040f0 <cpuid>
801067d4:	57                   	push   %edi
801067d5:	56                   	push   %esi
801067d6:	50                   	push   %eax
801067d7:	68 2c 88 10 80       	push   $0x8010882c
801067dc:	e8 4f 9f ff ff       	call   80100730 <cprintf>
    lapiceoi();
801067e1:	e8 4a c8 ff ff       	call   80103030 <lapiceoi>
    break;
801067e6:	83 c4 10             	add    $0x10,%esp
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
801067e9:	e8 22 d9 ff ff       	call   80104110 <myproc>
801067ee:	85 c0                	test   %eax,%eax
801067f0:	0f 85 cd fe ff ff    	jne    801066c3 <trap+0x43>
801067f6:	e9 e5 fe ff ff       	jmp    801066e0 <trap+0x60>
801067fb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801067ff:	90                   	nop
    if(myproc()->killed)
80106800:	e8 0b d9 ff ff       	call   80104110 <myproc>
80106805:	8b 70 28             	mov    0x28(%eax),%esi
80106808:	85 f6                	test   %esi,%esi
8010680a:	0f 85 c8 00 00 00    	jne    801068d8 <trap+0x258>
    myproc()->tf = tf;
80106810:	e8 fb d8 ff ff       	call   80104110 <myproc>
80106815:	89 58 1c             	mov    %ebx,0x1c(%eax)
    syscall();
80106818:	e8 33 ee ff ff       	call   80105650 <syscall>
    if(myproc()->killed)
8010681d:	e8 ee d8 ff ff       	call   80104110 <myproc>
80106822:	8b 48 28             	mov    0x28(%eax),%ecx
80106825:	85 c9                	test   %ecx,%ecx
80106827:	0f 84 f1 fe ff ff    	je     8010671e <trap+0x9e>
}
8010682d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106830:	5b                   	pop    %ebx
80106831:	5e                   	pop    %esi
80106832:	5f                   	pop    %edi
80106833:	5d                   	pop    %ebp
      exit();
80106834:	e9 57 df ff ff       	jmp    80104790 <exit>
80106839:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    uartintr();
80106840:	e8 5b 02 00 00       	call   80106aa0 <uartintr>
    lapiceoi();
80106845:	e8 e6 c7 ff ff       	call   80103030 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
8010684a:	e8 c1 d8 ff ff       	call   80104110 <myproc>
8010684f:	85 c0                	test   %eax,%eax
80106851:	0f 85 6c fe ff ff    	jne    801066c3 <trap+0x43>
80106857:	e9 84 fe ff ff       	jmp    801066e0 <trap+0x60>
8010685c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    kbdintr();
80106860:	e8 8b c6 ff ff       	call   80102ef0 <kbdintr>
    lapiceoi();
80106865:	e8 c6 c7 ff ff       	call   80103030 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
8010686a:	e8 a1 d8 ff ff       	call   80104110 <myproc>
8010686f:	85 c0                	test   %eax,%eax
80106871:	0f 85 4c fe ff ff    	jne    801066c3 <trap+0x43>
80106877:	e9 64 fe ff ff       	jmp    801066e0 <trap+0x60>
8010687c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(cpuid() == 0){
80106880:	e8 6b d8 ff ff       	call   801040f0 <cpuid>
80106885:	85 c0                	test   %eax,%eax
80106887:	0f 85 28 fe ff ff    	jne    801066b5 <trap+0x35>
      acquire(&tickslock);
8010688d:	83 ec 0c             	sub    $0xc,%esp
80106890:	68 80 52 11 80       	push   $0x80115280
80106895:	e8 c6 e8 ff ff       	call   80105160 <acquire>
      wakeup(&ticks);
8010689a:	c7 04 24 60 52 11 80 	movl   $0x80115260,(%esp)
      ticks++;
801068a1:	83 05 60 52 11 80 01 	addl   $0x1,0x80115260
      wakeup(&ticks);
801068a8:	e8 73 e2 ff ff       	call   80104b20 <wakeup>
      release(&tickslock);
801068ad:	c7 04 24 80 52 11 80 	movl   $0x80115280,(%esp)
801068b4:	e8 37 e8 ff ff       	call   801050f0 <release>
801068b9:	83 c4 10             	add    $0x10,%esp
    lapiceoi();
801068bc:	e9 f4 fd ff ff       	jmp    801066b5 <trap+0x35>
801068c1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    exit();
801068c8:	e8 c3 de ff ff       	call   80104790 <exit>
801068cd:	e9 0e fe ff ff       	jmp    801066e0 <trap+0x60>
801068d2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      exit();
801068d8:	e8 b3 de ff ff       	call   80104790 <exit>
801068dd:	e9 2e ff ff ff       	jmp    80106810 <trap+0x190>
801068e2:	0f 20 d6             	mov    %cr2,%esi
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
801068e5:	e8 06 d8 ff ff       	call   801040f0 <cpuid>
801068ea:	83 ec 0c             	sub    $0xc,%esp
801068ed:	56                   	push   %esi
801068ee:	57                   	push   %edi
801068ef:	50                   	push   %eax
801068f0:	ff 73 30             	push   0x30(%ebx)
801068f3:	68 50 88 10 80       	push   $0x80108850
801068f8:	e8 33 9e ff ff       	call   80100730 <cprintf>
      panic("trap");
801068fd:	83 c4 14             	add    $0x14,%esp
80106900:	68 25 88 10 80       	push   $0x80108825
80106905:	e8 86 9a ff ff       	call   80100390 <panic>
8010690a:	66 90                	xchg   %ax,%ax
8010690c:	66 90                	xchg   %ax,%ax
8010690e:	66 90                	xchg   %ax,%ax

80106910 <uartgetc>:
  outb(COM1+0, c);
}

static int
uartgetc(void)
{
80106910:	f3 0f 1e fb          	endbr32 
  if(!uart)
80106914:	a1 c0 5a 11 80       	mov    0x80115ac0,%eax
80106919:	85 c0                	test   %eax,%eax
8010691b:	74 1b                	je     80106938 <uartgetc+0x28>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010691d:	ba fd 03 00 00       	mov    $0x3fd,%edx
80106922:	ec                   	in     (%dx),%al
    return -1;
  if(!(inb(COM1+5) & 0x01))
80106923:	a8 01                	test   $0x1,%al
80106925:	74 11                	je     80106938 <uartgetc+0x28>
80106927:	ba f8 03 00 00       	mov    $0x3f8,%edx
8010692c:	ec                   	in     (%dx),%al
    return -1;
  return inb(COM1+0);
8010692d:	0f b6 c0             	movzbl %al,%eax
80106930:	c3                   	ret    
80106931:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80106938:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010693d:	c3                   	ret    
8010693e:	66 90                	xchg   %ax,%ax

80106940 <uartinit>:
{
80106940:	f3 0f 1e fb          	endbr32 
80106944:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80106945:	31 c9                	xor    %ecx,%ecx
80106947:	89 c8                	mov    %ecx,%eax
80106949:	89 e5                	mov    %esp,%ebp
8010694b:	57                   	push   %edi
8010694c:	bf fa 03 00 00       	mov    $0x3fa,%edi
80106951:	56                   	push   %esi
80106952:	89 fa                	mov    %edi,%edx
80106954:	53                   	push   %ebx
80106955:	83 ec 1c             	sub    $0x1c,%esp
80106958:	ee                   	out    %al,(%dx)
80106959:	be fb 03 00 00       	mov    $0x3fb,%esi
8010695e:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
80106963:	89 f2                	mov    %esi,%edx
80106965:	ee                   	out    %al,(%dx)
80106966:	b8 0c 00 00 00       	mov    $0xc,%eax
8010696b:	ba f8 03 00 00       	mov    $0x3f8,%edx
80106970:	ee                   	out    %al,(%dx)
80106971:	bb f9 03 00 00       	mov    $0x3f9,%ebx
80106976:	89 c8                	mov    %ecx,%eax
80106978:	89 da                	mov    %ebx,%edx
8010697a:	ee                   	out    %al,(%dx)
8010697b:	b8 03 00 00 00       	mov    $0x3,%eax
80106980:	89 f2                	mov    %esi,%edx
80106982:	ee                   	out    %al,(%dx)
80106983:	ba fc 03 00 00       	mov    $0x3fc,%edx
80106988:	89 c8                	mov    %ecx,%eax
8010698a:	ee                   	out    %al,(%dx)
8010698b:	b8 01 00 00 00       	mov    $0x1,%eax
80106990:	89 da                	mov    %ebx,%edx
80106992:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80106993:	ba fd 03 00 00       	mov    $0x3fd,%edx
80106998:	ec                   	in     (%dx),%al
  if(inb(COM1+5) == 0xFF)
80106999:	3c ff                	cmp    $0xff,%al
8010699b:	0f 84 8f 00 00 00    	je     80106a30 <uartinit+0xf0>
  uart = 1;
801069a1:	c7 05 c0 5a 11 80 01 	movl   $0x1,0x80115ac0
801069a8:	00 00 00 
801069ab:	89 fa                	mov    %edi,%edx
801069ad:	ec                   	in     (%dx),%al
801069ae:	ba f8 03 00 00       	mov    $0x3f8,%edx
801069b3:	ec                   	in     (%dx),%al
  ioapicenable(IRQ_COM1, 0);
801069b4:	83 ec 08             	sub    $0x8,%esp
  for(p="xv6...\n"; *p; p++)
801069b7:	bf 48 89 10 80       	mov    $0x80108948,%edi
801069bc:	be fd 03 00 00       	mov    $0x3fd,%esi
  ioapicenable(IRQ_COM1, 0);
801069c1:	6a 00                	push   $0x0
801069c3:	6a 04                	push   $0x4
801069c5:	e8 d6 c1 ff ff       	call   80102ba0 <ioapicenable>
801069ca:	c6 45 e7 76          	movb   $0x76,-0x19(%ebp)
801069ce:	83 c4 10             	add    $0x10,%esp
  for(p="xv6...\n"; *p; p++)
801069d1:	c6 45 e6 78          	movb   $0x78,-0x1a(%ebp)
801069d5:	8d 76 00             	lea    0x0(%esi),%esi
  if(!uart)
801069d8:	a1 c0 5a 11 80       	mov    0x80115ac0,%eax
801069dd:	bb 80 00 00 00       	mov    $0x80,%ebx
801069e2:	85 c0                	test   %eax,%eax
801069e4:	75 1c                	jne    80106a02 <uartinit+0xc2>
801069e6:	eb 2b                	jmp    80106a13 <uartinit+0xd3>
801069e8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801069ef:	90                   	nop
    microdelay(10);
801069f0:	83 ec 0c             	sub    $0xc,%esp
801069f3:	6a 0a                	push   $0xa
801069f5:	e8 56 c6 ff ff       	call   80103050 <microdelay>
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
801069fa:	83 c4 10             	add    $0x10,%esp
801069fd:	83 eb 01             	sub    $0x1,%ebx
80106a00:	74 07                	je     80106a09 <uartinit+0xc9>
80106a02:	89 f2                	mov    %esi,%edx
80106a04:	ec                   	in     (%dx),%al
80106a05:	a8 20                	test   $0x20,%al
80106a07:	74 e7                	je     801069f0 <uartinit+0xb0>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80106a09:	0f b6 45 e6          	movzbl -0x1a(%ebp),%eax
80106a0d:	ba f8 03 00 00       	mov    $0x3f8,%edx
80106a12:	ee                   	out    %al,(%dx)
  for(p="xv6...\n"; *p; p++)
80106a13:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
80106a17:	83 c7 01             	add    $0x1,%edi
80106a1a:	84 c0                	test   %al,%al
80106a1c:	74 12                	je     80106a30 <uartinit+0xf0>
80106a1e:	88 45 e6             	mov    %al,-0x1a(%ebp)
80106a21:	0f b6 47 01          	movzbl 0x1(%edi),%eax
80106a25:	88 45 e7             	mov    %al,-0x19(%ebp)
80106a28:	eb ae                	jmp    801069d8 <uartinit+0x98>
80106a2a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
}
80106a30:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106a33:	5b                   	pop    %ebx
80106a34:	5e                   	pop    %esi
80106a35:	5f                   	pop    %edi
80106a36:	5d                   	pop    %ebp
80106a37:	c3                   	ret    
80106a38:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a3f:	90                   	nop

80106a40 <uartputc>:
{
80106a40:	f3 0f 1e fb          	endbr32 
  if(!uart)
80106a44:	a1 c0 5a 11 80       	mov    0x80115ac0,%eax
80106a49:	85 c0                	test   %eax,%eax
80106a4b:	74 43                	je     80106a90 <uartputc+0x50>
{
80106a4d:	55                   	push   %ebp
80106a4e:	89 e5                	mov    %esp,%ebp
80106a50:	56                   	push   %esi
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80106a51:	be fd 03 00 00       	mov    $0x3fd,%esi
80106a56:	53                   	push   %ebx
80106a57:	bb 80 00 00 00       	mov    $0x80,%ebx
80106a5c:	eb 14                	jmp    80106a72 <uartputc+0x32>
80106a5e:	66 90                	xchg   %ax,%ax
    microdelay(10);
80106a60:	83 ec 0c             	sub    $0xc,%esp
80106a63:	6a 0a                	push   $0xa
80106a65:	e8 e6 c5 ff ff       	call   80103050 <microdelay>
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
80106a6a:	83 c4 10             	add    $0x10,%esp
80106a6d:	83 eb 01             	sub    $0x1,%ebx
80106a70:	74 07                	je     80106a79 <uartputc+0x39>
80106a72:	89 f2                	mov    %esi,%edx
80106a74:	ec                   	in     (%dx),%al
80106a75:	a8 20                	test   $0x20,%al
80106a77:	74 e7                	je     80106a60 <uartputc+0x20>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80106a79:	8b 45 08             	mov    0x8(%ebp),%eax
80106a7c:	ba f8 03 00 00       	mov    $0x3f8,%edx
80106a81:	ee                   	out    %al,(%dx)
}
80106a82:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106a85:	5b                   	pop    %ebx
80106a86:	5e                   	pop    %esi
80106a87:	5d                   	pop    %ebp
80106a88:	c3                   	ret    
80106a89:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a90:	c3                   	ret    
80106a91:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a98:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a9f:	90                   	nop

80106aa0 <uartintr>:

void
uartintr(void)
{
80106aa0:	f3 0f 1e fb          	endbr32 
80106aa4:	55                   	push   %ebp
80106aa5:	89 e5                	mov    %esp,%ebp
80106aa7:	83 ec 14             	sub    $0x14,%esp
  consoleintr(uartgetc);
80106aaa:	68 10 69 10 80       	push   $0x80106910
80106aaf:	e8 0c a0 ff ff       	call   80100ac0 <consoleintr>
}
80106ab4:	83 c4 10             	add    $0x10,%esp
80106ab7:	c9                   	leave  
80106ab8:	c3                   	ret    

80106ab9 <vector0>:
80106ab9:	6a 00                	push   $0x0
80106abb:	6a 00                	push   $0x0
80106abd:	e9 e5 fa ff ff       	jmp    801065a7 <alltraps>

80106ac2 <vector1>:
80106ac2:	6a 00                	push   $0x0
80106ac4:	6a 01                	push   $0x1
80106ac6:	e9 dc fa ff ff       	jmp    801065a7 <alltraps>

80106acb <vector2>:
80106acb:	6a 00                	push   $0x0
80106acd:	6a 02                	push   $0x2
80106acf:	e9 d3 fa ff ff       	jmp    801065a7 <alltraps>

80106ad4 <vector3>:
80106ad4:	6a 00                	push   $0x0
80106ad6:	6a 03                	push   $0x3
80106ad8:	e9 ca fa ff ff       	jmp    801065a7 <alltraps>

80106add <vector4>:
80106add:	6a 00                	push   $0x0
80106adf:	6a 04                	push   $0x4
80106ae1:	e9 c1 fa ff ff       	jmp    801065a7 <alltraps>

80106ae6 <vector5>:
80106ae6:	6a 00                	push   $0x0
80106ae8:	6a 05                	push   $0x5
80106aea:	e9 b8 fa ff ff       	jmp    801065a7 <alltraps>

80106aef <vector6>:
80106aef:	6a 00                	push   $0x0
80106af1:	6a 06                	push   $0x6
80106af3:	e9 af fa ff ff       	jmp    801065a7 <alltraps>

80106af8 <vector7>:
80106af8:	6a 00                	push   $0x0
80106afa:	6a 07                	push   $0x7
80106afc:	e9 a6 fa ff ff       	jmp    801065a7 <alltraps>

80106b01 <vector8>:
80106b01:	6a 08                	push   $0x8
80106b03:	e9 9f fa ff ff       	jmp    801065a7 <alltraps>

80106b08 <vector9>:
80106b08:	6a 00                	push   $0x0
80106b0a:	6a 09                	push   $0x9
80106b0c:	e9 96 fa ff ff       	jmp    801065a7 <alltraps>

80106b11 <vector10>:
80106b11:	6a 0a                	push   $0xa
80106b13:	e9 8f fa ff ff       	jmp    801065a7 <alltraps>

80106b18 <vector11>:
80106b18:	6a 0b                	push   $0xb
80106b1a:	e9 88 fa ff ff       	jmp    801065a7 <alltraps>

80106b1f <vector12>:
80106b1f:	6a 0c                	push   $0xc
80106b21:	e9 81 fa ff ff       	jmp    801065a7 <alltraps>

80106b26 <vector13>:
80106b26:	6a 0d                	push   $0xd
80106b28:	e9 7a fa ff ff       	jmp    801065a7 <alltraps>

80106b2d <vector14>:
80106b2d:	6a 0e                	push   $0xe
80106b2f:	e9 73 fa ff ff       	jmp    801065a7 <alltraps>

80106b34 <vector15>:
80106b34:	6a 00                	push   $0x0
80106b36:	6a 0f                	push   $0xf
80106b38:	e9 6a fa ff ff       	jmp    801065a7 <alltraps>

80106b3d <vector16>:
80106b3d:	6a 00                	push   $0x0
80106b3f:	6a 10                	push   $0x10
80106b41:	e9 61 fa ff ff       	jmp    801065a7 <alltraps>

80106b46 <vector17>:
80106b46:	6a 11                	push   $0x11
80106b48:	e9 5a fa ff ff       	jmp    801065a7 <alltraps>

80106b4d <vector18>:
80106b4d:	6a 00                	push   $0x0
80106b4f:	6a 12                	push   $0x12
80106b51:	e9 51 fa ff ff       	jmp    801065a7 <alltraps>

80106b56 <vector19>:
80106b56:	6a 00                	push   $0x0
80106b58:	6a 13                	push   $0x13
80106b5a:	e9 48 fa ff ff       	jmp    801065a7 <alltraps>

80106b5f <vector20>:
80106b5f:	6a 00                	push   $0x0
80106b61:	6a 14                	push   $0x14
80106b63:	e9 3f fa ff ff       	jmp    801065a7 <alltraps>

80106b68 <vector21>:
80106b68:	6a 00                	push   $0x0
80106b6a:	6a 15                	push   $0x15
80106b6c:	e9 36 fa ff ff       	jmp    801065a7 <alltraps>

80106b71 <vector22>:
80106b71:	6a 00                	push   $0x0
80106b73:	6a 16                	push   $0x16
80106b75:	e9 2d fa ff ff       	jmp    801065a7 <alltraps>

80106b7a <vector23>:
80106b7a:	6a 00                	push   $0x0
80106b7c:	6a 17                	push   $0x17
80106b7e:	e9 24 fa ff ff       	jmp    801065a7 <alltraps>

80106b83 <vector24>:
80106b83:	6a 00                	push   $0x0
80106b85:	6a 18                	push   $0x18
80106b87:	e9 1b fa ff ff       	jmp    801065a7 <alltraps>

80106b8c <vector25>:
80106b8c:	6a 00                	push   $0x0
80106b8e:	6a 19                	push   $0x19
80106b90:	e9 12 fa ff ff       	jmp    801065a7 <alltraps>

80106b95 <vector26>:
80106b95:	6a 00                	push   $0x0
80106b97:	6a 1a                	push   $0x1a
80106b99:	e9 09 fa ff ff       	jmp    801065a7 <alltraps>

80106b9e <vector27>:
80106b9e:	6a 00                	push   $0x0
80106ba0:	6a 1b                	push   $0x1b
80106ba2:	e9 00 fa ff ff       	jmp    801065a7 <alltraps>

80106ba7 <vector28>:
80106ba7:	6a 00                	push   $0x0
80106ba9:	6a 1c                	push   $0x1c
80106bab:	e9 f7 f9 ff ff       	jmp    801065a7 <alltraps>

80106bb0 <vector29>:
80106bb0:	6a 00                	push   $0x0
80106bb2:	6a 1d                	push   $0x1d
80106bb4:	e9 ee f9 ff ff       	jmp    801065a7 <alltraps>

80106bb9 <vector30>:
80106bb9:	6a 00                	push   $0x0
80106bbb:	6a 1e                	push   $0x1e
80106bbd:	e9 e5 f9 ff ff       	jmp    801065a7 <alltraps>

80106bc2 <vector31>:
80106bc2:	6a 00                	push   $0x0
80106bc4:	6a 1f                	push   $0x1f
80106bc6:	e9 dc f9 ff ff       	jmp    801065a7 <alltraps>

80106bcb <vector32>:
80106bcb:	6a 00                	push   $0x0
80106bcd:	6a 20                	push   $0x20
80106bcf:	e9 d3 f9 ff ff       	jmp    801065a7 <alltraps>

80106bd4 <vector33>:
80106bd4:	6a 00                	push   $0x0
80106bd6:	6a 21                	push   $0x21
80106bd8:	e9 ca f9 ff ff       	jmp    801065a7 <alltraps>

80106bdd <vector34>:
80106bdd:	6a 00                	push   $0x0
80106bdf:	6a 22                	push   $0x22
80106be1:	e9 c1 f9 ff ff       	jmp    801065a7 <alltraps>

80106be6 <vector35>:
80106be6:	6a 00                	push   $0x0
80106be8:	6a 23                	push   $0x23
80106bea:	e9 b8 f9 ff ff       	jmp    801065a7 <alltraps>

80106bef <vector36>:
80106bef:	6a 00                	push   $0x0
80106bf1:	6a 24                	push   $0x24
80106bf3:	e9 af f9 ff ff       	jmp    801065a7 <alltraps>

80106bf8 <vector37>:
80106bf8:	6a 00                	push   $0x0
80106bfa:	6a 25                	push   $0x25
80106bfc:	e9 a6 f9 ff ff       	jmp    801065a7 <alltraps>

80106c01 <vector38>:
80106c01:	6a 00                	push   $0x0
80106c03:	6a 26                	push   $0x26
80106c05:	e9 9d f9 ff ff       	jmp    801065a7 <alltraps>

80106c0a <vector39>:
80106c0a:	6a 00                	push   $0x0
80106c0c:	6a 27                	push   $0x27
80106c0e:	e9 94 f9 ff ff       	jmp    801065a7 <alltraps>

80106c13 <vector40>:
80106c13:	6a 00                	push   $0x0
80106c15:	6a 28                	push   $0x28
80106c17:	e9 8b f9 ff ff       	jmp    801065a7 <alltraps>

80106c1c <vector41>:
80106c1c:	6a 00                	push   $0x0
80106c1e:	6a 29                	push   $0x29
80106c20:	e9 82 f9 ff ff       	jmp    801065a7 <alltraps>

80106c25 <vector42>:
80106c25:	6a 00                	push   $0x0
80106c27:	6a 2a                	push   $0x2a
80106c29:	e9 79 f9 ff ff       	jmp    801065a7 <alltraps>

80106c2e <vector43>:
80106c2e:	6a 00                	push   $0x0
80106c30:	6a 2b                	push   $0x2b
80106c32:	e9 70 f9 ff ff       	jmp    801065a7 <alltraps>

80106c37 <vector44>:
80106c37:	6a 00                	push   $0x0
80106c39:	6a 2c                	push   $0x2c
80106c3b:	e9 67 f9 ff ff       	jmp    801065a7 <alltraps>

80106c40 <vector45>:
80106c40:	6a 00                	push   $0x0
80106c42:	6a 2d                	push   $0x2d
80106c44:	e9 5e f9 ff ff       	jmp    801065a7 <alltraps>

80106c49 <vector46>:
80106c49:	6a 00                	push   $0x0
80106c4b:	6a 2e                	push   $0x2e
80106c4d:	e9 55 f9 ff ff       	jmp    801065a7 <alltraps>

80106c52 <vector47>:
80106c52:	6a 00                	push   $0x0
80106c54:	6a 2f                	push   $0x2f
80106c56:	e9 4c f9 ff ff       	jmp    801065a7 <alltraps>

80106c5b <vector48>:
80106c5b:	6a 00                	push   $0x0
80106c5d:	6a 30                	push   $0x30
80106c5f:	e9 43 f9 ff ff       	jmp    801065a7 <alltraps>

80106c64 <vector49>:
80106c64:	6a 00                	push   $0x0
80106c66:	6a 31                	push   $0x31
80106c68:	e9 3a f9 ff ff       	jmp    801065a7 <alltraps>

80106c6d <vector50>:
80106c6d:	6a 00                	push   $0x0
80106c6f:	6a 32                	push   $0x32
80106c71:	e9 31 f9 ff ff       	jmp    801065a7 <alltraps>

80106c76 <vector51>:
80106c76:	6a 00                	push   $0x0
80106c78:	6a 33                	push   $0x33
80106c7a:	e9 28 f9 ff ff       	jmp    801065a7 <alltraps>

80106c7f <vector52>:
80106c7f:	6a 00                	push   $0x0
80106c81:	6a 34                	push   $0x34
80106c83:	e9 1f f9 ff ff       	jmp    801065a7 <alltraps>

80106c88 <vector53>:
80106c88:	6a 00                	push   $0x0
80106c8a:	6a 35                	push   $0x35
80106c8c:	e9 16 f9 ff ff       	jmp    801065a7 <alltraps>

80106c91 <vector54>:
80106c91:	6a 00                	push   $0x0
80106c93:	6a 36                	push   $0x36
80106c95:	e9 0d f9 ff ff       	jmp    801065a7 <alltraps>

80106c9a <vector55>:
80106c9a:	6a 00                	push   $0x0
80106c9c:	6a 37                	push   $0x37
80106c9e:	e9 04 f9 ff ff       	jmp    801065a7 <alltraps>

80106ca3 <vector56>:
80106ca3:	6a 00                	push   $0x0
80106ca5:	6a 38                	push   $0x38
80106ca7:	e9 fb f8 ff ff       	jmp    801065a7 <alltraps>

80106cac <vector57>:
80106cac:	6a 00                	push   $0x0
80106cae:	6a 39                	push   $0x39
80106cb0:	e9 f2 f8 ff ff       	jmp    801065a7 <alltraps>

80106cb5 <vector58>:
80106cb5:	6a 00                	push   $0x0
80106cb7:	6a 3a                	push   $0x3a
80106cb9:	e9 e9 f8 ff ff       	jmp    801065a7 <alltraps>

80106cbe <vector59>:
80106cbe:	6a 00                	push   $0x0
80106cc0:	6a 3b                	push   $0x3b
80106cc2:	e9 e0 f8 ff ff       	jmp    801065a7 <alltraps>

80106cc7 <vector60>:
80106cc7:	6a 00                	push   $0x0
80106cc9:	6a 3c                	push   $0x3c
80106ccb:	e9 d7 f8 ff ff       	jmp    801065a7 <alltraps>

80106cd0 <vector61>:
80106cd0:	6a 00                	push   $0x0
80106cd2:	6a 3d                	push   $0x3d
80106cd4:	e9 ce f8 ff ff       	jmp    801065a7 <alltraps>

80106cd9 <vector62>:
80106cd9:	6a 00                	push   $0x0
80106cdb:	6a 3e                	push   $0x3e
80106cdd:	e9 c5 f8 ff ff       	jmp    801065a7 <alltraps>

80106ce2 <vector63>:
80106ce2:	6a 00                	push   $0x0
80106ce4:	6a 3f                	push   $0x3f
80106ce6:	e9 bc f8 ff ff       	jmp    801065a7 <alltraps>

80106ceb <vector64>:
80106ceb:	6a 00                	push   $0x0
80106ced:	6a 40                	push   $0x40
80106cef:	e9 b3 f8 ff ff       	jmp    801065a7 <alltraps>

80106cf4 <vector65>:
80106cf4:	6a 00                	push   $0x0
80106cf6:	6a 41                	push   $0x41
80106cf8:	e9 aa f8 ff ff       	jmp    801065a7 <alltraps>

80106cfd <vector66>:
80106cfd:	6a 00                	push   $0x0
80106cff:	6a 42                	push   $0x42
80106d01:	e9 a1 f8 ff ff       	jmp    801065a7 <alltraps>

80106d06 <vector67>:
80106d06:	6a 00                	push   $0x0
80106d08:	6a 43                	push   $0x43
80106d0a:	e9 98 f8 ff ff       	jmp    801065a7 <alltraps>

80106d0f <vector68>:
80106d0f:	6a 00                	push   $0x0
80106d11:	6a 44                	push   $0x44
80106d13:	e9 8f f8 ff ff       	jmp    801065a7 <alltraps>

80106d18 <vector69>:
80106d18:	6a 00                	push   $0x0
80106d1a:	6a 45                	push   $0x45
80106d1c:	e9 86 f8 ff ff       	jmp    801065a7 <alltraps>

80106d21 <vector70>:
80106d21:	6a 00                	push   $0x0
80106d23:	6a 46                	push   $0x46
80106d25:	e9 7d f8 ff ff       	jmp    801065a7 <alltraps>

80106d2a <vector71>:
80106d2a:	6a 00                	push   $0x0
80106d2c:	6a 47                	push   $0x47
80106d2e:	e9 74 f8 ff ff       	jmp    801065a7 <alltraps>

80106d33 <vector72>:
80106d33:	6a 00                	push   $0x0
80106d35:	6a 48                	push   $0x48
80106d37:	e9 6b f8 ff ff       	jmp    801065a7 <alltraps>

80106d3c <vector73>:
80106d3c:	6a 00                	push   $0x0
80106d3e:	6a 49                	push   $0x49
80106d40:	e9 62 f8 ff ff       	jmp    801065a7 <alltraps>

80106d45 <vector74>:
80106d45:	6a 00                	push   $0x0
80106d47:	6a 4a                	push   $0x4a
80106d49:	e9 59 f8 ff ff       	jmp    801065a7 <alltraps>

80106d4e <vector75>:
80106d4e:	6a 00                	push   $0x0
80106d50:	6a 4b                	push   $0x4b
80106d52:	e9 50 f8 ff ff       	jmp    801065a7 <alltraps>

80106d57 <vector76>:
80106d57:	6a 00                	push   $0x0
80106d59:	6a 4c                	push   $0x4c
80106d5b:	e9 47 f8 ff ff       	jmp    801065a7 <alltraps>

80106d60 <vector77>:
80106d60:	6a 00                	push   $0x0
80106d62:	6a 4d                	push   $0x4d
80106d64:	e9 3e f8 ff ff       	jmp    801065a7 <alltraps>

80106d69 <vector78>:
80106d69:	6a 00                	push   $0x0
80106d6b:	6a 4e                	push   $0x4e
80106d6d:	e9 35 f8 ff ff       	jmp    801065a7 <alltraps>

80106d72 <vector79>:
80106d72:	6a 00                	push   $0x0
80106d74:	6a 4f                	push   $0x4f
80106d76:	e9 2c f8 ff ff       	jmp    801065a7 <alltraps>

80106d7b <vector80>:
80106d7b:	6a 00                	push   $0x0
80106d7d:	6a 50                	push   $0x50
80106d7f:	e9 23 f8 ff ff       	jmp    801065a7 <alltraps>

80106d84 <vector81>:
80106d84:	6a 00                	push   $0x0
80106d86:	6a 51                	push   $0x51
80106d88:	e9 1a f8 ff ff       	jmp    801065a7 <alltraps>

80106d8d <vector82>:
80106d8d:	6a 00                	push   $0x0
80106d8f:	6a 52                	push   $0x52
80106d91:	e9 11 f8 ff ff       	jmp    801065a7 <alltraps>

80106d96 <vector83>:
80106d96:	6a 00                	push   $0x0
80106d98:	6a 53                	push   $0x53
80106d9a:	e9 08 f8 ff ff       	jmp    801065a7 <alltraps>

80106d9f <vector84>:
80106d9f:	6a 00                	push   $0x0
80106da1:	6a 54                	push   $0x54
80106da3:	e9 ff f7 ff ff       	jmp    801065a7 <alltraps>

80106da8 <vector85>:
80106da8:	6a 00                	push   $0x0
80106daa:	6a 55                	push   $0x55
80106dac:	e9 f6 f7 ff ff       	jmp    801065a7 <alltraps>

80106db1 <vector86>:
80106db1:	6a 00                	push   $0x0
80106db3:	6a 56                	push   $0x56
80106db5:	e9 ed f7 ff ff       	jmp    801065a7 <alltraps>

80106dba <vector87>:
80106dba:	6a 00                	push   $0x0
80106dbc:	6a 57                	push   $0x57
80106dbe:	e9 e4 f7 ff ff       	jmp    801065a7 <alltraps>

80106dc3 <vector88>:
80106dc3:	6a 00                	push   $0x0
80106dc5:	6a 58                	push   $0x58
80106dc7:	e9 db f7 ff ff       	jmp    801065a7 <alltraps>

80106dcc <vector89>:
80106dcc:	6a 00                	push   $0x0
80106dce:	6a 59                	push   $0x59
80106dd0:	e9 d2 f7 ff ff       	jmp    801065a7 <alltraps>

80106dd5 <vector90>:
80106dd5:	6a 00                	push   $0x0
80106dd7:	6a 5a                	push   $0x5a
80106dd9:	e9 c9 f7 ff ff       	jmp    801065a7 <alltraps>

80106dde <vector91>:
80106dde:	6a 00                	push   $0x0
80106de0:	6a 5b                	push   $0x5b
80106de2:	e9 c0 f7 ff ff       	jmp    801065a7 <alltraps>

80106de7 <vector92>:
80106de7:	6a 00                	push   $0x0
80106de9:	6a 5c                	push   $0x5c
80106deb:	e9 b7 f7 ff ff       	jmp    801065a7 <alltraps>

80106df0 <vector93>:
80106df0:	6a 00                	push   $0x0
80106df2:	6a 5d                	push   $0x5d
80106df4:	e9 ae f7 ff ff       	jmp    801065a7 <alltraps>

80106df9 <vector94>:
80106df9:	6a 00                	push   $0x0
80106dfb:	6a 5e                	push   $0x5e
80106dfd:	e9 a5 f7 ff ff       	jmp    801065a7 <alltraps>

80106e02 <vector95>:
80106e02:	6a 00                	push   $0x0
80106e04:	6a 5f                	push   $0x5f
80106e06:	e9 9c f7 ff ff       	jmp    801065a7 <alltraps>

80106e0b <vector96>:
80106e0b:	6a 00                	push   $0x0
80106e0d:	6a 60                	push   $0x60
80106e0f:	e9 93 f7 ff ff       	jmp    801065a7 <alltraps>

80106e14 <vector97>:
80106e14:	6a 00                	push   $0x0
80106e16:	6a 61                	push   $0x61
80106e18:	e9 8a f7 ff ff       	jmp    801065a7 <alltraps>

80106e1d <vector98>:
80106e1d:	6a 00                	push   $0x0
80106e1f:	6a 62                	push   $0x62
80106e21:	e9 81 f7 ff ff       	jmp    801065a7 <alltraps>

80106e26 <vector99>:
80106e26:	6a 00                	push   $0x0
80106e28:	6a 63                	push   $0x63
80106e2a:	e9 78 f7 ff ff       	jmp    801065a7 <alltraps>

80106e2f <vector100>:
80106e2f:	6a 00                	push   $0x0
80106e31:	6a 64                	push   $0x64
80106e33:	e9 6f f7 ff ff       	jmp    801065a7 <alltraps>

80106e38 <vector101>:
80106e38:	6a 00                	push   $0x0
80106e3a:	6a 65                	push   $0x65
80106e3c:	e9 66 f7 ff ff       	jmp    801065a7 <alltraps>

80106e41 <vector102>:
80106e41:	6a 00                	push   $0x0
80106e43:	6a 66                	push   $0x66
80106e45:	e9 5d f7 ff ff       	jmp    801065a7 <alltraps>

80106e4a <vector103>:
80106e4a:	6a 00                	push   $0x0
80106e4c:	6a 67                	push   $0x67
80106e4e:	e9 54 f7 ff ff       	jmp    801065a7 <alltraps>

80106e53 <vector104>:
80106e53:	6a 00                	push   $0x0
80106e55:	6a 68                	push   $0x68
80106e57:	e9 4b f7 ff ff       	jmp    801065a7 <alltraps>

80106e5c <vector105>:
80106e5c:	6a 00                	push   $0x0
80106e5e:	6a 69                	push   $0x69
80106e60:	e9 42 f7 ff ff       	jmp    801065a7 <alltraps>

80106e65 <vector106>:
80106e65:	6a 00                	push   $0x0
80106e67:	6a 6a                	push   $0x6a
80106e69:	e9 39 f7 ff ff       	jmp    801065a7 <alltraps>

80106e6e <vector107>:
80106e6e:	6a 00                	push   $0x0
80106e70:	6a 6b                	push   $0x6b
80106e72:	e9 30 f7 ff ff       	jmp    801065a7 <alltraps>

80106e77 <vector108>:
80106e77:	6a 00                	push   $0x0
80106e79:	6a 6c                	push   $0x6c
80106e7b:	e9 27 f7 ff ff       	jmp    801065a7 <alltraps>

80106e80 <vector109>:
80106e80:	6a 00                	push   $0x0
80106e82:	6a 6d                	push   $0x6d
80106e84:	e9 1e f7 ff ff       	jmp    801065a7 <alltraps>

80106e89 <vector110>:
80106e89:	6a 00                	push   $0x0
80106e8b:	6a 6e                	push   $0x6e
80106e8d:	e9 15 f7 ff ff       	jmp    801065a7 <alltraps>

80106e92 <vector111>:
80106e92:	6a 00                	push   $0x0
80106e94:	6a 6f                	push   $0x6f
80106e96:	e9 0c f7 ff ff       	jmp    801065a7 <alltraps>

80106e9b <vector112>:
80106e9b:	6a 00                	push   $0x0
80106e9d:	6a 70                	push   $0x70
80106e9f:	e9 03 f7 ff ff       	jmp    801065a7 <alltraps>

80106ea4 <vector113>:
80106ea4:	6a 00                	push   $0x0
80106ea6:	6a 71                	push   $0x71
80106ea8:	e9 fa f6 ff ff       	jmp    801065a7 <alltraps>

80106ead <vector114>:
80106ead:	6a 00                	push   $0x0
80106eaf:	6a 72                	push   $0x72
80106eb1:	e9 f1 f6 ff ff       	jmp    801065a7 <alltraps>

80106eb6 <vector115>:
80106eb6:	6a 00                	push   $0x0
80106eb8:	6a 73                	push   $0x73
80106eba:	e9 e8 f6 ff ff       	jmp    801065a7 <alltraps>

80106ebf <vector116>:
80106ebf:	6a 00                	push   $0x0
80106ec1:	6a 74                	push   $0x74
80106ec3:	e9 df f6 ff ff       	jmp    801065a7 <alltraps>

80106ec8 <vector117>:
80106ec8:	6a 00                	push   $0x0
80106eca:	6a 75                	push   $0x75
80106ecc:	e9 d6 f6 ff ff       	jmp    801065a7 <alltraps>

80106ed1 <vector118>:
80106ed1:	6a 00                	push   $0x0
80106ed3:	6a 76                	push   $0x76
80106ed5:	e9 cd f6 ff ff       	jmp    801065a7 <alltraps>

80106eda <vector119>:
80106eda:	6a 00                	push   $0x0
80106edc:	6a 77                	push   $0x77
80106ede:	e9 c4 f6 ff ff       	jmp    801065a7 <alltraps>

80106ee3 <vector120>:
80106ee3:	6a 00                	push   $0x0
80106ee5:	6a 78                	push   $0x78
80106ee7:	e9 bb f6 ff ff       	jmp    801065a7 <alltraps>

80106eec <vector121>:
80106eec:	6a 00                	push   $0x0
80106eee:	6a 79                	push   $0x79
80106ef0:	e9 b2 f6 ff ff       	jmp    801065a7 <alltraps>

80106ef5 <vector122>:
80106ef5:	6a 00                	push   $0x0
80106ef7:	6a 7a                	push   $0x7a
80106ef9:	e9 a9 f6 ff ff       	jmp    801065a7 <alltraps>

80106efe <vector123>:
80106efe:	6a 00                	push   $0x0
80106f00:	6a 7b                	push   $0x7b
80106f02:	e9 a0 f6 ff ff       	jmp    801065a7 <alltraps>

80106f07 <vector124>:
80106f07:	6a 00                	push   $0x0
80106f09:	6a 7c                	push   $0x7c
80106f0b:	e9 97 f6 ff ff       	jmp    801065a7 <alltraps>

80106f10 <vector125>:
80106f10:	6a 00                	push   $0x0
80106f12:	6a 7d                	push   $0x7d
80106f14:	e9 8e f6 ff ff       	jmp    801065a7 <alltraps>

80106f19 <vector126>:
80106f19:	6a 00                	push   $0x0
80106f1b:	6a 7e                	push   $0x7e
80106f1d:	e9 85 f6 ff ff       	jmp    801065a7 <alltraps>

80106f22 <vector127>:
80106f22:	6a 00                	push   $0x0
80106f24:	6a 7f                	push   $0x7f
80106f26:	e9 7c f6 ff ff       	jmp    801065a7 <alltraps>

80106f2b <vector128>:
80106f2b:	6a 00                	push   $0x0
80106f2d:	68 80 00 00 00       	push   $0x80
80106f32:	e9 70 f6 ff ff       	jmp    801065a7 <alltraps>

80106f37 <vector129>:
80106f37:	6a 00                	push   $0x0
80106f39:	68 81 00 00 00       	push   $0x81
80106f3e:	e9 64 f6 ff ff       	jmp    801065a7 <alltraps>

80106f43 <vector130>:
80106f43:	6a 00                	push   $0x0
80106f45:	68 82 00 00 00       	push   $0x82
80106f4a:	e9 58 f6 ff ff       	jmp    801065a7 <alltraps>

80106f4f <vector131>:
80106f4f:	6a 00                	push   $0x0
80106f51:	68 83 00 00 00       	push   $0x83
80106f56:	e9 4c f6 ff ff       	jmp    801065a7 <alltraps>

80106f5b <vector132>:
80106f5b:	6a 00                	push   $0x0
80106f5d:	68 84 00 00 00       	push   $0x84
80106f62:	e9 40 f6 ff ff       	jmp    801065a7 <alltraps>

80106f67 <vector133>:
80106f67:	6a 00                	push   $0x0
80106f69:	68 85 00 00 00       	push   $0x85
80106f6e:	e9 34 f6 ff ff       	jmp    801065a7 <alltraps>

80106f73 <vector134>:
80106f73:	6a 00                	push   $0x0
80106f75:	68 86 00 00 00       	push   $0x86
80106f7a:	e9 28 f6 ff ff       	jmp    801065a7 <alltraps>

80106f7f <vector135>:
80106f7f:	6a 00                	push   $0x0
80106f81:	68 87 00 00 00       	push   $0x87
80106f86:	e9 1c f6 ff ff       	jmp    801065a7 <alltraps>

80106f8b <vector136>:
80106f8b:	6a 00                	push   $0x0
80106f8d:	68 88 00 00 00       	push   $0x88
80106f92:	e9 10 f6 ff ff       	jmp    801065a7 <alltraps>

80106f97 <vector137>:
80106f97:	6a 00                	push   $0x0
80106f99:	68 89 00 00 00       	push   $0x89
80106f9e:	e9 04 f6 ff ff       	jmp    801065a7 <alltraps>

80106fa3 <vector138>:
80106fa3:	6a 00                	push   $0x0
80106fa5:	68 8a 00 00 00       	push   $0x8a
80106faa:	e9 f8 f5 ff ff       	jmp    801065a7 <alltraps>

80106faf <vector139>:
80106faf:	6a 00                	push   $0x0
80106fb1:	68 8b 00 00 00       	push   $0x8b
80106fb6:	e9 ec f5 ff ff       	jmp    801065a7 <alltraps>

80106fbb <vector140>:
80106fbb:	6a 00                	push   $0x0
80106fbd:	68 8c 00 00 00       	push   $0x8c
80106fc2:	e9 e0 f5 ff ff       	jmp    801065a7 <alltraps>

80106fc7 <vector141>:
80106fc7:	6a 00                	push   $0x0
80106fc9:	68 8d 00 00 00       	push   $0x8d
80106fce:	e9 d4 f5 ff ff       	jmp    801065a7 <alltraps>

80106fd3 <vector142>:
80106fd3:	6a 00                	push   $0x0
80106fd5:	68 8e 00 00 00       	push   $0x8e
80106fda:	e9 c8 f5 ff ff       	jmp    801065a7 <alltraps>

80106fdf <vector143>:
80106fdf:	6a 00                	push   $0x0
80106fe1:	68 8f 00 00 00       	push   $0x8f
80106fe6:	e9 bc f5 ff ff       	jmp    801065a7 <alltraps>

80106feb <vector144>:
80106feb:	6a 00                	push   $0x0
80106fed:	68 90 00 00 00       	push   $0x90
80106ff2:	e9 b0 f5 ff ff       	jmp    801065a7 <alltraps>

80106ff7 <vector145>:
80106ff7:	6a 00                	push   $0x0
80106ff9:	68 91 00 00 00       	push   $0x91
80106ffe:	e9 a4 f5 ff ff       	jmp    801065a7 <alltraps>

80107003 <vector146>:
80107003:	6a 00                	push   $0x0
80107005:	68 92 00 00 00       	push   $0x92
8010700a:	e9 98 f5 ff ff       	jmp    801065a7 <alltraps>

8010700f <vector147>:
8010700f:	6a 00                	push   $0x0
80107011:	68 93 00 00 00       	push   $0x93
80107016:	e9 8c f5 ff ff       	jmp    801065a7 <alltraps>

8010701b <vector148>:
8010701b:	6a 00                	push   $0x0
8010701d:	68 94 00 00 00       	push   $0x94
80107022:	e9 80 f5 ff ff       	jmp    801065a7 <alltraps>

80107027 <vector149>:
80107027:	6a 00                	push   $0x0
80107029:	68 95 00 00 00       	push   $0x95
8010702e:	e9 74 f5 ff ff       	jmp    801065a7 <alltraps>

80107033 <vector150>:
80107033:	6a 00                	push   $0x0
80107035:	68 96 00 00 00       	push   $0x96
8010703a:	e9 68 f5 ff ff       	jmp    801065a7 <alltraps>

8010703f <vector151>:
8010703f:	6a 00                	push   $0x0
80107041:	68 97 00 00 00       	push   $0x97
80107046:	e9 5c f5 ff ff       	jmp    801065a7 <alltraps>

8010704b <vector152>:
8010704b:	6a 00                	push   $0x0
8010704d:	68 98 00 00 00       	push   $0x98
80107052:	e9 50 f5 ff ff       	jmp    801065a7 <alltraps>

80107057 <vector153>:
80107057:	6a 00                	push   $0x0
80107059:	68 99 00 00 00       	push   $0x99
8010705e:	e9 44 f5 ff ff       	jmp    801065a7 <alltraps>

80107063 <vector154>:
80107063:	6a 00                	push   $0x0
80107065:	68 9a 00 00 00       	push   $0x9a
8010706a:	e9 38 f5 ff ff       	jmp    801065a7 <alltraps>

8010706f <vector155>:
8010706f:	6a 00                	push   $0x0
80107071:	68 9b 00 00 00       	push   $0x9b
80107076:	e9 2c f5 ff ff       	jmp    801065a7 <alltraps>

8010707b <vector156>:
8010707b:	6a 00                	push   $0x0
8010707d:	68 9c 00 00 00       	push   $0x9c
80107082:	e9 20 f5 ff ff       	jmp    801065a7 <alltraps>

80107087 <vector157>:
80107087:	6a 00                	push   $0x0
80107089:	68 9d 00 00 00       	push   $0x9d
8010708e:	e9 14 f5 ff ff       	jmp    801065a7 <alltraps>

80107093 <vector158>:
80107093:	6a 00                	push   $0x0
80107095:	68 9e 00 00 00       	push   $0x9e
8010709a:	e9 08 f5 ff ff       	jmp    801065a7 <alltraps>

8010709f <vector159>:
8010709f:	6a 00                	push   $0x0
801070a1:	68 9f 00 00 00       	push   $0x9f
801070a6:	e9 fc f4 ff ff       	jmp    801065a7 <alltraps>

801070ab <vector160>:
801070ab:	6a 00                	push   $0x0
801070ad:	68 a0 00 00 00       	push   $0xa0
801070b2:	e9 f0 f4 ff ff       	jmp    801065a7 <alltraps>

801070b7 <vector161>:
801070b7:	6a 00                	push   $0x0
801070b9:	68 a1 00 00 00       	push   $0xa1
801070be:	e9 e4 f4 ff ff       	jmp    801065a7 <alltraps>

801070c3 <vector162>:
801070c3:	6a 00                	push   $0x0
801070c5:	68 a2 00 00 00       	push   $0xa2
801070ca:	e9 d8 f4 ff ff       	jmp    801065a7 <alltraps>

801070cf <vector163>:
801070cf:	6a 00                	push   $0x0
801070d1:	68 a3 00 00 00       	push   $0xa3
801070d6:	e9 cc f4 ff ff       	jmp    801065a7 <alltraps>

801070db <vector164>:
801070db:	6a 00                	push   $0x0
801070dd:	68 a4 00 00 00       	push   $0xa4
801070e2:	e9 c0 f4 ff ff       	jmp    801065a7 <alltraps>

801070e7 <vector165>:
801070e7:	6a 00                	push   $0x0
801070e9:	68 a5 00 00 00       	push   $0xa5
801070ee:	e9 b4 f4 ff ff       	jmp    801065a7 <alltraps>

801070f3 <vector166>:
801070f3:	6a 00                	push   $0x0
801070f5:	68 a6 00 00 00       	push   $0xa6
801070fa:	e9 a8 f4 ff ff       	jmp    801065a7 <alltraps>

801070ff <vector167>:
801070ff:	6a 00                	push   $0x0
80107101:	68 a7 00 00 00       	push   $0xa7
80107106:	e9 9c f4 ff ff       	jmp    801065a7 <alltraps>

8010710b <vector168>:
8010710b:	6a 00                	push   $0x0
8010710d:	68 a8 00 00 00       	push   $0xa8
80107112:	e9 90 f4 ff ff       	jmp    801065a7 <alltraps>

80107117 <vector169>:
80107117:	6a 00                	push   $0x0
80107119:	68 a9 00 00 00       	push   $0xa9
8010711e:	e9 84 f4 ff ff       	jmp    801065a7 <alltraps>

80107123 <vector170>:
80107123:	6a 00                	push   $0x0
80107125:	68 aa 00 00 00       	push   $0xaa
8010712a:	e9 78 f4 ff ff       	jmp    801065a7 <alltraps>

8010712f <vector171>:
8010712f:	6a 00                	push   $0x0
80107131:	68 ab 00 00 00       	push   $0xab
80107136:	e9 6c f4 ff ff       	jmp    801065a7 <alltraps>

8010713b <vector172>:
8010713b:	6a 00                	push   $0x0
8010713d:	68 ac 00 00 00       	push   $0xac
80107142:	e9 60 f4 ff ff       	jmp    801065a7 <alltraps>

80107147 <vector173>:
80107147:	6a 00                	push   $0x0
80107149:	68 ad 00 00 00       	push   $0xad
8010714e:	e9 54 f4 ff ff       	jmp    801065a7 <alltraps>

80107153 <vector174>:
80107153:	6a 00                	push   $0x0
80107155:	68 ae 00 00 00       	push   $0xae
8010715a:	e9 48 f4 ff ff       	jmp    801065a7 <alltraps>

8010715f <vector175>:
8010715f:	6a 00                	push   $0x0
80107161:	68 af 00 00 00       	push   $0xaf
80107166:	e9 3c f4 ff ff       	jmp    801065a7 <alltraps>

8010716b <vector176>:
8010716b:	6a 00                	push   $0x0
8010716d:	68 b0 00 00 00       	push   $0xb0
80107172:	e9 30 f4 ff ff       	jmp    801065a7 <alltraps>

80107177 <vector177>:
80107177:	6a 00                	push   $0x0
80107179:	68 b1 00 00 00       	push   $0xb1
8010717e:	e9 24 f4 ff ff       	jmp    801065a7 <alltraps>

80107183 <vector178>:
80107183:	6a 00                	push   $0x0
80107185:	68 b2 00 00 00       	push   $0xb2
8010718a:	e9 18 f4 ff ff       	jmp    801065a7 <alltraps>

8010718f <vector179>:
8010718f:	6a 00                	push   $0x0
80107191:	68 b3 00 00 00       	push   $0xb3
80107196:	e9 0c f4 ff ff       	jmp    801065a7 <alltraps>

8010719b <vector180>:
8010719b:	6a 00                	push   $0x0
8010719d:	68 b4 00 00 00       	push   $0xb4
801071a2:	e9 00 f4 ff ff       	jmp    801065a7 <alltraps>

801071a7 <vector181>:
801071a7:	6a 00                	push   $0x0
801071a9:	68 b5 00 00 00       	push   $0xb5
801071ae:	e9 f4 f3 ff ff       	jmp    801065a7 <alltraps>

801071b3 <vector182>:
801071b3:	6a 00                	push   $0x0
801071b5:	68 b6 00 00 00       	push   $0xb6
801071ba:	e9 e8 f3 ff ff       	jmp    801065a7 <alltraps>

801071bf <vector183>:
801071bf:	6a 00                	push   $0x0
801071c1:	68 b7 00 00 00       	push   $0xb7
801071c6:	e9 dc f3 ff ff       	jmp    801065a7 <alltraps>

801071cb <vector184>:
801071cb:	6a 00                	push   $0x0
801071cd:	68 b8 00 00 00       	push   $0xb8
801071d2:	e9 d0 f3 ff ff       	jmp    801065a7 <alltraps>

801071d7 <vector185>:
801071d7:	6a 00                	push   $0x0
801071d9:	68 b9 00 00 00       	push   $0xb9
801071de:	e9 c4 f3 ff ff       	jmp    801065a7 <alltraps>

801071e3 <vector186>:
801071e3:	6a 00                	push   $0x0
801071e5:	68 ba 00 00 00       	push   $0xba
801071ea:	e9 b8 f3 ff ff       	jmp    801065a7 <alltraps>

801071ef <vector187>:
801071ef:	6a 00                	push   $0x0
801071f1:	68 bb 00 00 00       	push   $0xbb
801071f6:	e9 ac f3 ff ff       	jmp    801065a7 <alltraps>

801071fb <vector188>:
801071fb:	6a 00                	push   $0x0
801071fd:	68 bc 00 00 00       	push   $0xbc
80107202:	e9 a0 f3 ff ff       	jmp    801065a7 <alltraps>

80107207 <vector189>:
80107207:	6a 00                	push   $0x0
80107209:	68 bd 00 00 00       	push   $0xbd
8010720e:	e9 94 f3 ff ff       	jmp    801065a7 <alltraps>

80107213 <vector190>:
80107213:	6a 00                	push   $0x0
80107215:	68 be 00 00 00       	push   $0xbe
8010721a:	e9 88 f3 ff ff       	jmp    801065a7 <alltraps>

8010721f <vector191>:
8010721f:	6a 00                	push   $0x0
80107221:	68 bf 00 00 00       	push   $0xbf
80107226:	e9 7c f3 ff ff       	jmp    801065a7 <alltraps>

8010722b <vector192>:
8010722b:	6a 00                	push   $0x0
8010722d:	68 c0 00 00 00       	push   $0xc0
80107232:	e9 70 f3 ff ff       	jmp    801065a7 <alltraps>

80107237 <vector193>:
80107237:	6a 00                	push   $0x0
80107239:	68 c1 00 00 00       	push   $0xc1
8010723e:	e9 64 f3 ff ff       	jmp    801065a7 <alltraps>

80107243 <vector194>:
80107243:	6a 00                	push   $0x0
80107245:	68 c2 00 00 00       	push   $0xc2
8010724a:	e9 58 f3 ff ff       	jmp    801065a7 <alltraps>

8010724f <vector195>:
8010724f:	6a 00                	push   $0x0
80107251:	68 c3 00 00 00       	push   $0xc3
80107256:	e9 4c f3 ff ff       	jmp    801065a7 <alltraps>

8010725b <vector196>:
8010725b:	6a 00                	push   $0x0
8010725d:	68 c4 00 00 00       	push   $0xc4
80107262:	e9 40 f3 ff ff       	jmp    801065a7 <alltraps>

80107267 <vector197>:
80107267:	6a 00                	push   $0x0
80107269:	68 c5 00 00 00       	push   $0xc5
8010726e:	e9 34 f3 ff ff       	jmp    801065a7 <alltraps>

80107273 <vector198>:
80107273:	6a 00                	push   $0x0
80107275:	68 c6 00 00 00       	push   $0xc6
8010727a:	e9 28 f3 ff ff       	jmp    801065a7 <alltraps>

8010727f <vector199>:
8010727f:	6a 00                	push   $0x0
80107281:	68 c7 00 00 00       	push   $0xc7
80107286:	e9 1c f3 ff ff       	jmp    801065a7 <alltraps>

8010728b <vector200>:
8010728b:	6a 00                	push   $0x0
8010728d:	68 c8 00 00 00       	push   $0xc8
80107292:	e9 10 f3 ff ff       	jmp    801065a7 <alltraps>

80107297 <vector201>:
80107297:	6a 00                	push   $0x0
80107299:	68 c9 00 00 00       	push   $0xc9
8010729e:	e9 04 f3 ff ff       	jmp    801065a7 <alltraps>

801072a3 <vector202>:
801072a3:	6a 00                	push   $0x0
801072a5:	68 ca 00 00 00       	push   $0xca
801072aa:	e9 f8 f2 ff ff       	jmp    801065a7 <alltraps>

801072af <vector203>:
801072af:	6a 00                	push   $0x0
801072b1:	68 cb 00 00 00       	push   $0xcb
801072b6:	e9 ec f2 ff ff       	jmp    801065a7 <alltraps>

801072bb <vector204>:
801072bb:	6a 00                	push   $0x0
801072bd:	68 cc 00 00 00       	push   $0xcc
801072c2:	e9 e0 f2 ff ff       	jmp    801065a7 <alltraps>

801072c7 <vector205>:
801072c7:	6a 00                	push   $0x0
801072c9:	68 cd 00 00 00       	push   $0xcd
801072ce:	e9 d4 f2 ff ff       	jmp    801065a7 <alltraps>

801072d3 <vector206>:
801072d3:	6a 00                	push   $0x0
801072d5:	68 ce 00 00 00       	push   $0xce
801072da:	e9 c8 f2 ff ff       	jmp    801065a7 <alltraps>

801072df <vector207>:
801072df:	6a 00                	push   $0x0
801072e1:	68 cf 00 00 00       	push   $0xcf
801072e6:	e9 bc f2 ff ff       	jmp    801065a7 <alltraps>

801072eb <vector208>:
801072eb:	6a 00                	push   $0x0
801072ed:	68 d0 00 00 00       	push   $0xd0
801072f2:	e9 b0 f2 ff ff       	jmp    801065a7 <alltraps>

801072f7 <vector209>:
801072f7:	6a 00                	push   $0x0
801072f9:	68 d1 00 00 00       	push   $0xd1
801072fe:	e9 a4 f2 ff ff       	jmp    801065a7 <alltraps>

80107303 <vector210>:
80107303:	6a 00                	push   $0x0
80107305:	68 d2 00 00 00       	push   $0xd2
8010730a:	e9 98 f2 ff ff       	jmp    801065a7 <alltraps>

8010730f <vector211>:
8010730f:	6a 00                	push   $0x0
80107311:	68 d3 00 00 00       	push   $0xd3
80107316:	e9 8c f2 ff ff       	jmp    801065a7 <alltraps>

8010731b <vector212>:
8010731b:	6a 00                	push   $0x0
8010731d:	68 d4 00 00 00       	push   $0xd4
80107322:	e9 80 f2 ff ff       	jmp    801065a7 <alltraps>

80107327 <vector213>:
80107327:	6a 00                	push   $0x0
80107329:	68 d5 00 00 00       	push   $0xd5
8010732e:	e9 74 f2 ff ff       	jmp    801065a7 <alltraps>

80107333 <vector214>:
80107333:	6a 00                	push   $0x0
80107335:	68 d6 00 00 00       	push   $0xd6
8010733a:	e9 68 f2 ff ff       	jmp    801065a7 <alltraps>

8010733f <vector215>:
8010733f:	6a 00                	push   $0x0
80107341:	68 d7 00 00 00       	push   $0xd7
80107346:	e9 5c f2 ff ff       	jmp    801065a7 <alltraps>

8010734b <vector216>:
8010734b:	6a 00                	push   $0x0
8010734d:	68 d8 00 00 00       	push   $0xd8
80107352:	e9 50 f2 ff ff       	jmp    801065a7 <alltraps>

80107357 <vector217>:
80107357:	6a 00                	push   $0x0
80107359:	68 d9 00 00 00       	push   $0xd9
8010735e:	e9 44 f2 ff ff       	jmp    801065a7 <alltraps>

80107363 <vector218>:
80107363:	6a 00                	push   $0x0
80107365:	68 da 00 00 00       	push   $0xda
8010736a:	e9 38 f2 ff ff       	jmp    801065a7 <alltraps>

8010736f <vector219>:
8010736f:	6a 00                	push   $0x0
80107371:	68 db 00 00 00       	push   $0xdb
80107376:	e9 2c f2 ff ff       	jmp    801065a7 <alltraps>

8010737b <vector220>:
8010737b:	6a 00                	push   $0x0
8010737d:	68 dc 00 00 00       	push   $0xdc
80107382:	e9 20 f2 ff ff       	jmp    801065a7 <alltraps>

80107387 <vector221>:
80107387:	6a 00                	push   $0x0
80107389:	68 dd 00 00 00       	push   $0xdd
8010738e:	e9 14 f2 ff ff       	jmp    801065a7 <alltraps>

80107393 <vector222>:
80107393:	6a 00                	push   $0x0
80107395:	68 de 00 00 00       	push   $0xde
8010739a:	e9 08 f2 ff ff       	jmp    801065a7 <alltraps>

8010739f <vector223>:
8010739f:	6a 00                	push   $0x0
801073a1:	68 df 00 00 00       	push   $0xdf
801073a6:	e9 fc f1 ff ff       	jmp    801065a7 <alltraps>

801073ab <vector224>:
801073ab:	6a 00                	push   $0x0
801073ad:	68 e0 00 00 00       	push   $0xe0
801073b2:	e9 f0 f1 ff ff       	jmp    801065a7 <alltraps>

801073b7 <vector225>:
801073b7:	6a 00                	push   $0x0
801073b9:	68 e1 00 00 00       	push   $0xe1
801073be:	e9 e4 f1 ff ff       	jmp    801065a7 <alltraps>

801073c3 <vector226>:
801073c3:	6a 00                	push   $0x0
801073c5:	68 e2 00 00 00       	push   $0xe2
801073ca:	e9 d8 f1 ff ff       	jmp    801065a7 <alltraps>

801073cf <vector227>:
801073cf:	6a 00                	push   $0x0
801073d1:	68 e3 00 00 00       	push   $0xe3
801073d6:	e9 cc f1 ff ff       	jmp    801065a7 <alltraps>

801073db <vector228>:
801073db:	6a 00                	push   $0x0
801073dd:	68 e4 00 00 00       	push   $0xe4
801073e2:	e9 c0 f1 ff ff       	jmp    801065a7 <alltraps>

801073e7 <vector229>:
801073e7:	6a 00                	push   $0x0
801073e9:	68 e5 00 00 00       	push   $0xe5
801073ee:	e9 b4 f1 ff ff       	jmp    801065a7 <alltraps>

801073f3 <vector230>:
801073f3:	6a 00                	push   $0x0
801073f5:	68 e6 00 00 00       	push   $0xe6
801073fa:	e9 a8 f1 ff ff       	jmp    801065a7 <alltraps>

801073ff <vector231>:
801073ff:	6a 00                	push   $0x0
80107401:	68 e7 00 00 00       	push   $0xe7
80107406:	e9 9c f1 ff ff       	jmp    801065a7 <alltraps>

8010740b <vector232>:
8010740b:	6a 00                	push   $0x0
8010740d:	68 e8 00 00 00       	push   $0xe8
80107412:	e9 90 f1 ff ff       	jmp    801065a7 <alltraps>

80107417 <vector233>:
80107417:	6a 00                	push   $0x0
80107419:	68 e9 00 00 00       	push   $0xe9
8010741e:	e9 84 f1 ff ff       	jmp    801065a7 <alltraps>

80107423 <vector234>:
80107423:	6a 00                	push   $0x0
80107425:	68 ea 00 00 00       	push   $0xea
8010742a:	e9 78 f1 ff ff       	jmp    801065a7 <alltraps>

8010742f <vector235>:
8010742f:	6a 00                	push   $0x0
80107431:	68 eb 00 00 00       	push   $0xeb
80107436:	e9 6c f1 ff ff       	jmp    801065a7 <alltraps>

8010743b <vector236>:
8010743b:	6a 00                	push   $0x0
8010743d:	68 ec 00 00 00       	push   $0xec
80107442:	e9 60 f1 ff ff       	jmp    801065a7 <alltraps>

80107447 <vector237>:
80107447:	6a 00                	push   $0x0
80107449:	68 ed 00 00 00       	push   $0xed
8010744e:	e9 54 f1 ff ff       	jmp    801065a7 <alltraps>

80107453 <vector238>:
80107453:	6a 00                	push   $0x0
80107455:	68 ee 00 00 00       	push   $0xee
8010745a:	e9 48 f1 ff ff       	jmp    801065a7 <alltraps>

8010745f <vector239>:
8010745f:	6a 00                	push   $0x0
80107461:	68 ef 00 00 00       	push   $0xef
80107466:	e9 3c f1 ff ff       	jmp    801065a7 <alltraps>

8010746b <vector240>:
8010746b:	6a 00                	push   $0x0
8010746d:	68 f0 00 00 00       	push   $0xf0
80107472:	e9 30 f1 ff ff       	jmp    801065a7 <alltraps>

80107477 <vector241>:
80107477:	6a 00                	push   $0x0
80107479:	68 f1 00 00 00       	push   $0xf1
8010747e:	e9 24 f1 ff ff       	jmp    801065a7 <alltraps>

80107483 <vector242>:
80107483:	6a 00                	push   $0x0
80107485:	68 f2 00 00 00       	push   $0xf2
8010748a:	e9 18 f1 ff ff       	jmp    801065a7 <alltraps>

8010748f <vector243>:
8010748f:	6a 00                	push   $0x0
80107491:	68 f3 00 00 00       	push   $0xf3
80107496:	e9 0c f1 ff ff       	jmp    801065a7 <alltraps>

8010749b <vector244>:
8010749b:	6a 00                	push   $0x0
8010749d:	68 f4 00 00 00       	push   $0xf4
801074a2:	e9 00 f1 ff ff       	jmp    801065a7 <alltraps>

801074a7 <vector245>:
801074a7:	6a 00                	push   $0x0
801074a9:	68 f5 00 00 00       	push   $0xf5
801074ae:	e9 f4 f0 ff ff       	jmp    801065a7 <alltraps>

801074b3 <vector246>:
801074b3:	6a 00                	push   $0x0
801074b5:	68 f6 00 00 00       	push   $0xf6
801074ba:	e9 e8 f0 ff ff       	jmp    801065a7 <alltraps>

801074bf <vector247>:
801074bf:	6a 00                	push   $0x0
801074c1:	68 f7 00 00 00       	push   $0xf7
801074c6:	e9 dc f0 ff ff       	jmp    801065a7 <alltraps>

801074cb <vector248>:
801074cb:	6a 00                	push   $0x0
801074cd:	68 f8 00 00 00       	push   $0xf8
801074d2:	e9 d0 f0 ff ff       	jmp    801065a7 <alltraps>

801074d7 <vector249>:
801074d7:	6a 00                	push   $0x0
801074d9:	68 f9 00 00 00       	push   $0xf9
801074de:	e9 c4 f0 ff ff       	jmp    801065a7 <alltraps>

801074e3 <vector250>:
801074e3:	6a 00                	push   $0x0
801074e5:	68 fa 00 00 00       	push   $0xfa
801074ea:	e9 b8 f0 ff ff       	jmp    801065a7 <alltraps>

801074ef <vector251>:
801074ef:	6a 00                	push   $0x0
801074f1:	68 fb 00 00 00       	push   $0xfb
801074f6:	e9 ac f0 ff ff       	jmp    801065a7 <alltraps>

801074fb <vector252>:
801074fb:	6a 00                	push   $0x0
801074fd:	68 fc 00 00 00       	push   $0xfc
80107502:	e9 a0 f0 ff ff       	jmp    801065a7 <alltraps>

80107507 <vector253>:
80107507:	6a 00                	push   $0x0
80107509:	68 fd 00 00 00       	push   $0xfd
8010750e:	e9 94 f0 ff ff       	jmp    801065a7 <alltraps>

80107513 <vector254>:
80107513:	6a 00                	push   $0x0
80107515:	68 fe 00 00 00       	push   $0xfe
8010751a:	e9 88 f0 ff ff       	jmp    801065a7 <alltraps>

8010751f <vector255>:
8010751f:	6a 00                	push   $0x0
80107521:	68 ff 00 00 00       	push   $0xff
80107526:	e9 7c f0 ff ff       	jmp    801065a7 <alltraps>
8010752b:	66 90                	xchg   %ax,%ax
8010752d:	66 90                	xchg   %ax,%ax
8010752f:	90                   	nop

80107530 <deallocuvm.part.0>:
// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
80107530:	55                   	push   %ebp
80107531:	89 e5                	mov    %esp,%ebp
80107533:	57                   	push   %edi
80107534:	56                   	push   %esi
80107535:	53                   	push   %ebx
  uint a, pa;

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
80107536:	8d 99 ff 0f 00 00    	lea    0xfff(%ecx),%ebx
8010753c:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
80107542:	83 ec 1c             	sub    $0x1c,%esp
80107545:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  for(; a  < oldsz; a += PGSIZE){
80107548:	39 d3                	cmp    %edx,%ebx
8010754a:	73 45                	jae    80107591 <deallocuvm.part.0+0x61>
8010754c:	89 c7                	mov    %eax,%edi
8010754e:	eb 0a                	jmp    8010755a <deallocuvm.part.0+0x2a>
    pte = walkpgdir(pgdir, (char*)a, 0);
    if(!pte)
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
80107550:	8d 59 01             	lea    0x1(%ecx),%ebx
80107553:	c1 e3 16             	shl    $0x16,%ebx
  for(; a  < oldsz; a += PGSIZE){
80107556:	39 da                	cmp    %ebx,%edx
80107558:	76 37                	jbe    80107591 <deallocuvm.part.0+0x61>
  pde = &pgdir[PDX(va)];
8010755a:	89 d9                	mov    %ebx,%ecx
8010755c:	c1 e9 16             	shr    $0x16,%ecx
  if(*pde & PTE_P){
8010755f:	8b 04 8f             	mov    (%edi,%ecx,4),%eax
80107562:	a8 01                	test   $0x1,%al
80107564:	74 ea                	je     80107550 <deallocuvm.part.0+0x20>
  return &pgtab[PTX(va)];
80107566:	89 de                	mov    %ebx,%esi
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80107568:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  return &pgtab[PTX(va)];
8010756d:	c1 ee 0a             	shr    $0xa,%esi
80107570:	81 e6 fc 0f 00 00    	and    $0xffc,%esi
80107576:	8d b4 30 00 00 00 80 	lea    -0x80000000(%eax,%esi,1),%esi
    if(!pte)
8010757d:	85 f6                	test   %esi,%esi
8010757f:	74 cf                	je     80107550 <deallocuvm.part.0+0x20>
    else if((*pte & PTE_P) != 0){
80107581:	8b 06                	mov    (%esi),%eax
80107583:	a8 01                	test   $0x1,%al
80107585:	75 19                	jne    801075a0 <deallocuvm.part.0+0x70>
  for(; a  < oldsz; a += PGSIZE){
80107587:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010758d:	39 da                	cmp    %ebx,%edx
8010758f:	77 c9                	ja     8010755a <deallocuvm.part.0+0x2a>
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
}
80107591:	8b 45 e0             	mov    -0x20(%ebp),%eax
80107594:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107597:	5b                   	pop    %ebx
80107598:	5e                   	pop    %esi
80107599:	5f                   	pop    %edi
8010759a:	5d                   	pop    %ebp
8010759b:	c3                   	ret    
8010759c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if(pa == 0)
801075a0:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801075a5:	74 25                	je     801075cc <deallocuvm.part.0+0x9c>
      kfree(v);
801075a7:	83 ec 0c             	sub    $0xc,%esp
      char *v = P2V(pa);
801075aa:	05 00 00 00 80       	add    $0x80000000,%eax
801075af:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  for(; a  < oldsz; a += PGSIZE){
801075b2:	81 c3 00 10 00 00    	add    $0x1000,%ebx
      kfree(v);
801075b8:	50                   	push   %eax
801075b9:	e8 22 b6 ff ff       	call   80102be0 <kfree>
      *pte = 0;
801075be:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
  for(; a  < oldsz; a += PGSIZE){
801075c4:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801075c7:	83 c4 10             	add    $0x10,%esp
801075ca:	eb 8a                	jmp    80107556 <deallocuvm.part.0+0x26>
        panic("kfree");
801075cc:	83 ec 0c             	sub    $0xc,%esp
801075cf:	68 fe 81 10 80       	push   $0x801081fe
801075d4:	e8 b7 8d ff ff       	call   80100390 <panic>
801075d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801075e0 <mappages>:
{
801075e0:	55                   	push   %ebp
801075e1:	89 e5                	mov    %esp,%ebp
801075e3:	57                   	push   %edi
801075e4:	56                   	push   %esi
801075e5:	53                   	push   %ebx
  a = (char*)PGROUNDDOWN((uint)va);
801075e6:	89 d3                	mov    %edx,%ebx
801075e8:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
{
801075ee:	83 ec 1c             	sub    $0x1c,%esp
801075f1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
801075f4:	8d 44 0a ff          	lea    -0x1(%edx,%ecx,1),%eax
801075f8:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801075fd:	89 45 dc             	mov    %eax,-0x24(%ebp)
80107600:	8b 45 08             	mov    0x8(%ebp),%eax
80107603:	29 d8                	sub    %ebx,%eax
80107605:	89 45 e0             	mov    %eax,-0x20(%ebp)
80107608:	eb 3d                	jmp    80107647 <mappages+0x67>
8010760a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return &pgtab[PTX(va)];
80107610:	89 da                	mov    %ebx,%edx
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80107612:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  return &pgtab[PTX(va)];
80107617:	c1 ea 0a             	shr    $0xa,%edx
8010761a:	81 e2 fc 0f 00 00    	and    $0xffc,%edx
80107620:	8d 94 10 00 00 00 80 	lea    -0x80000000(%eax,%edx,1),%edx
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
80107627:	85 d2                	test   %edx,%edx
80107629:	74 75                	je     801076a0 <mappages+0xc0>
    if(*pte & PTE_P)
8010762b:	f6 02 01             	testb  $0x1,(%edx)
8010762e:	0f 85 86 00 00 00    	jne    801076ba <mappages+0xda>
    *pte = pa | perm | PTE_P;
80107634:	0b 75 0c             	or     0xc(%ebp),%esi
80107637:	83 ce 01             	or     $0x1,%esi
8010763a:	89 32                	mov    %esi,(%edx)
    if(a == last)
8010763c:	3b 5d dc             	cmp    -0x24(%ebp),%ebx
8010763f:	74 6f                	je     801076b0 <mappages+0xd0>
    a += PGSIZE;
80107641:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  for(;;){
80107647:	8b 45 e0             	mov    -0x20(%ebp),%eax
  pde = &pgdir[PDX(va)];
8010764a:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
8010764d:	8d 34 18             	lea    (%eax,%ebx,1),%esi
80107650:	89 d8                	mov    %ebx,%eax
80107652:	c1 e8 16             	shr    $0x16,%eax
80107655:	8d 3c 81             	lea    (%ecx,%eax,4),%edi
  if(*pde & PTE_P){
80107658:	8b 07                	mov    (%edi),%eax
8010765a:	a8 01                	test   $0x1,%al
8010765c:	75 b2                	jne    80107610 <mappages+0x30>
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
8010765e:	e8 3d b7 ff ff       	call   80102da0 <kalloc>
80107663:	85 c0                	test   %eax,%eax
80107665:	74 39                	je     801076a0 <mappages+0xc0>
    memset(pgtab, 0, PGSIZE);
80107667:	83 ec 04             	sub    $0x4,%esp
8010766a:	89 45 d8             	mov    %eax,-0x28(%ebp)
8010766d:	68 00 10 00 00       	push   $0x1000
80107672:	6a 00                	push   $0x0
80107674:	50                   	push   %eax
80107675:	e8 b6 db ff ff       	call   80105230 <memset>
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
8010767a:	8b 55 d8             	mov    -0x28(%ebp),%edx
  return &pgtab[PTX(va)];
8010767d:	83 c4 10             	add    $0x10,%esp
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
80107680:	8d 82 00 00 00 80    	lea    -0x80000000(%edx),%eax
80107686:	83 c8 07             	or     $0x7,%eax
80107689:	89 07                	mov    %eax,(%edi)
  return &pgtab[PTX(va)];
8010768b:	89 d8                	mov    %ebx,%eax
8010768d:	c1 e8 0a             	shr    $0xa,%eax
80107690:	25 fc 0f 00 00       	and    $0xffc,%eax
80107695:	01 c2                	add    %eax,%edx
80107697:	eb 92                	jmp    8010762b <mappages+0x4b>
80107699:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
}
801076a0:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
801076a3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801076a8:	5b                   	pop    %ebx
801076a9:	5e                   	pop    %esi
801076aa:	5f                   	pop    %edi
801076ab:	5d                   	pop    %ebp
801076ac:	c3                   	ret    
801076ad:	8d 76 00             	lea    0x0(%esi),%esi
801076b0:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
801076b3:	31 c0                	xor    %eax,%eax
}
801076b5:	5b                   	pop    %ebx
801076b6:	5e                   	pop    %esi
801076b7:	5f                   	pop    %edi
801076b8:	5d                   	pop    %ebp
801076b9:	c3                   	ret    
      panic("remap");
801076ba:	83 ec 0c             	sub    $0xc,%esp
801076bd:	68 50 89 10 80       	push   $0x80108950
801076c2:	e8 c9 8c ff ff       	call   80100390 <panic>
801076c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801076ce:	66 90                	xchg   %ax,%ax

801076d0 <seginit>:
{
801076d0:	f3 0f 1e fb          	endbr32 
801076d4:	55                   	push   %ebp
801076d5:	89 e5                	mov    %esp,%ebp
801076d7:	83 ec 18             	sub    $0x18,%esp
  c = &cpus[cpuid()];
801076da:	e8 11 ca ff ff       	call   801040f0 <cpuid>
  pd[0] = size-1;
801076df:	ba 2f 00 00 00       	mov    $0x2f,%edx
801076e4:	69 c0 b0 00 00 00    	imul   $0xb0,%eax,%eax
801076ea:	66 89 55 f2          	mov    %dx,-0xe(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
801076ee:	c7 80 18 28 11 80 ff 	movl   $0xffff,-0x7feed7e8(%eax)
801076f5:	ff 00 00 
801076f8:	c7 80 1c 28 11 80 00 	movl   $0xcf9a00,-0x7feed7e4(%eax)
801076ff:	9a cf 00 
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
80107702:	c7 80 20 28 11 80 ff 	movl   $0xffff,-0x7feed7e0(%eax)
80107709:	ff 00 00 
8010770c:	c7 80 24 28 11 80 00 	movl   $0xcf9200,-0x7feed7dc(%eax)
80107713:	92 cf 00 
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
80107716:	c7 80 28 28 11 80 ff 	movl   $0xffff,-0x7feed7d8(%eax)
8010771d:	ff 00 00 
80107720:	c7 80 2c 28 11 80 00 	movl   $0xcffa00,-0x7feed7d4(%eax)
80107727:	fa cf 00 
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
8010772a:	c7 80 30 28 11 80 ff 	movl   $0xffff,-0x7feed7d0(%eax)
80107731:	ff 00 00 
80107734:	c7 80 34 28 11 80 00 	movl   $0xcff200,-0x7feed7cc(%eax)
8010773b:	f2 cf 00 
  lgdt(c->gdt, sizeof(c->gdt));
8010773e:	05 10 28 11 80       	add    $0x80112810,%eax
  pd[1] = (uint)p;
80107743:	66 89 45 f4          	mov    %ax,-0xc(%ebp)
  pd[2] = (uint)p >> 16;
80107747:	c1 e8 10             	shr    $0x10,%eax
8010774a:	66 89 45 f6          	mov    %ax,-0xa(%ebp)
  asm volatile("lgdt (%0)" : : "r" (pd));
8010774e:	8d 45 f2             	lea    -0xe(%ebp),%eax
80107751:	0f 01 10             	lgdtl  (%eax)
}
80107754:	c9                   	leave  
80107755:	c3                   	ret    
80107756:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010775d:	8d 76 00             	lea    0x0(%esi),%esi

80107760 <switchkvm>:
{
80107760:	f3 0f 1e fb          	endbr32 
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80107764:	a1 c4 5a 11 80       	mov    0x80115ac4,%eax
80107769:	05 00 00 00 80       	add    $0x80000000,%eax
}

static inline void
lcr3(uint val)
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
8010776e:	0f 22 d8             	mov    %eax,%cr3
}
80107771:	c3                   	ret    
80107772:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107779:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80107780 <switchuvm>:
{
80107780:	f3 0f 1e fb          	endbr32 
80107784:	55                   	push   %ebp
80107785:	89 e5                	mov    %esp,%ebp
80107787:	57                   	push   %edi
80107788:	56                   	push   %esi
80107789:	53                   	push   %ebx
8010778a:	83 ec 1c             	sub    $0x1c,%esp
8010778d:	8b 75 08             	mov    0x8(%ebp),%esi
  if(p == 0)
80107790:	85 f6                	test   %esi,%esi
80107792:	0f 84 cb 00 00 00    	je     80107863 <switchuvm+0xe3>
  if(p->kstack == 0)
80107798:	8b 46 08             	mov    0x8(%esi),%eax
8010779b:	85 c0                	test   %eax,%eax
8010779d:	0f 84 da 00 00 00    	je     8010787d <switchuvm+0xfd>
  if(p->pgdir == 0)
801077a3:	8b 46 04             	mov    0x4(%esi),%eax
801077a6:	85 c0                	test   %eax,%eax
801077a8:	0f 84 c2 00 00 00    	je     80107870 <switchuvm+0xf0>
  pushcli();
801077ae:	e8 3d d8 ff ff       	call   80104ff0 <pushcli>
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
801077b3:	e8 c8 c8 ff ff       	call   80104080 <mycpu>
801077b8:	89 c3                	mov    %eax,%ebx
801077ba:	e8 c1 c8 ff ff       	call   80104080 <mycpu>
801077bf:	89 c7                	mov    %eax,%edi
801077c1:	e8 ba c8 ff ff       	call   80104080 <mycpu>
801077c6:	83 c7 08             	add    $0x8,%edi
801077c9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801077cc:	e8 af c8 ff ff       	call   80104080 <mycpu>
801077d1:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
801077d4:	ba 67 00 00 00       	mov    $0x67,%edx
801077d9:	66 89 bb 9a 00 00 00 	mov    %di,0x9a(%ebx)
801077e0:	83 c0 08             	add    $0x8,%eax
801077e3:	66 89 93 98 00 00 00 	mov    %dx,0x98(%ebx)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
801077ea:	bf ff ff ff ff       	mov    $0xffffffff,%edi
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
801077ef:	83 c1 08             	add    $0x8,%ecx
801077f2:	c1 e8 18             	shr    $0x18,%eax
801077f5:	c1 e9 10             	shr    $0x10,%ecx
801077f8:	88 83 9f 00 00 00    	mov    %al,0x9f(%ebx)
801077fe:	88 8b 9c 00 00 00    	mov    %cl,0x9c(%ebx)
80107804:	b9 99 40 00 00       	mov    $0x4099,%ecx
80107809:	66 89 8b 9d 00 00 00 	mov    %cx,0x9d(%ebx)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80107810:	bb 10 00 00 00       	mov    $0x10,%ebx
  mycpu()->gdt[SEG_TSS].s = 0;
80107815:	e8 66 c8 ff ff       	call   80104080 <mycpu>
8010781a:	80 a0 9d 00 00 00 ef 	andb   $0xef,0x9d(%eax)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80107821:	e8 5a c8 ff ff       	call   80104080 <mycpu>
80107826:	66 89 58 10          	mov    %bx,0x10(%eax)
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
8010782a:	8b 5e 08             	mov    0x8(%esi),%ebx
8010782d:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80107833:	e8 48 c8 ff ff       	call   80104080 <mycpu>
80107838:	89 58 0c             	mov    %ebx,0xc(%eax)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
8010783b:	e8 40 c8 ff ff       	call   80104080 <mycpu>
80107840:	66 89 78 6e          	mov    %di,0x6e(%eax)
  asm volatile("ltr %0" : : "r" (sel));
80107844:	b8 28 00 00 00       	mov    $0x28,%eax
80107849:	0f 00 d8             	ltr    %ax
  lcr3(V2P(p->pgdir));  // switch to process's address space
8010784c:	8b 46 04             	mov    0x4(%esi),%eax
8010784f:	05 00 00 00 80       	add    $0x80000000,%eax
  asm volatile("movl %0,%%cr3" : : "r" (val));
80107854:	0f 22 d8             	mov    %eax,%cr3
}
80107857:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010785a:	5b                   	pop    %ebx
8010785b:	5e                   	pop    %esi
8010785c:	5f                   	pop    %edi
8010785d:	5d                   	pop    %ebp
  popcli();
8010785e:	e9 dd d7 ff ff       	jmp    80105040 <popcli>
    panic("switchuvm: no process");
80107863:	83 ec 0c             	sub    $0xc,%esp
80107866:	68 56 89 10 80       	push   $0x80108956
8010786b:	e8 20 8b ff ff       	call   80100390 <panic>
    panic("switchuvm: no pgdir");
80107870:	83 ec 0c             	sub    $0xc,%esp
80107873:	68 81 89 10 80       	push   $0x80108981
80107878:	e8 13 8b ff ff       	call   80100390 <panic>
    panic("switchuvm: no kstack");
8010787d:	83 ec 0c             	sub    $0xc,%esp
80107880:	68 6c 89 10 80       	push   $0x8010896c
80107885:	e8 06 8b ff ff       	call   80100390 <panic>
8010788a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80107890 <inituvm>:
{
80107890:	f3 0f 1e fb          	endbr32 
80107894:	55                   	push   %ebp
80107895:	89 e5                	mov    %esp,%ebp
80107897:	57                   	push   %edi
80107898:	56                   	push   %esi
80107899:	53                   	push   %ebx
8010789a:	83 ec 1c             	sub    $0x1c,%esp
8010789d:	8b 45 0c             	mov    0xc(%ebp),%eax
801078a0:	8b 75 10             	mov    0x10(%ebp),%esi
801078a3:	8b 7d 08             	mov    0x8(%ebp),%edi
801078a6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(sz >= PGSIZE)
801078a9:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
801078af:	77 4b                	ja     801078fc <inituvm+0x6c>
  mem = kalloc();
801078b1:	e8 ea b4 ff ff       	call   80102da0 <kalloc>
  memset(mem, 0, PGSIZE);
801078b6:	83 ec 04             	sub    $0x4,%esp
801078b9:	68 00 10 00 00       	push   $0x1000
  mem = kalloc();
801078be:	89 c3                	mov    %eax,%ebx
  memset(mem, 0, PGSIZE);
801078c0:	6a 00                	push   $0x0
801078c2:	50                   	push   %eax
801078c3:	e8 68 d9 ff ff       	call   80105230 <memset>
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U);
801078c8:	58                   	pop    %eax
801078c9:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
801078cf:	5a                   	pop    %edx
801078d0:	6a 06                	push   $0x6
801078d2:	b9 00 10 00 00       	mov    $0x1000,%ecx
801078d7:	31 d2                	xor    %edx,%edx
801078d9:	50                   	push   %eax
801078da:	89 f8                	mov    %edi,%eax
801078dc:	e8 ff fc ff ff       	call   801075e0 <mappages>
  memmove(mem, init, sz);
801078e1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801078e4:	89 75 10             	mov    %esi,0x10(%ebp)
801078e7:	83 c4 10             	add    $0x10,%esp
801078ea:	89 5d 08             	mov    %ebx,0x8(%ebp)
801078ed:	89 45 0c             	mov    %eax,0xc(%ebp)
}
801078f0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801078f3:	5b                   	pop    %ebx
801078f4:	5e                   	pop    %esi
801078f5:	5f                   	pop    %edi
801078f6:	5d                   	pop    %ebp
  memmove(mem, init, sz);
801078f7:	e9 d4 d9 ff ff       	jmp    801052d0 <memmove>
    panic("inituvm: more than a page");
801078fc:	83 ec 0c             	sub    $0xc,%esp
801078ff:	68 95 89 10 80       	push   $0x80108995
80107904:	e8 87 8a ff ff       	call   80100390 <panic>
80107909:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80107910 <loaduvm>:
{
80107910:	f3 0f 1e fb          	endbr32 
80107914:	55                   	push   %ebp
80107915:	89 e5                	mov    %esp,%ebp
80107917:	57                   	push   %edi
80107918:	56                   	push   %esi
80107919:	53                   	push   %ebx
8010791a:	83 ec 1c             	sub    $0x1c,%esp
8010791d:	8b 45 0c             	mov    0xc(%ebp),%eax
80107920:	8b 75 18             	mov    0x18(%ebp),%esi
  if((uint) addr % PGSIZE != 0)
80107923:	a9 ff 0f 00 00       	test   $0xfff,%eax
80107928:	0f 85 b7 00 00 00    	jne    801079e5 <loaduvm+0xd5>
  for(i = 0; i < sz; i += PGSIZE){
8010792e:	01 f0                	add    %esi,%eax
80107930:	89 f3                	mov    %esi,%ebx
80107932:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(readi(ip, P2V(pa), offset+i, n) != n)
80107935:	8b 45 14             	mov    0x14(%ebp),%eax
80107938:	01 f0                	add    %esi,%eax
8010793a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  for(i = 0; i < sz; i += PGSIZE){
8010793d:	85 f6                	test   %esi,%esi
8010793f:	0f 84 83 00 00 00    	je     801079c8 <loaduvm+0xb8>
80107945:	8d 76 00             	lea    0x0(%esi),%esi
  pde = &pgdir[PDX(va)];
80107948:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  if(*pde & PTE_P){
8010794b:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010794e:	29 d8                	sub    %ebx,%eax
  pde = &pgdir[PDX(va)];
80107950:	89 c2                	mov    %eax,%edx
80107952:	c1 ea 16             	shr    $0x16,%edx
  if(*pde & PTE_P){
80107955:	8b 14 91             	mov    (%ecx,%edx,4),%edx
80107958:	f6 c2 01             	test   $0x1,%dl
8010795b:	75 13                	jne    80107970 <loaduvm+0x60>
      panic("loaduvm: address should exist");
8010795d:	83 ec 0c             	sub    $0xc,%esp
80107960:	68 af 89 10 80       	push   $0x801089af
80107965:	e8 26 8a ff ff       	call   80100390 <panic>
8010796a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return &pgtab[PTX(va)];
80107970:	c1 e8 0a             	shr    $0xa,%eax
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80107973:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
  return &pgtab[PTX(va)];
80107979:	25 fc 0f 00 00       	and    $0xffc,%eax
8010797e:	8d 84 02 00 00 00 80 	lea    -0x80000000(%edx,%eax,1),%eax
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80107985:	85 c0                	test   %eax,%eax
80107987:	74 d4                	je     8010795d <loaduvm+0x4d>
    pa = PTE_ADDR(*pte);
80107989:	8b 00                	mov    (%eax),%eax
    if(readi(ip, P2V(pa), offset+i, n) != n)
8010798b:	8b 4d e0             	mov    -0x20(%ebp),%ecx
    if(sz - i < PGSIZE)
8010798e:	bf 00 10 00 00       	mov    $0x1000,%edi
    pa = PTE_ADDR(*pte);
80107993:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if(sz - i < PGSIZE)
80107998:	81 fb ff 0f 00 00    	cmp    $0xfff,%ebx
8010799e:	0f 46 fb             	cmovbe %ebx,%edi
    if(readi(ip, P2V(pa), offset+i, n) != n)
801079a1:	29 d9                	sub    %ebx,%ecx
801079a3:	05 00 00 00 80       	add    $0x80000000,%eax
801079a8:	57                   	push   %edi
801079a9:	51                   	push   %ecx
801079aa:	50                   	push   %eax
801079ab:	ff 75 10             	push   0x10(%ebp)
801079ae:	e8 cd a7 ff ff       	call   80102180 <readi>
801079b3:	83 c4 10             	add    $0x10,%esp
801079b6:	39 f8                	cmp    %edi,%eax
801079b8:	75 1e                	jne    801079d8 <loaduvm+0xc8>
  for(i = 0; i < sz; i += PGSIZE){
801079ba:	81 eb 00 10 00 00    	sub    $0x1000,%ebx
801079c0:	89 f0                	mov    %esi,%eax
801079c2:	29 d8                	sub    %ebx,%eax
801079c4:	39 c6                	cmp    %eax,%esi
801079c6:	77 80                	ja     80107948 <loaduvm+0x38>
}
801079c8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
801079cb:	31 c0                	xor    %eax,%eax
}
801079cd:	5b                   	pop    %ebx
801079ce:	5e                   	pop    %esi
801079cf:	5f                   	pop    %edi
801079d0:	5d                   	pop    %ebp
801079d1:	c3                   	ret    
801079d2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801079d8:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
801079db:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801079e0:	5b                   	pop    %ebx
801079e1:	5e                   	pop    %esi
801079e2:	5f                   	pop    %edi
801079e3:	5d                   	pop    %ebp
801079e4:	c3                   	ret    
    panic("loaduvm: addr must be page aligned");
801079e5:	83 ec 0c             	sub    $0xc,%esp
801079e8:	68 50 8a 10 80       	push   $0x80108a50
801079ed:	e8 9e 89 ff ff       	call   80100390 <panic>
801079f2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801079f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80107a00 <allocuvm>:
{
80107a00:	f3 0f 1e fb          	endbr32 
80107a04:	55                   	push   %ebp
80107a05:	89 e5                	mov    %esp,%ebp
80107a07:	57                   	push   %edi
80107a08:	56                   	push   %esi
80107a09:	53                   	push   %ebx
80107a0a:	83 ec 1c             	sub    $0x1c,%esp
  if(newsz >= KERNBASE)
80107a0d:	8b 45 10             	mov    0x10(%ebp),%eax
{
80107a10:	8b 7d 08             	mov    0x8(%ebp),%edi
  if(newsz >= KERNBASE)
80107a13:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80107a16:	85 c0                	test   %eax,%eax
80107a18:	0f 88 b2 00 00 00    	js     80107ad0 <allocuvm+0xd0>
  if(newsz < oldsz)
80107a1e:	3b 45 0c             	cmp    0xc(%ebp),%eax
    return oldsz;
80107a21:	8b 45 0c             	mov    0xc(%ebp),%eax
  if(newsz < oldsz)
80107a24:	0f 82 96 00 00 00    	jb     80107ac0 <allocuvm+0xc0>
  a = PGROUNDUP(oldsz);
80107a2a:	8d b0 ff 0f 00 00    	lea    0xfff(%eax),%esi
80107a30:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
  for(; a < newsz; a += PGSIZE){
80107a36:	39 75 10             	cmp    %esi,0x10(%ebp)
80107a39:	77 40                	ja     80107a7b <allocuvm+0x7b>
80107a3b:	e9 83 00 00 00       	jmp    80107ac3 <allocuvm+0xc3>
    memset(mem, 0, PGSIZE);
80107a40:	83 ec 04             	sub    $0x4,%esp
80107a43:	68 00 10 00 00       	push   $0x1000
80107a48:	6a 00                	push   $0x0
80107a4a:	50                   	push   %eax
80107a4b:	e8 e0 d7 ff ff       	call   80105230 <memset>
    if(mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
80107a50:	58                   	pop    %eax
80107a51:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80107a57:	5a                   	pop    %edx
80107a58:	6a 06                	push   $0x6
80107a5a:	b9 00 10 00 00       	mov    $0x1000,%ecx
80107a5f:	89 f2                	mov    %esi,%edx
80107a61:	50                   	push   %eax
80107a62:	89 f8                	mov    %edi,%eax
80107a64:	e8 77 fb ff ff       	call   801075e0 <mappages>
80107a69:	83 c4 10             	add    $0x10,%esp
80107a6c:	85 c0                	test   %eax,%eax
80107a6e:	78 78                	js     80107ae8 <allocuvm+0xe8>
  for(; a < newsz; a += PGSIZE){
80107a70:	81 c6 00 10 00 00    	add    $0x1000,%esi
80107a76:	39 75 10             	cmp    %esi,0x10(%ebp)
80107a79:	76 48                	jbe    80107ac3 <allocuvm+0xc3>
    mem = kalloc();
80107a7b:	e8 20 b3 ff ff       	call   80102da0 <kalloc>
80107a80:	89 c3                	mov    %eax,%ebx
    if(mem == 0){
80107a82:	85 c0                	test   %eax,%eax
80107a84:	75 ba                	jne    80107a40 <allocuvm+0x40>
      cprintf("allocuvm out of memory\n");
80107a86:	83 ec 0c             	sub    $0xc,%esp
80107a89:	68 cd 89 10 80       	push   $0x801089cd
80107a8e:	e8 9d 8c ff ff       	call   80100730 <cprintf>
  if(newsz >= oldsz)
80107a93:	8b 45 0c             	mov    0xc(%ebp),%eax
80107a96:	83 c4 10             	add    $0x10,%esp
80107a99:	39 45 10             	cmp    %eax,0x10(%ebp)
80107a9c:	74 32                	je     80107ad0 <allocuvm+0xd0>
80107a9e:	8b 55 10             	mov    0x10(%ebp),%edx
80107aa1:	89 c1                	mov    %eax,%ecx
80107aa3:	89 f8                	mov    %edi,%eax
80107aa5:	e8 86 fa ff ff       	call   80107530 <deallocuvm.part.0>
      return 0;
80107aaa:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
}
80107ab1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107ab4:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107ab7:	5b                   	pop    %ebx
80107ab8:	5e                   	pop    %esi
80107ab9:	5f                   	pop    %edi
80107aba:	5d                   	pop    %ebp
80107abb:	c3                   	ret    
80107abc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return oldsz;
80107ac0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
}
80107ac3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107ac6:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107ac9:	5b                   	pop    %ebx
80107aca:	5e                   	pop    %esi
80107acb:	5f                   	pop    %edi
80107acc:	5d                   	pop    %ebp
80107acd:	c3                   	ret    
80107ace:	66 90                	xchg   %ax,%ax
    return 0;
80107ad0:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
}
80107ad7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107ada:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107add:	5b                   	pop    %ebx
80107ade:	5e                   	pop    %esi
80107adf:	5f                   	pop    %edi
80107ae0:	5d                   	pop    %ebp
80107ae1:	c3                   	ret    
80107ae2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      cprintf("allocuvm out of memory (2)\n");
80107ae8:	83 ec 0c             	sub    $0xc,%esp
80107aeb:	68 e5 89 10 80       	push   $0x801089e5
80107af0:	e8 3b 8c ff ff       	call   80100730 <cprintf>
  if(newsz >= oldsz)
80107af5:	8b 45 0c             	mov    0xc(%ebp),%eax
80107af8:	83 c4 10             	add    $0x10,%esp
80107afb:	39 45 10             	cmp    %eax,0x10(%ebp)
80107afe:	74 0c                	je     80107b0c <allocuvm+0x10c>
80107b00:	8b 55 10             	mov    0x10(%ebp),%edx
80107b03:	89 c1                	mov    %eax,%ecx
80107b05:	89 f8                	mov    %edi,%eax
80107b07:	e8 24 fa ff ff       	call   80107530 <deallocuvm.part.0>
      kfree(mem);
80107b0c:	83 ec 0c             	sub    $0xc,%esp
80107b0f:	53                   	push   %ebx
80107b10:	e8 cb b0 ff ff       	call   80102be0 <kfree>
      return 0;
80107b15:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80107b1c:	83 c4 10             	add    $0x10,%esp
}
80107b1f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107b22:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107b25:	5b                   	pop    %ebx
80107b26:	5e                   	pop    %esi
80107b27:	5f                   	pop    %edi
80107b28:	5d                   	pop    %ebp
80107b29:	c3                   	ret    
80107b2a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80107b30 <deallocuvm>:
{
80107b30:	f3 0f 1e fb          	endbr32 
80107b34:	55                   	push   %ebp
80107b35:	89 e5                	mov    %esp,%ebp
80107b37:	8b 55 0c             	mov    0xc(%ebp),%edx
80107b3a:	8b 4d 10             	mov    0x10(%ebp),%ecx
80107b3d:	8b 45 08             	mov    0x8(%ebp),%eax
  if(newsz >= oldsz)
80107b40:	39 d1                	cmp    %edx,%ecx
80107b42:	73 0c                	jae    80107b50 <deallocuvm+0x20>
}
80107b44:	5d                   	pop    %ebp
80107b45:	e9 e6 f9 ff ff       	jmp    80107530 <deallocuvm.part.0>
80107b4a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80107b50:	89 d0                	mov    %edx,%eax
80107b52:	5d                   	pop    %ebp
80107b53:	c3                   	ret    
80107b54:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107b5b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80107b5f:	90                   	nop

80107b60 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80107b60:	f3 0f 1e fb          	endbr32 
80107b64:	55                   	push   %ebp
80107b65:	89 e5                	mov    %esp,%ebp
80107b67:	57                   	push   %edi
80107b68:	56                   	push   %esi
80107b69:	53                   	push   %ebx
80107b6a:	83 ec 0c             	sub    $0xc,%esp
80107b6d:	8b 75 08             	mov    0x8(%ebp),%esi
  uint i;

  if(pgdir == 0)
80107b70:	85 f6                	test   %esi,%esi
80107b72:	74 55                	je     80107bc9 <freevm+0x69>
  if(newsz >= oldsz)
80107b74:	31 c9                	xor    %ecx,%ecx
80107b76:	ba 00 00 00 80       	mov    $0x80000000,%edx
80107b7b:	89 f0                	mov    %esi,%eax
80107b7d:	89 f3                	mov    %esi,%ebx
80107b7f:	e8 ac f9 ff ff       	call   80107530 <deallocuvm.part.0>
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80107b84:	8d be 00 10 00 00    	lea    0x1000(%esi),%edi
80107b8a:	eb 0b                	jmp    80107b97 <freevm+0x37>
80107b8c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80107b90:	83 c3 04             	add    $0x4,%ebx
80107b93:	39 df                	cmp    %ebx,%edi
80107b95:	74 23                	je     80107bba <freevm+0x5a>
    if(pgdir[i] & PTE_P){
80107b97:	8b 03                	mov    (%ebx),%eax
80107b99:	a8 01                	test   $0x1,%al
80107b9b:	74 f3                	je     80107b90 <freevm+0x30>
      char * v = P2V(PTE_ADDR(pgdir[i]));
80107b9d:	25 00 f0 ff ff       	and    $0xfffff000,%eax
      kfree(v);
80107ba2:	83 ec 0c             	sub    $0xc,%esp
  for(i = 0; i < NPDENTRIES; i++){
80107ba5:	83 c3 04             	add    $0x4,%ebx
      char * v = P2V(PTE_ADDR(pgdir[i]));
80107ba8:	05 00 00 00 80       	add    $0x80000000,%eax
      kfree(v);
80107bad:	50                   	push   %eax
80107bae:	e8 2d b0 ff ff       	call   80102be0 <kfree>
80107bb3:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < NPDENTRIES; i++){
80107bb6:	39 df                	cmp    %ebx,%edi
80107bb8:	75 dd                	jne    80107b97 <freevm+0x37>
    }
  }
  kfree((char*)pgdir);
80107bba:	89 75 08             	mov    %esi,0x8(%ebp)
}
80107bbd:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107bc0:	5b                   	pop    %ebx
80107bc1:	5e                   	pop    %esi
80107bc2:	5f                   	pop    %edi
80107bc3:	5d                   	pop    %ebp
  kfree((char*)pgdir);
80107bc4:	e9 17 b0 ff ff       	jmp    80102be0 <kfree>
    panic("freevm: no pgdir");
80107bc9:	83 ec 0c             	sub    $0xc,%esp
80107bcc:	68 01 8a 10 80       	push   $0x80108a01
80107bd1:	e8 ba 87 ff ff       	call   80100390 <panic>
80107bd6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107bdd:	8d 76 00             	lea    0x0(%esi),%esi

80107be0 <setupkvm>:
{
80107be0:	f3 0f 1e fb          	endbr32 
80107be4:	55                   	push   %ebp
80107be5:	89 e5                	mov    %esp,%ebp
80107be7:	56                   	push   %esi
80107be8:	53                   	push   %ebx
  if((pgdir = (pde_t*)kalloc()) == 0)
80107be9:	e8 b2 b1 ff ff       	call   80102da0 <kalloc>
80107bee:	89 c6                	mov    %eax,%esi
80107bf0:	85 c0                	test   %eax,%eax
80107bf2:	74 42                	je     80107c36 <setupkvm+0x56>
  memset(pgdir, 0, PGSIZE);
80107bf4:	83 ec 04             	sub    $0x4,%esp
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80107bf7:	bb 20 b4 10 80       	mov    $0x8010b420,%ebx
  memset(pgdir, 0, PGSIZE);
80107bfc:	68 00 10 00 00       	push   $0x1000
80107c01:	6a 00                	push   $0x0
80107c03:	50                   	push   %eax
80107c04:	e8 27 d6 ff ff       	call   80105230 <memset>
80107c09:	83 c4 10             	add    $0x10,%esp
                (uint)k->phys_start, k->perm) < 0) {
80107c0c:	8b 43 04             	mov    0x4(%ebx),%eax
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
80107c0f:	83 ec 08             	sub    $0x8,%esp
80107c12:	8b 4b 08             	mov    0x8(%ebx),%ecx
80107c15:	ff 73 0c             	push   0xc(%ebx)
80107c18:	8b 13                	mov    (%ebx),%edx
80107c1a:	50                   	push   %eax
80107c1b:	29 c1                	sub    %eax,%ecx
80107c1d:	89 f0                	mov    %esi,%eax
80107c1f:	e8 bc f9 ff ff       	call   801075e0 <mappages>
80107c24:	83 c4 10             	add    $0x10,%esp
80107c27:	85 c0                	test   %eax,%eax
80107c29:	78 15                	js     80107c40 <setupkvm+0x60>
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80107c2b:	83 c3 10             	add    $0x10,%ebx
80107c2e:	81 fb 60 b4 10 80    	cmp    $0x8010b460,%ebx
80107c34:	75 d6                	jne    80107c0c <setupkvm+0x2c>
}
80107c36:	8d 65 f8             	lea    -0x8(%ebp),%esp
80107c39:	89 f0                	mov    %esi,%eax
80107c3b:	5b                   	pop    %ebx
80107c3c:	5e                   	pop    %esi
80107c3d:	5d                   	pop    %ebp
80107c3e:	c3                   	ret    
80107c3f:	90                   	nop
      freevm(pgdir);
80107c40:	83 ec 0c             	sub    $0xc,%esp
80107c43:	56                   	push   %esi
      return 0;
80107c44:	31 f6                	xor    %esi,%esi
      freevm(pgdir);
80107c46:	e8 15 ff ff ff       	call   80107b60 <freevm>
      return 0;
80107c4b:	83 c4 10             	add    $0x10,%esp
}
80107c4e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80107c51:	89 f0                	mov    %esi,%eax
80107c53:	5b                   	pop    %ebx
80107c54:	5e                   	pop    %esi
80107c55:	5d                   	pop    %ebp
80107c56:	c3                   	ret    
80107c57:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107c5e:	66 90                	xchg   %ax,%ax

80107c60 <kvmalloc>:
{
80107c60:	f3 0f 1e fb          	endbr32 
80107c64:	55                   	push   %ebp
80107c65:	89 e5                	mov    %esp,%ebp
80107c67:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
80107c6a:	e8 71 ff ff ff       	call   80107be0 <setupkvm>
80107c6f:	a3 c4 5a 11 80       	mov    %eax,0x80115ac4
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80107c74:	05 00 00 00 80       	add    $0x80000000,%eax
80107c79:	0f 22 d8             	mov    %eax,%cr3
}
80107c7c:	c9                   	leave  
80107c7d:	c3                   	ret    
80107c7e:	66 90                	xchg   %ax,%ax

80107c80 <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80107c80:	f3 0f 1e fb          	endbr32 
80107c84:	55                   	push   %ebp
80107c85:	89 e5                	mov    %esp,%ebp
80107c87:	83 ec 08             	sub    $0x8,%esp
80107c8a:	8b 45 0c             	mov    0xc(%ebp),%eax
  if(*pde & PTE_P){
80107c8d:	8b 55 08             	mov    0x8(%ebp),%edx
  pde = &pgdir[PDX(va)];
80107c90:	89 c1                	mov    %eax,%ecx
80107c92:	c1 e9 16             	shr    $0x16,%ecx
  if(*pde & PTE_P){
80107c95:	8b 14 8a             	mov    (%edx,%ecx,4),%edx
80107c98:	f6 c2 01             	test   $0x1,%dl
80107c9b:	75 13                	jne    80107cb0 <clearpteu+0x30>
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if(pte == 0)
    panic("clearpteu");
80107c9d:	83 ec 0c             	sub    $0xc,%esp
80107ca0:	68 12 8a 10 80       	push   $0x80108a12
80107ca5:	e8 e6 86 ff ff       	call   80100390 <panic>
80107caa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return &pgtab[PTX(va)];
80107cb0:	c1 e8 0a             	shr    $0xa,%eax
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80107cb3:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
  return &pgtab[PTX(va)];
80107cb9:	25 fc 0f 00 00       	and    $0xffc,%eax
80107cbe:	8d 84 02 00 00 00 80 	lea    -0x80000000(%edx,%eax,1),%eax
  if(pte == 0)
80107cc5:	85 c0                	test   %eax,%eax
80107cc7:	74 d4                	je     80107c9d <clearpteu+0x1d>
  *pte &= ~PTE_U;
80107cc9:	83 20 fb             	andl   $0xfffffffb,(%eax)
}
80107ccc:	c9                   	leave  
80107ccd:	c3                   	ret    
80107cce:	66 90                	xchg   %ax,%ax

80107cd0 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80107cd0:	f3 0f 1e fb          	endbr32 
80107cd4:	55                   	push   %ebp
80107cd5:	89 e5                	mov    %esp,%ebp
80107cd7:	57                   	push   %edi
80107cd8:	56                   	push   %esi
80107cd9:	53                   	push   %ebx
80107cda:	83 ec 1c             	sub    $0x1c,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
80107cdd:	e8 fe fe ff ff       	call   80107be0 <setupkvm>
80107ce2:	89 45 e0             	mov    %eax,-0x20(%ebp)
80107ce5:	85 c0                	test   %eax,%eax
80107ce7:	0f 84 b9 00 00 00    	je     80107da6 <copyuvm+0xd6>
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
80107ced:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80107cf0:	85 c9                	test   %ecx,%ecx
80107cf2:	0f 84 ae 00 00 00    	je     80107da6 <copyuvm+0xd6>
80107cf8:	31 f6                	xor    %esi,%esi
80107cfa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  if(*pde & PTE_P){
80107d00:	8b 4d 08             	mov    0x8(%ebp),%ecx
  pde = &pgdir[PDX(va)];
80107d03:	89 f0                	mov    %esi,%eax
80107d05:	c1 e8 16             	shr    $0x16,%eax
  if(*pde & PTE_P){
80107d08:	8b 04 81             	mov    (%ecx,%eax,4),%eax
80107d0b:	a8 01                	test   $0x1,%al
80107d0d:	75 11                	jne    80107d20 <copyuvm+0x50>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
      panic("copyuvm: pte should exist");
80107d0f:	83 ec 0c             	sub    $0xc,%esp
80107d12:	68 1c 8a 10 80       	push   $0x80108a1c
80107d17:	e8 74 86 ff ff       	call   80100390 <panic>
80107d1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  return &pgtab[PTX(va)];
80107d20:	89 f2                	mov    %esi,%edx
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80107d22:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  return &pgtab[PTX(va)];
80107d27:	c1 ea 0a             	shr    $0xa,%edx
80107d2a:	81 e2 fc 0f 00 00    	and    $0xffc,%edx
80107d30:	8d 84 10 00 00 00 80 	lea    -0x80000000(%eax,%edx,1),%eax
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
80107d37:	85 c0                	test   %eax,%eax
80107d39:	74 d4                	je     80107d0f <copyuvm+0x3f>
    if(!(*pte & PTE_P))
80107d3b:	8b 00                	mov    (%eax),%eax
80107d3d:	a8 01                	test   $0x1,%al
80107d3f:	0f 84 9f 00 00 00    	je     80107de4 <copyuvm+0x114>
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
80107d45:	89 c7                	mov    %eax,%edi
    flags = PTE_FLAGS(*pte);
80107d47:	25 ff 0f 00 00       	and    $0xfff,%eax
80107d4c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    pa = PTE_ADDR(*pte);
80107d4f:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
    if((mem = kalloc()) == 0)
80107d55:	e8 46 b0 ff ff       	call   80102da0 <kalloc>
80107d5a:	89 c3                	mov    %eax,%ebx
80107d5c:	85 c0                	test   %eax,%eax
80107d5e:	74 64                	je     80107dc4 <copyuvm+0xf4>
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
80107d60:	83 ec 04             	sub    $0x4,%esp
80107d63:	81 c7 00 00 00 80    	add    $0x80000000,%edi
80107d69:	68 00 10 00 00       	push   $0x1000
80107d6e:	57                   	push   %edi
80107d6f:	50                   	push   %eax
80107d70:	e8 5b d5 ff ff       	call   801052d0 <memmove>
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0) {
80107d75:	58                   	pop    %eax
80107d76:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80107d7c:	5a                   	pop    %edx
80107d7d:	ff 75 e4             	push   -0x1c(%ebp)
80107d80:	b9 00 10 00 00       	mov    $0x1000,%ecx
80107d85:	89 f2                	mov    %esi,%edx
80107d87:	50                   	push   %eax
80107d88:	8b 45 e0             	mov    -0x20(%ebp),%eax
80107d8b:	e8 50 f8 ff ff       	call   801075e0 <mappages>
80107d90:	83 c4 10             	add    $0x10,%esp
80107d93:	85 c0                	test   %eax,%eax
80107d95:	78 21                	js     80107db8 <copyuvm+0xe8>
  for(i = 0; i < sz; i += PGSIZE){
80107d97:	81 c6 00 10 00 00    	add    $0x1000,%esi
80107d9d:	39 75 0c             	cmp    %esi,0xc(%ebp)
80107da0:	0f 87 5a ff ff ff    	ja     80107d00 <copyuvm+0x30>
  return d;

bad:
  freevm(d);
  return 0;
}
80107da6:	8b 45 e0             	mov    -0x20(%ebp),%eax
80107da9:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107dac:	5b                   	pop    %ebx
80107dad:	5e                   	pop    %esi
80107dae:	5f                   	pop    %edi
80107daf:	5d                   	pop    %ebp
80107db0:	c3                   	ret    
80107db1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      kfree(mem);
80107db8:	83 ec 0c             	sub    $0xc,%esp
80107dbb:	53                   	push   %ebx
80107dbc:	e8 1f ae ff ff       	call   80102be0 <kfree>
      goto bad;
80107dc1:	83 c4 10             	add    $0x10,%esp
  freevm(d);
80107dc4:	83 ec 0c             	sub    $0xc,%esp
80107dc7:	ff 75 e0             	push   -0x20(%ebp)
80107dca:	e8 91 fd ff ff       	call   80107b60 <freevm>
  return 0;
80107dcf:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
80107dd6:	83 c4 10             	add    $0x10,%esp
}
80107dd9:	8b 45 e0             	mov    -0x20(%ebp),%eax
80107ddc:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107ddf:	5b                   	pop    %ebx
80107de0:	5e                   	pop    %esi
80107de1:	5f                   	pop    %edi
80107de2:	5d                   	pop    %ebp
80107de3:	c3                   	ret    
      panic("copyuvm: page not present");
80107de4:	83 ec 0c             	sub    $0xc,%esp
80107de7:	68 36 8a 10 80       	push   $0x80108a36
80107dec:	e8 9f 85 ff ff       	call   80100390 <panic>
80107df1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107df8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107dff:	90                   	nop

80107e00 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
80107e00:	f3 0f 1e fb          	endbr32 
80107e04:	55                   	push   %ebp
80107e05:	89 e5                	mov    %esp,%ebp
80107e07:	8b 45 0c             	mov    0xc(%ebp),%eax
  if(*pde & PTE_P){
80107e0a:	8b 55 08             	mov    0x8(%ebp),%edx
  pde = &pgdir[PDX(va)];
80107e0d:	89 c1                	mov    %eax,%ecx
80107e0f:	c1 e9 16             	shr    $0x16,%ecx
  if(*pde & PTE_P){
80107e12:	8b 14 8a             	mov    (%edx,%ecx,4),%edx
80107e15:	f6 c2 01             	test   $0x1,%dl
80107e18:	0f 84 fc 00 00 00    	je     80107f1a <uva2ka.cold>
  return &pgtab[PTX(va)];
80107e1e:	c1 e8 0c             	shr    $0xc,%eax
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80107e21:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
  if((*pte & PTE_P) == 0)
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)P2V(PTE_ADDR(*pte));
}
80107e27:	5d                   	pop    %ebp
  return &pgtab[PTX(va)];
80107e28:	25 ff 03 00 00       	and    $0x3ff,%eax
  if((*pte & PTE_P) == 0)
80107e2d:	8b 84 82 00 00 00 80 	mov    -0x80000000(%edx,%eax,4),%eax
  if((*pte & PTE_U) == 0)
80107e34:	89 c2                	mov    %eax,%edx
  return (char*)P2V(PTE_ADDR(*pte));
80107e36:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  if((*pte & PTE_U) == 0)
80107e3b:	83 e2 05             	and    $0x5,%edx
  return (char*)P2V(PTE_ADDR(*pte));
80107e3e:	05 00 00 00 80       	add    $0x80000000,%eax
80107e43:	83 fa 05             	cmp    $0x5,%edx
80107e46:	ba 00 00 00 00       	mov    $0x0,%edx
80107e4b:	0f 45 c2             	cmovne %edx,%eax
}
80107e4e:	c3                   	ret    
80107e4f:	90                   	nop

80107e50 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80107e50:	f3 0f 1e fb          	endbr32 
80107e54:	55                   	push   %ebp
80107e55:	89 e5                	mov    %esp,%ebp
80107e57:	57                   	push   %edi
80107e58:	56                   	push   %esi
80107e59:	53                   	push   %ebx
80107e5a:	83 ec 0c             	sub    $0xc,%esp
80107e5d:	8b 75 14             	mov    0x14(%ebp),%esi
80107e60:	8b 45 0c             	mov    0xc(%ebp),%eax
80107e63:	8b 55 10             	mov    0x10(%ebp),%edx
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
80107e66:	85 f6                	test   %esi,%esi
80107e68:	75 4d                	jne    80107eb7 <copyout+0x67>
80107e6a:	e9 a1 00 00 00       	jmp    80107f10 <copyout+0xc0>
80107e6f:	90                   	nop
  return (char*)P2V(PTE_ADDR(*pte));
80107e70:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
80107e76:	8d 8b 00 00 00 80    	lea    -0x80000000(%ebx),%ecx
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
80107e7c:	81 fb 00 00 00 80    	cmp    $0x80000000,%ebx
80107e82:	74 75                	je     80107ef9 <copyout+0xa9>
      return -1;
    n = PGSIZE - (va - va0);
80107e84:	89 fb                	mov    %edi,%ebx
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
80107e86:	89 55 10             	mov    %edx,0x10(%ebp)
    n = PGSIZE - (va - va0);
80107e89:	29 c3                	sub    %eax,%ebx
80107e8b:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    if(n > len)
80107e91:	39 f3                	cmp    %esi,%ebx
80107e93:	0f 47 de             	cmova  %esi,%ebx
    memmove(pa0 + (va - va0), buf, n);
80107e96:	29 f8                	sub    %edi,%eax
80107e98:	83 ec 04             	sub    $0x4,%esp
80107e9b:	01 c8                	add    %ecx,%eax
80107e9d:	53                   	push   %ebx
80107e9e:	52                   	push   %edx
80107e9f:	50                   	push   %eax
80107ea0:	e8 2b d4 ff ff       	call   801052d0 <memmove>
    len -= n;
    buf += n;
80107ea5:	8b 55 10             	mov    0x10(%ebp),%edx
    va = va0 + PGSIZE;
80107ea8:	8d 87 00 10 00 00    	lea    0x1000(%edi),%eax
  while(len > 0){
80107eae:	83 c4 10             	add    $0x10,%esp
    buf += n;
80107eb1:	01 da                	add    %ebx,%edx
  while(len > 0){
80107eb3:	29 de                	sub    %ebx,%esi
80107eb5:	74 59                	je     80107f10 <copyout+0xc0>
  if(*pde & PTE_P){
80107eb7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  pde = &pgdir[PDX(va)];
80107eba:	89 c1                	mov    %eax,%ecx
    va0 = (uint)PGROUNDDOWN(va);
80107ebc:	89 c7                	mov    %eax,%edi
  pde = &pgdir[PDX(va)];
80107ebe:	c1 e9 16             	shr    $0x16,%ecx
    va0 = (uint)PGROUNDDOWN(va);
80107ec1:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
  if(*pde & PTE_P){
80107ec7:	8b 0c 8b             	mov    (%ebx,%ecx,4),%ecx
80107eca:	f6 c1 01             	test   $0x1,%cl
80107ecd:	0f 84 4e 00 00 00    	je     80107f21 <copyout.cold>
  return &pgtab[PTX(va)];
80107ed3:	89 fb                	mov    %edi,%ebx
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80107ed5:	81 e1 00 f0 ff ff    	and    $0xfffff000,%ecx
  return &pgtab[PTX(va)];
80107edb:	c1 eb 0c             	shr    $0xc,%ebx
80107ede:	81 e3 ff 03 00 00    	and    $0x3ff,%ebx
  if((*pte & PTE_P) == 0)
80107ee4:	8b 9c 99 00 00 00 80 	mov    -0x80000000(%ecx,%ebx,4),%ebx
  if((*pte & PTE_U) == 0)
80107eeb:	89 d9                	mov    %ebx,%ecx
80107eed:	83 e1 05             	and    $0x5,%ecx
80107ef0:	83 f9 05             	cmp    $0x5,%ecx
80107ef3:	0f 84 77 ff ff ff    	je     80107e70 <copyout+0x20>
  }
  return 0;
}
80107ef9:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80107efc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80107f01:	5b                   	pop    %ebx
80107f02:	5e                   	pop    %esi
80107f03:	5f                   	pop    %edi
80107f04:	5d                   	pop    %ebp
80107f05:	c3                   	ret    
80107f06:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107f0d:	8d 76 00             	lea    0x0(%esi),%esi
80107f10:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80107f13:	31 c0                	xor    %eax,%eax
}
80107f15:	5b                   	pop    %ebx
80107f16:	5e                   	pop    %esi
80107f17:	5f                   	pop    %edi
80107f18:	5d                   	pop    %ebp
80107f19:	c3                   	ret    

80107f1a <uva2ka.cold>:
  if((*pte & PTE_P) == 0)
80107f1a:	a1 00 00 00 00       	mov    0x0,%eax
80107f1f:	0f 0b                	ud2    

80107f21 <copyout.cold>:
80107f21:	a1 00 00 00 00       	mov    0x0,%eax
80107f26:	0f 0b                	ud2    
