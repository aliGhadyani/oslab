
_debugger:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"
#include "stat.h"

int main(int argc, char* argv[]) {
   0:	f3 0f 1e fb          	endbr32 
   4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   8:	83 e4 f0             	and    $0xfffffff0,%esp
   b:	ff 71 fc             	push   -0x4(%ecx)
   e:	55                   	push   %ebp
   f:	89 e5                	mov    %esp,%ebp
  11:	53                   	push   %ebx
  12:	51                   	push   %ecx
  13:	8b 41 04             	mov    0x4(%ecx),%eax
    int proc = atoi(argv[1]);
  16:	83 ec 0c             	sub    $0xc,%esp
  19:	ff 70 04             	push   0x4(%eax)
  1c:	e8 6f 02 00 00       	call   290 <atoi>
    printf(1, "debugger: debugger runs for pid %d\n", proc);
  21:	83 c4 0c             	add    $0xc,%esp
  24:	50                   	push   %eax
    int proc = atoi(argv[1]);
  25:	89 c3                	mov    %eax,%ebx
    printf(1, "debugger: debugger runs for pid %d\n", proc);
  27:	68 c8 08 00 00       	push   $0x8c8
  2c:	6a 01                	push   $0x1
  2e:	e8 fd 04 00 00       	call   530 <printf>
    if(set_process_parent(proc) < 0) {
  33:	89 1c 24             	mov    %ebx,(%esp)
  36:	e8 80 03 00 00       	call   3bb <set_process_parent>
  3b:	83 c4 10             	add    $0x10,%esp
  3e:	85 c0                	test   %eax,%eax
  40:	78 4b                	js     8d <main+0x8d>
        printf(1, "debugger: process is debugging under another process\n");
        exit();
    }
    printf(1, "debugger: current process(%d) set as debugger parent of proc %d\n", getpid(), proc);
  42:	e8 3c 03 00 00       	call   383 <getpid>
  47:	53                   	push   %ebx
  48:	50                   	push   %eax
  49:	68 24 09 00 00       	push   $0x924
  4e:	6a 01                	push   $0x1
  50:	e8 db 04 00 00       	call   530 <printf>
    printf(1, "debugger: wait for proc %d to exit\n", proc);
  55:	83 c4 0c             	add    $0xc,%esp
  58:	53                   	push   %ebx
  59:	68 68 09 00 00       	push   $0x968
  5e:	6a 01                	push   $0x1
  60:	e8 cb 04 00 00       	call   530 <printf>
    while(wait() != -1);
  65:	83 c4 10             	add    $0x10,%esp
  68:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  6f:	90                   	nop
  70:	e8 96 02 00 00       	call   30b <wait>
  75:	83 f8 ff             	cmp    $0xffffffff,%eax
  78:	75 f6                	jne    70 <main+0x70>
    printf(1, "debugger: proc %d exited\n", proc);
  7a:	50                   	push   %eax
  7b:	53                   	push   %ebx
  7c:	68 8c 09 00 00       	push   $0x98c
  81:	6a 01                	push   $0x1
  83:	e8 a8 04 00 00       	call   530 <printf>
    exit();
  88:	e8 76 02 00 00       	call   303 <exit>
        printf(1, "debugger: process is debugging under another process\n");
  8d:	52                   	push   %edx
  8e:	52                   	push   %edx
  8f:	68 ec 08 00 00       	push   $0x8ec
  94:	6a 01                	push   $0x1
  96:	e8 95 04 00 00       	call   530 <printf>
        exit();
  9b:	e8 63 02 00 00       	call   303 <exit>

000000a0 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
  a0:	f3 0f 1e fb          	endbr32 
  a4:	55                   	push   %ebp
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
  a5:	31 c0                	xor    %eax,%eax
{
  a7:	89 e5                	mov    %esp,%ebp
  a9:	53                   	push   %ebx
  aa:	8b 4d 08             	mov    0x8(%ebp),%ecx
  ad:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while((*s++ = *t++) != 0)
  b0:	0f b6 14 03          	movzbl (%ebx,%eax,1),%edx
  b4:	88 14 01             	mov    %dl,(%ecx,%eax,1)
  b7:	83 c0 01             	add    $0x1,%eax
  ba:	84 d2                	test   %dl,%dl
  bc:	75 f2                	jne    b0 <strcpy+0x10>
    ;
  return os;
}
  be:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  c1:	89 c8                	mov    %ecx,%eax
  c3:	c9                   	leave  
  c4:	c3                   	ret    
  c5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000000d0 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  d0:	f3 0f 1e fb          	endbr32 
  d4:	55                   	push   %ebp
  d5:	89 e5                	mov    %esp,%ebp
  d7:	53                   	push   %ebx
  d8:	8b 4d 08             	mov    0x8(%ebp),%ecx
  db:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
  de:	0f b6 01             	movzbl (%ecx),%eax
  e1:	0f b6 1a             	movzbl (%edx),%ebx
  e4:	84 c0                	test   %al,%al
  e6:	75 18                	jne    100 <strcmp+0x30>
  e8:	eb 2a                	jmp    114 <strcmp+0x44>
  ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  f0:	0f b6 41 01          	movzbl 0x1(%ecx),%eax
    p++, q++;
  f4:	83 c1 01             	add    $0x1,%ecx
  f7:	8d 5a 01             	lea    0x1(%edx),%ebx
  while(*p && *p == *q)
  fa:	84 c0                	test   %al,%al
  fc:	74 12                	je     110 <strcmp+0x40>
    p++, q++;
  fe:	89 da                	mov    %ebx,%edx
  while(*p && *p == *q)
 100:	0f b6 1a             	movzbl (%edx),%ebx
 103:	38 c3                	cmp    %al,%bl
 105:	74 e9                	je     f0 <strcmp+0x20>
  return (uchar)*p - (uchar)*q;
 107:	29 d8                	sub    %ebx,%eax
}
 109:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 10c:	c9                   	leave  
 10d:	c3                   	ret    
 10e:	66 90                	xchg   %ax,%ax
  return (uchar)*p - (uchar)*q;
 110:	0f b6 5a 01          	movzbl 0x1(%edx),%ebx
 114:	31 c0                	xor    %eax,%eax
 116:	29 d8                	sub    %ebx,%eax
}
 118:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 11b:	c9                   	leave  
 11c:	c3                   	ret    
 11d:	8d 76 00             	lea    0x0(%esi),%esi

00000120 <strlen>:

uint
strlen(const char *s)
{
 120:	f3 0f 1e fb          	endbr32 
 124:	55                   	push   %ebp
 125:	89 e5                	mov    %esp,%ebp
 127:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
 12a:	80 3a 00             	cmpb   $0x0,(%edx)
 12d:	74 21                	je     150 <strlen+0x30>
 12f:	31 c0                	xor    %eax,%eax
 131:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 138:	83 c0 01             	add    $0x1,%eax
 13b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
 13f:	89 c1                	mov    %eax,%ecx
 141:	75 f5                	jne    138 <strlen+0x18>
    ;
  return n;
}
 143:	89 c8                	mov    %ecx,%eax
 145:	5d                   	pop    %ebp
 146:	c3                   	ret    
 147:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 14e:	66 90                	xchg   %ax,%ax
  for(n = 0; s[n]; n++)
 150:	31 c9                	xor    %ecx,%ecx
}
 152:	5d                   	pop    %ebp
 153:	89 c8                	mov    %ecx,%eax
 155:	c3                   	ret    
 156:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 15d:	8d 76 00             	lea    0x0(%esi),%esi

00000160 <memset>:

void*
memset(void *dst, int c, uint n)
{
 160:	f3 0f 1e fb          	endbr32 
 164:	55                   	push   %ebp
 165:	89 e5                	mov    %esp,%ebp
 167:	57                   	push   %edi
 168:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 16b:	8b 4d 10             	mov    0x10(%ebp),%ecx
 16e:	8b 45 0c             	mov    0xc(%ebp),%eax
 171:	89 d7                	mov    %edx,%edi
 173:	fc                   	cld    
 174:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 176:	8b 7d fc             	mov    -0x4(%ebp),%edi
 179:	89 d0                	mov    %edx,%eax
 17b:	c9                   	leave  
 17c:	c3                   	ret    
 17d:	8d 76 00             	lea    0x0(%esi),%esi

00000180 <strchr>:

char*
strchr(const char *s, char c)
{
 180:	f3 0f 1e fb          	endbr32 
 184:	55                   	push   %ebp
 185:	89 e5                	mov    %esp,%ebp
 187:	8b 45 08             	mov    0x8(%ebp),%eax
 18a:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 18e:	0f b6 10             	movzbl (%eax),%edx
 191:	84 d2                	test   %dl,%dl
 193:	75 16                	jne    1ab <strchr+0x2b>
 195:	eb 21                	jmp    1b8 <strchr+0x38>
 197:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 19e:	66 90                	xchg   %ax,%ax
 1a0:	0f b6 50 01          	movzbl 0x1(%eax),%edx
 1a4:	83 c0 01             	add    $0x1,%eax
 1a7:	84 d2                	test   %dl,%dl
 1a9:	74 0d                	je     1b8 <strchr+0x38>
    if(*s == c)
 1ab:	38 d1                	cmp    %dl,%cl
 1ad:	75 f1                	jne    1a0 <strchr+0x20>
      return (char*)s;
  return 0;
}
 1af:	5d                   	pop    %ebp
 1b0:	c3                   	ret    
 1b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return 0;
 1b8:	31 c0                	xor    %eax,%eax
}
 1ba:	5d                   	pop    %ebp
 1bb:	c3                   	ret    
 1bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000001c0 <gets>:

char*
gets(char *buf, int max)
{
 1c0:	f3 0f 1e fb          	endbr32 
 1c4:	55                   	push   %ebp
 1c5:	89 e5                	mov    %esp,%ebp
 1c7:	57                   	push   %edi
 1c8:	56                   	push   %esi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    cc = read(0, &c, 1);
 1c9:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 1cc:	53                   	push   %ebx
  for(i=0; i+1 < max; ){
 1cd:	31 db                	xor    %ebx,%ebx
{
 1cf:	83 ec 1c             	sub    $0x1c,%esp
  for(i=0; i+1 < max; ){
 1d2:	eb 2b                	jmp    1ff <gets+0x3f>
 1d4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cc = read(0, &c, 1);
 1d8:	83 ec 04             	sub    $0x4,%esp
 1db:	6a 01                	push   $0x1
 1dd:	57                   	push   %edi
 1de:	6a 00                	push   $0x0
 1e0:	e8 36 01 00 00       	call   31b <read>
    if(cc < 1)
 1e5:	83 c4 10             	add    $0x10,%esp
 1e8:	85 c0                	test   %eax,%eax
 1ea:	7e 1d                	jle    209 <gets+0x49>
      break;
    buf[i++] = c;
 1ec:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 1f0:	8b 55 08             	mov    0x8(%ebp),%edx
 1f3:	88 44 1a ff          	mov    %al,-0x1(%edx,%ebx,1)
    if(c == '\n' || c == '\r')
 1f7:	3c 0a                	cmp    $0xa,%al
 1f9:	74 25                	je     220 <gets+0x60>
 1fb:	3c 0d                	cmp    $0xd,%al
 1fd:	74 21                	je     220 <gets+0x60>
  for(i=0; i+1 < max; ){
 1ff:	89 de                	mov    %ebx,%esi
 201:	83 c3 01             	add    $0x1,%ebx
 204:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
 207:	7c cf                	jl     1d8 <gets+0x18>
      break;
  }
  buf[i] = '\0';
 209:	8b 45 08             	mov    0x8(%ebp),%eax
 20c:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
  return buf;
}
 210:	8d 65 f4             	lea    -0xc(%ebp),%esp
 213:	5b                   	pop    %ebx
 214:	5e                   	pop    %esi
 215:	5f                   	pop    %edi
 216:	5d                   	pop    %ebp
 217:	c3                   	ret    
 218:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 21f:	90                   	nop
  buf[i] = '\0';
 220:	8b 45 08             	mov    0x8(%ebp),%eax
 223:	89 de                	mov    %ebx,%esi
 225:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
}
 229:	8d 65 f4             	lea    -0xc(%ebp),%esp
 22c:	5b                   	pop    %ebx
 22d:	5e                   	pop    %esi
 22e:	5f                   	pop    %edi
 22f:	5d                   	pop    %ebp
 230:	c3                   	ret    
 231:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 238:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 23f:	90                   	nop

00000240 <stat>:

int
stat(const char *n, struct stat *st)
{
 240:	f3 0f 1e fb          	endbr32 
 244:	55                   	push   %ebp
 245:	89 e5                	mov    %esp,%ebp
 247:	56                   	push   %esi
 248:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 249:	83 ec 08             	sub    $0x8,%esp
 24c:	6a 00                	push   $0x0
 24e:	ff 75 08             	push   0x8(%ebp)
 251:	e8 ed 00 00 00       	call   343 <open>
  if(fd < 0)
 256:	83 c4 10             	add    $0x10,%esp
 259:	85 c0                	test   %eax,%eax
 25b:	78 2b                	js     288 <stat+0x48>
    return -1;
  r = fstat(fd, st);
 25d:	83 ec 08             	sub    $0x8,%esp
 260:	ff 75 0c             	push   0xc(%ebp)
 263:	89 c3                	mov    %eax,%ebx
 265:	50                   	push   %eax
 266:	e8 f0 00 00 00       	call   35b <fstat>
  close(fd);
 26b:	89 1c 24             	mov    %ebx,(%esp)
  r = fstat(fd, st);
 26e:	89 c6                	mov    %eax,%esi
  close(fd);
 270:	e8 b6 00 00 00       	call   32b <close>
  return r;
 275:	83 c4 10             	add    $0x10,%esp
}
 278:	8d 65 f8             	lea    -0x8(%ebp),%esp
 27b:	89 f0                	mov    %esi,%eax
 27d:	5b                   	pop    %ebx
 27e:	5e                   	pop    %esi
 27f:	5d                   	pop    %ebp
 280:	c3                   	ret    
 281:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
 288:	be ff ff ff ff       	mov    $0xffffffff,%esi
 28d:	eb e9                	jmp    278 <stat+0x38>
 28f:	90                   	nop

00000290 <atoi>:

int
atoi(const char *s)
{
 290:	f3 0f 1e fb          	endbr32 
 294:	55                   	push   %ebp
 295:	89 e5                	mov    %esp,%ebp
 297:	53                   	push   %ebx
 298:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 29b:	0f be 02             	movsbl (%edx),%eax
 29e:	8d 48 d0             	lea    -0x30(%eax),%ecx
 2a1:	80 f9 09             	cmp    $0x9,%cl
  n = 0;
 2a4:	b9 00 00 00 00       	mov    $0x0,%ecx
  while('0' <= *s && *s <= '9')
 2a9:	77 1a                	ja     2c5 <atoi+0x35>
 2ab:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 2af:	90                   	nop
    n = n*10 + *s++ - '0';
 2b0:	83 c2 01             	add    $0x1,%edx
 2b3:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
 2b6:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
  while('0' <= *s && *s <= '9')
 2ba:	0f be 02             	movsbl (%edx),%eax
 2bd:	8d 58 d0             	lea    -0x30(%eax),%ebx
 2c0:	80 fb 09             	cmp    $0x9,%bl
 2c3:	76 eb                	jbe    2b0 <atoi+0x20>
  return n;
}
 2c5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 2c8:	89 c8                	mov    %ecx,%eax
 2ca:	c9                   	leave  
 2cb:	c3                   	ret    
 2cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000002d0 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 2d0:	f3 0f 1e fb          	endbr32 
 2d4:	55                   	push   %ebp
 2d5:	89 e5                	mov    %esp,%ebp
 2d7:	57                   	push   %edi
 2d8:	8b 45 10             	mov    0x10(%ebp),%eax
 2db:	8b 55 08             	mov    0x8(%ebp),%edx
 2de:	56                   	push   %esi
 2df:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst;
  const char *src;

  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2e2:	85 c0                	test   %eax,%eax
 2e4:	7e 0f                	jle    2f5 <memmove+0x25>
 2e6:	01 d0                	add    %edx,%eax
  dst = vdst;
 2e8:	89 d7                	mov    %edx,%edi
 2ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *dst++ = *src++;
 2f0:	a4                   	movsb  %ds:(%esi),%es:(%edi)
  while(n-- > 0)
 2f1:	39 f8                	cmp    %edi,%eax
 2f3:	75 fb                	jne    2f0 <memmove+0x20>
  return vdst;
}
 2f5:	5e                   	pop    %esi
 2f6:	89 d0                	mov    %edx,%eax
 2f8:	5f                   	pop    %edi
 2f9:	5d                   	pop    %ebp
 2fa:	c3                   	ret    

000002fb <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 2fb:	b8 01 00 00 00       	mov    $0x1,%eax
 300:	cd 40                	int    $0x40
 302:	c3                   	ret    

00000303 <exit>:
SYSCALL(exit)
 303:	b8 02 00 00 00       	mov    $0x2,%eax
 308:	cd 40                	int    $0x40
 30a:	c3                   	ret    

0000030b <wait>:
SYSCALL(wait)
 30b:	b8 03 00 00 00       	mov    $0x3,%eax
 310:	cd 40                	int    $0x40
 312:	c3                   	ret    

00000313 <pipe>:
SYSCALL(pipe)
 313:	b8 04 00 00 00       	mov    $0x4,%eax
 318:	cd 40                	int    $0x40
 31a:	c3                   	ret    

0000031b <read>:
SYSCALL(read)
 31b:	b8 05 00 00 00       	mov    $0x5,%eax
 320:	cd 40                	int    $0x40
 322:	c3                   	ret    

00000323 <write>:
SYSCALL(write)
 323:	b8 10 00 00 00       	mov    $0x10,%eax
 328:	cd 40                	int    $0x40
 32a:	c3                   	ret    

0000032b <close>:
SYSCALL(close)
 32b:	b8 15 00 00 00       	mov    $0x15,%eax
 330:	cd 40                	int    $0x40
 332:	c3                   	ret    

00000333 <kill>:
SYSCALL(kill)
 333:	b8 06 00 00 00       	mov    $0x6,%eax
 338:	cd 40                	int    $0x40
 33a:	c3                   	ret    

0000033b <exec>:
SYSCALL(exec)
 33b:	b8 07 00 00 00       	mov    $0x7,%eax
 340:	cd 40                	int    $0x40
 342:	c3                   	ret    

00000343 <open>:
SYSCALL(open)
 343:	b8 0f 00 00 00       	mov    $0xf,%eax
 348:	cd 40                	int    $0x40
 34a:	c3                   	ret    

0000034b <mknod>:
SYSCALL(mknod)
 34b:	b8 11 00 00 00       	mov    $0x11,%eax
 350:	cd 40                	int    $0x40
 352:	c3                   	ret    

00000353 <unlink>:
SYSCALL(unlink)
 353:	b8 12 00 00 00       	mov    $0x12,%eax
 358:	cd 40                	int    $0x40
 35a:	c3                   	ret    

0000035b <fstat>:
SYSCALL(fstat)
 35b:	b8 08 00 00 00       	mov    $0x8,%eax
 360:	cd 40                	int    $0x40
 362:	c3                   	ret    

00000363 <link>:
SYSCALL(link)
 363:	b8 13 00 00 00       	mov    $0x13,%eax
 368:	cd 40                	int    $0x40
 36a:	c3                   	ret    

0000036b <mkdir>:
SYSCALL(mkdir)
 36b:	b8 14 00 00 00       	mov    $0x14,%eax
 370:	cd 40                	int    $0x40
 372:	c3                   	ret    

00000373 <chdir>:
SYSCALL(chdir)
 373:	b8 09 00 00 00       	mov    $0x9,%eax
 378:	cd 40                	int    $0x40
 37a:	c3                   	ret    

0000037b <dup>:
SYSCALL(dup)
 37b:	b8 0a 00 00 00       	mov    $0xa,%eax
 380:	cd 40                	int    $0x40
 382:	c3                   	ret    

00000383 <getpid>:
SYSCALL(getpid)
 383:	b8 0b 00 00 00       	mov    $0xb,%eax
 388:	cd 40                	int    $0x40
 38a:	c3                   	ret    

0000038b <sbrk>:
SYSCALL(sbrk)
 38b:	b8 0c 00 00 00       	mov    $0xc,%eax
 390:	cd 40                	int    $0x40
 392:	c3                   	ret    

00000393 <sleep>:
SYSCALL(sleep)
 393:	b8 0d 00 00 00       	mov    $0xd,%eax
 398:	cd 40                	int    $0x40
 39a:	c3                   	ret    

0000039b <uptime>:
SYSCALL(uptime)
 39b:	b8 0e 00 00 00       	mov    $0xe,%eax
 3a0:	cd 40                	int    $0x40
 3a2:	c3                   	ret    

000003a3 <poweroff>:
SYSCALL(poweroff)
 3a3:	b8 16 00 00 00       	mov    $0x16,%eax
 3a8:	cd 40                	int    $0x40
 3aa:	c3                   	ret    

000003ab <calculate_sum_of_digits>:
SYSCALL(calculate_sum_of_digits)
 3ab:	b8 17 00 00 00       	mov    $0x17,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    

000003b3 <get_parent_pid>:
SYSCALL(get_parent_pid)
 3b3:	b8 18 00 00 00       	mov    $0x18,%eax
 3b8:	cd 40                	int    $0x40
 3ba:	c3                   	ret    

000003bb <set_process_parent>:
SYSCALL(set_process_parent)
 3bb:	b8 19 00 00 00       	mov    $0x19,%eax
 3c0:	cd 40                	int    $0x40
 3c2:	c3                   	ret    
 3c3:	66 90                	xchg   %ax,%ax
 3c5:	66 90                	xchg   %ax,%ax
 3c7:	66 90                	xchg   %ax,%ax
 3c9:	66 90                	xchg   %ax,%ax
 3cb:	66 90                	xchg   %ax,%ax
 3cd:	66 90                	xchg   %ax,%ax
 3cf:	90                   	nop

000003d0 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
 3d0:	55                   	push   %ebp
 3d1:	89 e5                	mov    %esp,%ebp
 3d3:	57                   	push   %edi
 3d4:	56                   	push   %esi
 3d5:	53                   	push   %ebx
 3d6:	83 ec 3c             	sub    $0x3c,%esp
 3d9:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
 3dc:	89 d1                	mov    %edx,%ecx
{
 3de:	89 45 b8             	mov    %eax,-0x48(%ebp)
  if(sgn && xx < 0){
 3e1:	85 d2                	test   %edx,%edx
 3e3:	0f 89 7f 00 00 00    	jns    468 <printint+0x98>
 3e9:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
 3ed:	74 79                	je     468 <printint+0x98>
    neg = 1;
 3ef:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
    x = -xx;
 3f6:	f7 d9                	neg    %ecx
  } else {
    x = xx;
  }

  i = 0;
 3f8:	31 db                	xor    %ebx,%ebx
 3fa:	8d 75 d7             	lea    -0x29(%ebp),%esi
 3fd:	8d 76 00             	lea    0x0(%esi),%esi
  do{
    buf[i++] = digits[x % base];
 400:	89 c8                	mov    %ecx,%eax
 402:	31 d2                	xor    %edx,%edx
 404:	89 cf                	mov    %ecx,%edi
 406:	f7 75 c4             	divl   -0x3c(%ebp)
 409:	0f b6 92 b0 09 00 00 	movzbl 0x9b0(%edx),%edx
 410:	89 45 c0             	mov    %eax,-0x40(%ebp)
 413:	89 d8                	mov    %ebx,%eax
 415:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
 418:	8b 4d c0             	mov    -0x40(%ebp),%ecx
    buf[i++] = digits[x % base];
 41b:	88 14 1e             	mov    %dl,(%esi,%ebx,1)
  }while((x /= base) != 0);
 41e:	39 7d c4             	cmp    %edi,-0x3c(%ebp)
 421:	76 dd                	jbe    400 <printint+0x30>
  if(neg)
 423:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 426:	85 c9                	test   %ecx,%ecx
 428:	74 0c                	je     436 <printint+0x66>
    buf[i++] = '-';
 42a:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
 42f:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
 431:	ba 2d 00 00 00       	mov    $0x2d,%edx

  while(--i >= 0)
 436:	8b 7d b8             	mov    -0x48(%ebp),%edi
 439:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
 43d:	eb 07                	jmp    446 <printint+0x76>
 43f:	90                   	nop
    putc(fd, buf[i]);
 440:	0f b6 13             	movzbl (%ebx),%edx
 443:	83 eb 01             	sub    $0x1,%ebx
  write(fd, &c, 1);
 446:	83 ec 04             	sub    $0x4,%esp
 449:	88 55 d7             	mov    %dl,-0x29(%ebp)
 44c:	6a 01                	push   $0x1
 44e:	56                   	push   %esi
 44f:	57                   	push   %edi
 450:	e8 ce fe ff ff       	call   323 <write>
  while(--i >= 0)
 455:	83 c4 10             	add    $0x10,%esp
 458:	39 de                	cmp    %ebx,%esi
 45a:	75 e4                	jne    440 <printint+0x70>
}
 45c:	8d 65 f4             	lea    -0xc(%ebp),%esp
 45f:	5b                   	pop    %ebx
 460:	5e                   	pop    %esi
 461:	5f                   	pop    %edi
 462:	5d                   	pop    %ebp
 463:	c3                   	ret    
 464:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  neg = 0;
 468:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%ebp)
 46f:	eb 87                	jmp    3f8 <printint+0x28>
 471:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 478:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 47f:	90                   	nop

00000480 <printfloat>:

// MOD-2 : Added printf for floats
void
printfloat(int fd, float xx)
{
 480:	f3 0f 1e fb          	endbr32 
 484:	55                   	push   %ebp

  int beg=(int)(xx);
	int fin=(int)(xx*100)-beg*100;
  printint(fd, beg, 10, 1);
 485:	b9 0a 00 00 00       	mov    $0xa,%ecx
{
 48a:	89 e5                	mov    %esp,%ebp
 48c:	57                   	push   %edi
 48d:	56                   	push   %esi
  write(fd, &c, 1);
 48e:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 491:	53                   	push   %ebx
 492:	83 ec 38             	sub    $0x38,%esp
 495:	d9 45 0c             	flds   0xc(%ebp)
 498:	8b 75 08             	mov    0x8(%ebp),%esi
  int beg=(int)(xx);
 49b:	d9 7d d6             	fnstcw -0x2a(%ebp)
  printint(fd, beg, 10, 1);
 49e:	6a 01                	push   $0x1
  int beg=(int)(xx);
 4a0:	0f b7 45 d6          	movzwl -0x2a(%ebp),%eax
 4a4:	80 cc 0c             	or     $0xc,%ah
 4a7:	66 89 45 d4          	mov    %ax,-0x2c(%ebp)
  printint(fd, beg, 10, 1);
 4ab:	89 f0                	mov    %esi,%eax
  int beg=(int)(xx);
 4ad:	d9 6d d4             	fldcw  -0x2c(%ebp)
 4b0:	db 55 d0             	fistl  -0x30(%ebp)
 4b3:	d9 6d d6             	fldcw  -0x2a(%ebp)
 4b6:	8b 55 d0             	mov    -0x30(%ebp),%edx
	int fin=(int)(xx*100)-beg*100;
 4b9:	d8 0d c4 09 00 00    	fmuls  0x9c4
 4bf:	d9 6d d4             	fldcw  -0x2c(%ebp)
 4c2:	db 5d d0             	fistpl -0x30(%ebp)
 4c5:	d9 6d d6             	fldcw  -0x2a(%ebp)
 4c8:	6b da 9c             	imul   $0xffffff9c,%edx,%ebx
 4cb:	03 5d d0             	add    -0x30(%ebp),%ebx
  printint(fd, beg, 10, 1);
 4ce:	e8 fd fe ff ff       	call   3d0 <printint>
  write(fd, &c, 1);
 4d3:	83 c4 0c             	add    $0xc,%esp
 4d6:	c6 45 e7 2e          	movb   $0x2e,-0x19(%ebp)
 4da:	6a 01                	push   $0x1
 4dc:	57                   	push   %edi
 4dd:	56                   	push   %esi
 4de:	e8 40 fe ff ff       	call   323 <write>
  putc(fd, '.');
	if(fin<10)
 4e3:	83 c4 10             	add    $0x10,%esp
 4e6:	83 fb 09             	cmp    $0x9,%ebx
 4e9:	7e 25                	jle    510 <printfloat+0x90>
    putc(fd, '0');
	printint(fd, fin, 10, 1);
 4eb:	c7 45 08 01 00 00 00 	movl   $0x1,0x8(%ebp)
}
 4f2:	8d 65 f4             	lea    -0xc(%ebp),%esp
	printint(fd, fin, 10, 1);
 4f5:	89 da                	mov    %ebx,%edx
 4f7:	89 f0                	mov    %esi,%eax
}
 4f9:	5b                   	pop    %ebx
	printint(fd, fin, 10, 1);
 4fa:	b9 0a 00 00 00       	mov    $0xa,%ecx
}
 4ff:	5e                   	pop    %esi
 500:	5f                   	pop    %edi
 501:	5d                   	pop    %ebp
	printint(fd, fin, 10, 1);
 502:	e9 c9 fe ff ff       	jmp    3d0 <printint>
 507:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 50e:	66 90                	xchg   %ax,%ax
  write(fd, &c, 1);
 510:	83 ec 04             	sub    $0x4,%esp
 513:	c6 45 e7 30          	movb   $0x30,-0x19(%ebp)
 517:	6a 01                	push   $0x1
 519:	57                   	push   %edi
 51a:	56                   	push   %esi
 51b:	e8 03 fe ff ff       	call   323 <write>
    putc(fd, '0');
 520:	83 c4 10             	add    $0x10,%esp
 523:	eb c6                	jmp    4eb <printfloat+0x6b>
 525:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 52c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000530 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s, %f.
void
printf(int fd, const char *fmt, ...)
{
 530:	f3 0f 1e fb          	endbr32 
 534:	55                   	push   %ebp
 535:	89 e5                	mov    %esp,%ebp
 537:	57                   	push   %edi
 538:	56                   	push   %esi
 539:	53                   	push   %ebx
 53a:	83 ec 2c             	sub    $0x2c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 53d:	8b 75 0c             	mov    0xc(%ebp),%esi
 540:	0f b6 1e             	movzbl (%esi),%ebx
 543:	84 db                	test   %bl,%bl
 545:	0f 84 bd 00 00 00    	je     608 <printf+0xd8>
  ap = (uint*)(void*)&fmt + 1;
 54b:	8d 45 10             	lea    0x10(%ebp),%eax
 54e:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 551:	8d 7d e7             	lea    -0x19(%ebp),%edi
  state = 0;
 554:	31 d2                	xor    %edx,%edx
  ap = (uint*)(void*)&fmt + 1;
 556:	89 45 cc             	mov    %eax,-0x34(%ebp)
 559:	eb 33                	jmp    58e <printf+0x5e>
 55b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 55f:	90                   	nop
 560:	89 55 d0             	mov    %edx,-0x30(%ebp)
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
 563:	ba 25 00 00 00       	mov    $0x25,%edx
      if(c == '%'){
 568:	83 f8 25             	cmp    $0x25,%eax
 56b:	74 17                	je     584 <printf+0x54>
  write(fd, &c, 1);
 56d:	83 ec 04             	sub    $0x4,%esp
 570:	88 5d e7             	mov    %bl,-0x19(%ebp)
 573:	6a 01                	push   $0x1
 575:	57                   	push   %edi
 576:	ff 75 08             	push   0x8(%ebp)
 579:	e8 a5 fd ff ff       	call   323 <write>
 57e:	8b 55 d0             	mov    -0x30(%ebp),%edx
      } else {
        putc(fd, c);
 581:	83 c4 10             	add    $0x10,%esp
  for(i = 0; fmt[i]; i++){
 584:	0f b6 1e             	movzbl (%esi),%ebx
 587:	83 c6 01             	add    $0x1,%esi
 58a:	84 db                	test   %bl,%bl
 58c:	74 7a                	je     608 <printf+0xd8>
    c = fmt[i] & 0xff;
 58e:	0f be cb             	movsbl %bl,%ecx
 591:	0f b6 c3             	movzbl %bl,%eax
    if(state == 0){
 594:	85 d2                	test   %edx,%edx
 596:	74 c8                	je     560 <printf+0x30>
      }
    } else if(state == '%'){
 598:	83 fa 25             	cmp    $0x25,%edx
 59b:	75 e7                	jne    584 <printf+0x54>
      if(c == 'd'){
 59d:	83 f8 64             	cmp    $0x64,%eax
 5a0:	0f 84 9a 00 00 00    	je     640 <printf+0x110>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 5a6:	81 e1 f7 00 00 00    	and    $0xf7,%ecx
 5ac:	83 f9 70             	cmp    $0x70,%ecx
 5af:	74 5f                	je     610 <printf+0xe0>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 5b1:	83 f8 73             	cmp    $0x73,%eax
 5b4:	0f 84 d6 00 00 00    	je     690 <printf+0x160>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 5ba:	83 f8 63             	cmp    $0x63,%eax
 5bd:	0f 84 8d 00 00 00    	je     650 <printf+0x120>
        putc(fd, *ap);
        ap++;
      } else if(c == 'f'){ // MOD-2
 5c3:	83 f8 66             	cmp    $0x66,%eax
 5c6:	0f 84 f4 00 00 00    	je     6c0 <printf+0x190>
        printfloat(fd, (float)*ap);
        ap++;
      } else if(c == '%'){
 5cc:	83 f8 25             	cmp    $0x25,%eax
 5cf:	0f 84 ab 00 00 00    	je     680 <printf+0x150>
  write(fd, &c, 1);
 5d5:	83 ec 04             	sub    $0x4,%esp
 5d8:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
 5dc:	6a 01                	push   $0x1
 5de:	57                   	push   %edi
 5df:	ff 75 08             	push   0x8(%ebp)
 5e2:	e8 3c fd ff ff       	call   323 <write>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
        putc(fd, c);
 5e7:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 5ea:	83 c4 0c             	add    $0xc,%esp
 5ed:	6a 01                	push   $0x1
  for(i = 0; fmt[i]; i++){
 5ef:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 5f2:	57                   	push   %edi
 5f3:	ff 75 08             	push   0x8(%ebp)
 5f6:	e8 28 fd ff ff       	call   323 <write>
  for(i = 0; fmt[i]; i++){
 5fb:	0f b6 5e ff          	movzbl -0x1(%esi),%ebx
        putc(fd, c);
 5ff:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 602:	31 d2                	xor    %edx,%edx
  for(i = 0; fmt[i]; i++){
 604:	84 db                	test   %bl,%bl
 606:	75 86                	jne    58e <printf+0x5e>
    }
  }
 608:	8d 65 f4             	lea    -0xc(%ebp),%esp
 60b:	5b                   	pop    %ebx
 60c:	5e                   	pop    %esi
 60d:	5f                   	pop    %edi
 60e:	5d                   	pop    %ebp
 60f:	c3                   	ret    
        printint(fd, *ap, 16, 0);
 610:	83 ec 0c             	sub    $0xc,%esp
 613:	b9 10 00 00 00       	mov    $0x10,%ecx
 618:	6a 00                	push   $0x0
 61a:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 61d:	8b 45 08             	mov    0x8(%ebp),%eax
 620:	8b 13                	mov    (%ebx),%edx
 622:	e8 a9 fd ff ff       	call   3d0 <printint>
        ap++;
 627:	89 d8                	mov    %ebx,%eax
 629:	83 c4 10             	add    $0x10,%esp
      state = 0;
 62c:	31 d2                	xor    %edx,%edx
        ap++;
 62e:	83 c0 04             	add    $0x4,%eax
 631:	89 45 cc             	mov    %eax,-0x34(%ebp)
 634:	e9 4b ff ff ff       	jmp    584 <printf+0x54>
 639:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        printint(fd, *ap, 10, 1);
 640:	83 ec 0c             	sub    $0xc,%esp
 643:	b9 0a 00 00 00       	mov    $0xa,%ecx
 648:	6a 01                	push   $0x1
 64a:	eb ce                	jmp    61a <printf+0xea>
 64c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        putc(fd, *ap);
 650:	8b 5d cc             	mov    -0x34(%ebp),%ebx
  write(fd, &c, 1);
 653:	83 ec 04             	sub    $0x4,%esp
        putc(fd, *ap);
 656:	8b 03                	mov    (%ebx),%eax
  write(fd, &c, 1);
 658:	6a 01                	push   $0x1
        ap++;
 65a:	83 c3 04             	add    $0x4,%ebx
  write(fd, &c, 1);
 65d:	57                   	push   %edi
 65e:	ff 75 08             	push   0x8(%ebp)
        putc(fd, *ap);
 661:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 664:	e8 ba fc ff ff       	call   323 <write>
        ap++;
 669:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 66c:	83 c4 10             	add    $0x10,%esp
      state = 0;
 66f:	31 d2                	xor    %edx,%edx
 671:	e9 0e ff ff ff       	jmp    584 <printf+0x54>
 676:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 67d:	8d 76 00             	lea    0x0(%esi),%esi
        putc(fd, c);
 680:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 683:	83 ec 04             	sub    $0x4,%esp
 686:	e9 62 ff ff ff       	jmp    5ed <printf+0xbd>
 68b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 68f:	90                   	nop
        s = (char*)*ap;
 690:	8b 45 cc             	mov    -0x34(%ebp),%eax
 693:	8b 18                	mov    (%eax),%ebx
        ap++;
 695:	83 c0 04             	add    $0x4,%eax
 698:	89 45 cc             	mov    %eax,-0x34(%ebp)
        if(s == 0)
 69b:	85 db                	test   %ebx,%ebx
 69d:	74 4f                	je     6ee <printf+0x1be>
        while(*s != 0){
 69f:	0f b6 03             	movzbl (%ebx),%eax
      state = 0;
 6a2:	31 d2                	xor    %edx,%edx
        while(*s != 0){
 6a4:	84 c0                	test   %al,%al
 6a6:	0f 84 d8 fe ff ff    	je     584 <printf+0x54>
 6ac:	89 75 d0             	mov    %esi,-0x30(%ebp)
 6af:	89 de                	mov    %ebx,%esi
 6b1:	8b 5d 08             	mov    0x8(%ebp),%ebx
 6b4:	eb 4a                	jmp    700 <printf+0x1d0>
 6b6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 6bd:	8d 76 00             	lea    0x0(%esi),%esi
        printfloat(fd, (float)*ap);
 6c0:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 6c3:	31 d2                	xor    %edx,%edx
 6c5:	83 ec 0c             	sub    $0xc,%esp
 6c8:	89 55 d4             	mov    %edx,-0x2c(%ebp)
 6cb:	8b 03                	mov    (%ebx),%eax
        ap++;
 6cd:	83 c3 04             	add    $0x4,%ebx
        printfloat(fd, (float)*ap);
 6d0:	89 45 d0             	mov    %eax,-0x30(%ebp)
 6d3:	df 6d d0             	fildll -0x30(%ebp)
 6d6:	d9 1c 24             	fstps  (%esp)
 6d9:	ff 75 08             	push   0x8(%ebp)
 6dc:	e8 9f fd ff ff       	call   480 <printfloat>
        ap++;
 6e1:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 6e4:	83 c4 10             	add    $0x10,%esp
      state = 0;
 6e7:	31 d2                	xor    %edx,%edx
 6e9:	e9 96 fe ff ff       	jmp    584 <printf+0x54>
          s = "(null)";
 6ee:	bb a6 09 00 00       	mov    $0x9a6,%ebx
        while(*s != 0){
 6f3:	89 75 d0             	mov    %esi,-0x30(%ebp)
 6f6:	b8 28 00 00 00       	mov    $0x28,%eax
 6fb:	89 de                	mov    %ebx,%esi
 6fd:	8b 5d 08             	mov    0x8(%ebp),%ebx
  write(fd, &c, 1);
 700:	83 ec 04             	sub    $0x4,%esp
          s++;
 703:	83 c6 01             	add    $0x1,%esi
 706:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 709:	6a 01                	push   $0x1
 70b:	57                   	push   %edi
 70c:	53                   	push   %ebx
 70d:	e8 11 fc ff ff       	call   323 <write>
        while(*s != 0){
 712:	0f b6 06             	movzbl (%esi),%eax
 715:	83 c4 10             	add    $0x10,%esp
 718:	84 c0                	test   %al,%al
 71a:	75 e4                	jne    700 <printf+0x1d0>
      state = 0;
 71c:	8b 75 d0             	mov    -0x30(%ebp),%esi
 71f:	31 d2                	xor    %edx,%edx
 721:	e9 5e fe ff ff       	jmp    584 <printf+0x54>
 726:	66 90                	xchg   %ax,%ax
 728:	66 90                	xchg   %ax,%ax
 72a:	66 90                	xchg   %ax,%ax
 72c:	66 90                	xchg   %ax,%ax
 72e:	66 90                	xchg   %ax,%ax

00000730 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 730:	f3 0f 1e fb          	endbr32 
 734:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 735:	a1 8c 0c 00 00       	mov    0xc8c,%eax
{
 73a:	89 e5                	mov    %esp,%ebp
 73c:	57                   	push   %edi
 73d:	56                   	push   %esi
 73e:	53                   	push   %ebx
 73f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = (Header*)ap - 1;
 742:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 745:	8d 76 00             	lea    0x0(%esi),%esi
 748:	89 c2                	mov    %eax,%edx
 74a:	8b 00                	mov    (%eax),%eax
 74c:	39 ca                	cmp    %ecx,%edx
 74e:	73 30                	jae    780 <free+0x50>
 750:	39 c1                	cmp    %eax,%ecx
 752:	72 04                	jb     758 <free+0x28>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 754:	39 c2                	cmp    %eax,%edx
 756:	72 f0                	jb     748 <free+0x18>
      break;
  if(bp + bp->s.size == p->s.ptr){
 758:	8b 73 fc             	mov    -0x4(%ebx),%esi
 75b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 75e:	39 f8                	cmp    %edi,%eax
 760:	74 30                	je     792 <free+0x62>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 762:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 765:	8b 42 04             	mov    0x4(%edx),%eax
 768:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 76b:	39 f1                	cmp    %esi,%ecx
 76d:	74 3a                	je     7a9 <free+0x79>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 76f:	89 0a                	mov    %ecx,(%edx)
  freep = p;
}
 771:	5b                   	pop    %ebx
  freep = p;
 772:	89 15 8c 0c 00 00    	mov    %edx,0xc8c
}
 778:	5e                   	pop    %esi
 779:	5f                   	pop    %edi
 77a:	5d                   	pop    %ebp
 77b:	c3                   	ret    
 77c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 780:	39 c2                	cmp    %eax,%edx
 782:	72 c4                	jb     748 <free+0x18>
 784:	39 c1                	cmp    %eax,%ecx
 786:	73 c0                	jae    748 <free+0x18>
  if(bp + bp->s.size == p->s.ptr){
 788:	8b 73 fc             	mov    -0x4(%ebx),%esi
 78b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 78e:	39 f8                	cmp    %edi,%eax
 790:	75 d0                	jne    762 <free+0x32>
    bp->s.size += p->s.ptr->s.size;
 792:	03 70 04             	add    0x4(%eax),%esi
 795:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 798:	8b 02                	mov    (%edx),%eax
 79a:	8b 00                	mov    (%eax),%eax
 79c:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 79f:	8b 42 04             	mov    0x4(%edx),%eax
 7a2:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 7a5:	39 f1                	cmp    %esi,%ecx
 7a7:	75 c6                	jne    76f <free+0x3f>
    p->s.size += bp->s.size;
 7a9:	03 43 fc             	add    -0x4(%ebx),%eax
  freep = p;
 7ac:	89 15 8c 0c 00 00    	mov    %edx,0xc8c
    p->s.size += bp->s.size;
 7b2:	89 42 04             	mov    %eax,0x4(%edx)
    p->s.ptr = bp->s.ptr;
 7b5:	8b 43 f8             	mov    -0x8(%ebx),%eax
 7b8:	89 02                	mov    %eax,(%edx)
}
 7ba:	5b                   	pop    %ebx
 7bb:	5e                   	pop    %esi
 7bc:	5f                   	pop    %edi
 7bd:	5d                   	pop    %ebp
 7be:	c3                   	ret    
 7bf:	90                   	nop

000007c0 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
 7c0:	f3 0f 1e fb          	endbr32 
 7c4:	55                   	push   %ebp
 7c5:	89 e5                	mov    %esp,%ebp
 7c7:	57                   	push   %edi
 7c8:	56                   	push   %esi
 7c9:	53                   	push   %ebx
 7ca:	83 ec 1c             	sub    $0x1c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7cd:	8b 45 08             	mov    0x8(%ebp),%eax
  if((prevp = freep) == 0){
 7d0:	8b 3d 8c 0c 00 00    	mov    0xc8c,%edi
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7d6:	8d 70 07             	lea    0x7(%eax),%esi
 7d9:	c1 ee 03             	shr    $0x3,%esi
 7dc:	83 c6 01             	add    $0x1,%esi
  if((prevp = freep) == 0){
 7df:	85 ff                	test   %edi,%edi
 7e1:	0f 84 a9 00 00 00    	je     890 <malloc+0xd0>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7e7:	8b 07                	mov    (%edi),%eax
    if(p->s.size >= nunits){
 7e9:	8b 48 04             	mov    0x4(%eax),%ecx
 7ec:	39 f1                	cmp    %esi,%ecx
 7ee:	73 6d                	jae    85d <malloc+0x9d>
 7f0:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
 7f6:	bb 00 10 00 00       	mov    $0x1000,%ebx
 7fb:	0f 43 de             	cmovae %esi,%ebx
  p = sbrk(nu * sizeof(Header));
 7fe:	8d 0c dd 00 00 00 00 	lea    0x0(,%ebx,8),%ecx
 805:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
 808:	eb 17                	jmp    821 <malloc+0x61>
 80a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 810:	8b 10                	mov    (%eax),%edx
    if(p->s.size >= nunits){
 812:	8b 4a 04             	mov    0x4(%edx),%ecx
 815:	39 f1                	cmp    %esi,%ecx
 817:	73 4f                	jae    868 <malloc+0xa8>
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
 819:	8b 3d 8c 0c 00 00    	mov    0xc8c,%edi
 81f:	89 d0                	mov    %edx,%eax
 821:	39 c7                	cmp    %eax,%edi
 823:	75 eb                	jne    810 <malloc+0x50>
  p = sbrk(nu * sizeof(Header));
 825:	83 ec 0c             	sub    $0xc,%esp
 828:	ff 75 e4             	push   -0x1c(%ebp)
 82b:	e8 5b fb ff ff       	call   38b <sbrk>
  if(p == (char*)-1)
 830:	83 c4 10             	add    $0x10,%esp
 833:	83 f8 ff             	cmp    $0xffffffff,%eax
 836:	74 1b                	je     853 <malloc+0x93>
  hp->s.size = nu;
 838:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 83b:	83 ec 0c             	sub    $0xc,%esp
 83e:	83 c0 08             	add    $0x8,%eax
 841:	50                   	push   %eax
 842:	e8 e9 fe ff ff       	call   730 <free>
  return freep;
 847:	a1 8c 0c 00 00       	mov    0xc8c,%eax
      if((p = morecore(nunits)) == 0)
 84c:	83 c4 10             	add    $0x10,%esp
 84f:	85 c0                	test   %eax,%eax
 851:	75 bd                	jne    810 <malloc+0x50>
        return 0;
  }
}
 853:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return 0;
 856:	31 c0                	xor    %eax,%eax
}
 858:	5b                   	pop    %ebx
 859:	5e                   	pop    %esi
 85a:	5f                   	pop    %edi
 85b:	5d                   	pop    %ebp
 85c:	c3                   	ret    
    if(p->s.size >= nunits){
 85d:	89 c2                	mov    %eax,%edx
 85f:	89 f8                	mov    %edi,%eax
 861:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->s.size == nunits)
 868:	39 ce                	cmp    %ecx,%esi
 86a:	74 54                	je     8c0 <malloc+0x100>
        p->s.size -= nunits;
 86c:	29 f1                	sub    %esi,%ecx
 86e:	89 4a 04             	mov    %ecx,0x4(%edx)
        p += p->s.size;
 871:	8d 14 ca             	lea    (%edx,%ecx,8),%edx
        p->s.size = nunits;
 874:	89 72 04             	mov    %esi,0x4(%edx)
      freep = prevp;
 877:	a3 8c 0c 00 00       	mov    %eax,0xc8c
}
 87c:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return (void*)(p + 1);
 87f:	8d 42 08             	lea    0x8(%edx),%eax
}
 882:	5b                   	pop    %ebx
 883:	5e                   	pop    %esi
 884:	5f                   	pop    %edi
 885:	5d                   	pop    %ebp
 886:	c3                   	ret    
 887:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 88e:	66 90                	xchg   %ax,%ax
    base.s.ptr = freep = prevp = &base;
 890:	c7 05 8c 0c 00 00 90 	movl   $0xc90,0xc8c
 897:	0c 00 00 
    base.s.size = 0;
 89a:	bf 90 0c 00 00       	mov    $0xc90,%edi
    base.s.ptr = freep = prevp = &base;
 89f:	c7 05 90 0c 00 00 90 	movl   $0xc90,0xc90
 8a6:	0c 00 00 
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8a9:	89 f8                	mov    %edi,%eax
    base.s.size = 0;
 8ab:	c7 05 94 0c 00 00 00 	movl   $0x0,0xc94
 8b2:	00 00 00 
    if(p->s.size >= nunits){
 8b5:	e9 36 ff ff ff       	jmp    7f0 <malloc+0x30>
 8ba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        prevp->s.ptr = p->s.ptr;
 8c0:	8b 0a                	mov    (%edx),%ecx
 8c2:	89 08                	mov    %ecx,(%eax)
 8c4:	eb b1                	jmp    877 <malloc+0xb7>
