#include "types.h"
#include "user.h"
#include "stat.h"

int main(int argc, char* argv[]) {
    int s = 10;
    int pid = getpid();
    int saved_ebx;
    printf(1, "program: pids\n");
    asm volatile(
        "movl %%ebx, %0;" // saved_ebx = ebx
        "movl %1, %%ebx;" // ebx = number
        : "=r" (saved_ebx)
        : "r"(pid)
        );
    get_parent_pid(pid);
    asm("movl %0, %%ebx" : : "r"(saved_ebx));
    printf(1, "program: falling asleep\n");
    sleep(1000);
    printf(1, "program: I woke up\n");
    asm volatile(
        "movl %%ebx, %0;" // saved_ebx = ebx
        "movl %1, %%ebx;" // ebx = number
        : "=r" (saved_ebx)
        : "r"(pid)
        );
    get_parent_pid(pid);
    asm("movl %0, %%ebx" : : "r"(saved_ebx));
    sleep(s*100);
    printf(1, "program: %d second I fell asleep and now time to exit\n", s);
    exit();
}