#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define FILE_ADDR_SIZE 512
int main(int argc, char *argv[]) {
    char addr[FILE_ADDR_SIZE];
    int addr_sectrs[FILE_ADDR_SIZE];
    int fd;
    int resp;
    if(argc < 2) {
        resp = read(0, addr, FILE_ADDR_SIZE);
        if(resp < 0) {
            printf(1, "file address recieve failed!\n");
            exit();
        }
    } else {
        strcpy(addr, argv[1]);
    }
    fd = open(addr, O_RDONLY);
    if(fd < 0) {
        printf(1, "openning file failed!\n");
        exit();
    }
    get_file_sectors(fd, addr_sectrs, FILE_ADDR_SIZE);
    resp = 0;
    printf(1, "file sectors sequence will come in following:\n");
    while (addr_sectrs[resp] >= 0) {
        printf(1, "%d ", addr_sectrs[resp++]);
    }
    printf(1, "\n");
    exit();
}