
_factor:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
        i/=10;
    }
    return;
}
int
main(int argc, char *argv[]){
   0:	f3 0f 1e fb          	endbr32 
   4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   8:	83 e4 f0             	and    $0xfffffff0,%esp
   b:	ff 71 fc             	push   -0x4(%ecx)
   e:	55                   	push   %ebp
   f:	89 e5                	mov    %esp,%ebp
  11:	57                   	push   %edi
  12:	56                   	push   %esi
  13:	53                   	push   %ebx
  14:	51                   	push   %ecx
  15:	83 ec 28             	sub    $0x28,%esp
    if(argc<=1 || argc>2){
  18:	83 39 02             	cmpl   $0x2,(%ecx)
main(int argc, char *argv[]){
  1b:	8b 41 04             	mov    0x4(%ecx),%eax
    if(argc<=1 || argc>2){
  1e:	0f 85 f3 00 00 00    	jne    117 <main+0x117>
        printf(1, "argument count invalid\n");
        exit();
    }
    int a = num(argv[1]);
  24:	8b 50 04             	mov    0x4(%eax),%edx
    while(a[i]!='\0'){
  27:	0f be 02             	movsbl (%edx),%eax
  2a:	84 c0                	test   %al,%al
  2c:	0f 84 9e 01 00 00    	je     1d0 <main+0x1d0>
  32:	83 c2 01             	add    $0x1,%edx
    int r=0;
  35:	31 ff                	xor    %edi,%edi
  37:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  3e:	66 90                	xchg   %ax,%ax
        r*=10;
  40:	8d 0c bf             	lea    (%edi,%edi,4),%ecx
    while(a[i]!='\0'){
  43:	83 c2 01             	add    $0x1,%edx
        r+=a[i]-48;
  46:	8d 7c 48 d0          	lea    -0x30(%eax,%ecx,2),%edi
    while(a[i]!='\0'){
  4a:	0f be 42 ff          	movsbl -0x1(%edx),%eax
  4e:	84 c0                	test   %al,%al
  50:	75 ee                	jne    40 <main+0x40>
    int file;
    if(open("factor_result.txt", O_RDONLY) != -1){
  52:	50                   	push   %eax
  53:	50                   	push   %eax
  54:	6a 00                	push   $0x0
  56:	68 60 0b 00 00       	push   $0xb60
  5b:	e8 63 05 00 00       	call   5c3 <open>
  60:	83 c4 10             	add    $0x10,%esp
  63:	83 c0 01             	add    $0x1,%eax
  66:	74 10                	je     78 <main+0x78>
    	unlink("factor_result.txt");
  68:	83 ec 0c             	sub    $0xc,%esp
  6b:	68 60 0b 00 00       	push   $0xb60
  70:	e8 5e 05 00 00       	call   5d3 <unlink>
  75:	83 c4 10             	add    $0x10,%esp
    }
    file = open("factor_result.txt", O_CREATE | O_WRONLY );
  78:	56                   	push   %esi
  79:	56                   	push   %esi
  7a:	68 01 02 00 00       	push   $0x201
  7f:	68 60 0b 00 00       	push   $0xb60
  84:	e8 3a 05 00 00       	call   5c3 <open>
    if(file == -1){
  89:	83 c4 10             	add    $0x10,%esp
    file = open("factor_result.txt", O_CREATE | O_WRONLY );
  8c:	89 45 d0             	mov    %eax,-0x30(%ebp)
    if(file == -1){
  8f:	83 c0 01             	add    $0x1,%eax
  92:	0f 84 3f 01 00 00    	je     1d7 <main+0x1d7>
        printf(1, "failed in creat and open file!\n");
        exit();
    }
    for(int i=1; i<=a; i++){
  98:	be 01 00 00 00       	mov    $0x1,%esi
  9d:	85 ff                	test   %edi,%edi
  9f:	0f 8e 85 00 00 00    	jle    12a <main+0x12a>
  a5:	89 7d d4             	mov    %edi,-0x2c(%ebp)
  a8:	eb 0e                	jmp    b8 <main+0xb8>
  aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  b0:	83 c6 01             	add    $0x1,%esi
  b3:	3b 75 d4             	cmp    -0x2c(%ebp),%esi
  b6:	7f 72                	jg     12a <main+0x12a>
        if(a%i==0){
  b8:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  bb:	99                   	cltd   
  bc:	f7 fe                	idiv   %esi
  be:	85 d2                	test   %edx,%edx
  c0:	75 ee                	jne    b0 <main+0xb0>
  c2:	89 65 cc             	mov    %esp,-0x34(%ebp)
    while(a/10>0){
  c5:	83 fe 09             	cmp    $0x9,%esi
  c8:	0f 8e 1c 01 00 00    	jle    1ea <main+0x1ea>
  ce:	89 f2                	mov    %esi,%edx
    int r=1;
  d0:	bb 01 00 00 00       	mov    $0x1,%ebx
        a /= 10;
  d5:	bf cd cc cc cc       	mov    $0xcccccccd,%edi
  da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  e0:	89 d0                	mov    %edx,%eax
  e2:	89 d1                	mov    %edx,%ecx
        r++;
  e4:	83 c3 01             	add    $0x1,%ebx
        a /= 10;
  e7:	f7 e7                	mul    %edi
  e9:	c1 ea 03             	shr    $0x3,%edx
    while(a/10>0){
  ec:	83 f9 63             	cmp    $0x63,%ecx
  ef:	7f ef                	jg     e0 <main+0xe0>
            int k = digit(i);
            char p[k];
  f1:	83 c3 0f             	add    $0xf,%ebx
  f4:	89 e2                	mov    %esp,%edx
  f6:	89 d8                	mov    %ebx,%eax
  f8:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  fe:	83 e0 f0             	and    $0xfffffff0,%eax
 101:	29 da                	sub    %ebx,%edx
 103:	39 d4                	cmp    %edx,%esp
 105:	74 5a                	je     161 <main+0x161>
 107:	81 ec 00 10 00 00    	sub    $0x1000,%esp
 10d:	83 8c 24 fc 0f 00 00 	orl    $0x0,0xffc(%esp)
 114:	00 
 115:	eb ec                	jmp    103 <main+0x103>
        printf(1, "argument count invalid\n");
 117:	50                   	push   %eax
 118:	50                   	push   %eax
 119:	68 48 0b 00 00       	push   $0xb48
 11e:	6a 01                	push   $0x1
 120:	e8 8b 06 00 00       	call   7b0 <printf>
        exit();
 125:	e8 59 04 00 00       	call   583 <exit>
            charnum(p, i);
            write(file, p, digit(i));
            write(file, " ", 1);
        }
    }
    write(file, "\n", 1);
 12a:	53                   	push   %ebx
 12b:	6a 01                	push   $0x1
 12d:	68 5e 0b 00 00       	push   $0xb5e
 132:	8b 75 d0             	mov    -0x30(%ebp),%esi
 135:	56                   	push   %esi
 136:	e8 68 04 00 00       	call   5a3 <write>
    close(file);
 13b:	89 34 24             	mov    %esi,(%esp)
 13e:	e8 68 04 00 00       	call   5ab <close>
    printf(1, "%f\n", 128.512f);
 143:	68 62 10 60 40       	push   $0x40601062
 148:	68 00 00 00 40       	push   $0x40000000
 14d:	68 72 0b 00 00       	push   $0xb72
 152:	6a 01                	push   $0x1
 154:	e8 57 06 00 00       	call   7b0 <printf>
    exit();
 159:	83 c4 20             	add    $0x20,%esp
 15c:	e8 22 04 00 00       	call   583 <exit>
            char p[k];
 161:	25 ff 0f 00 00       	and    $0xfff,%eax
 166:	29 c4                	sub    %eax,%esp
 168:	85 c0                	test   %eax,%eax
 16a:	75 5d                	jne    1c9 <main+0x1c9>
 16c:	89 e0                	mov    %esp,%eax
 16e:	89 65 c8             	mov    %esp,-0x38(%ebp)
            charnum(p, i);
 171:	bb 01 00 00 00       	mov    $0x1,%ebx
        a /= 10;
 176:	bf cd cc cc cc       	mov    $0xcccccccd,%edi
            charnum(p, i);
 17b:	52                   	push   %edx
 17c:	52                   	push   %edx
 17d:	56                   	push   %esi
 17e:	50                   	push   %eax
 17f:	e8 1c 01 00 00       	call   2a0 <charnum>
 184:	83 c4 10             	add    $0x10,%esp
 187:	89 f2                	mov    %esi,%edx
 189:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        a /= 10;
 190:	89 d0                	mov    %edx,%eax
 192:	89 d1                	mov    %edx,%ecx
        r++;
 194:	83 c3 01             	add    $0x1,%ebx
        a /= 10;
 197:	f7 e7                	mul    %edi
 199:	c1 ea 03             	shr    $0x3,%edx
    while(a/10>0){
 19c:	83 f9 63             	cmp    $0x63,%ecx
 19f:	7f ef                	jg     190 <main+0x190>
            write(file, p, digit(i));
 1a1:	83 ec 04             	sub    $0x4,%esp
 1a4:	53                   	push   %ebx
 1a5:	ff 75 c8             	push   -0x38(%ebp)
 1a8:	8b 5d d0             	mov    -0x30(%ebp),%ebx
 1ab:	53                   	push   %ebx
 1ac:	e8 f2 03 00 00       	call   5a3 <write>
            write(file, " ", 1);
 1b1:	83 c4 0c             	add    $0xc,%esp
 1b4:	6a 01                	push   $0x1
 1b6:	68 76 0b 00 00       	push   $0xb76
 1bb:	53                   	push   %ebx
 1bc:	e8 e2 03 00 00       	call   5a3 <write>
 1c1:	8b 65 cc             	mov    -0x34(%ebp),%esp
 1c4:	e9 e7 fe ff ff       	jmp    b0 <main+0xb0>
            char p[k];
 1c9:	83 4c 04 fc 00       	orl    $0x0,-0x4(%esp,%eax,1)
 1ce:	eb 9c                	jmp    16c <main+0x16c>
    int r=0;
 1d0:	31 ff                	xor    %edi,%edi
 1d2:	e9 7b fe ff ff       	jmp    52 <main+0x52>
        printf(1, "failed in creat and open file!\n");
 1d7:	51                   	push   %ecx
 1d8:	51                   	push   %ecx
 1d9:	68 78 0b 00 00       	push   $0xb78
 1de:	6a 01                	push   $0x1
 1e0:	e8 cb 05 00 00       	call   7b0 <printf>
        exit();
 1e5:	e8 99 03 00 00       	call   583 <exit>
            charnum(p, i);
 1ea:	50                   	push   %eax
    int r=1;
 1eb:	bb 01 00 00 00       	mov    $0x1,%ebx
            charnum(p, i);
 1f0:	50                   	push   %eax
 1f1:	8d 45 e7             	lea    -0x19(%ebp),%eax
 1f4:	56                   	push   %esi
 1f5:	50                   	push   %eax
 1f6:	e8 a5 00 00 00       	call   2a0 <charnum>
            char p[k];
 1fb:	8d 45 e7             	lea    -0x19(%ebp),%eax
            charnum(p, i);
 1fe:	83 c4 10             	add    $0x10,%esp
            char p[k];
 201:	89 45 c8             	mov    %eax,-0x38(%ebp)
 204:	eb 9b                	jmp    1a1 <main+0x1a1>
 206:	66 90                	xchg   %ax,%ax
 208:	66 90                	xchg   %ax,%ax
 20a:	66 90                	xchg   %ax,%ax
 20c:	66 90                	xchg   %ax,%ax
 20e:	66 90                	xchg   %ax,%ax

00000210 <num>:
num(char a[]){
 210:	f3 0f 1e fb          	endbr32 
 214:	55                   	push   %ebp
 215:	89 e5                	mov    %esp,%ebp
 217:	8b 55 08             	mov    0x8(%ebp),%edx
    while(a[i]!='\0'){
 21a:	0f be 02             	movsbl (%edx),%eax
 21d:	84 c0                	test   %al,%al
 21f:	74 2f                	je     250 <num+0x40>
 221:	83 c2 01             	add    $0x1,%edx
    int r=0;
 224:	31 c9                	xor    %ecx,%ecx
 226:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 22d:	8d 76 00             	lea    0x0(%esi),%esi
        r*=10;
 230:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
    while(a[i]!='\0'){
 233:	83 c2 01             	add    $0x1,%edx
        r+=a[i]-48;
 236:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
    while(a[i]!='\0'){
 23a:	0f be 42 ff          	movsbl -0x1(%edx),%eax
 23e:	84 c0                	test   %al,%al
 240:	75 ee                	jne    230 <num+0x20>
}
 242:	89 c8                	mov    %ecx,%eax
 244:	5d                   	pop    %ebp
 245:	c3                   	ret    
 246:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 24d:	8d 76 00             	lea    0x0(%esi),%esi
    int r=0;
 250:	31 c9                	xor    %ecx,%ecx
}
 252:	5d                   	pop    %ebp
 253:	89 c8                	mov    %ecx,%eax
 255:	c3                   	ret    
 256:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 25d:	8d 76 00             	lea    0x0(%esi),%esi

00000260 <digit>:
digit(int a){
 260:	f3 0f 1e fb          	endbr32 
 264:	55                   	push   %ebp
 265:	89 e5                	mov    %esp,%ebp
 267:	56                   	push   %esi
 268:	8b 55 08             	mov    0x8(%ebp),%edx
 26b:	53                   	push   %ebx
    int r=1;
 26c:	bb 01 00 00 00       	mov    $0x1,%ebx
    while(a/10>0){
 271:	83 fa 09             	cmp    $0x9,%edx
 274:	7e 1b                	jle    291 <digit+0x31>
        a /= 10;
 276:	be cd cc cc cc       	mov    $0xcccccccd,%esi
 27b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 27f:	90                   	nop
 280:	89 d0                	mov    %edx,%eax
 282:	89 d1                	mov    %edx,%ecx
        r++;
 284:	83 c3 01             	add    $0x1,%ebx
        a /= 10;
 287:	f7 e6                	mul    %esi
 289:	c1 ea 03             	shr    $0x3,%edx
    while(a/10>0){
 28c:	83 f9 63             	cmp    $0x63,%ecx
 28f:	7f ef                	jg     280 <digit+0x20>
}
 291:	89 d8                	mov    %ebx,%eax
 293:	5b                   	pop    %ebx
 294:	5e                   	pop    %esi
 295:	5d                   	pop    %ebp
 296:	c3                   	ret    
 297:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 29e:	66 90                	xchg   %ax,%ax

000002a0 <charnum>:
charnum(char a[], int i){
 2a0:	f3 0f 1e fb          	endbr32 
 2a4:	55                   	push   %ebp
 2a5:	89 e5                	mov    %esp,%ebp
 2a7:	57                   	push   %edi
 2a8:	8b 4d 0c             	mov    0xc(%ebp),%ecx
 2ab:	56                   	push   %esi
 2ac:	53                   	push   %ebx
    while(a/10>0){
 2ad:	83 f9 09             	cmp    $0x9,%ecx
 2b0:	7e 5e                	jle    310 <charnum+0x70>
 2b2:	89 ca                	mov    %ecx,%edx
    int r=1;
 2b4:	bb 01 00 00 00       	mov    $0x1,%ebx
        a /= 10;
 2b9:	bf cd cc cc cc       	mov    $0xcccccccd,%edi
 2be:	66 90                	xchg   %ax,%ax
 2c0:	89 d0                	mov    %edx,%eax
 2c2:	89 d6                	mov    %edx,%esi
 2c4:	f7 e7                	mul    %edi
 2c6:	89 d8                	mov    %ebx,%eax
        r++;
 2c8:	83 c3 01             	add    $0x1,%ebx
        a /= 10;
 2cb:	c1 ea 03             	shr    $0x3,%edx
    while(a/10>0){
 2ce:	83 fe 63             	cmp    $0x63,%esi
 2d1:	7f ed                	jg     2c0 <charnum+0x20>
    for(int j=digit(i)-1; j>=0; j--){
 2d3:	03 45 08             	add    0x8(%ebp),%eax
        a[j] = (char)(48+(i%10));
 2d6:	8b 7d 08             	mov    0x8(%ebp),%edi
 2d9:	be 67 66 66 66       	mov    $0x66666667,%esi
 2de:	89 c3                	mov    %eax,%ebx
 2e0:	89 c8                	mov    %ecx,%eax
 2e2:	f7 ee                	imul   %esi
 2e4:	89 c8                	mov    %ecx,%eax
 2e6:	c1 f8 1f             	sar    $0x1f,%eax
 2e9:	c1 fa 02             	sar    $0x2,%edx
 2ec:	29 c2                	sub    %eax,%edx
 2ee:	8d 04 92             	lea    (%edx,%edx,4),%eax
 2f1:	01 c0                	add    %eax,%eax
 2f3:	29 c1                	sub    %eax,%ecx
 2f5:	89 d8                	mov    %ebx,%eax
    for(int j=digit(i)-1; j>=0; j--){
 2f7:	83 eb 01             	sub    $0x1,%ebx
        a[j] = (char)(48+(i%10));
 2fa:	83 c1 30             	add    $0x30,%ecx
 2fd:	88 4b 01             	mov    %cl,0x1(%ebx)
        i/=10;
 300:	89 d1                	mov    %edx,%ecx
    for(int j=digit(i)-1; j>=0; j--){
 302:	39 c7                	cmp    %eax,%edi
 304:	75 da                	jne    2e0 <charnum+0x40>
}
 306:	5b                   	pop    %ebx
 307:	5e                   	pop    %esi
 308:	5f                   	pop    %edi
 309:	5d                   	pop    %ebp
 30a:	c3                   	ret    
 30b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 30f:	90                   	nop
    while(a/10>0){
 310:	31 c0                	xor    %eax,%eax
 312:	eb bf                	jmp    2d3 <charnum+0x33>
 314:	66 90                	xchg   %ax,%ax
 316:	66 90                	xchg   %ax,%ax
 318:	66 90                	xchg   %ax,%ax
 31a:	66 90                	xchg   %ax,%ax
 31c:	66 90                	xchg   %ax,%ax
 31e:	66 90                	xchg   %ax,%ax

00000320 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
 320:	f3 0f 1e fb          	endbr32 
 324:	55                   	push   %ebp
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
 325:	31 c0                	xor    %eax,%eax
{
 327:	89 e5                	mov    %esp,%ebp
 329:	53                   	push   %ebx
 32a:	8b 4d 08             	mov    0x8(%ebp),%ecx
 32d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while((*s++ = *t++) != 0)
 330:	0f b6 14 03          	movzbl (%ebx,%eax,1),%edx
 334:	88 14 01             	mov    %dl,(%ecx,%eax,1)
 337:	83 c0 01             	add    $0x1,%eax
 33a:	84 d2                	test   %dl,%dl
 33c:	75 f2                	jne    330 <strcpy+0x10>
    ;
  return os;
}
 33e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 341:	89 c8                	mov    %ecx,%eax
 343:	c9                   	leave  
 344:	c3                   	ret    
 345:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 34c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000350 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 350:	f3 0f 1e fb          	endbr32 
 354:	55                   	push   %ebp
 355:	89 e5                	mov    %esp,%ebp
 357:	53                   	push   %ebx
 358:	8b 4d 08             	mov    0x8(%ebp),%ecx
 35b:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
 35e:	0f b6 01             	movzbl (%ecx),%eax
 361:	0f b6 1a             	movzbl (%edx),%ebx
 364:	84 c0                	test   %al,%al
 366:	75 18                	jne    380 <strcmp+0x30>
 368:	eb 2a                	jmp    394 <strcmp+0x44>
 36a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
 370:	0f b6 41 01          	movzbl 0x1(%ecx),%eax
    p++, q++;
 374:	83 c1 01             	add    $0x1,%ecx
 377:	8d 5a 01             	lea    0x1(%edx),%ebx
  while(*p && *p == *q)
 37a:	84 c0                	test   %al,%al
 37c:	74 12                	je     390 <strcmp+0x40>
    p++, q++;
 37e:	89 da                	mov    %ebx,%edx
  while(*p && *p == *q)
 380:	0f b6 1a             	movzbl (%edx),%ebx
 383:	38 c3                	cmp    %al,%bl
 385:	74 e9                	je     370 <strcmp+0x20>
  return (uchar)*p - (uchar)*q;
 387:	29 d8                	sub    %ebx,%eax
}
 389:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 38c:	c9                   	leave  
 38d:	c3                   	ret    
 38e:	66 90                	xchg   %ax,%ax
  return (uchar)*p - (uchar)*q;
 390:	0f b6 5a 01          	movzbl 0x1(%edx),%ebx
 394:	31 c0                	xor    %eax,%eax
 396:	29 d8                	sub    %ebx,%eax
}
 398:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 39b:	c9                   	leave  
 39c:	c3                   	ret    
 39d:	8d 76 00             	lea    0x0(%esi),%esi

000003a0 <strlen>:

uint
strlen(const char *s)
{
 3a0:	f3 0f 1e fb          	endbr32 
 3a4:	55                   	push   %ebp
 3a5:	89 e5                	mov    %esp,%ebp
 3a7:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
 3aa:	80 3a 00             	cmpb   $0x0,(%edx)
 3ad:	74 21                	je     3d0 <strlen+0x30>
 3af:	31 c0                	xor    %eax,%eax
 3b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 3b8:	83 c0 01             	add    $0x1,%eax
 3bb:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
 3bf:	89 c1                	mov    %eax,%ecx
 3c1:	75 f5                	jne    3b8 <strlen+0x18>
    ;
  return n;
}
 3c3:	89 c8                	mov    %ecx,%eax
 3c5:	5d                   	pop    %ebp
 3c6:	c3                   	ret    
 3c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 3ce:	66 90                	xchg   %ax,%ax
  for(n = 0; s[n]; n++)
 3d0:	31 c9                	xor    %ecx,%ecx
}
 3d2:	5d                   	pop    %ebp
 3d3:	89 c8                	mov    %ecx,%eax
 3d5:	c3                   	ret    
 3d6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 3dd:	8d 76 00             	lea    0x0(%esi),%esi

000003e0 <memset>:

void*
memset(void *dst, int c, uint n)
{
 3e0:	f3 0f 1e fb          	endbr32 
 3e4:	55                   	push   %ebp
 3e5:	89 e5                	mov    %esp,%ebp
 3e7:	57                   	push   %edi
 3e8:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 3eb:	8b 4d 10             	mov    0x10(%ebp),%ecx
 3ee:	8b 45 0c             	mov    0xc(%ebp),%eax
 3f1:	89 d7                	mov    %edx,%edi
 3f3:	fc                   	cld    
 3f4:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 3f6:	8b 7d fc             	mov    -0x4(%ebp),%edi
 3f9:	89 d0                	mov    %edx,%eax
 3fb:	c9                   	leave  
 3fc:	c3                   	ret    
 3fd:	8d 76 00             	lea    0x0(%esi),%esi

00000400 <strchr>:

char*
strchr(const char *s, char c)
{
 400:	f3 0f 1e fb          	endbr32 
 404:	55                   	push   %ebp
 405:	89 e5                	mov    %esp,%ebp
 407:	8b 45 08             	mov    0x8(%ebp),%eax
 40a:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 40e:	0f b6 10             	movzbl (%eax),%edx
 411:	84 d2                	test   %dl,%dl
 413:	75 16                	jne    42b <strchr+0x2b>
 415:	eb 21                	jmp    438 <strchr+0x38>
 417:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 41e:	66 90                	xchg   %ax,%ax
 420:	0f b6 50 01          	movzbl 0x1(%eax),%edx
 424:	83 c0 01             	add    $0x1,%eax
 427:	84 d2                	test   %dl,%dl
 429:	74 0d                	je     438 <strchr+0x38>
    if(*s == c)
 42b:	38 d1                	cmp    %dl,%cl
 42d:	75 f1                	jne    420 <strchr+0x20>
      return (char*)s;
  return 0;
}
 42f:	5d                   	pop    %ebp
 430:	c3                   	ret    
 431:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return 0;
 438:	31 c0                	xor    %eax,%eax
}
 43a:	5d                   	pop    %ebp
 43b:	c3                   	ret    
 43c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000440 <gets>:

char*
gets(char *buf, int max)
{
 440:	f3 0f 1e fb          	endbr32 
 444:	55                   	push   %ebp
 445:	89 e5                	mov    %esp,%ebp
 447:	57                   	push   %edi
 448:	56                   	push   %esi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    cc = read(0, &c, 1);
 449:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 44c:	53                   	push   %ebx
  for(i=0; i+1 < max; ){
 44d:	31 db                	xor    %ebx,%ebx
{
 44f:	83 ec 1c             	sub    $0x1c,%esp
  for(i=0; i+1 < max; ){
 452:	eb 2b                	jmp    47f <gets+0x3f>
 454:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cc = read(0, &c, 1);
 458:	83 ec 04             	sub    $0x4,%esp
 45b:	6a 01                	push   $0x1
 45d:	57                   	push   %edi
 45e:	6a 00                	push   $0x0
 460:	e8 36 01 00 00       	call   59b <read>
    if(cc < 1)
 465:	83 c4 10             	add    $0x10,%esp
 468:	85 c0                	test   %eax,%eax
 46a:	7e 1d                	jle    489 <gets+0x49>
      break;
    buf[i++] = c;
 46c:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 470:	8b 55 08             	mov    0x8(%ebp),%edx
 473:	88 44 1a ff          	mov    %al,-0x1(%edx,%ebx,1)
    if(c == '\n' || c == '\r')
 477:	3c 0a                	cmp    $0xa,%al
 479:	74 25                	je     4a0 <gets+0x60>
 47b:	3c 0d                	cmp    $0xd,%al
 47d:	74 21                	je     4a0 <gets+0x60>
  for(i=0; i+1 < max; ){
 47f:	89 de                	mov    %ebx,%esi
 481:	83 c3 01             	add    $0x1,%ebx
 484:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
 487:	7c cf                	jl     458 <gets+0x18>
      break;
  }
  buf[i] = '\0';
 489:	8b 45 08             	mov    0x8(%ebp),%eax
 48c:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
  return buf;
}
 490:	8d 65 f4             	lea    -0xc(%ebp),%esp
 493:	5b                   	pop    %ebx
 494:	5e                   	pop    %esi
 495:	5f                   	pop    %edi
 496:	5d                   	pop    %ebp
 497:	c3                   	ret    
 498:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 49f:	90                   	nop
  buf[i] = '\0';
 4a0:	8b 45 08             	mov    0x8(%ebp),%eax
 4a3:	89 de                	mov    %ebx,%esi
 4a5:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
}
 4a9:	8d 65 f4             	lea    -0xc(%ebp),%esp
 4ac:	5b                   	pop    %ebx
 4ad:	5e                   	pop    %esi
 4ae:	5f                   	pop    %edi
 4af:	5d                   	pop    %ebp
 4b0:	c3                   	ret    
 4b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4b8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4bf:	90                   	nop

000004c0 <stat>:

int
stat(const char *n, struct stat *st)
{
 4c0:	f3 0f 1e fb          	endbr32 
 4c4:	55                   	push   %ebp
 4c5:	89 e5                	mov    %esp,%ebp
 4c7:	56                   	push   %esi
 4c8:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 4c9:	83 ec 08             	sub    $0x8,%esp
 4cc:	6a 00                	push   $0x0
 4ce:	ff 75 08             	push   0x8(%ebp)
 4d1:	e8 ed 00 00 00       	call   5c3 <open>
  if(fd < 0)
 4d6:	83 c4 10             	add    $0x10,%esp
 4d9:	85 c0                	test   %eax,%eax
 4db:	78 2b                	js     508 <stat+0x48>
    return -1;
  r = fstat(fd, st);
 4dd:	83 ec 08             	sub    $0x8,%esp
 4e0:	ff 75 0c             	push   0xc(%ebp)
 4e3:	89 c3                	mov    %eax,%ebx
 4e5:	50                   	push   %eax
 4e6:	e8 f0 00 00 00       	call   5db <fstat>
  close(fd);
 4eb:	89 1c 24             	mov    %ebx,(%esp)
  r = fstat(fd, st);
 4ee:	89 c6                	mov    %eax,%esi
  close(fd);
 4f0:	e8 b6 00 00 00       	call   5ab <close>
  return r;
 4f5:	83 c4 10             	add    $0x10,%esp
}
 4f8:	8d 65 f8             	lea    -0x8(%ebp),%esp
 4fb:	89 f0                	mov    %esi,%eax
 4fd:	5b                   	pop    %ebx
 4fe:	5e                   	pop    %esi
 4ff:	5d                   	pop    %ebp
 500:	c3                   	ret    
 501:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
 508:	be ff ff ff ff       	mov    $0xffffffff,%esi
 50d:	eb e9                	jmp    4f8 <stat+0x38>
 50f:	90                   	nop

00000510 <atoi>:

int
atoi(const char *s)
{
 510:	f3 0f 1e fb          	endbr32 
 514:	55                   	push   %ebp
 515:	89 e5                	mov    %esp,%ebp
 517:	53                   	push   %ebx
 518:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 51b:	0f be 02             	movsbl (%edx),%eax
 51e:	8d 48 d0             	lea    -0x30(%eax),%ecx
 521:	80 f9 09             	cmp    $0x9,%cl
  n = 0;
 524:	b9 00 00 00 00       	mov    $0x0,%ecx
  while('0' <= *s && *s <= '9')
 529:	77 1a                	ja     545 <atoi+0x35>
 52b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 52f:	90                   	nop
    n = n*10 + *s++ - '0';
 530:	83 c2 01             	add    $0x1,%edx
 533:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
 536:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
  while('0' <= *s && *s <= '9')
 53a:	0f be 02             	movsbl (%edx),%eax
 53d:	8d 58 d0             	lea    -0x30(%eax),%ebx
 540:	80 fb 09             	cmp    $0x9,%bl
 543:	76 eb                	jbe    530 <atoi+0x20>
  return n;
}
 545:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 548:	89 c8                	mov    %ecx,%eax
 54a:	c9                   	leave  
 54b:	c3                   	ret    
 54c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000550 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 550:	f3 0f 1e fb          	endbr32 
 554:	55                   	push   %ebp
 555:	89 e5                	mov    %esp,%ebp
 557:	57                   	push   %edi
 558:	8b 45 10             	mov    0x10(%ebp),%eax
 55b:	8b 55 08             	mov    0x8(%ebp),%edx
 55e:	56                   	push   %esi
 55f:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst;
  const char *src;

  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 562:	85 c0                	test   %eax,%eax
 564:	7e 0f                	jle    575 <memmove+0x25>
 566:	01 d0                	add    %edx,%eax
  dst = vdst;
 568:	89 d7                	mov    %edx,%edi
 56a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *dst++ = *src++;
 570:	a4                   	movsb  %ds:(%esi),%es:(%edi)
  while(n-- > 0)
 571:	39 f8                	cmp    %edi,%eax
 573:	75 fb                	jne    570 <memmove+0x20>
  return vdst;
}
 575:	5e                   	pop    %esi
 576:	89 d0                	mov    %edx,%eax
 578:	5f                   	pop    %edi
 579:	5d                   	pop    %ebp
 57a:	c3                   	ret    

0000057b <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 57b:	b8 01 00 00 00       	mov    $0x1,%eax
 580:	cd 40                	int    $0x40
 582:	c3                   	ret    

00000583 <exit>:
SYSCALL(exit)
 583:	b8 02 00 00 00       	mov    $0x2,%eax
 588:	cd 40                	int    $0x40
 58a:	c3                   	ret    

0000058b <wait>:
SYSCALL(wait)
 58b:	b8 03 00 00 00       	mov    $0x3,%eax
 590:	cd 40                	int    $0x40
 592:	c3                   	ret    

00000593 <pipe>:
SYSCALL(pipe)
 593:	b8 04 00 00 00       	mov    $0x4,%eax
 598:	cd 40                	int    $0x40
 59a:	c3                   	ret    

0000059b <read>:
SYSCALL(read)
 59b:	b8 05 00 00 00       	mov    $0x5,%eax
 5a0:	cd 40                	int    $0x40
 5a2:	c3                   	ret    

000005a3 <write>:
SYSCALL(write)
 5a3:	b8 10 00 00 00       	mov    $0x10,%eax
 5a8:	cd 40                	int    $0x40
 5aa:	c3                   	ret    

000005ab <close>:
SYSCALL(close)
 5ab:	b8 15 00 00 00       	mov    $0x15,%eax
 5b0:	cd 40                	int    $0x40
 5b2:	c3                   	ret    

000005b3 <kill>:
SYSCALL(kill)
 5b3:	b8 06 00 00 00       	mov    $0x6,%eax
 5b8:	cd 40                	int    $0x40
 5ba:	c3                   	ret    

000005bb <exec>:
SYSCALL(exec)
 5bb:	b8 07 00 00 00       	mov    $0x7,%eax
 5c0:	cd 40                	int    $0x40
 5c2:	c3                   	ret    

000005c3 <open>:
SYSCALL(open)
 5c3:	b8 0f 00 00 00       	mov    $0xf,%eax
 5c8:	cd 40                	int    $0x40
 5ca:	c3                   	ret    

000005cb <mknod>:
SYSCALL(mknod)
 5cb:	b8 11 00 00 00       	mov    $0x11,%eax
 5d0:	cd 40                	int    $0x40
 5d2:	c3                   	ret    

000005d3 <unlink>:
SYSCALL(unlink)
 5d3:	b8 12 00 00 00       	mov    $0x12,%eax
 5d8:	cd 40                	int    $0x40
 5da:	c3                   	ret    

000005db <fstat>:
SYSCALL(fstat)
 5db:	b8 08 00 00 00       	mov    $0x8,%eax
 5e0:	cd 40                	int    $0x40
 5e2:	c3                   	ret    

000005e3 <link>:
SYSCALL(link)
 5e3:	b8 13 00 00 00       	mov    $0x13,%eax
 5e8:	cd 40                	int    $0x40
 5ea:	c3                   	ret    

000005eb <mkdir>:
SYSCALL(mkdir)
 5eb:	b8 14 00 00 00       	mov    $0x14,%eax
 5f0:	cd 40                	int    $0x40
 5f2:	c3                   	ret    

000005f3 <chdir>:
SYSCALL(chdir)
 5f3:	b8 09 00 00 00       	mov    $0x9,%eax
 5f8:	cd 40                	int    $0x40
 5fa:	c3                   	ret    

000005fb <dup>:
SYSCALL(dup)
 5fb:	b8 0a 00 00 00       	mov    $0xa,%eax
 600:	cd 40                	int    $0x40
 602:	c3                   	ret    

00000603 <getpid>:
SYSCALL(getpid)
 603:	b8 0b 00 00 00       	mov    $0xb,%eax
 608:	cd 40                	int    $0x40
 60a:	c3                   	ret    

0000060b <sbrk>:
SYSCALL(sbrk)
 60b:	b8 0c 00 00 00       	mov    $0xc,%eax
 610:	cd 40                	int    $0x40
 612:	c3                   	ret    

00000613 <sleep>:
SYSCALL(sleep)
 613:	b8 0d 00 00 00       	mov    $0xd,%eax
 618:	cd 40                	int    $0x40
 61a:	c3                   	ret    

0000061b <uptime>:
SYSCALL(uptime)
 61b:	b8 0e 00 00 00       	mov    $0xe,%eax
 620:	cd 40                	int    $0x40
 622:	c3                   	ret    

00000623 <poweroff>:
SYSCALL(poweroff)
 623:	b8 16 00 00 00       	mov    $0x16,%eax
 628:	cd 40                	int    $0x40
 62a:	c3                   	ret    

0000062b <calculate_sum_of_digits>:
SYSCALL(calculate_sum_of_digits)
 62b:	b8 17 00 00 00       	mov    $0x17,%eax
 630:	cd 40                	int    $0x40
 632:	c3                   	ret    

00000633 <get_parent_pid>:
SYSCALL(get_parent_pid)
 633:	b8 18 00 00 00       	mov    $0x18,%eax
 638:	cd 40                	int    $0x40
 63a:	c3                   	ret    

0000063b <set_process_parent>:
SYSCALL(set_process_parent)
 63b:	b8 19 00 00 00       	mov    $0x19,%eax
 640:	cd 40                	int    $0x40
 642:	c3                   	ret    
 643:	66 90                	xchg   %ax,%ax
 645:	66 90                	xchg   %ax,%ax
 647:	66 90                	xchg   %ax,%ax
 649:	66 90                	xchg   %ax,%ax
 64b:	66 90                	xchg   %ax,%ax
 64d:	66 90                	xchg   %ax,%ax
 64f:	90                   	nop

00000650 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
 650:	55                   	push   %ebp
 651:	89 e5                	mov    %esp,%ebp
 653:	57                   	push   %edi
 654:	56                   	push   %esi
 655:	53                   	push   %ebx
 656:	83 ec 3c             	sub    $0x3c,%esp
 659:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
 65c:	89 d1                	mov    %edx,%ecx
{
 65e:	89 45 b8             	mov    %eax,-0x48(%ebp)
  if(sgn && xx < 0){
 661:	85 d2                	test   %edx,%edx
 663:	0f 89 7f 00 00 00    	jns    6e8 <printint+0x98>
 669:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
 66d:	74 79                	je     6e8 <printint+0x98>
    neg = 1;
 66f:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
    x = -xx;
 676:	f7 d9                	neg    %ecx
  } else {
    x = xx;
  }

  i = 0;
 678:	31 db                	xor    %ebx,%ebx
 67a:	8d 75 d7             	lea    -0x29(%ebp),%esi
 67d:	8d 76 00             	lea    0x0(%esi),%esi
  do{
    buf[i++] = digits[x % base];
 680:	89 c8                	mov    %ecx,%eax
 682:	31 d2                	xor    %edx,%edx
 684:	89 cf                	mov    %ecx,%edi
 686:	f7 75 c4             	divl   -0x3c(%ebp)
 689:	0f b6 92 a0 0b 00 00 	movzbl 0xba0(%edx),%edx
 690:	89 45 c0             	mov    %eax,-0x40(%ebp)
 693:	89 d8                	mov    %ebx,%eax
 695:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
 698:	8b 4d c0             	mov    -0x40(%ebp),%ecx
    buf[i++] = digits[x % base];
 69b:	88 14 1e             	mov    %dl,(%esi,%ebx,1)
  }while((x /= base) != 0);
 69e:	39 7d c4             	cmp    %edi,-0x3c(%ebp)
 6a1:	76 dd                	jbe    680 <printint+0x30>
  if(neg)
 6a3:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 6a6:	85 c9                	test   %ecx,%ecx
 6a8:	74 0c                	je     6b6 <printint+0x66>
    buf[i++] = '-';
 6aa:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
 6af:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
 6b1:	ba 2d 00 00 00       	mov    $0x2d,%edx

  while(--i >= 0)
 6b6:	8b 7d b8             	mov    -0x48(%ebp),%edi
 6b9:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
 6bd:	eb 07                	jmp    6c6 <printint+0x76>
 6bf:	90                   	nop
    putc(fd, buf[i]);
 6c0:	0f b6 13             	movzbl (%ebx),%edx
 6c3:	83 eb 01             	sub    $0x1,%ebx
  write(fd, &c, 1);
 6c6:	83 ec 04             	sub    $0x4,%esp
 6c9:	88 55 d7             	mov    %dl,-0x29(%ebp)
 6cc:	6a 01                	push   $0x1
 6ce:	56                   	push   %esi
 6cf:	57                   	push   %edi
 6d0:	e8 ce fe ff ff       	call   5a3 <write>
  while(--i >= 0)
 6d5:	83 c4 10             	add    $0x10,%esp
 6d8:	39 de                	cmp    %ebx,%esi
 6da:	75 e4                	jne    6c0 <printint+0x70>
}
 6dc:	8d 65 f4             	lea    -0xc(%ebp),%esp
 6df:	5b                   	pop    %ebx
 6e0:	5e                   	pop    %esi
 6e1:	5f                   	pop    %edi
 6e2:	5d                   	pop    %ebp
 6e3:	c3                   	ret    
 6e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  neg = 0;
 6e8:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%ebp)
 6ef:	eb 87                	jmp    678 <printint+0x28>
 6f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 6f8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 6ff:	90                   	nop

00000700 <printfloat>:

// MOD-2 : Added printf for floats
void
printfloat(int fd, float xx)
{
 700:	f3 0f 1e fb          	endbr32 
 704:	55                   	push   %ebp

  int beg=(int)(xx);
	int fin=(int)(xx*100)-beg*100;
  printint(fd, beg, 10, 1);
 705:	b9 0a 00 00 00       	mov    $0xa,%ecx
{
 70a:	89 e5                	mov    %esp,%ebp
 70c:	57                   	push   %edi
 70d:	56                   	push   %esi
  write(fd, &c, 1);
 70e:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 711:	53                   	push   %ebx
 712:	83 ec 38             	sub    $0x38,%esp
 715:	d9 45 0c             	flds   0xc(%ebp)
 718:	8b 75 08             	mov    0x8(%ebp),%esi
  int beg=(int)(xx);
 71b:	d9 7d d6             	fnstcw -0x2a(%ebp)
  printint(fd, beg, 10, 1);
 71e:	6a 01                	push   $0x1
  int beg=(int)(xx);
 720:	0f b7 45 d6          	movzwl -0x2a(%ebp),%eax
 724:	80 cc 0c             	or     $0xc,%ah
 727:	66 89 45 d4          	mov    %ax,-0x2c(%ebp)
  printint(fd, beg, 10, 1);
 72b:	89 f0                	mov    %esi,%eax
  int beg=(int)(xx);
 72d:	d9 6d d4             	fldcw  -0x2c(%ebp)
 730:	db 55 d0             	fistl  -0x30(%ebp)
 733:	d9 6d d6             	fldcw  -0x2a(%ebp)
 736:	8b 55 d0             	mov    -0x30(%ebp),%edx
	int fin=(int)(xx*100)-beg*100;
 739:	d8 0d b4 0b 00 00    	fmuls  0xbb4
 73f:	d9 6d d4             	fldcw  -0x2c(%ebp)
 742:	db 5d d0             	fistpl -0x30(%ebp)
 745:	d9 6d d6             	fldcw  -0x2a(%ebp)
 748:	6b da 9c             	imul   $0xffffff9c,%edx,%ebx
 74b:	03 5d d0             	add    -0x30(%ebp),%ebx
  printint(fd, beg, 10, 1);
 74e:	e8 fd fe ff ff       	call   650 <printint>
  write(fd, &c, 1);
 753:	83 c4 0c             	add    $0xc,%esp
 756:	c6 45 e7 2e          	movb   $0x2e,-0x19(%ebp)
 75a:	6a 01                	push   $0x1
 75c:	57                   	push   %edi
 75d:	56                   	push   %esi
 75e:	e8 40 fe ff ff       	call   5a3 <write>
  putc(fd, '.');
	if(fin<10)
 763:	83 c4 10             	add    $0x10,%esp
 766:	83 fb 09             	cmp    $0x9,%ebx
 769:	7e 25                	jle    790 <printfloat+0x90>
    putc(fd, '0');
	printint(fd, fin, 10, 1);
 76b:	c7 45 08 01 00 00 00 	movl   $0x1,0x8(%ebp)
}
 772:	8d 65 f4             	lea    -0xc(%ebp),%esp
	printint(fd, fin, 10, 1);
 775:	89 da                	mov    %ebx,%edx
 777:	89 f0                	mov    %esi,%eax
}
 779:	5b                   	pop    %ebx
	printint(fd, fin, 10, 1);
 77a:	b9 0a 00 00 00       	mov    $0xa,%ecx
}
 77f:	5e                   	pop    %esi
 780:	5f                   	pop    %edi
 781:	5d                   	pop    %ebp
	printint(fd, fin, 10, 1);
 782:	e9 c9 fe ff ff       	jmp    650 <printint>
 787:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 78e:	66 90                	xchg   %ax,%ax
  write(fd, &c, 1);
 790:	83 ec 04             	sub    $0x4,%esp
 793:	c6 45 e7 30          	movb   $0x30,-0x19(%ebp)
 797:	6a 01                	push   $0x1
 799:	57                   	push   %edi
 79a:	56                   	push   %esi
 79b:	e8 03 fe ff ff       	call   5a3 <write>
    putc(fd, '0');
 7a0:	83 c4 10             	add    $0x10,%esp
 7a3:	eb c6                	jmp    76b <printfloat+0x6b>
 7a5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 7ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000007b0 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s, %f.
void
printf(int fd, const char *fmt, ...)
{
 7b0:	f3 0f 1e fb          	endbr32 
 7b4:	55                   	push   %ebp
 7b5:	89 e5                	mov    %esp,%ebp
 7b7:	57                   	push   %edi
 7b8:	56                   	push   %esi
 7b9:	53                   	push   %ebx
 7ba:	83 ec 2c             	sub    $0x2c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 7bd:	8b 75 0c             	mov    0xc(%ebp),%esi
 7c0:	0f b6 1e             	movzbl (%esi),%ebx
 7c3:	84 db                	test   %bl,%bl
 7c5:	0f 84 bd 00 00 00    	je     888 <printf+0xd8>
  ap = (uint*)(void*)&fmt + 1;
 7cb:	8d 45 10             	lea    0x10(%ebp),%eax
 7ce:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 7d1:	8d 7d e7             	lea    -0x19(%ebp),%edi
  state = 0;
 7d4:	31 d2                	xor    %edx,%edx
  ap = (uint*)(void*)&fmt + 1;
 7d6:	89 45 cc             	mov    %eax,-0x34(%ebp)
 7d9:	eb 33                	jmp    80e <printf+0x5e>
 7db:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 7df:	90                   	nop
 7e0:	89 55 d0             	mov    %edx,-0x30(%ebp)
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
 7e3:	ba 25 00 00 00       	mov    $0x25,%edx
      if(c == '%'){
 7e8:	83 f8 25             	cmp    $0x25,%eax
 7eb:	74 17                	je     804 <printf+0x54>
  write(fd, &c, 1);
 7ed:	83 ec 04             	sub    $0x4,%esp
 7f0:	88 5d e7             	mov    %bl,-0x19(%ebp)
 7f3:	6a 01                	push   $0x1
 7f5:	57                   	push   %edi
 7f6:	ff 75 08             	push   0x8(%ebp)
 7f9:	e8 a5 fd ff ff       	call   5a3 <write>
 7fe:	8b 55 d0             	mov    -0x30(%ebp),%edx
      } else {
        putc(fd, c);
 801:	83 c4 10             	add    $0x10,%esp
  for(i = 0; fmt[i]; i++){
 804:	0f b6 1e             	movzbl (%esi),%ebx
 807:	83 c6 01             	add    $0x1,%esi
 80a:	84 db                	test   %bl,%bl
 80c:	74 7a                	je     888 <printf+0xd8>
    c = fmt[i] & 0xff;
 80e:	0f be cb             	movsbl %bl,%ecx
 811:	0f b6 c3             	movzbl %bl,%eax
    if(state == 0){
 814:	85 d2                	test   %edx,%edx
 816:	74 c8                	je     7e0 <printf+0x30>
      }
    } else if(state == '%'){
 818:	83 fa 25             	cmp    $0x25,%edx
 81b:	75 e7                	jne    804 <printf+0x54>
      if(c == 'd'){
 81d:	83 f8 64             	cmp    $0x64,%eax
 820:	0f 84 9a 00 00 00    	je     8c0 <printf+0x110>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 826:	81 e1 f7 00 00 00    	and    $0xf7,%ecx
 82c:	83 f9 70             	cmp    $0x70,%ecx
 82f:	74 5f                	je     890 <printf+0xe0>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 831:	83 f8 73             	cmp    $0x73,%eax
 834:	0f 84 d6 00 00 00    	je     910 <printf+0x160>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 83a:	83 f8 63             	cmp    $0x63,%eax
 83d:	0f 84 8d 00 00 00    	je     8d0 <printf+0x120>
        putc(fd, *ap);
        ap++;
      } else if(c == 'f'){ // MOD-2
 843:	83 f8 66             	cmp    $0x66,%eax
 846:	0f 84 f4 00 00 00    	je     940 <printf+0x190>
        printfloat(fd, (float)*ap);
        ap++;
      } else if(c == '%'){
 84c:	83 f8 25             	cmp    $0x25,%eax
 84f:	0f 84 ab 00 00 00    	je     900 <printf+0x150>
  write(fd, &c, 1);
 855:	83 ec 04             	sub    $0x4,%esp
 858:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
 85c:	6a 01                	push   $0x1
 85e:	57                   	push   %edi
 85f:	ff 75 08             	push   0x8(%ebp)
 862:	e8 3c fd ff ff       	call   5a3 <write>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
        putc(fd, c);
 867:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 86a:	83 c4 0c             	add    $0xc,%esp
 86d:	6a 01                	push   $0x1
  for(i = 0; fmt[i]; i++){
 86f:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 872:	57                   	push   %edi
 873:	ff 75 08             	push   0x8(%ebp)
 876:	e8 28 fd ff ff       	call   5a3 <write>
  for(i = 0; fmt[i]; i++){
 87b:	0f b6 5e ff          	movzbl -0x1(%esi),%ebx
        putc(fd, c);
 87f:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 882:	31 d2                	xor    %edx,%edx
  for(i = 0; fmt[i]; i++){
 884:	84 db                	test   %bl,%bl
 886:	75 86                	jne    80e <printf+0x5e>
    }
  }
 888:	8d 65 f4             	lea    -0xc(%ebp),%esp
 88b:	5b                   	pop    %ebx
 88c:	5e                   	pop    %esi
 88d:	5f                   	pop    %edi
 88e:	5d                   	pop    %ebp
 88f:	c3                   	ret    
        printint(fd, *ap, 16, 0);
 890:	83 ec 0c             	sub    $0xc,%esp
 893:	b9 10 00 00 00       	mov    $0x10,%ecx
 898:	6a 00                	push   $0x0
 89a:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 89d:	8b 45 08             	mov    0x8(%ebp),%eax
 8a0:	8b 13                	mov    (%ebx),%edx
 8a2:	e8 a9 fd ff ff       	call   650 <printint>
        ap++;
 8a7:	89 d8                	mov    %ebx,%eax
 8a9:	83 c4 10             	add    $0x10,%esp
      state = 0;
 8ac:	31 d2                	xor    %edx,%edx
        ap++;
 8ae:	83 c0 04             	add    $0x4,%eax
 8b1:	89 45 cc             	mov    %eax,-0x34(%ebp)
 8b4:	e9 4b ff ff ff       	jmp    804 <printf+0x54>
 8b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        printint(fd, *ap, 10, 1);
 8c0:	83 ec 0c             	sub    $0xc,%esp
 8c3:	b9 0a 00 00 00       	mov    $0xa,%ecx
 8c8:	6a 01                	push   $0x1
 8ca:	eb ce                	jmp    89a <printf+0xea>
 8cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        putc(fd, *ap);
 8d0:	8b 5d cc             	mov    -0x34(%ebp),%ebx
  write(fd, &c, 1);
 8d3:	83 ec 04             	sub    $0x4,%esp
        putc(fd, *ap);
 8d6:	8b 03                	mov    (%ebx),%eax
  write(fd, &c, 1);
 8d8:	6a 01                	push   $0x1
        ap++;
 8da:	83 c3 04             	add    $0x4,%ebx
  write(fd, &c, 1);
 8dd:	57                   	push   %edi
 8de:	ff 75 08             	push   0x8(%ebp)
        putc(fd, *ap);
 8e1:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 8e4:	e8 ba fc ff ff       	call   5a3 <write>
        ap++;
 8e9:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 8ec:	83 c4 10             	add    $0x10,%esp
      state = 0;
 8ef:	31 d2                	xor    %edx,%edx
 8f1:	e9 0e ff ff ff       	jmp    804 <printf+0x54>
 8f6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 8fd:	8d 76 00             	lea    0x0(%esi),%esi
        putc(fd, c);
 900:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 903:	83 ec 04             	sub    $0x4,%esp
 906:	e9 62 ff ff ff       	jmp    86d <printf+0xbd>
 90b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 90f:	90                   	nop
        s = (char*)*ap;
 910:	8b 45 cc             	mov    -0x34(%ebp),%eax
 913:	8b 18                	mov    (%eax),%ebx
        ap++;
 915:	83 c0 04             	add    $0x4,%eax
 918:	89 45 cc             	mov    %eax,-0x34(%ebp)
        if(s == 0)
 91b:	85 db                	test   %ebx,%ebx
 91d:	74 4f                	je     96e <printf+0x1be>
        while(*s != 0){
 91f:	0f b6 03             	movzbl (%ebx),%eax
      state = 0;
 922:	31 d2                	xor    %edx,%edx
        while(*s != 0){
 924:	84 c0                	test   %al,%al
 926:	0f 84 d8 fe ff ff    	je     804 <printf+0x54>
 92c:	89 75 d0             	mov    %esi,-0x30(%ebp)
 92f:	89 de                	mov    %ebx,%esi
 931:	8b 5d 08             	mov    0x8(%ebp),%ebx
 934:	eb 4a                	jmp    980 <printf+0x1d0>
 936:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 93d:	8d 76 00             	lea    0x0(%esi),%esi
        printfloat(fd, (float)*ap);
 940:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 943:	31 d2                	xor    %edx,%edx
 945:	83 ec 0c             	sub    $0xc,%esp
 948:	89 55 d4             	mov    %edx,-0x2c(%ebp)
 94b:	8b 03                	mov    (%ebx),%eax
        ap++;
 94d:	83 c3 04             	add    $0x4,%ebx
        printfloat(fd, (float)*ap);
 950:	89 45 d0             	mov    %eax,-0x30(%ebp)
 953:	df 6d d0             	fildll -0x30(%ebp)
 956:	d9 1c 24             	fstps  (%esp)
 959:	ff 75 08             	push   0x8(%ebp)
 95c:	e8 9f fd ff ff       	call   700 <printfloat>
        ap++;
 961:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 964:	83 c4 10             	add    $0x10,%esp
      state = 0;
 967:	31 d2                	xor    %edx,%edx
 969:	e9 96 fe ff ff       	jmp    804 <printf+0x54>
          s = "(null)";
 96e:	bb 98 0b 00 00       	mov    $0xb98,%ebx
        while(*s != 0){
 973:	89 75 d0             	mov    %esi,-0x30(%ebp)
 976:	b8 28 00 00 00       	mov    $0x28,%eax
 97b:	89 de                	mov    %ebx,%esi
 97d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  write(fd, &c, 1);
 980:	83 ec 04             	sub    $0x4,%esp
          s++;
 983:	83 c6 01             	add    $0x1,%esi
 986:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 989:	6a 01                	push   $0x1
 98b:	57                   	push   %edi
 98c:	53                   	push   %ebx
 98d:	e8 11 fc ff ff       	call   5a3 <write>
        while(*s != 0){
 992:	0f b6 06             	movzbl (%esi),%eax
 995:	83 c4 10             	add    $0x10,%esp
 998:	84 c0                	test   %al,%al
 99a:	75 e4                	jne    980 <printf+0x1d0>
      state = 0;
 99c:	8b 75 d0             	mov    -0x30(%ebp),%esi
 99f:	31 d2                	xor    %edx,%edx
 9a1:	e9 5e fe ff ff       	jmp    804 <printf+0x54>
 9a6:	66 90                	xchg   %ax,%ax
 9a8:	66 90                	xchg   %ax,%ax
 9aa:	66 90                	xchg   %ax,%ax
 9ac:	66 90                	xchg   %ax,%ax
 9ae:	66 90                	xchg   %ax,%ax

000009b0 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 9b0:	f3 0f 1e fb          	endbr32 
 9b4:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 9b5:	a1 04 0f 00 00       	mov    0xf04,%eax
{
 9ba:	89 e5                	mov    %esp,%ebp
 9bc:	57                   	push   %edi
 9bd:	56                   	push   %esi
 9be:	53                   	push   %ebx
 9bf:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = (Header*)ap - 1;
 9c2:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 9c5:	8d 76 00             	lea    0x0(%esi),%esi
 9c8:	89 c2                	mov    %eax,%edx
 9ca:	8b 00                	mov    (%eax),%eax
 9cc:	39 ca                	cmp    %ecx,%edx
 9ce:	73 30                	jae    a00 <free+0x50>
 9d0:	39 c1                	cmp    %eax,%ecx
 9d2:	72 04                	jb     9d8 <free+0x28>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 9d4:	39 c2                	cmp    %eax,%edx
 9d6:	72 f0                	jb     9c8 <free+0x18>
      break;
  if(bp + bp->s.size == p->s.ptr){
 9d8:	8b 73 fc             	mov    -0x4(%ebx),%esi
 9db:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 9de:	39 f8                	cmp    %edi,%eax
 9e0:	74 30                	je     a12 <free+0x62>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 9e2:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 9e5:	8b 42 04             	mov    0x4(%edx),%eax
 9e8:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 9eb:	39 f1                	cmp    %esi,%ecx
 9ed:	74 3a                	je     a29 <free+0x79>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 9ef:	89 0a                	mov    %ecx,(%edx)
  freep = p;
}
 9f1:	5b                   	pop    %ebx
  freep = p;
 9f2:	89 15 04 0f 00 00    	mov    %edx,0xf04
}
 9f8:	5e                   	pop    %esi
 9f9:	5f                   	pop    %edi
 9fa:	5d                   	pop    %ebp
 9fb:	c3                   	ret    
 9fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 a00:	39 c2                	cmp    %eax,%edx
 a02:	72 c4                	jb     9c8 <free+0x18>
 a04:	39 c1                	cmp    %eax,%ecx
 a06:	73 c0                	jae    9c8 <free+0x18>
  if(bp + bp->s.size == p->s.ptr){
 a08:	8b 73 fc             	mov    -0x4(%ebx),%esi
 a0b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 a0e:	39 f8                	cmp    %edi,%eax
 a10:	75 d0                	jne    9e2 <free+0x32>
    bp->s.size += p->s.ptr->s.size;
 a12:	03 70 04             	add    0x4(%eax),%esi
 a15:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 a18:	8b 02                	mov    (%edx),%eax
 a1a:	8b 00                	mov    (%eax),%eax
 a1c:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 a1f:	8b 42 04             	mov    0x4(%edx),%eax
 a22:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 a25:	39 f1                	cmp    %esi,%ecx
 a27:	75 c6                	jne    9ef <free+0x3f>
    p->s.size += bp->s.size;
 a29:	03 43 fc             	add    -0x4(%ebx),%eax
  freep = p;
 a2c:	89 15 04 0f 00 00    	mov    %edx,0xf04
    p->s.size += bp->s.size;
 a32:	89 42 04             	mov    %eax,0x4(%edx)
    p->s.ptr = bp->s.ptr;
 a35:	8b 43 f8             	mov    -0x8(%ebx),%eax
 a38:	89 02                	mov    %eax,(%edx)
}
 a3a:	5b                   	pop    %ebx
 a3b:	5e                   	pop    %esi
 a3c:	5f                   	pop    %edi
 a3d:	5d                   	pop    %ebp
 a3e:	c3                   	ret    
 a3f:	90                   	nop

00000a40 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
 a40:	f3 0f 1e fb          	endbr32 
 a44:	55                   	push   %ebp
 a45:	89 e5                	mov    %esp,%ebp
 a47:	57                   	push   %edi
 a48:	56                   	push   %esi
 a49:	53                   	push   %ebx
 a4a:	83 ec 1c             	sub    $0x1c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 a4d:	8b 45 08             	mov    0x8(%ebp),%eax
  if((prevp = freep) == 0){
 a50:	8b 3d 04 0f 00 00    	mov    0xf04,%edi
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 a56:	8d 70 07             	lea    0x7(%eax),%esi
 a59:	c1 ee 03             	shr    $0x3,%esi
 a5c:	83 c6 01             	add    $0x1,%esi
  if((prevp = freep) == 0){
 a5f:	85 ff                	test   %edi,%edi
 a61:	0f 84 a9 00 00 00    	je     b10 <malloc+0xd0>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 a67:	8b 07                	mov    (%edi),%eax
    if(p->s.size >= nunits){
 a69:	8b 48 04             	mov    0x4(%eax),%ecx
 a6c:	39 f1                	cmp    %esi,%ecx
 a6e:	73 6d                	jae    add <malloc+0x9d>
 a70:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
 a76:	bb 00 10 00 00       	mov    $0x1000,%ebx
 a7b:	0f 43 de             	cmovae %esi,%ebx
  p = sbrk(nu * sizeof(Header));
 a7e:	8d 0c dd 00 00 00 00 	lea    0x0(,%ebx,8),%ecx
 a85:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
 a88:	eb 17                	jmp    aa1 <malloc+0x61>
 a8a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 a90:	8b 10                	mov    (%eax),%edx
    if(p->s.size >= nunits){
 a92:	8b 4a 04             	mov    0x4(%edx),%ecx
 a95:	39 f1                	cmp    %esi,%ecx
 a97:	73 4f                	jae    ae8 <malloc+0xa8>
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
 a99:	8b 3d 04 0f 00 00    	mov    0xf04,%edi
 a9f:	89 d0                	mov    %edx,%eax
 aa1:	39 c7                	cmp    %eax,%edi
 aa3:	75 eb                	jne    a90 <malloc+0x50>
  p = sbrk(nu * sizeof(Header));
 aa5:	83 ec 0c             	sub    $0xc,%esp
 aa8:	ff 75 e4             	push   -0x1c(%ebp)
 aab:	e8 5b fb ff ff       	call   60b <sbrk>
  if(p == (char*)-1)
 ab0:	83 c4 10             	add    $0x10,%esp
 ab3:	83 f8 ff             	cmp    $0xffffffff,%eax
 ab6:	74 1b                	je     ad3 <malloc+0x93>
  hp->s.size = nu;
 ab8:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 abb:	83 ec 0c             	sub    $0xc,%esp
 abe:	83 c0 08             	add    $0x8,%eax
 ac1:	50                   	push   %eax
 ac2:	e8 e9 fe ff ff       	call   9b0 <free>
  return freep;
 ac7:	a1 04 0f 00 00       	mov    0xf04,%eax
      if((p = morecore(nunits)) == 0)
 acc:	83 c4 10             	add    $0x10,%esp
 acf:	85 c0                	test   %eax,%eax
 ad1:	75 bd                	jne    a90 <malloc+0x50>
        return 0;
  }
}
 ad3:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return 0;
 ad6:	31 c0                	xor    %eax,%eax
}
 ad8:	5b                   	pop    %ebx
 ad9:	5e                   	pop    %esi
 ada:	5f                   	pop    %edi
 adb:	5d                   	pop    %ebp
 adc:	c3                   	ret    
    if(p->s.size >= nunits){
 add:	89 c2                	mov    %eax,%edx
 adf:	89 f8                	mov    %edi,%eax
 ae1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->s.size == nunits)
 ae8:	39 ce                	cmp    %ecx,%esi
 aea:	74 54                	je     b40 <malloc+0x100>
        p->s.size -= nunits;
 aec:	29 f1                	sub    %esi,%ecx
 aee:	89 4a 04             	mov    %ecx,0x4(%edx)
        p += p->s.size;
 af1:	8d 14 ca             	lea    (%edx,%ecx,8),%edx
        p->s.size = nunits;
 af4:	89 72 04             	mov    %esi,0x4(%edx)
      freep = prevp;
 af7:	a3 04 0f 00 00       	mov    %eax,0xf04
}
 afc:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return (void*)(p + 1);
 aff:	8d 42 08             	lea    0x8(%edx),%eax
}
 b02:	5b                   	pop    %ebx
 b03:	5e                   	pop    %esi
 b04:	5f                   	pop    %edi
 b05:	5d                   	pop    %ebp
 b06:	c3                   	ret    
 b07:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 b0e:	66 90                	xchg   %ax,%ax
    base.s.ptr = freep = prevp = &base;
 b10:	c7 05 04 0f 00 00 08 	movl   $0xf08,0xf04
 b17:	0f 00 00 
    base.s.size = 0;
 b1a:	bf 08 0f 00 00       	mov    $0xf08,%edi
    base.s.ptr = freep = prevp = &base;
 b1f:	c7 05 08 0f 00 00 08 	movl   $0xf08,0xf08
 b26:	0f 00 00 
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 b29:	89 f8                	mov    %edi,%eax
    base.s.size = 0;
 b2b:	c7 05 0c 0f 00 00 00 	movl   $0x0,0xf0c
 b32:	00 00 00 
    if(p->s.size >= nunits){
 b35:	e9 36 ff ff ff       	jmp    a70 <malloc+0x30>
 b3a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        prevp->s.ptr = p->s.ptr;
 b40:	8b 0a                	mov    (%edx),%ecx
 b42:	89 08                	mov    %ecx,(%eax)
 b44:	eb b1                	jmp    af7 <malloc+0xb7>
