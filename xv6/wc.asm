
_wc:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
  printf(1, "%d %d %d %s\n", l, w, c, name);
}

int
main(int argc, char *argv[])
{
   0:	f3 0f 1e fb          	endbr32 
   4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   8:	83 e4 f0             	and    $0xfffffff0,%esp
   b:	ff 71 fc             	push   -0x4(%ecx)
   e:	55                   	push   %ebp
   f:	89 e5                	mov    %esp,%ebp
  11:	57                   	push   %edi
  12:	56                   	push   %esi
  13:	be 01 00 00 00       	mov    $0x1,%esi
  18:	53                   	push   %ebx
  19:	51                   	push   %ecx
  1a:	83 ec 18             	sub    $0x18,%esp
  1d:	8b 01                	mov    (%ecx),%eax
  1f:	8b 59 04             	mov    0x4(%ecx),%ebx
  22:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  25:	83 c3 04             	add    $0x4,%ebx
  int fd, i;

  if(argc <= 1){
  28:	83 f8 01             	cmp    $0x1,%eax
  2b:	7e 52                	jle    7f <main+0x7f>
  2d:	8d 76 00             	lea    0x0(%esi),%esi
    wc(0, "");
    exit();
  }

  for(i = 1; i < argc; i++){
    if((fd = open(argv[i], 0)) < 0){
  30:	83 ec 08             	sub    $0x8,%esp
  33:	6a 00                	push   $0x0
  35:	ff 33                	push   (%ebx)
  37:	e8 f7 03 00 00       	call   433 <open>
  3c:	83 c4 10             	add    $0x10,%esp
  3f:	89 c7                	mov    %eax,%edi
  41:	85 c0                	test   %eax,%eax
  43:	78 26                	js     6b <main+0x6b>
      printf(1, "wc: cannot open %s\n", argv[i]);
      exit();
    }
    wc(fd, argv[i]);
  45:	83 ec 08             	sub    $0x8,%esp
  48:	ff 33                	push   (%ebx)
  for(i = 1; i < argc; i++){
  4a:	83 c6 01             	add    $0x1,%esi
  4d:	83 c3 04             	add    $0x4,%ebx
    wc(fd, argv[i]);
  50:	50                   	push   %eax
  51:	e8 4a 00 00 00       	call   a0 <wc>
    close(fd);
  56:	89 3c 24             	mov    %edi,(%esp)
  59:	e8 bd 03 00 00       	call   41b <close>
  for(i = 1; i < argc; i++){
  5e:	83 c4 10             	add    $0x10,%esp
  61:	39 75 e4             	cmp    %esi,-0x1c(%ebp)
  64:	75 ca                	jne    30 <main+0x30>
  }
  exit();
  66:	e8 88 03 00 00       	call   3f3 <exit>
      printf(1, "wc: cannot open %s\n", argv[i]);
  6b:	50                   	push   %eax
  6c:	ff 33                	push   (%ebx)
  6e:	68 db 09 00 00       	push   $0x9db
  73:	6a 01                	push   $0x1
  75:	e8 a6 05 00 00       	call   620 <printf>
      exit();
  7a:	e8 74 03 00 00       	call   3f3 <exit>
    wc(0, "");
  7f:	52                   	push   %edx
  80:	52                   	push   %edx
  81:	68 cd 09 00 00       	push   $0x9cd
  86:	6a 00                	push   $0x0
  88:	e8 13 00 00 00       	call   a0 <wc>
    exit();
  8d:	e8 61 03 00 00       	call   3f3 <exit>
  92:	66 90                	xchg   %ax,%ax
  94:	66 90                	xchg   %ax,%ax
  96:	66 90                	xchg   %ax,%ax
  98:	66 90                	xchg   %ax,%ax
  9a:	66 90                	xchg   %ax,%ax
  9c:	66 90                	xchg   %ax,%ax
  9e:	66 90                	xchg   %ax,%ax

000000a0 <wc>:
{
  a0:	f3 0f 1e fb          	endbr32 
  a4:	55                   	push   %ebp
  a5:	89 e5                	mov    %esp,%ebp
  a7:	57                   	push   %edi
  a8:	56                   	push   %esi
  a9:	53                   	push   %ebx
  l = w = c = 0;
  aa:	31 db                	xor    %ebx,%ebx
{
  ac:	83 ec 1c             	sub    $0x1c,%esp
  inword = 0;
  af:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  l = w = c = 0;
  b6:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
  bd:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
  c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  while((n = read(fd, buf, sizeof(buf))) > 0){
  c8:	83 ec 04             	sub    $0x4,%esp
  cb:	68 00 02 00 00       	push   $0x200
  d0:	68 20 0d 00 00       	push   $0xd20
  d5:	ff 75 08             	push   0x8(%ebp)
  d8:	e8 2e 03 00 00       	call   40b <read>
  dd:	83 c4 10             	add    $0x10,%esp
  e0:	89 c6                	mov    %eax,%esi
  e2:	85 c0                	test   %eax,%eax
  e4:	7e 62                	jle    148 <wc+0xa8>
    for(i=0; i<n; i++){
  e6:	31 ff                	xor    %edi,%edi
  e8:	eb 14                	jmp    fe <wc+0x5e>
  ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        inword = 0;
  f0:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
    for(i=0; i<n; i++){
  f7:	83 c7 01             	add    $0x1,%edi
  fa:	39 fe                	cmp    %edi,%esi
  fc:	74 42                	je     140 <wc+0xa0>
      if(buf[i] == '\n')
  fe:	0f be 87 20 0d 00 00 	movsbl 0xd20(%edi),%eax
        l++;
 105:	31 c9                	xor    %ecx,%ecx
 107:	3c 0a                	cmp    $0xa,%al
 109:	0f 94 c1             	sete   %cl
      if(strchr(" \r\t\n\v", buf[i]))
 10c:	83 ec 08             	sub    $0x8,%esp
 10f:	50                   	push   %eax
        l++;
 110:	01 cb                	add    %ecx,%ebx
      if(strchr(" \r\t\n\v", buf[i]))
 112:	68 b8 09 00 00       	push   $0x9b8
 117:	e8 54 01 00 00       	call   270 <strchr>
 11c:	83 c4 10             	add    $0x10,%esp
 11f:	85 c0                	test   %eax,%eax
 121:	75 cd                	jne    f0 <wc+0x50>
      else if(!inword){
 123:	8b 55 e4             	mov    -0x1c(%ebp),%edx
 126:	85 d2                	test   %edx,%edx
 128:	75 cd                	jne    f7 <wc+0x57>
    for(i=0; i<n; i++){
 12a:	83 c7 01             	add    $0x1,%edi
        w++;
 12d:	83 45 e0 01          	addl   $0x1,-0x20(%ebp)
        inword = 1;
 131:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
    for(i=0; i<n; i++){
 138:	39 fe                	cmp    %edi,%esi
 13a:	75 c2                	jne    fe <wc+0x5e>
 13c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      c++;
 140:	01 75 dc             	add    %esi,-0x24(%ebp)
 143:	eb 83                	jmp    c8 <wc+0x28>
 145:	8d 76 00             	lea    0x0(%esi),%esi
  if(n < 0){
 148:	75 24                	jne    16e <wc+0xce>
  printf(1, "%d %d %d %s\n", l, w, c, name);
 14a:	83 ec 08             	sub    $0x8,%esp
 14d:	ff 75 0c             	push   0xc(%ebp)
 150:	ff 75 dc             	push   -0x24(%ebp)
 153:	ff 75 e0             	push   -0x20(%ebp)
 156:	53                   	push   %ebx
 157:	68 ce 09 00 00       	push   $0x9ce
 15c:	6a 01                	push   $0x1
 15e:	e8 bd 04 00 00       	call   620 <printf>
}
 163:	83 c4 20             	add    $0x20,%esp
 166:	8d 65 f4             	lea    -0xc(%ebp),%esp
 169:	5b                   	pop    %ebx
 16a:	5e                   	pop    %esi
 16b:	5f                   	pop    %edi
 16c:	5d                   	pop    %ebp
 16d:	c3                   	ret    
    printf(1, "wc: read error\n");
 16e:	50                   	push   %eax
 16f:	50                   	push   %eax
 170:	68 be 09 00 00       	push   $0x9be
 175:	6a 01                	push   $0x1
 177:	e8 a4 04 00 00       	call   620 <printf>
    exit();
 17c:	e8 72 02 00 00       	call   3f3 <exit>
 181:	66 90                	xchg   %ax,%ax
 183:	66 90                	xchg   %ax,%ax
 185:	66 90                	xchg   %ax,%ax
 187:	66 90                	xchg   %ax,%ax
 189:	66 90                	xchg   %ax,%ax
 18b:	66 90                	xchg   %ax,%ax
 18d:	66 90                	xchg   %ax,%ax
 18f:	90                   	nop

00000190 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
 190:	f3 0f 1e fb          	endbr32 
 194:	55                   	push   %ebp
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
 195:	31 c0                	xor    %eax,%eax
{
 197:	89 e5                	mov    %esp,%ebp
 199:	53                   	push   %ebx
 19a:	8b 4d 08             	mov    0x8(%ebp),%ecx
 19d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while((*s++ = *t++) != 0)
 1a0:	0f b6 14 03          	movzbl (%ebx,%eax,1),%edx
 1a4:	88 14 01             	mov    %dl,(%ecx,%eax,1)
 1a7:	83 c0 01             	add    $0x1,%eax
 1aa:	84 d2                	test   %dl,%dl
 1ac:	75 f2                	jne    1a0 <strcpy+0x10>
    ;
  return os;
}
 1ae:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 1b1:	89 c8                	mov    %ecx,%eax
 1b3:	c9                   	leave  
 1b4:	c3                   	ret    
 1b5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 1bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000001c0 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 1c0:	f3 0f 1e fb          	endbr32 
 1c4:	55                   	push   %ebp
 1c5:	89 e5                	mov    %esp,%ebp
 1c7:	53                   	push   %ebx
 1c8:	8b 4d 08             	mov    0x8(%ebp),%ecx
 1cb:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
 1ce:	0f b6 01             	movzbl (%ecx),%eax
 1d1:	0f b6 1a             	movzbl (%edx),%ebx
 1d4:	84 c0                	test   %al,%al
 1d6:	75 18                	jne    1f0 <strcmp+0x30>
 1d8:	eb 2a                	jmp    204 <strcmp+0x44>
 1da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
 1e0:	0f b6 41 01          	movzbl 0x1(%ecx),%eax
    p++, q++;
 1e4:	83 c1 01             	add    $0x1,%ecx
 1e7:	8d 5a 01             	lea    0x1(%edx),%ebx
  while(*p && *p == *q)
 1ea:	84 c0                	test   %al,%al
 1ec:	74 12                	je     200 <strcmp+0x40>
    p++, q++;
 1ee:	89 da                	mov    %ebx,%edx
  while(*p && *p == *q)
 1f0:	0f b6 1a             	movzbl (%edx),%ebx
 1f3:	38 c3                	cmp    %al,%bl
 1f5:	74 e9                	je     1e0 <strcmp+0x20>
  return (uchar)*p - (uchar)*q;
 1f7:	29 d8                	sub    %ebx,%eax
}
 1f9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 1fc:	c9                   	leave  
 1fd:	c3                   	ret    
 1fe:	66 90                	xchg   %ax,%ax
  return (uchar)*p - (uchar)*q;
 200:	0f b6 5a 01          	movzbl 0x1(%edx),%ebx
 204:	31 c0                	xor    %eax,%eax
 206:	29 d8                	sub    %ebx,%eax
}
 208:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 20b:	c9                   	leave  
 20c:	c3                   	ret    
 20d:	8d 76 00             	lea    0x0(%esi),%esi

00000210 <strlen>:

uint
strlen(const char *s)
{
 210:	f3 0f 1e fb          	endbr32 
 214:	55                   	push   %ebp
 215:	89 e5                	mov    %esp,%ebp
 217:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
 21a:	80 3a 00             	cmpb   $0x0,(%edx)
 21d:	74 21                	je     240 <strlen+0x30>
 21f:	31 c0                	xor    %eax,%eax
 221:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 228:	83 c0 01             	add    $0x1,%eax
 22b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
 22f:	89 c1                	mov    %eax,%ecx
 231:	75 f5                	jne    228 <strlen+0x18>
    ;
  return n;
}
 233:	89 c8                	mov    %ecx,%eax
 235:	5d                   	pop    %ebp
 236:	c3                   	ret    
 237:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 23e:	66 90                	xchg   %ax,%ax
  for(n = 0; s[n]; n++)
 240:	31 c9                	xor    %ecx,%ecx
}
 242:	5d                   	pop    %ebp
 243:	89 c8                	mov    %ecx,%eax
 245:	c3                   	ret    
 246:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 24d:	8d 76 00             	lea    0x0(%esi),%esi

00000250 <memset>:

void*
memset(void *dst, int c, uint n)
{
 250:	f3 0f 1e fb          	endbr32 
 254:	55                   	push   %ebp
 255:	89 e5                	mov    %esp,%ebp
 257:	57                   	push   %edi
 258:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 25b:	8b 4d 10             	mov    0x10(%ebp),%ecx
 25e:	8b 45 0c             	mov    0xc(%ebp),%eax
 261:	89 d7                	mov    %edx,%edi
 263:	fc                   	cld    
 264:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 266:	8b 7d fc             	mov    -0x4(%ebp),%edi
 269:	89 d0                	mov    %edx,%eax
 26b:	c9                   	leave  
 26c:	c3                   	ret    
 26d:	8d 76 00             	lea    0x0(%esi),%esi

00000270 <strchr>:

char*
strchr(const char *s, char c)
{
 270:	f3 0f 1e fb          	endbr32 
 274:	55                   	push   %ebp
 275:	89 e5                	mov    %esp,%ebp
 277:	8b 45 08             	mov    0x8(%ebp),%eax
 27a:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 27e:	0f b6 10             	movzbl (%eax),%edx
 281:	84 d2                	test   %dl,%dl
 283:	75 16                	jne    29b <strchr+0x2b>
 285:	eb 21                	jmp    2a8 <strchr+0x38>
 287:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 28e:	66 90                	xchg   %ax,%ax
 290:	0f b6 50 01          	movzbl 0x1(%eax),%edx
 294:	83 c0 01             	add    $0x1,%eax
 297:	84 d2                	test   %dl,%dl
 299:	74 0d                	je     2a8 <strchr+0x38>
    if(*s == c)
 29b:	38 d1                	cmp    %dl,%cl
 29d:	75 f1                	jne    290 <strchr+0x20>
      return (char*)s;
  return 0;
}
 29f:	5d                   	pop    %ebp
 2a0:	c3                   	ret    
 2a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return 0;
 2a8:	31 c0                	xor    %eax,%eax
}
 2aa:	5d                   	pop    %ebp
 2ab:	c3                   	ret    
 2ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000002b0 <gets>:

char*
gets(char *buf, int max)
{
 2b0:	f3 0f 1e fb          	endbr32 
 2b4:	55                   	push   %ebp
 2b5:	89 e5                	mov    %esp,%ebp
 2b7:	57                   	push   %edi
 2b8:	56                   	push   %esi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    cc = read(0, &c, 1);
 2b9:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 2bc:	53                   	push   %ebx
  for(i=0; i+1 < max; ){
 2bd:	31 db                	xor    %ebx,%ebx
{
 2bf:	83 ec 1c             	sub    $0x1c,%esp
  for(i=0; i+1 < max; ){
 2c2:	eb 2b                	jmp    2ef <gets+0x3f>
 2c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cc = read(0, &c, 1);
 2c8:	83 ec 04             	sub    $0x4,%esp
 2cb:	6a 01                	push   $0x1
 2cd:	57                   	push   %edi
 2ce:	6a 00                	push   $0x0
 2d0:	e8 36 01 00 00       	call   40b <read>
    if(cc < 1)
 2d5:	83 c4 10             	add    $0x10,%esp
 2d8:	85 c0                	test   %eax,%eax
 2da:	7e 1d                	jle    2f9 <gets+0x49>
      break;
    buf[i++] = c;
 2dc:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 2e0:	8b 55 08             	mov    0x8(%ebp),%edx
 2e3:	88 44 1a ff          	mov    %al,-0x1(%edx,%ebx,1)
    if(c == '\n' || c == '\r')
 2e7:	3c 0a                	cmp    $0xa,%al
 2e9:	74 25                	je     310 <gets+0x60>
 2eb:	3c 0d                	cmp    $0xd,%al
 2ed:	74 21                	je     310 <gets+0x60>
  for(i=0; i+1 < max; ){
 2ef:	89 de                	mov    %ebx,%esi
 2f1:	83 c3 01             	add    $0x1,%ebx
 2f4:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
 2f7:	7c cf                	jl     2c8 <gets+0x18>
      break;
  }
  buf[i] = '\0';
 2f9:	8b 45 08             	mov    0x8(%ebp),%eax
 2fc:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
  return buf;
}
 300:	8d 65 f4             	lea    -0xc(%ebp),%esp
 303:	5b                   	pop    %ebx
 304:	5e                   	pop    %esi
 305:	5f                   	pop    %edi
 306:	5d                   	pop    %ebp
 307:	c3                   	ret    
 308:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 30f:	90                   	nop
  buf[i] = '\0';
 310:	8b 45 08             	mov    0x8(%ebp),%eax
 313:	89 de                	mov    %ebx,%esi
 315:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
}
 319:	8d 65 f4             	lea    -0xc(%ebp),%esp
 31c:	5b                   	pop    %ebx
 31d:	5e                   	pop    %esi
 31e:	5f                   	pop    %edi
 31f:	5d                   	pop    %ebp
 320:	c3                   	ret    
 321:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 328:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 32f:	90                   	nop

00000330 <stat>:

int
stat(const char *n, struct stat *st)
{
 330:	f3 0f 1e fb          	endbr32 
 334:	55                   	push   %ebp
 335:	89 e5                	mov    %esp,%ebp
 337:	56                   	push   %esi
 338:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 339:	83 ec 08             	sub    $0x8,%esp
 33c:	6a 00                	push   $0x0
 33e:	ff 75 08             	push   0x8(%ebp)
 341:	e8 ed 00 00 00       	call   433 <open>
  if(fd < 0)
 346:	83 c4 10             	add    $0x10,%esp
 349:	85 c0                	test   %eax,%eax
 34b:	78 2b                	js     378 <stat+0x48>
    return -1;
  r = fstat(fd, st);
 34d:	83 ec 08             	sub    $0x8,%esp
 350:	ff 75 0c             	push   0xc(%ebp)
 353:	89 c3                	mov    %eax,%ebx
 355:	50                   	push   %eax
 356:	e8 f0 00 00 00       	call   44b <fstat>
  close(fd);
 35b:	89 1c 24             	mov    %ebx,(%esp)
  r = fstat(fd, st);
 35e:	89 c6                	mov    %eax,%esi
  close(fd);
 360:	e8 b6 00 00 00       	call   41b <close>
  return r;
 365:	83 c4 10             	add    $0x10,%esp
}
 368:	8d 65 f8             	lea    -0x8(%ebp),%esp
 36b:	89 f0                	mov    %esi,%eax
 36d:	5b                   	pop    %ebx
 36e:	5e                   	pop    %esi
 36f:	5d                   	pop    %ebp
 370:	c3                   	ret    
 371:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
 378:	be ff ff ff ff       	mov    $0xffffffff,%esi
 37d:	eb e9                	jmp    368 <stat+0x38>
 37f:	90                   	nop

00000380 <atoi>:

int
atoi(const char *s)
{
 380:	f3 0f 1e fb          	endbr32 
 384:	55                   	push   %ebp
 385:	89 e5                	mov    %esp,%ebp
 387:	53                   	push   %ebx
 388:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 38b:	0f be 02             	movsbl (%edx),%eax
 38e:	8d 48 d0             	lea    -0x30(%eax),%ecx
 391:	80 f9 09             	cmp    $0x9,%cl
  n = 0;
 394:	b9 00 00 00 00       	mov    $0x0,%ecx
  while('0' <= *s && *s <= '9')
 399:	77 1a                	ja     3b5 <atoi+0x35>
 39b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 39f:	90                   	nop
    n = n*10 + *s++ - '0';
 3a0:	83 c2 01             	add    $0x1,%edx
 3a3:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
 3a6:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
  while('0' <= *s && *s <= '9')
 3aa:	0f be 02             	movsbl (%edx),%eax
 3ad:	8d 58 d0             	lea    -0x30(%eax),%ebx
 3b0:	80 fb 09             	cmp    $0x9,%bl
 3b3:	76 eb                	jbe    3a0 <atoi+0x20>
  return n;
}
 3b5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 3b8:	89 c8                	mov    %ecx,%eax
 3ba:	c9                   	leave  
 3bb:	c3                   	ret    
 3bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000003c0 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 3c0:	f3 0f 1e fb          	endbr32 
 3c4:	55                   	push   %ebp
 3c5:	89 e5                	mov    %esp,%ebp
 3c7:	57                   	push   %edi
 3c8:	8b 45 10             	mov    0x10(%ebp),%eax
 3cb:	8b 55 08             	mov    0x8(%ebp),%edx
 3ce:	56                   	push   %esi
 3cf:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst;
  const char *src;

  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 3d2:	85 c0                	test   %eax,%eax
 3d4:	7e 0f                	jle    3e5 <memmove+0x25>
 3d6:	01 d0                	add    %edx,%eax
  dst = vdst;
 3d8:	89 d7                	mov    %edx,%edi
 3da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *dst++ = *src++;
 3e0:	a4                   	movsb  %ds:(%esi),%es:(%edi)
  while(n-- > 0)
 3e1:	39 f8                	cmp    %edi,%eax
 3e3:	75 fb                	jne    3e0 <memmove+0x20>
  return vdst;
}
 3e5:	5e                   	pop    %esi
 3e6:	89 d0                	mov    %edx,%eax
 3e8:	5f                   	pop    %edi
 3e9:	5d                   	pop    %ebp
 3ea:	c3                   	ret    

000003eb <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 3eb:	b8 01 00 00 00       	mov    $0x1,%eax
 3f0:	cd 40                	int    $0x40
 3f2:	c3                   	ret    

000003f3 <exit>:
SYSCALL(exit)
 3f3:	b8 02 00 00 00       	mov    $0x2,%eax
 3f8:	cd 40                	int    $0x40
 3fa:	c3                   	ret    

000003fb <wait>:
SYSCALL(wait)
 3fb:	b8 03 00 00 00       	mov    $0x3,%eax
 400:	cd 40                	int    $0x40
 402:	c3                   	ret    

00000403 <pipe>:
SYSCALL(pipe)
 403:	b8 04 00 00 00       	mov    $0x4,%eax
 408:	cd 40                	int    $0x40
 40a:	c3                   	ret    

0000040b <read>:
SYSCALL(read)
 40b:	b8 05 00 00 00       	mov    $0x5,%eax
 410:	cd 40                	int    $0x40
 412:	c3                   	ret    

00000413 <write>:
SYSCALL(write)
 413:	b8 10 00 00 00       	mov    $0x10,%eax
 418:	cd 40                	int    $0x40
 41a:	c3                   	ret    

0000041b <close>:
SYSCALL(close)
 41b:	b8 15 00 00 00       	mov    $0x15,%eax
 420:	cd 40                	int    $0x40
 422:	c3                   	ret    

00000423 <kill>:
SYSCALL(kill)
 423:	b8 06 00 00 00       	mov    $0x6,%eax
 428:	cd 40                	int    $0x40
 42a:	c3                   	ret    

0000042b <exec>:
SYSCALL(exec)
 42b:	b8 07 00 00 00       	mov    $0x7,%eax
 430:	cd 40                	int    $0x40
 432:	c3                   	ret    

00000433 <open>:
SYSCALL(open)
 433:	b8 0f 00 00 00       	mov    $0xf,%eax
 438:	cd 40                	int    $0x40
 43a:	c3                   	ret    

0000043b <mknod>:
SYSCALL(mknod)
 43b:	b8 11 00 00 00       	mov    $0x11,%eax
 440:	cd 40                	int    $0x40
 442:	c3                   	ret    

00000443 <unlink>:
SYSCALL(unlink)
 443:	b8 12 00 00 00       	mov    $0x12,%eax
 448:	cd 40                	int    $0x40
 44a:	c3                   	ret    

0000044b <fstat>:
SYSCALL(fstat)
 44b:	b8 08 00 00 00       	mov    $0x8,%eax
 450:	cd 40                	int    $0x40
 452:	c3                   	ret    

00000453 <link>:
SYSCALL(link)
 453:	b8 13 00 00 00       	mov    $0x13,%eax
 458:	cd 40                	int    $0x40
 45a:	c3                   	ret    

0000045b <mkdir>:
SYSCALL(mkdir)
 45b:	b8 14 00 00 00       	mov    $0x14,%eax
 460:	cd 40                	int    $0x40
 462:	c3                   	ret    

00000463 <chdir>:
SYSCALL(chdir)
 463:	b8 09 00 00 00       	mov    $0x9,%eax
 468:	cd 40                	int    $0x40
 46a:	c3                   	ret    

0000046b <dup>:
SYSCALL(dup)
 46b:	b8 0a 00 00 00       	mov    $0xa,%eax
 470:	cd 40                	int    $0x40
 472:	c3                   	ret    

00000473 <getpid>:
SYSCALL(getpid)
 473:	b8 0b 00 00 00       	mov    $0xb,%eax
 478:	cd 40                	int    $0x40
 47a:	c3                   	ret    

0000047b <sbrk>:
SYSCALL(sbrk)
 47b:	b8 0c 00 00 00       	mov    $0xc,%eax
 480:	cd 40                	int    $0x40
 482:	c3                   	ret    

00000483 <sleep>:
SYSCALL(sleep)
 483:	b8 0d 00 00 00       	mov    $0xd,%eax
 488:	cd 40                	int    $0x40
 48a:	c3                   	ret    

0000048b <uptime>:
SYSCALL(uptime)
 48b:	b8 0e 00 00 00       	mov    $0xe,%eax
 490:	cd 40                	int    $0x40
 492:	c3                   	ret    

00000493 <poweroff>:
SYSCALL(poweroff)
 493:	b8 16 00 00 00       	mov    $0x16,%eax
 498:	cd 40                	int    $0x40
 49a:	c3                   	ret    

0000049b <calculate_sum_of_digits>:
SYSCALL(calculate_sum_of_digits)
 49b:	b8 17 00 00 00       	mov    $0x17,%eax
 4a0:	cd 40                	int    $0x40
 4a2:	c3                   	ret    

000004a3 <get_parent_pid>:
SYSCALL(get_parent_pid)
 4a3:	b8 18 00 00 00       	mov    $0x18,%eax
 4a8:	cd 40                	int    $0x40
 4aa:	c3                   	ret    

000004ab <set_process_parent>:
SYSCALL(set_process_parent)
 4ab:	b8 19 00 00 00       	mov    $0x19,%eax
 4b0:	cd 40                	int    $0x40
 4b2:	c3                   	ret    
 4b3:	66 90                	xchg   %ax,%ax
 4b5:	66 90                	xchg   %ax,%ax
 4b7:	66 90                	xchg   %ax,%ax
 4b9:	66 90                	xchg   %ax,%ax
 4bb:	66 90                	xchg   %ax,%ax
 4bd:	66 90                	xchg   %ax,%ax
 4bf:	90                   	nop

000004c0 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
 4c0:	55                   	push   %ebp
 4c1:	89 e5                	mov    %esp,%ebp
 4c3:	57                   	push   %edi
 4c4:	56                   	push   %esi
 4c5:	53                   	push   %ebx
 4c6:	83 ec 3c             	sub    $0x3c,%esp
 4c9:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
 4cc:	89 d1                	mov    %edx,%ecx
{
 4ce:	89 45 b8             	mov    %eax,-0x48(%ebp)
  if(sgn && xx < 0){
 4d1:	85 d2                	test   %edx,%edx
 4d3:	0f 89 7f 00 00 00    	jns    558 <printint+0x98>
 4d9:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
 4dd:	74 79                	je     558 <printint+0x98>
    neg = 1;
 4df:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
    x = -xx;
 4e6:	f7 d9                	neg    %ecx
  } else {
    x = xx;
  }

  i = 0;
 4e8:	31 db                	xor    %ebx,%ebx
 4ea:	8d 75 d7             	lea    -0x29(%ebp),%esi
 4ed:	8d 76 00             	lea    0x0(%esi),%esi
  do{
    buf[i++] = digits[x % base];
 4f0:	89 c8                	mov    %ecx,%eax
 4f2:	31 d2                	xor    %edx,%edx
 4f4:	89 cf                	mov    %ecx,%edi
 4f6:	f7 75 c4             	divl   -0x3c(%ebp)
 4f9:	0f b6 92 f8 09 00 00 	movzbl 0x9f8(%edx),%edx
 500:	89 45 c0             	mov    %eax,-0x40(%ebp)
 503:	89 d8                	mov    %ebx,%eax
 505:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
 508:	8b 4d c0             	mov    -0x40(%ebp),%ecx
    buf[i++] = digits[x % base];
 50b:	88 14 1e             	mov    %dl,(%esi,%ebx,1)
  }while((x /= base) != 0);
 50e:	39 7d c4             	cmp    %edi,-0x3c(%ebp)
 511:	76 dd                	jbe    4f0 <printint+0x30>
  if(neg)
 513:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 516:	85 c9                	test   %ecx,%ecx
 518:	74 0c                	je     526 <printint+0x66>
    buf[i++] = '-';
 51a:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
 51f:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
 521:	ba 2d 00 00 00       	mov    $0x2d,%edx

  while(--i >= 0)
 526:	8b 7d b8             	mov    -0x48(%ebp),%edi
 529:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
 52d:	eb 07                	jmp    536 <printint+0x76>
 52f:	90                   	nop
    putc(fd, buf[i]);
 530:	0f b6 13             	movzbl (%ebx),%edx
 533:	83 eb 01             	sub    $0x1,%ebx
  write(fd, &c, 1);
 536:	83 ec 04             	sub    $0x4,%esp
 539:	88 55 d7             	mov    %dl,-0x29(%ebp)
 53c:	6a 01                	push   $0x1
 53e:	56                   	push   %esi
 53f:	57                   	push   %edi
 540:	e8 ce fe ff ff       	call   413 <write>
  while(--i >= 0)
 545:	83 c4 10             	add    $0x10,%esp
 548:	39 de                	cmp    %ebx,%esi
 54a:	75 e4                	jne    530 <printint+0x70>
}
 54c:	8d 65 f4             	lea    -0xc(%ebp),%esp
 54f:	5b                   	pop    %ebx
 550:	5e                   	pop    %esi
 551:	5f                   	pop    %edi
 552:	5d                   	pop    %ebp
 553:	c3                   	ret    
 554:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  neg = 0;
 558:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%ebp)
 55f:	eb 87                	jmp    4e8 <printint+0x28>
 561:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 568:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 56f:	90                   	nop

00000570 <printfloat>:

// MOD-2 : Added printf for floats
void
printfloat(int fd, float xx)
{
 570:	f3 0f 1e fb          	endbr32 
 574:	55                   	push   %ebp

  int beg=(int)(xx);
	int fin=(int)(xx*100)-beg*100;
  printint(fd, beg, 10, 1);
 575:	b9 0a 00 00 00       	mov    $0xa,%ecx
{
 57a:	89 e5                	mov    %esp,%ebp
 57c:	57                   	push   %edi
 57d:	56                   	push   %esi
  write(fd, &c, 1);
 57e:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 581:	53                   	push   %ebx
 582:	83 ec 38             	sub    $0x38,%esp
 585:	d9 45 0c             	flds   0xc(%ebp)
 588:	8b 75 08             	mov    0x8(%ebp),%esi
  int beg=(int)(xx);
 58b:	d9 7d d6             	fnstcw -0x2a(%ebp)
  printint(fd, beg, 10, 1);
 58e:	6a 01                	push   $0x1
  int beg=(int)(xx);
 590:	0f b7 45 d6          	movzwl -0x2a(%ebp),%eax
 594:	80 cc 0c             	or     $0xc,%ah
 597:	66 89 45 d4          	mov    %ax,-0x2c(%ebp)
  printint(fd, beg, 10, 1);
 59b:	89 f0                	mov    %esi,%eax
  int beg=(int)(xx);
 59d:	d9 6d d4             	fldcw  -0x2c(%ebp)
 5a0:	db 55 d0             	fistl  -0x30(%ebp)
 5a3:	d9 6d d6             	fldcw  -0x2a(%ebp)
 5a6:	8b 55 d0             	mov    -0x30(%ebp),%edx
	int fin=(int)(xx*100)-beg*100;
 5a9:	d8 0d 0c 0a 00 00    	fmuls  0xa0c
 5af:	d9 6d d4             	fldcw  -0x2c(%ebp)
 5b2:	db 5d d0             	fistpl -0x30(%ebp)
 5b5:	d9 6d d6             	fldcw  -0x2a(%ebp)
 5b8:	6b da 9c             	imul   $0xffffff9c,%edx,%ebx
 5bb:	03 5d d0             	add    -0x30(%ebp),%ebx
  printint(fd, beg, 10, 1);
 5be:	e8 fd fe ff ff       	call   4c0 <printint>
  write(fd, &c, 1);
 5c3:	83 c4 0c             	add    $0xc,%esp
 5c6:	c6 45 e7 2e          	movb   $0x2e,-0x19(%ebp)
 5ca:	6a 01                	push   $0x1
 5cc:	57                   	push   %edi
 5cd:	56                   	push   %esi
 5ce:	e8 40 fe ff ff       	call   413 <write>
  putc(fd, '.');
	if(fin<10)
 5d3:	83 c4 10             	add    $0x10,%esp
 5d6:	83 fb 09             	cmp    $0x9,%ebx
 5d9:	7e 25                	jle    600 <printfloat+0x90>
    putc(fd, '0');
	printint(fd, fin, 10, 1);
 5db:	c7 45 08 01 00 00 00 	movl   $0x1,0x8(%ebp)
}
 5e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
	printint(fd, fin, 10, 1);
 5e5:	89 da                	mov    %ebx,%edx
 5e7:	89 f0                	mov    %esi,%eax
}
 5e9:	5b                   	pop    %ebx
	printint(fd, fin, 10, 1);
 5ea:	b9 0a 00 00 00       	mov    $0xa,%ecx
}
 5ef:	5e                   	pop    %esi
 5f0:	5f                   	pop    %edi
 5f1:	5d                   	pop    %ebp
	printint(fd, fin, 10, 1);
 5f2:	e9 c9 fe ff ff       	jmp    4c0 <printint>
 5f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 5fe:	66 90                	xchg   %ax,%ax
  write(fd, &c, 1);
 600:	83 ec 04             	sub    $0x4,%esp
 603:	c6 45 e7 30          	movb   $0x30,-0x19(%ebp)
 607:	6a 01                	push   $0x1
 609:	57                   	push   %edi
 60a:	56                   	push   %esi
 60b:	e8 03 fe ff ff       	call   413 <write>
    putc(fd, '0');
 610:	83 c4 10             	add    $0x10,%esp
 613:	eb c6                	jmp    5db <printfloat+0x6b>
 615:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 61c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000620 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s, %f.
void
printf(int fd, const char *fmt, ...)
{
 620:	f3 0f 1e fb          	endbr32 
 624:	55                   	push   %ebp
 625:	89 e5                	mov    %esp,%ebp
 627:	57                   	push   %edi
 628:	56                   	push   %esi
 629:	53                   	push   %ebx
 62a:	83 ec 2c             	sub    $0x2c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 62d:	8b 75 0c             	mov    0xc(%ebp),%esi
 630:	0f b6 1e             	movzbl (%esi),%ebx
 633:	84 db                	test   %bl,%bl
 635:	0f 84 bd 00 00 00    	je     6f8 <printf+0xd8>
  ap = (uint*)(void*)&fmt + 1;
 63b:	8d 45 10             	lea    0x10(%ebp),%eax
 63e:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 641:	8d 7d e7             	lea    -0x19(%ebp),%edi
  state = 0;
 644:	31 d2                	xor    %edx,%edx
  ap = (uint*)(void*)&fmt + 1;
 646:	89 45 cc             	mov    %eax,-0x34(%ebp)
 649:	eb 33                	jmp    67e <printf+0x5e>
 64b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 64f:	90                   	nop
 650:	89 55 d0             	mov    %edx,-0x30(%ebp)
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
 653:	ba 25 00 00 00       	mov    $0x25,%edx
      if(c == '%'){
 658:	83 f8 25             	cmp    $0x25,%eax
 65b:	74 17                	je     674 <printf+0x54>
  write(fd, &c, 1);
 65d:	83 ec 04             	sub    $0x4,%esp
 660:	88 5d e7             	mov    %bl,-0x19(%ebp)
 663:	6a 01                	push   $0x1
 665:	57                   	push   %edi
 666:	ff 75 08             	push   0x8(%ebp)
 669:	e8 a5 fd ff ff       	call   413 <write>
 66e:	8b 55 d0             	mov    -0x30(%ebp),%edx
      } else {
        putc(fd, c);
 671:	83 c4 10             	add    $0x10,%esp
  for(i = 0; fmt[i]; i++){
 674:	0f b6 1e             	movzbl (%esi),%ebx
 677:	83 c6 01             	add    $0x1,%esi
 67a:	84 db                	test   %bl,%bl
 67c:	74 7a                	je     6f8 <printf+0xd8>
    c = fmt[i] & 0xff;
 67e:	0f be cb             	movsbl %bl,%ecx
 681:	0f b6 c3             	movzbl %bl,%eax
    if(state == 0){
 684:	85 d2                	test   %edx,%edx
 686:	74 c8                	je     650 <printf+0x30>
      }
    } else if(state == '%'){
 688:	83 fa 25             	cmp    $0x25,%edx
 68b:	75 e7                	jne    674 <printf+0x54>
      if(c == 'd'){
 68d:	83 f8 64             	cmp    $0x64,%eax
 690:	0f 84 9a 00 00 00    	je     730 <printf+0x110>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 696:	81 e1 f7 00 00 00    	and    $0xf7,%ecx
 69c:	83 f9 70             	cmp    $0x70,%ecx
 69f:	74 5f                	je     700 <printf+0xe0>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 6a1:	83 f8 73             	cmp    $0x73,%eax
 6a4:	0f 84 d6 00 00 00    	je     780 <printf+0x160>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 6aa:	83 f8 63             	cmp    $0x63,%eax
 6ad:	0f 84 8d 00 00 00    	je     740 <printf+0x120>
        putc(fd, *ap);
        ap++;
      } else if(c == 'f'){ // MOD-2
 6b3:	83 f8 66             	cmp    $0x66,%eax
 6b6:	0f 84 f4 00 00 00    	je     7b0 <printf+0x190>
        printfloat(fd, (float)*ap);
        ap++;
      } else if(c == '%'){
 6bc:	83 f8 25             	cmp    $0x25,%eax
 6bf:	0f 84 ab 00 00 00    	je     770 <printf+0x150>
  write(fd, &c, 1);
 6c5:	83 ec 04             	sub    $0x4,%esp
 6c8:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
 6cc:	6a 01                	push   $0x1
 6ce:	57                   	push   %edi
 6cf:	ff 75 08             	push   0x8(%ebp)
 6d2:	e8 3c fd ff ff       	call   413 <write>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
        putc(fd, c);
 6d7:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 6da:	83 c4 0c             	add    $0xc,%esp
 6dd:	6a 01                	push   $0x1
  for(i = 0; fmt[i]; i++){
 6df:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 6e2:	57                   	push   %edi
 6e3:	ff 75 08             	push   0x8(%ebp)
 6e6:	e8 28 fd ff ff       	call   413 <write>
  for(i = 0; fmt[i]; i++){
 6eb:	0f b6 5e ff          	movzbl -0x1(%esi),%ebx
        putc(fd, c);
 6ef:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 6f2:	31 d2                	xor    %edx,%edx
  for(i = 0; fmt[i]; i++){
 6f4:	84 db                	test   %bl,%bl
 6f6:	75 86                	jne    67e <printf+0x5e>
    }
  }
 6f8:	8d 65 f4             	lea    -0xc(%ebp),%esp
 6fb:	5b                   	pop    %ebx
 6fc:	5e                   	pop    %esi
 6fd:	5f                   	pop    %edi
 6fe:	5d                   	pop    %ebp
 6ff:	c3                   	ret    
        printint(fd, *ap, 16, 0);
 700:	83 ec 0c             	sub    $0xc,%esp
 703:	b9 10 00 00 00       	mov    $0x10,%ecx
 708:	6a 00                	push   $0x0
 70a:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 70d:	8b 45 08             	mov    0x8(%ebp),%eax
 710:	8b 13                	mov    (%ebx),%edx
 712:	e8 a9 fd ff ff       	call   4c0 <printint>
        ap++;
 717:	89 d8                	mov    %ebx,%eax
 719:	83 c4 10             	add    $0x10,%esp
      state = 0;
 71c:	31 d2                	xor    %edx,%edx
        ap++;
 71e:	83 c0 04             	add    $0x4,%eax
 721:	89 45 cc             	mov    %eax,-0x34(%ebp)
 724:	e9 4b ff ff ff       	jmp    674 <printf+0x54>
 729:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        printint(fd, *ap, 10, 1);
 730:	83 ec 0c             	sub    $0xc,%esp
 733:	b9 0a 00 00 00       	mov    $0xa,%ecx
 738:	6a 01                	push   $0x1
 73a:	eb ce                	jmp    70a <printf+0xea>
 73c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        putc(fd, *ap);
 740:	8b 5d cc             	mov    -0x34(%ebp),%ebx
  write(fd, &c, 1);
 743:	83 ec 04             	sub    $0x4,%esp
        putc(fd, *ap);
 746:	8b 03                	mov    (%ebx),%eax
  write(fd, &c, 1);
 748:	6a 01                	push   $0x1
        ap++;
 74a:	83 c3 04             	add    $0x4,%ebx
  write(fd, &c, 1);
 74d:	57                   	push   %edi
 74e:	ff 75 08             	push   0x8(%ebp)
        putc(fd, *ap);
 751:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 754:	e8 ba fc ff ff       	call   413 <write>
        ap++;
 759:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 75c:	83 c4 10             	add    $0x10,%esp
      state = 0;
 75f:	31 d2                	xor    %edx,%edx
 761:	e9 0e ff ff ff       	jmp    674 <printf+0x54>
 766:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 76d:	8d 76 00             	lea    0x0(%esi),%esi
        putc(fd, c);
 770:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 773:	83 ec 04             	sub    $0x4,%esp
 776:	e9 62 ff ff ff       	jmp    6dd <printf+0xbd>
 77b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 77f:	90                   	nop
        s = (char*)*ap;
 780:	8b 45 cc             	mov    -0x34(%ebp),%eax
 783:	8b 18                	mov    (%eax),%ebx
        ap++;
 785:	83 c0 04             	add    $0x4,%eax
 788:	89 45 cc             	mov    %eax,-0x34(%ebp)
        if(s == 0)
 78b:	85 db                	test   %ebx,%ebx
 78d:	74 4f                	je     7de <printf+0x1be>
        while(*s != 0){
 78f:	0f b6 03             	movzbl (%ebx),%eax
      state = 0;
 792:	31 d2                	xor    %edx,%edx
        while(*s != 0){
 794:	84 c0                	test   %al,%al
 796:	0f 84 d8 fe ff ff    	je     674 <printf+0x54>
 79c:	89 75 d0             	mov    %esi,-0x30(%ebp)
 79f:	89 de                	mov    %ebx,%esi
 7a1:	8b 5d 08             	mov    0x8(%ebp),%ebx
 7a4:	eb 4a                	jmp    7f0 <printf+0x1d0>
 7a6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 7ad:	8d 76 00             	lea    0x0(%esi),%esi
        printfloat(fd, (float)*ap);
 7b0:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 7b3:	31 d2                	xor    %edx,%edx
 7b5:	83 ec 0c             	sub    $0xc,%esp
 7b8:	89 55 d4             	mov    %edx,-0x2c(%ebp)
 7bb:	8b 03                	mov    (%ebx),%eax
        ap++;
 7bd:	83 c3 04             	add    $0x4,%ebx
        printfloat(fd, (float)*ap);
 7c0:	89 45 d0             	mov    %eax,-0x30(%ebp)
 7c3:	df 6d d0             	fildll -0x30(%ebp)
 7c6:	d9 1c 24             	fstps  (%esp)
 7c9:	ff 75 08             	push   0x8(%ebp)
 7cc:	e8 9f fd ff ff       	call   570 <printfloat>
        ap++;
 7d1:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 7d4:	83 c4 10             	add    $0x10,%esp
      state = 0;
 7d7:	31 d2                	xor    %edx,%edx
 7d9:	e9 96 fe ff ff       	jmp    674 <printf+0x54>
          s = "(null)";
 7de:	bb ef 09 00 00       	mov    $0x9ef,%ebx
        while(*s != 0){
 7e3:	89 75 d0             	mov    %esi,-0x30(%ebp)
 7e6:	b8 28 00 00 00       	mov    $0x28,%eax
 7eb:	89 de                	mov    %ebx,%esi
 7ed:	8b 5d 08             	mov    0x8(%ebp),%ebx
  write(fd, &c, 1);
 7f0:	83 ec 04             	sub    $0x4,%esp
          s++;
 7f3:	83 c6 01             	add    $0x1,%esi
 7f6:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 7f9:	6a 01                	push   $0x1
 7fb:	57                   	push   %edi
 7fc:	53                   	push   %ebx
 7fd:	e8 11 fc ff ff       	call   413 <write>
        while(*s != 0){
 802:	0f b6 06             	movzbl (%esi),%eax
 805:	83 c4 10             	add    $0x10,%esp
 808:	84 c0                	test   %al,%al
 80a:	75 e4                	jne    7f0 <printf+0x1d0>
      state = 0;
 80c:	8b 75 d0             	mov    -0x30(%ebp),%esi
 80f:	31 d2                	xor    %edx,%edx
 811:	e9 5e fe ff ff       	jmp    674 <printf+0x54>
 816:	66 90                	xchg   %ax,%ax
 818:	66 90                	xchg   %ax,%ax
 81a:	66 90                	xchg   %ax,%ax
 81c:	66 90                	xchg   %ax,%ax
 81e:	66 90                	xchg   %ax,%ax

00000820 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 820:	f3 0f 1e fb          	endbr32 
 824:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 825:	a1 20 0f 00 00       	mov    0xf20,%eax
{
 82a:	89 e5                	mov    %esp,%ebp
 82c:	57                   	push   %edi
 82d:	56                   	push   %esi
 82e:	53                   	push   %ebx
 82f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = (Header*)ap - 1;
 832:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 835:	8d 76 00             	lea    0x0(%esi),%esi
 838:	89 c2                	mov    %eax,%edx
 83a:	8b 00                	mov    (%eax),%eax
 83c:	39 ca                	cmp    %ecx,%edx
 83e:	73 30                	jae    870 <free+0x50>
 840:	39 c1                	cmp    %eax,%ecx
 842:	72 04                	jb     848 <free+0x28>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 844:	39 c2                	cmp    %eax,%edx
 846:	72 f0                	jb     838 <free+0x18>
      break;
  if(bp + bp->s.size == p->s.ptr){
 848:	8b 73 fc             	mov    -0x4(%ebx),%esi
 84b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 84e:	39 f8                	cmp    %edi,%eax
 850:	74 30                	je     882 <free+0x62>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 852:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 855:	8b 42 04             	mov    0x4(%edx),%eax
 858:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 85b:	39 f1                	cmp    %esi,%ecx
 85d:	74 3a                	je     899 <free+0x79>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 85f:	89 0a                	mov    %ecx,(%edx)
  freep = p;
}
 861:	5b                   	pop    %ebx
  freep = p;
 862:	89 15 20 0f 00 00    	mov    %edx,0xf20
}
 868:	5e                   	pop    %esi
 869:	5f                   	pop    %edi
 86a:	5d                   	pop    %ebp
 86b:	c3                   	ret    
 86c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 870:	39 c2                	cmp    %eax,%edx
 872:	72 c4                	jb     838 <free+0x18>
 874:	39 c1                	cmp    %eax,%ecx
 876:	73 c0                	jae    838 <free+0x18>
  if(bp + bp->s.size == p->s.ptr){
 878:	8b 73 fc             	mov    -0x4(%ebx),%esi
 87b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 87e:	39 f8                	cmp    %edi,%eax
 880:	75 d0                	jne    852 <free+0x32>
    bp->s.size += p->s.ptr->s.size;
 882:	03 70 04             	add    0x4(%eax),%esi
 885:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 888:	8b 02                	mov    (%edx),%eax
 88a:	8b 00                	mov    (%eax),%eax
 88c:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 88f:	8b 42 04             	mov    0x4(%edx),%eax
 892:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 895:	39 f1                	cmp    %esi,%ecx
 897:	75 c6                	jne    85f <free+0x3f>
    p->s.size += bp->s.size;
 899:	03 43 fc             	add    -0x4(%ebx),%eax
  freep = p;
 89c:	89 15 20 0f 00 00    	mov    %edx,0xf20
    p->s.size += bp->s.size;
 8a2:	89 42 04             	mov    %eax,0x4(%edx)
    p->s.ptr = bp->s.ptr;
 8a5:	8b 43 f8             	mov    -0x8(%ebx),%eax
 8a8:	89 02                	mov    %eax,(%edx)
}
 8aa:	5b                   	pop    %ebx
 8ab:	5e                   	pop    %esi
 8ac:	5f                   	pop    %edi
 8ad:	5d                   	pop    %ebp
 8ae:	c3                   	ret    
 8af:	90                   	nop

000008b0 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
 8b0:	f3 0f 1e fb          	endbr32 
 8b4:	55                   	push   %ebp
 8b5:	89 e5                	mov    %esp,%ebp
 8b7:	57                   	push   %edi
 8b8:	56                   	push   %esi
 8b9:	53                   	push   %ebx
 8ba:	83 ec 1c             	sub    $0x1c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 8bd:	8b 45 08             	mov    0x8(%ebp),%eax
  if((prevp = freep) == 0){
 8c0:	8b 3d 20 0f 00 00    	mov    0xf20,%edi
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 8c6:	8d 70 07             	lea    0x7(%eax),%esi
 8c9:	c1 ee 03             	shr    $0x3,%esi
 8cc:	83 c6 01             	add    $0x1,%esi
  if((prevp = freep) == 0){
 8cf:	85 ff                	test   %edi,%edi
 8d1:	0f 84 a9 00 00 00    	je     980 <malloc+0xd0>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8d7:	8b 07                	mov    (%edi),%eax
    if(p->s.size >= nunits){
 8d9:	8b 48 04             	mov    0x4(%eax),%ecx
 8dc:	39 f1                	cmp    %esi,%ecx
 8de:	73 6d                	jae    94d <malloc+0x9d>
 8e0:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
 8e6:	bb 00 10 00 00       	mov    $0x1000,%ebx
 8eb:	0f 43 de             	cmovae %esi,%ebx
  p = sbrk(nu * sizeof(Header));
 8ee:	8d 0c dd 00 00 00 00 	lea    0x0(,%ebx,8),%ecx
 8f5:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
 8f8:	eb 17                	jmp    911 <malloc+0x61>
 8fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 900:	8b 10                	mov    (%eax),%edx
    if(p->s.size >= nunits){
 902:	8b 4a 04             	mov    0x4(%edx),%ecx
 905:	39 f1                	cmp    %esi,%ecx
 907:	73 4f                	jae    958 <malloc+0xa8>
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
 909:	8b 3d 20 0f 00 00    	mov    0xf20,%edi
 90f:	89 d0                	mov    %edx,%eax
 911:	39 c7                	cmp    %eax,%edi
 913:	75 eb                	jne    900 <malloc+0x50>
  p = sbrk(nu * sizeof(Header));
 915:	83 ec 0c             	sub    $0xc,%esp
 918:	ff 75 e4             	push   -0x1c(%ebp)
 91b:	e8 5b fb ff ff       	call   47b <sbrk>
  if(p == (char*)-1)
 920:	83 c4 10             	add    $0x10,%esp
 923:	83 f8 ff             	cmp    $0xffffffff,%eax
 926:	74 1b                	je     943 <malloc+0x93>
  hp->s.size = nu;
 928:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 92b:	83 ec 0c             	sub    $0xc,%esp
 92e:	83 c0 08             	add    $0x8,%eax
 931:	50                   	push   %eax
 932:	e8 e9 fe ff ff       	call   820 <free>
  return freep;
 937:	a1 20 0f 00 00       	mov    0xf20,%eax
      if((p = morecore(nunits)) == 0)
 93c:	83 c4 10             	add    $0x10,%esp
 93f:	85 c0                	test   %eax,%eax
 941:	75 bd                	jne    900 <malloc+0x50>
        return 0;
  }
}
 943:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return 0;
 946:	31 c0                	xor    %eax,%eax
}
 948:	5b                   	pop    %ebx
 949:	5e                   	pop    %esi
 94a:	5f                   	pop    %edi
 94b:	5d                   	pop    %ebp
 94c:	c3                   	ret    
    if(p->s.size >= nunits){
 94d:	89 c2                	mov    %eax,%edx
 94f:	89 f8                	mov    %edi,%eax
 951:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->s.size == nunits)
 958:	39 ce                	cmp    %ecx,%esi
 95a:	74 54                	je     9b0 <malloc+0x100>
        p->s.size -= nunits;
 95c:	29 f1                	sub    %esi,%ecx
 95e:	89 4a 04             	mov    %ecx,0x4(%edx)
        p += p->s.size;
 961:	8d 14 ca             	lea    (%edx,%ecx,8),%edx
        p->s.size = nunits;
 964:	89 72 04             	mov    %esi,0x4(%edx)
      freep = prevp;
 967:	a3 20 0f 00 00       	mov    %eax,0xf20
}
 96c:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return (void*)(p + 1);
 96f:	8d 42 08             	lea    0x8(%edx),%eax
}
 972:	5b                   	pop    %ebx
 973:	5e                   	pop    %esi
 974:	5f                   	pop    %edi
 975:	5d                   	pop    %ebp
 976:	c3                   	ret    
 977:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 97e:	66 90                	xchg   %ax,%ax
    base.s.ptr = freep = prevp = &base;
 980:	c7 05 20 0f 00 00 24 	movl   $0xf24,0xf20
 987:	0f 00 00 
    base.s.size = 0;
 98a:	bf 24 0f 00 00       	mov    $0xf24,%edi
    base.s.ptr = freep = prevp = &base;
 98f:	c7 05 24 0f 00 00 24 	movl   $0xf24,0xf24
 996:	0f 00 00 
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 999:	89 f8                	mov    %edi,%eax
    base.s.size = 0;
 99b:	c7 05 28 0f 00 00 00 	movl   $0x0,0xf28
 9a2:	00 00 00 
    if(p->s.size >= nunits){
 9a5:	e9 36 ff ff ff       	jmp    8e0 <malloc+0x30>
 9aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        prevp->s.ptr = p->s.ptr;
 9b0:	8b 0a                	mov    (%edx),%ecx
 9b2:	89 08                	mov    %ecx,(%eax)
 9b4:	eb b1                	jmp    967 <malloc+0xb7>
