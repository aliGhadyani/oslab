
_get_file_sectors:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define FILE_ADDR_SIZE 512
int main(int argc, char *argv[]) {
   0:	f3 0f 1e fb          	endbr32 
   4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   8:	83 e4 f0             	and    $0xfffffff0,%esp
   b:	ff 71 fc             	push   -0x4(%ecx)
   e:	55                   	push   %ebp
   f:	89 e5                	mov    %esp,%ebp
  11:	53                   	push   %ebx
  12:	51                   	push   %ecx
  13:	81 ec 00 0a 00 00    	sub    $0xa00,%esp
    char addr[FILE_ADDR_SIZE];
    int addr_sectrs[FILE_ADDR_SIZE];
    int fd;
    int resp;
    if(argc < 2) {
  19:	83 39 01             	cmpl   $0x1,(%ecx)
int main(int argc, char *argv[]) {
  1c:	8b 41 04             	mov    0x4(%ecx),%eax
    if(argc < 2) {
  1f:	0f 8e 93 00 00 00    	jle    b8 <main+0xb8>
        if(resp < 0) {
            printf(1, "file address recieve failed!\n");
            exit();
        }
    } else {
        strcpy(addr, argv[1]);
  25:	8d 9d f8 f5 ff ff    	lea    -0xa08(%ebp),%ebx
  2b:	52                   	push   %edx
  2c:	52                   	push   %edx
  2d:	ff 70 04             	push   0x4(%eax)
  30:	53                   	push   %ebx
  31:	e8 ca 00 00 00       	call   100 <strcpy>
  36:	83 c4 10             	add    $0x10,%esp
    }
    fd = open(addr, O_RDONLY);
  39:	83 ec 08             	sub    $0x8,%esp
  3c:	6a 00                	push   $0x0
  3e:	53                   	push   %ebx
  3f:	e8 5f 03 00 00       	call   3a3 <open>
    if(fd < 0) {
  44:	83 c4 10             	add    $0x10,%esp
  47:	85 c0                	test   %eax,%eax
  49:	0f 88 9b 00 00 00    	js     ea <main+0xea>
        printf(1, "openning file failed!\n");
        exit();
    }
    get_file_sectors(fd, addr_sectrs, FILE_ADDR_SIZE);
  4f:	52                   	push   %edx
  50:	8d 95 f8 f7 ff ff    	lea    -0x808(%ebp),%edx
  56:	68 00 02 00 00       	push   $0x200
  5b:	52                   	push   %edx
  5c:	50                   	push   %eax
  5d:	e8 b9 03 00 00       	call   41b <get_file_sectors>
    resp = 0;
    printf(1, "file sectors sequence will come in following:\n");
  62:	59                   	pop    %ecx
  63:	5b                   	pop    %ebx
  64:	68 84 08 00 00       	push   $0x884
  69:	6a 01                	push   $0x1
  6b:	e8 70 04 00 00       	call   4e0 <printf>
    while (addr_sectrs[resp] >= 0) {
  70:	8b 85 f8 f7 ff ff    	mov    -0x808(%ebp),%eax
  76:	83 c4 10             	add    $0x10,%esp
  79:	85 c0                	test   %eax,%eax
  7b:	78 28                	js     a5 <main+0xa5>
  7d:	8d 9d fc f7 ff ff    	lea    -0x804(%ebp),%ebx
  83:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  87:	90                   	nop
        printf(1, "%d ", addr_sectrs[resp++]);
  88:	83 ec 04             	sub    $0x4,%esp
    while (addr_sectrs[resp] >= 0) {
  8b:	83 c3 04             	add    $0x4,%ebx
        printf(1, "%d ", addr_sectrs[resp++]);
  8e:	50                   	push   %eax
  8f:	68 7d 08 00 00       	push   $0x87d
  94:	6a 01                	push   $0x1
  96:	e8 45 04 00 00       	call   4e0 <printf>
    while (addr_sectrs[resp] >= 0) {
  9b:	8b 43 fc             	mov    -0x4(%ebx),%eax
  9e:	83 c4 10             	add    $0x10,%esp
  a1:	85 c0                	test   %eax,%eax
  a3:	79 e3                	jns    88 <main+0x88>
    }
    printf(1, "\n");
  a5:	50                   	push   %eax
  a6:	50                   	push   %eax
  a7:	68 7b 08 00 00       	push   $0x87b
  ac:	6a 01                	push   $0x1
  ae:	e8 2d 04 00 00       	call   4e0 <printf>
    exit();
  b3:	e8 ab 02 00 00       	call   363 <exit>
        resp = read(0, addr, FILE_ADDR_SIZE);
  b8:	53                   	push   %ebx
  b9:	8d 9d f8 f5 ff ff    	lea    -0xa08(%ebp),%ebx
  bf:	68 00 02 00 00       	push   $0x200
  c4:	53                   	push   %ebx
  c5:	6a 00                	push   $0x0
  c7:	e8 af 02 00 00       	call   37b <read>
        if(resp < 0) {
  cc:	83 c4 10             	add    $0x10,%esp
  cf:	85 c0                	test   %eax,%eax
  d1:	0f 89 62 ff ff ff    	jns    39 <main+0x39>
            printf(1, "file address recieve failed!\n");
  d7:	51                   	push   %ecx
  d8:	51                   	push   %ecx
  d9:	68 48 08 00 00       	push   $0x848
  de:	6a 01                	push   $0x1
  e0:	e8 fb 03 00 00       	call   4e0 <printf>
            exit();
  e5:	e8 79 02 00 00       	call   363 <exit>
        printf(1, "openning file failed!\n");
  ea:	50                   	push   %eax
  eb:	50                   	push   %eax
  ec:	68 66 08 00 00       	push   $0x866
  f1:	6a 01                	push   $0x1
  f3:	e8 e8 03 00 00       	call   4e0 <printf>
        exit();
  f8:	e8 66 02 00 00       	call   363 <exit>
  fd:	66 90                	xchg   %ax,%ax
  ff:	90                   	nop

00000100 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
 100:	f3 0f 1e fb          	endbr32 
 104:	55                   	push   %ebp
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
 105:	31 c0                	xor    %eax,%eax
{
 107:	89 e5                	mov    %esp,%ebp
 109:	53                   	push   %ebx
 10a:	8b 4d 08             	mov    0x8(%ebp),%ecx
 10d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while((*s++ = *t++) != 0)
 110:	0f b6 14 03          	movzbl (%ebx,%eax,1),%edx
 114:	88 14 01             	mov    %dl,(%ecx,%eax,1)
 117:	83 c0 01             	add    $0x1,%eax
 11a:	84 d2                	test   %dl,%dl
 11c:	75 f2                	jne    110 <strcpy+0x10>
    ;
  return os;
}
 11e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 121:	89 c8                	mov    %ecx,%eax
 123:	c9                   	leave  
 124:	c3                   	ret    
 125:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 12c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000130 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 130:	f3 0f 1e fb          	endbr32 
 134:	55                   	push   %ebp
 135:	89 e5                	mov    %esp,%ebp
 137:	53                   	push   %ebx
 138:	8b 4d 08             	mov    0x8(%ebp),%ecx
 13b:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
 13e:	0f b6 01             	movzbl (%ecx),%eax
 141:	0f b6 1a             	movzbl (%edx),%ebx
 144:	84 c0                	test   %al,%al
 146:	75 18                	jne    160 <strcmp+0x30>
 148:	eb 2a                	jmp    174 <strcmp+0x44>
 14a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
 150:	0f b6 41 01          	movzbl 0x1(%ecx),%eax
    p++, q++;
 154:	83 c1 01             	add    $0x1,%ecx
 157:	8d 5a 01             	lea    0x1(%edx),%ebx
  while(*p && *p == *q)
 15a:	84 c0                	test   %al,%al
 15c:	74 12                	je     170 <strcmp+0x40>
    p++, q++;
 15e:	89 da                	mov    %ebx,%edx
  while(*p && *p == *q)
 160:	0f b6 1a             	movzbl (%edx),%ebx
 163:	38 c3                	cmp    %al,%bl
 165:	74 e9                	je     150 <strcmp+0x20>
  return (uchar)*p - (uchar)*q;
 167:	29 d8                	sub    %ebx,%eax
}
 169:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 16c:	c9                   	leave  
 16d:	c3                   	ret    
 16e:	66 90                	xchg   %ax,%ax
  return (uchar)*p - (uchar)*q;
 170:	0f b6 5a 01          	movzbl 0x1(%edx),%ebx
 174:	31 c0                	xor    %eax,%eax
 176:	29 d8                	sub    %ebx,%eax
}
 178:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 17b:	c9                   	leave  
 17c:	c3                   	ret    
 17d:	8d 76 00             	lea    0x0(%esi),%esi

00000180 <strlen>:

uint
strlen(const char *s)
{
 180:	f3 0f 1e fb          	endbr32 
 184:	55                   	push   %ebp
 185:	89 e5                	mov    %esp,%ebp
 187:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
 18a:	80 3a 00             	cmpb   $0x0,(%edx)
 18d:	74 21                	je     1b0 <strlen+0x30>
 18f:	31 c0                	xor    %eax,%eax
 191:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 198:	83 c0 01             	add    $0x1,%eax
 19b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
 19f:	89 c1                	mov    %eax,%ecx
 1a1:	75 f5                	jne    198 <strlen+0x18>
    ;
  return n;
}
 1a3:	89 c8                	mov    %ecx,%eax
 1a5:	5d                   	pop    %ebp
 1a6:	c3                   	ret    
 1a7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 1ae:	66 90                	xchg   %ax,%ax
  for(n = 0; s[n]; n++)
 1b0:	31 c9                	xor    %ecx,%ecx
}
 1b2:	5d                   	pop    %ebp
 1b3:	89 c8                	mov    %ecx,%eax
 1b5:	c3                   	ret    
 1b6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 1bd:	8d 76 00             	lea    0x0(%esi),%esi

000001c0 <memset>:

void*
memset(void *dst, int c, uint n)
{
 1c0:	f3 0f 1e fb          	endbr32 
 1c4:	55                   	push   %ebp
 1c5:	89 e5                	mov    %esp,%ebp
 1c7:	57                   	push   %edi
 1c8:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 1cb:	8b 4d 10             	mov    0x10(%ebp),%ecx
 1ce:	8b 45 0c             	mov    0xc(%ebp),%eax
 1d1:	89 d7                	mov    %edx,%edi
 1d3:	fc                   	cld    
 1d4:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 1d6:	8b 7d fc             	mov    -0x4(%ebp),%edi
 1d9:	89 d0                	mov    %edx,%eax
 1db:	c9                   	leave  
 1dc:	c3                   	ret    
 1dd:	8d 76 00             	lea    0x0(%esi),%esi

000001e0 <strchr>:

char*
strchr(const char *s, char c)
{
 1e0:	f3 0f 1e fb          	endbr32 
 1e4:	55                   	push   %ebp
 1e5:	89 e5                	mov    %esp,%ebp
 1e7:	8b 45 08             	mov    0x8(%ebp),%eax
 1ea:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 1ee:	0f b6 10             	movzbl (%eax),%edx
 1f1:	84 d2                	test   %dl,%dl
 1f3:	75 16                	jne    20b <strchr+0x2b>
 1f5:	eb 21                	jmp    218 <strchr+0x38>
 1f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 1fe:	66 90                	xchg   %ax,%ax
 200:	0f b6 50 01          	movzbl 0x1(%eax),%edx
 204:	83 c0 01             	add    $0x1,%eax
 207:	84 d2                	test   %dl,%dl
 209:	74 0d                	je     218 <strchr+0x38>
    if(*s == c)
 20b:	38 d1                	cmp    %dl,%cl
 20d:	75 f1                	jne    200 <strchr+0x20>
      return (char*)s;
  return 0;
}
 20f:	5d                   	pop    %ebp
 210:	c3                   	ret    
 211:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return 0;
 218:	31 c0                	xor    %eax,%eax
}
 21a:	5d                   	pop    %ebp
 21b:	c3                   	ret    
 21c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000220 <gets>:

char*
gets(char *buf, int max)
{
 220:	f3 0f 1e fb          	endbr32 
 224:	55                   	push   %ebp
 225:	89 e5                	mov    %esp,%ebp
 227:	57                   	push   %edi
 228:	56                   	push   %esi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    cc = read(0, &c, 1);
 229:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 22c:	53                   	push   %ebx
  for(i=0; i+1 < max; ){
 22d:	31 db                	xor    %ebx,%ebx
{
 22f:	83 ec 1c             	sub    $0x1c,%esp
  for(i=0; i+1 < max; ){
 232:	eb 2b                	jmp    25f <gets+0x3f>
 234:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cc = read(0, &c, 1);
 238:	83 ec 04             	sub    $0x4,%esp
 23b:	6a 01                	push   $0x1
 23d:	57                   	push   %edi
 23e:	6a 00                	push   $0x0
 240:	e8 36 01 00 00       	call   37b <read>
    if(cc < 1)
 245:	83 c4 10             	add    $0x10,%esp
 248:	85 c0                	test   %eax,%eax
 24a:	7e 1d                	jle    269 <gets+0x49>
      break;
    buf[i++] = c;
 24c:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 250:	8b 55 08             	mov    0x8(%ebp),%edx
 253:	88 44 1a ff          	mov    %al,-0x1(%edx,%ebx,1)
    if(c == '\n' || c == '\r')
 257:	3c 0a                	cmp    $0xa,%al
 259:	74 25                	je     280 <gets+0x60>
 25b:	3c 0d                	cmp    $0xd,%al
 25d:	74 21                	je     280 <gets+0x60>
  for(i=0; i+1 < max; ){
 25f:	89 de                	mov    %ebx,%esi
 261:	83 c3 01             	add    $0x1,%ebx
 264:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
 267:	7c cf                	jl     238 <gets+0x18>
      break;
  }
  buf[i] = '\0';
 269:	8b 45 08             	mov    0x8(%ebp),%eax
 26c:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
  return buf;
}
 270:	8d 65 f4             	lea    -0xc(%ebp),%esp
 273:	5b                   	pop    %ebx
 274:	5e                   	pop    %esi
 275:	5f                   	pop    %edi
 276:	5d                   	pop    %ebp
 277:	c3                   	ret    
 278:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 27f:	90                   	nop
  buf[i] = '\0';
 280:	8b 45 08             	mov    0x8(%ebp),%eax
 283:	89 de                	mov    %ebx,%esi
 285:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
}
 289:	8d 65 f4             	lea    -0xc(%ebp),%esp
 28c:	5b                   	pop    %ebx
 28d:	5e                   	pop    %esi
 28e:	5f                   	pop    %edi
 28f:	5d                   	pop    %ebp
 290:	c3                   	ret    
 291:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 298:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 29f:	90                   	nop

000002a0 <stat>:

int
stat(const char *n, struct stat *st)
{
 2a0:	f3 0f 1e fb          	endbr32 
 2a4:	55                   	push   %ebp
 2a5:	89 e5                	mov    %esp,%ebp
 2a7:	56                   	push   %esi
 2a8:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 2a9:	83 ec 08             	sub    $0x8,%esp
 2ac:	6a 00                	push   $0x0
 2ae:	ff 75 08             	push   0x8(%ebp)
 2b1:	e8 ed 00 00 00       	call   3a3 <open>
  if(fd < 0)
 2b6:	83 c4 10             	add    $0x10,%esp
 2b9:	85 c0                	test   %eax,%eax
 2bb:	78 2b                	js     2e8 <stat+0x48>
    return -1;
  r = fstat(fd, st);
 2bd:	83 ec 08             	sub    $0x8,%esp
 2c0:	ff 75 0c             	push   0xc(%ebp)
 2c3:	89 c3                	mov    %eax,%ebx
 2c5:	50                   	push   %eax
 2c6:	e8 f0 00 00 00       	call   3bb <fstat>
  close(fd);
 2cb:	89 1c 24             	mov    %ebx,(%esp)
  r = fstat(fd, st);
 2ce:	89 c6                	mov    %eax,%esi
  close(fd);
 2d0:	e8 b6 00 00 00       	call   38b <close>
  return r;
 2d5:	83 c4 10             	add    $0x10,%esp
}
 2d8:	8d 65 f8             	lea    -0x8(%ebp),%esp
 2db:	89 f0                	mov    %esi,%eax
 2dd:	5b                   	pop    %ebx
 2de:	5e                   	pop    %esi
 2df:	5d                   	pop    %ebp
 2e0:	c3                   	ret    
 2e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
 2e8:	be ff ff ff ff       	mov    $0xffffffff,%esi
 2ed:	eb e9                	jmp    2d8 <stat+0x38>
 2ef:	90                   	nop

000002f0 <atoi>:

int
atoi(const char *s)
{
 2f0:	f3 0f 1e fb          	endbr32 
 2f4:	55                   	push   %ebp
 2f5:	89 e5                	mov    %esp,%ebp
 2f7:	53                   	push   %ebx
 2f8:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 2fb:	0f be 02             	movsbl (%edx),%eax
 2fe:	8d 48 d0             	lea    -0x30(%eax),%ecx
 301:	80 f9 09             	cmp    $0x9,%cl
  n = 0;
 304:	b9 00 00 00 00       	mov    $0x0,%ecx
  while('0' <= *s && *s <= '9')
 309:	77 1a                	ja     325 <atoi+0x35>
 30b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 30f:	90                   	nop
    n = n*10 + *s++ - '0';
 310:	83 c2 01             	add    $0x1,%edx
 313:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
 316:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
  while('0' <= *s && *s <= '9')
 31a:	0f be 02             	movsbl (%edx),%eax
 31d:	8d 58 d0             	lea    -0x30(%eax),%ebx
 320:	80 fb 09             	cmp    $0x9,%bl
 323:	76 eb                	jbe    310 <atoi+0x20>
  return n;
}
 325:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 328:	89 c8                	mov    %ecx,%eax
 32a:	c9                   	leave  
 32b:	c3                   	ret    
 32c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000330 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 330:	f3 0f 1e fb          	endbr32 
 334:	55                   	push   %ebp
 335:	89 e5                	mov    %esp,%ebp
 337:	57                   	push   %edi
 338:	8b 45 10             	mov    0x10(%ebp),%eax
 33b:	8b 55 08             	mov    0x8(%ebp),%edx
 33e:	56                   	push   %esi
 33f:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst;
  const char *src;

  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 342:	85 c0                	test   %eax,%eax
 344:	7e 0f                	jle    355 <memmove+0x25>
 346:	01 d0                	add    %edx,%eax
  dst = vdst;
 348:	89 d7                	mov    %edx,%edi
 34a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *dst++ = *src++;
 350:	a4                   	movsb  %ds:(%esi),%es:(%edi)
  while(n-- > 0)
 351:	39 f8                	cmp    %edi,%eax
 353:	75 fb                	jne    350 <memmove+0x20>
  return vdst;
}
 355:	5e                   	pop    %esi
 356:	89 d0                	mov    %edx,%eax
 358:	5f                   	pop    %edi
 359:	5d                   	pop    %ebp
 35a:	c3                   	ret    

0000035b <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 35b:	b8 01 00 00 00       	mov    $0x1,%eax
 360:	cd 40                	int    $0x40
 362:	c3                   	ret    

00000363 <exit>:
SYSCALL(exit)
 363:	b8 02 00 00 00       	mov    $0x2,%eax
 368:	cd 40                	int    $0x40
 36a:	c3                   	ret    

0000036b <wait>:
SYSCALL(wait)
 36b:	b8 03 00 00 00       	mov    $0x3,%eax
 370:	cd 40                	int    $0x40
 372:	c3                   	ret    

00000373 <pipe>:
SYSCALL(pipe)
 373:	b8 04 00 00 00       	mov    $0x4,%eax
 378:	cd 40                	int    $0x40
 37a:	c3                   	ret    

0000037b <read>:
SYSCALL(read)
 37b:	b8 05 00 00 00       	mov    $0x5,%eax
 380:	cd 40                	int    $0x40
 382:	c3                   	ret    

00000383 <write>:
SYSCALL(write)
 383:	b8 10 00 00 00       	mov    $0x10,%eax
 388:	cd 40                	int    $0x40
 38a:	c3                   	ret    

0000038b <close>:
SYSCALL(close)
 38b:	b8 15 00 00 00       	mov    $0x15,%eax
 390:	cd 40                	int    $0x40
 392:	c3                   	ret    

00000393 <kill>:
SYSCALL(kill)
 393:	b8 06 00 00 00       	mov    $0x6,%eax
 398:	cd 40                	int    $0x40
 39a:	c3                   	ret    

0000039b <exec>:
SYSCALL(exec)
 39b:	b8 07 00 00 00       	mov    $0x7,%eax
 3a0:	cd 40                	int    $0x40
 3a2:	c3                   	ret    

000003a3 <open>:
SYSCALL(open)
 3a3:	b8 0f 00 00 00       	mov    $0xf,%eax
 3a8:	cd 40                	int    $0x40
 3aa:	c3                   	ret    

000003ab <mknod>:
SYSCALL(mknod)
 3ab:	b8 11 00 00 00       	mov    $0x11,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    

000003b3 <unlink>:
SYSCALL(unlink)
 3b3:	b8 12 00 00 00       	mov    $0x12,%eax
 3b8:	cd 40                	int    $0x40
 3ba:	c3                   	ret    

000003bb <fstat>:
SYSCALL(fstat)
 3bb:	b8 08 00 00 00       	mov    $0x8,%eax
 3c0:	cd 40                	int    $0x40
 3c2:	c3                   	ret    

000003c3 <link>:
SYSCALL(link)
 3c3:	b8 13 00 00 00       	mov    $0x13,%eax
 3c8:	cd 40                	int    $0x40
 3ca:	c3                   	ret    

000003cb <mkdir>:
SYSCALL(mkdir)
 3cb:	b8 14 00 00 00       	mov    $0x14,%eax
 3d0:	cd 40                	int    $0x40
 3d2:	c3                   	ret    

000003d3 <chdir>:
SYSCALL(chdir)
 3d3:	b8 09 00 00 00       	mov    $0x9,%eax
 3d8:	cd 40                	int    $0x40
 3da:	c3                   	ret    

000003db <dup>:
SYSCALL(dup)
 3db:	b8 0a 00 00 00       	mov    $0xa,%eax
 3e0:	cd 40                	int    $0x40
 3e2:	c3                   	ret    

000003e3 <getpid>:
SYSCALL(getpid)
 3e3:	b8 0b 00 00 00       	mov    $0xb,%eax
 3e8:	cd 40                	int    $0x40
 3ea:	c3                   	ret    

000003eb <sbrk>:
SYSCALL(sbrk)
 3eb:	b8 0c 00 00 00       	mov    $0xc,%eax
 3f0:	cd 40                	int    $0x40
 3f2:	c3                   	ret    

000003f3 <sleep>:
SYSCALL(sleep)
 3f3:	b8 0d 00 00 00       	mov    $0xd,%eax
 3f8:	cd 40                	int    $0x40
 3fa:	c3                   	ret    

000003fb <uptime>:
SYSCALL(uptime)
 3fb:	b8 0e 00 00 00       	mov    $0xe,%eax
 400:	cd 40                	int    $0x40
 402:	c3                   	ret    

00000403 <poweroff>:
SYSCALL(poweroff)
 403:	b8 16 00 00 00       	mov    $0x16,%eax
 408:	cd 40                	int    $0x40
 40a:	c3                   	ret    

0000040b <calculate_sum_of_digits>:
SYSCALL(calculate_sum_of_digits)
 40b:	b8 17 00 00 00       	mov    $0x17,%eax
 410:	cd 40                	int    $0x40
 412:	c3                   	ret    

00000413 <get_parent_pid>:
SYSCALL(get_parent_pid)
 413:	b8 18 00 00 00       	mov    $0x18,%eax
 418:	cd 40                	int    $0x40
 41a:	c3                   	ret    

0000041b <get_file_sectors>:
SYSCALL(get_file_sectors)
 41b:	b8 19 00 00 00       	mov    $0x19,%eax
 420:	cd 40                	int    $0x40
 422:	c3                   	ret    
 423:	66 90                	xchg   %ax,%ax
 425:	66 90                	xchg   %ax,%ax
 427:	66 90                	xchg   %ax,%ax
 429:	66 90                	xchg   %ax,%ax
 42b:	66 90                	xchg   %ax,%ax
 42d:	66 90                	xchg   %ax,%ax
 42f:	90                   	nop

00000430 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
 430:	55                   	push   %ebp
 431:	89 e5                	mov    %esp,%ebp
 433:	57                   	push   %edi
 434:	56                   	push   %esi
 435:	53                   	push   %ebx
 436:	83 ec 3c             	sub    $0x3c,%esp
 439:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
 43c:	89 d1                	mov    %edx,%ecx
{
 43e:	89 45 b8             	mov    %eax,-0x48(%ebp)
  if(sgn && xx < 0){
 441:	85 d2                	test   %edx,%edx
 443:	0f 89 7f 00 00 00    	jns    4c8 <printint+0x98>
 449:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
 44d:	74 79                	je     4c8 <printint+0x98>
    neg = 1;
 44f:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
    x = -xx;
 456:	f7 d9                	neg    %ecx
  } else {
    x = xx;
  }

  i = 0;
 458:	31 db                	xor    %ebx,%ebx
 45a:	8d 75 d7             	lea    -0x29(%ebp),%esi
 45d:	8d 76 00             	lea    0x0(%esi),%esi
  do{
    buf[i++] = digits[x % base];
 460:	89 c8                	mov    %ecx,%eax
 462:	31 d2                	xor    %edx,%edx
 464:	89 cf                	mov    %ecx,%edi
 466:	f7 75 c4             	divl   -0x3c(%ebp)
 469:	0f b6 92 bc 08 00 00 	movzbl 0x8bc(%edx),%edx
 470:	89 45 c0             	mov    %eax,-0x40(%ebp)
 473:	89 d8                	mov    %ebx,%eax
 475:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
 478:	8b 4d c0             	mov    -0x40(%ebp),%ecx
    buf[i++] = digits[x % base];
 47b:	88 14 1e             	mov    %dl,(%esi,%ebx,1)
  }while((x /= base) != 0);
 47e:	39 7d c4             	cmp    %edi,-0x3c(%ebp)
 481:	76 dd                	jbe    460 <printint+0x30>
  if(neg)
 483:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 486:	85 c9                	test   %ecx,%ecx
 488:	74 0c                	je     496 <printint+0x66>
    buf[i++] = '-';
 48a:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
 48f:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
 491:	ba 2d 00 00 00       	mov    $0x2d,%edx

  while(--i >= 0)
 496:	8b 7d b8             	mov    -0x48(%ebp),%edi
 499:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
 49d:	eb 07                	jmp    4a6 <printint+0x76>
 49f:	90                   	nop
    putc(fd, buf[i]);
 4a0:	0f b6 13             	movzbl (%ebx),%edx
 4a3:	83 eb 01             	sub    $0x1,%ebx
  write(fd, &c, 1);
 4a6:	83 ec 04             	sub    $0x4,%esp
 4a9:	88 55 d7             	mov    %dl,-0x29(%ebp)
 4ac:	6a 01                	push   $0x1
 4ae:	56                   	push   %esi
 4af:	57                   	push   %edi
 4b0:	e8 ce fe ff ff       	call   383 <write>
  while(--i >= 0)
 4b5:	83 c4 10             	add    $0x10,%esp
 4b8:	39 de                	cmp    %ebx,%esi
 4ba:	75 e4                	jne    4a0 <printint+0x70>
}
 4bc:	8d 65 f4             	lea    -0xc(%ebp),%esp
 4bf:	5b                   	pop    %ebx
 4c0:	5e                   	pop    %esi
 4c1:	5f                   	pop    %edi
 4c2:	5d                   	pop    %ebp
 4c3:	c3                   	ret    
 4c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  neg = 0;
 4c8:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%ebp)
 4cf:	eb 87                	jmp    458 <printint+0x28>
 4d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4d8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4df:	90                   	nop

000004e0 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
 4e0:	f3 0f 1e fb          	endbr32 
 4e4:	55                   	push   %ebp
 4e5:	89 e5                	mov    %esp,%ebp
 4e7:	57                   	push   %edi
 4e8:	56                   	push   %esi
 4e9:	53                   	push   %ebx
 4ea:	83 ec 2c             	sub    $0x2c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 4ed:	8b 75 0c             	mov    0xc(%ebp),%esi
 4f0:	0f b6 1e             	movzbl (%esi),%ebx
 4f3:	84 db                	test   %bl,%bl
 4f5:	0f 84 b4 00 00 00    	je     5af <printf+0xcf>
  ap = (uint*)(void*)&fmt + 1;
 4fb:	8d 45 10             	lea    0x10(%ebp),%eax
 4fe:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 501:	8d 7d e7             	lea    -0x19(%ebp),%edi
  state = 0;
 504:	31 d2                	xor    %edx,%edx
  ap = (uint*)(void*)&fmt + 1;
 506:	89 45 d0             	mov    %eax,-0x30(%ebp)
 509:	eb 33                	jmp    53e <printf+0x5e>
 50b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 50f:	90                   	nop
 510:	89 55 d4             	mov    %edx,-0x2c(%ebp)
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
 513:	ba 25 00 00 00       	mov    $0x25,%edx
      if(c == '%'){
 518:	83 f8 25             	cmp    $0x25,%eax
 51b:	74 17                	je     534 <printf+0x54>
  write(fd, &c, 1);
 51d:	83 ec 04             	sub    $0x4,%esp
 520:	88 5d e7             	mov    %bl,-0x19(%ebp)
 523:	6a 01                	push   $0x1
 525:	57                   	push   %edi
 526:	ff 75 08             	push   0x8(%ebp)
 529:	e8 55 fe ff ff       	call   383 <write>
 52e:	8b 55 d4             	mov    -0x2c(%ebp),%edx
      } else {
        putc(fd, c);
 531:	83 c4 10             	add    $0x10,%esp
  for(i = 0; fmt[i]; i++){
 534:	0f b6 1e             	movzbl (%esi),%ebx
 537:	83 c6 01             	add    $0x1,%esi
 53a:	84 db                	test   %bl,%bl
 53c:	74 71                	je     5af <printf+0xcf>
    c = fmt[i] & 0xff;
 53e:	0f be cb             	movsbl %bl,%ecx
 541:	0f b6 c3             	movzbl %bl,%eax
    if(state == 0){
 544:	85 d2                	test   %edx,%edx
 546:	74 c8                	je     510 <printf+0x30>
      }
    } else if(state == '%'){
 548:	83 fa 25             	cmp    $0x25,%edx
 54b:	75 e7                	jne    534 <printf+0x54>
      if(c == 'd'){
 54d:	83 f8 64             	cmp    $0x64,%eax
 550:	0f 84 9a 00 00 00    	je     5f0 <printf+0x110>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 556:	81 e1 f7 00 00 00    	and    $0xf7,%ecx
 55c:	83 f9 70             	cmp    $0x70,%ecx
 55f:	74 5f                	je     5c0 <printf+0xe0>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 561:	83 f8 73             	cmp    $0x73,%eax
 564:	0f 84 d6 00 00 00    	je     640 <printf+0x160>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 56a:	83 f8 63             	cmp    $0x63,%eax
 56d:	0f 84 8d 00 00 00    	je     600 <printf+0x120>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
 573:	83 f8 25             	cmp    $0x25,%eax
 576:	0f 84 b4 00 00 00    	je     630 <printf+0x150>
  write(fd, &c, 1);
 57c:	83 ec 04             	sub    $0x4,%esp
 57f:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
 583:	6a 01                	push   $0x1
 585:	57                   	push   %edi
 586:	ff 75 08             	push   0x8(%ebp)
 589:	e8 f5 fd ff ff       	call   383 <write>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
        putc(fd, c);
 58e:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 591:	83 c4 0c             	add    $0xc,%esp
 594:	6a 01                	push   $0x1
  for(i = 0; fmt[i]; i++){
 596:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 599:	57                   	push   %edi
 59a:	ff 75 08             	push   0x8(%ebp)
 59d:	e8 e1 fd ff ff       	call   383 <write>
  for(i = 0; fmt[i]; i++){
 5a2:	0f b6 5e ff          	movzbl -0x1(%esi),%ebx
        putc(fd, c);
 5a6:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 5a9:	31 d2                	xor    %edx,%edx
  for(i = 0; fmt[i]; i++){
 5ab:	84 db                	test   %bl,%bl
 5ad:	75 8f                	jne    53e <printf+0x5e>
    }
  }
}
 5af:	8d 65 f4             	lea    -0xc(%ebp),%esp
 5b2:	5b                   	pop    %ebx
 5b3:	5e                   	pop    %esi
 5b4:	5f                   	pop    %edi
 5b5:	5d                   	pop    %ebp
 5b6:	c3                   	ret    
 5b7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 5be:	66 90                	xchg   %ax,%ax
        printint(fd, *ap, 16, 0);
 5c0:	83 ec 0c             	sub    $0xc,%esp
 5c3:	b9 10 00 00 00       	mov    $0x10,%ecx
 5c8:	6a 00                	push   $0x0
 5ca:	8b 5d d0             	mov    -0x30(%ebp),%ebx
 5cd:	8b 45 08             	mov    0x8(%ebp),%eax
 5d0:	8b 13                	mov    (%ebx),%edx
 5d2:	e8 59 fe ff ff       	call   430 <printint>
        ap++;
 5d7:	89 d8                	mov    %ebx,%eax
 5d9:	83 c4 10             	add    $0x10,%esp
      state = 0;
 5dc:	31 d2                	xor    %edx,%edx
        ap++;
 5de:	83 c0 04             	add    $0x4,%eax
 5e1:	89 45 d0             	mov    %eax,-0x30(%ebp)
 5e4:	e9 4b ff ff ff       	jmp    534 <printf+0x54>
 5e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        printint(fd, *ap, 10, 1);
 5f0:	83 ec 0c             	sub    $0xc,%esp
 5f3:	b9 0a 00 00 00       	mov    $0xa,%ecx
 5f8:	6a 01                	push   $0x1
 5fa:	eb ce                	jmp    5ca <printf+0xea>
 5fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        putc(fd, *ap);
 600:	8b 5d d0             	mov    -0x30(%ebp),%ebx
  write(fd, &c, 1);
 603:	83 ec 04             	sub    $0x4,%esp
        putc(fd, *ap);
 606:	8b 03                	mov    (%ebx),%eax
  write(fd, &c, 1);
 608:	6a 01                	push   $0x1
        ap++;
 60a:	83 c3 04             	add    $0x4,%ebx
  write(fd, &c, 1);
 60d:	57                   	push   %edi
 60e:	ff 75 08             	push   0x8(%ebp)
        putc(fd, *ap);
 611:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 614:	e8 6a fd ff ff       	call   383 <write>
        ap++;
 619:	89 5d d0             	mov    %ebx,-0x30(%ebp)
 61c:	83 c4 10             	add    $0x10,%esp
      state = 0;
 61f:	31 d2                	xor    %edx,%edx
 621:	e9 0e ff ff ff       	jmp    534 <printf+0x54>
 626:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 62d:	8d 76 00             	lea    0x0(%esi),%esi
        putc(fd, c);
 630:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 633:	83 ec 04             	sub    $0x4,%esp
 636:	e9 59 ff ff ff       	jmp    594 <printf+0xb4>
 63b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 63f:	90                   	nop
        s = (char*)*ap;
 640:	8b 45 d0             	mov    -0x30(%ebp),%eax
 643:	8b 18                	mov    (%eax),%ebx
        ap++;
 645:	83 c0 04             	add    $0x4,%eax
 648:	89 45 d0             	mov    %eax,-0x30(%ebp)
        if(s == 0)
 64b:	85 db                	test   %ebx,%ebx
 64d:	74 17                	je     666 <printf+0x186>
        while(*s != 0){
 64f:	0f b6 03             	movzbl (%ebx),%eax
      state = 0;
 652:	31 d2                	xor    %edx,%edx
        while(*s != 0){
 654:	84 c0                	test   %al,%al
 656:	0f 84 d8 fe ff ff    	je     534 <printf+0x54>
 65c:	89 75 d4             	mov    %esi,-0x2c(%ebp)
 65f:	89 de                	mov    %ebx,%esi
 661:	8b 5d 08             	mov    0x8(%ebp),%ebx
 664:	eb 1a                	jmp    680 <printf+0x1a0>
          s = "(null)";
 666:	bb b3 08 00 00       	mov    $0x8b3,%ebx
        while(*s != 0){
 66b:	89 75 d4             	mov    %esi,-0x2c(%ebp)
 66e:	b8 28 00 00 00       	mov    $0x28,%eax
 673:	89 de                	mov    %ebx,%esi
 675:	8b 5d 08             	mov    0x8(%ebp),%ebx
 678:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 67f:	90                   	nop
  write(fd, &c, 1);
 680:	83 ec 04             	sub    $0x4,%esp
          s++;
 683:	83 c6 01             	add    $0x1,%esi
 686:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 689:	6a 01                	push   $0x1
 68b:	57                   	push   %edi
 68c:	53                   	push   %ebx
 68d:	e8 f1 fc ff ff       	call   383 <write>
        while(*s != 0){
 692:	0f b6 06             	movzbl (%esi),%eax
 695:	83 c4 10             	add    $0x10,%esp
 698:	84 c0                	test   %al,%al
 69a:	75 e4                	jne    680 <printf+0x1a0>
      state = 0;
 69c:	8b 75 d4             	mov    -0x2c(%ebp),%esi
 69f:	31 d2                	xor    %edx,%edx
 6a1:	e9 8e fe ff ff       	jmp    534 <printf+0x54>
 6a6:	66 90                	xchg   %ax,%ax
 6a8:	66 90                	xchg   %ax,%ax
 6aa:	66 90                	xchg   %ax,%ax
 6ac:	66 90                	xchg   %ax,%ax
 6ae:	66 90                	xchg   %ax,%ax

000006b0 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6b0:	f3 0f 1e fb          	endbr32 
 6b4:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6b5:	a1 64 0b 00 00       	mov    0xb64,%eax
{
 6ba:	89 e5                	mov    %esp,%ebp
 6bc:	57                   	push   %edi
 6bd:	56                   	push   %esi
 6be:	53                   	push   %ebx
 6bf:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = (Header*)ap - 1;
 6c2:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6c5:	8d 76 00             	lea    0x0(%esi),%esi
 6c8:	89 c2                	mov    %eax,%edx
 6ca:	8b 00                	mov    (%eax),%eax
 6cc:	39 ca                	cmp    %ecx,%edx
 6ce:	73 30                	jae    700 <free+0x50>
 6d0:	39 c1                	cmp    %eax,%ecx
 6d2:	72 04                	jb     6d8 <free+0x28>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 6d4:	39 c2                	cmp    %eax,%edx
 6d6:	72 f0                	jb     6c8 <free+0x18>
      break;
  if(bp + bp->s.size == p->s.ptr){
 6d8:	8b 73 fc             	mov    -0x4(%ebx),%esi
 6db:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 6de:	39 f8                	cmp    %edi,%eax
 6e0:	74 30                	je     712 <free+0x62>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 6e2:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 6e5:	8b 42 04             	mov    0x4(%edx),%eax
 6e8:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 6eb:	39 f1                	cmp    %esi,%ecx
 6ed:	74 3a                	je     729 <free+0x79>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 6ef:	89 0a                	mov    %ecx,(%edx)
  freep = p;
}
 6f1:	5b                   	pop    %ebx
  freep = p;
 6f2:	89 15 64 0b 00 00    	mov    %edx,0xb64
}
 6f8:	5e                   	pop    %esi
 6f9:	5f                   	pop    %edi
 6fa:	5d                   	pop    %ebp
 6fb:	c3                   	ret    
 6fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 700:	39 c2                	cmp    %eax,%edx
 702:	72 c4                	jb     6c8 <free+0x18>
 704:	39 c1                	cmp    %eax,%ecx
 706:	73 c0                	jae    6c8 <free+0x18>
  if(bp + bp->s.size == p->s.ptr){
 708:	8b 73 fc             	mov    -0x4(%ebx),%esi
 70b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 70e:	39 f8                	cmp    %edi,%eax
 710:	75 d0                	jne    6e2 <free+0x32>
    bp->s.size += p->s.ptr->s.size;
 712:	03 70 04             	add    0x4(%eax),%esi
 715:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 718:	8b 02                	mov    (%edx),%eax
 71a:	8b 00                	mov    (%eax),%eax
 71c:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 71f:	8b 42 04             	mov    0x4(%edx),%eax
 722:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 725:	39 f1                	cmp    %esi,%ecx
 727:	75 c6                	jne    6ef <free+0x3f>
    p->s.size += bp->s.size;
 729:	03 43 fc             	add    -0x4(%ebx),%eax
  freep = p;
 72c:	89 15 64 0b 00 00    	mov    %edx,0xb64
    p->s.size += bp->s.size;
 732:	89 42 04             	mov    %eax,0x4(%edx)
    p->s.ptr = bp->s.ptr;
 735:	8b 43 f8             	mov    -0x8(%ebx),%eax
 738:	89 02                	mov    %eax,(%edx)
}
 73a:	5b                   	pop    %ebx
 73b:	5e                   	pop    %esi
 73c:	5f                   	pop    %edi
 73d:	5d                   	pop    %ebp
 73e:	c3                   	ret    
 73f:	90                   	nop

00000740 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
 740:	f3 0f 1e fb          	endbr32 
 744:	55                   	push   %ebp
 745:	89 e5                	mov    %esp,%ebp
 747:	57                   	push   %edi
 748:	56                   	push   %esi
 749:	53                   	push   %ebx
 74a:	83 ec 1c             	sub    $0x1c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 74d:	8b 45 08             	mov    0x8(%ebp),%eax
  if((prevp = freep) == 0){
 750:	8b 3d 64 0b 00 00    	mov    0xb64,%edi
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 756:	8d 70 07             	lea    0x7(%eax),%esi
 759:	c1 ee 03             	shr    $0x3,%esi
 75c:	83 c6 01             	add    $0x1,%esi
  if((prevp = freep) == 0){
 75f:	85 ff                	test   %edi,%edi
 761:	0f 84 a9 00 00 00    	je     810 <malloc+0xd0>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 767:	8b 07                	mov    (%edi),%eax
    if(p->s.size >= nunits){
 769:	8b 48 04             	mov    0x4(%eax),%ecx
 76c:	39 f1                	cmp    %esi,%ecx
 76e:	73 6d                	jae    7dd <malloc+0x9d>
 770:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
 776:	bb 00 10 00 00       	mov    $0x1000,%ebx
 77b:	0f 43 de             	cmovae %esi,%ebx
  p = sbrk(nu * sizeof(Header));
 77e:	8d 0c dd 00 00 00 00 	lea    0x0(,%ebx,8),%ecx
 785:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
 788:	eb 17                	jmp    7a1 <malloc+0x61>
 78a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 790:	8b 10                	mov    (%eax),%edx
    if(p->s.size >= nunits){
 792:	8b 4a 04             	mov    0x4(%edx),%ecx
 795:	39 f1                	cmp    %esi,%ecx
 797:	73 4f                	jae    7e8 <malloc+0xa8>
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
 799:	8b 3d 64 0b 00 00    	mov    0xb64,%edi
 79f:	89 d0                	mov    %edx,%eax
 7a1:	39 c7                	cmp    %eax,%edi
 7a3:	75 eb                	jne    790 <malloc+0x50>
  p = sbrk(nu * sizeof(Header));
 7a5:	83 ec 0c             	sub    $0xc,%esp
 7a8:	ff 75 e4             	push   -0x1c(%ebp)
 7ab:	e8 3b fc ff ff       	call   3eb <sbrk>
  if(p == (char*)-1)
 7b0:	83 c4 10             	add    $0x10,%esp
 7b3:	83 f8 ff             	cmp    $0xffffffff,%eax
 7b6:	74 1b                	je     7d3 <malloc+0x93>
  hp->s.size = nu;
 7b8:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 7bb:	83 ec 0c             	sub    $0xc,%esp
 7be:	83 c0 08             	add    $0x8,%eax
 7c1:	50                   	push   %eax
 7c2:	e8 e9 fe ff ff       	call   6b0 <free>
  return freep;
 7c7:	a1 64 0b 00 00       	mov    0xb64,%eax
      if((p = morecore(nunits)) == 0)
 7cc:	83 c4 10             	add    $0x10,%esp
 7cf:	85 c0                	test   %eax,%eax
 7d1:	75 bd                	jne    790 <malloc+0x50>
        return 0;
  }
}
 7d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return 0;
 7d6:	31 c0                	xor    %eax,%eax
}
 7d8:	5b                   	pop    %ebx
 7d9:	5e                   	pop    %esi
 7da:	5f                   	pop    %edi
 7db:	5d                   	pop    %ebp
 7dc:	c3                   	ret    
    if(p->s.size >= nunits){
 7dd:	89 c2                	mov    %eax,%edx
 7df:	89 f8                	mov    %edi,%eax
 7e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->s.size == nunits)
 7e8:	39 ce                	cmp    %ecx,%esi
 7ea:	74 54                	je     840 <malloc+0x100>
        p->s.size -= nunits;
 7ec:	29 f1                	sub    %esi,%ecx
 7ee:	89 4a 04             	mov    %ecx,0x4(%edx)
        p += p->s.size;
 7f1:	8d 14 ca             	lea    (%edx,%ecx,8),%edx
        p->s.size = nunits;
 7f4:	89 72 04             	mov    %esi,0x4(%edx)
      freep = prevp;
 7f7:	a3 64 0b 00 00       	mov    %eax,0xb64
}
 7fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return (void*)(p + 1);
 7ff:	8d 42 08             	lea    0x8(%edx),%eax
}
 802:	5b                   	pop    %ebx
 803:	5e                   	pop    %esi
 804:	5f                   	pop    %edi
 805:	5d                   	pop    %ebp
 806:	c3                   	ret    
 807:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 80e:	66 90                	xchg   %ax,%ax
    base.s.ptr = freep = prevp = &base;
 810:	c7 05 64 0b 00 00 68 	movl   $0xb68,0xb64
 817:	0b 00 00 
    base.s.size = 0;
 81a:	bf 68 0b 00 00       	mov    $0xb68,%edi
    base.s.ptr = freep = prevp = &base;
 81f:	c7 05 68 0b 00 00 68 	movl   $0xb68,0xb68
 826:	0b 00 00 
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 829:	89 f8                	mov    %edi,%eax
    base.s.size = 0;
 82b:	c7 05 6c 0b 00 00 00 	movl   $0x0,0xb6c
 832:	00 00 00 
    if(p->s.size >= nunits){
 835:	e9 36 ff ff ff       	jmp    770 <malloc+0x30>
 83a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        prevp->s.ptr = p->s.ptr;
 840:	8b 0a                	mov    (%edx),%ecx
 842:	89 08                	mov    %ecx,(%eax)
 844:	eb b1                	jmp    7f7 <malloc+0xb7>
