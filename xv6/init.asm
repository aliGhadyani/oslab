
_init:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:

char *argv[] = { "sh", 0 };

int
main(void)
{
   0:	f3 0f 1e fb          	endbr32 
   4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   8:	83 e4 f0             	and    $0xfffffff0,%esp
   b:	ff 71 fc             	push   -0x4(%ecx)
   e:	55                   	push   %ebp
   f:	89 e5                	mov    %esp,%ebp
  11:	53                   	push   %ebx
  12:	51                   	push   %ecx
  int pid, wpid;

  if(open("console", O_RDWR) < 0){
  13:	83 ec 08             	sub    $0x8,%esp
  16:	6a 02                	push   $0x2
  18:	68 28 09 00 00       	push   $0x928
  1d:	e8 81 03 00 00       	call   3a3 <open>
  22:	83 c4 10             	add    $0x10,%esp
  25:	85 c0                	test   %eax,%eax
  27:	0f 88 ab 00 00 00    	js     d8 <main+0xd8>
    mknod("console", 1, 1);
    open("console", O_RDWR);
  }
  dup(0);  // stdout
  2d:	83 ec 0c             	sub    $0xc,%esp
  30:	6a 00                	push   $0x0
  32:	e8 a4 03 00 00       	call   3db <dup>
  dup(0);  // stderr
  37:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  3e:	e8 98 03 00 00       	call   3db <dup>
  printf(1, " OS-Lab Fall 2021\n    Team members:\n    1. Ali Ghadyani\n    2. Alireza Roshandel\n    3. Javad Afsari\n");
  43:	58                   	pop    %eax
  44:	5a                   	pop    %edx
  45:	68 78 09 00 00       	push   $0x978
  4a:	6a 01                	push   $0x1
  4c:	e8 3f 05 00 00       	call   590 <printf>
  51:	83 c4 10             	add    $0x10,%esp
  54:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(;;){
    printf(1, "init: starting sh\n");
  58:	83 ec 08             	sub    $0x8,%esp
  5b:	68 30 09 00 00       	push   $0x930
  60:	6a 01                	push   $0x1
  62:	e8 29 05 00 00       	call   590 <printf>
    pid = fork();
  67:	e8 ef 02 00 00       	call   35b <fork>
    if(pid < 0){
  6c:	83 c4 10             	add    $0x10,%esp
    pid = fork();
  6f:	89 c3                	mov    %eax,%ebx
    if(pid < 0){
  71:	85 c0                	test   %eax,%eax
  73:	78 2c                	js     a1 <main+0xa1>
      printf(1, "init: fork failed\n");
      exit();
    }
    if(pid == 0){
  75:	74 3d                	je     b4 <main+0xb4>
  77:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  7e:	66 90                	xchg   %ax,%ax
      exec("sh", argv);
      printf(1, "init: exec sh failed\n");
      exit();
    }
    while((wpid=wait()) >= 0 && wpid != pid)
  80:	e8 e6 02 00 00       	call   36b <wait>
  85:	85 c0                	test   %eax,%eax
  87:	78 cf                	js     58 <main+0x58>
  89:	39 c3                	cmp    %eax,%ebx
  8b:	74 cb                	je     58 <main+0x58>
      printf(1, "zombie!\n");
  8d:	83 ec 08             	sub    $0x8,%esp
  90:	68 6f 09 00 00       	push   $0x96f
  95:	6a 01                	push   $0x1
  97:	e8 f4 04 00 00       	call   590 <printf>
  9c:	83 c4 10             	add    $0x10,%esp
  9f:	eb df                	jmp    80 <main+0x80>
      printf(1, "init: fork failed\n");
  a1:	53                   	push   %ebx
  a2:	53                   	push   %ebx
  a3:	68 43 09 00 00       	push   $0x943
  a8:	6a 01                	push   $0x1
  aa:	e8 e1 04 00 00       	call   590 <printf>
      exit();
  af:	e8 af 02 00 00       	call   363 <exit>
      exec("sh", argv);
  b4:	50                   	push   %eax
  b5:	50                   	push   %eax
  b6:	68 c4 0c 00 00       	push   $0xcc4
  bb:	68 56 09 00 00       	push   $0x956
  c0:	e8 d6 02 00 00       	call   39b <exec>
      printf(1, "init: exec sh failed\n");
  c5:	5a                   	pop    %edx
  c6:	59                   	pop    %ecx
  c7:	68 59 09 00 00       	push   $0x959
  cc:	6a 01                	push   $0x1
  ce:	e8 bd 04 00 00       	call   590 <printf>
      exit();
  d3:	e8 8b 02 00 00       	call   363 <exit>
    mknod("console", 1, 1);
  d8:	51                   	push   %ecx
  d9:	6a 01                	push   $0x1
  db:	6a 01                	push   $0x1
  dd:	68 28 09 00 00       	push   $0x928
  e2:	e8 c4 02 00 00       	call   3ab <mknod>
    open("console", O_RDWR);
  e7:	5b                   	pop    %ebx
  e8:	58                   	pop    %eax
  e9:	6a 02                	push   $0x2
  eb:	68 28 09 00 00       	push   $0x928
  f0:	e8 ae 02 00 00       	call   3a3 <open>
  f5:	83 c4 10             	add    $0x10,%esp
  f8:	e9 30 ff ff ff       	jmp    2d <main+0x2d>
  fd:	66 90                	xchg   %ax,%ax
  ff:	90                   	nop

00000100 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
 100:	f3 0f 1e fb          	endbr32 
 104:	55                   	push   %ebp
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
 105:	31 c0                	xor    %eax,%eax
{
 107:	89 e5                	mov    %esp,%ebp
 109:	53                   	push   %ebx
 10a:	8b 4d 08             	mov    0x8(%ebp),%ecx
 10d:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while((*s++ = *t++) != 0)
 110:	0f b6 14 03          	movzbl (%ebx,%eax,1),%edx
 114:	88 14 01             	mov    %dl,(%ecx,%eax,1)
 117:	83 c0 01             	add    $0x1,%eax
 11a:	84 d2                	test   %dl,%dl
 11c:	75 f2                	jne    110 <strcpy+0x10>
    ;
  return os;
}
 11e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 121:	89 c8                	mov    %ecx,%eax
 123:	c9                   	leave  
 124:	c3                   	ret    
 125:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 12c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000130 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 130:	f3 0f 1e fb          	endbr32 
 134:	55                   	push   %ebp
 135:	89 e5                	mov    %esp,%ebp
 137:	53                   	push   %ebx
 138:	8b 4d 08             	mov    0x8(%ebp),%ecx
 13b:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
 13e:	0f b6 01             	movzbl (%ecx),%eax
 141:	0f b6 1a             	movzbl (%edx),%ebx
 144:	84 c0                	test   %al,%al
 146:	75 18                	jne    160 <strcmp+0x30>
 148:	eb 2a                	jmp    174 <strcmp+0x44>
 14a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
 150:	0f b6 41 01          	movzbl 0x1(%ecx),%eax
    p++, q++;
 154:	83 c1 01             	add    $0x1,%ecx
 157:	8d 5a 01             	lea    0x1(%edx),%ebx
  while(*p && *p == *q)
 15a:	84 c0                	test   %al,%al
 15c:	74 12                	je     170 <strcmp+0x40>
    p++, q++;
 15e:	89 da                	mov    %ebx,%edx
  while(*p && *p == *q)
 160:	0f b6 1a             	movzbl (%edx),%ebx
 163:	38 c3                	cmp    %al,%bl
 165:	74 e9                	je     150 <strcmp+0x20>
  return (uchar)*p - (uchar)*q;
 167:	29 d8                	sub    %ebx,%eax
}
 169:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 16c:	c9                   	leave  
 16d:	c3                   	ret    
 16e:	66 90                	xchg   %ax,%ax
  return (uchar)*p - (uchar)*q;
 170:	0f b6 5a 01          	movzbl 0x1(%edx),%ebx
 174:	31 c0                	xor    %eax,%eax
 176:	29 d8                	sub    %ebx,%eax
}
 178:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 17b:	c9                   	leave  
 17c:	c3                   	ret    
 17d:	8d 76 00             	lea    0x0(%esi),%esi

00000180 <strlen>:

uint
strlen(const char *s)
{
 180:	f3 0f 1e fb          	endbr32 
 184:	55                   	push   %ebp
 185:	89 e5                	mov    %esp,%ebp
 187:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
 18a:	80 3a 00             	cmpb   $0x0,(%edx)
 18d:	74 21                	je     1b0 <strlen+0x30>
 18f:	31 c0                	xor    %eax,%eax
 191:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 198:	83 c0 01             	add    $0x1,%eax
 19b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
 19f:	89 c1                	mov    %eax,%ecx
 1a1:	75 f5                	jne    198 <strlen+0x18>
    ;
  return n;
}
 1a3:	89 c8                	mov    %ecx,%eax
 1a5:	5d                   	pop    %ebp
 1a6:	c3                   	ret    
 1a7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 1ae:	66 90                	xchg   %ax,%ax
  for(n = 0; s[n]; n++)
 1b0:	31 c9                	xor    %ecx,%ecx
}
 1b2:	5d                   	pop    %ebp
 1b3:	89 c8                	mov    %ecx,%eax
 1b5:	c3                   	ret    
 1b6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 1bd:	8d 76 00             	lea    0x0(%esi),%esi

000001c0 <memset>:

void*
memset(void *dst, int c, uint n)
{
 1c0:	f3 0f 1e fb          	endbr32 
 1c4:	55                   	push   %ebp
 1c5:	89 e5                	mov    %esp,%ebp
 1c7:	57                   	push   %edi
 1c8:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 1cb:	8b 4d 10             	mov    0x10(%ebp),%ecx
 1ce:	8b 45 0c             	mov    0xc(%ebp),%eax
 1d1:	89 d7                	mov    %edx,%edi
 1d3:	fc                   	cld    
 1d4:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 1d6:	8b 7d fc             	mov    -0x4(%ebp),%edi
 1d9:	89 d0                	mov    %edx,%eax
 1db:	c9                   	leave  
 1dc:	c3                   	ret    
 1dd:	8d 76 00             	lea    0x0(%esi),%esi

000001e0 <strchr>:

char*
strchr(const char *s, char c)
{
 1e0:	f3 0f 1e fb          	endbr32 
 1e4:	55                   	push   %ebp
 1e5:	89 e5                	mov    %esp,%ebp
 1e7:	8b 45 08             	mov    0x8(%ebp),%eax
 1ea:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 1ee:	0f b6 10             	movzbl (%eax),%edx
 1f1:	84 d2                	test   %dl,%dl
 1f3:	75 16                	jne    20b <strchr+0x2b>
 1f5:	eb 21                	jmp    218 <strchr+0x38>
 1f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 1fe:	66 90                	xchg   %ax,%ax
 200:	0f b6 50 01          	movzbl 0x1(%eax),%edx
 204:	83 c0 01             	add    $0x1,%eax
 207:	84 d2                	test   %dl,%dl
 209:	74 0d                	je     218 <strchr+0x38>
    if(*s == c)
 20b:	38 d1                	cmp    %dl,%cl
 20d:	75 f1                	jne    200 <strchr+0x20>
      return (char*)s;
  return 0;
}
 20f:	5d                   	pop    %ebp
 210:	c3                   	ret    
 211:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return 0;
 218:	31 c0                	xor    %eax,%eax
}
 21a:	5d                   	pop    %ebp
 21b:	c3                   	ret    
 21c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000220 <gets>:

char*
gets(char *buf, int max)
{
 220:	f3 0f 1e fb          	endbr32 
 224:	55                   	push   %ebp
 225:	89 e5                	mov    %esp,%ebp
 227:	57                   	push   %edi
 228:	56                   	push   %esi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    cc = read(0, &c, 1);
 229:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 22c:	53                   	push   %ebx
  for(i=0; i+1 < max; ){
 22d:	31 db                	xor    %ebx,%ebx
{
 22f:	83 ec 1c             	sub    $0x1c,%esp
  for(i=0; i+1 < max; ){
 232:	eb 2b                	jmp    25f <gets+0x3f>
 234:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cc = read(0, &c, 1);
 238:	83 ec 04             	sub    $0x4,%esp
 23b:	6a 01                	push   $0x1
 23d:	57                   	push   %edi
 23e:	6a 00                	push   $0x0
 240:	e8 36 01 00 00       	call   37b <read>
    if(cc < 1)
 245:	83 c4 10             	add    $0x10,%esp
 248:	85 c0                	test   %eax,%eax
 24a:	7e 1d                	jle    269 <gets+0x49>
      break;
    buf[i++] = c;
 24c:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 250:	8b 55 08             	mov    0x8(%ebp),%edx
 253:	88 44 1a ff          	mov    %al,-0x1(%edx,%ebx,1)
    if(c == '\n' || c == '\r')
 257:	3c 0a                	cmp    $0xa,%al
 259:	74 25                	je     280 <gets+0x60>
 25b:	3c 0d                	cmp    $0xd,%al
 25d:	74 21                	je     280 <gets+0x60>
  for(i=0; i+1 < max; ){
 25f:	89 de                	mov    %ebx,%esi
 261:	83 c3 01             	add    $0x1,%ebx
 264:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
 267:	7c cf                	jl     238 <gets+0x18>
      break;
  }
  buf[i] = '\0';
 269:	8b 45 08             	mov    0x8(%ebp),%eax
 26c:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
  return buf;
}
 270:	8d 65 f4             	lea    -0xc(%ebp),%esp
 273:	5b                   	pop    %ebx
 274:	5e                   	pop    %esi
 275:	5f                   	pop    %edi
 276:	5d                   	pop    %ebp
 277:	c3                   	ret    
 278:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 27f:	90                   	nop
  buf[i] = '\0';
 280:	8b 45 08             	mov    0x8(%ebp),%eax
 283:	89 de                	mov    %ebx,%esi
 285:	c6 04 30 00          	movb   $0x0,(%eax,%esi,1)
}
 289:	8d 65 f4             	lea    -0xc(%ebp),%esp
 28c:	5b                   	pop    %ebx
 28d:	5e                   	pop    %esi
 28e:	5f                   	pop    %edi
 28f:	5d                   	pop    %ebp
 290:	c3                   	ret    
 291:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 298:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 29f:	90                   	nop

000002a0 <stat>:

int
stat(const char *n, struct stat *st)
{
 2a0:	f3 0f 1e fb          	endbr32 
 2a4:	55                   	push   %ebp
 2a5:	89 e5                	mov    %esp,%ebp
 2a7:	56                   	push   %esi
 2a8:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 2a9:	83 ec 08             	sub    $0x8,%esp
 2ac:	6a 00                	push   $0x0
 2ae:	ff 75 08             	push   0x8(%ebp)
 2b1:	e8 ed 00 00 00       	call   3a3 <open>
  if(fd < 0)
 2b6:	83 c4 10             	add    $0x10,%esp
 2b9:	85 c0                	test   %eax,%eax
 2bb:	78 2b                	js     2e8 <stat+0x48>
    return -1;
  r = fstat(fd, st);
 2bd:	83 ec 08             	sub    $0x8,%esp
 2c0:	ff 75 0c             	push   0xc(%ebp)
 2c3:	89 c3                	mov    %eax,%ebx
 2c5:	50                   	push   %eax
 2c6:	e8 f0 00 00 00       	call   3bb <fstat>
  close(fd);
 2cb:	89 1c 24             	mov    %ebx,(%esp)
  r = fstat(fd, st);
 2ce:	89 c6                	mov    %eax,%esi
  close(fd);
 2d0:	e8 b6 00 00 00       	call   38b <close>
  return r;
 2d5:	83 c4 10             	add    $0x10,%esp
}
 2d8:	8d 65 f8             	lea    -0x8(%ebp),%esp
 2db:	89 f0                	mov    %esi,%eax
 2dd:	5b                   	pop    %ebx
 2de:	5e                   	pop    %esi
 2df:	5d                   	pop    %ebp
 2e0:	c3                   	ret    
 2e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
 2e8:	be ff ff ff ff       	mov    $0xffffffff,%esi
 2ed:	eb e9                	jmp    2d8 <stat+0x38>
 2ef:	90                   	nop

000002f0 <atoi>:

int
atoi(const char *s)
{
 2f0:	f3 0f 1e fb          	endbr32 
 2f4:	55                   	push   %ebp
 2f5:	89 e5                	mov    %esp,%ebp
 2f7:	53                   	push   %ebx
 2f8:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 2fb:	0f be 02             	movsbl (%edx),%eax
 2fe:	8d 48 d0             	lea    -0x30(%eax),%ecx
 301:	80 f9 09             	cmp    $0x9,%cl
  n = 0;
 304:	b9 00 00 00 00       	mov    $0x0,%ecx
  while('0' <= *s && *s <= '9')
 309:	77 1a                	ja     325 <atoi+0x35>
 30b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 30f:	90                   	nop
    n = n*10 + *s++ - '0';
 310:	83 c2 01             	add    $0x1,%edx
 313:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
 316:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
  while('0' <= *s && *s <= '9')
 31a:	0f be 02             	movsbl (%edx),%eax
 31d:	8d 58 d0             	lea    -0x30(%eax),%ebx
 320:	80 fb 09             	cmp    $0x9,%bl
 323:	76 eb                	jbe    310 <atoi+0x20>
  return n;
}
 325:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 328:	89 c8                	mov    %ecx,%eax
 32a:	c9                   	leave  
 32b:	c3                   	ret    
 32c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000330 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 330:	f3 0f 1e fb          	endbr32 
 334:	55                   	push   %ebp
 335:	89 e5                	mov    %esp,%ebp
 337:	57                   	push   %edi
 338:	8b 45 10             	mov    0x10(%ebp),%eax
 33b:	8b 55 08             	mov    0x8(%ebp),%edx
 33e:	56                   	push   %esi
 33f:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst;
  const char *src;

  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 342:	85 c0                	test   %eax,%eax
 344:	7e 0f                	jle    355 <memmove+0x25>
 346:	01 d0                	add    %edx,%eax
  dst = vdst;
 348:	89 d7                	mov    %edx,%edi
 34a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *dst++ = *src++;
 350:	a4                   	movsb  %ds:(%esi),%es:(%edi)
  while(n-- > 0)
 351:	39 f8                	cmp    %edi,%eax
 353:	75 fb                	jne    350 <memmove+0x20>
  return vdst;
}
 355:	5e                   	pop    %esi
 356:	89 d0                	mov    %edx,%eax
 358:	5f                   	pop    %edi
 359:	5d                   	pop    %ebp
 35a:	c3                   	ret    

0000035b <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 35b:	b8 01 00 00 00       	mov    $0x1,%eax
 360:	cd 40                	int    $0x40
 362:	c3                   	ret    

00000363 <exit>:
SYSCALL(exit)
 363:	b8 02 00 00 00       	mov    $0x2,%eax
 368:	cd 40                	int    $0x40
 36a:	c3                   	ret    

0000036b <wait>:
SYSCALL(wait)
 36b:	b8 03 00 00 00       	mov    $0x3,%eax
 370:	cd 40                	int    $0x40
 372:	c3                   	ret    

00000373 <pipe>:
SYSCALL(pipe)
 373:	b8 04 00 00 00       	mov    $0x4,%eax
 378:	cd 40                	int    $0x40
 37a:	c3                   	ret    

0000037b <read>:
SYSCALL(read)
 37b:	b8 05 00 00 00       	mov    $0x5,%eax
 380:	cd 40                	int    $0x40
 382:	c3                   	ret    

00000383 <write>:
SYSCALL(write)
 383:	b8 10 00 00 00       	mov    $0x10,%eax
 388:	cd 40                	int    $0x40
 38a:	c3                   	ret    

0000038b <close>:
SYSCALL(close)
 38b:	b8 15 00 00 00       	mov    $0x15,%eax
 390:	cd 40                	int    $0x40
 392:	c3                   	ret    

00000393 <kill>:
SYSCALL(kill)
 393:	b8 06 00 00 00       	mov    $0x6,%eax
 398:	cd 40                	int    $0x40
 39a:	c3                   	ret    

0000039b <exec>:
SYSCALL(exec)
 39b:	b8 07 00 00 00       	mov    $0x7,%eax
 3a0:	cd 40                	int    $0x40
 3a2:	c3                   	ret    

000003a3 <open>:
SYSCALL(open)
 3a3:	b8 0f 00 00 00       	mov    $0xf,%eax
 3a8:	cd 40                	int    $0x40
 3aa:	c3                   	ret    

000003ab <mknod>:
SYSCALL(mknod)
 3ab:	b8 11 00 00 00       	mov    $0x11,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    

000003b3 <unlink>:
SYSCALL(unlink)
 3b3:	b8 12 00 00 00       	mov    $0x12,%eax
 3b8:	cd 40                	int    $0x40
 3ba:	c3                   	ret    

000003bb <fstat>:
SYSCALL(fstat)
 3bb:	b8 08 00 00 00       	mov    $0x8,%eax
 3c0:	cd 40                	int    $0x40
 3c2:	c3                   	ret    

000003c3 <link>:
SYSCALL(link)
 3c3:	b8 13 00 00 00       	mov    $0x13,%eax
 3c8:	cd 40                	int    $0x40
 3ca:	c3                   	ret    

000003cb <mkdir>:
SYSCALL(mkdir)
 3cb:	b8 14 00 00 00       	mov    $0x14,%eax
 3d0:	cd 40                	int    $0x40
 3d2:	c3                   	ret    

000003d3 <chdir>:
SYSCALL(chdir)
 3d3:	b8 09 00 00 00       	mov    $0x9,%eax
 3d8:	cd 40                	int    $0x40
 3da:	c3                   	ret    

000003db <dup>:
SYSCALL(dup)
 3db:	b8 0a 00 00 00       	mov    $0xa,%eax
 3e0:	cd 40                	int    $0x40
 3e2:	c3                   	ret    

000003e3 <getpid>:
SYSCALL(getpid)
 3e3:	b8 0b 00 00 00       	mov    $0xb,%eax
 3e8:	cd 40                	int    $0x40
 3ea:	c3                   	ret    

000003eb <sbrk>:
SYSCALL(sbrk)
 3eb:	b8 0c 00 00 00       	mov    $0xc,%eax
 3f0:	cd 40                	int    $0x40
 3f2:	c3                   	ret    

000003f3 <sleep>:
SYSCALL(sleep)
 3f3:	b8 0d 00 00 00       	mov    $0xd,%eax
 3f8:	cd 40                	int    $0x40
 3fa:	c3                   	ret    

000003fb <uptime>:
SYSCALL(uptime)
 3fb:	b8 0e 00 00 00       	mov    $0xe,%eax
 400:	cd 40                	int    $0x40
 402:	c3                   	ret    

00000403 <poweroff>:
SYSCALL(poweroff)
 403:	b8 16 00 00 00       	mov    $0x16,%eax
 408:	cd 40                	int    $0x40
 40a:	c3                   	ret    

0000040b <calculate_sum_of_digits>:
SYSCALL(calculate_sum_of_digits)
 40b:	b8 17 00 00 00       	mov    $0x17,%eax
 410:	cd 40                	int    $0x40
 412:	c3                   	ret    

00000413 <get_parent_pid>:
SYSCALL(get_parent_pid)
 413:	b8 18 00 00 00       	mov    $0x18,%eax
 418:	cd 40                	int    $0x40
 41a:	c3                   	ret    

0000041b <set_process_parent>:
SYSCALL(set_process_parent)
 41b:	b8 19 00 00 00       	mov    $0x19,%eax
 420:	cd 40                	int    $0x40
 422:	c3                   	ret    
 423:	66 90                	xchg   %ax,%ax
 425:	66 90                	xchg   %ax,%ax
 427:	66 90                	xchg   %ax,%ax
 429:	66 90                	xchg   %ax,%ax
 42b:	66 90                	xchg   %ax,%ax
 42d:	66 90                	xchg   %ax,%ax
 42f:	90                   	nop

00000430 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
 430:	55                   	push   %ebp
 431:	89 e5                	mov    %esp,%ebp
 433:	57                   	push   %edi
 434:	56                   	push   %esi
 435:	53                   	push   %ebx
 436:	83 ec 3c             	sub    $0x3c,%esp
 439:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
 43c:	89 d1                	mov    %edx,%ecx
{
 43e:	89 45 b8             	mov    %eax,-0x48(%ebp)
  if(sgn && xx < 0){
 441:	85 d2                	test   %edx,%edx
 443:	0f 89 7f 00 00 00    	jns    4c8 <printint+0x98>
 449:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
 44d:	74 79                	je     4c8 <printint+0x98>
    neg = 1;
 44f:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
    x = -xx;
 456:	f7 d9                	neg    %ecx
  } else {
    x = xx;
  }

  i = 0;
 458:	31 db                	xor    %ebx,%ebx
 45a:	8d 75 d7             	lea    -0x29(%ebp),%esi
 45d:	8d 76 00             	lea    0x0(%esi),%esi
  do{
    buf[i++] = digits[x % base];
 460:	89 c8                	mov    %ecx,%eax
 462:	31 d2                	xor    %edx,%edx
 464:	89 cf                	mov    %ecx,%edi
 466:	f7 75 c4             	divl   -0x3c(%ebp)
 469:	0f b6 92 e8 09 00 00 	movzbl 0x9e8(%edx),%edx
 470:	89 45 c0             	mov    %eax,-0x40(%ebp)
 473:	89 d8                	mov    %ebx,%eax
 475:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
 478:	8b 4d c0             	mov    -0x40(%ebp),%ecx
    buf[i++] = digits[x % base];
 47b:	88 14 1e             	mov    %dl,(%esi,%ebx,1)
  }while((x /= base) != 0);
 47e:	39 7d c4             	cmp    %edi,-0x3c(%ebp)
 481:	76 dd                	jbe    460 <printint+0x30>
  if(neg)
 483:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 486:	85 c9                	test   %ecx,%ecx
 488:	74 0c                	je     496 <printint+0x66>
    buf[i++] = '-';
 48a:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
 48f:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
 491:	ba 2d 00 00 00       	mov    $0x2d,%edx

  while(--i >= 0)
 496:	8b 7d b8             	mov    -0x48(%ebp),%edi
 499:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
 49d:	eb 07                	jmp    4a6 <printint+0x76>
 49f:	90                   	nop
    putc(fd, buf[i]);
 4a0:	0f b6 13             	movzbl (%ebx),%edx
 4a3:	83 eb 01             	sub    $0x1,%ebx
  write(fd, &c, 1);
 4a6:	83 ec 04             	sub    $0x4,%esp
 4a9:	88 55 d7             	mov    %dl,-0x29(%ebp)
 4ac:	6a 01                	push   $0x1
 4ae:	56                   	push   %esi
 4af:	57                   	push   %edi
 4b0:	e8 ce fe ff ff       	call   383 <write>
  while(--i >= 0)
 4b5:	83 c4 10             	add    $0x10,%esp
 4b8:	39 de                	cmp    %ebx,%esi
 4ba:	75 e4                	jne    4a0 <printint+0x70>
}
 4bc:	8d 65 f4             	lea    -0xc(%ebp),%esp
 4bf:	5b                   	pop    %ebx
 4c0:	5e                   	pop    %esi
 4c1:	5f                   	pop    %edi
 4c2:	5d                   	pop    %ebp
 4c3:	c3                   	ret    
 4c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  neg = 0;
 4c8:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%ebp)
 4cf:	eb 87                	jmp    458 <printint+0x28>
 4d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4d8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 4df:	90                   	nop

000004e0 <printfloat>:

// MOD-2 : Added printf for floats
void
printfloat(int fd, float xx)
{
 4e0:	f3 0f 1e fb          	endbr32 
 4e4:	55                   	push   %ebp

  int beg=(int)(xx);
	int fin=(int)(xx*100)-beg*100;
  printint(fd, beg, 10, 1);
 4e5:	b9 0a 00 00 00       	mov    $0xa,%ecx
{
 4ea:	89 e5                	mov    %esp,%ebp
 4ec:	57                   	push   %edi
 4ed:	56                   	push   %esi
  write(fd, &c, 1);
 4ee:	8d 7d e7             	lea    -0x19(%ebp),%edi
{
 4f1:	53                   	push   %ebx
 4f2:	83 ec 38             	sub    $0x38,%esp
 4f5:	d9 45 0c             	flds   0xc(%ebp)
 4f8:	8b 75 08             	mov    0x8(%ebp),%esi
  int beg=(int)(xx);
 4fb:	d9 7d d6             	fnstcw -0x2a(%ebp)
  printint(fd, beg, 10, 1);
 4fe:	6a 01                	push   $0x1
  int beg=(int)(xx);
 500:	0f b7 45 d6          	movzwl -0x2a(%ebp),%eax
 504:	80 cc 0c             	or     $0xc,%ah
 507:	66 89 45 d4          	mov    %ax,-0x2c(%ebp)
  printint(fd, beg, 10, 1);
 50b:	89 f0                	mov    %esi,%eax
  int beg=(int)(xx);
 50d:	d9 6d d4             	fldcw  -0x2c(%ebp)
 510:	db 55 d0             	fistl  -0x30(%ebp)
 513:	d9 6d d6             	fldcw  -0x2a(%ebp)
 516:	8b 55 d0             	mov    -0x30(%ebp),%edx
	int fin=(int)(xx*100)-beg*100;
 519:	d8 0d fc 09 00 00    	fmuls  0x9fc
 51f:	d9 6d d4             	fldcw  -0x2c(%ebp)
 522:	db 5d d0             	fistpl -0x30(%ebp)
 525:	d9 6d d6             	fldcw  -0x2a(%ebp)
 528:	6b da 9c             	imul   $0xffffff9c,%edx,%ebx
 52b:	03 5d d0             	add    -0x30(%ebp),%ebx
  printint(fd, beg, 10, 1);
 52e:	e8 fd fe ff ff       	call   430 <printint>
  write(fd, &c, 1);
 533:	83 c4 0c             	add    $0xc,%esp
 536:	c6 45 e7 2e          	movb   $0x2e,-0x19(%ebp)
 53a:	6a 01                	push   $0x1
 53c:	57                   	push   %edi
 53d:	56                   	push   %esi
 53e:	e8 40 fe ff ff       	call   383 <write>
  putc(fd, '.');
	if(fin<10)
 543:	83 c4 10             	add    $0x10,%esp
 546:	83 fb 09             	cmp    $0x9,%ebx
 549:	7e 25                	jle    570 <printfloat+0x90>
    putc(fd, '0');
	printint(fd, fin, 10, 1);
 54b:	c7 45 08 01 00 00 00 	movl   $0x1,0x8(%ebp)
}
 552:	8d 65 f4             	lea    -0xc(%ebp),%esp
	printint(fd, fin, 10, 1);
 555:	89 da                	mov    %ebx,%edx
 557:	89 f0                	mov    %esi,%eax
}
 559:	5b                   	pop    %ebx
	printint(fd, fin, 10, 1);
 55a:	b9 0a 00 00 00       	mov    $0xa,%ecx
}
 55f:	5e                   	pop    %esi
 560:	5f                   	pop    %edi
 561:	5d                   	pop    %ebp
	printint(fd, fin, 10, 1);
 562:	e9 c9 fe ff ff       	jmp    430 <printint>
 567:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 56e:	66 90                	xchg   %ax,%ax
  write(fd, &c, 1);
 570:	83 ec 04             	sub    $0x4,%esp
 573:	c6 45 e7 30          	movb   $0x30,-0x19(%ebp)
 577:	6a 01                	push   $0x1
 579:	57                   	push   %edi
 57a:	56                   	push   %esi
 57b:	e8 03 fe ff ff       	call   383 <write>
    putc(fd, '0');
 580:	83 c4 10             	add    $0x10,%esp
 583:	eb c6                	jmp    54b <printfloat+0x6b>
 585:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 58c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000590 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s, %f.
void
printf(int fd, const char *fmt, ...)
{
 590:	f3 0f 1e fb          	endbr32 
 594:	55                   	push   %ebp
 595:	89 e5                	mov    %esp,%ebp
 597:	57                   	push   %edi
 598:	56                   	push   %esi
 599:	53                   	push   %ebx
 59a:	83 ec 2c             	sub    $0x2c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 59d:	8b 75 0c             	mov    0xc(%ebp),%esi
 5a0:	0f b6 1e             	movzbl (%esi),%ebx
 5a3:	84 db                	test   %bl,%bl
 5a5:	0f 84 bd 00 00 00    	je     668 <printf+0xd8>
  ap = (uint*)(void*)&fmt + 1;
 5ab:	8d 45 10             	lea    0x10(%ebp),%eax
 5ae:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 5b1:	8d 7d e7             	lea    -0x19(%ebp),%edi
  state = 0;
 5b4:	31 d2                	xor    %edx,%edx
  ap = (uint*)(void*)&fmt + 1;
 5b6:	89 45 cc             	mov    %eax,-0x34(%ebp)
 5b9:	eb 33                	jmp    5ee <printf+0x5e>
 5bb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 5bf:	90                   	nop
 5c0:	89 55 d0             	mov    %edx,-0x30(%ebp)
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
 5c3:	ba 25 00 00 00       	mov    $0x25,%edx
      if(c == '%'){
 5c8:	83 f8 25             	cmp    $0x25,%eax
 5cb:	74 17                	je     5e4 <printf+0x54>
  write(fd, &c, 1);
 5cd:	83 ec 04             	sub    $0x4,%esp
 5d0:	88 5d e7             	mov    %bl,-0x19(%ebp)
 5d3:	6a 01                	push   $0x1
 5d5:	57                   	push   %edi
 5d6:	ff 75 08             	push   0x8(%ebp)
 5d9:	e8 a5 fd ff ff       	call   383 <write>
 5de:	8b 55 d0             	mov    -0x30(%ebp),%edx
      } else {
        putc(fd, c);
 5e1:	83 c4 10             	add    $0x10,%esp
  for(i = 0; fmt[i]; i++){
 5e4:	0f b6 1e             	movzbl (%esi),%ebx
 5e7:	83 c6 01             	add    $0x1,%esi
 5ea:	84 db                	test   %bl,%bl
 5ec:	74 7a                	je     668 <printf+0xd8>
    c = fmt[i] & 0xff;
 5ee:	0f be cb             	movsbl %bl,%ecx
 5f1:	0f b6 c3             	movzbl %bl,%eax
    if(state == 0){
 5f4:	85 d2                	test   %edx,%edx
 5f6:	74 c8                	je     5c0 <printf+0x30>
      }
    } else if(state == '%'){
 5f8:	83 fa 25             	cmp    $0x25,%edx
 5fb:	75 e7                	jne    5e4 <printf+0x54>
      if(c == 'd'){
 5fd:	83 f8 64             	cmp    $0x64,%eax
 600:	0f 84 9a 00 00 00    	je     6a0 <printf+0x110>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 606:	81 e1 f7 00 00 00    	and    $0xf7,%ecx
 60c:	83 f9 70             	cmp    $0x70,%ecx
 60f:	74 5f                	je     670 <printf+0xe0>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 611:	83 f8 73             	cmp    $0x73,%eax
 614:	0f 84 d6 00 00 00    	je     6f0 <printf+0x160>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 61a:	83 f8 63             	cmp    $0x63,%eax
 61d:	0f 84 8d 00 00 00    	je     6b0 <printf+0x120>
        putc(fd, *ap);
        ap++;
      } else if(c == 'f'){ // MOD-2
 623:	83 f8 66             	cmp    $0x66,%eax
 626:	0f 84 f4 00 00 00    	je     720 <printf+0x190>
        printfloat(fd, (float)*ap);
        ap++;
      } else if(c == '%'){
 62c:	83 f8 25             	cmp    $0x25,%eax
 62f:	0f 84 ab 00 00 00    	je     6e0 <printf+0x150>
  write(fd, &c, 1);
 635:	83 ec 04             	sub    $0x4,%esp
 638:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
 63c:	6a 01                	push   $0x1
 63e:	57                   	push   %edi
 63f:	ff 75 08             	push   0x8(%ebp)
 642:	e8 3c fd ff ff       	call   383 <write>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
        putc(fd, c);
 647:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 64a:	83 c4 0c             	add    $0xc,%esp
 64d:	6a 01                	push   $0x1
  for(i = 0; fmt[i]; i++){
 64f:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 652:	57                   	push   %edi
 653:	ff 75 08             	push   0x8(%ebp)
 656:	e8 28 fd ff ff       	call   383 <write>
  for(i = 0; fmt[i]; i++){
 65b:	0f b6 5e ff          	movzbl -0x1(%esi),%ebx
        putc(fd, c);
 65f:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 662:	31 d2                	xor    %edx,%edx
  for(i = 0; fmt[i]; i++){
 664:	84 db                	test   %bl,%bl
 666:	75 86                	jne    5ee <printf+0x5e>
    }
  }
 668:	8d 65 f4             	lea    -0xc(%ebp),%esp
 66b:	5b                   	pop    %ebx
 66c:	5e                   	pop    %esi
 66d:	5f                   	pop    %edi
 66e:	5d                   	pop    %ebp
 66f:	c3                   	ret    
        printint(fd, *ap, 16, 0);
 670:	83 ec 0c             	sub    $0xc,%esp
 673:	b9 10 00 00 00       	mov    $0x10,%ecx
 678:	6a 00                	push   $0x0
 67a:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 67d:	8b 45 08             	mov    0x8(%ebp),%eax
 680:	8b 13                	mov    (%ebx),%edx
 682:	e8 a9 fd ff ff       	call   430 <printint>
        ap++;
 687:	89 d8                	mov    %ebx,%eax
 689:	83 c4 10             	add    $0x10,%esp
      state = 0;
 68c:	31 d2                	xor    %edx,%edx
        ap++;
 68e:	83 c0 04             	add    $0x4,%eax
 691:	89 45 cc             	mov    %eax,-0x34(%ebp)
 694:	e9 4b ff ff ff       	jmp    5e4 <printf+0x54>
 699:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        printint(fd, *ap, 10, 1);
 6a0:	83 ec 0c             	sub    $0xc,%esp
 6a3:	b9 0a 00 00 00       	mov    $0xa,%ecx
 6a8:	6a 01                	push   $0x1
 6aa:	eb ce                	jmp    67a <printf+0xea>
 6ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        putc(fd, *ap);
 6b0:	8b 5d cc             	mov    -0x34(%ebp),%ebx
  write(fd, &c, 1);
 6b3:	83 ec 04             	sub    $0x4,%esp
        putc(fd, *ap);
 6b6:	8b 03                	mov    (%ebx),%eax
  write(fd, &c, 1);
 6b8:	6a 01                	push   $0x1
        ap++;
 6ba:	83 c3 04             	add    $0x4,%ebx
  write(fd, &c, 1);
 6bd:	57                   	push   %edi
 6be:	ff 75 08             	push   0x8(%ebp)
        putc(fd, *ap);
 6c1:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 6c4:	e8 ba fc ff ff       	call   383 <write>
        ap++;
 6c9:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 6cc:	83 c4 10             	add    $0x10,%esp
      state = 0;
 6cf:	31 d2                	xor    %edx,%edx
 6d1:	e9 0e ff ff ff       	jmp    5e4 <printf+0x54>
 6d6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 6dd:	8d 76 00             	lea    0x0(%esi),%esi
        putc(fd, c);
 6e0:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 6e3:	83 ec 04             	sub    $0x4,%esp
 6e6:	e9 62 ff ff ff       	jmp    64d <printf+0xbd>
 6eb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 6ef:	90                   	nop
        s = (char*)*ap;
 6f0:	8b 45 cc             	mov    -0x34(%ebp),%eax
 6f3:	8b 18                	mov    (%eax),%ebx
        ap++;
 6f5:	83 c0 04             	add    $0x4,%eax
 6f8:	89 45 cc             	mov    %eax,-0x34(%ebp)
        if(s == 0)
 6fb:	85 db                	test   %ebx,%ebx
 6fd:	74 4f                	je     74e <printf+0x1be>
        while(*s != 0){
 6ff:	0f b6 03             	movzbl (%ebx),%eax
      state = 0;
 702:	31 d2                	xor    %edx,%edx
        while(*s != 0){
 704:	84 c0                	test   %al,%al
 706:	0f 84 d8 fe ff ff    	je     5e4 <printf+0x54>
 70c:	89 75 d0             	mov    %esi,-0x30(%ebp)
 70f:	89 de                	mov    %ebx,%esi
 711:	8b 5d 08             	mov    0x8(%ebp),%ebx
 714:	eb 4a                	jmp    760 <printf+0x1d0>
 716:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 71d:	8d 76 00             	lea    0x0(%esi),%esi
        printfloat(fd, (float)*ap);
 720:	8b 5d cc             	mov    -0x34(%ebp),%ebx
 723:	31 d2                	xor    %edx,%edx
 725:	83 ec 0c             	sub    $0xc,%esp
 728:	89 55 d4             	mov    %edx,-0x2c(%ebp)
 72b:	8b 03                	mov    (%ebx),%eax
        ap++;
 72d:	83 c3 04             	add    $0x4,%ebx
        printfloat(fd, (float)*ap);
 730:	89 45 d0             	mov    %eax,-0x30(%ebp)
 733:	df 6d d0             	fildll -0x30(%ebp)
 736:	d9 1c 24             	fstps  (%esp)
 739:	ff 75 08             	push   0x8(%ebp)
 73c:	e8 9f fd ff ff       	call   4e0 <printfloat>
        ap++;
 741:	89 5d cc             	mov    %ebx,-0x34(%ebp)
 744:	83 c4 10             	add    $0x10,%esp
      state = 0;
 747:	31 d2                	xor    %edx,%edx
 749:	e9 96 fe ff ff       	jmp    5e4 <printf+0x54>
          s = "(null)";
 74e:	bb de 09 00 00       	mov    $0x9de,%ebx
        while(*s != 0){
 753:	89 75 d0             	mov    %esi,-0x30(%ebp)
 756:	b8 28 00 00 00       	mov    $0x28,%eax
 75b:	89 de                	mov    %ebx,%esi
 75d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  write(fd, &c, 1);
 760:	83 ec 04             	sub    $0x4,%esp
          s++;
 763:	83 c6 01             	add    $0x1,%esi
 766:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 769:	6a 01                	push   $0x1
 76b:	57                   	push   %edi
 76c:	53                   	push   %ebx
 76d:	e8 11 fc ff ff       	call   383 <write>
        while(*s != 0){
 772:	0f b6 06             	movzbl (%esi),%eax
 775:	83 c4 10             	add    $0x10,%esp
 778:	84 c0                	test   %al,%al
 77a:	75 e4                	jne    760 <printf+0x1d0>
      state = 0;
 77c:	8b 75 d0             	mov    -0x30(%ebp),%esi
 77f:	31 d2                	xor    %edx,%edx
 781:	e9 5e fe ff ff       	jmp    5e4 <printf+0x54>
 786:	66 90                	xchg   %ax,%ax
 788:	66 90                	xchg   %ax,%ax
 78a:	66 90                	xchg   %ax,%ax
 78c:	66 90                	xchg   %ax,%ax
 78e:	66 90                	xchg   %ax,%ax

00000790 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 790:	f3 0f 1e fb          	endbr32 
 794:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 795:	a1 cc 0c 00 00       	mov    0xccc,%eax
{
 79a:	89 e5                	mov    %esp,%ebp
 79c:	57                   	push   %edi
 79d:	56                   	push   %esi
 79e:	53                   	push   %ebx
 79f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = (Header*)ap - 1;
 7a2:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7a5:	8d 76 00             	lea    0x0(%esi),%esi
 7a8:	89 c2                	mov    %eax,%edx
 7aa:	8b 00                	mov    (%eax),%eax
 7ac:	39 ca                	cmp    %ecx,%edx
 7ae:	73 30                	jae    7e0 <free+0x50>
 7b0:	39 c1                	cmp    %eax,%ecx
 7b2:	72 04                	jb     7b8 <free+0x28>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 7b4:	39 c2                	cmp    %eax,%edx
 7b6:	72 f0                	jb     7a8 <free+0x18>
      break;
  if(bp + bp->s.size == p->s.ptr){
 7b8:	8b 73 fc             	mov    -0x4(%ebx),%esi
 7bb:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 7be:	39 f8                	cmp    %edi,%eax
 7c0:	74 30                	je     7f2 <free+0x62>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 7c2:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 7c5:	8b 42 04             	mov    0x4(%edx),%eax
 7c8:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 7cb:	39 f1                	cmp    %esi,%ecx
 7cd:	74 3a                	je     809 <free+0x79>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 7cf:	89 0a                	mov    %ecx,(%edx)
  freep = p;
}
 7d1:	5b                   	pop    %ebx
  freep = p;
 7d2:	89 15 cc 0c 00 00    	mov    %edx,0xccc
}
 7d8:	5e                   	pop    %esi
 7d9:	5f                   	pop    %edi
 7da:	5d                   	pop    %ebp
 7db:	c3                   	ret    
 7dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 7e0:	39 c2                	cmp    %eax,%edx
 7e2:	72 c4                	jb     7a8 <free+0x18>
 7e4:	39 c1                	cmp    %eax,%ecx
 7e6:	73 c0                	jae    7a8 <free+0x18>
  if(bp + bp->s.size == p->s.ptr){
 7e8:	8b 73 fc             	mov    -0x4(%ebx),%esi
 7eb:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 7ee:	39 f8                	cmp    %edi,%eax
 7f0:	75 d0                	jne    7c2 <free+0x32>
    bp->s.size += p->s.ptr->s.size;
 7f2:	03 70 04             	add    0x4(%eax),%esi
 7f5:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 7f8:	8b 02                	mov    (%edx),%eax
 7fa:	8b 00                	mov    (%eax),%eax
 7fc:	89 43 f8             	mov    %eax,-0x8(%ebx)
  if(p + p->s.size == bp){
 7ff:	8b 42 04             	mov    0x4(%edx),%eax
 802:	8d 34 c2             	lea    (%edx,%eax,8),%esi
 805:	39 f1                	cmp    %esi,%ecx
 807:	75 c6                	jne    7cf <free+0x3f>
    p->s.size += bp->s.size;
 809:	03 43 fc             	add    -0x4(%ebx),%eax
  freep = p;
 80c:	89 15 cc 0c 00 00    	mov    %edx,0xccc
    p->s.size += bp->s.size;
 812:	89 42 04             	mov    %eax,0x4(%edx)
    p->s.ptr = bp->s.ptr;
 815:	8b 43 f8             	mov    -0x8(%ebx),%eax
 818:	89 02                	mov    %eax,(%edx)
}
 81a:	5b                   	pop    %ebx
 81b:	5e                   	pop    %esi
 81c:	5f                   	pop    %edi
 81d:	5d                   	pop    %ebp
 81e:	c3                   	ret    
 81f:	90                   	nop

00000820 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
 820:	f3 0f 1e fb          	endbr32 
 824:	55                   	push   %ebp
 825:	89 e5                	mov    %esp,%ebp
 827:	57                   	push   %edi
 828:	56                   	push   %esi
 829:	53                   	push   %ebx
 82a:	83 ec 1c             	sub    $0x1c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 82d:	8b 45 08             	mov    0x8(%ebp),%eax
  if((prevp = freep) == 0){
 830:	8b 3d cc 0c 00 00    	mov    0xccc,%edi
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 836:	8d 70 07             	lea    0x7(%eax),%esi
 839:	c1 ee 03             	shr    $0x3,%esi
 83c:	83 c6 01             	add    $0x1,%esi
  if((prevp = freep) == 0){
 83f:	85 ff                	test   %edi,%edi
 841:	0f 84 a9 00 00 00    	je     8f0 <malloc+0xd0>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 847:	8b 07                	mov    (%edi),%eax
    if(p->s.size >= nunits){
 849:	8b 48 04             	mov    0x4(%eax),%ecx
 84c:	39 f1                	cmp    %esi,%ecx
 84e:	73 6d                	jae    8bd <malloc+0x9d>
 850:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
 856:	bb 00 10 00 00       	mov    $0x1000,%ebx
 85b:	0f 43 de             	cmovae %esi,%ebx
  p = sbrk(nu * sizeof(Header));
 85e:	8d 0c dd 00 00 00 00 	lea    0x0(,%ebx,8),%ecx
 865:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
 868:	eb 17                	jmp    881 <malloc+0x61>
 86a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 870:	8b 10                	mov    (%eax),%edx
    if(p->s.size >= nunits){
 872:	8b 4a 04             	mov    0x4(%edx),%ecx
 875:	39 f1                	cmp    %esi,%ecx
 877:	73 4f                	jae    8c8 <malloc+0xa8>
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
 879:	8b 3d cc 0c 00 00    	mov    0xccc,%edi
 87f:	89 d0                	mov    %edx,%eax
 881:	39 c7                	cmp    %eax,%edi
 883:	75 eb                	jne    870 <malloc+0x50>
  p = sbrk(nu * sizeof(Header));
 885:	83 ec 0c             	sub    $0xc,%esp
 888:	ff 75 e4             	push   -0x1c(%ebp)
 88b:	e8 5b fb ff ff       	call   3eb <sbrk>
  if(p == (char*)-1)
 890:	83 c4 10             	add    $0x10,%esp
 893:	83 f8 ff             	cmp    $0xffffffff,%eax
 896:	74 1b                	je     8b3 <malloc+0x93>
  hp->s.size = nu;
 898:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 89b:	83 ec 0c             	sub    $0xc,%esp
 89e:	83 c0 08             	add    $0x8,%eax
 8a1:	50                   	push   %eax
 8a2:	e8 e9 fe ff ff       	call   790 <free>
  return freep;
 8a7:	a1 cc 0c 00 00       	mov    0xccc,%eax
      if((p = morecore(nunits)) == 0)
 8ac:	83 c4 10             	add    $0x10,%esp
 8af:	85 c0                	test   %eax,%eax
 8b1:	75 bd                	jne    870 <malloc+0x50>
        return 0;
  }
}
 8b3:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return 0;
 8b6:	31 c0                	xor    %eax,%eax
}
 8b8:	5b                   	pop    %ebx
 8b9:	5e                   	pop    %esi
 8ba:	5f                   	pop    %edi
 8bb:	5d                   	pop    %ebp
 8bc:	c3                   	ret    
    if(p->s.size >= nunits){
 8bd:	89 c2                	mov    %eax,%edx
 8bf:	89 f8                	mov    %edi,%eax
 8c1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->s.size == nunits)
 8c8:	39 ce                	cmp    %ecx,%esi
 8ca:	74 54                	je     920 <malloc+0x100>
        p->s.size -= nunits;
 8cc:	29 f1                	sub    %esi,%ecx
 8ce:	89 4a 04             	mov    %ecx,0x4(%edx)
        p += p->s.size;
 8d1:	8d 14 ca             	lea    (%edx,%ecx,8),%edx
        p->s.size = nunits;
 8d4:	89 72 04             	mov    %esi,0x4(%edx)
      freep = prevp;
 8d7:	a3 cc 0c 00 00       	mov    %eax,0xccc
}
 8dc:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return (void*)(p + 1);
 8df:	8d 42 08             	lea    0x8(%edx),%eax
}
 8e2:	5b                   	pop    %ebx
 8e3:	5e                   	pop    %esi
 8e4:	5f                   	pop    %edi
 8e5:	5d                   	pop    %ebp
 8e6:	c3                   	ret    
 8e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 8ee:	66 90                	xchg   %ax,%ax
    base.s.ptr = freep = prevp = &base;
 8f0:	c7 05 cc 0c 00 00 d0 	movl   $0xcd0,0xccc
 8f7:	0c 00 00 
    base.s.size = 0;
 8fa:	bf d0 0c 00 00       	mov    $0xcd0,%edi
    base.s.ptr = freep = prevp = &base;
 8ff:	c7 05 d0 0c 00 00 d0 	movl   $0xcd0,0xcd0
 906:	0c 00 00 
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 909:	89 f8                	mov    %edi,%eax
    base.s.size = 0;
 90b:	c7 05 d4 0c 00 00 00 	movl   $0x0,0xcd4
 912:	00 00 00 
    if(p->s.size >= nunits){
 915:	e9 36 ff ff ff       	jmp    850 <malloc+0x30>
 91a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        prevp->s.ptr = p->s.ptr;
 920:	8b 0a                	mov    (%edx),%ecx
 922:	89 08                	mov    %ecx,(%eax)
 924:	eb b1                	jmp    8d7 <malloc+0xb7>
