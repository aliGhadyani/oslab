#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int
num(char a[]){
    int i=0;
    int r=0;
    while(a[i]!='\0'){
        r*=10;
        r+=a[i]-48;
        i++;
    }
    return r;
}
int
digit(int a){
    int r=1;
    while(a/10>0){
        a /= 10;
        r++;
    }
    return r;
}
void
charnum(char a[], int i){
    for(int j=digit(i)-1; j>=0; j--){
        a[j] = (char)(48+(i%10));
        i/=10;
    }
    return;
}
int
main(int argc, char *argv[]){
    if(argc<=1 || argc>2){
        printf(1, "argument count invalid\n");
        exit();
    }
    int a = num(argv[1]);
    int file;
    if(open("factor_result.txt", O_RDONLY) != -1){
    	unlink("factor_result.txt");
    }
    file = open("factor_result.txt", O_CREATE | O_WRONLY );
    if(file == -1){
        printf(1, "failed in creat and open file!\n");
        exit();
    }
    for(int i=1; i<=a; i++){
        if(a%i==0){
            int k = digit(i);
            char p[k];
            charnum(p, i);
            write(file, p, digit(i));
            write(file, " ", 1);
        }
    }
    write(file, "\n", 1);
    close(file);
    printf(1, "%f\n", 128.512f);
    exit();
}
