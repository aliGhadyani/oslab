#include "types.h"
#include "user.h"
#include "stat.h"

int main(int argc, char* argv[]) {
    int proc = atoi(argv[1]);
    printf(1, "debugger: debugger runs for pid %d\n", proc);
    if(set_process_parent(proc) < 0) {
        printf(1, "debugger: process is debugging under another process\n");
        exit();
    }
    printf(1, "debugger: current process(%d) set as debugger parent of proc %d\n", getpid(), proc);
    printf(1, "debugger: wait for proc %d to exit\n", proc);
    while(wait() != -1);
    printf(1, "debugger: proc %d exited\n", proc);
    exit();
}